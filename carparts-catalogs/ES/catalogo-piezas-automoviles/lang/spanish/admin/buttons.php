<?php
/* --------------------------------------------------------------
   $Id: buttons.php 10580 2017-01-19 13:16:33Z GTB $


   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(categories.php,v 1.22 2002/08/17); www.oscommerce.com 
   (c) 2003	 nextcommerce (categories.php,v 1.10 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

// buttons
define('BUTTON_REVERSE_SELECTION', 'Invertir selección');
define('BUTTON_SWITCH_PRODUCTS', 'Sólo productos');
define('BUTTON_SWITCH_CATEGORIES', 'Sólo categorías');
define('BUTTON_NEW_CATEGORIES', 'Nueva categoría');
define('BUTTON_NEW_PRODUCTS', 'Nuevo artículo');
define('BUTTON_COPY', 'Copiar');
define('BUTTON_BACK', 'Volver');
define('BUTTON_CANCEL', 'Cancelar');
define('BUTTON_EDIT', 'Editar');
define('BUTTON_DELETE', 'Borrar');
define('BUTTON_MOVE', 'Mover');
define('BUTTON_SAVE', 'Guardar');
define('BUTTON_STATUS_ON', 'Estado activo');
define('BUTTON_STATUS_OFF', 'Estado inactivo');
define('BUTTON_EDIT_ATTRIBUTES', 'Editar atributos');
define('BUTTON_INSERT', 'Pegar');
define('BUTTON_UPDATE', 'Actualizar');
define('BUTTON_EXPORT', 'Exportar');
define('BUTTON_CURRENCY_UPDATE', 'Actualizar tasas de cambio');
define('BUTTON_REVIEW_APPROVE', 'OK');
define('BUTTON_SEND_EMAIL', 'Mandar correo electrónico');
define('BUTTON_SEND_COUPON', 'Mandar cupón');
define('BUTTON_INVOICE', 'Factura');
define('BUTTON_PACKINGSLIP', 'Orden de entrega');
define('BUTTON_AFTERBUY_SEND', 'Afterbuy - mandar');
define('BUTTON_NEW_NEWSLETTER', 'Nuevo Newsletter');
define('BUTTON_RESET', 'Reiniciar');
define('BUTTON_STATUS', 'Grupo de clientes');
define('BUTTON_ACCOUNTING', 'Derechos de admin');
define('BUTTON_ORDERS', 'Pedidos');
define('BUTTON_EMAIL', 'Correo electrónico');
define('BUTTON_IPLOG', 'IP-Log');
define('BUTTON_NEW_ORDER', 'Nuevo pedido');
define('BUTTON_CREATE_ACCOUNT', 'Nuevo cliente');
define('BUTTON_SEARCH', 'Buscar');
define('BUTTON_PRODUCT_OPTIONS', 'Opciones del producto');
define('BUTTON_PREVIEW', 'Prevista');
define('BUTTON_MODULE_REMOVE', 'Desinstalar');
define('BUTTON_MODULE_INSTALL', 'Instalar');
define('BUTTON_START', 'Inicio');
define('BUTTON_NEW_CONTENT', 'Nuevo contenido');
define('BUTTON_BACKUP', 'Backup');
define('BUTTON_RESTORE', 'Restaurar');
define('BUTTON_NEW_BANNER', 'Nuevo Banner');
define('BUTTON_UPLOAD', 'Subir');
define('BUTTON_IMPORT', 'Importar');
define('BUTTON_CONFIRM', 'Confirmar');
define('BUTTON_REPORT', 'Reporte');
define('BUTTON_RELEASE', 'Canjear');
define('BUTTON_NEW_LANGUAGE', 'Nuevo idioma');
define('BUTTON_NEW_COUNTRY', 'Nuevo país');
define('BUTTON_NEW_CURRENCY', 'Nueva moneda');
define('BUTTON_NEW_ZONE', 'Nuevo Estado');
define('BUTTON_DETAILS', 'Detalles');
define('BUTTON_NEW_TAX_CLASS', 'Nueva clase de impuesto');
define('BUTTON_NEW_TAX_RATE', 'Nueva Tasa de impuesto');
define('BUTTON_SEND', 'Enviar');
define('BUTTON_REVERSE', 'Anular');

// BOF - Tomcraft - 2009-11-28 - Included xs:booster
define('BUTTON_EDIT_XTBOOSTER', 'Artículo de eBay');
define('BUTTON_XTBOOSTER_MULTI', 'Artículo múltiple de eBay');
// EOF - Tomcraft - 2009-11-28 - Included xs:booster

// BOF - Dokuman - 2010-02-04 - delete cache files in admin section
define('BUTTON_DELETE_CACHE', 'Vaciar caché');
define('BUTTON_DELETE_TEMP_CACHE', 'Vaciar caché de plantillas');
define('BUTTON_DELETE_LOGS', 'Borrar logfiles');
// EOF - Dokuman - 2010-02-04 - delete cache files in admin section

// BOF - web28 - 2011-07-13 - New Button
define('BUTTON_VIEW_PRODUCT', 'Ver artículo');
//EOF - web28 - 2011-07-13 - New Button

define('BUTTON_VALUES', 'Valores');
define('BUTTON_DELETE_BANKTRANSFER', 'Borrar datos bancarios');
define('BUTTON_BLACKLIST', 'Lista negra');
?>

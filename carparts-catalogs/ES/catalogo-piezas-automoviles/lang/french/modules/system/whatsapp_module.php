<?php
/*******************************************************
* file: whatsapp_module.php
* use: language file for system module
* (c) noRiddle 07-2017
* Released under the GNU General Public License
*******************************************************/

define('MODULE_SYSTEM_WHATSAPP_MODULE_HELP_LINK', ' <a onclick="window.open(\'popup_help.php?type=system&modul=whatsapp_module&lng=english\', \'Help\', \'scrollbars=yes,resizable=yes,menubar=yes,width=800,height=600\'); return false" target="_blank" href="popup_help.php?type=system&modul=whatsapp_module&lng=english"><b>[HELP]</b></a>');
define('MODULE_SYSTEM_WHATSAPP_MODULE_HELP_TEXT', '<h2>Needed template adaptations</h2>
<p>The Whatsapp icon must be implemented in the template within a condition whether the module is activated.</p>
<b>Condition whether modul is activated:</b>
<ul>
<li>Condition in template:<br />
<pre>{if $smarty.const.MODULE_WHATSAPP_MODULE_STATUS == \'true\'}<br />&nbsp;&nbsp;&nbsp;&nbsp;<i>HTML</i><br />{/if}</pre>
</li>
<li>Condition in PHP:<br />
<pre>if(MODULE_WHATSAPP_MODULE_STATUS == \'true\') {<br /><i>&nbsp;&nbsp;&nbsp;&nbsp;e.g. Smarty-definition</i><br />}</pre>
</li>
</ul>
<b>HTML for the implementation:</b>
<ul>
<li>In templae:<br />
<pre>&lt;a href="tel:{$smarty.const.MODULE_WHATSAPP_MODULE_TELNO}" title="{$smarty.const.MODULE_WHATSAPP_MODULE_TELNO}"&gt;HERE WHATSAPP IMAGE OR FONT-ICON&lt;/a&gt;</pre>
</li>
<li>In PHP:<br />
<pre>\'&lt;a href="tel:\'.MODULE_WHATSAPP_MODULE_TELNO.\'" title="\'.MODULE_WHATSAPP_MODULE_TELNO.\'"&gt;HERE WHATSAPP IMAGE OR FONT-ICON&lt;/a&gt;\'</pre>
</li>
</ul>');

define('MODULE_WHATSAPP_MODULE_TEXT_TITLE', 'Whatsapp module');
define('MODULE_WHATSAPP_MODULE_TEXT_DESCRIPTION', 'Makes module available which shows a Whatsapp logo.<br />Needs template adaptations! '.MODULE_SYSTEM_WHATSAPP_MODULE_HELP_LINK);
define('MODULE_WHATSAPP_MODULE_STATUS_TITLE', 'Activate Module ?');
define('MODULE_WHATSAPP_MODULE_STATUS_DESC', 'Activate Whatsapp module');
define('MODULE_WHATSAPP_MODULE_TELNO_TITLE', 'Tel.-No. for Whatsapp');
define('MODULE_WHATSAPP_MODULE_TELNO_DESC', 'Enter telephone number here over which shall be connected with Whatsapp.<br />(with Landesvorwahl; Example: for UK: +44 175 ....)');
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_ps_fee.php 10553 2017-01-11 13:45:14Z web28 $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ot_cod_fee.php,v 1.02 2003/02/24); www.oscommerce.com
   (C) 2001 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/


define('MODULE_ORDER_TOTAL_PS_FEE_TITLE', 'Calcul de vous-même');
define('MODULE_ORDER_TOTAL_PS_FEE_DESCRIPTION', 'Calcul de vous-même');

define('MODULE_ORDER_TOTAL_PS_FEE_STATUS_TITLE', 'de vous-même');
define('MODULE_ORDER_TOTAL_PS_FEE_STATUS_DESC', 'Calcul de vous-même');

define('MODULE_ORDER_TOTAL_PS_FEE_SORT_ORDER_TITLE', 'Ordre du triage');
define('MODULE_ORDER_TOTAL_PS_FEE_SORT_ORDER_DESC', 'Ordre d\'affichage');

define('MODULE_ORDER_TOTAL_PS_FEE_TAX_CLASS_TITLE', 'Catégorie fiscale');
define('MODULE_ORDER_TOTAL_PS_FEE_TAX_CLASS_DESC', 'Sélectionnez une catégorie fiscale');

  function define_shipping_titles_ps() {
    $module_keys = str_replace('.php','',MODULE_SHIPPING_INSTALLED);
    $installed_shipping_modules = explode(';',$module_keys);
    //support for ot_shipping
    $installed_shipping_modules[] = 'free';

    if (count($installed_shipping_modules) > 0) {
      foreach($installed_shipping_modules as $shipping_code) {
        $module_type = 'shipping';
        $file = $shipping_code.'.php';
        $shipping_code = strtoupper($shipping_code);
        $title = '';

        if (defined('DIR_FS_LANGUAGES') && file_exists(DIR_FS_LANGUAGES . 'german/modules/' . $module_type . '/' . $file)) {
          include_once(DIR_FS_LANGUAGES . 'german/modules/' . $module_type . '/' . $file);
          $title = constant('MODULE_SHIPPING_'.$shipping_code.'_TEXT_TITLE');
        }
        //support for ot_shipping
        $title = $shipping_code == 'FREE' ? 'Versandkostenfrei (Zusammenfassung Modul ot_shipping)' : $title;
        
        $shipping_code = ($shipping_code == 'FREEAMOUNT') ? 'FREEAMOUNT_FREE' : 'FEE_' . $shipping_code;

define('MODULE_ORDER_TOTAL_PS_'.$shipping_code.'_TITLE', $title);
define('MODULE_ORDER_TOTAL_PS_'.$shipping_code.'_DESC', '<ISO2-Code>:<Preis>, ....<br />
        00 comme code ISO2 permet le tarif pour tous les pays. Si
        00 est utilisé, celui-ci doit être indiqué comme dernier argument. Si 
        pas de 00:9.99 est indiqué, le tarif vers l\'étranger ne sera pas facturé 
        (si possible). Pour exclure un pays uniquement, ne pas indiquer de frais pour ce pays
        Exemple : DE:4.00,CH:,00:9.99<br />-> Explication : expédition vers DE : 4€ /
        Expédition vers CH : pas possible / expédition vers le reste du monde : 9,99€');
      }
    }
  }
  define_shipping_titles_ps();
?>

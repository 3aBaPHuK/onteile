<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_banners.php 35 2013-01-06 09:43:25Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_banners.php, v 1.6 2003/07/12);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   
   adapted modified 2.0.2.2, 07-2017, noRiddle
   ---------------------------------------------------------------------------*/

  require('includes/application_top.php');

  $affiliate_banner_extension = xtc_banner_image_extension();

  if ($_GET['action']) {
    switch ($_GET['action']) {
      case 'setaffiliate_flag':
        if ( ($_GET['affiliate_flag'] == '0') || ($_GET['affiliate_flag'] == '1') ) {
          xtc_set_banner_status($_GET['abID'], $_GET['affiliate_flag']);
          $messageStack->add_session(SUCCESS_BANNER_STATUS_UPDATED, 'success');
        } else {
          $messageStack->add_session(ERROR_UNKNOWN_STATUS_FLAG, 'error');
        }

        xtc_redirect(xtc_href_link(FILENAME_AFFILIATE_BANNER_MANAGER, 'page=' . $_GET['page'] . '&abID=' . $_GET['abID']));
        break;
      case 'insert':
      case 'update':
        $affiliate_banners_id = xtc_db_prepare_input($_POST['affiliate_banners_id']);
        $affiliate_banners_title = xtc_db_prepare_input($_POST['affiliate_banners_title']);
        $affiliate_banners_text = xtc_db_prepare_input($_POST['affiliate_banners_text']);
        $affiliate_link_to  = xtc_db_prepare_input($_POST['affiliate_products_id']);
        $new_affiliate_banners_group = xtc_db_prepare_input($_POST['new_affiliate_banners_group']);
        $affiliate_banners_group = (empty($new_affiliate_banners_group)) ? xtc_db_prepare_input($_POST['affiliate_banners_group']) : $new_affiliate_banners_group;
        $affiliate_banners_image_target = xtc_db_prepare_input($_POST['affiliate_banners_image_target']);
        $db_image_location = '';
        
        if($affiliate_link_to != '0') {
        	$affiliate_products_id = substr($affiliate_link_to, 1);
        	$affiliate_link_type = substr($affiliate_link_to, 0, 1);
        } else {
        	$affiliate_products_id = 0;
        	$affiliate_link_type = '';
        }
        
        $affiliate_banner_error = false;
        if (empty($affiliate_banners_title)) {
          $messageStack->add(ERROR_BANNER_TITLE_REQUIRED, 'error');
          $affiliate_banner_error = true;
          $_GET['action'] = 'new';
        }
/*      if (empty($affiliate_banners_group)) {
          $messageStack->add(ERROR_BANNER_GROUP_REQUIRED, 'error');
          $affiliate_banner_error = true;
        }
*/
		
        $sql_data_array = array('affiliate_banners_title' => $affiliate_banners_title,
                                'affiliate_products_id' => $affiliate_products_id,
                                'affiliate_link_type' => $affiliate_link_type,
                                'affiliate_banners_group' => $affiliate_banners_group,
                                'affiliate_banners_text' => $affiliate_banners_text);
        
        if (($_FILES['affiliate_banners_image']['name'] != '')) {
          if (!is_writeable(DIR_FS_CATALOG_IMAGES)) {
          	$messageStack->add(ERROR_DESTINATION_NOT_WRITEABLE, 'error');
            $affiliate_banner_error = true;
            $_GET['action'] = 'new';
          } else {
          	$image_location = DIR_FS_CATALOG_IMAGES . $_FILES['affiliate_banners_image']['name'];
          	move_uploaded_file($_FILES['affiliate_banners_image']['tmp_name'], $image_location);
          	@chmod($image_location, 0644);

            $db_image_location = $_FILES['affiliate_banners_image']['name'];
            
            $sql_data_array = array_merge($sql_data_array, array('affiliate_banners_image' => $db_image_location));
          }
        }
                                
        if ($_GET['action'] == 'insert') {
        	$insert_sql_data = array('affiliate_date_added' => 'now()',
        	                         'affiliate_status' => '1');
        	                         
        	$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
            xtc_db_perform(TABLE_AFFILIATE_BANNERS, $sql_data_array);
            $affiliate_banners_id = xtc_db_insert_id();

            // Banner ID 1 is generic Product Banner
            if ($affiliate_banners_id==1) xtc_db_query("update " . TABLE_AFFILIATE_BANNERS . " set affiliate_banners_id = affiliate_banners_id + 1");
            $messageStack->add_session(SUCCESS_BANNER_INSERTED, 'success');
        } elseif ($_GET['action'] == 'update') {
        	$insert_sql_data = array('affiliate_date_status_change' => 'now()');
        	$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
        	xtc_db_perform(TABLE_AFFILIATE_BANNERS, $sql_data_array, 'update', 'affiliate_banners_id = \'' . $affiliate_banners_id . '\'');
        	$messageStack->add_session(SUCCESS_BANNER_UPDATED, 'success');
        }
        xtc_redirect(xtc_href_link(FILENAME_AFFILIATE_BANNER_MANAGER, 'page=' . $_GET['page'] . '&abID=' . $affiliate_banners_id));
        break;
      case 'deleteconfirm':
        $affiliate_banners_id = xtc_db_prepare_input($_GET['abID']);
        $delete_image = xtc_db_prepare_input($_POST['delete_image']);

        if ($delete_image == 'on') {
          $affiliate_banner_query = xtc_db_query("select affiliate_banners_image from " . TABLE_AFFILIATE_BANNERS . " where affiliate_banners_id = '" . xtc_db_input($affiliate_banners_id) . "'");
          $affiliate_banner = xtc_db_fetch_array($affiliate_banner_query);
          if (file_exists(DIR_FS_CATALOG_IMAGES . $affiliate_banner['affiliate_banners_image'])) {
            if (is_writeable(DIR_FS_CATALOG_IMAGES . $affiliate_banner['affiliate_banners_image'])) {
              unlink(DIR_FS_CATALOG_IMAGES . $affiliate_banner['affiliate_banners_image']);
            } else {
              $messageStack->add_session(ERROR_IMAGE_IS_NOT_WRITEABLE, 'error');
            }
          } else {
            $messageStack->add_session(ERROR_IMAGE_DOES_NOT_EXIST, 'error');
          }
        }

        xtc_db_query("delete from " . TABLE_AFFILIATE_BANNERS . " where affiliate_banners_id = '" . xtc_db_input($affiliate_banners_id) . "'");
        xtc_db_query("delete from " . TABLE_AFFILIATE_BANNERS_HISTORY . " where affiliate_banners_id = '" . xtc_db_input($affiliate_banners_id) . "'");

        $messageStack->add_session(SUCCESS_BANNER_REMOVED, 'success');

        xtc_redirect(xtc_href_link(FILENAME_AFFILIATE_BANNER_MANAGER, 'page=' . $_GET['page']));
        break;
    }
  }
  
require (DIR_WS_INCLUDES.'head.php');
?>
<script language="javascript">
function popupImageWindow(url) {
  window.open(url,'popupImageWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=100,height=100,screenX=150,screenY=150,top=150,left=150')
}
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<div id="spiffycalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <?php //left_navigation
      if (USE_ADMIN_TOP_MENU == 'false') {
        echo '<td class="columnLeft2">'.PHP_EOL;
        echo '<!-- left_navigation //-->'.PHP_EOL;       
        require_once(DIR_WS_INCLUDES . 'column_left.php');
        echo '<!-- left_navigation eof //-->'.PHP_EOL; 
        echo '</td>'.PHP_EOL;      
      }
      ?>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo xtc_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
  if ($_GET['action'] == 'new') {
    $form_action = 'insert';
    if ($_GET['abID']) {
      $abID = xtc_db_prepare_input($_GET['abID']);
      $form_action = 'update';

      $affiliate_banner_query = xtc_db_query("select * from " . TABLE_AFFILIATE_BANNERS . " where affiliate_banners_id = '" . xtc_db_input($abID) . "'");
      $affiliate_banner = xtc_db_fetch_array($affiliate_banner_query);

      $abInfo = new objectInfo($affiliate_banner);
    } elseif ($_POST) {
      $abInfo = new objectInfo($_POST);
    } else {
      $abInfo = new objectInfo(array());
    }

    $groups_array = array();
    $groups_query = xtc_db_query("select distinct affiliate_banners_group from " . TABLE_AFFILIATE_BANNERS . " order by affiliate_banners_group");
    while ($groups = xtc_db_fetch_array($groups_query)) {
      $groups_array[] = array('id' => $groups['affiliate_banners_group'], 'text' => $groups['affiliate_banners_group']);
    }
?>
      <tr>
        <td><?php echo xtc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr><?php echo xtc_draw_form('new_banner', FILENAME_AFFILIATE_BANNER_MANAGER, 'page=' . $_GET['page'] . '&action=' . $form_action, 'post', 'enctype="multipart/form-data"'); if ($form_action == 'update') echo xtc_draw_hidden_field('affiliate_banners_id', $abID); ?>
        <td><table border="0" cellspacing="2" cellpadding="2"  width="60%">
          <tr>
            <td class="main"><?php echo TEXT_BANNERS_TITLE; ?></td>
            <td class="main"><?php echo xtc_draw_input_field('affiliate_banners_title', $abInfo->affiliate_banners_title, '', true); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo xtc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_BANNERS_LINKED_PRODUCT; ?></td>
            <td class="main">
            <?php
            $products_array[] = array('text' => TEXT_BANNERS_LINKED_HOME, 'id' => '0');
            
            // get all Kategories
            $query = "SELECT * FROM " . TABLE_CATEGORIES_DESCRIPTION . " WHERE categories_id LIKE'%' AND language_id = '" . $_SESSION['languages_id'] . "' ORDER BY categories_name ASC";
            $result = xtc_db_query($query);
            
            while ($line = xtc_db_fetch_array($result)) {
            	$products_array[] = array('text' => 'C - ' . $line['categories_name'], 'id' => 'c' . $line['categories_id']);
            }
            
            $query = "SELECT content_title, content_group FROM " . TABLE_CONTENT_MANAGER . " WHERE languages_id = '" . $_SESSION['languages_id'] . "' ORDER BY content_group ASC";
            $result = xtc_db_query($query);
            
            while ($line = xtc_db_fetch_array($result)) {
            	$products_array[] = array('text' => 'M - ' . $line['content_title'], 'id' => 'm' . $line['content_group']);
            }
            
            $query = "SELECT * FROM  ".TABLE_PRODUCTS_DESCRIPTION."  where products_id LIKE '%' AND language_id = '" . $_SESSION['languages_id'] . "' ORDER BY products_name ASC";
            $result = xtc_db_query($query);
            
            while ($line = xtc_db_fetch_array($result)) {
            	$products_array[] = array('text' => 'P - ' . $line['products_name'], 'id' => 'p' . $line['products_id']);
            }

            echo xtc_draw_pull_down_menu('affiliate_products_id', $products_array, $abInfo->affiliate_link_type . $abInfo->affiliate_products_id);
            
            ?>
            </td>
          </tr>
          <tr>
          	<td></td>
            <td class="main" style="font-size:9px;"><?php echo TEXT_BANNERS_LINKED_PRODUCT_NOTE ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo xtc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php
/*
          <tr>
            <td class="main" valign="top"><?php echo TEXT_BANNERS_GROUP; ?></td>
            <td class="main"><?php echo xtc_draw_pull_down_menu('affiliate_banners_group', $groups_array, $abInfo->affiliate_banners_group) . TEXT_BANNERS_NEW_GROUP . '<br>' . xtc_draw_input_field('new_affiliate_banners_group', '', '', ((sizeof($groups_array) > 0) ? false : true)); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo xtc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
*/
?>
          <tr>
            <td class="main" valign="top"><?php echo TEXT_BANNERS_TEXT; ?></td>
            <td class="main"><?php echo xtc_draw_textarea_field('affiliate_banners_text', 'soft', 40, 10, $abInfo->affiliate_banners_text); ?></td>
          </tr>
		  <tr>
            <td class="main" valign="top"><?php echo TEXT_BANNERS_IMAGE; ?></td>
            <td class="main"><?php echo xtc_draw_file_field('affiliate_banners_image'); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo xtc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo xtc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td align="right" class="main">
          	  <input type="submit" class="button float_right" onClick="this.blur();" value="<?php echo ($form_action == 'insert')?BUTTON_INSERT:BUTTON_UPDATE; ?>">
          	  <?php echo ' <a class="button float_right" onClick="this.blur();" href="' . xtc_href_link(FILENAME_AFFILIATE_BANNER_MANAGER, xtc_get_all_get_params(array('action'))) .'">' . BUTTON_CANCEL . '</a>'; ?>
        	</td>
          </tr>
        </table></td>
      </form></tr>
<?php
  } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_BANNERS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_PRODUCT_ID; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_STATISTICS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    $affiliate_banners_query_raw = "select * from " . TABLE_AFFILIATE_BANNERS . " order by affiliate_banners_title, affiliate_banners_group";
    $affiliate_banners_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $affiliate_banners_query_raw, $affiliate_banners_query_numrows);
    $affiliate_banners_query = xtc_db_query($affiliate_banners_query_raw);
    while ($affiliate_banners = xtc_db_fetch_array($affiliate_banners_query)) {
      $info_query = xtc_db_query("select sum(affiliate_banners_shown) as affiliate_banners_shown, sum(affiliate_banners_clicks) as affiliate_banners_clicks from " . TABLE_AFFILIATE_BANNERS_HISTORY . " where affiliate_banners_id = '" . $affiliate_banners['affiliate_banners_id'] . "'");
      $info = xtc_db_fetch_array($info_query);

      if (((!$_GET['abID']) || ($_GET['abID'] == $affiliate_banners['affiliate_banners_id'])) && (!$abInfo) && (substr($_GET['action'], 0, 3) != 'new')) {
        $abInfo_array = array_merge($affiliate_banners, $info);
        $abInfo = new objectInfo($abInfo_array);
      }

      $affiliate_banners_shown = ($info['affiliate_banners_shown'] != '') ? $info['affiliate_banners_shown'] : '0';
      $affiliate_banners_clicked = ($info['affiliate_banners_clicks'] != '') ? $info['affiliate_banners_clicks'] : '0';

      if ( (is_object($abInfo)) && ($affiliate_banners['affiliate_banners_id'] == $abInfo->affiliate_banners_id) ) {
        echo '              <tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_AFFILIATE_BANNERS,'abID=' . $abInfo->affiliate_banners_id . '&action=new')  . '\'">' . "\n";
      } else {
        echo '              <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_AFFILIATE_BANNERS, 'abID=' . $affiliate_banners['affiliate_banners_id']) . '\'">' . "\n";
      }
?>
                <td class="dataTableContent"><?php echo '<a href="javascript:popupImageWindow(\'' . FILENAME_AFFILIATE_POPUP_IMAGE . '?banner=' . $affiliate_banners['affiliate_banners_id'] . '\')">' . xtc_image(DIR_WS_IMAGES . 'icon_popup.gif', ICON_PREVIEW) . '</a>&nbsp;' . $affiliate_banners['affiliate_banners_title']; ?></td>
                <td class="dataTableContent" align="right"><?php if ($affiliate_banners['affiliate_products_id'] != 0) echo strtoupper($affiliate_banners['affiliate_link_type']) . ' - ' . $affiliate_banners['affiliate_products_id']; else echo '&nbsp;'; ?></td>
                <td class="dataTableContent" align="right"><?php echo $affiliate_banners_shown . ' / ' . $affiliate_banners_clicked; ?></td>
                <td class="dataTableContent" align="right"><?php if ( (is_object($abInfo)) && ($affiliate_banners['affiliate_banners_id'] == $abInfo->affiliate_banners_id) ) { echo xtc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . xtc_href_link(FILENAME_AFFILIATE_BANNER_MANAGER, 'page=' . $_GET['page'] . '&abID=' . $affiliate_banners['affiliate_banners_id']) . '">' . xtc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
              <tr>
                <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $affiliate_banners_split->display_count($affiliate_banners_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_BANNERS); ?></td>
                    <td class="smallText" align="right"><?php echo $affiliate_banners_split->display_links($affiliate_banners_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                  <tr>
                    <td align="right" colspan="2"><?php echo '<a class="button" onClick="this.blur();" href="'.xtc_href_link(FILENAME_AFFILIATE_BANNER_MANAGER, 'action=new').'">'.BUTTON_ADD_BANNER.'</a>'; ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();
  switch ($_GET['action']) {
    case 'delete':
      $heading[] = array('text' => '<b>' . $abInfo->affiliate_banners_title . '</b>');

      $contents = array('form' => xtc_draw_form('affiliate_banners', FILENAME_AFFILIATE_BANNER_MANAGER, 'page=' . $_GET['page'] . '&abID=' . $abInfo->affiliate_banners_id . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br><b>' . $abInfo->affiliate_banners_title . '</b>');
      if ($abInfo->affiliate_banners_image) $contents[] = array('text' => '<br>' . xtc_draw_checkbox_field('delete_image', 'on', true) . ' ' . TEXT_INFO_DELETE_IMAGE);
      $contents[] = array('align' => 'center', 'text' => '<br><input type="submit" value="' . BUTTON_DELETE . '">&nbsp;<a class="button" onClick="this.blur();" href="'.xtc_href_link(FILENAME_AFFILIATE_BANNER_MANAGER, 'page=' . $_GET['page'] . '&abID=' . $_GET['abID']).'">'.BUTTON_CANCEL.'</a>');
      break;
    default:
      if (is_object($abInfo)) {
        $sql = "select products_name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . $abInfo->affiliate_products_id . "' and language_id = '" . $_SESSION['languages_id'] . "'";
        $product_description_query = xtc_db_query($sql);
        $product_description = xtc_db_fetch_array($product_description_query);
        $heading[] = array('text' => '<b>' . $abInfo->affiliate_banners_title . '</b>');

        $contents[] = array('align' => 'center', 'text' => '<a class="button" onClick="this.blur();" href="'.xtc_href_link(FILENAME_AFFILIATE_BANNER_MANAGER, 'page=' . $_GET['page'] . '&abID=' . $abInfo->affiliate_banners_id . '&action=new').'">'.BUTTON_EDIT.'</a> <a class="button" onClick="this.blur();" href="'.xtc_href_link(FILENAME_AFFILIATE_BANNER_MANAGER, 'page=' . $_GET['page'] . '&abID=' . $abInfo->affiliate_banners_id . '&action=delete').'">'.BUTTON_DELETE.'</a>');
        $contents[] = array('text' => $product_description['products_name']);
        $contents[] = array('text' => '<br>' . TEXT_BANNERS_DATE_ADDED . ' ' . xtc_date_short($abInfo->affiliate_date_added));
        $contents[] = array('text' => '' . sprintf(TEXT_BANNERS_STATUS_CHANGE, xtc_date_short($abInfo->affiliate_date_status_change)));
      }
      break;
  }

  if ( (xtc_not_null($heading)) && (xtc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>

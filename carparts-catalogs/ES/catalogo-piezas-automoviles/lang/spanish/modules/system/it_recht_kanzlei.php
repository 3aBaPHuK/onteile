<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_API_IT_RECHT_KANZLEI_TEXT_TITLE', 'Actualización automática del bufete de abogados del derecho TI');
define('MODULE_API_IT_RECHT_KANZLEI_TEXT_DESCRIPTION', 'bufete de abogados del derecho TI - Actualización automática para textos jurídicos automáticos<br/><br/><b>IMPORTANTE:</b> Antes de utilizar el módulo se debe realizar la asignación de las páginas de contenido.<hr noshade>');
define('MODULE_API_IT_RECHT_KANZLEI_STATUS_TITLE', 'Estado');
define('MODULE_API_IT_RECHT_KANZLEI_STATUS_DESC', 'Estado del módulo');
define('MODULE_API_IT_RECHT_KANZLEI_TOKEN_TITLE', 'Token de autentificación');
define('MODULE_API_IT_RECHT_KANZLEI_TOKEN_DESC', 'El token de autentificación que usted proporciona al bufete de abogados del derecho TI.');
define('MODULE_API_IT_RECHT_KANZLEI_VERSION_TITLE', 'Versión API');
define('MODULE_API_IT_RECHT_KANZLEI_VERSION_DESC', 'Esta sólo se debe modificar si así lo solicita el bufete de abogados del derecho TI. (valor predeterminado: 1,0)');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_AGB_TITLE', '<hr noshade>Texto legal GTC');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_AGB_DESC', 'Indíquese en qué página debe insertarse automáticamente este texto jurídico.');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_DSE_TITLE', 'Texto jurídico sobre protección de datos');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_DSE_DESC', 'Indíquese en qué página debe insertarse automáticamente este texto jurídico.');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_WRB_TITLE', 'Texto jurídico sobre revocación');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_WRB_DESC', 'Indíquese en qué página debe insertarse automáticamente este texto jurídico.');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_IMP_TITLE', 'Texto jurídico sobre Impressum');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_IMP_DESC', 'Indíquese en qué página debe insertarse automáticamente este texto jurídico.');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_AGB_TITLE', '<hr noshade>Selección del texto jurídico de términos y condiciones generales en PDF');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_AGB_DESC', 'Indicación de si términos y condiciones generales deben estar disponible en formato PDF.');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_DSE_TITLE', 'Selección Protección de datos Texto jurídico en PDF');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_DSE_DESC', 'Indicación si el texto de protección de datos debe estar disponible en formato PDF.');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_WRB_TITLE', 'ón del texto jurídico de la revocación en PDF');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_WRB_DESC', 'Indicación si el texto de revocación debe estar disponible en formato PDF.');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_FILE_TITLE', '<hr noshade>Lugar de almacenamiento PDF');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_FILE_DESC', 'Indicación del lugar de almacenamiento de los textos jurídicos en PDF.');

?>

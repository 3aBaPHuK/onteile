<?php
/** 
 * -----------------------------------------------------------------------------------------
 * PDFBill NEXT by Robert Hoppe 
 * http://www.katado.com - xtcm@katado.com
 * Please visit http://pdfnext.katado.com for newer Versions
 * -----------------------------------------------------------------------------------------
 *  
 * Released under the GNU General Public License 
 * Initial Version By Leonid Lezner. leolezner@yahoo.de 
 */

// get fpdf_protection
require_once (DIR_FS_CATALOG . 'includes/classes/FPDF/fpdf_protection.php');

class PdfBrief extends FPDF_Protection
{
    // margins
	var $left_margin = 25;
	var $left_textoffset = 20;
	var $top_margin = 10;
	var $footer_y = -45; //-35; noRiddle

	// Addressfield for the letter window
	var $addresswindowmaxlen = 80;
	var $addresswindowtop = 40; //50; /*noRiddle*/

	// Font-Type of Bill
	var $fontfamily = 'Arial';

	// Shop-Logo Please adjust if needed
	//BOC adapted logo position, noRiddle
    //var $logo_x = 145;
	//var $logo_y = 30;
    var $logo_x = 20;
	var $logo_y = 10;
    //EOC adapted logo position, noRiddle

    var $pm; //new for payment method, 04-2021, noRiddle
    
    function __construct($pm) {
        parent::__construct();

        $this->pm = $pm; //new for payment method, 04-2021, noRiddle
    } //new __construct(), 04-2021, noRiddle

    /*//BOC new abstract function to get a param from child class (for payment method), 04-2021, noRiddle
    protected function additional_params() {return $this->pm;}
    protected function gimme_paym_meth() {
        return $this->additional_params();
    }
    //EOC new abstract function to get a param from child class (for payment method), 04-2021, noRiddle*/
	
    /**
     * Generate Footer
     * 
     * @access public
     * @return void
     */
	//function Footer()
    function Footer()
	{
		//$pm = $this->gimme_paym_meth(); //added parameter for payment method (see xtc_pdf_bill.inc), noRiddle

        //$this->SetFont($this->fontfamily,'',8);
        $this->SetFont($this->fontfamily,'',7); //want a bit smaller letters here, noRiddle
        
        // add mid line
		$this->Line(5, 297.0/2.0, 8, 297.0/2.0);
    
        // add info boxes
		$left = $this->PutInfoBlock(0, xtc_utf8_decode(TEXT_PDF_ANSCHRIFT), $this->footer_y); //added xtc_utf8_decode, noRiddle
		$left = $this->PutInfoBlock($left, xtc_utf8_decode(TEXT_PDF_KONTAKT), $this->footer_y); //added xtc_utf8_decode, noRiddle
		//BOC display different bank data depending on payment method (defined in xtc_pdf_bill.inc), noRiddle
        //$left = $this->PutInfoBlock($left, xtc_utf8_decode(TEXT_PDF_BANK), $this->footer_y); //added xtc_utf8_decode, noRiddle
        if($this->pm == 'pui' || $this->pm == 'ppi') {
            $left = $this->PutInfoBlock($left, xtc_utf8_decode(TEXT_PDF_PAYPAL), $this->footer_y);
        } else if($this->pm == 'ppc' || $this->pm == 'some') { //added 09-12-2019 (use: see /inc/xtc_pdf_bill.inc.php function xtc_pdf_bill()), noRiddle
            $left = $this->PutInfoBlock($left, '', $this->footer_y);
        } else if($this->pm == 'giro') {
            $left = $this->PutInfoBlock($left, xtc_utf8_decode(TEXT_PDF_BANK), $this->footer_y);
        }
        //EOC display different bank data depending on payment method (defined in xtc_pdf_bill.inc), noRiddle
		$left = $this->PutInfoBlock($left, xtc_utf8_decode(TEXT_PDF_GESCHAEFT), $this->footer_y); //added xtc_utf8_decode, noRiddle

        // add bottom line
        //$this->SetLineWidth(0.1);
        $this->SetLineWidth(0.6); //we want it a bit thicker and coloured
        $this->SetDrawColor(83,114,166); //set color, noRiddle
        //$this->Line(20, $this->GetY() - 15, 195, $this->GetY() - 15);
        $this->Line(20, $this->GetY() - 5, 195, $this->GetY() - 5); /*noRiddle*/
        $this->SetDrawColor(0,0,0); //reset color, noRiddle
        
		$this->SetY(290);
		$this->SetX(0);
        $this->Cell(0, 4, xtc_utf8_decode(TEXT_PDF_SEITE).' '.$this->PageNo().' '.xtc_utf8_decode(TEXT_PDF_SEITE_VON).' {nb}', 0, 0, 'C');
	}
	
    /**
     * Generate PDF-Title
     * 
     * @access public
     * @param string $title
     * @return void
     */
	function Init($title)
	{
		$this->SetAutoPageBreak(true, abs($this->footer_y) + 10);
		$this->SetCreator("PdfBrief, PDF-RechnungNEXT (c) 2011 Robert Hoppe www.katado.com");	
		$this->AliasNbPages();
		$this->AddPage();
		$this->SetFillColor(230,230,230);
		//$this->SetDisplayMode('fullwidth');
        $this->SetDisplayMode('real'); /*noRiddle*/
		$this->SetTitle($title);
        //BOC don't need this, doesn't work in some browsers anyway (see http://www.fpdf.org/en/script/script37.php), noRiddle
		//$this->SetProtection(array('print'),'', PDF_MASTER_PASS); // nur Drucken erlaubt, kein User-Passwort, jedoch ein Master-Passwort
        //EOC don't need this, doesn't work in some browsers anyway (see http://www.fpdf.org/en/script/script37.php), noRiddle
	}
	
    /**
     * Build Header
     * 
     * @access public
     * @return void
     */
	function Header()
	{
		$this->SetFont($this->fontfamily,'',9);
		
		$this->SetX($this->left_margin);
		$this->SetLeftMargin($this->left_margin);
		
		// Ausgabe des Headertextes
		$this->Cell(0, 4, TEXT_PDF_HEADER, 0, 1);
		// Schiebe die Zeile auf die Anfangsposition
		$this->SetY($this->top_margin);
		// Ausgabe der Seitenzahl
		//$this->Cell(0, 4, TEXT_PDF_SEITE.' '.$this->PageNo().' '.TEXT_PDF_SEITE_VON.' {nb}', 0, 0, 'R'); //display pages in footer, noRiddle

		// F�r die nachfolgenden Seiten der Abstand vom Header
		$this->Ln();
		$this->Ln();
	}
	
    /**
     * Maxlegth
     *
     * @access public
     * @param string array $strings
     * @return void 
     */
	function maxlen($strings)
	{
		$max = 0;
		for ($i = 0; $i < count($strings); $i++) {
			if($this->GetStringWidth($strings[$i]) > $max) {
				$max = $this->GetStringWidth($strings[$i]);
            } 
		}
		
		return $max + 6;
	}

    /**
     * Info Block
     *
     * @access public
     * @param int $left
     * @param string $body
     * @param int $y
     * @return void
     */
	function PutInfoBlock($left, $body, $y)
	{
		$this->SetFont($this->fontfamily,'',8);
		
		$this->SetY($y);
		
		//$body .= "\n";
		//$body_arr = preg_split("/\n/", $body);
        $body_arr = explode("\n", $body); //explode is sufficient here, noRiddle
		$maxlen = $this->maxlen($body_arr);
		
		$this->SetLeftMargin($left + $this->left_margin);
		$this->SetX($left + $this->left_margin);
		
		//$this->MultiCell($maxlen, 4, $body, 'T');
		$this->MultiCell($maxlen, 4, $body, 0);
		
		return $left + $maxlen;
	}
	

    /**
     * Address Block
     * 
     * @access public
     * @param string $kundenadresse
     * @param string $geschaeftsadresse
     * @return void
     */
	function Adresse($kundenadresse, $geschaeftsadresse)
	{
		// Kundenadresse ausgeben
		$this->SetX($this->left_textoffset + 5);
		//$this->SetLeftMargin($this->left_textoffset + 5);
        $this->SetLeftMargin($this->left_textoffset); //why + 5 ?, noRiddle
		
		$this->SetY($this->addresswindowtop + 7);
		
		$this->SetFont($this->fontfamily,'',12); // normal
		$this->MultiCell($this->addresswindowmaxlen - 5, 4, xtc_utf8_decode($kundenadresse)); //added xtc_utf8_decode, noRiddle
		
		// Shopadresse ausgeben
		$this->SetX($this->left_textoffset);
		$this->SetLeftMargin($this->left_textoffset);
		
		$this->SetY($this->addresswindowtop);
		
		$this->SetFont($this->fontfamily,'',7); // klein //increase font size a bit, was very small, noRiddle (was $this->SetFont($this->fontfamily,'',6);)
		$this->Cell($this->addresswindowmaxlen, 4, xtc_utf8_decode($geschaeftsadresse)); //added xtc_utf8_decode, noRiddle
		
		//$this->Rect($this->left_textoffset, $this->addresswindowtop, $this->addresswindowmaxlen, 40);
	}
	
	function Logo($pfad)
	{
        $size = Getimagesize($pfad);
        //$this->Image($pfad, $this->logo_x, $this->logo_y, $size[0] / 300 * 25.4);
        $this->Image($pfad, $this->logo_x, $this->logo_y, $size[0] / 5); //addapted to old logo size (in shop 1.06), noRiddle
	}
}

?>

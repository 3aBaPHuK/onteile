<?php
/*********************************************************************
* file: generate_shoppingcart_link.php for use in backend of shop
* path: /lang/german/admin/
* use: language file for module
* 
* (c) copyright 10-2020, noRiddle
*********************************************************************/

define('GSCL_NO_MODEL_NO_TXT', 'Sie haben entweder keine Artikelnummer eingegeben oder die Artikelnummer existiert nicht im Shop.');
define('GSCL_ONE_MODEL_NO_EXISTS_TXT', 'Mindestens eine der eingegebenen Artikelnummern existiert nicht im Shop.');
define('GSCL_STATUS_WRONG_TXT', 'Artikel %s haben entweder nicht product_status 1 (=> deaktiviert) oder nicht gm_price_status 0 (=> Preisanfrage oder nicht lieferbar). Bitte im Shop prüfen.');
define('GSCL_EXPLANATION_TXT', '<h1>Generieren Sie einen Warenkorb-Link</h1>
                                <ul>
                                    <li>Artikelnummer(n) eingeben und gegebenenfalls die Stückzahl anpassen und "Den Link generieren" klicken
                                        <ul>
                                            <li>Wenn mehrere Artikelnummern benötigt werden:<br />weiteres Eingabe-Feld mit Button "Weiteres Eingabe-Feld" generieren</li>
                                            <li>Wenn weniger Artikelnummern benötigt werden als Eingabefelder vorhanden sind:<br />nicht benötigte Felder mit Button "Weniger Eingabe-Felder" entfernen</li>
                                         </ul>
                                    </li>
                                    <li>Um Artikelnummer live zur prüfen irgendwo ins Weiße klicken</li>
                                    <li>Link kopieren und an Kunden per Mail senden</li>
                                </ul>');
define('GSCL_MODEL_NO_TXT', 'Artikelnummer:');
define('GSCL_PCS_TXT', '- | - Stück:');
define('GSCL_MORE_INPUT_TXT', '+ Weiteres Eingabe-Feld');
define('GSCL_LESS_INPUT_TXT', '- Weniger Eingabe-Felder');
define('GSCL_GENERATE_LINK_TXT', '&raquo; Den Link generieren');
define('GSCL_DELETE_ALL_ENTRIES_TXT', 'x Alle Eingaben löschen');
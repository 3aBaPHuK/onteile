<?php
/**************************************************************************************************************
* file: freeboxes.php
* use: fill free boxes with content manager contents with content_group between certain values (see config)
*      if sort_order >= certain value (see config) its a footer box, else its a left or right column box
*      sort_order of content will be respected when displaying boxes
*      content_heading will be box header
*      box will only be displayed when content is active
* (c) noRiddle 08-2016
**************************************************************************************************************/

//BOC config
$cg_b = 200; //begin content groups for boxes
$cg_e = 299; //end content groups for boxes
$sf_b = 100; //if sort_order >= this value, box will be displayed in footer, else in left or right column
//EOC config

// include smarty
include(DIR_FS_BOXES_INC . 'smarty_default.php');

// reset cache id
$cache_id = '';

$how_many_free_arr = array();
$box_free_array = array();
$how_many_free_qu = xtDBquery("SELECT content_group, 
                                         sort_order 
                                    FROM ".TABLE_CONTENT_MANAGER." 
                                   WHERE content_group BETWEEN ".$cg_b." AND ".$cg_e." 
                                     AND content_active = '1' 
                                     AND languages_id = '".(int)$_SESSION['languages_id']."' 
                                ORDER BY sort_order");

if(xtc_db_num_rows($how_many_free_qu, true) > 0) {
    while($how_many_free = xtc_db_fetch_array($how_many_free_qu)) {
        $how_many_free_arr[$how_many_free['content_group']] = $how_many_free['sort_order'];
    }
    //echo '<pre>'.print_r($how_many_free_arr, true).'</pre>';

    foreach($how_many_free_arr as $no => $sort) {
        // set cache id
        $cache_id = md5($_SESSION['language'].$_SESSION['customers_status']['customers_status_id'].$sort.$no);
         
        if($sort < $sf_b) {
            if (!$box_smarty->is_cached(CURRENT_TEMPLATE.'/boxes/box_free_col.html', $cache_id) || !$cache) {
                $content_data =  $main->getContentData($no, $_SESSION['languages_id'], $_SESSION['customers_status']['customers_status_id']);
                //echo '<pre>'.print_r($content_data, true).'</pre>';
            }
        } else {
            if (!$box_smarty->is_cached(CURRENT_TEMPLATE.'/boxes/box_free_foot.html', $cache_id) || !$cache) {
                $content_data =  $main->getContentData($no, $_SESSION['languages_id'], $_SESSION['customers_status']['customers_status_id']);
                //echo '<pre>'.print_r($content_data, true).'</pre>';
            }
        }
        
        if (!empty($content_data)) {
            $content_string = $content_data['content_text'];
            $content_header = $content_data['content_heading'];
            
            $box_smarty->assign('BOX_CONTENT', $content_string);
            $box_smarty->assign('BOX_HEADER', $content_header);
            $box_smarty->assign('BOX_NO', 'free-bx-'.$no);
        }

        if($sort < $sf_b) {
            if(!$cache) {
                ${'box_FREE_COL_'.$no} = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_free_col.html');
            } else {
                ${'box_FREE_COL_'.$no} = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_free_col.html', $cache_id);
            }
            $box_free_array[] = array('box_col' => ${'box_FREE_COL_'.$no});
        } else {
            if(!$cache) {
                ${'box_FREE_FOOT_'.$no} = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_free_foot.html');
            } else {
                ${'box_FREE_FOOT_'.$no} = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_free_foot.html', $cache_id);
            }
            $box_free_array[] = array('box_foot' => ${'box_FREE_FOOT_'.$no});
        }
    }
    //echo '<pre>'.print_r($box_free_array, true).'</pre>';
}

if(!empty($box_free_array)) {   
    $smarty->assign('box_FREE', $box_free_array);
}
?>
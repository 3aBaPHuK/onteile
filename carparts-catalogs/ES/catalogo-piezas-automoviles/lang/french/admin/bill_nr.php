<?php
/** 
 * -----------------------------------------------------------------------------------------
 * PDFBill NEXT by Robert Hoppe
 * Copyright 2011 Robert Hoppe - xtcm@katado.com - http://www.katado.com
 *
 * Please visit http://pdfnext.katado.com for newer Versions
 * -----------------------------------------------------------------------------------------
 *  
 * Released under the GNU General Public License 
 * 
 */
define('PDF_BILL_NR_TITLE', 'Attribuer un numéro de facturation');
define('PDF_BILL_NR_HEAD', 'Numéro de facturation généré automotiquement:');
define('PDF_BILL_NR_GIVEN', '<h3>Numéro de facturation déjà attribué!</h3><br />Le-voici encore une fois: ');
define('PDF_BILL_NR_INFO', 'Veuillez vérifier éventuellement!');
define('PDF_BILL_NR_SUBMIT', 'Confirmer');
define('PDF_CLOSE_WINDOW', 'Fermer la session');
define('PDF_CANCEL', 'Annuler et fermer la session');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_payment.php 3481 2012-08-22 07:07:50Z dokuman $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (C) 2007 Estelco - Ebusiness & more - http://www.estelco.de
   (C) 2004 IT eSolutions Andreas Zimmermann - http://www.it-esolutions.de

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_PAYMENT_HELP_LINK', ' <a onclick="window.open(\'popup_help.php?type=order_total&modul=ot_payment&lng=spanish\', \'Ayuda\', \'scrollbars=yes,resizable=yes,menubar=yes,width=800,height=600\'); return false" target="_blank" href="popup_help.php?type=order_total&modul=ot_payment&lng=spanish"><b>[AYUDA]</b></a>');
define('MODULE_ORDER_TOTAL_PAYMENT_HELP_TEXT', '<h2>descuento y recargo por modalidades de pago</h2>
Si deben ser posibles más escalas de descuento (por defecto son <b>3</b>), el valor de la variable $num (archivos de idioma) o $this->num (archivo de módulo) debe ser cambiado al valor deseado en todos los archivos antes de la instalación.
<hr>
<h3>Campos de escala de descuento</h3>
<p class="red">Aviso: Para los códigos de país es decisiva la dirección de entrega. Si un descuento/recargo debe ser válido para todos los países, utilice 00 como código u omita completamente la indicación del código de país (incluido el "|")!</p>
<h4>Utilizar la siguiente notación para los descuentos:</h4>
<pre>   <span class="blue">DE</span>|<span class="green">100</span>:<span class="red">4</span>,<span class="green">200</span>:<span class="red">5</span></pre>
<p>Significado:</p>
<p>Para clientes de <span class="blue">Alemania</span> se concede a partir de <span class="green">100€</span> un descuento de <span class="red">4%</span>, a partir de <span class="green">200€</span> un descuento de <span class="red">5%</span>.</p>
<pre>   <span class="green">100</span>:<span class="red">2</span>,<span class="green">200</span>:<span class="red">3</span></pre>
<p>Significado:</p>
<p>FPara clientes de todos los países se concede a partir de <span class="green">100€</span> un descuento de <span class="red">2%</span>, a partir de <span class="green">200€</span> un descuento de <span class="red">3%</span> .</p>
<h4>Utilizar la siguiente notación para los recargos:</h4>
<pre>   <span class="blue">DE</span>|<span class="green">100</span>:<span class="red">-3</span></pre>
<p>Significado:</p>
<p>Para clientes de <span class="blue">Alemania</span> se calcula a partir de <span class="green">100€</span> un recargo de <span class="red">3%</span> .</p>
<h4>Ejemplo para Paypal</h4>
1ª escala de descuento
<pre>   <span class="blue">DE</span>|<span class="green">0</span>:<span class="red">-1.9</span>&<span class="lila">-0.35</span></pre>
2ª escala de descuento
<pre>   <span class="blue">00</span>|<span class="green">0</span>:<span class="red">-3.4</span>&<span class="lila">-0.35</span></pre>
<p>Significado:</p>
<p>Para clientes de <span class="blue">Alemania</span> se calcula a partir de <span class="green">0€</span> (es decir siempre) un recargo de <span class="red">1,9%</span> más <span class="lila">0,35€</span> .</p>
<p>Para clientes de <span class="blue">todos los otros países (00=todos)</span> se calcula a partir de <span class="green">0€</span> un recargo de <span class="red">3,4%</span> más <span class="lila">0,35€</span> .</p>
<p>Aquí es importante el orden de las entradas (todos los demás países son siempre los últimos) y que el "cálculo múltiple" se ajuste a "false", de lo contrario se calculan los dos recargos.</p>
<h4>Ejemplo para importes fijos</h4>
<pre>   <span class="green">0</span>:<span class="red">0</span>&<span class="lila">-2</span></pre>
<p>Significado</p>
<p>Para clientes de todos los países se calcula a partir de <span class="green">0€</span> (es decir siempre) un recargo de <span class="red">0%</span> (es decir ningún recargo porcentual) más <span class="lila">2,00€</span> (del recargo fijo).</p>
<hr>
<h3>Campos Modalidad de pago</h3>
<p>En los campos se introduce el <b>código interno</b> de la modalidad de pago, p.ej. <b>moneyorder</b> para el pago por adelantado o <b>cod</b> para el pago contra reembolso. Separar varios modalidades de pago con comma</p>Véase también Módulos -> Opciones de pago -> Columna "Nombre del módulo (para uso interno)".<br/><br/>
<hr>
<h3>Mostrar con la modalidad de pago en el proceso de pedido</h3>
Si ya en el proceso de pedido se debe indicar el descuento correspondiente con la selección de pago, ajuste la opción "Mostrar con las modalidades de pago" a "true". <br/><br/>
También puede ajustar el tipo de visualización mediante la opción "Tipo de visualización en el proceso de pedido al seleccionar el pago:
<p> -- default: Porcentaje o importe, dependiendo de las entradas para la escala de descuento</p>
<p> -- price: Siempre se muestra el importe real.</p>'
);

define('MODULE_ORDER_TOTAL_PAYMENT_TITLE', 'Descuento & recargo en las modalidades de pago');
define('MODULE_ORDER_TOTAL_PAYMENT_DESCRIPTION', 'Descuento y recargo en las modalidades de pago'.MODULE_ORDER_TOTAL_PAYMENT_HELP_LINK);

define('MODULE_ORDER_TOTAL_PAYMENT_STATUS_TITLE', 'Mostrar descuento');
define('MODULE_ORDER_TOTAL_PAYMENT_STATUS_DESC', '¿Quiere activar el descuento por modalidad de pago?');

define('MODULE_ORDER_TOTAL_PAYMENT_SORT_ORDER_TITLE', '<hr>Orden de clasificación');
define('MODULE_ORDER_TOTAL_PAYMENT_SORT_ORDER_DESC', 'Orden de visualización');

for ($j=1; $j<=MODULE_ORDER_TOTAL_PAYMENT_NUMBER; $j++) {
define('MODULE_ORDER_TOTAL_PAYMENT_PERCENTAGE' . $j . '_TITLE', '<hr>'.$j . '. Escala de descuento');
define('MODULE_ORDER_TOTAL_PAYMENT_PERCENTAGE' . $j . '_DESC', 'Descuento (valor mínimo:porcentaje');
define('MODULE_ORDER_TOTAL_PAYMENT_TYPE' . $j . '_TITLE', $j . '. modalidad de pago');
define('MODULE_ORDER_TOTAL_PAYMENT_TYPE' . $j . '_DESC', 'Modalidades de pago a las que se debe aplicar el descuento');
}

define('MODULE_ORDER_TOTAL_PAYMENT_INC_SHIPPING_TITLE', '<hr>incluido costos de envío');
define('MODULE_ORDER_TOTAL_PAYMENT_INC_SHIPPING_DESC', 'Los costos de envío se descuentan.');

define('MODULE_ORDER_TOTAL_PAYMENT_INC_TAX_TITLE', '<hr>incluido IVA');
define('MODULE_ORDER_TOTAL_PAYMENT_INC_TAX_DESC', 'IVA se descuenta.');

define('MODULE_ORDER_TOTAL_PAYMENT_CALC_TAX_TITLE', '<hr>cálculo del IVA');
define('MODULE_ORDER_TOTAL_PAYMENT_CALC_TAX_DESC', 'Calcular de nuevo el total del IVA');

define('MODULE_ORDER_TOTAL_PAYMENT_ALLOWED_TITLE', '<hr>Zonas permitidas');
define('MODULE_ORDER_TOTAL_PAYMENT_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas que deben permitirse para este módulo. (p.ej. AT,DE (si está vacío, se permiten todas las zonas))');

define('MODULE_ORDER_TOTAL_PAYMENT_DISCOUNT', 'Descuento');
define('MODULE_ORDER_TOTAL_PAYMENT_FEE', 'Recargo');

define('MODULE_ORDER_TOTAL_PAYMENT_TAX_CLASS_TITLE', '<hr>SClase de impuestos');
define('MODULE_ORDER_TOTAL_PAYMENT_TAX_CLASS_DESC', 'La clase de impuesto es irrelevante y sólo sirve para evitar un mensaje de error.');

define('MODULE_ORDER_TOTAL_PAYMENT_BREAK_TITLE', '<hr>Cálculo múltiple');
define('MODULE_ORDER_TOTAL_PAYMENT_BREAK_DESC', '¿Deberían ser posibles múltiples cálculos? Si no, será cancelado después del primer descuento adecuado.');

define('MODULE_ORDER_TOTAL_PAYMENT_SHOW_IN_CHECKOUT_PAYMENT_TITLE', '<hr>Visualización en las modalidades de pago');
define('MODULE_ORDER_TOTAL_PAYMENT_SHOW_IN_CHECKOUT_PAYMENT_DESC', 'Mostrar en el proceso de pedido durante la selección de pagos');

define('MODULE_ORDER_TOTAL_PAYMENT_SHOW_TYPE_TITLE', '<hr>Tipo de visualización para las modalidades de pago');
define('MODULE_ORDER_TOTAL_PAYMENT_SHOW_TYPE_DESC', 'Tipo de visualización en el proceso de pedido para la selección de pagos <br />-- default: Porcentaje o importe, dependiendo de las entradas para la escala de descuento<br />-- price: siempre se muestra el importe real');

define('MODULE_ORDER_TOTAL_PAYMENT_NUMBER_TITLE', 'Número de modalidades de pago');
define('MODULE_ORDER_TOTAL_PAYMENT_NUMBER_DESC', 'Número de descuentos & recargos sobre las modalidades de pago');
?>

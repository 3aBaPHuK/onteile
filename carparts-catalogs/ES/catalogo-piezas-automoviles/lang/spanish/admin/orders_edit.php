<?php
/* --------------------------------------------------------------
   $Id: orders_edit.php,v 1.0 

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(orders.php,v 1.27 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (orders.php,v 1.7 2003/08/14); www.nextcommerce.org
   (c) 2006 XT-Commerce (orders_edit.php)

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

// Allgemeine Texte
define('TABLE_HEADING', 'Editar datos del pedido');
define('TABLE_HEADING_ORDER', 'Número del pedido');
define('TEXT_SAVE_ORDER', 'Finalizar el procesamiento de pedidos y recalcular el pedido.');

define('TEXT_EDIT_ADDRESS', 'Dirección y datos de clientes');
define('TEXT_EDIT_PRODUCTS', 'Artículos, opciones de artículos y precios');
define('TEXT_EDIT_OTHER', 'Costos de envío, modalidades de pago, monedas, idiomas, totales, IVA, descuentos, etc.');
define('TEXT_EDIT_GIFT', 'Cupones y descuento');
define('TEXT_EDIT_ADDRESS_SUCCESS', 'Se ha guardado el cambio de dirección.');

define('IMAGE_EDIT_ADDRESS', 'Editar o insertar direcciones');
define('IMAGE_EDIT_PRODUCTS', 'Editar o insertar artículos y opciones');
define('IMAGE_EDIT_OTHER', 'Editar o insertar costos de envío, pago, cupones, etc.');

// Adressaenderung
define('TEXT_INVOICE_ADDRESS', 'Dirección del cliente');
define('TEXT_SHIPPING_ADDRESS', 'Dirección del envío');
define('TEXT_BILLING_ADDRESS', 'Dirección de la factura');

define('TEXT_COMPANY', 'Empresa:');
define('TEXT_NAME', 'Nombre:');
define('TEXT_STREET', 'Calle');
define('TEXT_ZIP', 'Código postal:');
define('TEXT_CITY', 'Ciudad:');
define('TEXT_COUNTRY', 'País:');
define('TEXT_CUSTOMER_GROUP', 'Grupo de clientes en el pedido');
define('TEXT_CUSTOMER_EMAIL', 'Correo electrónico:');
define('TEXT_CUSTOMER_TELEPHONE', 'Teléfono:');
define('TEXT_CUSTOMER_UST', 'Número de IVA:');
define('TEXT_CUSTOMER_CID', 'Número del cliente:');
define('TEXT_ORDERS_ADDRESS_EDIT_INFO', '¡Tenga en cuenta que los datos introducidos aquí sólo se modificarán en el pedido y no en la cuenta de cliente!');

// Artikelbearbeitung

define('TEXT_SMALL_NETTO', '(Neto)');
define('TEXT_PRODUCT_ID', 'pID:');
define('TEXT_PRODUCTS_MODEL', 'Número del artículo');
define('TEXT_QUANTITY', 'Cantidad:');
define('TEXT_PRODUCT', 'Artículo:');
define('TEXT_TAX', 'IVA:');
define('TEXT_PRICE', 'Precio:');
define('TEXT_FINAL', 'Total:');
define('TEXT_PRODUCT_SEARCH', 'Búsqueda de artículo:');

define('TEXT_PRODUCT_OPTION', 'Características del artículo:');
define('TEXT_PRODUCT_OPTION_VALUE', 'Valor de la opción:');
define('TEXT_PRICE_PREFIX', 'Price Prefix:');
define('TEXT_SAVE_ORDER', 'Completar el pedido y volver a calcular');
define('TEXT_INS', 'agregar:');
define('TEXT_SHIPPING', 'Módulo de costos de envío');
define('TEXT_COD_COSTS', 'Costos de envío contra reembolso');
define('TEXT_VALUE', 'Precio');
define('TEXT_DESC', 'insertar');

// Sonstiges

define('TEXT_PAYMENT', 'Modalidad de pago');
define('TEXT_SHIPPING', 'Modo de envío');
define('TEXT_LANGUAGE', 'Idioma:');
define('TEXT_CURRENCIES', 'Monedas:');
define('TEXT_ORDER_TOTAL', 'Resumen:');
define('TEXT_SAVE', 'Guardar');
define('TEXT_ACTUAL', 'Actual: ');
define('TEXT_NEW', 'Nuevo: ');
define('TEXT_PRICE', 'Costos: ');

define('TEXT_ADD_TAX', 'incl. ');
define('TEXT_NO_TAX', 'plus');

define('TEXT_ORDERS_EDIT_INFO', '<b>Avisos importantes:</b><br />
Por favor, seleccione el grupo de clientes correcto en dirección/datos del cliente.<br />
¡Si el grupo de clientes cambia, se deben guardar de nuevo todas las partidas individuales de la factura!<br />
¡Los costos de envío deben ser cambiados manualmente!<br />
¡Dependiendo del grupo de clientes, los costos de envío deben ser introducidos en bruto o en neto!<br />
');

define('TEXT_CUSTOMER_GROUP_INFO', ' ¡Si el grupo de clientes cambia, se deben guardar de nuevo todas las partidas individuales de la factura!');

define('TEXT_ORDER_TITLE', 'Título:');
define('TEXT_ORDER_VALUE', 'Valor:');
define('ERROR_INPUT_TITLE', 'Sin entrada para el título');
define('ERROR_INPUT_EMPTY', 'Sin entrada para el título y el valor');
define('ERROR_INPUT_SHIPPING_TITLE', '¡Todavía no se ha seleccionado ningún módulo de envío!');

// note for graduated prices
define('TEXT_ORDERS_PRODUCT_EDIT_INFO', '<b>Aviso:</b> ¡Para los precios de escala, el precio unitario debe ajustarse manualmente!');

define('TEXT_FIRSTNAME', 'Nombre:');
define('TEXT_LASTNAME', 'Apellido:');

define('TEXT_GENDER', 'Saludo:');
define('TEXT_MR', 'Señor');
define('TEXT_MRS', 'Señora');

define('TEXT_SAVE_CUSTOMERS_DATA', 'Guardar datos del cliente');

define('TEXT_PRODUCTS_SEARCH_INFO', 'Nombre o número de artículo o GTIN/EAN');
define('TEXT_PRODUCTS_STATUS', 'Estado:');
define('TEXT_PRODUCTS_IMAGE', 'Imagen del artículo:');
define('TEXT_PRODUCTS_QTY', 'Stock:');
define('TEXT_PRODUCTS_EAN', 'GTIN/EAN:');
define('TEXT_PRODUCTS_TAX_RATE', 'Tasa de impuesto:');
define('TEXT_PRODUCTS_DATE_AVAILABLE', 'Fecha de publicación:');
define('TEXT_IMAGE_NONEXISTENT', '---');
?>

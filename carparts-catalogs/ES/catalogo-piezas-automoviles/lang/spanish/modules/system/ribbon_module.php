<?php
/*******************************************************
* file: ribbon_module.php
* use: language file for system module
* (c) noRiddle 07-2017
* Released under the GNU General Public License
*******************************************************/

define('MODULE_RIBBON_MODULE_TEXT_TITLE', 'Módulo Ribbon');
define('MODULE_RIBBON_MODULE_TEXT_DESCRIPTION', 'Proporciona la extensión que permite mostrar una banda con signos de porcentaje en las ofertas especiales.');
define('MODULE_RIBBON_MODULE_STATUS_TITLE', '¿Activar módulo?');
define('MODULE_RIBBON_MODULE_STATUS_DESC', 'Activar el módulo Ribbon');
?>

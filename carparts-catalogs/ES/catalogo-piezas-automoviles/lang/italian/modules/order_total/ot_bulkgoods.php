<?php
/***************************************
* file: ot_bulkgoods
* use: language file for above ot module
* (c) noRiddle 05-2017
***************************************/

define('MODULE_ORDER_TOTAL_BULKGOODS_HELP_LINK', ' <a onclick="window.open(\'popup_help.php?type=order_total&modul=ot_bulkgoods&lng=english\', \'Hilfe\', \'scrollbars=yes,resizable=yes,menubar=yes,width=800,height=600\'); return false" target="_blank" href="popup_help.php?type=order_total&modul=ot_sperrgut&lng=english"><b>[HILFE]</b></a>');
define('MODULE_ORDER_TOTAL_BULKGOODS_HELP_TEXT', '<h2>configurable bulk goods module</h2>
<p>Actually this module should be self-explanatory.<br />However, folowing a short instruction.<p>
<ul>
<li>Install module.</li>
<li>Please observe the sorting ! If "<i>ot_cod_fee</i>" is also installed, which per default has got the sorting "35", the "<i>ot_sperrgut</i>" should have the sorting "34".</li>
<li>Determine the calculation mode per Dropdown.<br />Im Warenkorb werden<br />- insofern nicht "nur einmal die h&ouml;chsten Sperrgutkosten je Warenkorb" ausgew&auml;hlt wurde -<br /> die Sperrgutkosten bei jedem Artikel einzeln angezeigt<br />und au&szlig;erdem werden die Gesamt-Sperrgutkosten in der Zusamenfassung angezeigt.<br />In der Produkt-Einzelansicht werden die Sperrgutkosten unter dem Versandkostenlink angezeigt</li>
<li>Angeben ob ein Link zu Informationen über "Sperrgutkosten" auf der checkout_shipping-Seite bei jeder Versandart angezeigt werden soll.<br />"Versandkostenfrei" und "Selbstabholung" werden nicht mit dem Link versehen.<br />Wenn \'Ja\' gewählt wird, nicht vergessen die Informationen über Sperrgutkosten in den Content für Versandkosten mit aufzunehmen.<br />Sowohl auf der Produkt-Einzelansicht als auch auf der ccheckout_shipping-Seite (wenn aktiviert) wird ein Link zu dem Content angezeigt.</li>
<li>Auf der checkout_confirmation-Seite, in der Bestellung im Backend sowie in der Bestellbest&auml;tigung werden die Sperrgutkosten automatisch aufgef&uuml;hrt.</li>
</ul><br /><p><a href="http://www.revilonetz.de/kontakt" target="_blank">noRiddle</a> w&uuml;nscht viel Freude und Erfolg mit dem Modul</p>');

define('MODULE_ORDER_TOTAL_BULKGOODS_TITLE', 'Bulk goods costs'.MODULE_ORDER_TOTAL_BULKGOODS_HELP_LINK);
define('MODULE_ORDER_TOTAL_BULKGOODS_DESCRIPTION', 'Bulk goods costs of an order');
  
define('MODULE_ORDER_TOTAL_BULKGOODS_CC_TITLE', 'Bulk goods costs');
 
define('MODULE_ORDER_TOTAL_BULKGOODS_STATUS_TITLE','Bulk goods costs active');
define('MODULE_ORDER_TOTAL_BULKGOODS_STATUS_DESC','Activate bulk godds costs?');
  
define('MODULE_ORDER_TOTAL_BULKGOODS_SORT_ORDER_TITLE','Sort sequenceSortierreihenfolge');
define('MODULE_ORDER_TOTAL_BULKGOODS_SORT_ORDER_DESC', 'Order of display.');

define('MODULE_ORDER_TOTAL_BULKGOODS_COSTS_TITLE','Costs per bulk good');
define('MODULE_ORDER_TOTAL_BULKGOODS_COSTS_DESC','');

define('MODULE_ORDER_TOTAL_BULKGOODS_METHOD_TITLE','How shall the bulk goods costs be calculated ?');
define('MODULE_ORDER_TOTAL_BULKGOODS_METHOD_DESC','<ul style="list-style-type:decimal;"><li>add up all bulk goods costs = the costs will be multiplied by the quantity of the product</li><li>bulk goods costs only once per product = the costs will be charged only once per product independent of the quantity of the product</li><li>only the highest bulk goods costs once per cart = goes without saying</li></ul>');

define('MODULE_ORDER_TOTAL_BULKGOODS_SHOW_IN_CHECKOUT_SHIPPING_TITLE','Auf checkout_shipping anzeigen');
define('MODULE_ORDER_TOTAL_BULKGOODS_SHOW_IN_CHECKOUT_SHIPPING_DESC','<ul><li>Bei \'JA\' wird ein Link zu den Sperrgutkosten auf der checkout_shipping-Seite in den zur Verf&uuml;gung stehenden Versandarten angezeigt.</li><li>Nicht vergessen dafür die Informationen über Sperrgutkosten in den Content für Versandkosten mit aufzunehmen.</li></ul>');
  
//BOC dropdown array bulgoods module
define('NR_ADD_UP_ALL_BULKGOODS_COSTS', 'add up all bulk goods costs');
define('NR_ADD_UP_ONE_BULKGOODS_COST_PER_ARTICLE', 'bulk goods costs only once per product');
define('NR_ONLY_HIGHEST_BULKGOODS_COST_PER_SHOPPING_CART', 'only the highest bulk goods costs once per cart');
//EOC dropdown array bulkgoods module

define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT_TITLE', 'Automatically read out customer status');
define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT_DESC', 'Don\'t edit!!, is drawn automatically from database.');

define('MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS_TITLE', 'Tax class');
define('MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS_DESC', 'Choose the follwoing tax class for bulk goods costs');

//BOC customer status factors
//if(MODULE_ORDER_TOTAL_BULKGOODS_STATUS == 'true') {
if(strpos(MODULE_ORDER_TOTAL_INSTALLED, 'ot_bulkgoods.php')) {
    $custstat_array = json_decode(MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT, true);
    foreach($custstat_array as $k => $cs_name) {
        define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$k.'_TITLE', 'Surcharge factor for customer group "<i>'.$cs_name.'</i>"');
        define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$k.'_DESC', 'Multiplication factor for customer group '.$k.' (= "<i>'.$cs_name.'</i>")<br />(The bulk goods cost value which is determined with the product will for this customer group be be multiplied with the here indicated value.)');
    }
}
?>
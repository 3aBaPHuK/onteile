<?php
/************************************************************************
* language file for imageslider                                         *
* plugin derived from imageslider by Hetfield                           *
* based on                                                              *
* - responsive kenburner slider by codecanyon, licence necessary        *
*                                                                       *
* copyright (c) for all changes: noRiddle of webdesign-endres.de 2013   *
************************************************************************/

$slider_measures = array();
$slider_flyin_measures = array();
if((isset($data20) && !empty($data20)) && !isset($dataC20) && file_exists('../includes/local/configure.php')) {
    switch($shop) {
        case 'audi'.$shop_suffix:
            $slider_measures[$shop] = array('1420px', '250px');
            $slider_flyin_measures[$shop] = array('478', '146');
        break;
        default:
            $slider_measures[$shop] = array('1420px', '250px');
            $slider_flyin_measures[$shop] = array('478', '146');
        break;
    }
} else if((isset($dataC20) && !empty($dataC20)) || !file_exists('../includes/local/configure.php')) {
    $slider_measures[$shop] = array('1580px', '250px');
    $slider_flyin_measures[$shop] = array('478', '146');
}

define('HEADING_TITLE_II', 'CoKeBuMo-Imageslider ed. 1.5.2');
define('TABLE_HEADING_IMAGESLIDERS', 'Imageslider');
define('TABLE_HEADING_SPEED', 'Velocidad');
define('TABLE_HEADING_SORTING', 'Orden');
define('TABLE_HEADING_STATUS', 'Estado');
define('TABLE_HEADING_ACTION', 'Acción');
define('TEXT_HEADING_NEW_IMAGESLIDER', 'Imagen nueva');
define('TEXT_HEADING_EDIT_IMAGESLIDER', 'Editar imagen');
define('TEXT_HEADING_DELETE_IMAGESLIDER', 'Borrar imagen');
define('TEXT_IMAGESLIDERS', 'Imagen:');
define('TEXT_IMAGE_DIMENSIONS', 'Dimensiones originales de la imagen: ');
define('TEXT_DATE_ADDED', 'agregrado el:');
define('TEXT_LAST_MODIFIED', 'Última modificación el:');
define('TEXT_IMAGE_NONEXISTENT', 'LA IMAGEN NO EXISTE');
define('TEXT_NEW_INTRO', 'Por favor, cree la nueva imagen con todos los datos relevantes.');
define('TEXT_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_IMAGESLIDERS_TITLE', 'Título para la imagen:');
define('TEXT_IMAGESLIDERS_NAME', 'Nombre para la entrada de la imagen:');
define('TEXT_IMAGESLIDERS_IMAGE', 'Imagen:');
define('TEXT_IMAGESLIDERS_URL', 'Enlazar imagen <span style="color:#B30000; font-size:9px;"> (sólo es posible si no se ha implementado ningún vídeo (véase más adelante), en caso de una implementación de vídeo seleccione "Sin enlace")</span>');
define('TEXT_TARGET', 'Ventana destino');
define('TEXT_TYP', 'Tipo de enlace:');
define('TEXT_URL', 'Dirección del enlace:');
define('TEXT_IMAGESLIDERS_ALT', 'alt-tag para imagen (Google ?):');
define('TEXT_IMAGESLIDERS_VIDEO_URL', 'Embed-URL de un vídeo deseado para esta imagen. "YouTube" y "Vimeo" son compatibles.');
define('TEXT_IMAGESLIDERS_VIDEO_DESC', 'Descripción del video:');
define('TEXT_IMAGESLIDERS_CREDITS_DESC', 'Explicaciones y agradecimientos:');
define('TEXT_IMAGESLIDERS_CREDITS_EXPL', '<p>Basado en kenburner by codecanyon, código ampliamente modificado para obtener mejores efectos de texto y otros cambios de <a href="http://www.revilonetz.de" target="_blank">noRiddle</a> ed. 1.5.2 | 03-2013 - '.date('Y').'</p><p>Para preguntas sobre la interfaz o solicitudes especiales de extensiones, por favor contacte <a class="imgslider-link" href="http://www.revilonetz.de/kontakt" target="_blank">noRiddle</a></p>');
define('TXT_BUTTON_COPY_SLIDER', 'Copiar Slider de la tienda %s');
define('TXT_IDS_RANGE', 'Introduzca aquí las identificaciones de la forma X-Y. (Ejemplo:10-15)<br />Si el campo se deja en blanco, se copian todas las imágenes.');
define('TXT_COPY_SLIDER_EXPL', 'Actualmente sólo las imágenes activadas se copian desde el slider de la tienda %1$s .<br />ADVERTENCIA: %1$s puede tener diferentes dimensiones, por favor, compruebe primero.');
define('TXT_BUTTON_NEWIMAGE', 'Insertar nueva imagen');
define('TXT_BUTTON_COPY_IMAGE', 'Copiar Imagen con todos los ajustes');
define('TXT_DEFAULT_COPY_IMAGE', 'Seleccionar imagen');

define('NONE_TARGET', 'No establecer un destino.');
define('TARGET_BLANK', '_blank');
define('TARGET_TOP', '_top');
define('TARGET_SELF', '_self');
define('TARGET_PARENT', '_parent');
define('TYP_NONE', 'Sin enlace');
define('TYP_PRODUCT', 'Enlace al producto (por favor, introduzca sólo el ID del producto en la dirección del enlace).');
define('TYP_CATEGORIE', 'Enlace a la categoría (por favor, introduzca sólo el catID en la dirección del enlace.<span style="color:#c00">!!  Para las subcategorías sólo el último ID sin guión bajo antes !!</span>)');
define('TYP_CONTENT', 'Enlace a la página de contenido (introduzca sólo el coID en la dirección del enlace).');
define('TYP_INTERN', 'Enlace interno de la tienda (p. ej. account.php o newsletter.php)');
define('TYP_EXTERN', 'Enlace externo (p.ej. http://www.externerlink.com)');
define('TEXT_IMAGESLIDERS_DESCRIPTION', 'Descripción de la imagen:');
define('INFO_IMAGESLIDERS_DESCRIPTION', 'Si se insertan imágenes, el ancho máximo es  '.$slider_flyin_measures[$shop][0].', la altura máxima es '.$slider_flyin_measures[$shop][1].' .<br />Por favor, elimine las dimensiones de la imagen en el explorador de archivos.<br />En el menú desplegable "Estilo" se pueden seleccionar varios tamaños de letra.<br />Por favor, no utilice el menú desplegable "Tamaño" para los tamaños de letras.');
define('TEXT_DELETE_INTRO', '¿Está seguro de que quiere borrar esta imagen?');
define('TEXT_DELETE_IMAGE', '¿También borrar la imagen?');
define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Error: El directorio %s está protegido contra escritura. ¡Por favor, corrija los derechos de acceso a este directorio!');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Error: ¡El directorio %s no existe!');
define('TEXT_DISPLAY_NUMBER_OF_IMAGESLIDERS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de unt total de <b>%d</b> Imágenes en la Slideshow)');  //'Cantidad de imágenes en la Slideshow');
define('IMAGE_ICON_STATUS_GREEN', 'Activado');
define('IMAGE_ICON_STATUS_GREEN_LIGHT', 'Activar');
define('IMAGE_ICON_STATUS_RED', 'No activado');
define('IMAGE_ICON_STATUS_RED_LIGHT', 'Desactivar');
define('ACTIVE', 'activado');
define('NOTACTIVE', 'no activado');

define('INFO_IMAGE_NAME', 'Por favor, introduzca aquí un nombre para la imagen con el fin de reconocerla más tarde en caso de editarla por su nombre.');
define('INFO_IMAGE_SORT', 'Introduzca aquí un número entero para determinar el orden en que se muestran las imágenes.');
define('INFO_IMAGE_ACTIVE', 'Aquí puede activar o desactivar la imagen.<br>Las demás imágenes activadas no se afectan.');
define('INFO_IMAGE_UPLOAD', 'El nombre de la imagen a cargar no debe ser demasiado largo (no más de 38 caracteres sin extensión de archivo).<br>Se debe cargar una imagen para cada idioma en el que se vaya a mostrar el slider.<br><span style="color:#c00;">!! Dimensiones para su tienda !!</span><br>ancho de imagen: '.$slider_measures[$shop][0].'<br>altura de imagen: '.$slider_measures[$shop][1]);
define('INFO_IMAGE_TITLE', 'El título de la imagen es necesario para hacer referencia al texto que aparece en la imagen. <br>Por favor, especifique un título corto único.<br>Título sólo puede aparecer una vez, es decir, utilice títulos diferentes para otras imágenes.<br>Lo mejor es usar siempre el mismo título y numéralo.<br>p.ej.:<br>cap1, siguiente imagen cap2 etc.<br /><span style="color:#c00">Si no se especifica ningún texto (descripción de la imagen), no se asigna ningún título aquí !</span>');
define('INFO_IMAGE_ALT', 'Introduzca aquí el texto que aparecerá en la alt-tag de la imagen.<br />Puede ser importante para los resultados de los motores de búsqueda (SERP).');

//***BOC KenBurns Codecanyon***
define('TEXT_IMAGESLIDERS_DATA_ATTRIBUTES', 'Efectos para el Slider <span style="color:#B30000; font-size:9px;">(si se selecciona un valor en una idioma, también se configuran los valores de las otros idiomas)</span>');
define('INFO_IMAGE_DATA_ATTRIBUTES', 'De las opciones mostradas aquí, una debe ser seleccionada cuando se activa el efecto KenBurns.<br>Los campos de texto también deben ser rellenados (ver allí).');
define('INFO_IMAGE_CAPTION_ATTRIBUTES', 'Por favor, asegúrese de que los efectos coinciden con la ubicación de la pantalla (ver abajo).');
define('INFO_IMAGE_VIDEO_URL', 'En el caso de que la imagen actual contenga un vídeo, introduzca aquí la Embed-URL.<br />Embed-URL  significa la URL contenida en el código de incrustación proporcionado por "YouTube" o "Vimeo". <span style="color:#c00;">! Sólo la URL, no todo el código de incrustación !</span><br />Puede suprimir la visualización de "vídeos relacionados" después de reproducir el vídeo añadiendo lo siguiente (para vídeos de YouTube) a la URL: <i>&amp;rel=0</i><br />Hay otros parámetros que se pueden utilizar para influir en el comportamiento del reproductor de vídeo.<br />Para ello, consulte las especificaciones de YouTube y/o de Vimeo.');

define('TXT_GENERAL_KENBURNS', 'Activa o desactiva el efecto KenBurns.');
define('TXT_IF_KENBURNS_ON', 'Si Kenburns está desactivado, los ajustes de abajo no son todos necesarios (vea allí).');
define('TXT_GENERAL_TRANSITION', 'La transición de las imágenes, "fade" o "slide" (<span style="color:#c00;">! siempre debe ser especificada</span>)');
define('TXT_GENERAL_STARTALIGN', 'Inicio del efecto KenBurns, horizontal | vertical');
define('TXT_GENERAL_ENDALIGN', 'Fin del efecto KenBurns, horizontal | vertical');
define('TXT_GENERAL_ZOOM_AND_PAN', 'Ajustes de zoom y duración del movimiento.');
define('TXT_GENERAL_CAPTION', 'Establece dónde aparece el texto y con qué efecto.');
define('TEXT_IMAGESLIDERS_CAPTION_ATTRIBUTES', 'Texto atributos <span style="color:#B30000; font-size:9px;">(si se selecciona un valor en una idioma, también se configuran los valores de las otros idiomas)</span>');
define('TXT_CAPTION_DIRECTION_AND_EFFECT', 'Los efectos y los lugares que encajan están uno al lado del otro.');
define('TXT_CAPTION_LEFT', 'Texto a la izquierda');
define('TXT_CAPTION_FADE_RIGHT', 'Se desliza de izquierda a derecha.');
define('TXT_CAPTION_RIGHT', 'Texto a la derecha');
define('TXT_CAPTION_FADE_LEFT', 'Se desliza de derecha a izquierda.');
define('TXT_CAPTION_TOP', 'Texto arriba');
define('TXT_CAPTION_FADE_DOWN', 'Se desliza de arriba hacia abajo.');
define('TXT_CAPTION_BOTTOM', 'Texto abajo');
define('TXT_CAPTION_FADE_UP', 'Se desliza de abajo hacia arriba.');
define('TXT_CAPTION_FADE', 'se aclara con el efecto "fade", siempre encaja');
define('TXT_KENBURN_ON', '<b>Encendido</b>');
define('TXT_KENBURN_OFF', '<b>Apagado</b>');
define('TXT_TRANSITION_FADE', '<b>Fade</b>');
define('TXT_TRANSITION_SLIDE', '<b>Slide</b>');
define('TXT_STARTALIGN_LEFTTOP', '<b>izquierda | arriba</b>');
define('TXT_STARTALIGN_LEFTCENTER', '<b>izquierda | centro</b>');
define('TXT_STARTALIGN_LEFTBOTTOM', '<b>izquierda | abajo</b>');
define('TXT_STARTALIGN_CENTERTOP', '<b>centro | arriba</b>');
define('TXT_STARTALIGN_CENTERCENTER', '<b>centro | centro</b>');
define('TXT_STARTALIGN_CENTERBOTTOM', '<b>centro | abajo</b>');
define('TXT_STARTALIGN_RIGHTTOP', '<b>derecha | arriba</b>');
define('TXT_STARTALIGN_RIGHTCENTER', '<b>derecha | centro</b>');
define('TXT_STARTALIGN_RIGHTBOTTOM', '<b>derecha | abajo</b>');
define('TXT_STARTALING_RANDOM', '<b>aleatorio</b>');
define('TXT_ENDALIGN_LEFTTOP', '<b>izquierda | arriba</b>');
define('TXT_ENDALIGN_LEFTCENTER', '<b>izquierda | centro</b>');
define('TXT_ENDALIGN_LEFTBOTTOM', '<b>izquierda | abajo</b>');
define('TXT_ENDALIGN_CENTERTOP', '<b>centro | arriba</b>');
define('TXT_ENDALIGN_CENTERCENTER', '<b>centro | centro</b>');
define('TXT_ENDALIGN_CENTERBOTTOM', '<b>centro | abajo</b>');
define('TXT_ENDALIGN_RIGHTTOP', '<b>derecha | arriba</b>');
define('TXT_ENDALIGN_RIGHTCENTER', '<b>derecha | centro</b>');
define('TXT_ENDALIGN_RIGHTBOTTOM', '<b>derecha | abajo</b>');
define('TXT_ENDALIGN_RANDOM', '<b>aleatorio</b>');
define('TXT_ZOOM', '<b>Alejar o acercar el zoom</b>, introduzca uno de los valores "out | in | random" ');
define('TXT_ZOOMFACT', 'Factor de zoom (introduzca un número de coma de un solo dígito (! la coma debe ser un punto), preferiblemente entre 1 y 2). Si el valor anterior se ajusta a "out", el factor de zoom debe ser correspondientemente alto o la imagen debe ser más grande que el contenedor HTML en el que se encuentra.');
define('TXT_PANDURATION', 'duración del movimiento en segundos (<span style="color:#c00;">! debe especificarse siempre</span>) (si no se especifica, se utiliza el valor especificado como timer para el cambio de pantalla en <i>general.js.php</i>  (5) )');
?>

<?php
/* --------------------------------------------------------------
   $Id: csv_backend.php 5217 2013-07-22 14:47:23Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/


define('TITLE', 'CSV Backend');

define('IMPORT', 'Importación');
define('EXPORT', 'Exportación');
define('UPLOAD', 'Cargar archivo en el servidor');
define('SELECT', 'Seleccionar el archivo a importar y realizar la importación (/import directorio)');
define('SAVE', 'Guardar en el servidor (/export directorio)');
define('LOAD', 'Enviar archivo al navegador');
define('CSV_TEXTSIGN_TITLE', 'Carácter de reconocimiento de texto');
define('CSV_TEXTSIGN_DESC', 'P.Ej. "   |  <span style="color:#c00;">¡Con punto y coma como separador, el carácter de reconocimiento de texto debe establecerse en " !</span>');
define('CSV_SEPERATOR_TITLE', 'Separador');
define('CSV_SEPERATOR_DESC', 'P.Ej. ;   |  <span style="color:#c00;">¡si el campo de entrada se deja vacío por defecto se utiliza \t (= pestaña) durante la exportación/importación !</span>');
define('COMPRESS_EXPORT_TITLE', 'Compresión');
define('COMPRESS_EXPORT_DESC', 'Compresión de datos exportados');
define('CSV_SETUP', 'Configuraciones');
define('TEXT_IMPORT', '');
define('TEXT_PRODUCTS', 'Productos');
define('TEXT_EXPORT', 'El archivo exportado se almacena en el directorio /export');
define('CSV_CATEGORY_DEFAULT_TITLE', 'Categoría para la importación');
define('CSV_CATEGORY_DEFAULT_DESC', 'Todos los artículos que no se han asignado  <b>ninguna</b> categoría en el archivo de importación CSV y que todavía no existen en la tienda, se importarán a esta categoría.<br/><b>Importante:</b> Si no desea importar artículos sin una categoría en el archivo de importación CSV, seleccione la categoría "Superior" porque no se importará ningún artículo a esta categoría.');
//BOC added constants for category depth, noRiddle
define('CSV_CAT_DEPTH_TITLE', 'Profundidad de categoría');
define('CSV_CAT_DEPTH_DESC', '¿Qué tan profundo debe ir el árbol de categorías? (p.ej. con la configuración por defecto 4: categoría principal y tres subcategorías)<br />Esta configuración es importante para importar correctamente las categorías creadas en el CSV. Lo mismo se aplica a la exportación.<br /><span style="color:#c00;">¡Más de 4 puede llevar a pérdidas de rendimiento y quizás no sea amigable para el cliente!');
//EOC added constants for category depth, noRiddle
?>

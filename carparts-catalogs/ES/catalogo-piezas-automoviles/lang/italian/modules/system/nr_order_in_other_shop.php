<?php
/************************************************************
* file: nr_order_in_other_shop.php
* use: language file for system file
* (c) noRiddle 04-2018
************************************************************/

define('MODULE_NR_ORDER_IN_OTHER_SHOP_TITLE', 'Order in another shop');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_DESCRIPTION', 'Modul serves for the purpose to order merchandise in the shopping cart in another shop. (e.g in case of a logistical shortage).');

define('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS_TITLE', 'Status');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS_DESC', 'Activate module ?');

define('MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP_TITLE', 'In which shop shall the order be executed ?');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP_DESC', 'Please enter only the shop name here in lowercase, without suffixes like "-ersatzteile" oder "-spare-parts".<br />Examples: skoda, bmw, bmw-bike, chrysler-dodge-jeep-ram');

$nr_oios_lngs = xtc_get_languages();
for($oios = 0, $coios = count($nr_oios_lngs); $oios < $coios; $oios++) {
    define('MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_'.strtoupper($nr_oios_lngs[$oios]['code']).'_TITLE', xtc_image(DIR_WS_LANGUAGES . $nr_oios_lngs[$oios]['directory'] .'/admin/images/'. $nr_oios_lngs[$oios]['image'], $nr_oios_lngs[$oios]['name']).' Explanatory text above "Checkout" button '.strtoupper($nr_oios_lngs[$oios]['code']));
    define('MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_'.strtoupper($nr_oios_lngs[$oios]['code']).'_DESC', 'Enter the text (reason) here why the order shall be finished in another shop.<br />As a placeholder for the above entered shop use <i>%s</i>.<br /><span style="display:inline-block; font-size:11px; line-height:12px;">(With help of the backup funktion any text may be saved.<br />With deinstallation and subsequent installation without restauration of the backup the standard text will be displayed again.)</span>');
}
?>
<?php
/*************************************************
* file: define_admin_lang_constants_en.php
* use: define additional lang constants
* (c) noRiddle 12-2016 - 01-2017
*************************************************/

//BOC favorites top menu
define('NR_MASTER_ADMIN_FAV_ALT_TXT', 'MasterAdminTools');
//BOC favorites top menu

//BOC order_status_comments
define('TEXT_ORDER_STATUS_COMMENTS_DROPDOWN', 'Comments boilerplate');
define('TXT_ORDER_STATUS_COMMENTS_CHOOSE', 'please choose');
//EOC order_status_comments

//BOC edit gm_price_status
define('NR_EDITPROD_PLEASE_CHOOSE', 'Please choose');
define('NR_EDITPROD_PROD_NOTAVAIL', 'Part no more available');
define('NR_EDITPROD_PROD_WILL_REPLACED', 'Part will be replaced');
define('NR_EDITPROD_HEADING', 'Part %s will be replaced or is no more available ?');
define('NR_EDITPROD_HEADING_COMMENT', '(Caution please, cannot be countermanded from here.)');
define('NR_EDITPROD_REPL_MODEL_TXT', 'Since the part will be replaced, enter the model number of the replacing part here <strong>without blanks and special characters</strong>');
define('NR_EDITPROD_BUTTON', 'Update part');
define('NR_EDITPROD_ALERT_NO_SPECIALCHARS', 'Please enter the new model no. (without blanks and special chars !!)<br />or choose "Part no more available".');
define('NR_EDITPROD_ALERT_REPLPART_NO_EXISTS', 'The indicated replacing partdoes\'t exist in the shop.<br />Please verify via search function in the frontend of the shop and generate the products if necessary.');
define('NR_EDITPROD_EDIT_SUCCESS', 'OKAY: Part with model no. %s has been successfully updated.');
define('NR_EDITPRODPRICE_HEADING', 'Change RRP of part %s ? (Click to open)');
define('NR_EDITPRODPRICE_HEADING_COMMENT', '(Enter new RRP price <u>netto</u> with a dot for a comma)');
define('NR_EDITPRODPRICE_TXT', 'New RRP');
define('NR_EDITPRODPRICE_EDIT_SUCCESS', 'OKAY: Price of part with model no. %s has been successfully updated.');
//EOC edit gm_price_status

//BOC new buttons for admin access
define('ADMACC_BUTTON_SET_ALL', 'Set all rights');
define('ADMACC_BUTTON_UNSET_ALL', 'Withdraw all rights');
define('ADMACC_BUTTON_SET_SW', 'Admin rights for stock workers');
define('ADMACC_BUTTON_SET_TRANSL', 'Admin rights for translators');
define('ADMACC_BUTTON_SET_MSW', 'Rights main stock worker');
define('ADMACC_BUTTON_SET_SEO', 'Rights SEO agent');
//BOC new buttons for admin access

//BOC gm_price_status, noRiddle
define('TEXT_PRODUCTS_PS_NORMAL', 'normal');
define('TEXT_PRODUCTS_PS_ONREQUEST', 'price on request');
define('TEXT_PRODUCTS_PS_NOPURCHASE', 'not available');
define('TEXT_PRODUCTS_GM_PRICE_STATUS', 'Price status:');
//EOC gm_price_status, noRiddle

//BOC bulk costs, noRiddle
define('TEXT_PRODUCTS_BULK', 'Bulk costs:');
//EOC bulk costs, noRiddle

//BOC inclusion for affiliate program, noRiddle
require(DIR_FS_LANGUAGES.'english/admin/'.'affiliate_configuration.php');
require(DIR_FS_LANGUAGES.'english/admin/'.'affiliate_english.php');
//EOC inclusion for affiliate program, noRiddle

//BOC heading for staff who edited order, noRiddle
define('TABLE_HEADING_CUSTOMER_STAFF', 'Staff member');
//EOC heading for staff who edited order, noRiddle

//BOC new contant for total shipping weight in order
define('AH_ORDER_TOT_WEIGHT', 'Shipping weight:');
//EOC new contant for total shipping weight in order

//BOC multi_order_status based on module by XTC-DELUXE.DE, noRiddle
define('TABLE_HEADING_EDIT','Edit all');
define('HEADING_MULTI_ORDER_STATUS','For all selected orders:');
define('BUTTON_CLOSE_PRINT_PAGES','Close all printing windows');
define('WARNING_ORDER_NOT_UPDATED_ALL', 'Warning: Not all orders have been updated.');
define('TEXT_DO_STATUS_CHANGE','Update status:');
define('TEXT_DO_PRINT_INVOICE','Print invoice:');
define('TEXT_DO_PRINT_PACKINGSLIP','Print packing slip:');
define('MULTISTATUS_DELETE_EXPLAIN', 'Delete?<br />(<span style="font-size:8px;">all three chackboxes have to be checked,<br />to avoid accidental deletion.</span>)');
//EOC multi_order_status based on module by XTC-DELUXE.DE, noRiddle

//BOC EK Preis
define('DEALERS_PRICE', 'Purchase price');
//EOC EK Preis

define('TABLE_HEADING_CATALOG_CREDIT', 'VIN search credit');

//BOC PDFBill NEXT
define('BUTTON_INVOICE_PDF', 'Bill PDF');
define('BUTTON_PACKINGSLIP_PDF', 'Packingslip PDF');
define('BUTTON_BILL_NR', 'Billing number:');
define('BUTTON_SET_BILL_NR', 'Set billing number');

define('MODULE_PDF_BILL_STATUS_TITLE', 'Module activated ?');
define('MODULE_PDF_BILL_STATUS_DESC', '');
define('PDF_BILL_LASTNR_TITLE', 'Last invoice number');
define('PDF_BILL_LASTNR_DESC', 'The last invoice number for the automated generation of Bills.');
define('PDF_USE_ORDERID_PREFIX_TITLE', 'invoice number Prefix');
define('PDF_USE_ORDERID_PREFIX_DESC', 'Prefix for the invoice number. Only used if order number is used as the invoice number.');
define('PDF_USE_ORDERID_TITLE', 'Order number as invoice number');
define('PDF_USE_ORDERID_DESC', 'Use invoice number instead of ordernumber');
define('PDF_STATUS_COMMENT_TITLE', 'PDF invoice status comment');
define('PDF_STATUS_COMMENT_DESC', 'Comment which will be added to the system when sending an PDF invoice.');
define('PDF_STATUS_COMMENT_SLIP_TITLE', 'Packaging slip status comment');
define('PDF_STATUS_COMMENT_SLIP_DESC', 'Comment which will be added to the system when sending a PDF packing slip.');
define('PDF_FILENAME_SLIP_TITLE', 'Packaging Slip Filename');
define('PDF_FILENAME_SLIP_DESC', 'Filename of packaging slip. <strong>Please without .pdf and Spaces. Use underscore instead of spaces!</strong>.');
define('PDF_MAIL_SUBJECT_TITLE', 'Invoice mail subject');
define('PDF_MAIL_SUBJECT_DESC', 'Please fill in the invoice mail subject. <strong>{oID}</strong> will be replaced with the order number.');
define('PDF_MAIL_SUBJECT_SLIP_TITLE', 'Packaging slip mail subject');
define('PDF_MAIL_SUBJECT_SLIP_DESC', 'Please fill in the packaging slip mail subject. <strong>{oID}</strong> will be replaced with the order number.');
define('PDF_MAIL_SLIP_COPY_TITLE', 'Packaging slip - forward mail');
define('PDF_MAIL_SLIP_COPY_DESC', 'Please enter forwarding addresses for mails of the packaging slip.');
define('PDF_MAIL_BILL_COPY_TITLE', 'Invoice - Forward Mail');
define('PDF_MAIL_BILL_COPY_DESC', 'Please enter forwarding addresses for mails of the Invoice-Mail');
define('PDF_FILENAME_TITLE', 'Invoice filename');
define('PDF_FILENAME_DESC', 'Filename of invoice. <strong>Please without .pdf</strong>. Spaces will be replaced with underscores. Variables: <strong>{oID}</strong>, <strong>{bill}</strong>, <strong>{cID}</strong>.');
define('PDF_SEND_ORDER_TITLE', 'Automated Invoice-PDF Mail Send');
define('PDF_SEND_ORDER_DESC', 'Invoice-PDF will be automatically send after the order process is finished.');

define('PDF_USE_CUSTOMER_ID_TITLE', 'Use customers-id instead of customers-csid');
define('PDF_USE_CUSTOMER_ID_DESC', 'Use the customers-id instead of customers-csi. Sometime needed if csi is not automatically set.');

define('PDF_STATUS_ID_BILL_TITLE', 'Order Status ID - Invoice-PDF');
define('PDF_STATUS_ID_BILL_DESC', 'You are able to find the Order Status ID in the Browser URL-Line <strong>oID=</strong> while editing the Order Status.');
define('PDF_STATUS_ID_SLIP_TITLE', 'Order Status ID - Slip-PDF');
define('PDF_STATUS_ID_SLIP_DESC', 'You are able to find the Order Status ID in the Browser URL-Line <strong>oID=</strong> while editing the Order Status..');

define('PDF_PRODUCT_MODEL_LENGTH_TITLE', 'Maximum product model length');
define('PDF_PRODUCT_MODEL_LENGTH_DESC', 'Maximum count of characters until the product model gets truncated.');
define('PDF_UPDATE_STATUS_TITLE', 'Update order status');
define('PDF_UPDATE_STATUS_DESC', 'Update status automatically after PDF-Mail.');
define('PDF_USE_ORDERID_SUFFIX_TITLE', 'Invoice number suffix');
define('PDF_USE_ORDERID_SUFFIX_DESC', 'Suffix for the invoice number. Only used if order number is used as the invoice number.');
define('PDF_STATUS_SEND_TITLE', 'Send invoice on order status update');
define('PDF_STATUS_SEND_DESC', 'Send invoice automatically when order status is the indicated below');
define('PDF_STATUS_SEND_ID_TITLE', 'Send pdf invoice with this order status');
define('PDF_STATUS_SEND_ID_DESC', 'The invoice will be send on update to this order status ID<br />(if "Send invoice on order status update" is set to "Yes")');

define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_TITLE', 'Send packing slip only to logistician');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_DESC', 'If you activate this function the packing slip will not be sent to the customer but only to the below indicated mail-address.');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME_TITLE', 'Forwarder name');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME_DESC', 'Enter name of the forwarder, who should get the packslip.<br />(Only if "Send packing slip only to logistician" to "Yes".)');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL_TITLE', 'Forwarder email');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL_DESC', 'Enter email of the forwarder, who should get the packslip.<br />(Only if "Send packing slip only to logistician" to "Yes".)');

define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_TITLE', 'Next Invoicenumber');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_DESC', '<span style="color:#c00;">Only change well-considered. Number will be assigned automatically by the system !<br />In case you set "Order number as invoice number" to "Yes", the change of the number will have naturally no effect.</span>');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT_TITLE', 'Invoicenumber Format');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT_DESC', 'Construction scheme: {n}=current number, {d}=day, {m}=month, {y}=year, <br>"100{n}-{d}-{m}-{y}" gets "10099-28-02-2007"');
define('PDF_COMMENT_ON_PACKING_SLIP_TITLE', 'Comment of client on packing slip ?');
define('PDF_COMMENT_ON_PACKING_SLIP_DESC', 'Print the comment which was posted by the client while ordering on the packing slip ?');

//BOC - Innergemeinschaftliche Lieferungen
define('PDF_BILL_EU_CUSTOMERS_GROUP_ID_TITLE','Customer group EU Merchant:');
define('PDF_BILL_EU_CUSTOMERS_GROUP_ID_DESC','<b>Customer group ID (cID)</b> of EU Merchant - for there customers the tax free note according to &sect; 4 Nr. 1 b Value Added Tax Act (UStG) is added to the invoice!<br /><b>Separate several entries by comma!</b><br />You find the customer group ID in the browser input row after <strong>cID=</strong> when editing the customer group.');
//EOC - Innergemeinschaftliche Lieferungen

define('TABLE_HEADING_BILL_NR', 'Billing no.');
define('PDF_BILL_NR_INFO2', 'Please open the order to enter the invoice number,<br />at the end of the order!');
//EOC PDFBill NEXT

//BOC yandex heat map, noRiddle
define('ACTIVATE_YANDEX_HEATMAP_TITLE', 'Activate Yandex heat map ?');
define('ACTIVATE_YANDEX_HEATMAP_DESC', 'To use this the heat map must be integrated in <i>/templates/DEIN_TEMPLATE/javascript/general_bottom.js.php</i>.');
//EOC yandex heat map, noRiddle
?>
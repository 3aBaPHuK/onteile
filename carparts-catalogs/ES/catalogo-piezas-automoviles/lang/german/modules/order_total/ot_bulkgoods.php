<?php
/***************************************
* file: ot_bulkgoods
* use: language file for above ot module
* (c) noRiddle 05-2017
***************************************/

define('MODULE_ORDER_TOTAL_BULKGOODS_HELP_LINK', ' <a onclick="window.open(\'popup_help.php?type=order_total&modul=ot_bulkgoods&lng=german\', \'Hilfe\', \'scrollbars=yes,resizable=yes,menubar=yes,width=800,height=600\'); return false" target="_blank" href="popup_help.php?type=order_total&modul=ot_sperrgut&lng=german"><b>[HILFE]</b></a>');
define('MODULE_ORDER_TOTAL_BULKGOODS_HELP_TEXT', '<h2>konfigurierbares Sperrgutmodul</h2>
<p>Eigentlich ist das Modul selbsterkl&auml;rend aufgebaut.<br />Im Falle es jedoch zu Verst&auml;ndnisproblemen kommt, hier nochmals eine kleine Anleitung.<p>
<ul>
<li>Modul installieren.</li>
<li>Bitte auf die Sortierung achten !, wenn ebenfalls "<i>ot_cod_fee</i>" installiert ist, welches per default die Sortierung "35" hat, sollte das "<i>ot_sperrgut</i>" die Sortierung "34" bekommen.</li>
<li>Per Dropdown Art der Berechnung bestimmen.<br />Im Warenkorb werden<br />- insofern nicht "nur einmal die h&ouml;chsten Sperrgutkosten je Warenkorb" ausgew&auml;hlt wurde -<br /> die Sperrgutkosten bei jedem Artikel einzeln angezeigt<br />und au&szlig;erdem werden die Gesamt-Sperrgutkosten in der Zusamenfassung angezeigt.<br />In der Produkt-Einzelansicht werden die Sperrgutkosten unter dem Versandkostenlink angezeigt</li>
<li>Angeben ob ein Link zu Informationen über "Sperrgutkosten" auf der checkout_shipping-Seite bei jeder Versandart angezeigt werden soll.<br />"Versandkostenfrei" und "Selbstabholung" werden nicht mit dem Link versehen.<br />Wenn \'Ja\' gewählt wird, nicht vergessen die Informationen über Sperrgutkosten in den Content für Versandkosten mit aufzunehmen.<br />Sowohl auf der Produkt-Einzelansicht als auch auf der ccheckout_shipping-Seite (wenn aktiviert) wird ein Link zu dem Content angezeigt.</li>
<li>Auf der checkout_confirmation-Seite, in der Bestellung im Backend sowie in der Bestellbest&auml;tigung werden die Sperrgutkosten automatisch aufgef&uuml;hrt.</li>
</ul><br /><p><a href="http://www.revilonetz.de/kontakt" target="_blank">noRiddle</a> w&uuml;nscht viel Freude und Erfolg mit dem Modul</p>');

define('MODULE_ORDER_TOTAL_BULKGOODS_TITLE', 'Sperrgutkosten'.MODULE_ORDER_TOTAL_BULKGOODS_HELP_LINK);
define('MODULE_ORDER_TOTAL_BULKGOODS_DESCRIPTION', 'Sperrgutkosten einer Bestellung');
  
define('MODULE_ORDER_TOTAL_BULKGOODS_CC_TITLE', 'Sperrgutzuschlag');
 
define('MODULE_ORDER_TOTAL_BULKGOODS_STATUS_TITLE','Sperrgut aktiv');
define('MODULE_ORDER_TOTAL_BULKGOODS_STATUS_DESC','Aktivieren der Sperrgutkosten?');
  
define('MODULE_ORDER_TOTAL_BULKGOODS_SORT_ORDER_TITLE','Sortierreihenfolge');
define('MODULE_ORDER_TOTAL_BULKGOODS_SORT_ORDER_DESC', 'Anzeigereihenfolge.');

define('MODULE_ORDER_TOTAL_BULKGOODS_COSTS_TITLE','Kosten pro Sperrgut');
define('MODULE_ORDER_TOTAL_BULKGOODS_COSTS_DESC','');

define('MODULE_ORDER_TOTAL_BULKGOODS_METHOD_TITLE','Wie sollen die Sperrgutkosten berechnet werden ?');
define('MODULE_ORDER_TOTAL_BULKGOODS_METHOD_DESC','<ul style="list-style-type:decimal;"><li>alle Sperrgutkosten hochaddieren = die Kosten werden mit der Stck.-Zahl des Artikels multilpiziert</li><li>nur einmal je Artikel Sperrgutkosten = die Kosten werden nur einmal je Artikel berechnet, unabh&auml;ngig von der St&uuml;ckzahl</li><li>nur einmal die h&ouml;chsten Sperrgutkosten je Warenkorb = erkl&auml;rt sich von selbst</li></ul>');

define('MODULE_ORDER_TOTAL_BULKGOODS_SHOW_IN_CHECKOUT_SHIPPING_TITLE','Auf checkout_shipping anzeigen');
define('MODULE_ORDER_TOTAL_BULKGOODS_SHOW_IN_CHECKOUT_SHIPPING_DESC','<ul><li>Bei \'JA\' wird ein Link zu den Sperrgutkosten auf der checkout_shipping-Seite in den zur Verf&uuml;gung stehenden Versandarten angezeigt.</li><li>Nicht vergessen dafür die Informationen über Sperrgutkosten in den Content für Versandkosten mit aufzunehmen.</li></ul>');
  
//BOC dropdown array bulgoods module
define('NR_ADD_UP_ALL_BULKGOODS_COSTS', 'alle Sperrgutkosten hochaddieren');
define('NR_ADD_UP_ONE_BULKGOODS_COST_PER_ARTICLE', 'nur einmal je Artikel Sperrgutkosten');
define('NR_ONLY_HIGHEST_BULKGOODS_COST_PER_SHOPPING_CART', 'nur einmal die h&ouml;chsten Sperrgutkosten je Warenkorb');
//EOC dropdown array bulkgoods module

define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT_TITLE', 'Automatisch ausgelesene Kundenstatus');
define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT_DESC', 'Nicht editieren!!, wird automatisch aus der DB geholt.');

define('MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS_TITLE', 'Steuerklasse');
define('MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS_DESC', 'Folgende Steuerklasse für die Sperrgutkosten wählen');

//BOC customer status factors
//if(MODULE_ORDER_TOTAL_BULKGOODS_STATUS == 'true') {
if(strpos(MODULE_ORDER_TOTAL_INSTALLED, 'ot_bulkgoods.php')) {
    $custstat_array = json_decode(MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT, true);
    foreach($custstat_array as $k => $cs_name) {
        define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$k.'_TITLE', 'Faktor-Aufschlag für KG "<i>'.$cs_name.'</i>"');
        define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$k.'_DESC', 'Multiplikationsfaktor für Kundengruppe '.$k.' (= "<i>'.$cs_name.'</i>")<br />(Der Wert der bei dem Produkt als Sperrgut hinterlegt ist wird bei dieser Kundengruppe mit dem hier angegebenen Wert multipliziert.)');
    }
}
?>
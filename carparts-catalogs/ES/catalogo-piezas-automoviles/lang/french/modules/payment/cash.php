<?php

/* -----------------------------------------------------------------------------------------
   $Id: cash.php 1102 2005-07-24 15:05:38Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com
   (c) 2003	 nextcommerce (invoice.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_CASH_TEXT_DESCRIPTION', 'Paiement comptant');
define('MODULE_PAYMENT_CASH_TEXT_TITLE', 'Paiement comptant');
define('MODULE_PAYMENT_CASH_TEXT_INFO', '');
define('MODULE_PAYMENT_CASH_STATUS_TITLE', 'Activer paiement comptant');
define('MODULE_PAYMENT_CASH_STATUS_DESC', 'Voulez-vous accepter des paiements comptants?');
define('MODULE_PAYMENT_CASH_ORDER_STATUS_ID_TITLE', 'Établir le statut de commande');
define('MODULE_PAYMENT_CASH_ORDER_STATUS_ID_DESC', 'Moduler les commandes effectuées avec ce module vers ce statut');
define('MODULE_PAYMENT_CASH_SORT_ORDER_TITLE', 'Ordre d\'affichage');
define('MODULE_PAYMENT_CASH_SORT_ORDER_DESC', 'Ordre d\'affichage. Affiche d\'abord le premier chiffre.');
define('MODULE_PAYMENT_CASH_ZONE_TITLE', 'Zone de paiement');
define('MODULE_PAYMENT_CASH_ZONE_DESC', 'Si une zone est choisi, la méthode de paiement n\'est que valable pour cette zone.');
define('MODULE_PAYMENT_CASH_ALLOWED_TITLE', 'Zone autorisées');
define('MODULE_PAYMENT_CASH_ALLOWED_DESC', 'Entrez <b>séparément</b> les zones qui dévraient être autorisées pour ce module. (p.ex. AT,DE (si vide, toutes les zones autorisées))');
?>

<?php
/*
  $Id: backup.php,v 1.16 2002/03/16 21:30:02 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Administrador del Backup de la base de datos');

define('TEXT_INFO_DO_BACKUP', '¡La base de datos se está respaldando!');
define('TEXT_INFO_DO_BACKUP_OK', '¡La base de datos se ha respaldado con éxito!');
define('TEXT_INFO_DO_GZIP', '¡El fichero de Backup se está empaquetando!');
define('TEXT_INFO_WAIT', '¡Por favor, espere!');

define('TEXT_INFO_DO_RESTORE', '¡La base de datos se está restaurando!');
define('TEXT_INFO_DO_RESTORE_OK', '¡La base de datos se ha restaurado con éxito!');
define('TEXT_INFO_DO_GUNZIP', '¡El fichero de Backup se está desempaquetando!');

define('ERROR_BACKUP_DIRECTORY_DOES_NOT_EXIST', 'Error: el directorio para el respaldo no existe. Por favor, arregle el error en su configure.php.');
define('ERROR_BACKUP_DIRECTORY_NOT_WRITEABLE', 'Error: el directorio para el respaldo no se puede escribir.');
define('ERROR_DOWNLOAD_LINK_NOT_ACCEPTABLE', 'Error: El enlace de descarga no es aceptable.');
define('ERROR_DECOMPRESSOR_NOT_AVAILABLE', 'Error: No se dispone de un desempaquetador adecuado.');
define('ERROR_UNKNOWN_FILE_TYPE', 'Error: Tipo de fichero desconocido.');
define('ERROR_RESTORE_FAILES', 'Error: Restauración fracasada.');
define('ERROR_DATABASE_SAVED', 'Error: La base de datos no se ha podido respaldar.');
define('ERROR_TEXT_PATH', 'Error: ¡La ruta a mysqldump no fue encontrada o especificada!');

define('SUCCESS_LAST_RESTORE_CLEARED', 'Exitoso: La última fecha de restauración ha sido borrado.');
define('SUCCESS_DATABASE_SAVED', 'Exitoso: La base de datos se ha respaldado.');
define('SUCCESS_DATABASE_RESTORED', 'Exitoso: La base de datos se ha restaurado.');
define('SUCCESS_BACKUP_DELETED', 'Exitoso: El respaldo se ha eliminado.');

define('TEXT_BACKUP_UNCOMPRESSED', 'El fichero de Backup se ha desempaquetado: ');

define('TEXT_SIMULATION', '<br>(Simulación con log-fichero)');

?>

<?php
/**************************************************************
* build mySQL string for load data local infile
* c) noRiddle 12-2015
**************************************************************/
$field_str = '';
$columns_query = xtc_db_query("SHOW COLUMNS FROM csv_import_aux");
while($columns = xtc_db_fetch_array($columns_query)) {
    if(isset($has_prid) && $has_prid === true) {
        if($columns['Field'] != 'import_id') {
            $field_str .= $columns['Field'].',';
        }
    } else if(!isset($has_prid)) {
        if($columns['Field'] != 'import_id' && $columns['Field'] != 'products_id') {
            $field_str .= $columns['Field'].',';
        }
    }
}
$field_str = rtrim($field_str, ',');
$textsign = CSV_TEXTSIGN != '' ? "ENCLOSED BY '".CSV_TEXTSIGN."'" : '';
                                               
$load_sql_aux_table = "LOAD DATA LOCAL INFILE '".$_SERVER['DOCUMENT_ROOT']."/".$shop."/import/$post_select_file' 
                                   INTO TABLE csv_import_aux 
                                CHARACTER SET utf8
                         FIELDS TERMINATED BY '".CSV_SEPERATOR."'
                                  ".$textsign."
                          LINES TERMINATED BY '\r\n' 
                               IGNORE 1 LINES (".$field_str.")";
?>
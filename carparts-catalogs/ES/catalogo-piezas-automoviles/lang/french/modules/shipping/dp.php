<?php
/* -----------------------------------------------------------------------------------------
   $Id: dp.php 5122 2013-07-18 11:47:17Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(dp.php,v 1.4 2003/02/18 04:28:00); www.oscommerce.com
   (c) 2003 nextcommerce (dp.php,v 1.5 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   German Post (Deutsche Post WorldNet)         	Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   enhanced on 2010-12-08 18:17:30Z franky_n
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_DP_TEXT_TITLE', 'Deutsche Post (Poste allemande)');
define('MODULE_SHIPPING_DP_TEXT_DESCRIPTION', 'Deutsche Post (poste allemande) - envoi mondial');
define('MODULE_SHIPPING_DP_TEXT_WAY', 'Envoi vers');
define('MODULE_SHIPPING_DP_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_DP_INVALID_ZONE', 'Malheureusement il n\'est pas possible d\'envoyer dans ce pays');
define('MODULE_SHIPPING_DP_UNDEFINED_RATE', 'Les frais de port ne peuvent pas être calculés pour le moment');

define('MODULE_SHIPPING_DP_STATUS_TITLE' , 'Deutsche Post (poste allemande) WorldNet');
define('MODULE_SHIPPING_DP_STATUS_DESC' , 'Voulez-vous proposer l\'envoi par la poste allemande?');
define('MODULE_SHIPPING_DP_HANDLING_TITLE' , 'Frais de traitement');
define('MODULE_SHIPPING_DP_HANDLING_DESC' , 'Frais de traitement pour ce type d\'envoi en Euro');
define('MODULE_SHIPPING_DP_TAX_CLASS_TITLE' , 'Catégorie fiscale');
define('MODULE_SHIPPING_DP_TAX_CLASS_DESC' , 'Appliquer la catégorie fiscale suivate de frais de port.');
define('MODULE_SHIPPING_DP_ZONE_TITLE' , 'Zone d\'envoi');
define('MODULE_SHIPPING_DP_ZONE_DESC' , 'Si vous séléctionnez une zone, ce type d\'envoi sera seulement proposé dans cette zone.');
define('MODULE_SHIPPING_DP_SORT_ORDER_TITLE' , 'Ordre d\'affichage');
define('MODULE_SHIPPING_DP_SORT_ORDER_DESC' , 'Les plus bas sont affichés d\'abord.');
define('MODULE_SHIPPING_DP_ALLOWED_TITLE' , 'Zones d\'envoi individuelles');
define('MODULE_SHIPPING_DP_ALLOWED_DESC' , 'Indiquez les zones <b>individuellement</b> dans lesquelles l\'envoi est possible. Par exemple AT,DE');
define('MODULE_SHIPPING_DP_NUMBER_ZONES_TITLE' , 'Nombre de zones');
define('MODULE_SHIPPING_DP_NUMBER_ZONES_DESC' , 'Nombre des zones à disposition');
define('MODULE_SHIPPING_DP_DISPLAY_TITLE' , 'Activer l\'affichage');
define('MODULE_SHIPPING_DP_DISPLAY_DESC' , 'Voulez-vous afficher quand l\'envoi dans un certain pays n\'est pas possible ou si les frais de port ne peuvent pas être calculés?');

for ($module_shipping_dp_i = 1; $module_shipping_dp_i <= MODULE_SHIPPING_DP_NUMBER_ZONES; $module_shipping_dp_i ++) {
define('MODULE_SHIPPING_DP_COUNTRIES_'.$module_shipping_dp_i.'_TITLE' , '<hr/>DP Zone '.$module_shipping_dp_i.' Pays');
define('MODULE_SHIPPING_DP_COUNTRIES_'.$module_shipping_dp_i.'_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone '.$module_shipping_dp_i.' (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_DP_COST_'.$module_shipping_dp_i.'_TITLE' , 'DP Zone '.$module_shipping_dp_i.' Tableau d\'envoi');
define('MODULE_SHIPPING_DP_COST_'.$module_shipping_dp_i.'_DESC' , 'Frais de port de la zone '.$module_shipping_dp_i.' se référant au poids de la commande. Exemple: 3:8.50,7:10.50,99999:12.00... Poids plus de 0 et moind de 3 coutent 8.50, moins de 7 coute 10.50 pour la zone '.$module_shipping_dp_i.'.');
}
?>

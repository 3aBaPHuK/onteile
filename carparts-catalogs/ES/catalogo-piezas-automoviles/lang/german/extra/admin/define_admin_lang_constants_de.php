<?php
/*************************************************
* file: define_admin_lang_constants_de.php
* use: define additional lang constants
* (c) noRiddle 12-2016 - 01-2017
*************************************************/

//BOC favorites top menu
define('NR_MASTER_ADMIN_FAV_ALT_TXT', 'MasterAdminTools');
//BOC favorites top menu

//BOC order_status_comments
define('TEXT_ORDER_STATUS_COMMENTS_DROPDOWN', 'Kommentar-Vorlagen');
define('TXT_ORDER_STATUS_COMMENTS_CHOOSE', 'bitte auswählen');
//EOC order_status_comments

//BOC edit gm_price_status
define('NR_EDITPROD_PLEASE_CHOOSE', 'Bitte auswählen');
define('NR_EDITPROD_PROD_NOTAVAIL', 'Teil entfällt');
define('NR_EDITPROD_PROD_WILL_REPLACED', 'Teil wird ersetzt');
define('NR_EDITPROD_HEADING', 'Artikel %s wird ersetzt oder entfällt ?');
define('NR_EDITPROD_HEADING_COMMENT', '(Bitte mit Vorsicht, kann von hier nicht rückgängig gemacht werden.)');
define('NR_EDITPROD_REPL_MODEL_TXT', 'Da der Artikel ersetzt wird, hier die Artikelnummer des ersetzenden Artikels <strong>ohne Leer- und Sonderzeichen</strong> eingeben');
define('NR_EDITPROD_BUTTON', 'Artikel updaten');
define('NR_EDITPROD_ALERT_NO_SPECIALCHARS', 'Bitte neue Artikelnummer (ohne Leer- und Sonderzeichen !!) eingeben<br />oder "Teil entfällt" wählen.');
define('NR_EDITPROD_ALERT_REPLPART_NO_EXISTS', 'Das angegebene Ersetzteil existiert nicht im Shop.<br />Bitte mittels Suchfunktion im Frontend des Shop überprüfen und ggfls. Artikel anlegen.');
define('NR_EDITPROD_EDIT_SUCCESS', 'OKAY: Artikel mit Artikelnummer %s wurde erfolgreich geändert.');
define('NR_EDITPRODPRICE_HEADING', 'Artikel %s: UVP ändern ? (Klick zum öffnen)');
define('NR_EDITPRODPRICE_HEADING_COMMENT', '(Neuen UVP-Preis bitte <u>netto</u> und mit Punkt als Komma eingeben)');
define('NR_EDITPRODPRICE_TXT', 'Neue UVP');
define('NR_EDITPRODPRICE_EDIT_SUCCESS', 'OKAY: Preis des Artikels mit Artikelnummer %s wurde erfolgreich geändert.');
//EOC edit gm_price_status

//BOC new buttons for admin access
define('ADMACC_BUTTON_SET_ALL', 'Alle Rechte setzen');
define('ADMACC_BUTTON_UNSET_ALL', 'Alle Rechte entziehen');
define('ADMACC_BUTTON_SET_SW', 'Rechte Lageristen');
define('ADMACC_BUTTON_SET_TRANSL', 'Rechte Übersetzer');
define('ADMACC_BUTTON_SET_MSW', 'Rechte Lagerleiter');
define('ADMACC_BUTTON_SET_SEO', 'Rechte SEO-Agent');
//BOC new buttons for admin access

//BOC gm_price_status, noRiddle
define('TEXT_PRODUCTS_PS_NORMAL', 'normal');
define('TEXT_PRODUCTS_PS_ONREQUEST', 'Preis auf Anfrage');
define('TEXT_PRODUCTS_PS_NOPURCHASE', 'nicht lieferbar');
define('TEXT_PRODUCTS_GM_PRICE_STATUS', 'Preis-Status:');
//EOC gm_price_status, noRiddle

//BOC bulk costs, noRiddle
define('TEXT_PRODUCTS_BULK', 'Sperrgutkosten:');
//EOC bulk costs, noRiddle

//BOC inclusion for affiliate program, noRiddle
require(DIR_FS_LANGUAGES.'german/admin/'.'affiliate_configuration.php');
require(DIR_FS_LANGUAGES.'german/admin/'.'affiliate_german.php');
//EOC inclusion for affiliate program, noRiddle

//BOC heading for staff who edited order, noRiddle
define('TABLE_HEADING_CUSTOMER_STAFF', 'Mitarbeiter');
//EOC heading for staff who edited order, noRiddle

//BOC new contant for total shipping weight in order
define('AH_ORDER_TOT_WEIGHT', 'Versandgewicht:');
//EOC new contant for total shipping weight in order

//BOC multi_order_status based on module by XTC-DELUXE.DE, noRiddle
define('TABLE_HEADING_EDIT','Edit all');
define('HEADING_MULTI_ORDER_STATUS','F&uuml;r alle markierten Bestellungen:');
define('BUTTON_CLOSE_PRINT_PAGES','Alle Druckfenster schlie&szlig;en.');
define('WARNING_ORDER_NOT_UPDATED_ALL', 'Hinweis: Einige Bestellungen wurden nicht aktualisiert.');
define('TEXT_DO_STATUS_CHANGE','Status aktualisieren:');
define('TEXT_DO_PRINT_INVOICE','Rechnung drucken:');
define('TEXT_DO_PRINT_PACKINGSLIP','Lieferschein drucken:');
define('MULTISTATUS_DELETE_EXPLAIN', 'Löschen?<br />(<span style="font-size:8px;">Es müssen alle drei Checkboxen angehakt werden,<br />um ein versehentliches Löschen zu vermeiden.</span>)');
//EOC multi_order_status based on module by XTC-DELUXE.DE, noRiddle

//BOC EK Preis
define('DEALERS_PRICE', 'Einkaufspreis');
//EOC EK Preis

//BOC vin search credit, noRiddle
define('TABLE_HEADING_CATALOG_CREDIT', 'VIN-Suche-Credit');
//EOC vin search credit, noRiddle

//BOC PDFBill NEXT
define('BUTTON_INVOICE_PDF', 'Rechnung PDF');
define('BUTTON_PACKINGSLIP_PDF', 'Lieferschein PDF');
define('BUTTON_BILL_NR', 'Rechnungsnummer:');
define('BUTTON_SET_BILL_NR', 'Rechnungsnummer vergeben');

define('MODULE_PDF_BILL_STATUS_TITLE', 'Modul aktiviert ?');
define('MODULE_PDF_BILL_STATUS_DESC', '');
define('PDF_BILL_LASTNR_TITLE', 'Letzte Rechnungsnummer');
define('PDF_BILL_LASTNR_DESC', 'Die letzte Rechnungsnummer f&uuml;r die automatische Vergabe.');
define('PDF_USE_ORDERID_PREFIX_TITLE', 'Rechnungsnummer Prefix');
define('PDF_USE_ORDERID_PREFIX_DESC', 'Prefix f&uuml;r die Rechnungsnummer, falls die Bestellnummer als Rechnungsnummer verwendet wird.');
define('PDF_USE_ORDERID_TITLE', 'Bestellnummer als Rechnungsnummer');
define('PDF_USE_ORDERID_DESC', 'Durch diese Option wird die Bestellnummer als Rechnungsnummer verwendet.');
define('PDF_STATUS_COMMENT_TITLE', 'Bestell-PDF Status Kommentar');
define('PDF_STATUS_COMMENT_DESC', 'Kommentar der beim Verschicken einer Rechnung in das System hinzugef&uuml;gt wird.');
define('PDF_STATUS_COMMENT_SLIP_TITLE', 'Lieferschein-PDF Status Kommentar');
define('PDF_STATUS_COMMENT_SLIP_DESC', 'Kommentar der beim Verschicken eines Lieferschein in das System hinzugef&uuml;gt wird.');
define('PDF_FILENAME_SLIP_TITLE', 'Lieferschein Dateiname');
define('PDF_FILENAME_SLIP_DESC', 'Dateiname des Lieferscheins. Leerzeichen werden durch einen Unterstrich ersetzt. Variablen: <strong>{oID}</strong>, <strong>{bill}</strong>, <strong>{cID}</strong>. <strong>Bitte ohne .pdf</strong>.');
define('PDF_MAIL_SUBJECT_TITLE', 'Rechnungs-Mail Betreff');
define('PDF_MAIL_SUBJECT_DESC', 'Geben Sie hier den Betreff f&uuml;r die Rechnungsmail an. <strong>{oID}</strong> dient als Platzhalter f&uuml;r die Bestellnummer.');
define('PDF_MAIL_SUBJECT_SLIP_TITLE', 'Lieferschein-Mail Betreff');
define('PDF_MAIL_SUBJECT_SLIP_DESC', 'Geben Sie hier den Betreff f&uuml;r die Lieferscheinmail an. <strong>{oID}</strong> dient als Platzhalter f&uuml;r die Bestellnummer.');
define('PDF_MAIL_SLIP_COPY_TITLE', 'Lieferschein - Weiterleitungsadresse');
define('PDF_MAIL_SLIP_COPY_DESC', 'Geben Sie hier eine E-Mailaddresse an, wenn Sie eine Kopie erhalten wollen.');
define('PDF_MAIL_BILL_COPY_TITLE', 'Rechnung - Weiterleitungsadresse');
define('PDF_MAIL_BILL_COPY_DESC', 'Geben Sie hier eine E-Mailaddresse an, wenn Sie eine Kopie erhalten wollen.');
define('PDF_FILENAME_TITLE', 'Rechnung Dateiname');
define('PDF_FILENAME_DESC', 'Dateiname der Rechnung. Leerzeichen werden durch einen Unterstrich ersetzt. Variablen: <strong>{oID}</strong>, <strong>{bill}</strong>, <strong>{cID}</strong>. <strong>Bitte ohne .pdf</strong>.');
define('PDF_SEND_ORDER_TITLE', 'Rechnungs-PDF automatisch versenden');
define('PDF_SEND_ORDER_DESC', 'Wenn diese Option aktiviert ist, wird die Rechnungs-PDF direkt nach der Bestellung automatisch verschickt.');

define('PDF_USE_CUSTOMER_ID_TITLE', 'Nutze Kunden-ID als Kundennummer');
define('PDF_USE_CUSTOMER_ID_DESC', 'Die Kunden-ID wird als Kundennummer verwendet. Bitte auf false stellen, falls eine Kundennummer vergeben wird.');

define('PDF_STATUS_ID_BILL_TITLE', 'Bestellstatus-ID - Rechnungs-PDF');
define('PDF_STATUS_ID_BILL_DESC', 'Die Bestellstatus-ID finden Sie in der Browserzeile nach <strong>oID=</strong> wenn Sie den Bestellstatus editieren.');
define('PDF_STATUS_ID_SLIP_TITLE', 'Bestellstatus-ID - Lieferschein');
define('PDF_STATUS_ID_SLIP_DESC', 'Die Bestellstatus-ID finden Sie in der Browserzeile nach <strong>oID=</strong> wenn Sie den Bestellstatus editieren.');

define('PDF_PRODUCT_MODEL_LENGTH_TITLE', 'Maximall&auml;nge Artikelnummer');
define('PDF_PRODUCT_MODEL_LENGTH_DESC', 'Anzahl der Zeichen nachdem eine Artikelnummer abgeschnitten wird. Bitte beachten, dass zu Lange Artikelnummer das Layout der PDF zerst&ouml;ren k&ouml;nnen.');
define('PDF_UPDATE_STATUS_TITLE', 'Bestellstatus aktualisieren');
define('PDF_UPDATE_STATUS_DESC', 'Bestellstatus wird nach dem Mailversand der PDF automatisch aktualisiert.');
define('PDF_USE_ORDERID_SUFFIX_TITLE', 'Rechnungsnummer Suffix');
define('PDF_USE_ORDERID_SUFFIX_DESC', 'Suffix f&uuml;r die Rechnungsnummer, falls die Bestellnummer als Rechnungsnummer verwendet wird.');
define('PDF_STATUS_SEND_TITLE', 'Rechnung bei Umstellung auf Bestellstatus versenden');
define('PDF_STATUS_SEND_DESC', 'Rechnung automatisch versenden bei Bestellstatus unten');
define('PDF_STATUS_SEND_ID_TITLE', 'Sende Rechnungs-PDF bei Bestellstatus-ID');
define('PDF_STATUS_SEND_ID_DESC', 'Bei Umstellung auf diese order status ID wird die Rechnung verschickt.<br />(wenn "Rechnung bei Umstellung auf Bestellstatus versenden" auf "JA" steht)');

define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_TITLE', 'Lieferschein nur an Logistiker/Kommissionierer senden');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_DESC', 'Wenn Sie diese Funktion aktivieren werden der Lieferschein nicht an den Kunden sondern ausschließlich an die unten angegebene Mail gesendet.');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME_TITLE', 'Logistiker Name');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME_DESC', 'Geben Sie hier den Namen des Logistikers ein, der den Lieferschein erhält.<br />(Nur wenn "Lieferschein nur an Logistiker/Kommissionierer senden" auf "Ja" steht.)');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL_TITLE', 'Logistikers E-Mail');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL_DESC', 'Geben Sie hier die E-Mail-Addresse des Logistikers ein, der den Lieferschein erhält.<br />(Nur wenn "Lieferschein nur an Logistiker/Kommissionierer senden" auf "Ja" steht.)');

define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_TITLE', 'N&auml;chste Rechnungsnummer');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_DESC', '<span style="color:#c00;">Nur mit Bedacht &auml;ndern, die Rechnungsnummer wird fortlaufend von System vergeben !<br />Wenn "Bestellnummer als Rechnungsnummer" auf "Ja" steht hat das Ändern selbstverständlich keine Wirkung.</span>');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT_TITLE', 'Rechnungsnummer Format');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT_DESC', 'Aufbauschema Rechn.Nr.: {n}=laufende Nummer, {d}=Tag, {m}=Monat, {y}=Jahr,<br />z.B. "100{n}-{d}-{m}-{y}" ergibt "10099-28-02-2007"');
define('PDF_COMMENT_ON_PACKING_SLIP_TITLE', 'Kommentar des Kunden auf Lieferschein ?');
define('PDF_COMMENT_ON_PACKING_SLIP_DESC', 'Den Kommentar den der Kunde bei Bestellung angegeben hat auf den Lieferschein drucken ?');

//BOC - Innergemeinschaftliche Lieferungen
define('PDF_BILL_EU_CUSTOMERS_GROUP_ID_TITLE','Kundengruppe EU H&auml;ndler:');
define('PDF_BILL_EU_CUSTOMERS_GROUP_ID_DESC','<b>Kundengruppen ID (cID)</b> der EU H&auml;ndler - f&uuml;r diese Kunden wird in der Rechnung der Steuerfreihinweis nach &sect; 4 Nr. 1 b Umsatzsteuergesetz (UStG) hinzugef&uuml;gt!<br /><b>Mehrere Eintr&auml;ge mit Komma trennen!</b><br />Die Kundengruppen-ID finden Sie in der Browserzeile nach <strong>cID=</strong> wenn Sie die Kundengruppen editieren.');
//EOC - Innergemeinschaftliche Lieferungen

define('TABLE_HEADING_BILL_NR', 'Rechnungsnummer');
define('PDF_BILL_NR_INFO2', 'Bitte den Auftrag öffnen zum bearbeiten,<br />und unten am Ende die neue Rechnungsnummer eingeben!');
//EOC PDFBill NEXT

//BOC new lang constants for guset status ids (group 17 (= Zusatzmodule)), noRiddle
define('GUEST_STATUS_IDS_TITLE', 'Gast-Status-IDs');
define('GUEST_STATUS_IDS_DESC', 'Alle Gast-Kundengruppen-IDs komma-separiert eintragen.');
//EOC new lang constants for guset status ids (group 17 (= Zusatzmodule)), noRiddle
//BOC new lang constant for account success content, noRiddle
define('NR_ACCOUNT_SUCCESS_CONTENT_TITLE', 'Content für erfolgreiche Kontoerstellung');
define('NR_ACCOUNT_SUCCESS_CONTENT_DESC', 'Hier coID des Contents eintragen welcher den Text für die Info-Seite nach erfolreicher Kontoerstellung beinhaltet.');
//EOC new lang constant for account success content, noRiddle

//BOC yandex heat map, noRiddle
define('ACTIVATE_YANDEX_HEATMAP_TITLE', 'Yandex Heat-Map aktivieren ?');
define('ACTIVATE_YANDEX_HEATMAP_DESC', 'Zur Verwendung mu&szlig; die Heat-Map in <i>/templates/DEIN_TEMPLATE/javascript/general_bottom.js.php</i> integriert sein.');
//EOC yandex heat map, noRiddle
?>
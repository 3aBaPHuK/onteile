<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_WISHLIST_SYSTEM_TEXT_TITLE', 'bloc de notas');
define('MODULE_WISHLIST_SYSTEM_TEXT_DESCRIPTION', 'Con el bloc de notas, hay funciones disponibles para almacenar artículos.');
define('MODULE_WISHLIST_SYSTEM_STATUS_TITLE', '¿Activar módulo?');
define('MODULE_WISHLIST_SYSTEM_STATUS_DESC', 'Activar bloc de notas');
?>

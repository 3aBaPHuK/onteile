<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_EASYMARKETING_TEXT_DESCRIPTION', 'Module - Easymarketing.de');
define('MODULE_EASYMARKETING_TEXT_TITLE', 'Easymarketing.de');
define('MODULE_EASYMARKETING_STATUS_TITLE', 'Statut');
define('MODULE_EASYMARKETING_STATUS_DESC', 'Statut de module') ;
define('MODULE_EASYMARKETING_SHOP_TOKEN_TITLE', 'Boutique Token') ;
define('MODULE_EASYMARKETING_SHOP_TOKEN_DESC', 'Il faut le déposer chez easymarketing.<br/><br/><b>Remarque:</b> Il faut que le Token contient 32 signes. Si ce n\'est pas le cas, il sera généré automatiquement.' );
define('MODULE_EASYMARKETING_ACCESS_TOKEN_TITLE', 'Accès Token') ;
define('MODULE_EASYMARKETING_ACCESS_TOKEN_DESC', 'Vous recevez le Token de la part de Easymarketing.');
define('MODULE_EASYMARKETING_CONDITION_DEFAULT_TITLE', 'État de l\'article') ;
define('MODULE_EASYMARKETING_CONDITION_DEFAULT_DESC', 'Veuillez indiquer l\'état de l\'article.');
define('MODULE_EASYMARKETING_AVAILIBILLITY_STOCK_0_TITLE', 'Disponibilité - stock <1') ;
define('MODULE_EASYMARKETING_AVAILIBILLITY_STOCK_0_DESC', 'Veillez indiquer la disponibilité des articles qui ont un stock négatif ou un stock de 0');
define('MODULE_EASYMARKETING_AVAILIBILLITY_STOCK_1_TITLE', 'Disponibilité - stock > 0') ;
define('MODULE_EASYMARKETING_AVAILIBILLITY_STOCK_1_DESC', 'Veuillez indiquer la disponibilité des articles qui ont un stock positif');

?>

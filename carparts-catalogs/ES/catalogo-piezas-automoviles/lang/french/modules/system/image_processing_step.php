<?php
/* -----------------------------------------------------------------------------------------
   $Id: image_processing_step.php 2992 2012-06-07 16:59:49Z web28 $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com
   (c) 2003	 nextcommerce (invoice.php,v 1.6 2003/08/24); www.nextcommerce.org
   (c) 2006 XT-Commerce (image_processing_step.php 950 2005-05-14; www.xt-commerce.com
   --------------------------------------------------------------
   Contribution
   image_processing_step (step-by-step Variante B) by INSEH 2008-03-26

   new javascript reload / only missing image/ max images  by web28 2011-03-17

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_STEP_IMAGE_PROCESS_TEXT_DESCRIPTION', 'Toutes les images dans les listes seront<br /><br />
/images/product_images/popup_images/<br />
/images/product_images/info_images/<br />
/images/product_images/thumbnail_images/ <br /> <br /> recréées.<br /> <br />
Pour cela, le script traite seulement un nombre limité de %s images et appelle soi-même après de nouveau.<br /> <br />');
define('MODULE_STEP_IMAGE_PROCESS_TEXT_TITLE', 'Imageprocessing - Images de produit');
define('MODULE_STEP_IMAGE_PROCESS_STATUS_DESC', 'Statut de module');
define('MODULE_STEP_IMAGE_PROCESS_STATUS_TITLE', 'Statut');
define('IMAGE_EXPORT', 'Appuyer sur Start pour démarrer le traitement en lot. Ce processus peut prendre quelques temps - ne pas interrompre !');
define('IMAGE_EXPORT_TYPE', '<hr noshade><strong>Traitement en lot :</strong>');

define('IMAGE_STEP_INFO', 'Images créées : ');
define('IMAGE_STEP_INFO_READY', ' - Fini !');
define('TEXT_MAX_IMAGES', 'Nombre maximum d\'images par recharge de la page');
define('TEXT_ONLY_MISSING_IMAGES', 'Créer images manquantes uniquement');
define('MODULE_STEP_READY_STYLE_TEXT', '<div class="ready_info">%s</div>');
define('MODULE_STEP_READY_STYLE_BACK', MODULE_STEP_READY_STYLE_TEXT);
define('TEXT_LOWER_FILE_EXT', 'Convertir suffixe du fichier en minuscules Ex. : <b> JPG -> jpg</b>');
define('IMAGE_COUNT_INFO', 'Nombre d\'images en %s: %s pièces. ');

define('TEXT_LOGFILE', 'Activer logging, utile pour la recherche d\'erreurs. Le fichier journal sera enregistré dans le dossier /log dans le registre principal.');

?>

<?php
/* -----------------------------------------------------------------------------------------
* $Id: PDFBill_Next.php 4242 2016-01-28 13:29:09Z Ralph_84 $
* 
* Modified - community made shopping
* http://www.modified-shop.org

* Copyright (c) 2009 - 2016 Modified
   
* Installer PDFBill Next Modified 2.0 r9281
* erstellt von Ralph_84
******************************************************************
* contibutions:
* completely reworked for modified 2.0.X by noRiddle (c) 10-2017
******************************************************************
* -----------------------------------------------------------------------------------------
* Released under the GNU General Public License
* ---------------------------------------------------------------------------------------*/

defined('_VALID_XTC') or die('Direct Access to this location is not allowed.');

class pdfbill {
    var $code, $title, $description, $enabled;

    function __construct() {
        global $current_page;
        $this->code = 'pdfbill';
        $this->title = MODULE_PDFBILL_TEXT_TITLE;
        $this->description = MODULE_PDFBILL_TEXT_DESCRIPTION;
            
        if (defined('MODULE_PDF_BILL_STATUS')) {
            $this->enabled = (MODULE_PDF_BILL_STATUS == 'True') ? true : false;
        } else {
            $this->enabled = false;
        }
            
        $this->module_filename = $current_page;
        $this->properties = array('process_key' => false);
    }

    function process($file) {
        //do nothing
    }

    function display() {
        $this->do_modul_updates();
        $text2 = MODULE_PDFBILL_STATUS_TITLE;
        $text .= '<br /><div align="center">' . xtc_button(BUTTON_SAVE) . xtc_button_link(BUTTON_CANCEL, xtc_href_link(FILENAME_MODULE_EXPORT, 'set=' . $_GET['set'] . '&module='.$this->code) . "</div>");

        return array('text' => $text);
    }

    function check() {
        if (!isset($this->_check)) {
            $check_query = xtc_db_query("SELECT `configuration_value` 
                                           FROM " . TABLE_CONFIGURATION . " 
                                          WHERE `configuration_key` = 'MODULE_PDF_BILL_STATUS'");
            $this->_check = xtc_db_num_rows($check_query);
        }

        return $this->_check;
    }

    function install() {
        //BOC add fields only if they do not exist, noRiddle
        $orders_fields_arr = array();
        $orders_field_query = xtc_db_query("SHOW COLUMNS FROM ".TABLE_ORDERS." LIKE 'ibn_bill%'");
        while($orders_fields = xtc_db_fetch_array($orders_field_query)) {
            $orders_fields_arr[] = $orders_fields['Field'];
        }
        if(!in_array('ibn_billnr', $orders_fields_arr)) {
            xtc_db_query("ALTER TABLE " . TABLE_ORDERS . " ADD ibn_billnr VARCHAR(32);");
        }
        if(!in_array('ibn_billdate', $orders_fields_arr)) {
            xtc_db_query("ALTER TABLE " . TABLE_ORDERS . " ADD `ibn_billdate` DATE NOT NULL;");
        }
        //EOC add fields only if they do not exist, noRiddle

        xtc_db_query("ALTER TABLE " . TABLE_ADMIN_ACCESS . " ADD pdfbill_del INT( 1 ) NOT NULL ;");
        xtc_db_query("UPDATE " . TABLE_ADMIN_ACCESS . " SET pdfbill_del = '1' WHERE customers_id = '1';");			
        xtc_db_query("ALTER TABLE " . TABLE_ADMIN_ACCESS . " ADD print_order_pdf INT( 1 ) NOT NULL ;");
        xtc_db_query("UPDATE " . TABLE_ADMIN_ACCESS . " SET print_order_pdf = '1' WHERE customers_id = '1';");
        xtc_db_query("ALTER TABLE " . TABLE_ADMIN_ACCESS . " ADD print_packingslip_pdf INT( 1 ) NOT NULL ;");
        xtc_db_query("UPDATE " . TABLE_ADMIN_ACCESS . " SET print_packingslip_pdf = '1' WHERE customers_id = '1';");
        xtc_db_query("ALTER TABLE " . TABLE_ADMIN_ACCESS . " ADD pdf_bill_languages INT( 1 ) NOT NULL ;");
        xtc_db_query("UPDATE " . TABLE_ADMIN_ACCESS . " SET pdf_bill_languages = '1' WHERE customers_id = '1';");
        //BOC don't determine configuration_group_id, correct entry in field visible | MODULE_INVOICE_NUMBER not needed, noRiddle
        //xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES (99, 'PDFBill Configuration', 'PDFBill Overall Configuration', NULL, 99);");
        xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (`configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES ('PDFBill Configuration', 'PDFBill Overall Configuration', NULL, 1);");
        $pdfbill_gr_insert_id = xtc_db_insert_id();
        //xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, date_added) values ('MODULE_INVOICE_NUMBER_STATUS', 'True',  '6', '1', now())");
        //EOC don't determine configuration_group_id, correct entry in field visible | MODULE_INVOICE_NUMBER not needed, noRiddle
        xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, date_added) values ('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR', '1',  '".$pdfbill_gr_insert_id."', '2', now())");
        xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, date_added) values ('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT', '{n}-{d}-{m}-{y}',  '".$pdfbill_gr_insert_id."', '6', now())");			
        xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " (`configuration_key`, `configuration_value`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES
        ('MODULE_PDF_BILL_STATUS', 'True', '".$pdfbill_gr_insert_id."', 1, NULL, now(), NULL, 'xtc_cfg_select_option(array(\'True\', \'False\'),'),
        ('PDF_USE_ORDERID', 'true', '".$pdfbill_gr_insert_id."', 3, NULL, now(), NULL, 'xtc_cfg_select_option(array(\'true\', \'false\'),'),
        ('PDF_USE_ORDERID_PREFIX', '', '".$pdfbill_gr_insert_id."', 4, NULL, now(), NULL, NULL),
        ('PDF_USE_ORDERID_SUFFIX', '', '".$pdfbill_gr_insert_id."', 5, NULL, now(), NULL, NULL),
        ('PDF_SEND_ORDER', 'false', '".$pdfbill_gr_insert_id."', 7, NULL, now(), NULL, 'xtc_cfg_select_option(array(\'true\', \'false\'),'),
        ('PDF_STATUS_SEND', 'false', '".$pdfbill_gr_insert_id."', 8, NULL, now(), NULL, 'xtc_cfg_select_option(array(\'true\', \'false\'),'),
        ('PDF_STATUS_SEND_ID', '1', '".$pdfbill_gr_insert_id."', 9, NULL, now(), NULL, NULL),
        ('PDF_UPDATE_STATUS', 'false', '".$pdfbill_gr_insert_id."', 10, NULL, now(), NULL, 'xtc_cfg_select_option(array(\'true\', \'false\'),'),
        ('PDF_STATUS_ID_BILL', '1', '".$pdfbill_gr_insert_id."', 11, NULL, now(), NULL, NULL),
        ('PDF_STATUS_ID_SLIP', '1', '".$pdfbill_gr_insert_id."', 12, NULL, now(), NULL, NULL),
        ('PDF_FILENAME', 'Rechnung_{bill}', '".$pdfbill_gr_insert_id."', 13, NULL, now(), NULL, NULL),
        ('PDF_FILENAME_SLIP', 'Lieferschein_{bill}', '".$pdfbill_gr_insert_id."', 14, NULL, now(), NULL, NULL),
        ('PDF_MAIL_SUBJECT', 'DE::Ihre Rechnung||EN::Your invoice', '".$pdfbill_gr_insert_id."', 15, NULL, now(), NULL, 'xtc_cfg_input_email_language;PDF_MAIL_SUBJECT'),
        ('PDF_MAIL_SUBJECT_SLIP', 'DE::Ihr Lieferschein||EN::Your packing slip', '".$pdfbill_gr_insert_id."', 16, NULL, now(), NULL, 'xtc_cfg_input_email_language;PDF_MAIL_SUBJECT_SLIP'),
        ('PDF_STATUS_COMMENT', 'Rechnung versendet', '".$pdfbill_gr_insert_id."', 17, NULL, now(), NULL, NULL),
        ('PDF_STATUS_COMMENT_SLIP', 'Lieferschein verschickt', '".$pdfbill_gr_insert_id."', 18, NULL, now(), NULL, NULL),
        ('PDF_MAIL_SLIP_ONLY_LOGISTICIAN', 'false', '".$pdfbill_gr_insert_id."', 19, NULL, now(), NULL, 'xtc_cfg_select_option(array(\'true\', \'false\'),'),
        ('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME', '', '".$pdfbill_gr_insert_id."', 20, NULL, now(), NULL, NULL),
        ('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL', '', '".$pdfbill_gr_insert_id."', 21, NULL, now(), NULL, NULL),
        ('PDF_MAIL_SLIP_COPY', '', '".$pdfbill_gr_insert_id."', 22, NULL, now(), NULL, NULL),
        ('PDF_MAIL_BILL_COPY', '', '".$pdfbill_gr_insert_id."', 23, NULL, now(), NULL, NULL),
        ('PDF_BILL_EU_CUSTOMERS_GROUP_ID', '4', '".$pdfbill_gr_insert_id."', 24, NULL, now(), NULL, NULL),
        ('PDF_PRODUCT_MODEL_LENGTH', '7', '".$pdfbill_gr_insert_id."', 25, NULL, now(), NULL, NULL),
        ('PDF_COMMENT_ON_PACKING_SLIP', 'false', '".$pdfbill_gr_insert_id."', 26, NULL, now(), NULL, 'xtc_cfg_select_option(array(\'true\', \'false\'),'),
        ('PDF_USE_CUSTOMER_ID', 'false', '".$pdfbill_gr_insert_id."', 28, NULL, now(), NULL, 'xtc_cfg_select_option(array(\'true\', \'false\'),');");
    }

    function remove() {
        xtc_db_query("DELETE FROM ".TABLE_CONFIGURATION_GROUP." WHERE configuration_group_title = 'PDFBill Configuration'"); //delete entry in configuration_group as well, noRiddle
        xtc_db_query("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key in ('" . implode("', '", $this->keys2()) . "')");
        xtc_db_query("ALTER TABLE " . TABLE_ADMIN_ACCESS . " DROP pdfbill_del");
        xtc_db_query("ALTER TABLE " . TABLE_ADMIN_ACCESS . " DROP print_order_pdf");
        xtc_db_query("ALTER TABLE " . TABLE_ADMIN_ACCESS . " DROP print_packingslip_pdf");
        xtc_db_query("ALTER TABLE " . TABLE_ADMIN_ACCESS . " DROP pdf_bill_languages");
        //xtc_db_query("ALTER TABLE " . TABLE_ORDERS . " DROP `ibn_billnr`;");
        //xtc_db_query("ALTER TABLE " . TABLE_ORDERS . " DROP `ibn_billdate`;");
        //xtc_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key LIKE 'MODULE_INVOICE_NUMBER_%'"); //MODULE_INVOICE_NUMBER not needed, noRiddle
        
        unset($_SESSION['sorting']);
    }

    function keys() {
        $key = array('MODULE_PDF_BILL_STATUS');

      return $key;
    } 

    function keys2() {
        $key = array('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR',
                     'MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT',
                     'MODULE_PDF_BILL_STATUS',
                     'PDF_USE_ORDERID',
                     'PDF_USE_ORDERID_PREFIX',
                     'PDF_USE_ORDERID_SUFFIX',
                     'PDF_SEND_ORDER',
                     'PDF_STATUS_SEND',
                     'PDF_STATUS_SEND_ID',
                     'PDF_UPDATE_STATUS',
                     'PDF_STATUS_ID_BILL',
                     'PDF_STATUS_ID_SLIP',
                     'PDF_FILENAME',
                     'PDF_FILENAME_SLIP',
                     'PDF_MAIL_SUBJECT',
                     'PDF_MAIL_SUBJECT_SLIP',
                     'PDF_STATUS_COMMENT',
                     'PDF_STATUS_COMMENT_SLIP',
                     'PDF_MAIL_SLIP_ONLY_LOGISTICIAN',
                     'PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME',
                     'PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL',
                     'PDF_MAIL_SLIP_COPY',
                     'PDF_MAIL_BILL_COPY',
                     'PDF_BILL_EU_CUSTOMERS_GROUP_ID',
                     'PDF_PRODUCT_MODEL_LENGTH',
                     'PDF_COMMENT_ON_PACKING_SLIP',
                     'PDF_USE_CUSTOMER_ID'
                    );

        return $key;
    }

    function do_modul_updates() {
        //do nothing
    }
}
?>
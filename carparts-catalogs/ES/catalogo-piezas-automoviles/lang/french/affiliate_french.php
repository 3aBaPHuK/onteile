<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_german.php 40 2013-01-08 16:36:44Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_german.php, v 1.12 2003/08/18);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('BOX_INFORMATION_AFFILIATE', 'Programme d\'affiliation');
define('BOX_AFFILIATE_INFO', 'Informations sur le partenaire');
define('BOX_AFFILIATE_SUMMARY', 'Aperçu du compte d\'affiliation');
define('BOX_AFFILIATE_ACCOUNT', 'Éditer le compte d\'affiliation');
define('BOX_AFFILIATE_CLICKRATE', 'Aperçu des clics');
define('BOX_AFFILIATE_PAYMENT', 'Paiements de commission');
define('BOX_AFFILIATE_SALES', 'Aperçu des ventes');
define('BOX_AFFILIATE_BANNERS', 'Bannière');
define('BOX_AFFILIATE_TEXTLINKS', 'Lien du texte');
define('BOX_AFFILIATE_PRODUCTLINK', 'Lien du produit');
define('BOX_AFFILIATE_CONTACT', 'Contact');
define('BOX_AFFILIATE_FAQ', 'FAQ');
define('BOX_AFFILIATE_AGB', 'Conditions de participation');
define('BOX_AFFILIATE_LOGIN', 'Login compte d\'affiliation');
define('BOX_AFFILIATE_LOGOUT', 'Fermer la session');

define('ENTRY_AFFILIATE_ACCEPT_AGB', 'Veuillez confirmer que vous avez lu et êtes d\'accord avec les <a class="iframe" target="_blank" href="%s" target="_blank">Terms d\'Associés & les Conditions</a>.');
define('ENTRY_AFFILIATE_AGB_ERROR', 'Vous devez accepter nos Terms d\'Associés & les Conditions' );
define('ENTRY_AFFILIATE_PAYMENT_CHECK_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_CHECK_ERROR', ' <small><font color="#FF0000">nécessaire</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_PAYPAL_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_PAYPAL_ERROR', ' <small><font color="#FF0000">nécessaire</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_BANK_NAME_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_BANK_NAME_ERROR', ' <small><font color="#FF0000">nécessaire</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NAME_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NAME_ERROR', ' <small><font color="#FF0000">nécessaire</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NUMBER_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NUMBER_ERROR', ' <small><font color="#FF0000">nécessaire</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_BANK_BRANCH_NUMBER_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_BANK_BRANCH_NUMBER_ERROR', ' <small><font color="#FF0000">nécessaire</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_BANK_SWIFT_CODE_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_BANK_SWIFT_CODE_ERROR', ' <small><font color="#FF0000">nécessaire</font></small>');
define('ENTRY_AFFILIATE_COMPANY_TEXT', '');
define('ENTRY_AFFILIATE_COMPANY_ERROR', ' <small><font color="#FF0000">nécessaire</font></small>');
define('ENTRY_AFFILIATE_COMPANY_TAXID_TEXT', '');
define('ENTRY_AFFILIATE_COMPANY_TAXID_ERROR', ' <small><font color="#FF0000">nécessaire</font></small>');
define('ENTRY_AFFILIATE_HOMEPAGE_TEXT', '*'); //' <small><font color="#000000"> (http://)</font></small>');
define('ENTRY_AFFILIATE_HOMEPAGE_ERROR', ' <small><font color="#FF0000">nécessaire (http://)</font></small>');
define('PASSWORD_HIDDEN', 'Votre mot de passe a été caché.');

define('TEXT_AFFILIATE_PERIOD', 'Période: ');
define('TEXT_AFFILIATE_STATUS', 'Statut: ');
define('TEXT_AFFILIATE_LEVEL', 'Niveau: ');
define('TEXT_AFFILIATE_ALL_PERIODS', 'Toutes les périodes');
define('TEXT_AFFILIATE_ALL_STATUS', 'Tous les statuts');
define('TEXT_AFFILIATE_ALL_LEVELS', 'Tous les niveaus');
define('TEXT_AFFILIATE_PERSONAL_LEVEL', 'Personnel');
define('TEXT_AFFILIATE_LEVEL_SUFFIX', 'Niveau ');
define('TEXT_AFFILIATE_NAME', 'Nom de la bannière');
define('TEXT_AFFILIATE_INFO', 'Veuillez copier le code HTML et l\'insérer dans la page Web.' );
define('TEXT_DISPLAY_NUMBER_OF_CLICKS', 'Affiche <b>%d</b> à <b>%d</b> (de <b>%d</b> clics au total)');
define('TEXT_DISPLAY_NUMBER_OF_SALES', 'Affiche <b>%d</b> à <b>%d</b> (de <b>%d</b> ventes au total)');
define('TEXT_DISPLAY_NUMBER_OF_PAYMENTS', 'Affiche <b>%d</b> à <b>%d</b> (de <b>%d</b> paiements au total)');
define('TEXT_DELETED_ORDER_BY_ADMIN', 'Supprimé (Admin)');
define('TEXT_AFFILIATE_PERSONAL_LEVEL_SHORT', 'Pers.');

define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - Nouveau mot de passe pour le programme d\'affiliation');

define('MAIL_AFFILIATE_SUBJECT', 'Bienvenue au programme d\'affiliaiton de' . STORE_NAME);

define('EMAIL_SUBJECT', 'Programme d\'affiliation');

define('NAVBAR_TITLE', 'Programme d\'affiliation');
define('NAVBAR_TITLE_AFFILIATE', 'Login');
define('NAVBAR_TITLE_BANNERS', 'Bannière');
define('NAVBAR_TITLE_CLICKS', 'Clics');
define('NAVBAR_TITLE_CONTACT', 'Contact');
define('NAVBAR_TITLE_DETAILS', 'Modifiér les données');
define('NAVBAR_TITLE_DETAILS_OK', 'Données modifiées');
define('NAVBAR_TITLE_FAQ', 'Questions');
define('NAVBAR_TITLE_INFO', 'Informations');
define('NAVBAR_TITLE_LOGOUT', 'Logout');
define('NAVBAR_TITLE_PASSWORD_FORGOTTEN', 'Mot de passe oublié?');
define('NAVBAR_TITLE_PAYMENT', 'Paiement');
define('NAVBAR_TITLE_SALES', 'Ventes');
define('NAVBAR_TITLE_SIGNUP', 'S\'inscrire');
define('NAVBAR_TITLESIGNUP_OK', 'Inscrit');
define('NAVBAR_TITLE_SUMMARY', 'Aperçu');
define('NAVBAR_TITLE_TERMS', 'Terms et Conditions');

define('IMAGE_BANNERS', 'Bannière d\'affiliation');
define('IMAGE_CLICKTHROUGHS', 'Rapport des Clics');
define('IMAGE_SALES', 'Rapport des Ventes');
?>

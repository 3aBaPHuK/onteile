<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_discount.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommercebased on original files from OSCommerce CVS 2.2 2002/08/28 02:14:35 www.oscommerce.com
   (c) 2003	 nextcommerce (ot_discount.php,v 1.5 2003/08/13); www.nextcommerce.org 

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_DISCOUNT_TITLE', 'Réduction');
define('MODULE_ORDER_TOTAL_DISCOUNT_DESCRIPTION', 'Réduction sur la commande');
  
define('MODULE_ORDER_TOTAL_DISCOUNT_STATUS_TITLE', 'Réduction');
define('MODULE_ORDER_TOTAL_DISCOUNT_STATUS_DESC', 'Afficher la réduction?');
  
define('MODULE_ORDER_TOTAL_DISCOUNT_SORT_ORDER_TITLE', 'Ordre de triage');
define('MODULE_ORDER_TOTAL_DISCOUNT_SORT_ORDER_DESC', 'Ordre de l\'affichage');
?>

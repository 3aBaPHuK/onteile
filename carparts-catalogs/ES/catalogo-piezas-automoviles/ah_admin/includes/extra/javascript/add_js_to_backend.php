<?php
/********************************************************
* file: add_js_to_backend.php
* use: add necessary javascript in backend files
* (c) noRiddle 10-2017
********************************************************/

//echo '<pre>'.$PHP_SELF.'</pre>';

//BOC multi_order_status based on module by XTC-DELUXE.DE, noRiddle
if(strpos($PHP_SELF, 'orders.php') !== false  && !isset($_GET['action'])) {
    if (xtc_not_null($_GET['print_oID'])) {
        if ($_GET['print_invoice'] == 'on') {
            echo '<script>var invoice'.$i.' = window.open(\''.xtc_href_link(FILENAME_PRINT_ORDER,'print_oID='.$_GET['print_oID']).'\', \'invoice\', \'toolbar=0, width=640, height=600\')</script>';
        }
        if ($_GET['print_packingslip'] == 'on') {
            echo '<script>var packingslip'.$i.' = window.open(\''.xtc_href_link(FILENAME_PRINT_PACKINGSLIP,'print_oID='.$_GET['print_oID']).'\', \'packingslip\', \'toolbar=0, width=640, height=600\')</script>';
        }
    }
?>
<script type="text/javascript">
/* <![CDATA[ */
//BOC mark row with selected orders in multi status
$(function() {
    var $inpt_multi_stat = $('input[name="change_status_oID[]"]'),
        $inpt_checkall = $('input[name="checkAll"]')
    
    //select all edit change_status_oID with one click
    $inpt_checkall.click(function() {
        $inpt_multi_stat.prop('checked', $(this).is(':checked')).change();
    });
    
    //mark checked orders for multi status
    $inpt_multi_stat.each(function() {
        var $oid = $(this).val();

        $(this).change(function() {
            if($(this).prop('checked')) {
                //alert($oid);
                $('.multi-stat-row-'+$oid).addClass('multi-sel');
            } else {
                $('.multi-stat-row-'+$oid).removeClass('multi-sel');
            }
        });
        
    });
});
//EOC mark row with selected orders in multi status
/*]]>*/
</script>

<?php
}
//EOC multi_order_status based on module by XTC-DELUXE.DE, noRiddle

//BOC dealer price
if(strpos($PHP_SELF, 'categories.php') !== false && (isset($_GET['action']) && $_GET['action'] == 'new_product')) {
?>
<script type="text/javascript">
/* <![CDATA[ */
$(function() {
    var $tblinpttr = $('.div_header + .tableInput > tbody > tr'),
        tr_len = $tblinpttr.length,
        $prdndp = $('.prod-new-deal-price');

    $prdndp.parent().unwrap();
    $prdndp.unwrap();
    $prdndp.insertBefore($tblinpttr.eq(1)); //tr_len - 3
});
/*]]>*/
</script>
<?php
}
//EOC dealer price
?>
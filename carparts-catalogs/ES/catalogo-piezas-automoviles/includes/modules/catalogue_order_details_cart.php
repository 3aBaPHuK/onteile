<?php
/********************************************
* file: catalogue_order_details_cart.php
* use: order details in cart for catalogue
* (c) noRiddle 10-2016
********************************************/

include_once 'nr_catalogue_files/config.php';

$module_smarty = new Smarty;

$module_smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');

// include needed functions
require_once (DIR_FS_INC.'xtc_format_price.inc.php');

$module_content = array ();

//echo '<br /><br /><pre>'.print_r($_SESSION['nr_ctlg_brands'], true).'</pre>';
//echo '<br><br><br><pre>'.print_r($catalogue_products, true).'</pre>';

foreach($catalogue_products as $brand => $products_model) {
    for ($i = 0, $n = sizeof($products_model); $i < $n; $i ++) {
      //show 'delete button' in shopping cart
      $del_button = '<a href="'
              . xtc_href_link(FILENAME_CATALOGUE_SHOPPING_CART, 'action=catalogue_remove_product&brd='.$products_model[$i]['brand'].'&mod=' . $products_model[$i]['model'], 'NONSSL') // web28 - 2010-09-20 - change SSL -> NONSSL
              . '">' . xtc_image_button('cart_del.gif', IMAGE_BUTTON_DELETE) . '</a>';
      //show 'delete link' in shopping cart
      $del_link = '<a href="'
              . xtc_href_link(FILENAME_CATALOGUE_SHOPPING_CART, 'action=catalogue_remove_product&brd='.$products_model[$i]['brand'].'&mod=' . $products_model[$i]['model'], 'NONSSL') // web28 - 2010-09-20 - change SSL -> NONSSL
              . '">' . IMAGE_BUTTON_DELETE . '</a>';

      $module_content[$brand][$i] = array('PRODUCTS_BRAND' => $products_model[$i]['brand'],
                                          'PRODUCTS_NAME' => $products_model[$i]['name'],
                                          'PRODUCTS_QTY' => xtc_draw_input_field('cart_quantity[]', $products_model[$i]['quantity'], 'size="2"').
                                                            xtc_draw_hidden_field('products_model[]', $products_model[$i]['model']).
                                                            xtc_draw_hidden_field('products_name[]', $products_model[$i]['name']).
                                                            xtc_draw_hidden_field('products_brand[]', $products_model[$i]['brand']).
                                                            xtc_draw_hidden_field('lax_brand[]', $products_model[$i]['lax_brand']).
                                                            xtc_draw_hidden_field('products_price[]', $products_model[$i]['price']).
                                                            xtc_draw_hidden_field('products_final_price[]', $products_model[$i]['final_price']).
                                                            xtc_draw_hidden_field('products_tax_class_id[]', $products_model[$i]['tax_class_id']).
                                                            xtc_draw_hidden_field('products_tax[]', $products_model[$i]['tax']),
                                          'PRODUCTS_MODEL' => $products_model[$i]['model'],
                                          'PRODUCTS_TAX' => number_format($products_model[$i]['tax'], TAX_DECIMAL_PLACES),
                                          'BOX_DELETE' => xtc_draw_checkbox_field('cart_delete[]', $products_model[$i]['model']), 
                                          'BUTTON_DELETE' => $del_button,
                                          'LINK_DELETE' => $del_link,                  
                                          //'PRODUCTS_PRICE' => $xtPrice->xtcFormat($products_model[$i]['price'] * $products_model[$i]['quantity'], true, $products_model[$i]['tax_class_id'], true), 
                                          //'PRODUCTS_PRICE' => $xtPrice->xtcFormat($products_model[$i]['final_price'], true, $products_model[$i]['tax_class_id'], true),
                                          'PRODUCTS_PRICE' => $xtPrice->xtcFormat($products_model[$i]['final_price'], true), 
                                          //'PRODUCTS_SINGLE_PRICE' => $xtPrice->xtcFormat($products_model[$i]['price'], true, $products_model[$i]['tax_class_id'], true));
                                          'PRODUCTS_SINGLE_PRICE' => $xtPrice->xtcFormat($products_model[$i]['price'], true));
    
        if($i == 0) {
            //$brand_pic[$brand] = 'nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/75x75/'.Config::$nr_icon_arr[$products_model[$i]['lax_brand']];
            $brand_pic[$brand] = 'nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/75x75/'.$_SESSION['nr_ctlg_brands'][$products_model[$i]['lax_brand']]['brand_icon'];
        }
    }

    $discount = 0;
    $total_content[$brand] = '';
    $ust_content[$brand] = '';
    $total = $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->show_total($brand);
    if ($_SESSION['customers_status']['customers_status_ot_discount_flag'] == '1' && $_SESSION['customers_status']['customers_status_ot_discount'] != '0.00') {
        if ($_SESSION['customers_status']['customers_status_show_price_tax'] == 0 && $_SESSION['customers_status']['customers_status_add_tax_ot'] == 1) {
            $price = $total - $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->show_tax($brand, false);
        } else {
            $price = $total;
        }
        $discount = $xtPrice->xtcGetDC($price, $_SESSION['customers_status']['customers_status_ot_discount']);
        $total_content[$brand] = $_SESSION['customers_status']['customers_status_ot_discount'].' % '.SUB_TITLE_OT_DISCOUNT.' -'.xtc_format_price($discount, $price_special = 1, $calculate_currencies = false).'<br />';
    }

    if ($_SESSION['customers_status']['customers_status_show_price'] == '1') {
        if ($_SESSION['customers_status']['customers_status_show_price_tax'] == 0 && $_SESSION['customers_status']['customers_status_add_tax_ot'] == 0) $total-=$discount;
        if ($_SESSION['customers_status']['customers_status_show_price_tax'] == 0 && $_SESSION['customers_status']['customers_status_add_tax_ot'] == 1) $total-=$discount;
        if ($_SESSION['customers_status']['customers_status_show_price_tax'] == 1) $total-=$discount;
        $total_content[$brand] .= SUB_TITLE_SUB_TOTAL.' '.$xtPrice->xtcFormat($total, true).'<br />';
    } else {
        $total_content[$brand] .= NOT_ALLOWED_TO_SEE_PRICES.'<br />';
    }

    if (SHOW_SHIPPING == 'true') {
        $module_smarty->assign('SHIPPING_INFO', $main->getShippingLink()); //web28 -2012-09-29 - use main function
    }
    if ($_SESSION['customers_status']['customers_status_show_price'] == '1') {
        $ust_content[$brand] = $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->show_tax($brand);
    }
    
    //BOC brand pic
    
    //EOC brand pic
    
    //BOC forms and buttons
    $form_action[$brand] = '';
    $form_end[$brand] = '';
    $button_reload[$brand] = '';
    $link_compose[$brand] = '';
    $link_str[$brand] = '';
    $remotelink_str[$brand] = '';
    
    //$link_compose[$brand] = '//www.'.$domain.'/'; //$domain defined in central config.php
    $link_compose[$brand] .= $remote_url[$cat_env]['domain'].'/';
    
    //echo '<pre>$_SESSION[catalogue_cart]->count_contents('.$brand.'): '.$_SESSION['catalogue_cart'.$_SESSION['shopsess']]->count_contents($brand).'</pre>';
    if ($_SESSION['catalogue_cart'.$_SESSION['shopsess']]->count_contents($brand) > 0) {
        $form_action[$brand] .= xtc_draw_form('catalogue_cart_quantity', xtc_href_link('catalogue_shopping_cart.php', 'action=catalogue_update_product', $request_type));
        $form_end[$brand] .= '</form>';
        $butt_rel_text = sprintf(IMAGE_BUTTON_UPDATE_CAT_CART, ucwords($brand));
        $button_reload[$brand] .= xtc_image_submit('button_update_cart.gif', $butt_rel_text);

        $link_compose[$brand] .= $brand.$shop_suffix.'?action=custom_cart&type=model&cfc=1&p_id='; //added parameter to distinguish between coming from catalogue or with help of shopping cart link, noRiddle
        for($j = 0, $n = sizeof($products_model); $j < $n; $j ++) {
            $link_compose[$brand] .= $products_model[$j]['model'].'-'.$products_model[$j]['quantity'].'~';
        }
        $link_compose[$brand] = rtrim($link_compose[$brand], '~');
        $ext_link[$brand] = $link_compose[$brand]; //xtc_href_link($link_compose[$brand], '', 'SSL');
        //$link_str[$brand] = '<a href="'.$ext_link[$brand].'" target="_blank">'.xtc_image_button('button_catalogue_checkout.gif', IMAGE_BUTTON_CATALOGUE_CHECKOUT).'</a>';
        $link_str[$brand] .= '<a class="rem-gen" data-shp="'.$brand.'" data-lnk="'.$ext_link[$brand].'"><span class="cssButton cssButtonColor2" title="'.IMAGE_BUTTON_CATALOGUE_CHECKOUT.'"><i class="fa fa-chevron-circle-right"></i><span class="cssButtonText">'.IMAGE_BUTTON_CATALOGUE_CHECKOUT.'</span></span></a>';
        $remotelink_str[$brand] .= '<a href="" id="click-'.$brand.'"></a>'; // style="visibility:hidden;"></a>';
    }
    //EOC forms and buttons
}

//echo '<pre>$_SESSION[catalogue_cart]: '.print_r($_SESSION['catalogue_cart'.$_SESSION['shopsess']], true).'</pre>';
//echo '<pre>$module_content: '.print_r($module_content, true).'</pre>';
//echo '<pre>$total_content: '.print_r($total_content, true).'</pre>';
//echo '<pre>$ust_content: '.print_r($ust_content, true).'</pre>';

    //BOC forms and buttons
    $module_smarty->assign('FORM_ACTION', $form_action);
    $module_smarty->assign('FORM_END', $form_end);
    $module_smarty->assign('BUTTON_RELOAD', $button_reload);
    if(in_array($_SESSION['customer_country_id'], $nr_activated_states)) { //customer can only buy if he is in shops countries array, noRiddle
        $module_smarty->assign('BUTTON_CHECKOUT', $link_str);
        $module_smarty->assign('REMOTEBUTTON_CHECKOUT', $remotelink_str);
    }
    $module_smarty->assign('BRAND_PIC', $brand_pic);
    //EOC forms and buttons
    
    if($ust_content) {
        $module_smarty->assign('UST_CONTENT', $ust_content);
    }
    
    $module_smarty->assign('TOTAL_CONTENT', $total_content);
    
    $module_smarty->assign('language', $_SESSION['language']);
    $module_smarty->assign('module_content', $module_content);

    $module_smarty->caching = 0;
    $module = $module_smarty->fetch(CURRENT_TEMPLATE.'/module/catalogue_order_details.html');

    $smarty->assign('MODULE_catalogue_order_details', $module);
?>
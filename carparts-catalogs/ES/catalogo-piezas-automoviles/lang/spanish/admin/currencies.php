<?php
/* --------------------------------------------------------------
   $Id: currencies.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(currencies.php,v 1.15 2003/05/02); www.oscommerce.com 
   (c) 2003	 nextcommerce (currencies.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/
   
define('HEADING_TITLE', 'Monedas');

define('TABLE_HEADING_CURRENCY_NAME', 'Moneda');
define('TABLE_HEADING_CURRENCY_CODES', 'Código de moneda');
define('TABLE_HEADING_CURRENCY_VALUE', 'Valor');
define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_CURRENCY_STATUS', 'Estado');

define('TEXT_INFO_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_INFO_CURRENCY_TITLE', 'Nombre:');
define('TEXT_INFO_CURRENCY_CODE', 'Código de la moneda según la norma ISO 4217:');
define('TEXT_INFO_CURRENCY_SYMBOL_LEFT', 'Símbolo a de la izquierda:');
define('TEXT_INFO_CURRENCY_SYMBOL_RIGHT', 'Símbolo de la derecha:');
define('TEXT_INFO_CURRENCY_DECIMAL_POINT', 'Punto decimal:');
define('TEXT_INFO_CURRENCY_THOUSANDS_POINT', 'Punto mil:');
define('TEXT_INFO_CURRENCY_DECIMAL_PLACES', 'Decimales:');
define('TEXT_INFO_CURRENCY_LAST_UPDATED', 'última modificación:');
define('TEXT_INFO_CURRENCY_VALUE', 'Valor:');
define('TEXT_INFO_CURRENCY_EXAMPLE', 'Ejemplo:');
define('TEXT_INFO_INSERT_INTRO', 'Por favor, introduzca la nueva moneda con todos los datos relevantes.');
define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar esta moneda?');
define('TEXT_INFO_HEADING_NEW_CURRENCY', 'Nueva moneda');
define('TEXT_INFO_HEADING_EDIT_CURRENCY', 'Editar moneda');
define('TEXT_INFO_HEADING_DELETE_CURRENCY', 'Borrar moneda');
define('TEXT_INFO_SET_AS_DEFAULT', TEXT_SET_DEFAULT . ' (se requiere actualización manual de las tasas de cambio.)');
define('TEXT_INFO_CURRENCY_UPDATED', 'La tasa de cambio %s (%s) ha sido actualizada con éxito.');

define('ERROR_REMOVE_DEFAULT_CURRENCY', 'Error: No se debe borrar la moneda por defecto. Por favor, defina una nueva moneda por defecto y repita el proceso.');
define('ERROR_CURRENCY_INVALID', 'Error: La tasa de cambio para %s (%s) no fue actualizada. ¿Es éste un código de moneda válido?');
define('WARNING_PRIMARY_SERVER_FAILED', 'El servicio primario "%s" no pudo determinar la tasa de cambio %s (%s) o no está disponible. Se está intentando de nuevo a través del servicio secundario.');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id: coupon_admin.php 10755 2017-06-02 13:29:46Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(coupon_admin.php,v 1.1.2.5 2003/05/13); www.oscommerce.com
   (c) 2006 XT-Commerce

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c) Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('TOP_BAR_TITLE', 'Statistique');
define('HEADING_TITLE', 'Rabais coupons');
define('HEADING_TITLE_STATUS', 'Statut:');
define('TEXT_CUSTOMER', 'Client');
define('TEXT_COUPON', 'Nom coupon');
define('TEXT_COUPON_ALL', 'Tous les coupons');
define('TEXT_COUPON_ACTIVE', 'Coupons actifs');
define('TEXT_COUPON_INACTIVE', 'Coupons inactifs');
define('TEXT_SUBJECT', 'Objet:');
define('TEXT_FROM', 'de:');
define('TEXT_FREE_SHIPPING', 'Sans frais d\'expédition');
define('TEXT_MESSAGE', 'Message:');
define('TEXT_SELECT_CUSTOMER', 'Choisir client');
define('TEXT_ALL_CUSTOMERS', 'Tous les clients');
define('TEXT_NEWSLETTER_CUSTOMERS', 'Tous les abonnés à la newsletter');
define('TEXT_CONFIRM_DELETE', 'Supprimer le coupon?');

define('TEXT_TO_REDEEM', 'Vous pouvez utiliser le coupon pendant votre commande. Pour faire cela il faut que vous entriez le code coupon dans le champ prévu et cliquiez sur le bouton "Utiliser.');
define('TEXT_IN_CASE', 'Si vous avec des problèmes.');
define('TEXT_VOUCHER_IS', 'Votre code de coupon:');
define('TEXT_REMEMBER', 'Conservez bien votre code de coupon pour que vous puissiez profiter de cette offre');
define('TEXT_VISIT', 'Quand vous nous visitez la prochaine fois sur' . HTTP_SERVER . DIR_WS_CATALOG. '.');
define('TEXT_ENTER_CODE', 'et entrer le code');

define('TABLE_HEADING_ACTION', 'Action');

define('CUSTOMER_ID', 'Numéro client');
define('CUSTOMER_NAME', 'Nom client');
define('REDEEM_DATE', 'Utilisé lé');
define('IP_ADDRESS', 'Adresse IP');

define('TEXT_REDEMPTIONS', 'Utilisation');
define('TEXT_REDEMPTIONS_TOTAL', 'Total:');
define('TEXT_REDEMPTIONS_CUSTOMER', 'Pour ce client:');
define('TEXT_NO_FREE_SHIPPING', 'Pas exclus des frais d\'expédition');

define('NOTICE_EMAIL_SENT_TO', 'Remarque: mail envoyé à: %s');
define('ERROR_NO_CUSTOMER_SELECTED', 'Erreur: Pas de client sélectionné.');
define('COUPON_NAME', 'Nom coupon');
define('COUPON_AMOUNT', 'Valeur coupon');
define('COUPON_CODE', 'Code coupon');
define('COUPON_STARTDATE', 'valable à partir de');
define('COUPON_FINISHDATE', 'valable jusqu\'à');
define('COUPON_FREE_SHIP', 'Libre des frais d\'expédition');
define('COUPON_DESC', 'Description coupon');
define('COUPON_MIN_ORDER', 'Coupon valeur maximale de commande');
define('COUPON_USES_COUPON', 'Nombre/ utilisations par coupon');
define('COUPON_USES_USER', 'Nombre/ utilisations par client');
define('COUPON_PRODUCTS', 'Liste des articles valables');
define('COUPON_CATEGORIES', 'Liste des catégories valables');
define('VOUCHER_NUMBER_USED', 'Nombre utilisé');
define('DATE_CREATED', 'crée le');
define('DATE_MODIFIED', 'modifié le');
define('TEXT_HEADING_NEW_COUPON', 'Créer un nouveau coupon');
define('TEXT_NEW_INTRO', 'Veuillez indiquer les informations suivants pour le nouveau coupon.<br/>');

define('COUPON_NAME_HELP', 'Une brève description pour le coupon');
define('COUPON_AMOUNT_HELP', 'Entrez le rabais pour le coupon ici. Un valeur fixe ou un rabais en pourcentage comme p.ex. 10%');
define('COUPON_CODE_HELP', 'Vous pouvez entrer votre propre code ici (max. 16 signes). Laissez libre ce champ, le cookie sera généré automatiquement.' );
define('COUPON_STARTDATE_HELP', 'La date à partir de laquelle le coupon est valable.<br/>');
define('COUPON_FINISHDATE_HELP', 'La date de l\'expiratoin du coupon.<br>');
define('COUPON_FREE_SHIP_HELP', 'Coupon pour une livraison sans frais d\'expédition.');
define('COUPON_DESC_HELP', 'Description du coupon pour le client');
define('COUPON_MIN_ORDER_HELP', 'Valeur minimale de commande avant que le coupon soit valide');
define('COUPON_USES_COUPON_HELP', 'Entrez ici combien de fois le coupon peut être utilisé . Laissez vide ce champ pour  une utilisation illimitée.');
define('COUPON_USES_USER_HELP', 'Entrez ici combien de fois un client peut utiliser ce coupon. Laissez vide ce champ pour  une utilisation illimitée.');
define('COUPON_PRODUCTS_HELP', 'Une liste séparées par des virgules des IDs des articles pour lesquels ce coupon est valable. Un champ vide signifie pas de limite.');
define('COUPON_CATEGORIES_HELP', 'Une liste séparées par des virgules des IDs des articles pour lesquels ce coupon est valable. Un champ vide signifie pas de limite.');
define('COUPON_ID', 'cID');
define('BUTTON_DELETE_NO_CONFIRM', 'Supprimer sans confirmation');
define('TEXT_NONE', 'Pas de limite');
define('TEXT_COUPON_DELETE', 'Supprimer');
define('TEXT_COUPON_STATUS', 'Statut');
define('TEXT_COUPON_DETAILS', 'Fichiers coupon');
define('TEXT_COUPON_EMAIL', 'Envoie du mail');
define('TEXT_COUPON_OVERVIEW', 'Aperçu');
define('TEXT_COUPON_EMAIL_PREVIEW', 'Confirmation');
define('TEXT_COUPON_MINORDER', 'Valeur minimale de commande');
define('TEXT_VIEW', 'Vue en liste');
define('TEXT_VIEW_SHORT', 'Affichage');
//BOF - web28 - 2011-04-13 - ADD Coupon message infos
define('COUPON_MINORDER_INFO', "\nValeur minimale de commande:");
define('COUPON_RESTRICT_INFO', "\nCet coupon n\'est que valable pour des produits particuliers!");
define('COUPON_INFO', "\nValeur du coupn: ");
define('COUPON_FREE_SHIPPING', 'Pas de frais d\'expédition');
define('COUPON_LINK_TEXT', '\n\nDétails');
define('COUPON_CATEGORIES_RESTRICT', '\nValable pour ces catégories');
define('COUPON_PRODUCTS_RESTRICT', '\nValable pour ces articles');
define('COUPON_NO_RESTRICT', '\nValable pour ces articles');
//EOF - web28 - 2011-04-13 - ADD Coupon message infos

//BOF - web28 - 2011-07-05 - ADD error message
define('ERROR_NO_COUPON_NAME', 'ERREUR: Pas de nom de coupon ');
define('ERROR_NO_COUPON_AMOUNT', 'ERREUR: Pas de valeur de coupon ');
//EOF - web28 - 2011-07-05 - ADD error message

define('COUPON_DATE_START_TT', 'Commence à 00:00:00');
define('COUPON_DATE_END_TT', 'Se termina à minuit (23:59:59)');
define('ERROR_COUPON_DATE', 'ERREUR: La date de fin est plus vieux que la date de début');

define('TEXT_OT_COUPON_STATUS_INFO', 'Il faut que le module coupon (ot_coupon) soit installé (Modules -> <a href="'.xtc_href_link('modules.php','set=ordertotal').'">Résumé</a>)'
);
?>

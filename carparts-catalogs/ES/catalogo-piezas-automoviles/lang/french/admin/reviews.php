<?php
/* --------------------------------------------------------------
   $Id: reviews.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(reviews.php,v 1.6 2002/02/06); www.oscommerce.com 
   (c) 2003	 nextcommerce (reviews.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Critiques de produits');

define('TABLE_HEADING_PRODUCTS', 'Articles');
define('TABLE_HEADING_CUSTOMER', 'Clients');
define('TABLE_HEADING_RATING', 'Evaluation');
define('TABLE_HEADING_DATE_ADDED', 'ajouté le:');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Statut');

define('ENTRY_PRODUCT', 'Article:');
define('ENTRY_FROM', 'de:');
define('ENTRY_DATE', 'Date:');
define('ENTRY_REVIEW', 'Critique:');
define('ENTRY_REVIEW_TEXT', '<span class="smallText colorRed"><b>REMARQUE:</b></span> <span class="smallText">HTML n\'est pas converti! </span>');
define('ENTRY_RATING', 'Evaluation:');

define('TEXT_INFO_DELETE_REVIEW_INTRO', 'Êtes-vous sùr de vouloir supprimer l\'évaluation?');

define('TEXT_INFO_DATE_ADDED', 'ajouté le:');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification:');
define('TEXT_IMAGE_NONEXISTENT', 'IMAGE N\'EXISTE PAS');
define('TEXT_INFO_REVIEW_AUTHOR', 'écrit par:');
define('TEXT_INFO_REVIEW_RATING', 'Evaluation:');
define('TEXT_INFO_REVIEW_READ', 'lu:');
define('TEXT_INFO_REVIEW_SIZE', 'Taille:');
define('TEXT_INFO_PRODUCTS_AVERAGE_RATING', 'Evaluation moyenne:');

define('TEXT_OF_5_STARS', '%s de 5 étoiles!');
define('TEXT_GOOD', '<span class="smallText colorRed"><b>BIEN</b></span>');
define('TEXT_BAD', '<span class="smallText colorRed"><b>MAUVAIS</b></span>');
define('TEXT_INFO_HEADING_DELETE_REVIEW', 'Supprimer l\'évaluation');
?>

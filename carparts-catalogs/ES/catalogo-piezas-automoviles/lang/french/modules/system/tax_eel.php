<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_TAX_EEL_TEXT_TITLE', 'Taux d\'imposition pour des prestations fournies électriquement');
define('MODULE_TAX_EEL_TEXT_DESCRIPTION', 'Tous les taux d\'impositions des états membres de l\'UE seront insérés.');
define('MODULE_TAX_EEL_TEXT_DESCRIPTION_PROCESSED', 'Tous les taux d\'impositions des états membres de l\'UE oint déjà été insérés.');

?>

<?php
/***************************************************************************************************
* file advanced_search_result.php
* use: new file to redirect to our new search_results.php if coming from originalersatzteile-online
* (c) noRiddle 09-2017
***************************************************************************************************/

include ('includes/application_top.php');

if(isset($_GET['keywords'])) {
    xtc_redirect(xtc_href_link(FILENAME_NR_ADVANCED_SEARCH_RESULT, xtc_get_all_get_params(array('keywords')).'keywords='.$_GET['keywords']));
}
?>
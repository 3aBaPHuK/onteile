<?php
/* -----------------------------------------------------------------------------------------
   $Id: gv_sent.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(gv_sent.php,v 1.2 2003/02/18 00:15:52); www.oscommerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Bon d\'achats envoyés');

define('TABLE_HEADING_SENDERS_NAME', 'Expéditeur');
define('TABLE_HEADING_VOUCHER_VALUE', 'Valeur du coupon');
define('TABLE_HEADING_VOUCHER_CODE', 'Code du bon d\'achat');
define('TABLE_HEADING_DATE_SENT', 'Date d\'envoi');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_SENDERS_ID', 'N° de l\'expéditeur:');
define('TEXT_INFO_AMOUNT_SENT', 'Montant envoyé:');
define('TEXT_INFO_DATE_SENT', 'Date:');
define('TEXT_INFO_VOUCHER_CODE', 'Code du bon dachat:');
define('TEXT_INFO_EMAIL_ADDRESS', 'Adresse email:');
define('TEXT_INFO_DATE_REDEEMED', 'Date d\'utilisation:');
define('TEXT_INFO_IP_ADDRESS', 'Adresse IP:');
define('TEXT_INFO_CUSTOMERS_ID', 'N° Cient:');
define('TEXT_INFO_NOT_REDEEMED', 'Pas utilisé');
?>

<?php
/***************************************************
* file: add_cart_actions.php
* use: add products to cart via external link, no products with attributes allowed, URL would be e.g.: DOMAIN/?action=custom_cart&p_id=1-2~9-3~2-6, noRiddle
*      Example: http://www.online-teile.com/volkswagen-ersatzteile?action=custom_cart&p_id=365761504-2~782301-4~2429112-6
*      New with attribute: DOMAIN/?action=custom_cart&p_id=1{1}1-2~9-3~2-6
* (c) noRiddle 01-2017
***************************************************/

if(isset($_GET['p_id'])) {
    $goto = FILENAME_SHOPPING_CART;
    //remove potentially exsitent products in cart
    foreach($_SESSION['cart_'.$_SESSION['shopsess']]->contents as $key => $val){
        $_SESSION['cart_'.$_SESSION['shopsess']]->remove($key);
    }
    
    $prod_id_arr = explode('~', $_GET['p_id']);
    foreach($prod_id_arr as $val) {
        $prod_id = '';
        $att_array = array();
        $attr_arr = array();
        
        $prod_id_and_pcs = explode('-', $val);
        //BOC new for shop version 2.0.2.2, add attribute deposit if exists, noRiddle
        //$prod_id = (int)$prod_id_and_pcs[0];
        if(isset($_GET['type']) && $_GET['type'] == 'model') { //make use of model n�. possible with new GET parameter, 09-11-2018, noRiddle
            $temp_prid = explode('{', $prod_id_and_pcs[0]);
            $prodid_qu_str = "SELECT products_id FROM ".TABLE_PRODUCTS." WHERE products_model = '".xtc_db_input($temp_prid[0])."'";
            $prodid_qu = xtc_db_query($prodid_qu_str);
            if(xtc_db_num_rows($prodid_qu) > 0) {
                $prodid_arr = xtc_db_fetch_array($prodid_qu);
                $prod_id = (int)$prodid_arr['products_id'];
            }
        } else {
            $prod_id = (int)xtc_get_prid($prod_id_and_pcs[0]);
        }
        $pcs = (int)$prod_id_and_pcs[1];
        //BOC new for shop version 2.0.2.2, add attribute deposit if exists, noRiddle
        if (strpos($prod_id_and_pcs[0], '{') !== false) {
            $att_array = preg_split('/[{}]/', $prod_id_and_pcs[0], null, PREG_SPLIT_NO_EMPTY);
            array_shift($att_array);
            for ($a = 0, $ca = count($att_array); $a < $ca; $a += 2) {
                $attr_arr[$att_array[$a]] = $att_array[$a + 1];
            }
        }
        if($prod_id != '') {
            //$_SESSION['cart_'.$_SESSION['shopsess']]->add_cart($prod_id, $pcs);
            $_SESSION['cart_'.$_SESSION['shopsess']]->add_cart($prod_id, $pcs, (isset($attr_arr) && !empty($attr_arr) ? $attr_arr : ''));
        }
        //EOC new for shop version 2.0.2.2, add attribute deposit if exists, noRiddle
    }
}
xtc_redirect(xtc_href_link($goto, xtc_get_all_get_params(array ('action','p_id','type'))));
?>
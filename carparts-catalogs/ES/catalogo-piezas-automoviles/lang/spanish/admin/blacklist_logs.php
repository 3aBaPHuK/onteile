<?php
  /* --------------------------------------------------------------
   $Id: blacklist_logs.php 10584 2017-01-20 10:45:19Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Registros de la lista negra');

define('TABLE_HEADING_IP', 'Dirección IP');
define('TABLE_HEADING_BANNED', 'Tiempo bloqueado');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_EDIT_ENTRY', 'Editar registro');
define('TEXT_NEW_ENTRY', 'Nuevo registro');
define('TEXT_ENTRY_IP', 'Dirección IP');
define('TEXT_ENTRY_IP_INFO', 'Especifique la dirección IP que desea bloquear.');
define('TEXT_ENTRY_TIME', 'Tiempo');
define('TEXT_ENTRY_TIME_INFO', 'Especifique la fecha y la hora hasta la que se debe bloquear la IP.');

define('TEXT_DELETE_INTRO', '¿Está seguro de que desea borrar esta dirección IP?');

define('ERROR_LOG_DIRECTORY_DOES_NOT_EXIST', 'Error: El directorio de respaldo es inexistente.');
define('ERROR_LOG_DIRECTORY_NOT_WRITEABLE', 'Error: El directorio de respaldo está protegido contra escritura.');
?>

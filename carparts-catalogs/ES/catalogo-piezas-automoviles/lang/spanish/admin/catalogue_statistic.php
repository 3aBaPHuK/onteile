<?php
/***************************************************
* file: catalogue_statistic.php
* use: language file
*      
* (c) noRiddle 08-2018
***************************************************/

define('SC_PLEASE_CHOOSE_TXT', 'Seleccione por favor');
define('SC_SEARCH_ONLY_VIN_TXT', 'Solo mostrar búsquedas por VIN');
define('SC_SEARCH_ONLY_WIZARD_TXT', 'Solo mostrar búsquedas por parámetros');
define('SC_SEARCH_VIN_FOUND_TXT', 'Solo mostrar NIVs encontrados');
define('SC_SEARCH_VIN_NOT_FOUND_TXT', 'Solo mostrar NIVS non encontrados');

define('SC_TABLE_HEADING_CUST_ID', 'customers_id');
define('SC_TABLE_HEADING_CUST_IP', 'customer IP');
define('SC_TABLE_HEADING_DATE_ADDED', 'date_added');
define('SC_TABLE_HEADING_BRAND', 'Marca');
define('SC_TABLE_HEADING_VINORWIZ', 'Buscar NIV o parámetro');
define('SC_TABLE_HEADING_VIN', 'NIV');
define('SC_TABLE_HEADING_VIN_FOUND', 'NIV no encontrado ?');

define('SC_TEXT_SORT_ASC', 'ordenar ascendente');
define('SC_TEXT_SORT_DESC', 'ordenar descendente');

define('SC_VARIOUS_FILTERS_TXT', 'Filtros diversos');
define('SC_FILTER_VINORWIZ_TXT', 'Filtro buscar por:');
define('SC_FILTER_BRAND_TXT', 'Filtro marca:');
define('SC_FILTER_CUSTMAIL_TXT', 'Filtro cliente (dirección correo electr.):');
define('SC_SHOW_BOUGHT_VINS', 'Mostrar créditos comprados: ');
define('SC_FILTER_FROM_DATE_TXT', 'fecha desde (incl.):');
define('SC_FILTER_TO_DATE_TXT', 'fecha hasta (incl.):');
define('SC_BOUGHT_TEXT', ' | comprado: ');

define('SC_TAB_HEADING_STATISTIC', 'Estatistica');
define('SC_TAB_HEADING_TABLE', 'Tabla');
define('SC_CLICK_ON_FOR_STATS_TXT', 'Si eran cargados datos mediante los filtros arriba, cliquee en la pestaña "Estatistica" para cargar la estatistica.<br />Si no se encuentran datos en la pestaña "Tabla" inicie filtros arriba (p. ej. mediante el botón "Go")');
define('SC_GRAPH_HEADING_SEARCHBY', 'Búsquedas %s %s hasta el %s');
define('SC_GRAPH_HEADING_BRAND_RELATIONS', '%s Búsquedas por marca %s hasta el %s');

define('SC_TEXT_DISPLAY_HOW_MANY_OF_HO_MANY', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> búsquedas)');
<?php
# customers_notice - Menueintrag

defined( '_VALID_XTC' ) or die( 'Direct Access to this location is not allowed.' );

  //Sprachabhaengiger Menueeintrag, kann fuer weiter Sprachen ergaenzt werden
  switch ($_SESSION['language_code']) {
    case 'de':
      define('BOX_CUSTOMERS_NOTICE','Kunden Hinweise');
      break;
    case 'en':
      define('BOX_CUSTOMERS_NOTICE','Customers Notice');
      break;
    case 'en_us':
      define('BOX_CUSTOMERS_NOTICE','Customers Notice');
      break;
    case 'es':
      define('BOX_CUSTOMERS_NOTICE','Aviso para clientes');
      break;
    case 'es_mx':
      define('BOX_CUSTOMERS_NOTICE','Aviso para clientes');
      break;
    case 'fr':
      define('BOX_CUSTOMERS_NOTICE','Notification pour clients');
      break;
    case 'it':
      define('BOX_CUSTOMERS_NOTICE','Notifica per clienti');
      break;
    default:
      define('BOX_CUSTOMERS_NOTICE','Customers Notice');
      break;
  }

  //BOX_HEADING_TOOLS = Name der box in der der neue Menueeintrag erscheinen soll
  $add_contents[BOX_HEADING_TOOLS][] = array( 
    'admin_access_name' => 'customers_notice',   //Eintrag fuer Adminrechte
    'filename' => 'customers_notice.php',        //Dateiname der neuen Admindatei
    'boxname' => BOX_CUSTOMERS_NOTICE,     //Anzeigename im Menue
    'parameter' => '',                  //zusaetzliche Parameter z.B. 'set=export'
    'ssl' => ''                         //SSL oder NONSSL, kein Eintrag = NONSSL
    );
  
?>
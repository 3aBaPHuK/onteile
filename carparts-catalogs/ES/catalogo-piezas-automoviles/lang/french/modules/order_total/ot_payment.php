<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_payment.php 3481 2012-08-22 07:07:50Z dokuman $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (C) 2007 Estelco - Ebusiness & more - http://www.estelco.de
   (C) 2004 IT eSolutions Andreas Zimmermann - http://www.it-esolutions.de

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_PAYMENT_HELP_LINK', ' <a onclick="window.open(\'popup_help.php?type=order_total&modul=ot_payment&lng=german\', \'Aide\', \'scrollbars=yes,resizable=yes,menubar=yes,width=800,height=600\'); return false" target="_blank" href="popup_help.php?type=order_total&modul=ot_payment&lng=german"><b>[AIDE]</b></a>');
define('MODULE_ORDER_TOTAL_PAYMENT_HELP_TEXT', '<h2>Réduction et surtaxe sur les modes de paiement</h2>
Si davantage d\'échelles de réduction doivent être possible (<b>3</b> à défaut), il faut que la valeur de la variable $num (fichier vocal) et $this->num (fichier du module) passe à la valeur souhaitée.
<hr>
<h3>Champs échelle de réduction</h3>
<p class="red">Remarque : l’adresse d\'expédition est déterminante pour les codes des pays. Pour valider une réduction/un supplément pour tous les pays, utilisez le code 00 ou ne saisissez pas de code de pays (y compris le"|")!</p>
<h4>Utiliser la notation suivante pour les réductions :</h4>
<pre>   <span class="blue">DE</span>|<span class="green">100</span>:<span class="red">4</span>,<span class="green">200</span>:<span class="red">5</span></pre>
<p>Signification :</p>
<p>Les clients de <span class="blue">l’Allemagne</span> pourront bénéficier d\'une réduction de <span class="red">4%</span> à partir de <span class="green">100€</span> et d\'une réduction de <span class="red">5%</span> à partir de <span class="green">200€</span>.</p>
<pre>   <span class="green">100</span>:<span class="red">2</span>,<span class="green">200</span>:<span class="red">3</span></pre>
<p>Signification :</p>
<p>Les clients de tous les pays pourront bénéficier d\'une réduction de <span class="red">2%</span> à partir de <span class="green">100€</span> et d\'une réduction de <span class="red">3%</span> à partir de <span class="green">200€</span>.</p>
<h4>Utiliser la notation suivante pour les surtaxes :</h4>
<pre>   <span class="blue">DE</span>|<span class="green">100</span>:<span class="red">-3</span></pre>
<p>Signification :</p>
<p>Une surtaxe de <span class="red">3%</span> sera facturée à partir de <span class="green">100€</span> pour les clients de <span class="blue">l’Allemagne</span>.</p>
<h4>Exemple pour Paypal</h4>
1ère échelle de réduction
<pre>   <span class="blue">DE</span>|<span class="green">0</span>:<span class="red">-1.9</span>&<span class="lila">-0.35</span></pre>
2e échelle de réduction
<pre>   <span class="blue">00</span>|<span class="green">0</span>:<span class="red">-3.4</span>&<span class="lila">-0.35</span></pre>
<p>Signification :</p>
<p>Une surtaxe de <span class="red">1,9%</span>, plus <span class="lila">0,35€</span> sera facturée à partir de <span class="green">0€</span> (donc toujours) pour les clients de <span class="blue">l\'Allemagne</span>.</p>
<p>Une surtaxe de span class="red">3,4%</span>, plus <span class="lila">0,35€</span> sera facturée à partir de <span class="green">0€</span> pour les clients de <span class="blue">tous les pays restants (00=tous)</span>.</p>
<p>Ce qui compte ici c\'est l\'ordre des entrées (saisissez les pays restants toujours en dernier) et que "false" soit sélectionné à "calcul multiple", sinon les deux surtaxes seront facturées.</p>
<h4>Exemple pour montant fixe</h4>
<pre>   <span class="green">0</span>:<span class="red">0</span>&<span class="lila">-2</span></pre>
<p>signification :</p>
<p>Une surtaxe de <span class="red">0%</span> (pas de surtaxe en pourcentage donc), plus <span class="lila">2,00€</span> (la surtaxe fixe) sera facturée pour les clients de tous les pays.</p>
<hr>
<h3>Champs mode de paiement</h3>
<p>Saisir le <b>code interne</b> du mode de paiement dans les champs, p.ex. <b>moneyorder</b> pour paiement d\'avance ou <b>cod</b> pour contre-remboursement. Diviser plusieurs modes de paiement par une virgule</p>Voir aussi modules -> modes de paiement -> colonne "nom du module (pour utilisation interne)".<br/><br/>
<hr>
<h3>Afficher à mode de paiement pendant processus de commande</h3>
Passez l\'option "Afficher à des modes de paiement" à "true", si la réduction correspondante doit être déjà affichée lors de la sélection du mode de paiement dans le processus de commande. <br/><br/>
En plus, il est possible de régler le mode d\'affichage avec l\'option "Mode d\'affichage lors de la sélection du mode de paiement dans le processus de commande" :
<p> -- default: pourcentage ou montant, selon des entrées à l\'échelle de réduction</p>
<p> -- price: Le montant réel est toujours affiché</p>'
);

define('MODULE_ORDER_TOTAL_PAYMENT_TITLE', 'Réduction & surtaxe sur les modes de paiement');
define('MODULE_ORDER_TOTAL_PAYMENT_DESCRIPTION', 'Réduction et surtaxe sur les modes de paiement'.MODULE_ORDER_TOTAL_PAYMENT_HELP_LINK);

define('MODULE_ORDER_TOTAL_PAYMENT_STATUS_TITLE', 'Afficher réduction');
define('MODULE_ORDER_TOTAL_PAYMENT_STATUS_DESC', 'Souhaitez-vous activer la réduction sur les modes de paiement ?');

define('MODULE_ORDER_TOTAL_PAYMENT_SORT_ORDER_TITLE', '<hr>Ordre de triage');
define('MODULE_ORDER_TOTAL_PAYMENT_SORT_ORDER_DESC', 'Ordre d\'affichage');

for ($j=1; $j<=MODULE_ORDER_TOTAL_PAYMENT_NUMBER; $j++) {
define('MODULE_ORDER_TOTAL_PAYMENT_PERCENTAGE' . $j . '_TITLE', '<hr>'.$j . '. Échelle de réduction');
define('MODULE_ORDER_TOTAL_PAYMENT_PERCENTAGE' . $j . '_DESC', 'Rabais (montant minimum:pourcentage)');
define('MODULE_ORDER_TOTAL_PAYMENT_TYPE' . $j . '_TITLE', $j . '. Mode de paiement');
define('MODULE_ORDER_TOTAL_PAYMENT_TYPE' . $j . '_DESC', 'Modes de paiement sur lesquels la réduction doit être appliquée');
}

define('MODULE_ORDER_TOTAL_PAYMENT_INC_SHIPPING_TITLE', '<hr>Frais de port inclus');
define('MODULE_ORDER_TOTAL_PAYMENT_INC_SHIPPING_DESC', 'La réduction sera également appliquée sur les frais de port');

define('MODULE_ORDER_TOTAL_PAYMENT_INC_TAX_TITLE', '<hr>Impôt sur le chiffre d\'affaires inclus');
define('MODULE_ORDER_TOTAL_PAYMENT_INC_TAX_DESC', 'La réduction sera également appliquée sur l\'impôt sur le chiffre d\'affaires');

define('MODULE_ORDER_TOTAL_PAYMENT_CALC_TAX_TITLE', '<hr>Calcul de l\'impôt sur le chiffre d\'affaires');
define('MODULE_ORDER_TOTAL_PAYMENT_CALC_TAX_DESC', 'recalculer le montant de l\'impôt sur le chiffre d\'affaires');

define('MODULE_ORDER_TOTAL_PAYMENT_ALLOWED_TITLE', '<hr>Zones autorisés');
define('MODULE_ORDER_TOTAL_PAYMENT_ALLOWED_DESC' , 'Indiquez les zones <b>un par un</b>, qui seront autorisés pour le module. (p.ex. AT,DE (si vide, tous les zones seront autorisés))');

define('MODULE_ORDER_TOTAL_PAYMENT_DISCOUNT', 'Réduction');
define('MODULE_ORDER_TOTAL_PAYMENT_FEE', 'Surtaxe');

define('MODULE_ORDER_TOTAL_PAYMENT_TAX_CLASS_TITLE', '<hr>Catégorie fiscale');
define('MODULE_ORDER_TOTAL_PAYMENT_TAX_CLASS_DESC', 'La catégorie fiscale n\'est pas déterminant et ne sert qu\'à éviter un message d\'érreur.');

define('MODULE_ORDER_TOTAL_PAYMENT_BREAK_TITLE', '<hr>Calcul multiple');
define('MODULE_ORDER_TOTAL_PAYMENT_BREAK_DESC', 'Souhaitez-vous que des calculs multiples puissent être possibles ? Si non, la transaction sera annulée après la première réduction appropriée.');

define('MODULE_ORDER_TOTAL_PAYMENT_SHOW_IN_CHECKOUT_PAYMENT_TITLE', '<hr>Afficher à des modes de paiement');
define('MODULE_ORDER_TOTAL_PAYMENT_SHOW_IN_CHECKOUT_PAYMENT_DESC', 'Afficher lors des modes de paiement dans le processus de la commande');

define('MODULE_ORDER_TOTAL_PAYMENT_SHOW_TYPE_TITLE', '<hr>Mode d\'affichage à des modes de paiement');
define('MODULE_ORDER_TOTAL_PAYMENT_SHOW_TYPE_DESC', 'Mode d\'affichage lors de la sélection du mode de paiement dans le processus de la commande<br />-- default: pourcentage ou montant, selon les entrées à l\'échelle de réduction <br />-- price: Le montant réel est toujours affiché');

define('MODULE_ORDER_TOTAL_PAYMENT_NUMBER_TITLE', 'Nombre modes de paiement');
define('MODULE_ORDER_TOTAL_PAYMENT_NUMBER_DESC', 'Nombre réduction & surtaxe sur les modes de paiement');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id: chp.php 5123 2013-07-18 11:49:11Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(chp.php,v 1.01 2003/02/18 03:30:00); www.oscommerce.com 
   (c) 2003	 nextcommerce (chp.php,v 1.4 2003/08/1); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   swiss_post_1.02       	Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/


define('MODULE_SHIPPING_CHP_TEXT_TITLE', 'Schweizerische Post (poste suisse9');
define('MODULE_SHIPPING_CHP_TEXT_DESCRIPTION', 'Die Schweizerische Post (la poste suisse)');
define('MODULE_SHIPPING_CHP_TEXT_WAY', 'Envoi vers');
define('MODULE_SHIPPING_CHP_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_CHP_INVALID_ZONE', 'Malheureusement il n\'est pas possible d\'envoyer dans ce pays');
define('MODULE_SHIPPING_CHP_UNDEFINED_RATE', 'Les frais de port ne peuvent pas être caluclés pour le moment');

define('MODULE_SHIPPING_CHP_STATUS_TITLE' , 'Schweizerische Post (poste suisse)');
define('MODULE_SHIPPING_CHP_STATUS_DESC' , 'Voulez-vous proposer l\'envoi par la Schweizerische Post?');
define('MODULE_SHIPPING_CHP_HANDLING_TITLE' , 'Frais de traitement');
define('MODULE_SHIPPING_CHP_HANDLING_DESC' , 'Frais de traitement pour ce type d\'envoi en CHF');
define('MODULE_SHIPPING_CHP_TAX_CLASS_TITLE' , 'Taux d\'imposition');
define('MODULE_SHIPPING_CHP_TAX_CLASS_DESC' , 'Veuillez séléctionner le taux de TVA pour ce type d\'envoi');
define('MODULE_SHIPPING_CHP_ZONE_TITLE' , 'Zone d\'envoi');
define('MODULE_SHIPPING_CHP_ZONE_DESC' , 'Si vous choisissez une zone ce type d\'envoi est seulement porposé dans cette zone.');
define('MODULE_SHIPPING_CHP_SORT_ORDER_TITLE' , 'Ordre d\'affichage');
define('MODULE_SHIPPING_CHP_SORT_ORDER_DESC' , 'Les plus bas sont affichés d\'abord.');
define('MODULE_SHIPPING_CHP_ALLOWED_TITLE' , 'Zones d\'envoi individuels');
define('MODULE_SHIPPING_CHP_ALLOWED_DESC' , 'Veuillez indiquer les zones <b>individuellement</b> pour lesquelles l\'envoi est possible. Par exemple AT,DE');

define('MODULE_SHIPPING_CHP_COUNTRIES_1_TITLE' , 'Zone de tarif 0 pays');
define('MODULE_SHIPPING_CHP_COUNTRIES_1_DESC' , 'Zone intérieure');
define('MODULE_SHIPPING_CHP_COST_ECO_1_TITLE' , 'Tableau des tarifs de la zone 0 jusqu\'à 30kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_1_DESC' , 'Tableau des tarifs pour la zone intérieure, basé sur <b>\'ECO\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_PRI_1_TITLE' , 'Tableau des tarifs de la zone 0 jusq\'à 30kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_1_DESC' , 'Tableau des tarifs pour la zone intérieure, basé sur <b>\'PRI\'<b> jusqu\'à un poids d\'envoi de 30kg.');

define('MODULE_SHIPPING_CHP_COUNTRIES_2_TITLE' , 'Zone de tarif 1 Pays');
define('MODULE_SHIPPING_CHP_COUNTRIES_2_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 1 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_CHP_COST_ECO_2_TITLE' , 'Tableau des tarifs de la zone 1 jusqu\'à 30kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_2_DESC' , 'Tableau des tarifs pour la zone 1, basé sur <b>\'ECO\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_PRI_2_TITLE' , 'Tableau des tarifs de la zone 1 jusqu\'à 30kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_2_DESC' , 'Tableau des tarifs pour la zone 1, basé sur <b>\'PRI\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_URG_2_TITLE' , 'Tableau des tarifs de la zone 1 jusqu\'à 30kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_2_DESC' , 'Tableau des tarifs pour la zone 1, basé sur <b>\'URG\'<b> jusqu\'à un poids d\'envoi de 30kg.');

define('MODULE_SHIPPING_CHP_COUNTRIES_3_TITLE' , 'Zone de tarif 2 Pays');
define('MODULE_SHIPPING_CHP_COUNTRIES_3_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 2 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_CHP_COST_ECO_3_TITLE' , 'Tableau des tarifs de la zone 2 jusqu\'à 30kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_3_DESC' , 'Tableau des tarifs pour la zone 2, basé sur <b>\'ECO\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_PRI_3_TITLE' , 'Tableau des tarifs de la zone 2 jusqu\'à 30kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_3_DESC' , 'Tableau des tarifs pour la zone 2, basé sur <b>\'PRI\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_URG_3_TITLE' , 'Tableau des tarifs de la zone 2 jusqu\'à 30kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_3_DESC' , 'Tableau des tarifs pour la zone 2, basé sur <b>\'URG\'<b> jusqu\'à un poids d\'envoi de 30kg.');

define('MODULE_SHIPPING_CHP_COUNTRIES_4_TITLE' , 'Zone de tarif 3 Pays');
define('MODULE_SHIPPING_CHP_COUNTRIES_4_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 3 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_CHP_COST_ECO_4_TITLE' , 'Tableau des tarifs de la zone 3 jusqu\'à 30kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_4_DESC' , 'Tableau des tarifs pour la zone 3, basé sur <b>\'ECO\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_PRI_4_TITLE' , 'Tableau des tarifs de la zone 3 jusqu\'à 30kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_4_DESC' , 'Tableau des tarifs pour la zone 3, basé sur <b>\'PRI\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_URG_4_TITLE' , 'Tableau des tarifs de la zone 3 jusqu\'à 30kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_4_DESC' , 'Tableau des tarifs pour la zone 3, basé sur <b>\'URG\'<b> jusqu\'à un poids d\'envoi de 30kg.');

define('MODULE_SHIPPING_CHP_COUNTRIES_5_TITLE' , 'Zone de tarif 4 Pays');
define('MODULE_SHIPPING_CHP_COUNTRIES_5_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 4 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_CHP_COST_ECO_5_TITLE' , 'Tableau des tarifs de la zone 4 jusqu\'à 30kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_5_DESC' , 'Tableau des tarifs pour la zone 4, basé sur <b>\'ECO\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_PRI_5_TITLE' , 'Tableau des tarifs de la zone 4 jusqu\'à 30kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_5_DESC' , 'Tableau des tarifs pour la zone 4, basé sur <b>\'PRI\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_URG_5_TITLE' , 'Tableau des tarifs de la zone 4 jusqu\'à 30kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_5_DESC' , 'Tableau des tarifs pour la zone 4, basé sur <b>\'URG\'<b> jusqu\'à un poids d\'envoi de 30kg.');

define('MODULE_SHIPPING_CHP_COUNTRIES_6_TITLE' , 'Zone de tarif 4 Pays');
define('MODULE_SHIPPING_CHP_COUNTRIES_6_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 4 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_CHP_COST_ECO_6_TITLE' , 'Tableau des tarifs de la zone 4 jusqu\'à 30kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_6_DESC' , 'Tableau des tarifs pour la zone 4, basé sur <b>\'ECO\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_PRI_6_TITLE' , 'Tableau des tarifs de la zone 4 jusqu\'à 30kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_6_DESC' , 'Tableau des tarifs pour la zone 4, basé sur <b>\'PRI\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_URG_6_TITLE' , 'Tableau des tarifs de la zone 4 jusqu\'à 30kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_6_DESC' , 'Tableau des tarifs pour la zone 4, basé sur <b>\'URG\'<b> jusqu\'à un poids d\'envoi de 30kg.');

define('MODULE_SHIPPING_CHP_COUNTRIES_7_TITLE' , 'Zone de tarif 5 Pays');
define('MODULE_SHIPPING_CHP_COUNTRIES_7_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 5 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_CHP_COST_ECO_7_TITLE' , 'Tableau des tarifs de la zone 5 jusqu\'à 30kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_7_DESC' , 'Tableau des tarifs pour la zone 5, basé sur <b>\'ECO\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_PRI_7_TITLE' , 'Tableau des tarifs de la zone 5 jusqu\'à 30kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_7_DESC' , 'Tableau des tarifs pour la zone 5, basé sur <b>\'PRI\'<b> jusqu\'à un poids d\'envoi de 30kg.');
define('MODULE_SHIPPING_CHP_COST_URG_7_TITLE' , 'Tableau des tarifs de la zone 5 jusqu\'à 30kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_7_DESC' , 'Tableau des tarifs pour la zone 5, basé sur <b>\'URG\'<b> jusqu\'à un poids d\'envoi de 30kg.');
?>

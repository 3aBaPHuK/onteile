<?php
/**********************************************
* file reworked and adadapted by noRiddle
*
* (c) noRiddle 03-2018
**********************************************/

require_once dirname(__FILE__) . '/../template.php';

//BOC connect to shop db, noRiddle
//include_once (dirname(__FILE__).'/shop_dbs/conn_to_shop_db.php'); //do this in application_top, noRiddle
//EOC connect to shop db, noRiddle

//BOC code for catalogue shopping cart
if(isset($_POST['check_to_buy'])) {
    //echo '<pre>$post_arr: '.print_r($post_arr, true).'</pre>';
    //echo '<pre>'.print_r($_POST, true).'</pre>';
    //echo '<pre>$_POST[check_to_buy]: '.print_r($_POST['check_to_buy'], true).'</pre>';
    //echo '<pre>$_POST[products_model]: '.print_r($_POST['products_model'], true).'</pre>';
}
//echo '<pre>$_SESSION[catalogue_cart]: '.print_r($_SESSION['catalogue_cart'.$_SESSION['shopsess']], true).'</pre>';
//echo '<pre>'.$_SESSION['customer_id'].'</pre>';
//echo '<pre>'.session_id().'</pre>';
//EOC code for catalogue shopping cart

class GuayaquilDetailsList extends GuayaquilTemplate
{
    //var $closedimage = 'images/closed.gif';
    var $closedimage = '<i class="fa fa-arrow-down"></i>'; //define font icon here, noRiddle
    //var $opennedimage = 'images/openned.gif';
    var $opennedimage = '<i class="fa fa-arrow-up"></i>'; //define font icon here, noRiddle
    var $fullreplacementimage = 'images/replacement.gif';
    var $forwardreplacementimage = 'images/replacement-forward.gif';
    var $backwardreplacementimage = 'images/replacement-backward.gif';
    var $detailinfoimage = 'images/info.gif';
    var $cartimage = 'images/cart.gif';
    //BOC new pics for not available" and "price question", noRiddle
    var $noavail_img = 'nr_catalogue_files/guayaquillib/render/details/images/not_available.png';
    var $pricequest_img = 'nr_catalogue_files/guayaquillib/render/details/images/price_question.png';
    var $prodrepl_img = 'nr_catalogue_files/guayaquillib/render/details/images/product_replace.png';
    //EOC new pics for not available" and "price question", noRiddle

    //BOC add checkbox column, noRiddle
    //var $columns = array('OEM' => 3, 'Name' => 3, 'Cart' => 1, 'Note' => 2, 'Tooltip' => 1);
    var $columns = array('OEM' => 3, 'Name' => 3, 'Cart' => 1, 'Note' => 2, 'Tooltip' => 1, 'CHKB' => 3);
    //var $basic_columns = array('PNC' => 1, 'OEM' => 3, 'Name' => 3, 'Cart' => 1, 'Tooltip' => 1, 'flag' => 1, 'availability' => 1, 'Note' => 2,);
    var $basic_columns = array('PNC' => 1, 'OEM' => 3, 'Name' => 3, 'Cart' => 1, 'Tooltip' => 1, 'flag' => 1, 'availability' => 1, 'Note' => 2, 'CHKB' => 3);
    //EOC add checkbox column, noRiddle

    var $currency = '%s';
    var $group_by_filter = false;
    var $row_class = '';
    
    //BOC define vars for model array and search string, noRiddle
    var $mod_arr = array();
    var $modls = '';
    var $shp_data_arr = array();
    //EOC define vars for model array and search string, noRiddle
    
    static $table_no = 0;

    protected $details;
    protected $catalog;

    protected $rowno = 1;

    function __construct(IGuayaquilExtender $extender) //added $shp_db_link, noRiddle
    {
        parent::__construct($extender);

        //$this->closedimage              = $this->Convert2uri(dirname(__FILE__) . DIRECTORY_SEPARATOR . $this->closedimage);
        //$this->closedimage = $closedimage; //redefined, see above, noRiddle
        //$this->opennedimage             = $this->Convert2uri(dirname(__FILE__) . DIRECTORY_SEPARATOR . $this->opennedimage);
        //$this->opennedimage = $opennedimage; //redefined, see above, noRiddle
        $this->fullreplacementimage     = $this->Convert2uri(dirname(__FILE__) . DIRECTORY_SEPARATOR . $this->fullreplacementimage);
        $this->forwardreplacementimage  = $this->Convert2uri(dirname(__FILE__) . DIRECTORY_SEPARATOR . $this->forwardreplacementimage);
        $this->backwardreplacementimage = $this->Convert2uri(dirname(__FILE__) . DIRECTORY_SEPARATOR . $this->backwardreplacementimage);
        $this->detailinfoimage          = $this->Convert2uri(dirname(__FILE__) . DIRECTORY_SEPARATOR . $this->detailinfoimage);
        $this->cartimage                = $this->Convert2uri(dirname(__FILE__) . DIRECTORY_SEPARATOR . $this->cartimage);
        
        //BOC define db link, noRiddle
        global $shp_db_link;
        $this->shp_db_link = $shp_db_link;
        //echo '<pre>'.var_dump($this->shp_db_link).'</pre>';
        //EOC define db link, noRiddle
        //BOC new array with in shops activated states, noRiddle
        global $nr_activated_states;
        $this->nr_activated_states = $nr_activated_states;
        //EOC new array with in shops activated states, noRiddle
        
        //BOC config
        $this->chunk_no = 25;
        //EOC config
    }

    function Draw($catalog, $details)
    {
        $this->catalog      = $catalog;
        $this->details      = $details;
        
        //echo '<pre>'.print_r($details, true).'</pre>';

        //$this->AppendJavaScript(dirname(dirname(__FILE__)) . '/jquery.tooltip.js'); //include this in /templates/catalogue/javascript/general.js.php, noRiddle
        //$this->AppendJavaScript(dirname(__FILE__) . '/detailslist.js'); //include this in /templates/catalogue/javascript/general.js.php, noRiddle

        $html = $this->DrawJavaScript();
        
        //BOC begin form tag, noRiddle
        $html .= xtc_draw_form('into_cat_shp_cart'. (++GuayaquilDetailsList::$table_no), xtc_href_link(FILENAME_CONTENT, xtc_get_all_get_params()), 'post', 'class="into-cat-shp-cart"');
        //EOC begin form tag, noRiddle

        //$html .= '<table class="guayaquil_table" width="96%" id="g_DetailTable' . (++GuayaquilDetailsList::$table_no) . '">';
        $html .= '<table class="guayaquil_table" id="g_DetailTable' . (++GuayaquilDetailsList::$table_no) . '">';

        foreach ($this->basic_columns as $column => &$visibility) {
            $visibility = array_key_exists($column, $this->columns) ? $this->columns[$column] : 0; //?$this->columns[$column]:$visibility;
        }

        $html .= $this->DrawHeader($this->basic_columns);
        
        //BOC add tbody, noRiddle
        $html .= '<tbody>';
        //EOC add tbody, noRiddle
        //BOC try to fix header, noRiddle
        $html .= '<tr><td colspan="'.$this->cnt_dspl.'" class="tbl-cont"><div class="inside-tbl"'.(isset($_GET['coID']) && $_GET['coID'] != '1007' ? ' style="height:564px;"' : ' style="max-height:564px;"').'><table><tbody>';
        //EOC try to fix header, noRiddle
        
        $this->row_class = '';
        $found = false;
        //$cnt = -1; //set counter, noRiddle
        $out = array(); //define array for attributes, noRiddle
        foreach ($details as $detail) {
            $detail['note'] = '';
            //$cnt++; //augment counter; noRiddle
            
            foreach ($detail->attribute as $attr) {
                $detail['note'] .= '<span class="item"><span class="name">' . (string)$attr->attributes()->name . '</span><span class="value">' . (string)$attr->attributes()->value . '</span></span>';
                $out[(string)$attr->attributes()->name] = (string)$attr->attributes()->value; //write attributes in array, noRiddle
            }
            //BOC add checkbox field, noRiddle
            //BOC filter if replacing part exists
            /*if(array_key_exists('replPart', $out)) {
                $repl_arr = explode(':', $out['replPart']);
                //$chbx_val = $repl_arr[1];
                //echo '<pre>'.print_r($repl_arr, true).'</pre>';
            }*/
            //} else {
                $chbx_val = $this->getProperty($detail, 'oem', $isArray);
            //}
            $chbx_val = preg_replace('#[^0-9a-z]#i', '', $chbx_val);
            $detail['chkb'] = xtc_draw_checkbox_field('check_to_buy[]', $chbx_val);
            //$detail['chkb'] = xtc_draw_checkbox_field('check_to_buy[]', "$cnt"); //have counter as value, noRiddle
            //EOC add checkbox field, noRiddle
        }
        if ($this->group_by_filter) {
            $searched   = array();
            $additional = array();
            foreach ($details as $detail) {
                $found = true;
                if ((string)$detail['match']) {
                    $searched[] = $detail;
                } else {
                    $additional[] = $detail;
                }
            }

            if (!$found)
                foreach ($details->row as $detail) {
                    if ((string)$detail['match']) {
                        $searched[] = $detail;
                    } else {
                        $additional[] = $detail;
                    }
                }

            //BOC get data from external shop, noRiddle
            foreach ($searched as $detail) {
                //BOC collect all model numbers, noRiddle
                $model_no = $this->getProperty($detail, 'oem', $isArray);
                //$model_no = str_replace(array(' ', '-', '_'), '', $model_no);
                //$model_no = preg_replace('/\s+/', '', $model_no);
                $model_no = preg_replace('#[^0-9a-z]#i', '', $model_no);
                if($this->getProperty($detail, 'oem', $isArray) != '') {
                    $this->mod_arr[] = "'".xtc_db_input($model_no)."'";
                }
                //EOC collect all model numbers, noRiddle
            }
            foreach ($additional as $detail) {
                //BOC collect all model numbers, noRiddle
                $model_no = $this->getProperty($detail, 'oem', $isArray);
                //$model_no = str_replace(array(' ', '-', '_'), '', $model_no);
                //$model_no = preg_replace('/\s+/', '', $model_no);
                $model_no = preg_replace('#[^0-9a-z]#i', '', $model_no);
                if($this->getProperty($detail, 'oem', $isArray) != '') {
                    $this->mod_arr[] = "'".xtc_db_input($model_no)."'";
                }
                //EOC collect all model numbers, noRiddle
            }
            
            //BOC split mod_arr if more than $this->chunk_no items (see config in __construct)
            /*if(count($this->mod_arr) > $this->chunk_no) {
                $this->chunked_mod_arr = array_chunk($this->mod_arr, $this->chunk_no, true);
            }
            for($cma = 0, $ccma = count($this->chunked_mod_arr); $cma <= $ccma; $cma++) {
                $this->modls = implode(',', $this->chunked_mod_arr[$cma]);
                $this->shp_data_arr = $this->find_prices_and_statuses_from_shop($this->modls);
            }*/
            //EOC split mod_arr if more than $this->chunk_no items (see config in __construct)
            
            $this->mod_arr = array_unique($this->mod_arr); //sometimes we have a lot of duplicates, 05-2020, noRiddle
            $this->modls = implode(',', $this->mod_arr);
            $this->shp_data_arr = $this->find_prices_and_statuses_from_shop($this->modls);
            //EOC get data from external shop, noRiddle
            
            foreach ($searched as $detail) {
                $html .= $this->DrawItem($catalog, $detail);
                $this->rowno++;
            }

            $html .= $this->DrawAdditionalItemSplitter();
            $this->row_class .= ' g_addgr g_addgr_collapsed ';

            foreach ($additional as $detail) {
                $html .= $this->DrawItem($catalog, $detail);
                $this->rowno++;
            }
        } else {
            //BOC get data from external shop, noRiddle
            foreach ($details as $detail) {
                //BOC collect all model numbers, noRiddle
                $model_no = $this->getProperty($detail, 'oem', $isArray);
                //$model_no = str_replace(array(' ', '-', '_'), '', $model_no);
                //$model_no = preg_replace('/\s+/', '', $model_no);
                $model_no = preg_replace('#[^0-9a-z]#i', '', $model_no);
                if($this->getProperty($detail, 'oem', $isArray) != '') {
                    $this->mod_arr[] = "'".xtc_db_input($model_no)."'";
                }
                //EOC collect all model numbers, noRiddle
            }
            foreach ($details->row as $detail) {
                //BOC collect all model numbers, noRiddle
                $model_no = $this->getProperty($detail, 'oem', $isArray);
                //$model_no = str_replace(array(' ', '-', '_'), '', $model_no);
                //$model_no = preg_replace('/\s+/', '', $model_no);
                $model_no = preg_replace('#[^0-9a-z]#i', '', $model_no);
                if($this->getProperty($detail, 'oem', $isArray) != '') {
                    $this->mod_arr[] = "'".xtc_db_input($model_no)."'";
                }
                //EOC collect all model numbers, noRiddle
            }
            
            //BOC split mod_arr if more than $this->chunk_no items (see config in __construct)
            /*if(count($this->mod_arr) > $this->chunk_no) {
                $this->chunked_mod_arr = array_chunk($this->mod_arr, $this->chunk_no, true);
            }
            for($cma = 0, $ccma = count($this->chunked_mod_arr); $cma <= $ccma; $cma++) {
                $this->modls = implode(',', $this->chunked_mod_arr[$cma]);
                $this->shp_data_arr = $this->find_prices_and_statuses_from_shop($this->modls);
            }*/
            //EOC split mod_arr if more than $this->chunk_no items (see config in __construct)
            
            $this->mod_arr = array_unique($this->mod_arr); //sometimes we have a lot of duplicates, 05-2020, noRiddle
            $this->modls = implode(',', $this->mod_arr);
            $this->shp_data_arr = $this->find_prices_and_statuses_from_shop($this->modls);
            //EOC get data from external shop, noRiddle
            
            foreach ($details as $detail) {
                $found = true;

                $html .= $this->DrawItem($catalog, $detail);
                $this->rowno++;
            }

            if (!$found)
                foreach ($details->row as $detail) {
                    $html .= $this->DrawItem($catalog, $detail);
                    $this->rowno++;
                }
        }

        //BOC END try to fix header, noRiddle
        $html .= '</tbody></table></div>';
        $html .= '</td></tr>';
        //EOC END try to fix header, noRiddle
        //BOC add tbody, noRiddle
        $html .= '</tbody>';
        //EOC add tbody, noRiddle
        $html .= '</table>';
        //BOC two submit buttons, noRiddle
        if(isset($_GET['coID']) && $_GET['coID'] == '1007') {
            $cl_sub_bott = 'subm-qdet-bott';
        } else {
            $cl_sub_bott = 'subm-bott';
        }
        
        //if(isset($_GET['coID']) && $_GET['coID'] != '1007') {
            //$html.= '<p class="subm-top"><button class="btn button-catalogue-cart" type="submit" title="'.$this->GetLocalizedString("nr_into_shopp_cart").'"><i class="glyphicon glyphicon-arrow-down"></i>&nbsp;<i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;'.$this->GetLocalizedString("nr_into_shopp_cart").'&nbsp;<i class="glyphicon glyphicon-arrow-down"></i></button></p>';
        if(in_array($_SESSION['customer_country_id'], $this->nr_activated_states)) { //customer can only buy if he is in shops countries array, noRiddle
            $html .= '<p class="subm-top">';
            $html .= '<span class="cssButton cssButtonColor2"><i class="fa fa-arrow-down"></i> <i class="fa fa-shopping-cart"></i><span class="cssButtonText">'.$this->GetLocalizedString("nr_into_shopp_cart").'</span><button type="submit" class="cssButtonText" title="'.$this->GetLocalizedString("nr_into_shopp_cart_t").'">'.$this->GetLocalizedString("nr_into_shopp_cart").'</button><i class="fa fa-shopping-cart"></i> <i class="fa fa-arrow-down"></i></span>';
            $html .= '</p>';
        //}

            $html .= '<p class="'.$cl_sub_bott.'">';
            $html .= '<span class="cssButton cssButtonColor2"><i class="fa fa-arrow-up"></i> <i class="fa fa-shopping-cart"></i><span class="cssButtonText">'.$this->GetLocalizedString("nr_into_shopp_cart").'</span><button type="submit" class="cssButtonText" title="'.$this->GetLocalizedString("nr_into_shopp_cart_t").'">'.$this->GetLocalizedString("nr_into_shopp_cart").'</button><i class="fa fa-shopping-cart"></i> <i class="fa fa-arrow-up"></i></span>';
            $html .= '</p>';
        } //END customer can only buy if he is in shops countries array, noRiddle
        //EOC two submit buttons, noRiddle
        //BOC end form tag, noRiddle
        $html .= '</form>';
        //EOC end form tag, noRiddle

        if ($this->rowno == 1)
            $html = $this->DrawEmptySet();

        return $html;
        
        //echo '<pre>'.print_r($details, true).'</pre>';
        
        //BOC make query to external shop and close shop db, noRiddle
        //return '<pre>'.print_r($this->mod_arr, true).'</pre>';
        //return '<pre>'.$this->modls.'</pre>';
        //return '<pre>'.print_r($this->shp_data_arr, true).'</pre>';
        //return '<pre>'.$this->shp_data_arr.'</pre>';
        
        //$this->close_shop_db(); //close external shop db connection, noRiddle | we do that in application_bottom (auto_include), noRiddle
        //EOC make query to external shop and close shop db, noRiddle
    }

    function DrawItem($catalog, $detail)
    {
        $html = $this->DrawDetailRow($detail, $this->columns, false, $catalog);

        return $html;
    }

    function DrawJavaScript()
    {
        $html = "<script type=\"text/javascript\">\n";
        $html .= "var opennedimage = '" . $this->opennedimage . "'; \n";
        $html .= "var closedimage = '" . $this->closedimage . "'; \n";
        $html .= "jQuery(document).ready(function($){ \n";
        //BOC don't need tooltip since we display data directly in table. noRiddle
        //$html .= "jQuery('td.g_rowdatahint').tooltip({track: true, delay: 0, showURL: false, fade: 250, positionLeft: true, bodyHandler: g_getHint}); \n";
        //EOC don't need tooltip since we display data directly in table. noRiddle

        //BOC modified because of use of font icon, noRiddle
        //$html .= "jQuery('img.g_addtocart').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '" . $this->GetLocalizedString("AddToCartHint") . "'; } }); \n";
        //BOC we don't use cart icon here, in case of 'not available' we show an info icon which links to the external shops contact form, noRiddle
        //$html .= "jQuery('.icon-shopping-cart').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '" . $this->GetLocalizedString("AddToCartHint") . "'; } }); \n";
        $html .= "jQuery('.unit-inf-ic').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '" . $this->GetLocalizedString("price_question_txt") . "'; } }); \n";
        //EOC modified because of use of font icon, noRiddle
        //BOC not available tooltip, noRiddle
        $html .= "jQuery('.noavail-inf-ic').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '" . $this->GetLocalizedString('nr_not_available') . "'; } }); \n";
        //EOC not available tooltip, noRiddle
        //BOC product replaced, noRiddle
        $html .= "jQuery('.repl-inf-ic').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '" . $this->GetLocalizedString('nr_prod_replaced') . "'; } }); \n";
        //EOC product replaced, noRiddle

        $html .= "jQuery('td[name=c_toggle] img').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '" . $this->GetLocalizedString("ToggleReplacements") . "'; } }); \n";

        $html .= "jQuery('img.c_rfull').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '<h3>" . $this->GetLocalizedString("ReplacementWay") . "</h3>" . $this->GetLocalizedString("ReplacementWayFull") . "'; } }); \n";

        $html .= "jQuery('img.c_rforw').tooltip({track: true, delay: 0, showURL: false, fade: 250,	bodyHandler: function() { return '<h3>" . $this->GetLocalizedString("ReplacementWay") . "</h3>" . $this->GetLocalizedString("ReplacementWayForward") . "'; } }); \n";

        $html .= "jQuery('img.c_rbackw').tooltip({track: true, delay: 0, showURL: false, fade: 250, bodyHandler: function() { return '<h3>" . $this->GetLocalizedString("ReplacementWay") . "</h3>" . $this->GetLocalizedString("ReplacementWayBackward") . "'; } }); \n";
        $html .= "});\n";
        $html .= "</script>\n";

        return $html;
    }

    function getProperty($detail, $propertyName, $isArray)
    {
        if ($isArray) {
            return $detail->$propertyName;
        }

        return $detail[$propertyName];
    }

    function DrawEmptySet()
    {
        return $this->GetLocalizedString('nothingfound');
    }

    function DrawDetailRow($detail, $columns, $isArray, $catalog)
    {   
        //BOC don't display 'no more available' and 'Quantity' == 'X', noRiddle
        //if(strpos($name, 'nicht mehr lieferbar') === false && strpos($name, 'no more available') === false && strtoupper($nr_dl_qty) != 'X') { //sometimes Quantity is just X, noRiddle
            $html = '<tr';

            //$detail->addAttribute('filter', '*');
            $filter = $this->getProperty($detail, 'filter', $isArray);
            $bits   = (int)$this->getProperty($detail, 'flag', $isArray);

            $pnc = $this->getProperty($detail, 'codeonimage', $isArray);

            $html .= ' class="' . $this->row_class . (strlen($filter) > 0 ? 'g_filter_row ' : '') . ($bits ? ' g_nonstandarddetail ' : '') . 'g_collapsed g_highlight"';
            $html .= ' name="' . (isset($pnc) ? $pnc : 'd_' . $this->rowno) . '"';
            $html .= ' id="d_' . $this->rowno . '"';
            $html .= ' onmouseout="hl(this, \'out\');" onmouseover="hl(this, \'in\');">';
            
            $ct = 0; //add count for class, noRiddle
            foreach ($this->basic_columns as $column => $visibility) {
                $ct++; //increase count, noRiddle
                $html .= $this->DrawDetailCell($detail, strtolower($column), $visibility, $isArray, $ct); //added count param $ct, noRiddle
            }
                //$html .= '<td>'.$column.' => '.$visibility.'</td>';
                //$html .= '<pre>'.print_r($this->basic_columns, true).'</pre>';
                //$html .= '<pre>'.print_r($detail, true).'</pre>';

            $html .= '</tr>';
        //}

        return $html;
        //return '<pre>'.print_r($this->basic_columns, true).'</pre>';
    }

    function DrawDetailCell($detail, $column, $visibility, $isArray, $ct) //added count param $ct, noRiddle
    {
        //BOC no cart column, noRiddle
        if($column != 'cart') {
            $html = '<td name="c_' . $column . '"';

            //BOC display OEM always, noRiddle
            //if (($visibility & 1) == 0) {
            if (($visibility & 1) == 0 && $column != 'oem' && $column != 'chkb') { 
                $html .= ' style="display:none;"';
            }
            //EOC display OEM always, noRiddle
            if ($column == 'tooltip') {
                $html .= ' class="g_rowdatahint td_'.$ct.'"'; //added count class, noRiddle
            } elseif (($visibility & 2) > 0) {
                $html .= ' class="g_ttd td_'.$ct.'"'; //added count class, noRiddle
            } else { //added else to add count class everywhere, noRiddle
                $html .= ' class="td_'.$ct.'"';
            }
            $html .= '>';

            $html .= $this->DrawDetailCellValue($detail, $column, $visibility, $isArray);

            $html .= '</td>';
            return $html;
        }
        //BOC no cart column, noRiddle
    }

    function GetFilterURL($detail, $isArray)
    {
        $filter = $this->getProperty($detail, 'filter', $isArray);
        if (!strlen($filter)) {
            return false;
        }

        return $this->FormatLink('filter', $detail, $this->catalog);
    }

    function DrawDetailCellValue($detail, $column, $visibility, $isArray)
    {
        //global $xtPrice, $main, $groups_other_price_arr_1, $groups_other_price_arr_8; //make our new price class, main class and group arrays available, noRiddle
        global $nr_foreign_xtcPrice, $main, $data20, $the_shop, $shop_suffix, $remote_url, $cat_env; //$the_shop defined in shop_dbs/conn_to_shop_db.php
        
        $name = trim($this->getProperty($detail, 'name', $isArray)); //define this here (see below) and trim, noRiddle
        $name = str_replace(';', '; ', $name); //have empty spaces between ; for automatic line breaks, noRiddle
        $out = array(); //define var, noRiddle
        //BOC write attributes in array, noRiddle
        foreach ($detail->attribute as $attr) {
            $out[(string)$attr->attributes()->name] = (string)$attr->attributes()->value;
        }
        //EOC write attributes in array, noRiddle
        
        //BOC filter if replacing part exists
        /*if(array_key_exists('replPart', $out)) {
            $repl_arr = explode(':', $out['replPart']);
            $model_no = $repl_arr[1];
        } else {*/
            $model_no = $this->getProperty($detail, 'oem', $isArray);
        //}

        //$model_no = str_replace(array(' ', '-', '_'), '', $model_no);
        //$model_no = preg_replace('/\s+/', '', $model_no);
        $model_no = preg_replace('#[^0-9a-z]#i', '', $model_no);
        //$model_no = ltrim($model_no, '0');
        //EOC filter if replacing part exists
        
        //BOC get needed quantity, noRiddle
        $nr_oem = strtoupper($model_no);
        $nr_oem = strtoupper($nr_oem);
        if(array_key_exists('Mengeneinheit', $out) || array_key_exists('Quantity', $out) || (isset($this->shp_data_arr[$nr_oem]['gm_min_order']) && $this->shp_data_arr[$nr_oem]['gm_min_order'] > 1)) {
            if(array_key_exists('Mengeneinheit', $out)) {
                //if(strtoupper($out['Mengeneinheit']) == 'X') {
                if(!preg_match('#[0-9]#', $out['Mengeneinheit'])) {
                    $nr_dl_qty = '1';
                } else {
                    $nr_dl_qty = ltrim($out['Mengeneinheit'], '0');
                }
            } else if(array_key_exists('Quantity', $out)) {
                //if(strtoupper($out['Quantity']) == 'X') {
                if(!preg_match('#[0-9]#', $out['Quantity'])) {
                    $nr_dl_qty = '1';
                } else {
                    $nr_dl_qty = ltrim($out['Quantity'], '0');
                }
            } else if($this->shp_data_arr[$nr_oem]['gm_min_order'] > 1) {
                $nr_dl_qty = $this->shp_data_arr[$nr_oem]['gm_min_order'];
            }
        } else {
            $nr_dl_qty = '1';
        }
        //EOC get needed quantity, noRiddle
        
        //BOC if $nr_oem not found in shop or found and products_status = '0', new 11-2017, noRiddle
        /*if(!array_key_exists($nr_oem, $this->shp_data_arr) || (array_key_exists($nr_oem, $this->shp_data_arr) && $this->shp_data_arr[$nr_oem]['products_status'] == '0')) {
            $shp_data_arr_like = $this->find_prices_and_statuses_from_shop_like($nr_oem);
            $this->shp_data_arr[$nr_oem] = $shp_data_arr_like[$nr_oem];
            
            //set our new products_model from shop as value in checkbox
            $chbx_val_like = $shp_data_arr_like[$nr_oem]['products_model'];
            $chbx_val_like = preg_replace('#[^0-9a-z]#i', '', $chbx_val_like);
            $detail['chkb'] = xtc_draw_checkbox_field('check_to_buy[]', $chbx_val_like);
        }*/
        //EOC if $nr_oem not found in shop or found and products_status = '0', new 11-2017, noRiddle
        
        //BOC build price array and get tax info, noRiddle
        if(isset($this->shp_data_arr[$nr_oem]['products_id'])) {
            $nr_dl_tax_rate = isset($nr_foreign_xtcPrice->TAX[$this->shp_data_arr[$nr_oem]['products_tax_class_id']]) ? $nr_foreign_xtcPrice->TAX[$this->shp_data_arr[$nr_oem]['products_tax_class_id']] : 0;
            $nr_dl_price = $nr_foreign_xtcPrice->xtcGetPrice($this->shp_data_arr[$nr_oem]['products_id'], $format = true, 1, $this->shp_data_arr[$nr_oem]['products_tax_class_id'], $this->shp_data_arr[$nr_oem]['products_price'], 1);
        }

        $nr_dl_tx_inf = isset($this->shp_data_arr[$nr_oem]['gm_price_status']) && $this->shp_data_arr[$nr_oem]['gm_price_status'] == '0' ? ($nr_dl_tax_rate != TAX_INFO_EXCL ? '('.$main->getTaxInfo($nr_dl_tax_rate).')' : '') : '';
        //EOC build price array and get tax info, noRiddle
        
        //BOC don't display 'no more available' and 'Quantity' == 'X', noRiddle
        //if(strpos($name, 'nicht mehr lieferbar') === false && strpos($name, 'no more available') === false && strtoupper($nr_dl_qty) != 'X') { //sometimes Quantity is just X, noRiddle
        $shp202_arr = array(); //array('mercedes');
        $act_coid = !in_array($_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'], $shp202_arr) ? '7' : '500';
        
            switch ($column) {
                //BOC add new column, noRiddle
                case 'chkb':
                    if(isset($this->shp_data_arr[$nr_oem]['products_id']) && $this->shp_data_arr[$nr_oem]['products_id'] != '') {
                        $inf_link = $remote_url[$cat_env]['domain'].'/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].$shop_suffix.'/shop_content.php?coID='.$act_coid.'&products_id='.$this->shp_data_arr[$nr_oem]['products_id'].'&products_name='.$name.'&products_model=Model-No.:'.$nr_oem;
                    } else {
                        $inf_link = $remote_url[$cat_env]['domain'].'/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].$shop_suffix.'/shop_content.php?coID='.$act_coid.'&products_id=&products_name='.$name.'&products_model=Model-No.:'.$nr_oem;
                    }
                    
                    //if(!empty($this->shp_data_arr) && $this->shp_data_arr[$nr_oem]['products_id'] != '') {
                    if(in_array($_SESSION['customer_country_id'], $this->nr_activated_states)) { //customer can only buy if he is in shops countries array, noRiddle
                        if(!empty($this->shp_data_arr) && isset($this->shp_data_arr[$nr_oem]['products_id'])) {
                            switch(true) {
                                case array_key_exists('replPart', $out):
                                    return '';
                                break;
                                case !isset($this->shp_data_arr[$nr_oem]['gm_price_status']):
                                    return '';
                                break;
                                case $this->shp_data_arr[$nr_oem]['products_status'] != '1':
                                    return '';
                                break;
                                case $this->shp_data_arr[$nr_oem]['gm_price_status'] == '0' && strpos($name, 'nicht mehr lieferbar') === false && strpos($name, 'no more available') === false:
                                    return $this->getProperty($detail, 'chkb', $isArray);
                                break;
                                case $this->shp_data_arr[$nr_oem]['gm_price_status'] == '0':
                                    return $this->getProperty($detail, 'chkb', $isArray);
                                break;
                                case $this->shp_data_arr[$nr_oem]['gm_price_status'] == '1':
                                    return ''; //'<div class="unit-inf-ic"><a href="'.$inf_link.'" target="_blank"><i class="icon-info"></i></a></div>';
                                break;
                                case $this->shp_data_arr[$nr_oem]['gm_price_status'] == '2':
                                    return '';
                                break;
                            }
                        } else {
                            switch(true) {
                                case array_key_exists('replPart', $out):
                                    return '';
                                break;
                                case strpos($name, 'nicht mehr lieferbar') === false && strpos($name, 'no more available') === false:
                                    return ''; //$this->getProperty($detail, 'chkb', $isArray);
                            }
                        }
                    } else { //END customer can only buy if he is in shops countries array, noRiddle
                        return '';
                    }
                    
                    /*if($this->shp_data_arr[$nr_oem]['gm_price_status'] == '0' && strpos($name, 'nicht mehr lieferbar') === false && strpos($name, 'no more available') === false) {
                        return $this->getProperty($detail, 'chkb', $isArray);
                    } else if($this->shp_data_arr[$nr_oem]['gm_price_status'] == '1') {
                        return '<div class="unit-inf-ic"><a href="'.$inf_link.'" target="_blank"><i class="icon-info"></i></a></div>';
                    } else if($this->shp_data_arr[$nr_oem]['gm_price_status'] == '2') {
                        return '';
                    }*/
                //EOC add new column, noRiddle
                case 'pnc':
                    return $this->getProperty($detail, 'codeonimage', $isArray);

                case 'oem':
                    return strtoupper($nr_oem); //$this->getProperty($detail, 'oem', $isArray);

                case 'amount':
                    return $this->getProperty($detail, 'amount', $isArray);

                case 'name':
                    $link = $this->FormatLink('detail', $detail, $this->catalog);

                    $html = ''; //'<a href="'.$link.'" target="_blank">'; // added target blank, noRiddle
                    //$name = $this->getProperty($detail, 'name', $isArray); //define this obove before switch, noRiddle
                    if (!strlen((string)$name)) {
                        //$name = 'Наименование не указано';
                        $name = $this->GetLocalizedString('name_not_known'); //localize this, noRiddle
                    }
                    
                    //BOC calculate price, noRiddle //test mercedes model: A1648100819
                    //if(!empty($this->shp_data_arr) && $this->shp_data_arr[$nr_oem]['products_id'] != '') {
                    if(!empty($this->shp_data_arr) && isset($this->shp_data_arr[$nr_oem]['products_id']) && $this->shp_data_arr[$nr_oem]['gm_price_status'] == '0') {
                        //BOC format price like in our new s_price_listing.html, noRiddle
                        //BOC Produkt Sonderpreis Langform mit Ausgabe Alter Preis, Neuer Preis, Sie sparen
                        if($nr_dl_price['flag'] == 'Special' || $nr_dl_price['flag'] == 'SpecialDiscount') {
                            $dl_price = ' <span class="s_old_price">'
                                               .'<span class="s_small_price">'.INSTEAD.'</span>'.$nr_dl_price['old_price']
                                        .'</span>'
                                        .'<span class="s_new_price">'
                                            .'<span class="price-green"><span class="s_small_price">'.ONLY.'</span> '.$nr_dl_price['special_price'].'</span>'
                                        .'</span>'
                                        .'<span class="s_save_price">'.YOU_SAVE.' '.$nr_dl_price['save_percent'].'% / '.$nr_dl_price['save_diff'].'</span>';
                        //EOC Produkt Sonderpreis Langform mit Ausgabe Alter Preis, Neuer Preis, Sie sparen
                        } else if($nr_dl_price['flag'] == 'SpecialGraduated') {
                            if($nr_dl_price['uvp'] != '') {
                                //BOC Produkt UVP Preis mit Ausgabe Ihr Preis, UVP
                                //BOC filter whether old/new price shall be shown (see nr_foreign_xtcPrice class}
                                if($nr_dl_price['show_persoff'] === true) {
                                    $dl_price = '<span class="s_uvp_price">'
                                                     .'<span class="price-green"><span class="s_small_price">'.YOUR_PRICE.'</span> '.$nr_dl_price['special_price'].'</span>'
                                                 .'</span>'
                                                 .'<span class="s_item_price"><span class="s_small_price">'.MSRP.'</span> '.$nr_dl_price['old_price'].'</span>';
                                } else {
                                    $dl_price = '<span class="s_uvp_price">'
                                                   .'<span class="price-green"><span class="s_small_price">'.YOUR_PRICE.'</span> '.$nr_dl_price['special_price'].'</span>'
                                                .'</span>';
                                }
                            //EOC filter whether old/new price shall be shown (see nr_foreign_xtcPrice class}
                            //BOC Produkt UVP Preis mit Ausgabe Ihr Preis, UVP
                            } else {
                                //BOC Produkt Staffelpreis mit Ausgabe ab Preis, Stückpreis
                                $dl_price = '<span class="s_graduated_price">'
                                                .'<span class="price-green"><span class="s_small_price">'.FROM.'</span> '.$nr_dl_price['old_price'].'</span>'
                                            .'</span>'
                                            .'<span class="s_item_price"><span class="s_small_price">'.UNIT_PRICE.'</span> '.$nr_dl_price['special_price'].'</span>';
                                //EOC Produkt Staffelpreis mit Ausgabe ab Preis, Stückpreis
                            }
                        } else if($nr_dl_price['flag'] == 'NotAllowed') {
                            $dl_price = '<span class="s_no_price">'.$nr_dl_price['not_allowed'].'</span>';
                        } else {
                            //BOC Produkt Standardpreis mit Ausgabe ab Preis
                            $dl_price = '<span class="s_standard_price"><span class="price-green">';
                                if($nr_dl_price['from'] != '') {
                                    $dl_price .= '<span class="s_small_price">'.$nr_dl_price['from'].'</span>';
                                }
                                $dl_price .= $nr_dl_price['standard_price'];
                            $dl_price .= '</span></span>';
                            //EOC Produkt Standardpreis mit Ausgabe ab Preis
                        }
                        //EOC format price like in our new s_price_listing.html, noRiddle
                    } else {
                        $dl_price = '';
                    }
                    //EOC calculate price, noRiddle

                    //$html .= ' &raquo; ' . $name .($oldprice != '' ? '<br />'.$oldprice.' ('.$tx_inf.')' : '').'<br /><span class="price-green">'.$fin_price.' ('.$tx_inf.')</span>'; //added price, noRiddle
                    $html .= ' &raquo; ' . $name . '<div class="clb_price">' . $dl_price.'</div> <div class="clb_tax">'.$nr_dl_tx_inf.'</div>'; //added price, noRiddle
                    
                    //BOC show all details, no use of toolttip to show them, noRiddle
                    foreach ($detail->attribute as $attr) {
                        if((string)$attr->attributes()->name == 'replPart') {
                            $html .= '<span class="col67 red">'.(string)$attr->attributes()->name.': '.(string)$attr->attributes()->value.'</span><br /><span class="col67 red">'.$this->GetLocalizedString('txt_see_repl_part').'</span><br />';
                        } else {
                            /*BOC try to catch "Anmerkung: .... Bildtafel:; 906-015;", meaning another image
                            (example: https://www.online-teile.com/oem-kataloge/nr_vehicle_1004.html?c=VW1221&vid=1360&cid=2&ssd=$*KwEEMCEJcQZVYn4CWUc9IFxIaG9xAQIIBhEXGF53SxNUFRAIT0MrYRUcERREXRBPW10UZXRmFBsQEAxDXQwXBggGGxAQAkNdDBcScncVTQAAAABXJQ4F$)
                            and click 253-070 and then part 15)
                            mmmh, can't get link back to image search
                            */
                            $att_val = (string)$attr->attributes()->value;
                            if(preg_match('#[0-9]+-[0-9]+#', $att_val, $catch)) {
                                $new_pic = $catch[0];
                            } else {
                                $new_pic = '';
                            }
                            //EOC try to catch "Anmerkung: .... Bildtafel:; 906-015;", meaning another image
                            
                            $html .= '<span class="col67">'.(string)$attr->attributes()->name.': '.str_replace(array(';;', ';', ','), array('', '; ', ', '), (string)$attr->attributes()->value).'</span><br />'; //fix presentation, noRiddle
                        }
                    }
                    
                    //BOC handle products_replace
                    if(isset($this->shp_data_arr[$nr_oem])) {
                        if($this->shp_data_arr[$nr_oem]['gm_price_status'] == '2' && $this->shp_data_arr[$nr_oem]['products_status'] == '1' && !empty(trim($this->shp_data_arr[$nr_oem]['products_replace'])) && strtolower($this->shp_data_arr[$nr_oem]['products_replace']) != 'anfrage') {
                            $html .= '<br /><a href="" class="prod-repl-dat" data-prod_repl="'.trim($this->shp_data_arr[$nr_oem]['products_replace']).'">&raquo; '.S_PART_WAS_REPLACED_WITH.'<span>'.trim($this->shp_data_arr[$nr_oem]['products_replace']).'</span></a>';
                        }
                    }
                    //EOC handle products_replace
                    //EOC show all details, no use of toolttip to show them, noRiddle
                    
                    return $html;
                    //return '<pre>'.print_r($this->shp_data_arr, true).'</pre>'; //$this->shp_data_arr[$nr_oem]['products_price'];

                case 'note':
                    return str_replace("\n", "<br>", $this->getProperty($detail, 'note', $isArray));

                case 'tooltip':
                    if(isset($this->shp_data_arr[$nr_oem]['products_id']) && $this->shp_data_arr[$nr_oem]['products_id'] != '') {
                        $inf_link = $remote_url[$cat_env]['domain'].'/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].$shop_suffix.'/shop_content.php?coID='.$act_coid.'&products_id='.$this->shp_data_arr[$nr_oem]['products_id'].'&products_name='.$name.'&products_model=Model-No.:'.$nr_oem;
                    } else {
                        $inf_link = $remote_url[$cat_env]['domain'].'/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].$shop_suffix.'/shop_content.php?coID='.$act_coid.'&products_id=&products_name='.$name.'&products_model=Model-No.:'.$nr_oem;
                    }
                    
                    //if(!empty($this->shp_data_arr) && $this->shp_data_arr[$nr_oem]['products_id'] != '') {
                    if(!empty($this->shp_data_arr) && isset($this->shp_data_arr[$nr_oem]['products_id'])) {
                        $hid_price_field = xtc_draw_hidden_field('prod_price[]', ($this->shp_data_arr[$nr_oem]['specials_new_products_price'] != '' ? $this->shp_data_arr[$nr_oem]['specials_new_products_price'] : ($this->shp_data_arr[$nr_oem]['personal_offer'] != '' ? $this->shp_data_arr[$nr_oem]['personal_offer'] : $this->shp_data_arr[$nr_oem]['products_price'])));
                        //$hid_price_field = xtc_draw_hidden_field('prod_price[]', $nr_dl_price['plain']); //use brutto price ?

                        $all_inpt_fields = xtc_draw_input_field('prod_model_qty[]', $nr_dl_qty, 'class="clog-detlist-ipt"')
                                          .xtc_draw_hidden_field('products_model[]', $this->shp_data_arr[$nr_oem]['products_model']) //was $nr_oem, use $this->shp_data_arr[$nr_oem]['products_model'], new 11-2017, noRiddle
                                          .xtc_draw_hidden_field('products_id[]', $this->shp_data_arr[$nr_oem]['products_id'])
                                          .xtc_draw_hidden_field('products_name[]', $name)
                                          //.xtc_draw_hidden_field('prod_brand[]', Config::$nr_brand_arr[$_GET['c']])
                                          .xtc_draw_hidden_field('prod_brand[]', $_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'])
                                          .xtc_draw_hidden_field('lax_brand', $_GET['c'])
                                          .$hid_price_field
                                          .xtc_draw_hidden_field('prod_txcl_id[]', $this->shp_data_arr[$nr_oem]['products_tax_class_id']);
                        $no_qty_all_inpt_fields = xtc_draw_hidden_field('products_model[]', $this->shp_data_arr[$nr_oem]['products_model']) //was $nr_oem, use $this->shp_data_arr[$nr_oem]['products_model'], new 11-2017, noRiddle
                                                 .xtc_draw_hidden_field('products_id[]', $this->shp_data_arr[$nr_oem]['products_id'])
                                                 .xtc_draw_hidden_field('products_name[]', $name)
                                                 //.xtc_draw_hidden_field('prod_brand[]', Config::$nr_brand_arr[$_GET['c']])
                                                 .xtc_draw_hidden_field('prod_brand[]', $_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'])
                                                 .xtc_draw_hidden_field('lax_brand', $_GET['c'])
                                                 .$hid_price_field
                                                 .xtc_draw_hidden_field('prod_txcl_id[]', $this->shp_data_arr[$nr_oem]['products_tax_class_id']);
                        
                        switch(true) {
                            case array_key_exists('replPart', $out):
                                $nr_ipt_field = '';
                            break;
                            case !isset($this->shp_data_arr[$nr_oem]['gm_price_status']):
                                //$nr_ipt_field = '<span class="noavail-inf-ic"><i class="icon-info"></i></span>'; //$no_qty_all_inpt_fields;
                                //$nr_ipt_field = '<span class="noavail-inf-ic"><img src="'.$this->noavail_img.'" alt="not available" /></span>';
                                $nr_ipt_field = '<div class="unit-inf-ic"><a href="'.$inf_link.'" target="_blank"><img src="'.$this->pricequest_img.'" alt="price question" /></a></div>';
                            break;
                            case isset($this->shp_data_arr[$nr_oem]['products_status']) && $this->shp_data_arr[$nr_oem]['products_status'] != '1':
                                //$nr_ipt_field = '<span class="noavail-inf-ic"><i class="icon-info"></i></span>'; //$no_qty_all_inpt_fields;
                                //$nr_ipt_field = '<span class="noavail-inf-ic"><img src="'.$this->noavail_img.'" alt="not available" /></span>';
                                $nr_ipt_field = '<div class="unit-inf-ic"><a href="'.$inf_link.'" target="_blank"><img src="'.$this->pricequest_img.'" alt="price question" /></a></div>';
                            break;
                            case $this->shp_data_arr[$nr_oem]['gm_price_status'] == '0' && strpos($name, 'nicht mehr lieferbar') === false && strpos($name, 'no more available') === false:
                                $nr_ipt_field = $all_inpt_fields;
                            break;
                            case $this->shp_data_arr[$nr_oem]['gm_price_status'] == '0':
                                $nr_ipt_field = $all_inpt_fields;
                            break;
                            case $this->shp_data_arr[$nr_oem]['gm_price_status'] == '1' || (!empty(trim($this->shp_data_arr[$nr_oem]['products_replace'])) && strtolower($this->shp_data_arr[$nr_oem]['products_replace']) == 'anfrage'):
                                //$nr_ipt_field = '<div class="unit-inf-ic"><a href="'.$inf_link.'" target="_blank"><i class="icon-info"></i></a></div>'; //'.$no_qty_all_inpt_fields.'
                                $nr_ipt_field = '<div class="unit-inf-ic"><a href="'.$inf_link.'" target="_blank"><img src="'.$this->pricequest_img.'" alt="price question" /></a></div>';
                            break;
                            case $this->shp_data_arr[$nr_oem]['gm_price_status'] == '2' && $this->shp_data_arr[$nr_oem]['products_status'] == '1' && !empty(trim($this->shp_data_arr[$nr_oem]['products_replace'])) && strtolower($this->shp_data_arr[$nr_oem]['products_replace']) != 'anfrage':
                                $nr_ipt_field = '<span class="repl-inf-ic"><img src="'.$this->prodrepl_img.'" alt="not available" /></span>';
                            break;
                            case ($this->shp_data_arr[$nr_oem]['gm_price_status'] == '2' && empty(trim($this->shp_data_arr[$nr_oem]['products_replace']))):
                                //$nr_ipt_field = '<span class="noavail-inf-ic"><i class="icon-info"></i></span>'; //$no_qty_all_inpt_fields; $this->GetLocalizedString('nr_not_available')
                                $nr_ipt_field = '<span class="noavail-inf-ic"><img src="'.$this->noavail_img.'" alt="not available" /></span>';
                            break;
                        }
                    } else {
                        if(!isset($data20[$the_shop]) || (isset($data20[$the_shop]) && $data20[$the_shop]['sql_user'] == '')) { //new case when shop not online, 09-2018, noRiddle
                            $nr_ipt_field = '<div class="unit-inf-ic"><a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=20').'" target="_blank"><img src="'.$this->pricequest_img.'" alt="price question" /></a></div>';
                        } else {
                            $nr_ipt_field = '<div class="unit-inf-ic"><a href="'.$inf_link.'" target="_blank"><img src="'.$this->pricequest_img.'" alt="price question" /></a></div>';
                        }
                    }
                    
                    /*if($this->shp_data_arr[$nr_oem]['gm_price_status'] == '0' && strpos($name, 'nicht mehr lieferbar') === false && strpos($name, 'no more available') === false) {
                        $nr_ipt_field = xtc_draw_input_field('prod_model_qty[]', $nr_dl_qty, 'class="clog-detlist-ipt"')
                                        .xtc_draw_hidden_field('products_model[]', $nr_oem);
                    } else if($this->shp_data_arr[$nr_oem]['gm_price_status'] == '1') {
                        $nr_ipt_field = '<div class="unit-inf-ic"><a href="'.$inf_link.'" target="_blank"><i class="icon-info"></i></a></div>';
                    } else if($this->shp_data_arr[$nr_oem]['gm_price_status'] == '2') {
                        $nr_ipt_field = '';
                    }*/
                    //}
                    
                    return '<div class=".addtobasket_input">'.$nr_ipt_field.'</div>';
                    //EOC use input field for quantity here, noRiddle

                case 'flag':
                    $bits  = (int)$this->getProperty($detail, 'flag', $isArray);
                    $flags = '';
                    if ($bits & 1 > 0) {
                        $flags .= 'Нестандартная деталь';
                    }
                    return $flags;

                default:
                    return str_replace("\n", "<br>", $this->getProperty($detail, $column, $isArray));
            }
        //} //EOC don't display 'no more available' and 'Quantity' == 'X', noRiddle
    }

    function DrawHeader($columns)
    {
        //BOC add thead and class for count, noRiddle
        //$html = '<tr>';
        $html = '<thead><tr>';
        $ct = 0;
        $this->cnt_dspl = 0; //count only columns to display, noRiddle
        foreach ($columns as $column => $visibility) {
            $ct++;
            $html .= $this->DrawHeaderCell(strtolower($column), $visibility, $ct);
            
            //if(($visibility & 1) == 0 && $column != 'oem' && $column != 'chkb') {
            if($column != 'PNC' && $column != 'OEM' && $column != 'Name' && $column != 'Tooltip' && $column != 'CHKB') {
                $this->cnt_dspl--; //substract not displayed columns, noRiddle
            }
        }

        $html .= '</tr></thead>';
        //EOC add thead and class for count, noRiddle
        
        $this->cnt_dspl += $ct; //add total (displayed and not displayed) columns, noRiddle
        if($_GET['coID'] == '1007') $this->cnt_dspl -= 1; //substract cart column (see in DrawDetailCell() ), noRiddle

        return $html;
        //return '<pre>'.print_r($columns, true).'</pre>';
        //return '<pre>'.$ct.' | '.$this->cnt_dspl.'</pre>';
    }

    function DrawHeaderCell($column, $visibility, $ct) //add count, noRiddle
    {
        //BOC display OEM and checkbox column always and not cart column, noRiddle
        //return '<th id="c_' . $column . '"' . (($visibility & 1) == 0 ? ' style="display:none;"' : '') . '>' . $this->DrawHeaderCellValue($column, $visibility) . '</th>';
        if($column != 'cart') {
            return '<th class="c_' . $column . ' th_'.$ct.'"' . ((($visibility & 1) == 0 && $column != 'oem' && $column != 'chkb') ? ' style="display:none;"' : '') . '>' . $this->DrawHeaderCellValue($column, $visibility) . '</th>';
        }
        //EOC display OEM and checkbox column always and not cart column, noRiddle
    }

    function DrawHeaderCellValue($column, $visibility)
    {
        switch ($column) {
            //BOC new checkbox cell, noRiddle
            case 'chkb':
                return $this->GetLocalizedString('ColumnDetailChkb');
            //EOC new checkbox cell, noRiddle
            case 'toggle':
                return '&nbsp;';

            case 'pnc':
                return $this->GetLocalizedString('ColumnDetailCodeOnImage');

            case 'oem':
                return $this->GetLocalizedString('ColumnDetailOEM');

            case 'amount':
                return $this->GetLocalizedString('ColumnDetailAmount');

            case 'name':
                return $this->GetLocalizedString('ColumnDetailName');

            case 'cart':
                return '&nbsp;';

            case 'price':
                return $this->GetLocalizedString('ColumnDetailPrice');

            case 'note':
                return $this->GetLocalizedString('ColumnDetailNote');

            case 'tooltip':
                return $this->GetLocalizedString('ColumnDetailQty');

            case 'availability':
                return $this->GetLocalizedString('WhereToBuy');

            default:
                return $this->GetLocalizedString('ColumnDetail' . $column);
        }

        return '';
    }

    function DrawAdditionalItemSplitter()
    {
        //BOC use font icon here, see above, noRiddle
        /*return '<tr>
            <td colspan="' . count($this->columns) . '">
                <img class="g_additional_toggler g_addcollapsed" class="g_addcollapsed" src="' . $this->closedimage . '" width="16" height="16" onclick="g_toggleAdditional(\'g_DetailTable' . GuayaquilDetailsList::$table_no . '\', opennedimage, closedimage);">
                <a href="#" onClick="g_toggleAdditional(\'g_DetailTable' . GuayaquilDetailsList::$table_no . '\', opennedimage, closedimage); return false;"> '.$this->GetLocalizedString('otherunitparts').'</a>
            </td>
        </tr>';*/
        
        //BOC try button here, noRiddle
        /*return '<tr>
            <td colspan="' . count($this->columns) . '">
                <span class="g_additional_toggler g_addcollapsed" onclick="g_toggleAdditional(\'g_DetailTable' . GuayaquilDetailsList::$table_no . '\', opennedimage, closedimage);">' . $this->closedimage . '</span>
                <a href="#" onClick="g_toggleAdditional(\'g_DetailTable' . GuayaquilDetailsList::$table_no . '\', opennedimage, closedimage); return false;"> '.$this->GetLocalizedString('otherunitparts').'</a>
            </td>
        </tr>';*/
        
        /*return '<tr>
            <td colspan="' . count($this->columns) . '">
                <span class="g_additional_toggler g_addcollapsed" onclick="g_toggleAdditional(\'g_DetailTable' . GuayaquilDetailsList::$table_no . '\', opennedimage, closedimage);">' . $this->closedimage . '</span>
                <a href="#" onClick="g_toggleAdditional(\'g_DetailTable' . GuayaquilDetailsList::$table_no . '\', opennedimage, closedimage); return false;"><span class="btn button-catalogue">'.$this->GetLocalizedString('otherunitparts').'</span></a>
            </td>
        </tr>';*/
        
        /*$html .= '<a class="gWizardVehicleLink" href="'.$this->FormatLink('vehicles', $wizard, $catalog).'" title="'.$this->GetLocalizedString('List vehicles').'"><span class="cssButton cssButtonColor1"><i class="fa fa-eye"></i><span class="cssButtonText">';
        $html .= $this->GetLocalizedString('List vehicles');
        $html .= '</span></span></a>';*/
        
        //$it_split_butt = '<tr><td colspan="' . count($this->columns) . '">';
        $need_rest_colsp = $this->cnt_dspl - 2; //rest of needed cells, noRiddle
        $it_split_butt = '<tr><td colspan="2">';
        //$it_split_butt .= '<span class="g_additional_toggler g_addcollapsed" onclick="g_toggleAdditional(\'g_DetailTable' . GuayaquilDetailsList::$table_no . '\', opennedimage, closedimage);">' . $this->closedimage . '</span>';
        $it_split_butt .= '<a href="#" onClick="g_toggleAdditional(\'g_DetailTable' . GuayaquilDetailsList::$table_no . '\', opennedimage, closedimage); return false;">';
        $it_split_butt .= '<span class="cssButton cssButtonColor3"><span class="cssButtonText"><span class="g_additional_toggler g_addcollapsed" onclick="g_toggleAdditional(\'g_DetailTable' . GuayaquilDetailsList::$table_no . '\', opennedimage, closedimage);">' . $this->closedimage . '</span> '.$this->GetLocalizedString('otherunitparts').'</span></span>';
        $it_split_butt .= '</a>';
        $it_split_butt .= '</td>';
        for($nclsp = 0; $nclsp < $need_rest_colsp; $nclsp++) {
            $it_split_butt .= '<td>&nbsp;</td>';
        } //rest of needed cells, noRiddle
        $it_split_butt .= '</tr>';
        
        return $it_split_butt;
        //EOC try button here, noRiddle
        //BOC use font icon here, see above, noRiddle
    }
    
    //BOC new functions to fetch data from shop, noRiddle
    function find_prices_and_statuses_from_shop($mod_nos) {
        $data_arr = array();
        $actual_cust_stat = empty($_SESSION['customers_status']['customers_status_id']) ? DEFAULT_CUSTOMERS_STATUS_ID_GUEST : $_SESSION['customers_status']['customers_status_id']; //this is done in price class as well, why I don't actually know
        $persoff_table = 'personal_offers_by_customers_status_'.$actual_cust_stat;
        
        //try force index to make it faster ?
        //REPLACE(p.products_model,' ','') as products_model,
        $shp_qu_str = "SELECT p.products_id,
                              p.products_model,
                              p.products_replace,
                              p.products_price,
                              p.products_tax_class_id,
                              p.products_status,
                              p.gm_min_order,
                              p.gm_graduated_qty,
                              p.gm_price_status,
                              po.personal_offer,
                              s.specials_new_products_price
                         FROM ".TABLE_PRODUCTS." p
                  FORCE INDEX (idx_products_model)
                    LEFT JOIN ".$persoff_table." po
                           ON po.products_id = p.products_id AND quantity = '1'
                    LEFT JOIN ".TABLE_SPECIALS." s
                           ON s.products_id = p.products_id
                          AND s.status = '1' #only active specials, added 10-2017, noRiddle
                        WHERE p.products_model IN(".$mod_nos.")";
                        //WHERE REPLACE(p.products_model,' ','') IN(".$mod_nos.")"; //TRIM is set in import script on all products_model, we don't need it here; thus index is used and query is faster
                        
        
//test examples for products_model   //00002160299,00002160314,00002165560,00002290479,00002294787,00002294788,00002295609,00002304649,00002321508,00002753164,00002753541,00002759386,2160299,2160314,2165560,2290479,2294787,2294788,2295609,2304649,2321508,2753164,2753541,2759386
        
                          
        //if(is_resource($this->shp_db_link)) { //gives false, strange, noRiddle
        if($this->shp_db_link) {
            //$shp_qu = mysql_query($shp_qu_str, $this->shp_db_link);
            $shp_qu = mysqli_query($this->shp_db_link, $shp_qu_str);
            if(!$shp_qu) return '<pre>Query zu Shop fehlgeschlagen: '.mysqli_error($this->shp_db_link).'</pre>';
            //if(mysql_num_rows($shp_qu)) {
            if(mysqli_num_rows($shp_qu)) {
                //while($shp_arr = mysql_fetch_array($shp_qu, MYSQL_ASSOC)) {
                while($shp_arr = mysqli_fetch_array($shp_qu, MYSQLI_ASSOC)) {
                    $data_arr[strtoupper($shp_arr['products_model'])] = $shp_arr;
                }
            } else {
                $data_arr = array();
            }
        } else {
            $data_arr = array();
        }

        return $data_arr;
        //return $shp_qu_str;
    }
    
    /*function find_prices_and_statuses_from_shop_like($like_modl) { //new 11-2017, noRiddle
        $data_arr_like = array();
        $actual_cust_stat = empty($_SESSION['customers_status']['customers_status_id']) ? DEFAULT_CUSTOMERS_STATUS_ID_GUEST : $_SESSION['customers_status']['customers_status_id']; //this is done in price class as well, why I don't actually know
        $persoff_table_like = 'personal_offers_by_customers_status_'.$actual_cust_stat;
        
        //try force index to make it faster ?
        //REPLACE(p.products_model,' ','') as products_model,
        $shp_qu_str_like = "SELECT p.products_id,
                              p.products_model,
                              p.products_replace,
                              p.products_price,
                              p.products_tax_class_id,
                              p.products_status,
                              p.gm_min_order,
                              p.gm_graduated_qty,
                              p.gm_price_status,
                              po.personal_offer,
                              s.specials_new_products_price
                         FROM ".TABLE_PRODUCTS." p
                  FORCE INDEX (idx_products_model)
                    LEFT JOIN ".$persoff_table_like." po
                           ON po.products_id = p.products_id AND quantity = '1'
                    LEFT JOIN ".TABLE_SPECIALS." s
                           ON s.products_id = p.products_id
                          AND s.status = '1' #only active specials, added 10-2017, noRiddle
                        WHERE p.products_model LIKE '%".$like_modl."'
                          AND p.products_status = '1'";
    
        //if(is_resource($this->shp_db_link)) { //gives false, strange, noRiddle
        if($this->shp_db_link) {
            //$shp_qu_like = mysql_query($shp_qu_str_like, $this->shp_db_link);
            $shp_qu_like = mysqli_query($this->shp_db_link, $shp_qu_str_like);
            if(!$shp_qu_like) return '<pre>Query zu Shop fehlgeschlagen: '.mysqli_error().'</pre>';
            
            if(mysqll_num_rows($shp_qu_like)) {
                //while($shp_arr_like = mysql_fetch_array($shp_qu_like, MYSQL_ASSOC)) {
                while($shp_arr_like = mysqli_fetch_array($shp_qu_like, MYSQLI_ASSOC)) {
                    $data_arr_like[strtoupper($like_modl)] = $shp_arr_like;
                }
            } else {
                $data_arr_like = array();
            }
        } else {
            $data_arr_like = array();
        }
        
        return $data_arr_like;
    }*/
    
    /*function close_shop_db() {
        if($this->shp_db_link != false)
            mysqll_close($this->shp_db_link);
        $this->shp_db_link = false;
    }*/ //close shop db in application_bottom (auto_include), noRiddle
    //EOC new functions to fetch data from shop, noRiddle
}
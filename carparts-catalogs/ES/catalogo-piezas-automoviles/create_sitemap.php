<?php
/**************************************************
* sitemap creator per ajax
* (c) Mario Maeles and noRiddle 07-2015
**************************************************/
include 'includes/application_top.php';

//BOC let only admin call this file, noRiddle
if (!isset($_SESSION['customer_id']) || (isset($_SESSION['customer_id']) && $_SESSION['customers_status']['customers_status_id'] != '0')) {
    xtc_redirect(xtc_href_link(FILENAME_LOGIN, '', 'NONSSL')); 
} 
//EOC let only admin call this file, noRiddle

if(isset($_SESSION['sitemaps_elapsed_time'])) unset($_SESSION['sitemaps_elapsed_time']); //make sure elapsed_time session is not set on start, noRiddle

include 'create_sitemap_function.php';

$entries = get_entries();
$linePerFile = isset($_GET['no_of_entries']) ? (int)$_GET['no_of_entries'] : 1000;
/*end($entries);
$key = key($entries);
$total_prods = ($key + 1);
$files2create = ceil(($key+1) / $linePerFile); //corrected with +1 since array starts with 0 amd there is 1 more element than the last key indicates, noRiddle*/
$prod_cnts = $entries[0];
$total_prods = ($prod_cnts);
$files2create = ceil(($prod_cnts) / $linePerFile);
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sitemaps erstellen</title>
<style>
#main {width:980px; margin:0 auto;}
h1 {margin:8px 0; font-size:30px;}
h2 {margin:8px 0 15px; font-size:18px; color:#c00;}
h3 {margin:0; padding:2px 5px; background:#ccc; cursor:pointer;}
h3:not(.open):after {content:'\2193  open \2193'; color:#fff; padding-left:830px;}
h3.open:after {content:'\2191  close \2191'; color:#fff; padding-left:830px;}
ul {margin:0 8px;}
#help div {padding:5px; border:1px solid #ccc;}
.inpt-f {width:200px; font-size:16px; background:#fff; border:1px solid #939393; border-radius:5px;}
.cr-sitem, #store {border:1px solid #939393; border-radius:5px; cursor:pointer;}
.cr-sitem {font-size:18px; color:#fff; background:#aaa;}
.cr-sitem span {font-size:14px;}
#store {font-size:16px; color:green; background:#ccc;}
#counter {color:#c00;}
.cont {margin:24px 0;}
#what-done {
background: #ccc none repeat scroll 0 0;
border-radius: 5px;
color: #129511;
margin: 12px 0;
padding: 3px 5px;
}
</style>
</head>
<body>
<div id="main">
    <h1>Sitemaps erstellen für den Shop <?php echo $shop; ?></h1>
    <h2>Es werden lediglich Content-Sitemaps erstellt mit den Marken-Contents die bei ausgeloggtem Zustand aufrufbar sind.</h2>
    <div id="help">
        <h3>Hilfe</h3>
        <div>
            <ul>
                <li>Bei der Anzahl an Einträgen pro Sitemap nicht mehr als 22000 eintragen (Default ist 12000, was ca. 2.3 MB entspricht).</li>
                <li>Bei Google und evtl. anderen Suchmaschinen die /DOMAIN/SHOP/sitemap/sitemap_index.xml anmelden.</li>
            </ul>
        </div>
    </div>
    <div class="cont"> 
    <?php
        //BOC form for no. of entries, noRiddle
        $form_no_of_entries = xtc_draw_form('noofentries', $PHP_SELF, 'get');
        $form_no_of_entries .= 'wieviele Einträge pro Sitemap &raquo; '.xtc_draw_input_field('no_of_entries', (isset($_GET['no_of_entries']) ? (int)$_GET['no_of_entries'] : '1000'), 'id="no_of_entries" class="inpt-f"');
        $form_no_of_entries .= ' <button id="store" type="submit">speichern</button>';
        $form_no_of_entries .= '</form>';
        echo $form_no_of_entries;
        //EOC form for no. of entries, noRiddle
    ?>
    </div>
    <div class="cont">
        <button class="cr-sitem" onClick="loop_sitemap()">
            &raquo; <?php echo $files2create; ?> Content-Sitemaps erstellen <br /><span>(für <?php echo number_format($total_prods, 0, ',', '.'); ?> Contents)</span>
        </button>
        <span id="counter"></span>
    </div>
    <div id="what-done"></div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    var $span_counter = $('span#counter'), //store elements in var, noRiddle
        $div_done = $('#what-done');

    function loop_sitemap() {
        file_counter = 1;
        get_loop(file_counter, <?php echo $files2create; ?>);
    }
    function get_loop(file_counter, files2create) {
        //alert(file_counter);
        $span_counter.html(' --- Erstelle: ' + file_counter + ' von ' + files2create + ' Content-Sitemaps');
        $.post("create_sitemap_loop.php", {file_counter: file_counter, lines_to_create: <?php echo $linePerFile; ?>, all_files: <?php echo $files2create; ?>}) //post also $linePerFile, noRiddle
                .done(function (data) {
                    $div_done.html(data);
                    file_counter = file_counter + 1;
                    if (file_counter <= files2create) {
                        get_loop(file_counter, files2create);
                    }
                })
                .fail(function (data) {
                    $span_counter.html(' | GEHT NICHT, MIST... | Abbruch bei Sitemap ' + file_counter); //show when abortion happens, noRiddle
                });
    }

    //help container
    $(function() {
        var top = $('#help h3'),
            cont = $('#help div');
            cont.hide();
            top.click(function() {
                cont.slideToggle(600);
                $(this).toggleClass('open');
            });
    });
</script>
</body>
</html>
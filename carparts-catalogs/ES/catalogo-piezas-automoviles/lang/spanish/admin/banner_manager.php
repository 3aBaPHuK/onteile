<?php
/* --------------------------------------------------------------
   $Id: banner_manager.php 10156 2016-07-27 08:40:37Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(banner_manager.php,v 1.25 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (banner_manager.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Administrador de Banner');

define('TABLE_HEADING_BANNERS', 'Banner');
define('TABLE_HEADING_GROUPS', 'Grupo');
define('TABLE_HEADING_STATISTICS', 'Mostrar / Clics');
define('TABLE_HEADING_STATUS', 'Estado');
define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_LANGUAGE', 'Idioma');

define('TEXT_BANNERS_TITLE', 'Título del Banner:');
define('TEXT_BANNERS_URL', 'Banner-URL:');
define('TEXT_BANNERS_URL_NOTE', 'URL de destino haciendo clic en el banner.');
define('TEXT_BANNERS_GROUP', 'Grupo del Banner:');
define('TEXT_BANNERS_NEW_GROUP', 'Seleccione el grupo de Banner deseado (si está disponible) en el cuadro desplegable o introduzca un nuevo grupo de Banner abajo.');
define('TEXT_BANNERS_NEW_GROUP_NOTE', 'Para que un banner se muestre en la plantilla, la plantilla debe ser extendida.<br/>Ejemplol: El grupo de Banner es banner, entonces {$BANNER} puede ser mostrado en la plantilla en index.html');
define('TEXT_BANNERS_IMAGE', 'Imagen (Fichero):');
define('TEXT_BANNERS_IMAGE_LOCAL', 'Seleccione la imagen deseada haciendo clic en "Buscar" o seleccione un banner existente.<br /><strong>Tipos de ficheros permititos:</strong> jpg, jpeg, jpe, gif, png, bmp, tiff, tif, bmp, swf, cab');
define('TEXT_BANNERS_IMAGE_TARGET', 'Destino de la imagen (Guardar en):');
define('TEXT_BANNERS_HTML_TEXT', 'Texto HTML:');
define('TEXT_BANNERS_HTML_TEXT_NOTE', 'Aquí se puede introducir directamente el código HTML de un servicio afiliado para mostrar un banner.');
define('TEXT_BANNERS_EXPIRES_ON', 'Validez hasta:');
define('TEXT_BANNERS_OR_AT', ', o hasta');
define('TEXT_BANNERS_IMPRESSIONS', 'Impresiones/Mostrar.');
define('TEXT_BANNERS_SCHEDULED_AT', 'Validez desde:');
define('TEXT_BANNERS_BANNER_NOTE', '<b>Comentario sobre el Banner:</b><ul><li>Puede utilizar un Banner de imagen o uno de texto HTML, utilizar ambos simultáneamente no es posible.</li><li>Si utilizan ambos tipos de Banner simultáneamente, sólo se muestra el Banner de Texto HTML.</li></ul>');
define('TEXT_BANNERS_INSERT_NOTE', '<b>Comentario:</b><ul><li>¡Debe tener permiso de escritura en el directorio de imágenes!</li><li>No llene el campo \'Destino de la imagen (Guardar en)\' , si no quiere copiar ninguna imagen en su servidor (p.ej. si la imagen ya se encuentra en el servidor).</li><li>Das \'Destino de la imagen (Guardar en)\' campo debe ser un directorio ya existente con \'/\' al final (p.ej. banners/).</li></ul>');
define('TEXT_BANNERS_EXPIRCY_NOTE', '<b>Comentario validez:</b><ul><li>¡Sólo llenar un campo!</li><li>Si el Banner debe ser mostrado infinitamente, no introduzca nada en estos campos.</li></ul>');
define('TEXT_BANNERS_SCHEDULE_NOTE', '<b>Comentario validez desde:</b><ul><li>Cuando se utiliza este función, el Banner recién se muesta apartir de la fecha indicada.</li><li>Todos los Banner con esta función se muestran hasta su activación como desactivado.</li></ul>');

define('TEXT_BANNERS_DATE_ADDED', 'agregado el:');
define('TEXT_BANNERS_SCHEDULED_AT_DATE', 'Validez desde: <b>%s</b>');
define('TEXT_BANNERS_EXPIRES_AT_DATE', 'Validez hasta el: <b>%s</b>');
define('TEXT_BANNERS_EXPIRES_AT_IMPRESSIONS', 'Validez hasta: <b>%s</b> impresiones/mostrar');
define('TEXT_BANNERS_STATUS_CHANGE', 'Estado modificado: %s');

define('TEXT_BANNERS_DATA', 'D<br />A<br />T<br />E<br />N');
define('TEXT_BANNERS_LAST_3_DAYS', 'los últimos 3 días');
define('TEXT_BANNERS_BANNER_VIEWS', 'Anuncio de Banner');
define('TEXT_BANNERS_BANNER_CLICKS', 'Clics de Banner');

define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar este Banner?');
define('TEXT_INFO_DELETE_IMAGE', 'Borrar imagen de Banner');

define('SUCCESS_BANNER_INSERTED', 'Éxito: El Banner ha sido agregado.');
define('SUCCESS_BANNER_UPDATED', 'Éxito: El Banner ha sido actualizado.');
define('SUCCESS_BANNER_REMOVED', 'Éxito: El Banner ha sido borrado.');
define('SUCCESS_BANNER_STATUS_UPDATED', 'Éxito: El estado del Banner ha sido actualizado.');

define('ERROR_BANNER_TITLE_REQUIRED', 'Error: Se requiere un título de Banner.');
define('ERROR_BANNER_GROUP_REQUIRED', 'Error: Se requiere un grupo de Banner.');
define('ERROR_BANNER_IMAGE_HTML_REQUIRED', 'Error: Se requiere un imagen de Banner o texto HTML.');
define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Error: El directorio de destino %s no existe.');
define('ERROR_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Error: El directorio de destino %s no es escribible.');
define('ERROR_IMAGE_DOES_NOT_EXIST', 'Error: La imagen no existe.');
define('ERROR_IMAGE_IS_NOT_WRITEABLE', 'Error: La imagen no puede ser borrada.');
define('ERROR_UNKNOWN_STATUS_FLAG', 'Error: Desconocido estado flag.');

define('ERROR_GRAPHS_DIRECTORY_DOES_NOT_EXIST', 'Error: ¡El directorio \'graphs\' es inexistente! Por favor, cree un directorio \'graphs\' en el directorio \'images\'.');
define('ERROR_GRAPHS_DIRECTORY_NOT_WRITEABLE', 'Error: ¡El directorio \'graphs\' es protegido contra escritura!');

// BOF - Tomcraft - 2009-11-06 - Use variable TEXT_BANNERS_DATE_FORMAT
define('TEXT_BANNERS_DATE_FORMAT', 'AAAA-MM-DD');
// EOF - Tomcraft - 2009-11-06 - Use variable TEXT_BANNERS_DATE_FORMAT

define('TEXT_BANNERS_LANGUAGE', 'Idioma:');
define('TEXT_BANNERS_LANGUAGE_NOTE', '¿Para qué idioma se debe mostrarse el banner?');
define('TEXT_NO_FILE', '-- ningún fichero --');
?>

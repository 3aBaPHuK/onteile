<?php
/*******************************************************
* file: ribbon_module.php
* use: language file for system module
* (c) noRiddle 07-2017
* Released under the GNU General Public License
*******************************************************/

define('MODULE_RIBBON_MODULE_TEXT_TITLE', 'Ribbon module');
define('MODULE_RIBBON_MODULE_TEXT_DESCRIPTION', 'Makes module available which shows percent ribbons on specials images');
define('MODULE_RIBBON_MODULE_STATUS_TITLE', 'Activate Module ?');
define('MODULE_RIBBON_MODULE_STATUS_DESC', 'Activate Ribbon module');
?>
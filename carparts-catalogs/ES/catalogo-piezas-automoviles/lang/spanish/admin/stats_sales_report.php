<?php
/* --------------------------------------------------------------
   $Id: stats_sales_report.php 1311 2005-10-18 12:30:40Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(stats_sales_report.php,v 1.6 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (stats_sales_report.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/
 

define('REPORT_DATE_FORMAT', 'd. m. Y');

define('HEADING_TITLE', 'Informe de ventas');

define('REPORT_SALES_STATISTICS', 'Filtro de estadísticas de ventas');
define('REPORT_TYPE_YEARLY', 'Anual');
define('REPORT_TYPE_MONTHLY', 'Mensual');
define('REPORT_TYPE_WEEKLY', 'Semanal');
define('REPORT_TYPE_DAILY', 'Diario');
define('REPORT_START_DATE', 'desde la fecha');
define('REPORT_END_DATE', 'hasta la fecha (incluido)');
define('REPORT_DETAIL', 'Detalle');
define('REPORT_MAX', 'Mostrar lo mejor');
define('REPORT_ALL', 'Todos');
define('REPORT_SORT', 'Clasificación');
define('REPORT_EXP', 'Exportación');
define('REPORT_SEND', 'Enviar');
define('EXP_NORMAL', 'Normal');
define('EXP_HTML', 'HTML only');
define('EXP_CSV', 'CSV');

define('TABLE_HEADING_DATE', 'Fecha');
define('TABLE_HEADING_ORDERS', '# pedidos');
define('TABLE_HEADING_ITEMS', '# artículos');
define('TABLE_HEADING_REVENUE', 'Ventas');
define('TABLE_HEADING_SHIPPING', 'Envío');

define('DET_HEAD_ONLY', 'sin detalles');
define('DET_DETAIL', 'Mostrar detalles');
define('DET_DETAIL_ONLY', 'Detalles con Importe');

define('SORT_VAL0', 'Estándar');
define('SORT_VAL1', 'Descripción');
define('SORT_VAL2', 'Descripción desde');
define('SORT_VAL3', '# artículos');
define('SORT_VAL4', '# artículos desde');
define('SORT_VAL5', 'Ventas');
define('SORT_VAL6', 'Ventas desde');

define('REPORT_STATUS_FILTER', 'Estado');
define('REPORT_PAYMENT_FILTER', 'Modalidad de pago');

define('SR_SEPARATOR1', ';');
define('SR_SEPARATOR2', ';');
define('SR_NEWLINE', '<br />');
?>

<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_payment.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_payment.php, v 1.5 2003/02/17);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Pogramme affilié commission');
define('HEADING_TITLE_SEARCH', 'Rechercher:');
define('HEADING_TITLE_STATUS', 'Statut:');

define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_AFILIATE_NAME', 'Partenaire');
define('TABLE_HEADING_PAYMENT', 'Commission (incl.)');
define('TABLE_HEADING_NET_PAYMENT', 'Commission (excl.)');
define('TABLE_HEADING_DATE_BILLED', 'Date facturé');
define('TABLE_HEADING_NEW_VALUE', 'Nouveau statut');
define('TABLE_HEADING_OLD_VALUE', 'Ancien statut');
define('TABLE_HEADING_AFFILIATE_NOTIFIED', 'Informer le partenaire');
define('TABLE_HEADING_DATE_ADDED', 'Ajouté le:');

define('TEXT_DATE_PAYMENT_BILLED', 'Compensé:');
define('TEXT_DATE_ORDER_LAST_MODIFIED', 'Dernier changement:');
define('TEXT_AFFILIATE_PAYMENT', 'Commission');
define('TEXT_AFFILIATE_BILLED', 'Date de facturation');
define('TEXT_AFFILIATE', 'Partenaire');

define('TEXT_INFO_HEADING_DELETE_PAYMENT', 'Supprimer le décompte');
define('TEXT_INFO_DELETE_INTRO', 'Vous êtes sûr de vouloir supprimer la commission?');

define('TEXT_DISPLAY_NUMBER_OF_PAYMENTS', 'Montrant <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> commissions)');

define('TEXT_AFFILIATE_PAYING_POSSIBILITIES', 'Possibilités de paiement:');
define('TEXT_AFFILIATE_PAYMENT_CHECK', 'par chèque:');
define('TEXT_AFFILIATE_PAYMENT_CHECK_PAYEE', 'Destinataire du chèque:');
define('TEXT_AFFILIATE_PAYMENT_PAYPAL', 'Par PayPal:');
define('TEXT_AFFILIATE_PAYMENT_PAYPAL_EMAIL', 'email compte PayPal:');
define('TEXT_AFFILIATE_PAYMENT_BANK_TRANSFER', 'Par virement bancaire:');
define('TEXT_AFFILIATE_PAYMENT_BANK_NAME', 'Institut de crédit:');
define('TEXT_AFFILIATE_PAYMENT_BANK_ACCOUNT_NAME', 'Propriétaire du compte:');
define('TEXT_AFFILIATE_PAYMENT_BANK_ACCOUNT_NUMBER', 'Numéro de compte:');
define('TEXT_AFFILIATE_PAYMENT_BANK_BRANCH_NUMBER', 'Code bancaire:');
define('TEXT_AFFILIATE_PAYMENT_BANK_SWIFT_CODE', 'Code SWIFT:');

define('IMAGE_AFFILIATE_BILLING', 'Start Billing Engine');

define('PAYMENT_STATUS', 'Statut de paiement');
define('PAYMENT_NOTIFY_AFFILIATE', 'Informer le partenaire');

define('TEXT_ALL_PAYMENTS', 'Tous les paiements');
define('TEXT_NO_PAYMENT_HISTORY', 'Pas d\'historique de paiements disponible!');

define('EMAIL_TEXT_SUBJECT', 'Changement de statut de votre paiement');

define('SUCCESS_BILLING', 'Remarque: Vos commissions ont été réglées avec succès!');
define('SUCCESS_PAYMENT_UPDATED', 'Remarque: Le statut du paiement a été actualisé avec succès.');
define('ERROR_PAYMENT_DOES_NOT_EXIST', 'Erreur: Le paiement n\'existe pas!');
?>

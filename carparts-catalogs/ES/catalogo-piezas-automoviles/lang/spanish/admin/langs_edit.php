<?php
/***********************************************************************************************************
* file: langs_edit.php
* use: language file for /Admin/langs_edit.php
*
* (c) noRiddle 06-2018
***********************************************************************************************************/

define('NR_LANGS_EDIT_GUIDE_HEADING', 'manual de instrucciones');
define('NR_LANGS_EDIT_GUIDE_TXT', '<ul>
                                    <li><u>Preparación:</u>
                                        <ul>
                                            <li>Seleccionar el idioma básico y cargarlo en la base de datos.<br />
                                                !! Este idioma básico no debe modificarse si ya ha editado y guardado idiomas. De lo contrario, la asignación de las líneas no es posible !!</li>
                                            <li>Seleccionar idiomas de ayuda y cargarlos en la base de datos.<br />
                                                Cada idioma a editar debe existir como un directorio en <i>/lang/helpfiles/</i> con al menos un archivo. Si no existen plantillas, simplemente suba un archivo de otro idioma allí.</li>
                                        </ul>
                                    </li>
                                    <li><span class="red"><u>Observe con atención !!</u></span>
                                        <ul>
                                            <li>Las comillas solas deben preservarse obligatoriamente.<br />
                                            Si forman parte del texto (por ejemplo en inglés "don\'t") deben ser "escape-t", lo que significa que deben ir precedidos de un backslash.<br />
                                            Así lo parece: "don\'t" o "d\'accord" .</li>
                                            <li>HTML y otros caracteres especiales pertenecientes a PHP también deben preservarse obligatoriamente.<br />
                                            Con un poco de experiencia se puede ver lo que es el texto y debe ser traducido y qué partes pertenecen al código del programa (HTML, Javascript o PHP).</li>
                                        </ul>
                                    </li>
                                    <li><u>Guardar:</u>
                                        <ul>
                                            <li>Cuando los idiomas están abiertos para edición, la sesión se guarda automáticamente cada 10 minutos para evitar la expiración de la sesión y el cierre de sesión.</li>
                                            <li>Cuando se guardan los idiomas editados, se guardan primero en la base de datos.<br />
                                            Sólo si se ha hecho clic en "Generar archivos de idioma para el archivo seleccionado", los archivos asociados se generan y almacenan en <i>/lang/translated/</i> en una carpeta que lleva el nombre de la lengua.</li>
                                        </ul>
                                    </li>
                                  </ul>'
);

define('NR_LANGS_EDIT_LOAD_LANGS_HEADING', 'Cargar idiomas en la base de datos');
define('NR_LANGS_EDIT_LOAD_LANGS_FILETREE_HEADING', 'árbol de archivos');
define('NR_LANGS_EDIT_LOAD_BASIS_LANG', 'Cargar el idioma básico deseado en la base de datos:');
define('NR_LANGS_EDIT_WARNING_LOAD_BASIS_JS', '¿Recargar realmente el idioma básico?');
define('NR_LANGS_EDIT_LOAD_HELPER_LANG', 'Cargar idiomas a editar en la base de datos:');
define('NR_LANGS_EDIT_WARN_NO_BASIS_LANG', '¡Primero se debe cargar un idioma básico!');
define('NR_LANGS_EDIT_WARN_HELP_DIR_NOT_EXISTS', 'El directorio <i>/lang/helpfiles/</i> no existe.<br />Si existen plantillas de archivos de idioma, por favor cárguelas en <i>/lang/helpfiles/IDIOMA/</i> .<br />Para cada idioma reemplace IDIOMA con el nombre en inglés del idioma en minúsculas. (Ejemplo: /lang/helpfiles/italian/ para italiano)');
define('NR_LANGS_EDIT_LOAD_EXIST_LANG', 'Cargar los idiomas existentes en la base de datos:');
define('NR_LANGS_EDIT_LOADED_TXT', 'Cargado:');
define('NR_LANGS_EDIT_HELPINGLANG_TXT', 'idioma de ayuda');
define('NR_LANGS_EDIT_EXISTINGLANG_TXT', 'ya existe en vivo');

define('NR_LANGS_EDIT_ATTENTION_READ_TXT', '¡ATENCIÓN, por favor, lea! ');
define('NR_LANGS_EDIT_EXPL_LOAD_BASIC_TXT', '!! No cambiar si los idiomas ya han sido editados. !!<br />
    El idioma seleccionado se vuelve a leer completamente.<br />
    Para asignar otros idiomas correctamente, el/los idioma(s) a editar también debe(n) ser recargado(s) después de recargar el idioma básico.<br />
    !! Los datos ya almacenados se perderán y no se podrán asignar !!');
define('NR_LANGS_EDIT_EXPL_LOAD_HELPER_TXT', 'En el directorio <i>/lang/helpfiles/</i> hay directorios para otros idiomas que se descargaron como paquetes de idiomas, por ejemplo, de modified.<br />
    Estos pueden ser usados como una plantilla para la traducción.<br />
    Al cargar en la base de datos, cada valor se asigna programáticamente al valor correspondiente del idioma básico cargado a la izquierda.');
define('NR_LANGS_EDIT_EXPL_LOAD_EXIST_TXT', 'El idioma seleccionado es un idioma cuyos archivos ya existen en el directorio en vivo y se leerá completamente nuevo.<br />
    Se pueden leer varios idiomas consecutivamente. Pueden servir de modelo para la traducción.<br />
    !! Tenga en cuenta que un idioma que se carga varias veces sobrescribe el idioma cargado anteriormente !!');
define('NR_LANGS_EDIT_EXPL_SHOW_LANGS_TXT', 'El idioma cargado arriba como idioma básico siempre se muestra automáticamente como modelo.<br />
    Los idiomas ya existentes en vivo, si están marcados, sólo se cargan como modelos a seguir y no son editables.<br />
    !! Si ya se han almacenado traducciones propias, éstas se cargan automáticamente !!');
define('NR_LANGS_EDIT_EXPL_GENERATE_FILES', 'Después de guardar las traducciones, se deberían generar los archivos de los idiomas editados para el archivo seleccionado.');
define('NR_LANGS_EDIT_FILL_EMPTY_WITH_ENGL', '¿Rellenar los valores vacíos con valores ingleses ?');

define('NR_LANGS_EDIT_LOAD_IN_DB_TXT', 'cargar en la base de datos');
define('NR_LANGS_EDIT_SHOW_MARKED_LANGS_TO_EDIT', 'Mostrar los idiomas marcados para el archivo seleccionado para su edición');
define('NR_LANGS_EDIT_SAVE_TRANSL_TO_DB', 'Guardar los cambios en la base de datos');
define('NR_LANGS_EDIT_GENERATE_FILES', 'Generar archivos de idioma para el archivo seleccionado');

//messagestack
define('NR_LANGS_EDIT_NOLANG_CHOOSEN_OR_NO_DIR', '¡No se ha seleccionado ningún idioma o el idioma no está disponible como Directory!');
define('NR_LANGS_EDIT_CONSTS_LOADED', 'Se leyeron las constantes definidas de %s para %s.');
define('NR_LANGS_EDIT_CONFS_LOADED', 'Se leyeron las variables definidas de Smarty-Conf de %s para %s.');
define('NR_LANGS_EDIT_UNALLOWED_LANGS_MSG', '!! Idioma(s) no autorizado(s) no se puede(n) mostrar !!');
define('NR_LANGS_EDIT_GENERATED_FILES_SUCCESS', '%s en %s fue escrito en <i>/%s</i> .');
define('NR_LANGS_EDIT_GENERATED_FILES_ERROR', '%s  en %s no pudo ser escrito en  <i>/%s</i>. <br />No hay datos para el archivo en la base de datos. <br />Se deben guardar primero los datos para el archivo en la base de datos.');
?>
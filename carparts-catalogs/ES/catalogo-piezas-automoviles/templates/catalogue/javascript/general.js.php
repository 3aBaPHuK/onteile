<?php
/*-----------------------------------------------------------
   $Id:$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
  -----------------------------------------------------------
   based on: (c) 2003 - 2006 XT-Commerce (general.js.php)
  -----------------------------------------------------------
   Released under the GNU General Public License
   -----------------------------------------------------------
   <script src="<?php echo DIR_WS_BASE.DIR_TMPL_JS; ?>jquery-1.8.3.min.js" type="text/javascript"></script>
*/
define('DIR_TMPL_JS', DIR_TMPL.'javascript/');
// this javascriptfile get includes at the TOP of every template page in shop
// you can add your template specific js scripts here
// we need jQuery 1.8.1, otherwise scripts don't work on unit.php
//BOC google adds, noRiddle
?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<?php //EOC google adds, noRiddle ?>
<script type="text/javascript">
var DIR_WS_BASE="<?php echo DIR_WS_BASE ?>";
var SetSecCookie = <?php echo ((HTTP_SERVER == HTTPS_SERVER && $request_type == 'SSL') ? 'true' : 'false'); ?>;
var DIR_WS_CATALOG = "<?php echo DIR_WS_CATALOG; //need this in oil.js for path, 05-2021, noRiddle ?>";
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script>/*<![CDATA[*/!window.jQuery && document.write(unescape('%3Cscript src="<?php echo DIR_WS_BASE.DIR_TMPL_JS; ?>jquery-1.8.1.js"%3E%3C/script%3E'))/*]]>*/</script>
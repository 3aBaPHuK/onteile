<?php
/***************************************
* file: ot_bulkgoods
* use: language file for above ot module
* (c) noRiddle 05-2017
***************************************/

define('MODULE_ORDER_TOTAL_BULKGOODS_HELP_LINK', ' <a onclick="window.open(\'popup_help.php?type=order_total&modul=ot_bulkgoods&lng=spanish\', \'Ayuda\', \'scrollbars=yes,resizable=yes,menubar=yes,width=800,height=600\'); return false" target="_blank" href="popup_help.php?type=order_total&modul=ot_sperrgut&lng=spanish"><b>[AYUDA]</b></a>');
define('MODULE_ORDER_TOTAL_BULKGOODS_HELP_TEXT', '<h2>módulo de mercancías voluminosas configurable</h2>
<p>En realidad, el módulo se explica por sí mismo.<br />Sin embargo, en caso de problemas de comprensión, aquí también hay un pequeño manual.<p>
<ul>
<li>Instalar módulo.</li>
<li>Por favor, preste atención a la clasificación !, si también está instalado "<i>ot_cod_fee</i>" que por defecto tiene la clasificación "35" la "<i>ot_sperrgut</i>" debería obtener la clasificación "34" .</li>
<li>Determinar el tipo de cálculo por menú desplegable.<br />En la cesta de compra se muestran<br />- a menos que se haya seleccionado "sólo una vez los costos de mercancías voluminosas más altos por cesta de compra" -<br /> los costos de mercancías voluminosas para cada artículo individualmente<br />y además los costos totales de las mercancías voluminosas se muestran en el resumen.<br />En la vista individual del producto, los costos de las mercancías voluminosas se muestran bajo el enlace de costos de envío</li>
<li>Indicar si se debe mostrar un enlace a la información sobre "costos de mercancías voluminosas" en la página checkout_shipping para cada modo de envío.<br />"Libre de costo de envío" y "Recogida propia" no se proporcionan con el enlace.<br />Si se selecciona \'Sí\' no olvide incorporar la información sobre los costos de las mercancías voluminosas en el contenido para los costos de envío.<br />Un enlace al contenido se muestra tanto en la vista individual del producto como en la página checkout_shipping-Seite (si está activada).</li>
<li>En la página checkout_confirmation, en el pedido en el Backend como también en la confirmación de pedido se muestran automáticamente los costos de mercancías voluminosas.</li>
</ul><br /><p><a href="http://www.revilonetz.de/kontakt" target="_blank">noRiddle</a> desea mucha diversión y éxito con el módulo</p>');

define('MODULE_ORDER_TOTAL_BULKGOODS_TITLE', 'Costos de mercancías voluminosas'.MODULE_ORDER_TOTAL_BULKGOODS_HELP_LINK);
define('MODULE_ORDER_TOTAL_BULKGOODS_DESCRIPTION', 'Costos de mercancías voluminosas de un pedido');
  
define('MODULE_ORDER_TOTAL_BULKGOODS_CC_TITLE', 'Recargo por mercancías voluminosas');
 
define('MODULE_ORDER_TOTAL_BULKGOODS_STATUS_TITLE', 'Mercancías voluminosas activado');
define('MODULE_ORDER_TOTAL_BULKGOODS_STATUS_DESC', '¿Activar los costos de mercancías voluminosas?');
  
define('MODULE_ORDER_TOTAL_BULKGOODS_SORT_ORDER_TITLE', 'Orden de clasificación');
define('MODULE_ORDER_TOTAL_BULKGOODS_SORT_ORDER_DESC', 'Orden de visualización');

define('MODULE_ORDER_TOTAL_BULKGOODS_COSTS_TITLE', 'Costos por mercancía voluminosa');
define('MODULE_ORDER_TOTAL_BULKGOODS_COSTS_DESC', '');

define('MODULE_ORDER_TOTAL_BULKGOODS_METHOD_TITLE', '¿Cómo se deben calcular los costos de mercancías voluminosas?');
define('MODULE_ORDER_TOTAL_BULKGOODS_METHOD_DESC', '<ul style="list-style-type:decimal;"><li>sumar todos los costos de mercancías voluminosas = los costos se multiplican con el número de unidades del artículo</li><li>sólo una vez costos de mercancías voluminosas por cada artículo = los costos se calculan sólo una vez por cada artículo, independientemente del número de unidades</li><li>sólo una vez los mayores costos de mercancías voluminosas por cada cesta de compra = autoexplicativo</li></ul>');

define('MODULE_ORDER_TOTAL_BULKGOODS_SHOW_IN_CHECKOUT_SHIPPING_TITLE', 'Mostrar en checkout_shipping');
define('MODULE_ORDER_TOTAL_BULKGOODS_SHOW_IN_CHECKOUT_SHIPPING_DESC', '<ul><li>Cuando se selecciona \'SÍ\' se muestra un enlace a los costos de mercancías voluminosas en la página de checkout_shipping para cada tipo de envío que está a disposición.</li><li>No olvidar incorporar para ello la información sobre los costos de mercancías voluminosas en el contenido para costos de envío.</li></ul>');
  
//BOC dropdown array bulgoods module
define('NR_ADD_UP_ALL_BULKGOODS_COSTS', 'sumar todos los costos de mercancías voluminosas');
define('NR_ADD_UP_ONE_BULKGOODS_COST_PER_ARTICLE', 'sólo una vez costos de mercancías voluminosas por cada artículo');
define('NR_ONLY_HIGHEST_BULKGOODS_COST_PER_SHOPPING_CART', 'sólo una vez los mayores costos de mercancías voluminosas por cada cesta de compra');
//EOC dropdown array bulkgoods module

define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT_TITLE', 'Lectura automática del estado del cliente');
define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT_DESC', '¡No editar!!, se obtiene automáticamente de la base de datos.');

define('MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS_TITLE', 'Clase de impuestos');
define('MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS_DESC', 'Seleccionar la sigiuente clase de impuestos para los costos de mercancías voluminosas');

//BOC customer status factors
//if(MODULE_ORDER_TOTAL_BULKGOODS_STATUS == 'true') {
if(strpos(MODULE_ORDER_TOTAL_INSTALLED, 'ot_bulkgoods.php')) {
    $custstat_array = json_decode(MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT, true);
    foreach($custstat_array as $k => $cs_name) {
define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$k.'_TITLE', 'Recargo de factor para sociedad comanditaria "<i>'.$cs_name.'</i>"');
define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$k.'_DESC', 'Factor de multiplicación para el grupo de clientes '.$k.' (= "<i>'.$cs_name.'</i>")<br />(El valor que está asignado para el producto como mercancía voluminosa, se multiplica para este grupo de clientes por el valor especificado aquí.)');
    }
}
?>

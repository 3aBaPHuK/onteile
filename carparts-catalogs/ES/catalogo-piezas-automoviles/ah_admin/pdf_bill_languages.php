<?php
/******************************************************************************************************
* file: pdf_bill_languages.php
* use: have the constants used in the invoice pdf in database and be able to edit them in shop backend
* original: unknown
* (c) noRiddle 10-2017
******************************************************************************************************/

require('includes/application_top.php');

$languages = xtc_get_languages();
$cl = sizeof($languages);

//verify all available languaes are in table pdf_bill_languages
$pdf_lang_consts_arr = array();
$pdf_lang_const_qu = xtc_db_query("SHOW COLUMNS FROM pdf_bill_languages");
while($pdf_lang_const_arr = xtc_db_fetch_array($pdf_lang_const_qu)) {
    $comps = explode('_', $pdf_lang_const_arr['Field']);
    if(isset($comps[1]) && $comps[1] != 'value') {
        $l_field = $comps[1];
    } else {
        $l_field = $pdf_lang_const_arr['Field'];
    }
    
    $pdf_lang_consts_arr[] = $l_field;
}
//echo '<pre>'.print_r($pdf_lang_consts_arr, true).'</pre>';

for($l = 0; $l < $cl; $l++) {
    if(!in_array($languages[$l]['id'], $pdf_lang_consts_arr)) {
        $langcol_to_add = 'lang_'.$languages[$l]['id'];
        xtc_db_query("ALTER TABLE pdf_bill_languages ADD ".$langcol_to_add." MEDIUMTEXT NOT NULL");
    }
}

$lang_pdfbill = array();
$lang_pdfbill_query = xtc_db_query("SELECT * FROM pdf_bill_languages");
while($lang_pdfbill_arr = xtc_db_fetch_array($lang_pdfbill_query)) {
    $lang_pdfbill[$lang_pdfbill_arr['id']] = $lang_pdfbill_arr;
}
//echo '<pre>'.print_r($lang_pdfbill, true).'</pre>';

if(isset($_GET['action']) && $_GET['action'] = 'save' && isset($_POST['val'])) {
    //echo '<pre>'.print_r($_POST['val'], true).'</pre>';
    $param_cnt = 0;
    $post_cnt = sizeof($_POST['val']);
    foreach($_POST['val'] as $val_id => $langs) {
        $param_cnt++;
        $sql_arr = array();
        foreach($langs as $langid => $val) {
            $sql_arr['lang_'.$langid] = $val;
        }
        //echo '<pre>'.print_r($sql_arr, true).'</pre>';
        if(xtc_db_perform('pdf_bill_languages', $sql_arr, 'update', "id = '".(int)$val_id."'")) {
            if($param_cnt == $post_cnt) {
                $messageStack->add_session('Alle Sprach-Konstanten erfolgreich ugedatet', 'success');
            }
        } else {
            $messageStack->add_session($lang_pdfbill[$val_id]['lang_value'].'Update fehlgeschlagen', 'warning');
        }
    }
    xtc_redirect(xtc_href_link('pdf_bill_languages.php', xtc_get_all_get_params(array('action'))));
}

require (DIR_WS_INCLUDES.'head.php');
?>
<style>
.dataTableContent textarea {width:98%;}
</style>
</head>
<body>
  <!-- header //-->
  <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
  <!-- header_eof //-->
  <!-- body //-->
  <table class="tableBody">
    <tr>
      <?php //left_navigation
      if (USE_ADMIN_TOP_MENU == 'false') {
        echo '<td class="columnLeft2">'.PHP_EOL;
        echo '<!-- left_navigation //-->'.PHP_EOL;       
        require_once(DIR_WS_INCLUDES . 'column_left.php');
        echo '<!-- left_navigation eof //-->'.PHP_EOL; 
        echo '</td>'.PHP_EOL;      
      }
      ?>
      <!-- body_text //-->
      <td class="boxCenter">   
        <div class="pageHeading pdg2 mrg5">PDFBillNext Textbausteine für die PDFs</div>
            <?php
            echo xtc_draw_form('pdfbillnext_lang_params', 'pdf_bill_languages.php?action=save');
            $clsp = $cl + 1;
            $perc_clsp = round(100/$clsp, 3);
            ?>
            <table style="table-layout:fixed; width:100%;">
                <tr>
                    <td class="dataTableContent" colspan="<?php echo $clsp; ?>">
                        <div class="txta-r"><?php echo xtc_button(BUTTON_SAVE); ?></div>
                    </td>
                </tr>
                <tr>
                    <td class="dataTableHeadingContent" style="width:<?php echo $perc_clsp; ?>%">Constant</td>
                    <?php
                    $lflag_style = 'border:1px solid #aaaaaa; padding:4px; width:99%; background:#F3F3F3';
                    for ($hl = 0; $hl < $cl; $hl++) {
                        echo '<td class="dataTableHeadingContent" style="width:'.$perc_clsp.'%"><span style="'.$lflag_style.'">'.xtc_image(DIR_WS_LANGUAGES . $languages[$hl]['directory'] .'/admin/images/'. $languages[$hl]['image'], $languages[$hl]['name']) . ' ' . $languages[$hl]['name'].'</span></td>';
                    }
                    ?>
                </tr>
                <?php
                //for($b = 0, $cpdfb = sizeof($lang_pdfbill); $b < $cpdfb; $b++) {
                foreach($lang_pdfbill as $id => $vals) {
                ?>
                <tr>
                    <td class="dataTableContent" style="width:<?php echo $perc_clsp; ?>%">
                        <b><?php  echo $lang_pdfbill[$id]['lang_value']; ?></b>
                        <br /><br /><?php echo constant($lang_pdfbill[$id]['lang_value'].'_DESC'); ?>
                    </td>
                    <?php
                    for($i = 0; $i < $cl; $i++) {
                    ?>
                    <td class="dataTableContent" style="width:<?php echo $perc_clsp; ?>%"><?php echo xtc_draw_textarea_field('val['.$id.']['.$languages[$i]['id'].']', 'soft', '', '5', $lang_pdfbill[$id]['lang_'.$languages[$i]['id']]); ?></td>
                    <?php
                    }
                    ?>
                </tr>
                <?php
                }
                ?>
                <tr>
                    <td class="dataTableContent" colspan="<?php echo $clsp; ?>">
                        <div class="txta-r"><?php echo xtc_button(BUTTON_SAVE); ?></div>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
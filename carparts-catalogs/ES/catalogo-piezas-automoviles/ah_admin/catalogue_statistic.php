<?php
/***************************************************
* file: catalogue_statistic.php
* use: statistic about the use of the catalogue
*      
* (c) noRiddle 08-2018
* interresante Seite f�r VIN Suche: https://www.autodna.com/vin-check | alternative: http://www.auto-vin-decoder.com/
*
main query:
SELECT * FROM stat_catalog ORDER BY date_added DESC
***************************************************/

require('includes/application_top.php');

$sc_allowed_get_array = array('sc_vinorwiz_filter', 'sc_brand_filter', 'sc_custmail_filter', 'sc_from_date', 'sc_to_date', 'page', 'sc_sorting');
$sc_brand_exclude_arr = array('BMW', 'BMW MOTORRAD', 'MINI', 'ROLLS-ROYCE'); //exclude brand with own catalogue
$sc_name_exclude_arr = array('BMW', 'BMW Motorrad', 'Mini', 'Rolls-Royce'); //exclude brand name with own catalogue

if(isset($_GET) && !empty($_GET)) {
    $unallowed_get = false;
    foreach($_GET as $get_key => $get_val) {
        if(!in_array($get_key, $sc_allowed_get_array)) {
            $unallowed_get = true;
            break;
        }
    }
} else {
    $unallowed_get = true;
}
//echo '<br><br><br><br><br><br><pre>'.print_r($_GET, true).'</pre>';
//echo '<pre>'.var_dump($unallowed_get).'</pre>';

$sc_cfg_max_display_results_key = MAX_DISPLAY_LIST_CUSTOMERS;
$sc_page_max_display_results = xtc_cfg_save_max_display_results($sc_cfg_max_display_results_key);

$get_sc_winorwiz = false;
$get_sc_brandfilt = false;
$get_custmailfilt = false;

$ajax_post_url = xtc_href_link('catalogue_statistic.php', xtc_get_all_get_params(array()));

//BOC set vars for different posts
if(isset($_GET['sc_vinorwiz_filter']) && $_GET['sc_vinorwiz_filter'] != '') $get_sc_winorwiz = true;
if(isset($_GET['sc_brand_filter']) && $_GET['sc_brand_filter'] != '') $get_sc_brandfilt = true;
if(isset($_GET['sc_custmail_filter']) && $_GET['sc_custmail_filter'] != '') $get_custmailfilt = true;
if(isset($_GET['sc_from_date']) && isset($_GET['sc_to_date']) && !empty($_GET['sc_from_date']) && !empty($_GET['sc_to_date'])) $get_fromtodate = true;
//EOC set vars for different posts
/*echo '<pre>'.print_r($_GET, true).'</pre>';
echo var_dump($get_sc_winorwiz);
echo var_dump($get_sc_brandfilt);
echo var_dump($get_custmailfilt);*/

$general_sc_qu_str = "SELECT * FROM stat_catalog";
$where_str = '';
$sort = '';

//statistical presentation
if(isset($_POST['show_stat_info']))
$general_sc_sp_qu_str = "SELECT count(*) FROM stat_catalog";
$where_sp_str = '';

//BOC pull-downs for date filter
$day_arr = array();
$month_arr = array();
$year_arr = array();

//days
$day_arr[] = array('id' => 0, 'text' => '');
for ($i = 1; $i < 32; $i++) {
    $day_arr[] = array('id' => $i, 'text' => $i);
}

//BOC set one month as default range
//$date_day_from_sel = xtc_draw_pull_down_menu('sc_from_date[]', $day_arr, (isset($_GET['sc_from_date'][0]) ? $_GET['sc_from_date'][0] : $day_arr[1]['id']), 'id="from-day" size="1"', false);
$mom_day_in_month = cal_days_in_month(CAL_GREGORIAN, date("n"), date("Y"));
$date_day_from_sel = xtc_draw_pull_down_menu('sc_from_date[]', $day_arr, (isset($_GET['sc_from_date'][0]) ? $_GET['sc_from_date'][0] : (date("j") > $mom_day_in_month ? $mom_day_in_month : date("j"))), 'id="from-day" size="1"', false); //range one month
//EOC set one month as default range
$date_day_to_sel = xtc_draw_pull_down_menu('sc_to_date[]', $day_arr, (isset($_GET['sc_to_date'][0]) ? $_GET['sc_to_date'][0] : date("j")), 'id="to-day" size="1"', false);
$reset_day_from = (date("j") > $mom_day_in_month ? $mom_day_in_month : date("j")); //$day_arr[1]['id'];
$reset_day_to = date("j");

//months
$month_arr[] = array('id' => 0, 'text' => '');
for ($i = 1; $i < 13; $i++) {
    $month_arr[] = array('id' => $i, 'text' => strftime("%B", mktime(0, 0, 0, $i, 1)));
}

//BOC set one month as default range
//$date_month_from_sel = xtc_draw_pull_down_menu('sc_from_date[]', $month_arr, (isset($_GET['sc_from_date'][1]) ? $_GET['sc_from_date'][1] : $month_arr[1]['id']), 'id="from-month" size="1"', false);
$date_month_from_sel = xtc_draw_pull_down_menu('sc_from_date[]', $month_arr, (isset($_GET['sc_from_date'][1]) ? $_GET['sc_from_date'][1] : (date("n")-1)), 'id="from-month" size="1"', false); //range one month
//EOC set one month as default range
$date_month_to_sel = xtc_draw_pull_down_menu('sc_to_date[]', $month_arr, (isset($_GET['sc_to_date'][1]) ? $_GET['sc_to_date'][1] : date("n")), 'id="to-month" size="1"', false);
$reset_month_from = (date("n")-1); //$month_arr[1]['id'];
$reset_month_to = date("n");

//years
$year_arr[] = array('id' => 0, 'text' => '');
for ($i = 2; $i >= 0; $i--) {
    $year_arr[] = array('id' => (date("Y") - $i), 'text' => (date("Y") - $i));
}
$year_arr = array_reverse($year_arr);

$date_year_from_sel = xtc_draw_pull_down_menu('sc_from_date[]', $year_arr, (isset($_GET['sc_from_date'][2]) ? $_GET['sc_from_date'][2] : $year_arr[0]['id']), 'id="from-year" size="1"', false);
$date_year_to_sel = xtc_draw_pull_down_menu('sc_to_date[]', $year_arr, (isset($_GET['sc_to_date'][2]) ? $_GET['sc_to_date'][2] : $year_arr[0]['id']), 'id="to-year" size="1"', false);
$reset_year_from = $year_arr[0]['id'];
$reset_year_to = date("Y");
//EOC pull-downs for date filter

if($get_sc_winorwiz === true) {
    switch($_GET['sc_vinorwiz_filter']) {
        case 'only_sb_vin':
            $where_str .= " WHERE vin_or_wiz = 'findByVIN'";
          break;
        case 'only_sb_wiz':
            $where_str .= " WHERE vin_or_wiz = 'findByWizard2'";
          break;
        case 'vin_found':
            $where_str .= " WHERE vin_or_wiz = 'findByVIN' AND found_vin = '1'";
          break;
        case 'vin_not_found':
            $sc_add_field = $get_sc_brandfilt === true ? " brand," : '';
            $general_sc_qu_str = "SELECT DISTINCT search_vin,".$sc_add_field." found_vin FROM stat_catalog"; 
            $where_str .= " WHERE vin_or_wiz = 'findByVIN' AND found_vin = '0'";
          break;
    }
}

if($get_sc_brandfilt === true) {
    if($where_str != '') { //($get_sc_winorwiz === true) {
        $where_str .= " AND brand = '".xtc_db_input($_GET['sc_brand_filter'])."'";
    } else {
        $where_str .= " WHERE brand = '".xtc_db_input($_GET['sc_brand_filter'])."'";
    }
}

if($get_custmailfilt === true) {
    $sc_cust_qu = xtc_db_query("SELECT customers_id FROM customers WHERE customers_email_address = '".xtc_db_input($_GET['sc_custmail_filter'])."' AND account_type = 0");
    if(xtc_db_num_rows($sc_cust_qu) == 1) {
        $sc_cust_arr = xtc_db_fetch_array($sc_cust_qu);
        if($where_str != '') { //($get_sc_winorwiz === true || $get_sc_brandfilt === true) {
            $where_str .= " AND cust_id = ".(int)$sc_cust_arr['customers_id'];
        } else {
            $where_str .= " WHERE cust_id = ".(int)$sc_cust_arr['customers_id'];
        }
        
        //BOC show bought credits in first result row via ajax (see below)
        if(isset($_POST['show_bought_credits']) && $_POST['show_bought_credits'] == 'true') {
            $bought_credits = 0;
            $ord_cred_qu = xtc_db_query("SELECT orders_id FROM orders WHERE customers_id = ".(int)$sc_cust_arr['customers_id']);
            if(xtc_db_num_rows($ord_cred_qu)) {
                $ord_in_str = '';
                while($ord_cred_arr = xtc_db_fetch_array($ord_cred_qu)) {
                    $ord_in_str .= $ord_cred_arr['orders_id'].',';
                }
                $ord_in_str = rtrim($ord_in_str, ',');

                $ord_prod_cred_qu = xtc_db_query("SELECT op.products_quantity, opa.products_options_values
                                                    FROM orders_products op
                                               LEFT JOIN orders_products_attributes opa
                                                      ON opa.orders_id = op.orders_id
                                                   WHERE op.orders_id IN(".$ord_in_str.")");

                while($ord_prod_cred_arr = xtc_db_fetch_array($ord_prod_cred_qu)) {
                    $qty_arr = explode(' ', $ord_prod_cred_arr['products_options_values']);
                    $sngl_qty_cred = $qty_arr[0];
                    $qty_cred = $ord_prod_cred_arr['products_quantity'] * $sngl_qty_cred;
                    $bought_credits += $qty_cred;
                }
            }

            echo SC_BOUGHT_TEXT.$bought_credits;

            exit;
        }
        //EOC show bought credits in first result row via ajax (see below)
    }
}

if($get_fromtodate === true) {
    $startdate = mktime(0, 0, 0, (int)$_GET['sc_from_date'][1], (int)$_GET['sc_from_date'][0], (int)$_GET['sc_from_date'][2]);

    $days_in_month = cal_days_in_month(CAL_GREGORIAN, $_GET['sc_to_date'][1], $_GET['sc_to_date'][2]);
    $day_to_big = false;
    if($_GET['sc_to_date'][0] > $days_in_month) {
        $day_to_big = true;
        $enddate = mktime(0, 0, 0, (int)$_GET['sc_to_date'][1], (int)$days_in_month +1, (int)$_GET['sc_to_date'][2]); //need +1 for day since choosen day will have time 00:00:00 where day starts and not ends
    } else {
        $enddate = mktime(0, 0, 0, (int)$_GET['sc_to_date'][1], (int)$_GET['sc_to_date'][0] +1, (int)$_GET['sc_to_date'][2]); //need +1 for day since choosen day will have time 00:00:00 where day starts and not ends
    }

    if($enddate < $startdate) {
        $out .= '<span style="color:#c00;">Das "bis Datum" mu� h�her sein als das "von Datum".</span>';
    } else {
        if($where_str != '') {
            $where_str .= " AND DATE(date_added) >= '".xtc_db_input(date("Y-m-d H:i:s", $startdate)). "' AND DATE(date_added) <= '".xtc_db_input(date("Y-m-d H:i:s", $enddate))."'";
            //$where_str .= " AND UNIX_TIMESTAMP(date_added) >= ".(int)$startdate." AND UNIX_TIMESTAMP(date_added) <= ".(int)$enddate; //why as complicated as above ?
        } else {
            $where_str .= " WHERE DATE(date_added) >= '".xtc_db_input(date("Y-m-d H:i:s", $startdate)). "' AND DATE(date_added) <= '".xtc_db_input(date("Y-m-d H:i:s", $enddate))."'";
            //$where_str .= " WHERE UNIX_TIMESTAMP(date_added) >= ".(int)$startdate." AND UNIX_TIMESTAMP(date_added) <= ".(int)$enddate; //why as complicated as above ?
        }
    }
}

if(isset($_GET['sc_sorting']) && xtc_not_null($_GET['sc_sorting'])) {
    switch($_GET['sc_sorting']) {
        case 'cust_id':
            $sort .= " ORDER BY cust_id";
          break;
        case 'cust_id-desc':
            $sort .= " ORDER BY cust_id DESC";
          break;
        case 'date_ad':
            $sort .= " ORDER BY date_added";
          break;
        case 'date_ad-desc':
            $sort .= " ORDER BY date_added DESC";
          break;
        case 'c_param':
            $sort .= " ORDER BY search_c";
          break;
        case 'c_param-desc':
            $sort .= " ORDER BY search_c DESC";
          break;
        case 'brand':
            $sort .= " ORDER BY brand";
          break;
        case 'brand-desc':
            $sort .= " ORDER BY brand DESC";
          break;
        case 'vorw':
            $sort .= " ORDER BY vin_or_wiz";
          break;
        case 'vorw-desc':
            $sort .= " ORDER BY vin_or_wiz DESC";
          break;
        case 'svin':
            $sort .= " ORDER BY search_vin";
          break;
        case 'svin-desc':
            $sort .= " ORDER BY search_vin DESC";
          break;
        case 'foundv':
            $sort .= " ORDER BY found_vin";
          break;
        case 'foundv-desc':
            $sort .= " ORDER BY found_vin DESC";
          break;
    }
} else {
    $sort .= $get_sc_winorwiz === true && $_GET['sc_vinorwiz_filter'] == 'vin_not_found' ? '' : " ORDER BY date_added DESC";
}

//build query string
$stat_cat_qu_str = $general_sc_qu_str.$where_str.$sort;

//split pages and execute query
if($unallowed_get === false) {
    if($get_sc_winorwiz === true && $_GET['sc_vinorwiz_filter'] == 'vin_not_found') {
        $stat_cat_split = new splitPageResults($_GET['page'], $sc_page_max_display_results, $stat_cat_qu_str, $stat_cat_qu_numrows, 'search_vin');
    } else {
        $stat_cat_split = new splitPageResults($_GET['page'], $sc_page_max_display_results, $stat_cat_qu_str, $stat_cat_qu_numrows);
    }
    $stat_cat_qu = xtc_db_query($stat_cat_qu_str);
}

//BOC build drop down arrays for filters
$sc_vinorwiz_filter_arr = array(0 => array('id' => '', 'text' => SC_PLEASE_CHOOSE_TXT),
                                1 => array('id' => 'only_sb_vin', 'text' => SC_SEARCH_ONLY_VIN_TXT),
                                2 => array('id' => 'only_sb_wiz', 'text' => SC_SEARCH_ONLY_WIZARD_TXT),
                                3 => array('id' => 'vin_found', 'text' => SC_SEARCH_VIN_FOUND_TXT),
                                4 => array('id' => 'vin_not_found', 'text' => SC_SEARCH_VIN_NOT_FOUND_TXT)
                               );

$sc_brand_filter_arr = array(0 => array('id' => '', 'text' => SC_PLEASE_CHOOSE_TXT));
//$sc_brand_qu_str = "SELECT lax_brand FROM laximo_data_helper ORDER BY lax_brand ASC";
$sc_brand_qu_str = "SELECT lax_name FROM laximo_data_helper ORDER BY lax_name ASC";
$sc_brand_qu = xtc_db_query($sc_brand_qu_str);
while($sc_brand_arr = xtc_db_fetch_array($sc_brand_qu)) {
    //if(!in_array($sc_brand_arr['lax_brand'], $sc_brand_exclude_arr)) {
    if(!in_array($sc_brand_arr['lax_name'], $sc_name_exclude_arr)) {
        //$sc_brand_filter_arr[] = array('id' => $sc_brand_arr['lax_brand'], 'text' => $sc_brand_arr['lax_brand']);
        $sc_brand_filter_arr[] = array('id' => $sc_brand_arr['lax_name'], 'text' => $sc_brand_arr['lax_name']);
    }
}
//EOC build drop down arrays for filters

//BOC statistical presentation per ajax
if(isset($_POST['show_stat_info']) && $_POST['show_stat_info'] == 'true') {
    if($unallowed_get === false) {
        //BOC generally need vars
        //exclude admins in $admin_array (includes/extra/application_top/application_top_end/do_application_top_end.php)
        $sc_admin_in_str = implode(',', $admin_array);
        $sc_all_stats = '';

        if($get_fromtodate === true) {
            $ajx_startdate = mktime(0, 0, 0, (int)$_GET['sc_from_date'][1], (int)$_GET['sc_from_date'][0], (int)$_GET['sc_from_date'][2]);
            $ajx_days_in_month = cal_days_in_month(CAL_GREGORIAN, $_GET['sc_to_date'][1], $_GET['sc_to_date'][2]);
            $ajx_day_to_big = false;
            if($_GET['sc_to_date'][0] > $ajx_days_in_month) {
                $ajx_day_to_big = true;
                $ajx_enddate = mktime(0, 0, 0, (int)$_GET['sc_to_date'][1], (int)$ajx_days_in_month +1, (int)$_GET['sc_to_date'][2]); //need +1 for day since choosen day will have time 00:00:00 where day starts and not ends
            } else {
                $ajx_enddate = mktime(0, 0, 0, (int)$_GET['sc_to_date'][1], (int)$_GET['sc_to_date'][0] +1, (int)$_GET['sc_to_date'][2]); //need +1 for day since choosen day will have time 00:00:00 where day starts and not ends
            }
        }

        if($get_custmailfilt === true) {
            $sc_cust_qu = xtc_db_query("SELECT customers_id FROM customers WHERE customers_email_address = '".xtc_db_input($_GET['sc_custmail_filter'])."' AND account_type = 0");
            if(xtc_db_num_rows($sc_cust_qu) == 1) {
                $sc_cust_arr = xtc_db_fetch_array($sc_cust_qu);
            }
        }
        //EOC generally need vars
        
        //BOC search for brands in relation to each other
        if($get_sc_brandfilt === false) {
            // SELECT brand, COUNT(*) AS brd_count FROM stat_catalog GROUP BY brand ORDER BY brd_count DESC |  WHERE brand NOT IN ('BMW', 'BMW MOTORRAD', 'MINI', 'ROLLS_ROYCE') ?
            $brands_relation_main_qu_str = "SELECT brand, COUNT(*) AS brd_count FROM stat_catalog";
            $brands_relation_group_qu_str = " GROUP BY brand ORDER BY brd_count DESC";
            $brands_realtion_where_str = '';
            
            if($get_fromtodate === true) {
                if($brands_realtion_where_str != '') {
                    $brands_realtion_where_str .= " AND UNIX_TIMESTAMP(date_added) >= ".(int)$ajx_startdate." AND UNIX_TIMESTAMP(date_added) <= ".(int)$ajx_enddate;
                } else {
                    $brands_realtion_where_str .= " WHERE UNIX_TIMESTAMP(date_added) >= ".(int)$ajx_startdate." AND UNIX_TIMESTAMP(date_added) <= ".(int)$ajx_enddate;
                }
            }
            
            if($get_custmailfilt === true) {
                if(!empty($sc_cust_arr)) {
                    if($brands_realtion_where_str != '') { //($get_sc_winorwiz === true || $get_sc_brandfilt === true) {
                        $brands_realtion_where_str .= " AND cust_id = ".(int)$sc_cust_arr['customers_id'];
                    } else {
                        $brands_realtion_where_str .= " WHERE cust_id = ".(int)$sc_cust_arr['customers_id'];
                    }
                }
            }
            
            $brands_realtion_where_str .= " AND cust_id NOT IN(".$sc_admin_in_str.")"; //exclude admins
            
            $brands_relation_qu_str = $brands_relation_main_qu_str.$brands_realtion_where_str.$brands_relation_group_qu_str;
            
            $brands_relation_qu = xtc_db_query($brands_relation_qu_str);
            $brands_relation_array = array();
            While($brands_relation_arr = xtc_db_fetch_array($brands_relation_qu)) {
                if(!in_array($brands_relation_arr['brand'], $sc_brand_exclude_arr)) {
                    $brands_relation_array[$brands_relation_arr['brand']] = $brands_relation_arr['brd_count'];
                }
            }

            //ksort($brands_relation_array); //sort alphabetically by brand

            $br_rel_tot = array_sum($brands_relation_array);
            //$brands_relation_array['Total'] = $br_rel_tot;
            $tot_arr = array('Total' => $br_rel_tot);
            $brands_relation_array = $tot_arr + $brands_relation_array;

            foreach($brands_relation_array as $brand => $br_val) {
                $br_val_perc = 100*$br_val/$br_rel_tot;
                $brands_relation_array[$brand] = array($br_val, $br_val_perc);
            }

            $brands_graph_heading = sprintf(SC_GRAPH_HEADING_BRAND_RELATIONS, ($get_custmailfilt === true ? $_GET['sc_custmail_filter'] : ''), ($get_fromtodate === true ? date("j-m-Y H:i", $ajx_startdate) : ''), ($get_fromtodate === true ? date("j-m-Y H:i", $ajx_enddate) : ''));
            
            $sc_brand_rel_graph = make_perc_stat_graph($brands_relation_array, $brands_graph_heading, 4, 15); //, $type = 'horizontal'
            $sc_all_stats .= $sc_brand_rel_graph;
            //$sc_all_stats .= '<pre>'.print_r($brands_relation_array, true).'</pre>';
        }
        //EOC search for brands in relation to each other

        //BOC vin or wizard statistic
        if($get_sc_winorwiz === false) {
            //e.g.: SELECT vin_or_wiz, found_vin, COUNT(*) AS vow_count FROM stat_catalog WHERE DATE(date_added) >= '2018-08-10 00:00:00' AND DATE(date_added) <= '2018-09-11 00:00:00' GROUP BY vin_or_wiz, found_vin ORDER BY vow_count DESC  ??
            $vinorwiz_stat_main_qu_str = "SELECT vin_or_wiz, found_vin, COUNT(*) AS vow_count FROM stat_catalog";
            $vinorwiz_group_qu_str =  " GROUP BY vin_or_wiz, found_vin ORDER BY vow_count DESC";
            $vinorwiz_stat_where_str = "";

            if($get_sc_brandfilt === true) {
                if($vinorwiz_stat_where_str != '') {
                    $vinorwiz_stat_where_str .= " AND brand = '".xtc_db_input($_GET['sc_brand_filter'])."'";
                } else {
                    $vinorwiz_stat_where_str .= " WHERE brand = '".xtc_db_input($_GET['sc_brand_filter'])."'";
                }
            }
            if($get_fromtodate === true) {
                if($vinorwiz_stat_where_str != '') {
                    $vinorwiz_stat_where_str .= " AND UNIX_TIMESTAMP(date_added) >= ".(int)$ajx_startdate." AND UNIX_TIMESTAMP(date_added) <= ".(int)$ajx_enddate;
                } else {
                    $vinorwiz_stat_where_str .= " WHERE UNIX_TIMESTAMP(date_added) >= ".(int)$ajx_startdate." AND UNIX_TIMESTAMP(date_added) <= ".(int)$ajx_enddate;
                }
            }
            if($get_custmailfilt === true) {
                if(!empty($sc_cust_arr)) {
                    if($vinorwiz_stat_where_str != '') { //($get_sc_winorwiz === true || $get_sc_brandfilt === true) {
                        $vinorwiz_stat_where_str .= " AND cust_id = ".(int)$sc_cust_arr['customers_id'];
                    } else {
                        $vinorwiz_stat_where_str .= " WHERE cust_id = ".(int)$sc_cust_arr['customers_id'];
                    }
                }
            }

            $vinorwiz_stat_where_str .= " AND cust_id NOT IN(".$sc_admin_in_str.")"; //exclude admins
            
            $vinorwiz_stat_qu_str = $vinorwiz_stat_main_qu_str.$vinorwiz_stat_where_str.$vinorwiz_group_qu_str;
            
            $vinorwiz_stat_qu = xtc_db_query($vinorwiz_stat_qu_str);
            $vinorwiz_stat_array = array();
            while($vinorwiz_stat_arr = xtc_db_fetch_array($vinorwiz_stat_qu)) {
                //$vinorwiz_stat_array[$vinorwiz_stat_arr['vin_or_wiz']] = $vinorwiz_stat_arr['vow_count'];
                //e.g. [findByWizard2] => 2552, [findByVIN] => 2476
                $vinorwiz_stat_array[] = $vinorwiz_stat_arr;
            }
            
            //BOC build final array for display
            $vinorwiz_finalstat_arr = array();
            $fbwiz_cnt = 0;
            $fbv_cnt = 0;
            $fbv_not_found_cnt = 0;
            //foreach($vinorwiz_stat_array as $val_arr) {
            for($i = 0, $cvsa = count($vinorwiz_stat_array); $i < $cvsa; $i++) {
                if($vinorwiz_stat_array[$i]['vin_or_wiz'] == 'findByWizard2') {
                    $fbwiz_cnt += $vinorwiz_stat_array[$i]['vow_count'];
                }
                if($vinorwiz_stat_array[$i]['vin_or_wiz'] == 'findByVIN') {
                    $fbv_cnt += $vinorwiz_stat_array[$i]['vow_count'];
                }
                if($vinorwiz_stat_array[$i]['vin_or_wiz'] == 'findByVIN' && $vinorwiz_stat_array[$i]['found_vin'] == '0') {
                    $fbv_not_found_cnt += $vinorwiz_stat_array[$i]['vow_count']; //all requests of not found VINs
                }
            }
            $vinorwiz_finalstat_arr['Search by Parameter'] = $fbwiz_cnt;
            $vinorwiz_finalstat_arr['Search by VIN'] = $fbv_cnt;

            $vow_tot = array_sum($vinorwiz_finalstat_arr);
            $temp_vinorwiz_finalstat_arr['Total'] = $vow_tot;
            
            $vinorwiz_finalstat_arr = $temp_vinorwiz_finalstat_arr + $vinorwiz_finalstat_arr; //we want Total as first entry in array

            //BOC distinct not found VINs
            $vin_not_found_distinct_main_qu_str = "SELECT COUNT(DISTINCT search_vin) AS vow_count FROM stat_catalog";
            $vin_not_found_distinct_where_str = " WHERE found_vin = '0'";
            if($get_sc_brandfilt === true) {
                if($vin_not_found_distinct_where_str != '') {
                    $vin_not_found_distinct_where_str .= " AND brand = '".xtc_db_input($_GET['sc_brand_filter'])."'";
                } else {
                    $vin_not_found_distinct_where_str .= " WHERE brand = '".xtc_db_input($_GET['sc_brand_filter'])."'";
                }
            }
            if($get_fromtodate === true) {
                if($vin_not_found_distinct_where_str != '') {
                    $vin_not_found_distinct_where_str .= " AND UNIX_TIMESTAMP(date_added) >= ".(int)$ajx_startdate." AND UNIX_TIMESTAMP(date_added) <= ".(int)$ajx_enddate;
                } else {
                    $vin_not_found_distinct_where_str .= " WHERE UNIX_TIMESTAMP(date_added) >= ".(int)$ajx_startdate." AND UNIX_TIMESTAMP(date_added) <= ".(int)$ajx_enddate;
                }
            }
            if($get_custmailfilt === true) {
                if(!empty($sc_cust_arr)) {
                    if($vin_not_found_distinct_where_str != '') { //($get_sc_winorwiz === true || $get_sc_brandfilt === true) {
                        $vin_not_found_distinct_where_str .= " AND cust_id = ".(int)$sc_cust_arr['customers_id'];
                    } else {
                        $vin_not_found_distinct_where_str .= " WHERE cust_id = ".(int)$sc_cust_arr['customers_id'];
                    }
                }
            }
            
            $vin_not_found_distinct_qu_str = $vin_not_found_distinct_main_qu_str.$vin_not_found_distinct_where_str;
            $vin_not_found_distinct_qu = xtc_db_query($vin_not_found_distinct_qu_str);
            $vin_not_found_distinct_arr = xtc_db_fetch_array($vin_not_found_distinct_qu);
            
            $vinorwiz_finalstat_arr['VIN not found (dist.)'] = $vin_not_found_distinct_arr['vow_count'];
            //EOC distinct not found VINs
            $vinorwiz_finalstat_arr['VIN not found (tot.)'] = $fbv_not_found_cnt;
            //EOC build final array for display
            
            foreach($vinorwiz_finalstat_arr as $vow => $val) {
                if($vow != 'VIN not found (dist.)' && $vow != 'VIN not found (tot.)') {
                    $vow_perc = $vow_tot > 0 ? (100*$val/$vow_tot) : 0;
                    $vinorwiz_finalstat_arr[$vow] = array($val, $vow_perc);
                } else {
                    $vow_perc = $fbv_cnt > 0 ? (100*$val/$fbv_cnt) : 0;
                    $vinorwiz_finalstat_arr[$vow] = array($val, $vow_perc);
                }
            }

            //$sc_all_stats .= '<pre>'.print_r($vinorwiz_finalstat_arr, true).'</pre>';
            //echo '<pre>'.$vinorwiz_stat_qu_str.'<br>'.var_dump($get_sc_brandfilt).'<br>'.var_dump($get_fromtodate).'</pre>';
            
            $vow_graph_heading = sprintf(SC_GRAPH_HEADING_SEARCHBY, ($get_sc_brandfilt === true ? $_GET['sc_brand_filter'] : ''), ($get_fromtodate === true ? date("j-m-Y H:i", $ajx_startdate) : ''), ($get_fromtodate === true ? date("j-m-Y H:i", $ajx_enddate) : ''));
            
            $sc_stat_graph = make_perc_stat_graph($vinorwiz_finalstat_arr, $vow_graph_heading, 4, 20);
            $sc_all_stats .= $sc_stat_graph;
        } else {
            $sc_all_stats .= '';
        }
        //EOC vin or wizard statistic
    } else {
        $sc_all_stats .= '';
    }
    
    echo $sc_all_stats;
    exit();
}
//EOC statistical presentation per ajax

//BOC functions
function sc_sorting($page, $sort) {
    $nav = '<span class="sort-span"><a href="'.xtc_href_link($page, xtc_get_all_get_params(array('action', 'sc_sorting')).'sc_sorting='.$sort).'" title="'.SC_TEXT_SORT_ASC.'">';
    $nav .= '<i class="fa fa-arrow-down"></i></a>';
    $nav .= '<a href="'.xtc_href_link($page, xtc_get_all_get_params(array('action', 'sc_sorting')).'sc_sorting='.$sort.'-desc').'" title="'.SC_TEXT_SORT_DESC.'">';
    $nav .= '<i class="fa fa-arrow-up"></i></a></span>';
    return $nav;
}

function make_perc_stat_graph($data_arr, $graph_heading, $oneperc_height = 4, $bar_width = 20, $type = 'vertical') {
    $size_datarr = sizeof($data_arr);

    switch($type) {
        case 'vertical':
            $y_wrap_height = ($oneperc_height * 100) + 100;
            $y_height = $oneperc_height * 100;
            $x_bar_width = $bar_width;
            $hlfx_bar_width = $x_bar_width/2;
            $x_width = ($size_datarr*2 + 1) * $x_bar_width;
            $sg_wrap_width = $x_width + 51;
          break;
        case 'horizontal':
        
          break;
    }

    $stat_frame_wrap_op = '<div class="stat-graph-wrap" style="width:'.$sg_wrap_width.'px; height:'.$y_wrap_height.'px;">';
    $stat_graph_heading = '<h3>'.$graph_heading.'</h3>';
    $stat_frame = '<div class="y-axis" style="height:'.$y_height.'px;">';
    if($type == 'vertical') {
        for($g = 1; $g <= 20; $g++) { //grid lines in 5% steps
            $grid_pos_bott = $g * 5;
            $stat_frame .= '<div class="grids-x" style="width:'.($x_width+11).'px; bottom:'.$grid_pos_bott.'%;"></div>';
        }
        
        $stat_frame .= '<span class="perc y-10">10%&nbsp;</span>'
                      .'<span class="perc y-20">20%&nbsp;</span>'
                     .'<span class="perc y-30">30%&nbsp;</span>'
                     .'<span class="perc y-40">40%&nbsp;</span>'
                     .'<span class="perc y-50">50%&nbsp;</span>'
                     .'<span class="perc y-60">60%&nbsp;</span>'
                     .'<span class="perc y-70">70%&nbsp;</span>'
                     .'<span class="perc y-80">80%&nbsp;</span>'
                     .'<span class="perc y-90">90%&nbsp;</span>'
                     .'<span class="perc y-100">100%&nbsp;</span>';
    }
    
    $stat_frame .= '</div>'
                  .'<div class="x-axis" style="width:'.$x_width.'px;">';
    if($type == 'horizontal') {
        $stat_frame .= '<span class="percx x-10">&nbsp;10%</span>'
                      .'<span class="percx x-20">&nbsp;20%</span>'
                     .'<span class="percx x-30">&nbsp;30%</span>'
                     .'<span class="percx x-40">&nbsp;40%</span>'
                     .'<span class="percx x-50">&nbsp;50%</span>'
                     .'<span class="percx x-60">&nbsp;60%</span>'
                     .'<span class="percx x-70">&nbsp;70%</span>'
                     .'<span class="percx x-80">&nbsp;80%</span>'
                     .'<span class="percx x-90">&nbsp;90%</span>'
                     .'<span class="percx x-100">&nbsp;100%</span>';
    }
    $stat_frame .= '</div>';

    if($type == 'vertical') {
        $stat_bars = '';
        $x_legend = '';
        if(!is_array($data_arr)) $data_arr = array();
        $bar_cnt = 0;
        foreach($data_arr as $key => $value) {
            $bar_cnt++;

            $bar_height = round($value[1] * $oneperc_height); //3px = 1% of 300px height
            $calc_bar_pos_left = 51 + (($bar_cnt * $x_bar_width) + (($bar_cnt-1) * $x_bar_width));
            $bar_pos_left = ($key != 'VIN not found (dist.)') ? $calc_bar_pos_left : ($calc_bar_pos_left - $x_bar_width +3); //- (round($hlfx_bar_width/3))); //changed 18-05-2019, noRiddle
            $xlegend_pos = $hlfx_bar_width; //$bar_pos_left + $hlfx_bar_width;
            
            $stat_bars .= '<div class="vert-stat-bar vbar-'.$bar_cnt.($bar_cnt % 2 == 0 ? ' vbar-pastel' : '').'" style="height:'.$bar_height.'px; width:'.$x_bar_width.'px; left:'.$bar_pos_left.'px;">'
                         .'<span class="vert-bar-desc'.($bar_cnt % 2 == 0 ? ' legend-pastel' : '').'" style="left:'.$hlfx_bar_width.'px;">'.number_format($value[0], 0, ',', '.').' | '.number_format(round($value[1], 2), 2, ',', '').'%</span>'
                         .'<span class="x-legend xl-'.$bar_cnt.($bar_cnt % 2 == 0 ? ' legend-pastel' : '').'" style="left:'.$xlegend_pos.'px; top:101%">'.$key.'</span>'
                         .'</div>';
            //$x_legend .= '<span class="x-legend xl-'.$bar_cnt.($bar_cnt % 2 == 0 ? ' legend-pastel' : '').'" style="left:'.$xlegend_pos.'px; top:'.$y_wrap_height.'px;">'.$key.'</span>';
        }
        
        $stat_frame_wrap_cl = '</div>';

        $total_stat_frame = $stat_frame_wrap_op . $stat_graph_heading . $stat_frame . $stat_bars . $x_legend . $stat_frame_wrap_cl;
    }
    if($type == 'horizontal') {
        $stat_bars = '';

        if(!is_array($data_arr)) $data_arr = array();
        $arr_count = sizeof($data_arr);
        $bar_cnt = 0;
        foreach($data_arr as $key => $value) {
            $bar_cnt++;
            
            $bar_width = round($value[1] * $oneperc_height); //3px = 1% of 300px
            $bar_height = 3;
            $bar_top_pos = 50 + ($bar_cnt > 1 ? ($bar_cnt*2*$bar_height) : 0);
            $stat_bars .= '<div class="horiz-stat-bar hbar-'.$bar_cnt.'" style="width:'.$bar_width.'px; height:'.$bar_height.'px; top:'.$bar_top_pos.'px;">'.'</div>'; //'<span class="horiz-bar-desc">'.$value[0].'<br />'.round($value[1], 2).'%</span>'.
        }
        
        $stat_frame_wrap_cl = '</div>';
        
        $total_stat_frame = $stat_frame_wrap_op . $stat_graph_heading . $stat_frame . $stat_bars . $stat_frame_wrap_cl;
    }

    echo $total_stat_frame;
}
//EOC functions

//BOC HTML
require (DIR_WS_INCLUDES.'head.php');
echo '<link rel="stylesheet" type="text/css" href="/'.$shop.'/templates/'.CURRENT_TEMPLATE.'/css/font-awesome.css">';
echo '<link rel="stylesheet" type="text/css" href="includes/css/stat_catalog.css">';
?>
</head>
<body>
    <?php
    require(DIR_WS_INCLUDES . 'header.php');
    //echo '<pre>'.$stat_cat_qu_str.'</pre>';
    ?>
    <table class="tableBody">
        <tr>
            <?php //left_navigation
            if (USE_ADMIN_TOP_MENU == 'false') {
            echo '<td class="columnLeft2">'.PHP_EOL;
            echo '<!-- left_navigation //-->'.PHP_EOL;       
            require_once(DIR_WS_INCLUDES . 'column_left.php');
            echo '<!-- left_navigation eof //-->'.PHP_EOL; 
            echo '</td>'.PHP_EOL;      
            }
            ?>
            <td class="boxCenter">
                <div class="pageHeading pdg2 mrg5">Catalogue statistic</div>
                <?php if($unallowed_get === false) { ?>
                <div class="smallText pdg2 flt-l"><?php echo $stat_cat_split->display_count($stat_cat_qu_numrows, $sc_page_max_display_results, (int)$_GET['page'], SC_TEXT_DISPLAY_HOW_MANY_OF_HO_MANY); ?></div>
                <div class="smallText pdg2 flt-r"><?php echo $stat_cat_split->display_links($stat_cat_qu_numrows, $sc_page_max_display_results, MAX_DISPLAY_PAGE_LINKS, (int)$_GET['page'], xtc_get_all_get_params(array('page'))); ?></div>
                <?php } ?>
                <div class="sc-filters">
                    <?php
                    echo '<fieldset><legend>'.SC_VARIOUS_FILTERS_TXT.' | <pre class="inl-pre">Query: '.$stat_cat_qu_str.'</pre></legend>';
                    //echo xtc_draw_form('sc_filters', 'catalogue_statistic.php', '', 'get', 'id="sc-filters" class="fl-form cf"');
                    echo xtc_draw_form('sc_filters', 'catalogue_statistic.php', '', 'get');
                    echo '<div id="sc-filters" class="fl-div">';
                    echo '<div class="inl-block">';
                    echo '<label class="sc-lbl">'.SC_FILTER_VINORWIZ_TXT.'</label>';
                    //echo '<span class="sc-filter-dropdowns">'.xtc_draw_pull_down_menu('sc_vinorwiz_filter', $sc_vinorwiz_filter_arr, (isset($_GET['sc_vinorwiz_filter']) ? $_GET['sc_vinorwiz_filter'] : '1'), 'id="sc-vinorwiz-filter" onchange="this.form.submit()"').'</span>';
                    echo '<span class="sc-filter-dropdowns">'.xtc_draw_pull_down_menu('sc_vinorwiz_filter', $sc_vinorwiz_filter_arr, (isset($_GET['sc_vinorwiz_filter']) ? $_GET['sc_vinorwiz_filter'] : '1'), 'id="sc-vinorwiz-filter" onchange="dosubmit(this.form)"').'</span>';
                    echo '</div><div class="inl-block">';
                    echo '<label class="sc-lbl">'.SC_FILTER_BRAND_TXT.'</label>';
                    //echo '<span class="sc-filter-dropdowns">'.xtc_draw_pull_down_menu('sc_brand_filter', $sc_brand_filter_arr, (isset($_GET['sc_brand_filter']) ? $_GET['sc_brand_filter'] : '1'), 'id="sc-brand-filter" onchange="this.form.submit()"').'</span>';
                    echo '<span class="sc-filter-dropdowns">'.xtc_draw_pull_down_menu('sc_brand_filter', $sc_brand_filter_arr, (isset($_GET['sc_brand_filter']) ? $_GET['sc_brand_filter'] : '1'), 'id="sc-brand-filter" onchange="dosubmit(this.form)"').'</span>';
                    echo '</div>';
                    echo '<div class="inl-block">';
                    echo '<label class="sc-lbl">'.SC_FILTER_CUSTMAIL_TXT.(isset($_GET['sc_custmail_filter']) && $_GET['sc_custmail_filter'] != '' ? '<br />'.SC_SHOW_BOUGHT_VINS.xtc_draw_checkbox_field('show_bought_credits') : '').'</label>';
                    echo '<span class="sc-filter-dropdowns">'.xtc_draw_input_field('sc_custmail_filter', (isset($_GET['sc_custmail_filter']) && $_GET['sc_custmail_filter'] != '' ? $_GET['sc_custmail_filter'] : ''), 'id="sc-custmail-filter"').'</span>';
                    echo '</div>';
                    echo '<span class="sc-filter-dropdowns"><button class="button" type="button" onclick="reset_filters()">&laquo; Reset</button></span>';
                    //echo '</form>';
                    //echo xtc_draw_form('sc_date_filters', 'catalogue_statistic.php', '', 'get', 'id="sc-date-filters" class="fl-form cf"');
                    echo '</div>';
                    echo '<div id="sc-date-filters" class="fl-div">';
                    echo '<div class="inl-block">';
                    echo '<label class="sc-lbl">'.SC_FILTER_FROM_DATE_TXT.'</label>';
                    echo '<span class="sc-filter-dropdowns">'.$date_day_from_sel.'</span>';
                    echo '<span class="sc-filter-dropdowns">'.$date_month_from_sel.'</span>';
                    echo '<span class="sc-filter-dropdowns">'.$date_year_from_sel.'</span>';
                    echo '</div>';
                    echo '<div class="inl-block">';
                    echo '<label class="sc-lbl">'.SC_FILTER_TO_DATE_TXT.'</label>';
                    echo '<span class="sc-filter-dropdowns">'.$date_day_to_sel.'</span>';
                    echo '<span class="sc-filter-dropdowns">'.$date_month_to_sel.'</span>';
                    echo '<span class="sc-filter-dropdowns">'.$date_year_to_sel.'</span>';
                    echo '</div>';
                    echo '<span class="sc-filter-dropdowns"><button class="button" type="button" onclick="reset_date_filters()">&laquo; Reset</button></span>';
                    echo '<span class="sc-filter-dropdowns"><button class="button" type="submit">&laquo; Go</button></span>';
                    echo '</div>';
                    echo '</form>';
                    echo '</fieldset>';
                    ?>
                </div>
                <ul class="acc-nav">
                    <li><a id="show-stats" class="acc-menu" href="#"><?php echo SC_TAB_HEADING_STATISTIC; ?></a></li>
                    <li><a class="acc-menu" href="#"><?php echo SC_TAB_HEADING_TABLE; ?></a></li>
                </ul>
                <div class="list-wrap">
                    <div class="acc-lists">
                        <div class="main-cat-stat">
                            <div id="stat-info-cont" class="cf">
                                <p><?php echo SC_CLICK_ON_FOR_STATS_TXT; ?></p>
                                <i class="fa fa-spinner" id="working-gif"></i>
                            </div>
                        </div>
                    </div>
                    <div class="acc-lists">
                        <div class="main-cat-stat">
                            <table class="tableBoxCenter collapse">
                                <tr class="dataTableHeadingRow">
                                    <td class="dataTableHeadingContent"><?php echo SC_TABLE_HEADING_CUST_ID.sc_sorting('catalogue_statistic.php', 'cust_id'); ?></td>
                                    <td class="dataTableHeadingContent"><?php echo SC_TABLE_HEADING_CUST_IP; ?></td>
                                    <td class="dataTableHeadingContent"><?php echo SC_TABLE_HEADING_DATE_ADDED.sc_sorting('catalogue_statistic.php', 'date_ad'); ?></td>
                                    <td class="dataTableHeadingContent"><?php echo SC_TABLE_HEADING_BRAND.sc_sorting('catalogue_statistic.php', 'brand'); ?></td>
                                    <td class="dataTableHeadingContent"><?php echo SC_TABLE_HEADING_VINORWIZ.sc_sorting('catalogue_statistic.php', 'vorw'); ?></td>
                                    <td class="dataTableHeadingContent"><?php echo SC_TABLE_HEADING_VIN.sc_sorting('catalogue_statistic.php', 'svin'); ?></td>
                                    <td class="dataTableHeadingContent"><?php echo SC_TABLE_HEADING_VIN_FOUND.sc_sorting('catalogue_statistic.php', 'foundv'); ?></td>
                                </tr>
                                <?php
                                $sc_cnt = 0;
                                if($stat_cat_qu) {
                                    while($stat_cat_arr = xtc_db_fetch_array($stat_cat_qu)) { 
                                        $sc_cnt++;
                                        if(isset($stat_cat_arr['cust_id'])) {
                                            $cust_data_qu = xtc_db_query("SELECT customers_firstname, customers_lastname, customers_email_address, catalog_credit FROM customers WHERE customers_id = ".(int)$stat_cat_arr['cust_id']);
                                            $cust_data_arr = xtc_db_fetch_array($cust_data_qu);
                                        }
                                ?>
                                <tr class="dataTableRow">
                                    <td class="dataTableContent">
                                        <?php echo isset($stat_cat_arr['cust_id']) ? $stat_cat_arr['cust_id'].' | VIN-Credit: '.$cust_data_arr['catalog_credit'].($sc_cnt == 1 ? '<span class="bought-vin-creds"></span>' : '').'<br>'.$cust_data_arr['customers_firstname'].' '.$cust_data_arr['customers_lastname'].'<br>'.$cust_data_arr['customers_email_address'] : ''; ?>
                                    </td>
                                    <td class="dataTableContent"><?php echo isset($stat_cat_arr['cust_ip']) ? $stat_cat_arr['cust_ip'] : ''; ?></td>
                                    <td class="dataTableContent"><?php echo isset($stat_cat_arr['date_added']) ? $stat_cat_arr['date_added'] : ''; ?></td>
                                    <td class="dataTableContent"><?php echo isset($stat_cat_arr['brand']) ? $stat_cat_arr['brand'] : ''; ?></td>
                                    <td class="dataTableContent"><?php echo isset($stat_cat_arr['vin_or_wiz']) ? $stat_cat_arr['vin_or_wiz'] : ''; ?></td>
                                    <td class="dataTableContent"><?php echo isset($stat_cat_arr['search_vin']) ? $stat_cat_arr['search_vin'] : ''; ?></td>
                                    <td class="dataTableContent"><?php echo isset($stat_cat_arr['found_vin']) ? $stat_cat_arr['found_vin'] : ''; ?></td>
                                </tr>
                                <?php 
                                    }
                                } 
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <?php if($unallowed_get === false) { ?>
                <div class="smallText pdg2 flt-l"><?php echo $stat_cat_split->display_count($stat_cat_qu_numrows, $sc_page_max_display_results, (int)$_GET['page'], SC_TEXT_DISPLAY_HOW_MANY_OF_HO_MANY); ?></div>
                <div class="smallText pdg2 flt-r"><?php echo $stat_cat_split->display_links($stat_cat_qu_numrows, $sc_page_max_display_results, MAX_DISPLAY_PAGE_LINKS, (int)$_GET['page'], xtc_get_all_get_params(array('page'))); ?></div>
                <?php } ?>
                <?php echo draw_input_per_page($PHP_SELF, $sc_cfg_max_display_results_key, $sc_page_max_display_results); ?>
            </td>
        </tr>
    </table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br />
<script>
var hash = window.location.hash;
var delay = 600;

function scrollToTop(elm) {
    var itOff = $(elm).offset().top;
    var itPos = itOff -200; //  - 60
    if(hash != '') {        
        setTimeout(function(){$('html, body').animate({scrollTop:itPos}, 1000)}, 100);
    }
}

$(function(){
  //*** BOC tabs with slide effect ***
    var $acclists = $('.acc-lists');
    var $accnava = $('.acc-nav li').find('a');
    $('.acc-nav').css('display', 'block');
    $acclists.slice(1).hide();
    $accnava.first().addClass('active');

    $('.acc-menu').each(function(index){
        var $this = $(this);
        $this.click(function(e){
            e.preventDefault();
            $accnava.removeClass('active');
            $this.addClass('active');
            $acclists.stop(false,true).slideUp(delay);
            $acclists.eq(index).stop().slideDown(delay);
        });
    });
  //*** EOC tabs with slide effect ***
    if(hash != '') {
        $(hash).bind('click', function(event){event.preventDefault();}).triggerHandler('click');
        if(hash.slice(-5) == 'panel'){
            scrollToTop($('#multiacc'));
        } else {
        scrollToTop(hash);
        }
    }
    
});

function dosubmit(form) {
    form.submit();
}
function reset_filters() {
    $('#sc-filters input').val('')
    $('#sc-filters select').val('').change();
}
function reset_date_filters() {
    $('#from-day').val(<?php echo $reset_day_from; ?>);
    $('#from-month').val(<?php echo $reset_month_from; ?>);
    $('#from-year').val(<?php echo $reset_year_from; ?>);
    $('#to-day').val(<?php echo $reset_day_to; ?>);
    $('#to-month').val(<?php echo $reset_month_to; ?>);
    $('#to-year').val(<?php echo $reset_year_to; ?>);
    $('form[name="sc_filters"]').submit();
}
$(function() {
    // show bought credits
    if($('input[name="show_bought_credits"]').length) {
        $ipt_sbc = $('input[name="show_bought_credits"]');
        $ipt_sbc.on('click', function() {
            if($(this).prop('checked')) {
                $.post("<?php echo $ajax_post_url; ?>",
                    {show_bought_credits: ""+true+""},
                    function(data){
                        $('span.bought-vin-creds').text(data);
                    }
                );
            } else {
                $('span.bought-vin-creds').text('');
            }
        });
    }

    // generate statistical information
    var $show_stats = $('#show-stats'),
        $stat_info_cont = $('#stat-info-cont'),
        $wrk_gif = $('#working-gif');
    $show_stats.on('click', function() {
        $wrk_gif.show();
        $.post("<?php echo $ajax_post_url; ?>",
            {show_stat_info: ""+true+""},
            function(data){
                if(data != '') {
                    setTimeout(function() {
                        $stat_info_cont.html(data);
                        $wrk_gif.hide();
                    }, delay+200);
                } else {
                    $wrk_gif.hide();
                }
            }
        );
    });

    //load statistic on page load (thus also on form submit) with a bit delay for the tabs to build up
    $(window).load(function() {
        setTimeout(function() {
            $show_stats.trigger('click');
        }, 200);
    });
});
</script>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
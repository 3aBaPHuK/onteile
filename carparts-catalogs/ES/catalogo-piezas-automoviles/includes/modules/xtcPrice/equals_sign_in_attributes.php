<?php
/**************************************************
* file: equals_sign_in_attributes.php
* use: make it possible to use = for attributes
* (c) noRiddle 04-2018
**************************************************/

class equals_sign_in_attributes {
    function __construct() {
        $this->code = 'equals_sign_in_attributes';
        $this->title = 'Gleichheitszeichen bei Attributen';
        $this->description = '';
        $this->name = 'MODULE_XTCPRICE_'.strtoupper($this->code);
        $this->enabled = defined($this->name.'_STATUS') && constant($this->name.'_STATUS') == 'true' ? true : false;
        $this->sort_order = defined($this->name.'_SORT_ORDER') ? constant($this->name.'_SORT_ORDER') : '';
        $this->translate();
    }

    function translate() {
    }

    function check() {
        if (!isset($this->_check)) {
            $check_query = xtc_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = '".$this->name."_STATUS'");
            $this->_check = xtc_db_num_rows($check_query);
        }
        return $this->_check;
    }

    function keys() {
        define($this->name.'_STATUS_TITLE', TEXT_DEFAULT_STATUS_TITLE);
        define($this->name.'_STATUS_DESC', TEXT_DEFAULT_STATUS_DESC);
        define($this->name.'_SORT_ORDER_TITLE', TEXT_DEFAULT_SORT_ORDER_TITLE);
        define($this->name.'_SORT_ORDER_DESC', TEXT_DEFAULT_SORT_ORDER_DESC);

        return array(
            $this->name.'_STATUS',
            $this->name.'_SORT_ORDER'
        );
    }

    function install() {
        xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) values ('".$this->name."_STATUS', 'true','6', '1','xtc_cfg_select_option(array(\'true\', \'false\'), ', now())");
        xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, date_added) values ('".$this->name."_SORT_ORDER', '1','6', '2', now())");
    }

    function remove() {
        xtc_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key LIKE '".$this->name."_%'");
    }

    function GetOptionPrice($dataArr, $attribute_data, $pID, $option, $value, $qty) {
        if($dataArr['weight_prefix'] == '=') {$dataArr['weight'] = ($attribute_data['products_weight'] * -1) + $attribute_data['options_values_weight'];} //substract products weight, will be added later in other modules
        if($dataArr['price_prefix'] == '=') {
            global $xtPrice;
            $CalculateCurr = (($attribute_data['products_tax_class_id'] == 0) ? true : false);
            $attprice = $xtPrice->xtcFormat($attribute_data['options_values_price'], false, $attribute_data['products_tax_class_id'], $CalculateCurr);

            //BOC respect special price if applicable, noRiddle
            //$pprice = $xtPrice->xtcFormat($attribute_data['products_price'], false, $attribute_data['products_tax_class_id'], $CalculateCurr);
            if($specPrice = $xtPrice->xtcCheckSpecial($pID)) {
                $temp_pprice = $specPrice;
            } else {
                $temp_pprice = $attribute_data['products_price'];
            }
            $pprice = $xtPrice->xtcFormat($temp_pprice, false, $attribute_data['products_tax_class_id'], $CalculateCurr);
            //EOC respect special price if applicable, noRiddle

            $price = ($pprice * -1) + $attprice; //substract products price, will be added again later in other modules
            $dataArr['price'] = $price;
        }

        return $dataArr;
    }
}
?>
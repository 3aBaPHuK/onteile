<?php
/* -----------------------------------------------------------------------------------------
   $Id: configuration.php 10896 2017-08-11 11:31:54Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(configuration.php,v 1.8 2002/01/04); www.oscommerce.com
   (c) 2003	 nextcommerce (configuration.php,v 1.16 2003/08/25); www.nextcommerce.org
   (c) 2006 XT-Commerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('TABLE_HEADING_CONFIGURATION_TITLE', 'Nom');
define('TABLE_HEADING_CONFIGURATION_VALUE', 'Valeur');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Veuillez effectuer les modifications nécessaires');
define('TEXT_INFO_DATE_ADDED', 'Ajouté le:');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification:');

// language definitions for config
define('STORE_NAME_TITLE' , 'Nom de la boutique');
define('STORE_NAME_DESC' , 'Le nom de la boutique en ligne');
define('STORE_OWNER_TITLE' , 'Propriétaire');
define('STORE_OWNER_DESC' , 'Le nom de l\'opérateur de la boutique');
define('STORE_OWNER_EMAIL_ADDRESS_TITLE' , 'Adresse mail');
define('STORE_OWNER_EMAIL_ADDRESS_DESC' , 'L\'adresse mail de l\'opérateur de la boutique');

define('EMAIL_FROM_TITLE' , 'Email de');
define('EMAIL_FROM_DESC' , 'Adresse mail pour envoyer des mails (sendmail).');

define('STORE_COUNTRY_TITLE' , 'Pays');
define('STORE_COUNTRY_DESC' , 'Le site de la boutique en ligne <br /><br /><b>Remarque: N\'oubliez pas d\'ajuster la région.</b>');
define('STORE_ZONE_TITLE' , 'Région');
define('STORE_ZONE_DESC' , 'La région de la boutique en ligne');

define('EXPECTED_PRODUCTS_SORT_TITLE' , 'Ordre de l\'annonce des articles');
define('EXPECTED_PRODUCTS_SORT_DESC' , 'C\'est l\'ordre dans lequel aparaîssent les articles annoncés.');
define('EXPECTED_PRODUCTS_FIELD_TITLE' , 'Champ de tri pour l\'annonce des articles');
define('EXPECTED_PRODUCTS_FIELD_DESC' , 'C\'est la colonne que vous utilisez pour le tris des articles annoncés.');

define('USE_DEFAULT_LANGUAGE_CURRENCY_TITLE' , 'Changer automatiquement vers la monnaie nationale');
define('USE_DEFAULT_LANGUAGE_CURRENCY_DESC' , 'Changer automatiquement vers la monnaie nationale si disponible.');

define('SEND_EXTRA_ORDER_EMAILS_TO_TITLE' , 'Envoyer un email de commande à:');
define('SEND_EXTRA_ORDER_EMAILS_TO_DESC' , 'Si vous souhaitez envoyér un email de commande supplémentaire, veuillez saisir les adresses mails de la manière suivante:  Nom 1 <E-Mail@adresse1>, Nom 2 <E-Mail@adresse2>');

define('SEARCH_ENGINE_FRIENDLY_URLS_TITLE' , 'Utiliser des URL adaptées aux moteurs de recherche?');
define('SEARCH_ENGINE_FRIENDLY_URLS_DESC' , 'Les URLs des pages peuvent être affichés automatiquement de manière optimisée pour des moteurs de recherche. <br /><br /><strong>Avertissement:</strong> Pour des URLs adaptées aux moteurs de recherche, le fichier _.htaccess doit être activé dans le répertoire principale de la boutique ou doit être renommé .htaccess. En plus le serveur web doit soutenir <a href="http://www.modrewrite.de/" target="_blank">mod_rewrite</a> (Demander votre fournisseur d’hébergement si vous ne pouvez pas le vérifier.)');

define('DISPLAY_CART_TITLE' , 'Est-ce que vous souhaitez afficher le panier après un ajout?');
define('DISPLAY_CART_DESC' , 'Après l\'ajout d\'un article retour au panier ou retour à l\'article?');

define('ALLOW_GUEST_TO_TELL_A_FRIEND_TITLE' , 'Permettre aux invitées d\'informer ses amis?');
define('ALLOW_GUEST_TO_TELL_A_FRIEND_DESC' , 'Permettre aux invitées d\'informer ses amis par mails sur des articles?');

define('ADVANCED_SEARCH_DEFAULT_OPERATOR_TITLE' , 'Lien de recherche');
define('ADVANCED_SEARCH_DEFAULT_OPERATOR_DESC' , 'L\'opérateur par défaut pour lier des mots de recherche.');

define('STORE_NAME_ADDRESS_TITLE' , 'Adresse commerciale et numéro téléphone etc.');
define('STORE_NAME_ADDRESS_DESC' , 'Entrer votre adresse commerciale comme en-tête.');

define('SHOW_COUNTS_TITLE' , 'Nombre des articles après noms descatégories?');
define('SHOW_COUNTS_DESC' , 'Compte en récurrence le nombre des articles différents par groupe de produits et affiche le nombre (x) derrière chaque nom de catégorie');

define('DISPLAY_PRICE_WITH_TAX_TITLE' , 'Afficher le prix incl. TVA');
define('DISPLAY_PRICE_WITH_TAX_DESC' , 'Afficher les prix incl. impôts (true) ou calculer à la fin (false');

define('DEFAULT_CUSTOMERS_STATUS_ID_ADMIN_TITLE' , 'Statut de client (groupe de client) pour des administrateur au frontend');
define('DEFAULT_CUSTOMERS_STATUS_ID_ADMIN_DESC' , 'Choisissez le statut client (groupe) avec quels droits de groupe des clients l\'administrateur est au frontend.');
define('DEFAULT_CUSTOMERS_STATUS_ID_GUEST_TITLE' , 'Statut client (groupe client) pour des invités');
define('DEFAULT_CUSTOMERS_STATUS_ID_GUEST_DESC' , 'Choisissez le statut client (groupe) pour des invitées de l\'ID respectif!');
define('DEFAULT_CUSTOMERS_STATUS_ID_TITLE' , 'Statut client pour de nouveaux clients');
define('DEFAULT_CUSTOMERS_STATUS_ID_DESC' , 'Choisissez le statut client (groupe) pour de nouveau clients parl\'ID respectif!<br />TIPP: Vous pouvez aménager d\'autres groupes dans le menu groupes des clients et p.ex. faire des semaines d\'action: Cette semaine 10 % de remise sur tous les achats de nouveaux clients?');

define('ALLOW_ADD_TO_CART_TITLE' , 'Permis d\'ajouter des articles au panier');
define('ALLOW_ADD_TO_CART_DESC' , 'Permis d\'ajouter des articles au panier. Aussi si "Afficher les prix" est reglé sur "Non" pour cette groupe de clients');
define('ALLOW_DISCOUNT_ON_PRODUCTS_ATTRIBUTES_TITLE' , 'Utiliser les rabais aussi pour des attributs des articles?');
define('ALLOW_DISCOUNT_ON_PRODUCTS_ATTRIBUTES_DESC' , 'Permis d\'utiliser le rabais du groupe des clients pour les attributs des articles (Seulement si l\'article n\'est pas classé comme "offre spéciale")');
define('CURRENT_TEMPLATE_TITLE' , 'Set de modèles (thème)');
define('CURRENT_TEMPLATE_DESC' , 'Choisissez un set de modèles (thème). Le thème doit se trouver dans le dossier www.Ihre-Domain.com/templates/.');

define('ENTRY_FIRST_NAME_MIN_LENGTH_TITLE' , 'Prénom');
define('ENTRY_FIRST_NAME_MIN_LENGTH_DESC' , 'Longueur minimum du prénom');
define('ENTRY_LAST_NAME_MIN_LENGTH_TITLE' , 'Nom de famille');
define('ENTRY_LAST_NAME_MIN_LENGTH_DESC' , 'Longueur minimum du nom de famille');
define('ENTRY_DOB_MIN_LENGTH_TITLE' , 'Date de naissance');
define('ENTRY_DOB_MIN_LENGTH_DESC' , 'Longueur minimum de la date de naissance');
define('ENTRY_EMAIL_ADDRESS_MIN_LENGTH_TITLE' , 'Adresse email');
define('ENTRY_EMAIL_ADDRESS_MIN_LENGTH_DESC' , 'Longueur minimum de l\'adresse mail');
define('ENTRY_STREET_ADDRESS_MIN_LENGTH_TITLE' , 'Rue');
define('ENTRY_STREET_ADDRESS_MIN_LENGTH_DESC' , 'Longueur minimum du nom de rue');
define('ENTRY_COMPANY_MIN_LENGTH_TITLE' , 'Entreprise');
define('ENTRY_COMPANY_MIN_LENGTH_DESC' , 'Longueur minimum du nom de l\'entreprise');
define('ENTRY_POSTCODE_MIN_LENGTH_TITLE' , 'Code Postal');
define('ENTRY_POSTCODE_MIN_LENGTH_DESC' , 'Longueur minimum du code postal');
define('ENTRY_CITY_MIN_LENGTH_TITLE' , 'Ville');
define('ENTRY_CITY_MIN_LENGTH_DESC' , 'Longueur minimum du nom de la ville');
define('ENTRY_STATE_MIN_LENGTH_TITLE' , 'Région');
define('ENTRY_STATE_MIN_LENGTH_DESC' , 'Longueur minimum de la région');
define('ENTRY_TELEPHONE_MIN_LENGTH_TITLE' , 'Numéro téléphone');
define('ENTRY_TELEPHONE_MIN_LENGTH_DESC' , 'Longueur minimum du numéro de téléphone');
define('ENTRY_PASSWORD_MIN_LENGTH_TITLE' , 'Mot de passe');
define('ENTRY_PASSWORD_MIN_LENGTH_DESC' , 'Longueur minimum du mot de passe');

define('REVIEW_TEXT_MIN_LENGTH_TITLE' , 'Évaluations');
define('REVIEW_TEXT_MIN_LENGTH_DESC' , 'Longueur minimum du texte des évaluaitons');

define('MIN_DISPLAY_BESTSELLERS_TITLE' , 'Bestseller');
define('MIN_DISPLAY_BESTSELLERS_DESC' , 'Nombre minimum des bestseller qui seront affichés');
define('MIN_DISPLAY_ALSO_PURCHASED_TITLE' , 'Acheté également');
define('MIN_DISPLAY_ALSO_PURCHASED_DESC' , 'Nombre minimum des articles achetés également qui seront affichés dans la visualisation des articles');

define('MAX_ADDRESS_BOOK_ENTRIES_TITLE' , 'Entrées répertoire d\'adresses');
define('MAX_ADDRESS_BOOK_ENTRIES_DESC' , 'Nombre maximum des entrées dans le répertoire d\'adresses par client');
define('MAX_DISPLAY_SEARCH_RESULTS_TITLE' , 'Nombre des articles');
define('MAX_DISPLAY_SEARCH_RESULTS_DESC' , 'Nombre des Article dans le listage des produits');
define('MAX_DISPLAY_PAGE_LINKS_TITLE' , 'Tourner la page');
define('MAX_DISPLAY_PAGE_LINKS_DESC' , 'Nombre des pages seules pour lesquelles doit être affiché un lien dans le menu de la navigation du site');
define('MAX_DISPLAY_SPECIAL_PRODUCTS_TITLE' , 'Offres spéciales');
define('MAX_DISPLAY_SPECIAL_PRODUCTS_DESC' , 'Nombre maximum des offres spéciales qui seront affichées');
define('MAX_DISPLAY_NEW_PRODUCTS_TITLE' , 'Nouveaux module de visualisation des articles');
define('MAX_DISPLAY_NEW_PRODUCTS_DESC' , 'Nombre maximum de nouveaux articles qui seront ajoutés au panier');
define('MAX_DISPLAY_UPCOMING_PRODUCTS_TITLE' , 'Articles attendus au menu de visualisation');
define('MAX_DISPLAY_UPCOMING_PRODUCTS_DESC' , 'Nombre maximum des articles attendus qui seront affichés sur la page d\'accueil');
define('MAX_DISPLAY_MANUFACTURERS_IN_A_LIST_TITLE' , 'Liste des fabricants seuil de tolérance');
define('MAX_DISPLAY_MANUFACTURERS_IN_A_LIST_DESC' , 'Dans la boîte du fabricant; Si le nombre des fabricants dépasse ce seuil une liste déroulante où une boîte de listes seront affiché au lieu de la liste des liens courant (dépandant de ce qui est entré sous "Liste fabricant").');
define('MAX_MANUFACTURERS_LIST_TITLE' , 'Liste des fabricants');
define('MAX_MANUFACTURERS_LIST_DESC' , 'Dans la boîte des fabricant; Si la valeur est mise à "1", la boîte des fabricant sera affiché en tant que liste déroulante. Sinon en tant que boîte des listes avec le nombre de colonnes.');
define('MAX_DISPLAY_MANUFACTURER_NAME_LEN_TITLE' , 'Longueur du nom du fabricant');
define('MAX_DISPLAY_MANUFACTURER_NAME_LEN_DESC' , 'Dans la boîte du fabricant; longueur maximum des noms dans la boîte des fabricants');
define('MAX_DISPLAY_NEW_REVIEWS_TITLE' , 'Nouvelle évaluation');
define('MAX_DISPLAY_NEW_REVIEWS_DESC' , 'Nombre maximum de nouvelles évaluations qui doivent être affichées');
define('MAX_RANDOM_SELECT_REVIEWS_TITLE' , 'Sélection aléatoire des évaluations');
define('MAX_RANDOM_SELECT_REVIEWS_DESC' , 'De combien d\'évauations doivent être sélectionnées les évaluations affichées aléatoirement?');
define('MAX_RANDOM_SELECT_NEW_TITLE' , 'Sélection aléatoire de nouveaux articles');
define('MAX_RANDOM_SELECT_NEW_DESC' , 'De combien de nouveaux articles doivent être sélectionnés les articles affichés aléatoirement ?');
define('MAX_RANDOM_SELECT_SPECIALS_TITLE' , 'Sélection aléatoire des offres spéciales');
define('MAX_RANDOM_SELECT_SPECIALS_DESC' , 'De combien d\' offres spéciales doivent être sélectionnées les offres spéciiales affichées aléatirement?');
define('MAX_DISPLAY_CATEGORIES_PER_ROW_TITLE' , 'Sélection catégorie par ligne');
define('MAX_DISPLAY_CATEGORIES_PER_ROW_DESC' , 'Nombre de catégories qu\'il faut afficher par ligne dans les aperçus.');
define('MAX_DISPLAY_PRODUCTS_NEW_TITLE' , 'Liste de nouveaux articles');
define('MAX_DISPLAY_PRODUCTS_NEW_DESC' , 'Nombre maximum de nouveaux articles qu\'il faut afficher dans la liste.');
define('MAX_DISPLAY_BESTSELLERS_TITLE' , 'Bestsellers');
define('MAX_DISPLAY_BESTSELLERS_DESC' , 'Nombre maximum des bestsellers qu\'il faut afficher');
define('MAX_DISPLAY_BESTSELLERS_DAYS_TITLE' , 'Nombre de jours des bestsellers');
define('MAX_DISPLAY_BESTSELLERS_DAYS_DESC' , 'Nombre maximum de jours un article peut être affiché en tant que bestseller');
define('MAX_DISPLAY_ALSO_PURCHASED_TITLE' , 'Acheté également');
define('MAX_DISPLAY_ALSO_PURCHASED_DESC' , 'Nombre maximum d\'articles achetés également qui doivent être affichés lors de la visualisation des articles');
define('MAX_DISPLAY_PRODUCTS_IN_ORDER_HISTORY_BOX_TITLE' , 'Boîte aperçu des bestsellers');
define('MAX_DISPLAY_PRODUCTS_IN_ORDER_HISTORY_BOX_DESC' , 'Nombre maximum des articles qui doivent apparaître dans la boîte des aperçus des bestsellers du client.');
define('MAX_DISPLAY_ORDER_HISTORY_TITLE' , 'Aperçu des commandes');
define('MAX_DISPLAY_ORDER_HISTORY_DESC' , 'Nombre maximum des commandes qui doivent apparaître dans l\'espace client de la boutique.');
define('MAX_PRODUCTS_QTY_TITLE', 'Nombre maximum des produits');
define('MAX_PRODUCTS_QTY_DESC', 'Nombre maximum des articles ajoutés au panier');
define('MAX_DISPLAY_NEW_PRODUCTS_DAYS_TITLE' , 'Nombre des jours pour nouveaux produits');
define('MAX_DISPLAY_NEW_PRODUCTS_DAYS_DESC' , 'Nombre maximum des jours de l\'affichage du nouveau article');

define('PRODUCT_IMAGE_THUMBNAIL_WIDTH_TITLE' , 'Largeur des miniatures des articles');
define('PRODUCT_IMAGE_THUMBNAIL_WIDTH_DESC' , 'Largeur maximum des miniatures des articles en pixel (Standard: 160). Pour des valeurs plus grandes il faut éventuellement ajuster "productPreviewImage" dans le fichier stylesheet.css de la miniature.');
define('PRODUCT_IMAGE_THUMBNAIL_HEIGHT_TITLE' , 'Hauteur de la miniature de l\'article');
define('PRODUCT_IMAGE_THUMBNAIL_HEIGHT_DESC' , 'Hauteur maximum de la miniature de l\'article en pixel (Standard: 160)');

define('PRODUCT_IMAGE_INFO_WIDTH_TITLE' , 'Largeur des images d\'information d\'article'
);
define('PRODUCT_IMAGE_INFO_WIDTH_DESC' , 'Largeur maximum des images d\'information d\'article en pixel (standard: 230).'
);
define('PRODUCT_IMAGE_INFO_HEIGHT_TITLE' , 'Hauteur des images d\'information d\'article');
define('PRODUCT_IMAGE_INFO_HEIGHT_DESC' , 'Hauteur maximum des images d\'information d\'article en pixel (standard: 230).'
);

define('PRODUCT_IMAGE_POPUP_WIDTH_TITLE' , 'Largeur des images popup des articles');
define('PRODUCT_IMAGE_POPUP_WIDTH_DESC' , 'Largeur maximum des images popup des articles en pixel (standard: 800)');
define('PRODUCT_IMAGE_POPUP_HEIGHT_TITLE' , 'Hauteur des images popup des articles');
define('PRODUCT_IMAGE_POPUP_HEIGHT_DESC' , 'Hauteur maximum des images popup des articles en pixel (standard: 800)');

define('SMALL_IMAGE_WIDTH_TITLE' , 'Largeur des images des articles');
define('SMALL_IMAGE_WIDTH_DESC' , 'Largeur maximum des images des articles en pixel');
define('SMALL_IMAGE_HEIGHT_TITLE' , 'Hauteur des images des articles');
define('SMALL_IMAGE_HEIGHT_DESC' , 'Hauteur maximum des images des articles en pixel');

define('SUBCATEGORY_IMAGE_WIDTH_TITLE' , 'Largeur des images des sous-catégories');
define('SUBCATEGORY_IMAGE_WIDTH_DESC' , 'Largeur maximum des images des sous-catégories en pixel');
define('SUBCATEGORY_IMAGE_HEIGHT_TITLE' , 'Hauteur des images des sous-catégories');
define('SUBCATEGORY_IMAGE_HEIGHT_DESC' , 'Hauteur maximum des images des sous-catégories en pixel');

define('CONFIG_CALCULATE_IMAGE_SIZE_TITLE' , 'Calculer la taille des images');
define('CONFIG_CALCULATE_IMAGE_SIZE_DESC' , 'Voulez vous calculer les tailles des images?');

define('IMAGE_REQUIRED_TITLE' , 'Images nécessaires?');
define('IMAGE_REQUIRED_DESC' , 'Si vous réglé "1", des images inexistants seront affichés comme cadre. Bien pour les développeurs.');

define('MO_PICS_TITLE', 'Nombre des images de produit supplémentaires');
define('MO_PICS_DESC', 'Nombre des images de produit qui sont aditionellement à disposition.');

//This is for the Images showing your products for preview. All the small stuff.

define('PRODUCT_IMAGE_THUMBNAIL_BEVEL_TITLE' , 'Miniatures d\'article: Bevel<br /><img src="images/config_bevel.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_BEVEL_DESC' , 'Miniatures d\'article:Bevel<br /><br />Default valeur: (8,FFCCCC,330000)<br /><br />shaded bevelled edges<br />Utilisation:<br />(edge width,hex light colour,hex dark colour)');

define('PRODUCT_IMAGE_THUMBNAIL_GREYSCALE_TITLE' , 'Miniatures d\'article:Greyscale<br /><img src="images/config_greyscale.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_GREYSCALE_DESC' , 'Miniatures d\'article:Greyscale<br /><br />Default valeur: (32,22,22)<br /><br />basic black n white<br />Utilisation:<br />(int red,int green,int blue)');

define('PRODUCT_IMAGE_THUMBNAIL_ELLIPSE_TITLE' , 'Miniatures d\'article:Ellipse<br /><img src="images/config_eclipse.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_ELLIPSE_DESC' , 'Miniatures d\'article:Ellipse<br /><br />Default valeur: (FFFFFF)<br /><br />ellipse on bg colour<br />Utilisation:<br />(hex background colour)');

define('PRODUCT_IMAGE_THUMBNAIL_ROUND_EDGES_TITLE' , 'Miniatures d\'article:Round-edges<br /><img src="images/config_edge.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_ROUND_EDGES_DESC' , 'Miniatures d\'article:Round-edges<br /><br />Default valeur: (5,FFFFFF,3)<br /><br />corner trimming<br />Utilisation:<br />(edge_radius,background colour,anti-alias width)');

define('PRODUCT_IMAGE_THUMBNAIL_MERGE_TITLE' , 'Miniatures d\'article:Merge<br /><img src="images/config_merge.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_MERGE_DESC' , 'Miniatures d\'article:Merge<br /><br />Default valeur: (overlay.gif,10,-50,60,FF0000)<br /><br />overlay merge image<br />Utilisation:<br />(merge image,x start [neg = from right],y start [neg = from base],opacity, transparent colour on merge image)');

define('PRODUCT_IMAGE_THUMBNAIL_FRAME_TITLE' , 'Miniatures d\'article:Frame<br /><img src="images/config_frame.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_FRAME_DESC' , 'Miniatures d\'article:Frame<br /><br />Default valeur: (FFFFFF,000000,3,EEEEEE)<br /><br />plain raised border<br />Utilisation:<br />(hex light colour,hex dark colour,int width of mid bit,hex frame colour [optional - defaults to half way between light and dark edges])');

define('PRODUCT_IMAGE_THUMBNAIL_DROP_SHADOW_TITLE' , 'Miniatures d\'article:Drop-Shadow<br /><img src="images/config_shadow.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_DROP_SHADOW_DESC' , 'Miniatures d\'article:Drop-Shadow<br /><br />Default valeur: (3,333333,FFFFFF)<br /><br />more like a dodgy motion blur [semi buggy]<br />Utilisation:<br />(shadow width,hex shadow colour,hex background colour)');

define('PRODUCT_IMAGE_THUMBNAIL_MOTION_BLUR_TITLE' , 'Miniatures d\'article:Motion-Blur<br /><img src="images/config_motion.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_MOTION_BLUR_DESC' , 'Miniatures d\'article:Motion-Blur<br /><br />Default valeur: (4,FFFFFF)<br /><br />fading parallel lines<br />Utilisation:<br />(int number of lines,hex background colour)');

//And this is for the Images showing your products in single-view

define('PRODUCT_IMAGE_INFO_BEVEL_TITLE' , 'Images d\'article:Bevel');
define('PRODUCT_IMAGE_INFO_BEVEL_DESC' , 'Images d\'article:Bevel<br /><br />Default valeur: (8,FFCCCC,330000)<br /><br />shaded bevelled edges<br />Utilisation:<br />(edge width, hex light colour, hex dark colour)');

define('PRODUCT_IMAGE_INFO_GREYSCALE_TITLE' , 'Images d\'article:Greyscale');
define('PRODUCT_IMAGE_INFO_GREYSCALE_DESC' , 'Images d\'article:Greyscale<br /><br />Default valeur: (32,22,22)<br /><br />basic black n white<br />Utilisation:<br />(int red, int green, int blue)');

define('PRODUCT_IMAGE_INFO_ELLIPSE_TITLE' , 'Images d\'article:Ellipse');
define('PRODUCT_IMAGE_INFO_ELLIPSE_DESC' , 'Images d\'article:Ellipse<br /><br />Default valeur: (FFFFFF)<br /><br />ellipse on bg colour<br />Utilisation:<br />(hex background colour)');

define('PRODUCT_IMAGE_INFO_ROUND_EDGES_TITLE' , 'Images d\'article:Round-edges');
define('PRODUCT_IMAGE_INFO_ROUND_EDGES_DESC' , 'Images d\'article:Round-edges<br /><br />Default valeur: (5,FFFFFF,3)<br /><br />corner trimming<br />Utilisation:<br />( edge_radius, background colour, anti-alias width)');

define('PRODUCT_IMAGE_INFO_MERGE_TITLE' , 'Images d\'article:Merge');
define('PRODUCT_IMAGE_INFO_MERGE_DESC' , 'Images d\'article:Merge<br /><br />Default valeur: (overlay.gif,10,-50,60,FF0000)<br /><br />overlay merge image<br />Utilisation:<br />(merge image,x start [neg = from right],y start [neg = from base],opacity,transparent colour on merge image)');

define('PRODUCT_IMAGE_INFO_FRAME_TITLE' , 'Images d\'article:Frame');
define('PRODUCT_IMAGE_INFO_FRAME_DESC' , 'Images d\'article:Frame<br /><br />Default valeur: (FFFFFF,000000,3,EEEEEE)<br /><br />plain raised border<br />Utilisation:<br />(hex light colour,hex dark colour,int width of mid bit,hex frame colour [optional - defaults to half way between light and dark edges])');

define('PRODUCT_IMAGE_INFO_DROP_SHADOW_TITLE' , 'Images d\'article:Drop-Shadow');
define('PRODUCT_IMAGE_INFO_DROP_SHADOW_DESC' , 'Images d\'article:Drop-Shadow<br /><br />Default valeur: (3,333333,FFFFFF)<br /><br />more like a dodgy motion blur [semi buggy]<br />Utilisation:<br />(shadow width,hex shadow colour,hex background colour)');

define('PRODUCT_IMAGE_INFO_MOTION_BLUR_TITLE' , 'Images d\'article:Motion-Blur');
define('PRODUCT_IMAGE_INFO_MOTION_BLUR_DESC' , 'Images d\'article:Motion-Blur<br /><br />Default valeur: (4,FFFFFF)<br /><br />fading parallel lines<br />Utilisation:<br />(int number of lines,hex background colour)');

define('PRODUCT_IMAGE_POPUP_BEVEL_TITLE' , 'Images popup du produit:Bevel');
define('PRODUCT_IMAGE_POPUP_BEVEL_DESC' , 'Images popup du produit:Bevel<br /><br />Default valeur: (8,FFCCCC,330000)<br /><br />shaded bevelled edges<br />Utilisation:<br />(edge width,hex light colour,hex dark colour)');

define('PRODUCT_IMAGE_POPUP_GREYSCALE_TITLE' , 'Images popup du produit:Greyscale');
define('PRODUCT_IMAGE_POPUP_GREYSCALE_DESC' , 'Images popup du produit:Greyscale<br /><br />Default Valeur: (32,22,22)<br /><br />basic black n white<br />Utilisation:<br />(int red,int green,int blue)');

define('PRODUCT_IMAGE_POPUP_ELLIPSE_TITLE' , 'Images popup du produit:Ellipse');
define('PRODUCT_IMAGE_POPUP_ELLIPSE_DESC' , 'Images popup du produit:Ellipse<br /><br />Default valeur: (FFFFFF)<br /><br />ellipse on bg colour<br />Utilisation:<br />(hex background colour)');

define('PRODUCT_IMAGE_POPUP_ROUND_EDGES_TITLE' , 'Images popup du produit:Round-edges');
define('PRODUCT_IMAGE_POPUP_ROUND_EDGES_DESC' , 'Images popup du produit:Round-edges<br /><br />Default valeur: (5,FFFFFF,3)<br /><br />corner trimming<br />Utilisation:<br />(edge_radius,background colour,anti-alias width)');

define('PRODUCT_IMAGE_POPUP_MERGE_TITLE' , 'Images popup du produit:Merge');
define('PRODUCT_IMAGE_POPUP_MERGE_DESC' , 'Images popup du produit:Merge<br /><br />Default valeur: (overlay.gif,10,-50,60,FF0000)<br /><br />overlay merge image<br />Utilisation:<br />(merge image,x start [neg = from right],y start [neg = from base],opacity,transparent colour on merge image)');

define('PRODUCT_IMAGE_POPUP_FRAME_TITLE' , 'Images popup du produit:Frame');
define('PRODUCT_IMAGE_POPUP_FRAME_DESC' , 'Images popup du produit:Frame<br /><br />Default valeur: (FFFFFF,000000,3,EEEEEE)<br /><br />plain raised border<br />Utilisation:<br />(hex light colour,hex dark colour,int width of mid bit,hex frame colour [optional - defaults to half way between light and dark edges])');

define('PRODUCT_IMAGE_POPUP_DROP_SHADOW_TITLE' , 'Images popup du produit:Drop-Shadow');
define('PRODUCT_IMAGE_POPUP_DROP_SHADOW_DESC' , 'Images popup du produit:Drop-Shadow<br /><br />Default valeur: (3,333333,FFFFFF)<br /><br />more like a dodgy motion blur [semi buggy]<br />Utilisation:<br />(shadow width,hex shadow colour,hex background colour)');

define('PRODUCT_IMAGE_POPUP_MOTION_BLUR_TITLE' , 'Images popup du produit:Motion-Blur');
define('PRODUCT_IMAGE_POPUP_MOTION_BLUR_DESC' , 'Images popup du produit:Motion-Blur<br /><br />Default valeur: (4,FFFFFF)<br /><br />fading parallel lines<br />Utilisation:<br />(int number of lines,hex background colour)');

define('IMAGE_MANIPULATOR_TITLE', 'GDlib processing');
define('IMAGE_MANIPULATOR_DESC', 'Manipulateur image pour GD2 ou GD1<br /><br /><b>HINWEIS:</b> image_manipulator_GD2_advanced.php supporte des PNG\'s transparents');


define('ACCOUNT_GENDER_TITLE' , 'Titre');
define('ACCOUNT_GENDER_DESC' , 'Demander le titre lors de l\'ouverture/modification du compte');
define('ACCOUNT_DOB_TITLE' , 'Date de naissance');
define('ACCOUNT_DOB_DESC' , 'Demander la date de naissance lors de l\'ouverture/modification du compte');
define('ACCOUNT_COMPANY_TITLE' , 'Entreprise');
define('ACCOUNT_COMPANY_DESC' , 'Demander l\'entreprise lors de l\'ouverture/modification du compte');
define('ACCOUNT_SUBURB_TITLE' , 'Adresse');
define('ACCOUNT_SUBURB_DESC' , 'Demander l\'adresse lors de l\'ouverture/modification du compte');
define('ACCOUNT_STATE_TITLE' , 'Région');
define('ACCOUNT_STATE_DESC' , 'Demander la région lors de l\'ouverture/modification du compte');

define('DEFAULT_CURRENCY_TITLE' , 'Devise par défaut');
define('DEFAULT_CURRENCY_DESC' , 'Devise qui est utilisé par défaut');
define('DEFAULT_LANGUAGE_TITLE' , 'Langue par défaut');
define('DEFAULT_LANGUAGE_DESC' , 'Langue qui est utilisé par défaut');
define('DEFAULT_ORDERS_STATUS_ID_TITLE' , 'Statut de commande par défaut pour de nouvelles commandes');
define('DEFAULT_ORDERS_STATUS_ID_DESC' , 'Dès l\'entrée d\'une nouvelle commande, le statut est statut de commande.');

define('SHIPPING_MAX_WEIGHT_TITLE' , 'Poids maximum d\'un paquet à envoyer');
define('SHIPPING_MAX_WEIGHT_DESC' , 'Partenaires de transport (Post/UPS etc.) tiennen un poids de paquet maximumg. Entrer une valeur.');
define('SHIPPING_BOX_WEIGHT_TITLE' , 'Poids à vide');
define('SHIPPING_BOX_WEIGHT_DESC' , 'Quel est le poids d\'un paquet vide petit à moyen?');
define('SHIPPING_BOX_PADDING_TITLE' , 'Paquets plus grands - prise de poids en %');
define('SHIPPING_BOX_PADDING_DESC' , 'Pour environ 10  entrez 10');
define('SHOW_SHIPPING_TITLE' , 'Annonce frais de transport');
define('SHOW_SHIPPING_DESC' , 'Afficher les frais d\'expédition');
define('SHIPPING_INFOS_TITLE' , 'Frais d\'expédition');
define('SHIPPING_INFOS_DESC' , 'Sélectionne le contenu pour afficher les frais d\'expédition');
define('SHIPPING_DEFAULT_TAX_CLASS_METHOD_TITLE' , 'Méthode de facturation de la tranche d\'imposition par défaut');
define('SHIPPING_DEFAULT_TAX_CLASS_METHOD_DESC' , 'aucun: ne pas montrer d\'impôt de frais d\'expédition<br />auto proportional: afficher l\'impôt des frais d\'expédition proportionnel à la commande<br />auto max: afficher l\'impôt de frais d\'expédition en utilisant le plus grand groupe de revenus');

define('PRODUCT_LIST_FILTER_TITLE' , 'Afficher le filtre de tri dans les listes de produit?');
define('PRODUCT_LIST_FILTER_DESC' , 'Afficher le filtre de tri pour les groupes de produits/ fabricants etc. Filtre (false=inactif; true=aktif)');

define('STOCK_CHECK_TITLE' , 'Vérifier le stock');
define('STOCK_CHECK_DESC' , 'Vérifier si assez de stock est disponible pour l\'expédition.');

define('ATTRIBUTE_STOCK_CHECK_TITLE' , 'Vérifier le stock de l\'attribut');
define('ATTRIBUTE_STOCK_CHECK_DESC' , 'Vérifier le stock avec des attributs particulers');
define('STOCK_LIMITED_TITLE' , 'Enlever le stock');
define('STOCK_LIMITED_DESC' , 'Enlever la quantité des produits commandés du stock');
define('STOCK_ALLOW_CHECKOUT_TITLE' , 'Permettre l\'achat des produits qui ne sont pas en stock');
define('STOCK_ALLOW_CHECKOUT_DESC' , 'Permettre la commande même si le produit n\'est plus en stock');
define('STOCK_MARK_PRODUCT_OUT_OF_STOCK_TITLE' , 'Marquage des articles qui ne sont plus en stock');
define('STOCK_MARK_PRODUCT_OUT_OF_STOCK_DESC' , 'Montrer au client quels produits ne sont plus en stock.');
define('STOCK_REORDER_LEVEL_TITLE' , 'Notification à l\'administrateur qu\'il faut recommander un article');
define('STOCK_REORDER_LEVEL_DESC' , 'À partir de quel nombre d\'exemplaires doit apparaître cette notification? (fonction plannifiée)');
define('STORE_PAGE_PARSE_TIME_TITLE' , 'Enregistrer le temps de calcul du chargement de la page');
define('STORE_PAGE_PARSE_TIME_DESC' , 'Mémoire du temps nécessaire pour calculer les scripts jusqu\'au output de la page');
define('STORE_PARSE_DATE_TIME_FORMAT_TITLE' , 'Format de la date dans le fichier Log');
define('STORE_PARSE_DATE_TIME_FORMAT_DESC' , 'Le format de la date (standard:%j/%m/%a %H:%M:%S)');
define('STORE_DB_SLOW_QUERY_TITLE' , 'Slow Query Log');
define('STORE_DB_SLOW_QUERY_DESC' , 'La sauvegarde des queries SQL a besoin de plus de temps. <br/><strong>Attention: La sauvegarde des interrogations de la banque de données doit être activée!</strong>.<br/><strong>Attention: Le fichier peut devenir grandissime après une longue période d\'action !</strong>.<br/><br/>Le fichier log sera sauvegardé dans le dossier /log dans le répertoire principal.');
define('STORE_DB_SLOW_QUERY_TIME_TITLE' , 'Slow Query Log - temps');
define('STORE_DB_SLOW_QUERY_TIME_DESC' , 'Veuillez entrer le temps à partir duquel les queries SQL sont à noté au fichier-journal.');

define('DISPLAY_PAGE_PARSE_TIME_TITLE' , 'Afficher le temps de calcul des pages');
define('DISPLAY_PAGE_PARSE_TIME_DESC' , 'Si la sauvegarde des temps de calcul est activée, vous pouvez les afficher dans le footer. <br /><strong>désactivé</strong>: Désactive l\'annonce complètement <br /><strong>admin</strong>: Seulement l\'administrateur voit le temps de calcul <br /><strong>tous</strong>: Chacun voit les temps de calcul');
define('STORE_DB_TRANSACTIONS_TITLE' , 'Sauvegarder les interrogations de la banque des données');
define('STORE_DB_TRANSACTIONS_DESC' , 'Sauvegarder les interrogations uniques de la banque des donées au fichier-journal des temps de calcul<br/><strong>Attention: Fichier peut devenir grandissime après une longue période d‘action! </strong>.<br/><br/>Le fichier log sera sauvegardé dans le dossier /log dans le répertoire principal.');

define('USE_CACHE_TITLE' , 'Utiliser le cache');
define('USE_CACHE_DESC' , 'Utiliser les fonctionnalités de cache');

define('DB_CACHE_TITLE', 'DB cache');
define('DB_CACHE_DESC', 'Des interrogations de base de données peuvent être sauvegardé en mémoire cache pour diminuer la charge de la base de données et pour augmenter la vitesse' );

define('DB_CACHE_EXPIRE_TITLE', 'DB Cache durée de vie');
define('DB_CACHE_EXPIRE_DESC', 'Temps en secondes avant que des fichiers cache sont écrasé par des données de la base de données.');

define('DIR_FS_CACHE_TITLE' , 'Dossier cache');
define('DIR_FS_CACHE_DESC' , 'Le dossier avec les fichiers mise en cache à sauvegarder');

define('ACCOUNT_OPTIONS_TITLE', 'Type de création de compte');
define('ACCOUNT_OPTIONS_DESC', 'Comment est-ce que vous voulez configurer la procédure d‘inscription? <br />Vous avez le choix entre un compte de client et une commande unique sans création de compte (un compte sera créé, mais pas visible pour le client)');

define('EMAIL_TRANSPORT_TITLE' , 'méthode de transport email');
define('EMAIL_TRANSPORT_DESC' , '<b>Recommandation: smtp</b> - Défin si le serveur utilise un lien local au programme sendmail et s\'il a besoin d\'une connexion SMTP via TCP/IP. Des serveurs qui fonctionnent sur Windows ou Mac OS devraient utiliser SMTP.');

define('EMAIL_LINEFEED_TITLE' , 'Sauts de ligne email');
define('EMAIL_LINEFEED_DESC' , 'Défine les signes qui sont utilisés pour séparer les en-tête des mails.' );
define('EMAIL_USE_HTML_TITLE' , 'Utiliser le MIME HTML pour envoyer des mails');
define('EMAIL_USE_HTML_DESC' , 'Envoyer des emails en format HTML');
define('ENTRY_EMAIL_ADDRESS_CHECK_TITLE' , 'Vérifier les adresses email via DNS');
define('ENTRY_EMAIL_ADDRESS_CHECK_DESC' , 'L\'adresse email peuvent être vérifié via un serveur DNS');
define('SEND_EMAILS_TITLE' , 'Envoyer des mails');
define('SEND_EMAILS_DESC' , 'Envoyer des mails aux clients');
define('SENDMAIL_PATH_TITLE' , 'Chemin vers sendmail');
define('SENDMAIL_PATH_DESC' , 'Si vous utilisez sendmail, indiquez le chemin vers le programme sendmail (normalement: //usr/bin/sendmail):');
define('SMTP_MAIN_SERVER_TITLE' , 'Adresse du serveur SMTP');
define('SMTP_MAIN_SERVER_DESC' , 'Indiquez l\'adresse du serveur principal SMTP.');
define('SMTP_BACKUP_SERVER_TITLE' , 'Adresse du serveur de sauvegarde SMTP');
define('SMTP_BACKUP_SERVER_DESC' , 'Indiquez l\'adresse du serveur de sauvegarde SMTP.');
define('SMTP_USERNAME_TITLE' , 'Identifian SMTP');
define('SMTP_USERNAME_DESC' , 'Veuillez indiquer l\'identifiant de votre compte SMTP.');
define('SMTP_PASSWORD_TITLE' , 'Mot de passe SMTP');
define('SMTP_PASSWORD_DESC' , 'Veuillez indiquer le mot de passe de votre compte SMTP');
define('SMTP_AUTH_TITLE' , 'SMTP-Auth');
define('SMTP_AUTH_DESC' , 'Est-ce que le serveur SMTP nécessite une authentification sécurisée?');
define('SMTP_PORT_TITLE' , 'Port SMTP');
define('SMTP_PORT_DESC' , 'Indiquez le port SMTP de votre serveur SMTP(défaut: 25)?');

//DokuMan - 2011-09-20 - E-Mail SQL errors
define('EMAIL_SQL_ERRORS_TITLE', 'Envoyer des SQL-notifications d\'erreurs via mail');
define('EMAIL_SQL_ERRORS_DESC', 'Si "true" un mail contenant les SQL-notifications d’erreur sera envoyé au gérant de la boutique. Par contre la SQL-notification d’erreur sera caché au client. <br /> Si "false" la notification d‘erreur respective sera distribué rapidement et visible pour tout le monde (défaut).');

//Constants for contact_us
define('CONTACT_US_EMAIL_ADDRESS_TITLE' , 'Contact - adresse email');
define('CONTACT_US_EMAIL_ADDRESS_DESC' , 'Veuillez entrer une adresse mail pour nous envoyer un mail via le formulaire "Nous contacter".');
define('CONTACT_US_NAME_TITLE' , 'Nous contacter - adresse mail, nom');
define('CONTACT_US_NAME_DESC' , 'Veuillez entrer un nom pour l\'envoie du mail via le formulaire "Nous contacter".');
define('CONTACT_US_FORWARDING_STRING_TITLE' , 'Nous contacter - adresses mail de retransmission');
define('CONTACT_US_FORWARDING_STRING_DESC' , 'Veuillez entrer d\'autres adresses mails qui doivent être contactées via le forumlaire "Nous contacter" (séparés par ,).');
define('CONTACT_US_REPLY_ADDRESS_TITLE' , 'Contact - adresse mail de réponse');
define('CONTACT_US_REPLY_ADDRESS_DESC' , 'Veuillez entrer une adresse mail à laquelle vos clients peuvent répondre.');
define('CONTACT_US_REPLY_ADDRESS_NAME_TITLE' , 'Contact - adresse mail de réponse, nom');
define('CONTACT_US_REPLY_ADDRESS_NAME_DESC' , 'Nom pour l\'adresse mail de réponse.');
define('CONTACT_US_EMAIL_SUBJECT_TITLE' , 'Contact - Objet de l\'email');
define('CONTACT_US_EMAIL_SUBJECT_DESC' , 'Objet des mails du formulaire contact des boutiques');

//Constants for support system
define('EMAIL_SUPPORT_ADDRESS_TITLE' , 'Support technique - adresses email');
define('EMAIL_SUPPORT_ADDRESS_DESC' , 'Veuillez entrer une adresse mail correcte pour envoyer un mail via le <b>système de support</b> (création de compte, mot de passe oublié).');
define('EMAIL_SUPPORT_NAME_TITLE' , 'Support technique - adresse mail, nom');
define('EMAIL_SUPPORT_NAME_DESC' , 'Veuillez entrer un nom pour l\'envoie des mails via le <b>système de support</b> (création de compte, mot de passe oublié).');
define('EMAIL_SUPPORT_FORWARDING_STRING_TITLE' , 'Support technique - adresses mail de retransmission');
define('EMAIL_SUPPORT_FORWARDING_STRING_DESC' , 'Veuillez entrer d\'autres adresses mail qui doivent être contactés via le <b>système de support</b> (séparés par ,)');
define('EMAIL_SUPPORT_REPLY_ADDRESS_TITLE' , 'Support technique - adresse email de réponse');
define('EMAIL_SUPPORT_REPLY_ADDRESS_DESC' , 'Veuillez entrer une adresse mail à laquelle vos clients peuvent répondre.');
define('EMAIL_SUPPORT_REPLY_ADDRESS_NAME_TITLE' , 'Support technique - adresse email de réponse, nom');
define('EMAIL_SUPPORT_REPLY_ADDRESS_NAME_DESC' , 'Nom pour l\'adresse mail de réponse.');
define('EMAIL_SUPPORT_SUBJECT_TITLE' , 'Support technique - objet de l\'email');
define('EMAIL_SUPPORT_SUBJECT_DESC' , 'Objet des mails du <b>système de support</b>.');

//Constants for Billing system
define('EMAIL_BILLING_ADDRESS_TITLE' , 'Compensation - adresse email');
define('EMAIL_BILLING_ADDRESS_DESC' , 'Veuillez entrer une adresse mail correcte pour envoyer un mail via le <b>système de compensation</b> (confirmation de la commande, modification du statut,..).');
define('EMAIL_BILLING_NAME_TITLE' , 'Compensation - adresse email, nom');
define('EMAIL_BILLING_NAME_DESC' , 'Veuillez entrer un nom pour l\'envoie des mails via le  <b>système de compensation</b> (confirmation de la commande, modification du statut,..).');
define('EMAIL_BILLING_FORWARDING_STRING_TITLE' , 'Compensation - adresses mail de retransmission');
define('EMAIL_BILLING_FORWARDING_STRING_DESC' , 'Veuillez entrer d\'autres adresses mail qui doivent être contactés via le <b>système de compensation</b> (séparés par ,).');
define('EMAIL_BILLING_REPLY_ADDRESS_TITLE' , 'Compensation - adresse email de réponse');
define('EMAIL_BILLING_REPLY_ADDRESS_DESC' , 'Veuillez entrer une adresse mail à laquelle vos clients peuvent répondre.');
define('EMAIL_BILLING_REPLY_ADDRESS_NAME_TITLE' , 'Compensation - adresse email de réponse, nom');
define('EMAIL_BILLING_REPLY_ADDRESS_NAME_DESC' , 'Nom pour l\'adresse mail de réponse.');
define('EMAIL_BILLING_SUBJECT_TITLE' , 'Compensation - Objet de l\'email modification du statut');
define('EMAIL_BILLING_SUBJECT_DESC' , 'Veuillez entrer un objet de mail pour les mails du <b>système de compensation</b> (modifications du statut).');
define('EMAIL_BILLING_SUBJECT_ORDER_TITLE', 'Compensation - Objet du mail pour des commandes');
define('EMAIL_BILLING_SUBJECT_ORDER_DESC', 'Veuillez entrer un objet de mail pour les mails de commande. (p.ex.: <b>Votre commande{$nr},le{$date}</b>) ps: les variables suivants sont à disposition, {$nr},{$date},{$firstname},{$lastname}');
define('MODULE_ORDER_MAIL_STEP_SUBJECT_TITLE', 'Compensation - objet du mail pour des commandes');
define('MODULE_ORDER_MAIL_STEP_SUBJECT_DESC', 'Veuillez entrer un objet de mail pour les mails de confirmation de commande. (p.ex.: <b>Votre commande{$nr},le{$date}</b>) ps: les variables suivants sont à disposition, {$nr},{$date},{$firstname},{$lastname}');

define('DOWNLOAD_ENABLED_TITLE' , 'Permettre le téléchargement d\'articles');
define('DOWNLOAD_ENABLED_DESC' , 'Activer la fonction téléchargement d\'articles (logiciel etc.).');
define('DOWNLOAD_BY_REDIRECT_TITLE' , 'Téléchargement par redirection');
define('DOWNLOAD_BY_REDIRECT_DESC' , 'Utiliser la redirection du navigateur pour des téléchargements d\'articles. Désactiver sur des systèmes non-Linux/Unix.');
define('DOWNLOAD_MAX_DAYS_TITLE' , 'Date d\'expiration des liens de téléchargement (jours)');
define('DOWNLOAD_MAX_DAYS_DESC' , 'Nombre de jours d\'activité du lien de téléchargement. 0 signifie illimité.');
define('DOWNLOAD_MAX_COUNT_TITLE' , 'Nombre maximum de téléchargements d\'un produit médiatique');
define('DOWNLOAD_MAX_COUNT_DESC' , 'Réglez le nombre maximum de téléchargements permises aux clients. 0 signifie pas de téléchargement.');
define('DOWNLOAD_MULTIPLE_ATTRIBUTES_ALLOWED_TITLE' , 'Attributs multiples pour des téléchargements');
define('DOWNLOAD_MULTIPLE_ATTRIBUTES_ALLOWED_DESC' , 'Autoriser des attributs multiples afin de sauter le mode de livraison.');

define('GZIP_COMPRESSION_TITLE' , 'Activer la compression GZip');
define('GZIP_COMPRESSION_DESC' , 'Activer la compression HTTP GZip afin d\'accélerer la vitesse de construction de page.');
define('GZIP_LEVEL_TITLE' , 'Level de compression');
define('GZIP_LEVEL_DESC' , 'Sélectionnez un niveau de compression entre 0-9 (0=minimum, 9=maximum)');

define('SESSION_WARNING', '<br /><br /><span class="col-red"><strong>Attention:</strong></span>Cette fonction peut éventuellement avoir un impact sur la fiabilité de votre boutique. Veuillez seulement modifier si toutes  conséquences possibles sont claires et si le serveur soutien la fonction !');

define('SESSION_WRITE_DIRECTORY_TITLE' , 'Session emplacement');
define('SESSION_WRITE_DIRECTORY_DESC' , 'Sélectionnez le dossier suivant si les sessions doivent être sauvegardés en tant que fichier.');
define('SESSION_FORCE_COOKIE_USE_TITLE' , 'Favoriser l\'utilisation des cookies');
define('SESSION_FORCE_COOKIE_USE_DESC' , 'Démarrer la session si les cookies sont autorisés par le navigateur. (standard "false")'.SESSION_WARNING);
define('SESSION_CHECK_SSL_SESSION_ID_TITLE' , 'Vérifier l\'ID de la session SSL');
define('SESSION_CHECK_SSL_SESSION_ID_DESC' , 'Vérifier l\'ID de la session SSL pendant chaque affichage d\'une page HTTPS. (standard "false")'.SESSION_WARNING);
define('SESSION_CHECK_USER_AGENT_TITLE' , 'Vérifier l\'agent d\'utilisateur');
define('SESSION_CHECK_USER_AGENT_DESC' , 'Vérifier l\'agent d\'utilisateur du navigateur du client pendant chaque affichage d\'une page. (standard "false")'.SESSION_WARNING);
define('SESSION_CHECK_IP_ADDRESS_TITLE' , 'Vérifier votre adresse IP');
define('SESSION_CHECK_IP_ADDRESS_DESC' , 'Vérifier l\'adresse IP du client  pendant chaque affichage d\'une page. (standard "false")'.SESSION_WARNING);
define('SESSION_RECREATE_TITLE' , 'Renouveler la session');
define('SESSION_RECREATE_DESC' , 'Une fois le client se connecte renouveler la session et allouer une nouvelle ID de session (PHP >=4.1 needed). (standard"false")'.SESSION_WARNING);

define('DISPLAY_CONDITIONS_ON_CHECKOUT_TITLE' , 'Signer les condition générales');
define('DISPLAY_CONDITIONS_ON_CHECKOUT_DESC' , 'Afficher et signer les conditions générales pendant le processus de la commande');

define('META_MIN_KEYWORD_LENGTH_TITLE' , 'Longueur minimum des meta-keywords');
define('META_MIN_KEYWORD_LENGTH_DESC' , 'Longueur minimum des meta-keywords générés automatiquement (description d\'articles)');
define('META_KEYWORDS_NUMBER_TITLE' , 'Nombre meta-keywords');
define('META_KEYWORDS_NUMBER_DESC' , 'Nombre meta-keywords');
define('META_AUTHOR_TITLE' , 'auteur');
define('META_AUTHOR_DESC' , '<meta name="author">');
define('META_PUBLISHER_TITLE' , 'publisher');
define('META_PUBLISHER_DESC' , '<meta name="publisher">');
define('META_COMPANY_TITLE' , 'company');
define('META_COMPANY_DESC' , '<meta name="company">');
define('META_TOPIC_TITLE' , 'page-topic');
define('META_TOPIC_DESC' , '<meta name="page-topic">');
define('META_REPLY_TO_TITLE' , 'reply-to');
define('META_REPLY_TO_DESC' , '<meta name="reply-to">');
define('META_REVISIT_AFTER_TITLE' , 'revisit-after');
define('META_REVISIT_AFTER_DESC' , '<meta name="revisit-after">');
define('META_ROBOTS_TITLE' , 'robots');
define('META_ROBOTS_DESC' , '<meta name="robots">');
define('META_DESCRIPTION_TITLE' , 'Description');
define('META_DESCRIPTION_DESC' , '<meta name="description">');
define('META_KEYWORDS_TITLE' , 'Keywords');
define('META_KEYWORDS_DESC' , '<meta name="keywords">');

define('MODULE_PAYMENT_INSTALLED_TITLE' , 'Modules de paiement installés');
define('MODULE_PAYMENT_INSTALLED_DESC' , 'Liste des noms de fichiers des modules de paiement (séparés par point-virgule (;)). La liste est actualisée automatiquement, il n\'est pas nécessaire de l\'éditer. (Exemple:  cc.php;cod.php;paypal.php)');
define('MODULE_ORDER_TOTAL_INSTALLED_TITLE' , 'Modules totales de commande installées');
define('MODULE_ORDER_TOTAL_INSTALLED_DESC' , 'Liste des noms de fichiers des modules totales de commande (séparés par point-virgule (;)). La liste est actualisée automatiquement, il n\'est pas nécessaire de l\'éditer. (Exemple: ot_subtotal.php;ot_tax.php;ot_shipping.php;ot_total.php)');
define('MODULE_SHIPPING_INSTALLED_TITLE' , 'Modules d\'expédition installés');
define('MODULE_SHIPPING_INSTALLED_DESC' , 'Liste des noms de fichiers des modules d\'expédition (séparés par point-virgule (;)). La liste est actualisée automatiquement, il n\'est pas nécessaire de l\'éditer. (Exemple: ups.php;flat.php;item.php)');

define('CACHE_LIFETIME_TITLE', 'Durée de vie de la lémoire cache');
define('CACHE_LIFETIME_DESC', 'Temps en secondes avant que les fichers cache sont écrasés automatiquement.');
define('CACHE_CHECK_TITLE', 'Vérifer si la mémoire cache a été modifée');
define('CACHE_CHECK_DESC', 'Si  "true", les en-têtes If-Modified-Since sont tenues en compte par des contenus sauvegardés en mémoire cache.  Des en-têtes HTTP seront distribuées. Ainsi des pages consultées régulièrement ne seront pas toujours renvoyées au client.');

define('PRODUCT_REVIEWS_VIEW_TITLE', 'Évaluations dans des détails des articles');
define('PRODUCT_REVIEWS_VIEW_DESC', 'Nombre des évaluations affichées dans des détails des articles');

define('DELETE_GUEST_ACCOUNT_TITLE', 'Supprier des comptes visiteurs');
define('DELETE_GUEST_ACCOUNT_DESC', 'Supprimer des comptes visiteurs après des commande avec succès? (Commandes seront conservées)');

define('USE_WYSIWYG_TITLE', 'Activer l\'éditeur WYSIWYG');
define('USE_WYSIWYG_DESC', 'Activer l\'éditeur WYSIWYG pour CMS et des articles');

define('PRICE_IS_BRUTTO_TITLE', 'Brut admin');
define('PRICE_IS_BRUTTO_DESC', 'Autorise la saisie des prix bruts par l\'administrateur');

define('PRICE_PRECISION_TITLE', 'Décimales brute/net');
define('PRICE_PRECISION_DESC', 'Taux d\'exactitude des conversions (N\'as pas d\'impact sur l\'annonce dans la boutique, affiche toujours deux décimales.)' );

define('CHECK_CLIENT_AGENT_TITLE', 'Éviter des sessions spider');
define('CHECK_CLIENT_AGENT_DESC', 'Autoriser aux spiders des moteurs de recherche connus l\'accès à la page.');
define('SHOW_IP_LOG_TITLE', 'IP-Log pendant checkout?');
define('SHOW_IP_LOG_DESC', 'Afficher le texte "Votre IP sera sauvegardé pour des raisons de sécurité" pendant le checkout?');

define('ACTIVATE_GIFT_SYSTEM_TITLE', 'Activer le système de bons');
define('ACTIVATE_GIFT_SYSTEM_DESC', 'Activer le système de bons? <br/><br/><b>Attention: </b>Il faut toujours activer les modules  ot_coupon <a href="'.xtc_href_link(FILENAME_MODULES, 'set=ordertotal&module=ot_coupon').'"><b>ici</b></a> und ot_gv <a href="'.xtc_href_link(FILENAME_MODULES, 'set=ordertotal&module=ot_gv').'"><b>hier</b></a>.');

define('ACTIVATE_SHIPPING_STATUS_TITLE', 'Activer l\'annonce du statut d\'expédition?');
define('ACTIVATE_SHIPPING_STATUS_DESC', 'Activer l\'annonce du statut d\'expédition? (De différentes temps d\'expédition peuvent être définis pour de différents articles. Après l\'activation apparaîtra un nouveau point <b>Statut d\'expédition</b> quand vous entrez l\'article)');

define('IMAGE_QUALITY_TITLE', 'Qualité d\'image');
define('IMAGE_QUALITY_DESC', 'Qualité d\'image (0=compression la plus haute, 100=meilleure qualité)');

define('GROUP_CHECK_TITLE', 'Vérification du groupe de clients');
define('GROUP_CHECK_DESC', 'Autoriser l\'accès aux catégories, produits et contenues particuliers seulement à un certain groupe de clients? (Après l\'activation apparaîtront de différentes possibilités de saisie pour des articles, des catégorie et au contentmanager)');

define('ACTIVATE_NAVIGATOR_TITLE', 'Activer le navigateur d\'articles?');
define('ACTIVATE_NAVIGATOR_DESC', 'Activer/Désactiver le navigateur d\'articles sur les pages des articles (pour des raisons de performance à nombre d\'articles élevés)');

define('QUICKLINK_ACTIVATED_TITLE', 'Activer fonction du lien multiple/fontion de copie');
define('QUICKLINK_ACTIVATED_DESC', 'La possibilité de cocher des catégories uniques permet à la fonction du lien multiple/de copie de faciliter le processus de copier/mettre en lien un article dans plusieurs catégories.');

define('ACTIVATE_REVERSE_CROSS_SELLING_TITLE', 'Marketing croisé inverse');
define('ACTIVATE_REVERSE_CROSS_SELLING_DESC', 'Activer la fonction marketing croisé inverse');

define('DOWNLOAD_UNALLOWED_PAYMENT_TITLE', 'Module de paiement de téléchargement pas autorisé');
define('DOWNLOAD_UNALLOWED_PAYMENT_DESC', 'Séparer des modes de paiement <strong>NON</strong> autorisés pour des produits de téléchargement par une virgule. Par exemple {banktransfer,cod,invoice,moneyorder}');
define('DOWNLOAD_MIN_ORDERS_STATUS_TITLE', 'Statut de commande');
define('DOWNLOAD_MIN_ORDERS_STATUS_DESC', 'Statut de commande qui autorise des téléchargements commandés.');

// Vat ID
define('STORE_OWNER_VAT_ID_TITLE' , 'ID TVA du gérant de la boutique');
define('STORE_OWNER_VAT_ID_DESC' , 'TVA de votre entreprise');
define('DEFAULT_CUSTOMERS_VAT_STATUS_ID_TITLE' , 'Statut client pour des clients avec des ID TVA vérifés (étrangère)');
define('DEFAULT_CUSTOMERS_VAT_STATUS_ID_DESC' , 'Sélectionnez un (groupe de) statut du client pour des clients avec des ID TVA vérifiés!');
define('ACCOUNT_COMPANY_VAT_CHECK_TITLE' , 'Demander l\'ID TVA');
define('ACCOUNT_COMPANY_VAT_CHECK_DESC' , 'L\'ID TVA peut être saisie par les clients. Si false, le champ de saisie ne sera plus affiché.');
define('ACCOUNT_COMPANY_VAT_LIVE_CHECK_TITLE' , 'Vérifier la plausibilité de l\'ID TVA');
define('ACCOUNT_COMPANY_VAT_LIVE_CHECK_DESC' , 'La plausibilité de l\'ID TVA sera vérifié en ligne. Pour cela le service web du portail des impôts de l‘UE (<a href="http://ec.europa.eu/taxation_customs" style="font-style:italic">http://ec.europa.eu/taxation_customs</a>).<br/> Nécessite PHP5 avec soutien "SOAP" activé!<br/><br/><span class="messageStackSuccess"> Le soutien "PHP5 SOAP" '.(in_array ('soap', get_loaded_extensions()) ? '' : '<span class="messageStackError"> N’EST PAS </span>').' Active actuellement!</span><br/><br/>');
define('ACCOUNT_COMPANY_VAT_GROUP_TITLE' , 'Ajuster le groupe du client après la vérification de l\'ID TVA?');
define('ACCOUNT_COMPANY_VAT_GROUP_DESC' , 'L\'activation de cette option modifiera le groupe du client après une vérification positive de l\'ID TVA');
define('ACCOUNT_VAT_BLOCK_ERROR_TITLE' , 'Empêcher la saisie des ID TVA faux ou pas validés?');
define('ACCOUNT_VAT_BLOCK_ERROR_DESC' , 'Après l\'activation de cette option seulement des ID TVA vérifiés et corrects seront saisies');
define('DEFAULT_CUSTOMERS_VAT_STATUS_ID_LOCAL_TITLE', 'Statut client pour des clients avec des ID TVA vérifés (national)');
define('DEFAULT_CUSTOMERS_VAT_STATUS_ID_LOCAL_DESC', 'Sélectionnez un (groupe de) statut du client pour des clients avec des ID TVA vérifiés!');

// Google Conversion
define('GOOGLE_CONVERSION_TITLE', 'Google Conversion-Tracking');
define('GOOGLE_CONVERSION_DESC', 'L\'enregistrement des keywords de conversion pendant des commandes');
define('GOOGLE_CONVERSION_ID_TITLE', 'ID de conversion');
define('GOOGLE_CONVERSION_ID_DESC', 'Votre ID de conversion Google');
define('GOOGLE_LANG_TITLE', 'Langue Google');
define('GOOGLE_LANG_DESC', 'Code ISO des langues utilisées');
define('GOOGLE_CONVERSION_LABEL_TITLE', 'Label de conversion de Google');
define('GOOGLE_CONVERSION_LABEL_DESC', 'Votre label de conversion de Google');

// Afterbuy
define('AFTERBUY_ACTIVATED_TITLE', 'Actif');
define('AFTERBUY_ACTIVATED_DESC', 'Activer la passerelle Afterbuy');
define('AFTERBUY_PARTNERID_TITLE', 'ID d\'affiliation ');
define('AFTERBUY_PARTNERID_DESC', 'Votre ID Afterbuy d\'affiliation');
define('AFTERBUY_PARTNERPASS_TITLE', 'Mot de passe d\'affiliation');
define('AFTERBUY_PARTNERPASS_DESC', 'Votre mot de passe d\'affiliation pour la passerelle Afterbuy XML');
define('AFTERBUY_USERID_TITLE', 'ID utilisateur');
define('AFTERBUY_USERID_DESC', 'Votre ID utilisateur Afterbuy');
define('AFTERBUY_ORDERSTATUS_TITLE', 'Statut de commande');
define('AFTERBUY_ORDERSTATUS_DESC', 'Statut de commande  après transmission des données de commande');
define('AFTERBUY_URL', 'Vous trouvez une description de Afterbuy ici: <a href="http://www.afterbuy.de" target="new">http://www.afterbuy.de</a>');
define('AFTERBUY_DEALERS_TITLE', 'Marquer en tant que commerçant');
define('AFTERBUY_DEALERS_DESC', 'Entrez les ID de groupe des commerçants qui doivent devenir des commerçants dans Afterbuy ici. <br />Exemple: <em>6,5,8</em>. Pas d‘espaces autorisés!'
);
define('AFTERBUY_IGNORE_GROUPE_TITLE', 'Ignoruer le groupe de clients');
define('AFTERBUY_IGNORE_GROUPE_DESC', 'Ignorer quel groupe de clients? <br />Exemple: <em>6,5,8</em>. Pas d‘espaces autorisés!');

// Search-Options
define('SEARCH_IN_DESC_TITLE', 'Recherche dans la description du produit');
define('SEARCH_IN_DESC_DESC', 'Activer afin de rendre possible la recherche dans la description du produit (court + longue)');
define('SEARCH_IN_ATTR_TITLE', 'Recherche dans des attributs du produit');
define('SEARCH_IN_ATTR_DESC', 'Activer afin de rendre possible la recherche dans des attributs du produit (p.ex. couleur, longueur)');
define('SEARCH_IN_MANU_TITLE', 'Recherche dans commerçants');
define('SEARCH_IN_MANU_DESC', 'Activer afin de rendre possible la recherche dans commerçants');

// changes for 3.0.4 SP2
define('REVOCATION_ID_TITLE', 'Droit de retour de la marchandise');
define('REVOCATION_ID_DESC', 'Sélectionne le contenu afin d\'afficher le droit de retour de la marchandise');
define('DISPLAY_REVOCATION_ON_CHECKOUT_TITLE', 'Annonce droit de retour de la marchandise');
define('DISPLAY_REVOCATION_ON_CHECKOUT_DESC', 'Afficher Droit de retour de la marchandise sur checkout_confirmation?');

// BOF - Tomcraft - 2009-10-03 - Paypal Express Modul
define('PAYPAL_MODE_TITLE', 'Mode PayPal:');
define('PAYPAL_MODE_DESC', 'En direct (Normal) ou essai (Sandbox). Selon le mode il faut modifier l’accès API chez PayPal : <br/>Link: <a href="https://www.paypal.com/de/cgi-bin/webscr?cmd=_get-api-signature&generic-flow=true" target="_blank"><strong>Mettre en place un accès API pour le mode en direct </strong></a><br/>Link: <a href="https://www.sandbox.paypal.com/de/cgi-bin/webscr?cmd=_get-api-signature&generic-flow=true" target="_blank"><strong> Mettre en place un accès API pour le mode essai </strong></a><br/> Vous n’avez pas encore de compte PayPal? <a href="https://www.paypal.com/de/cgi-bin/webscr?cmd=_registration-run" target="_blank"><strong>Veuillez cliquer ici afin d’ouvrir un compte PayPal.</strong></a>');
define('PAYPAL_API_USER_TITLE', 'Utilisateur PayPal API (en direct)');
define('PAYPAL_API_USER_DESC', 'Entrez le nom d\'utilisateur ici.');
define('PAYPAL_API_PWD_TITLE', 'Mot de passe PayPal API (en direct)');
define('PAYPAL_API_PWD_DESC', 'Entrez le mot de passe ici.');
define('PAYPAL_API_SIGNATURE_TITLE', 'Signature PayPal-API (en direct)');
define('PAYPAL_API_SIGNATURE_DESC', 'Entrez la signature API ici.');
define('PAYPAL_API_SANDBOX_USER_TITLE', 'Utilisateur PayPal API (Sandbox)');
define('PAYPAL_API_SANDBOX_USER_DESC', 'Entrez le nom d\'utilisateur ici.');
define('PAYPAL_API_SANDBOX_PWD_TITLE', 'Mot de passe PayPal API (Sandbox)');
define('PAYPAL_API_SANDBOX_PWD_DESC', 'Entrez le mot de passe ici.');
define('PAYPAL_API_SANDBOX_SIGNATURE_TITLE', 'Signature PayPal API (Sandbox)');
define('PAYPAL_API_SANDBOX_SIGNATURE_DESC', 'Entrez la signature API ici.');
define('PAYPAL_API_VERSION_TITLE', 'Version PayPal API');
define('PAYPAL_API_VERSION_DESC', 'Entrez la version PayPal API actuel ici - p.ex.: 119.0');
define('PAYPAL_API_IMAGE_TITLE', 'Logo de boutique PayPal');
define('PAYPAL_API_IMAGE_DESC', 'Entrez le fichier logo qui doit être affiché sur PayPal ici. <br />Attention: La Transmission se passe seulement si la boutique travaille en mode SSL.<br />Largeur maximum de l’image 750px, hauteur maximum 90px .<br />Le fichier provient de: '.DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/img/');
define('PAYPAL_API_CO_BACK_TITLE', 'Couleur de fond PayPal');
define('PAYPAL_API_CO_BACK_DESC', 'Entrez la couleur de fond qui doit être affiché sur PayPal ici. P.ex. FEE8B9');
define('PAYPAL_API_CO_BORD_TITLE', 'Couleur de bordure PayPal');
define('PAYPAL_API_CO_BORD_DESC', 'Entrez la couleur de bordure qui doit être affiché sur PayPal ici. P.ex. E4C558');
define('PAYPAL_ERROR_DEBUG_TITLE', 'Message d\'erreur PayPal');
define('PAYPAL_ERROR_DEBUG_DESC', 'Est-ce que vous souhaitez afficher l\'erreur original de PayPal? Normal=false');
define('PAYPAL_ORDER_STATUS_TMP_ID_TITLE', 'Statut de commande "interrompu"');
define('PAYPAL_ORDER_STATUS_TMP_ID_DESC', 'Sélectionnez le statut de commande');
define('PAYPAL_ORDER_STATUS_SUCCESS_ID_TITLE', 'Statut de commande OK');
define('PAYPAL_ORDER_STATUS_SUCCESS_ID_DESC', 'Sélectionnez le statut de command pour une transaction avec succès (p.ex. paiement PP ouvert)');
define('PAYPAL_ORDER_STATUS_PENDING_ID_TITLE', 'Statut de commande "en traitement"');
define('PAYPAL_ORDER_STATUS_PENDING_ID_DESC', 'Choisissez le statut de commande pour une transaction qui n\'a pas encore été traité par PayPal (p.ex. paiement PP attendant)');
define('PAYPAL_ORDER_STATUS_REJECTED_ID_TITLE', 'Statut de commande "refusé"');
define('PAYPAL_ORDER_STATUS_REJECTED_ID_DESC', 'Choisissez le statut de commande pour une transaction qui refusé(p.ex. paiement PP refusé)');
define('PAYPAL_COUNTRY_MODE_TITLE', 'Mode de pays PayPal');
define('PAYPAL_COUNTRY_MODE_DESC', 'Sélectionnez le paramètre pour le mode de pays. De différents fonctions de PayPal ne sont que possible en Grand-Bretagne (p.ex. Direct Payment)');
define('PAYPAL_EXPRESS_ADDRESS_CHANGE_TITLE', 'Coordonnées PayPall-Express');
define('PAYPAL_EXPRESS_ADDRESS_CHANGE_DESC', 'Autorise la modification des coordonnées transmises par PayPal');
define('PAYPAL_EXPRESS_ADDRESS_OVERRIDE_TITLE', 'Transmettre l\'adresse d\'expédition');
define('PAYPAL_EXPRESS_ADDRESS_OVERRIDE_DESC', 'Autorise la modification des coordonnées transmises par PayPal (compte existant)');
define('PAYPAL_INVOICE_TITLE', 'Préfixe de boutique pour numéro de facture de PayPal');
define('PAYPAL_INVOICE_DESC', 'Libre choix des lettres (préfixe) qui précèdent le numéro de commande et qui sont utilisés pour la création du numéro de facturation PayPal. <br /> Ainsi plusieurs boutiques peuvent travailler avec un seul compte PayPal. Des conflits concernant les mêmes numéros de commande sont évités. Chaque commande obtient un numéro de facturation sur le compte PayPal.');
define('PAYPAL_BRANDNAME_TITLE', 'Nom boutique PayPal');
define('PAYPAL_BRANDNAME_DESC', 'Entrez le nom que vous souhaitez afficher sur PayPal.');
// EOF - Tomcraft - 2009-10-03 - Paypal Express Modul

// BOF - Tomcraft - 2009-11-02 - New admin top menu
define('USE_ADMIN_TOP_MENU_TITLE' , 'Navigation top de l\'administrateur');
define('USE_ADMIN_TOP_MENU_DESC' , 'Activer la navigation top de l\'administrateur? Sinon le menu sera affiché sur le côté gauche (classique)' );
// EOF - Tomcraft - 2009-11-02 - New admin top menu

// BOF - Tomcraft - 2009-11-02 - Admin language tabs
define('USE_ADMIN_LANG_TABS_TITLE' , 'Onglets de langue dans catégories/articles');
define('USE_ADMIN_LANG_TABS_DESC' , 'Activer les onglets de langue dans les champs de saisie pour des catégories/des articles?');
// EOF - Tomcraft - 2009-11-02 - Admin language tabs

// BOF - Hendrik - 2010-08-11 - Thumbnails in admin products list
define('USE_ADMIN_THUMBS_IN_LIST_TITLE' , 'Images listes des produits');
define('USE_ADMIN_THUMBS_IN_LIST_DESC' , 'Afficher une colonne supplémentaire avec des images des catégories / articles dans la liste des produits de l\'administrateur?');
define('USE_ADMIN_THUMBS_IN_LIST_STYLE_TITLE', 'Images des listes des produits style CSS');
define('USE_ADMIN_THUMBS_IN_LIST_STYLE_DESC', 'Vous pouvez entrer des informations simples en style CSS ici - p.ex. pour la largeur maximale: max-width:90px;');
// EOF - Hendrik - 2010-08-11 - Thumbnails in admin products list

// BOF - Tomcraft - 2009-11-05 - Advanced contact form
//define('USE_CONTACT_EMAIL_ADDRESS_TITLE' , 'Kontaktformular - Sendeoption'); // not needed anymore!
//define('USE_CONTACT_EMAIL_ADDRESS_DESC' , 'Kontakt-E-Mail-Adresse des Shops zum Versenden des Kontaktformulars verwenden (wichtig f&uuml;r einige Provider z.B Hosteurope)'); // not needed anymore!
// EOF - Tomcraft - 2009-11-05 - Advanced contact form

// BOF - Dokuman - 2010-02-04 - delete cache files in admin section
define('DELETE_CACHE_SUCCESSFUL', 'Mémoire cache vidée avec succès.');
define('DELETE_TEMP_CACHE_SUCCESSFUL', 'Mémoire cache modèle vidée avec succès.');
// EOF - Dokuman - 2010-02-04 - delete cache files in admin section

// BOF - DokuMan - 2010-08-13 - set Google RSS Feed in admin section
define('GOOGLE_RSS_FEED_REFID_TITLE' , 'Flux RSS Google - refID');
define('GOOGLE_RSS_FEED_REFID_DESC' , 'Entrez l\'ID de campagne ici. Il sera ajouté à tout lien du flux RSS Google');
// EOF - DokuMan - 2010-08-13 - set Google RSS Feed in admin section

// BOF - web28 - 2010-08-17 -  Bildgroessenberechnung kleinerer Bilder
define('PRODUCT_IMAGE_NO_ENLARGE_UNDER_DEFAULT_TITLE', 'Mise à échelle des image avec une résolution faible');
define('PRODUCT_IMAGE_NO_ENLARGE_UNDER_DEFAULT_DESC', 'Activez le paramètre <strong>Non</strong> afind d‘éviter que les images avec une résolution faible sont ajustés aux valeurs par défaut de largeur et de hauteur. Si vous activez le paramètre <strong>Oui</strong>, les images avec une résolution faible seront ajustées aux valeurs par défaut. Dans ce cas les images peuvent apparaître floues et pixelisées.');
// EOF - web28 - 2010-08-17 -  Bildgroessenberechnung kleinerer Bilder

//BOF - hendrik - 2011-05-14 - independent invoice number and date
//define('IBN_BILLNR_TITLE', 'N&auml;chste Rechnungsnummer');
//define('IBN_BILLNR_DESC', 'Beim Zuweisung einer Bestellung wird diese Nummer als n&auml;chstes vergeben.');
//define('IBN_BILLNR_FORMAT_TITLE', 'Rechnungsnummer Format');       //ibillnr
//define('IBN_BILLNR_FORMAT_DESC', 'Aufbauschema Rechn.Nr.: {n}=laufende Nummer, {d}=Tag, {m}=Monat, {y}=Jahr, <br>z.B. "100{n}-{d}-{m}-{y}" ergibt "10099-28-02-2007"');
//EOF - hendrik - 2011-05-14 - independent invoice number and date

//BOC - h-h-h - 2011-12-23 - Button "Buy Now" optional - default off
define('SHOW_BUTTON_BUY_NOW_TITLE', 'Afficher le bouton "panier" dans les listes des produits');
define('SHOW_BUTTON_BUY_NOW_DESC', '<span class="col-red"><strong>ATTENTION:</strong></span> Cela peut donner lieu à des avertissements si le client n‘aperçoit pas les caractéristiques essentielles de l’article dans la liste des produits !');
//EOC - h-h-h - 2011-12-23 - Button "Buy Now" optional - default off

//split page results
define('MAX_DISPLAY_ORDER_RESULTS_TITLE', 'Nombre commandes par page');
define('MAX_DISPLAY_ORDER_RESULTS_DESC', 'Nombre maximum des commandes par page');
define('MAX_DISPLAY_LIST_PRODUCTS_TITLE', 'Nombre des articles par page');
define('MAX_DISPLAY_LIST_PRODUCTS_DESC', 'Nombre maximum des articles par page');
define('MAX_DISPLAY_LIST_CUSTOMERS_TITLE', 'Nombre des clients par page');
define('MAX_DISPLAY_LIST_CUSTOMERS_DESC', 'Nombre maximum des clients par page');
define('MAX_ROW_LISTS_ATTR_OPTIONS_TITLE', 'Caractéristiques de l\'article: Nombre des caractéristiques de l\'article par page');
define('MAX_ROW_LISTS_ATTR_OPTIONS_DESC', 'Nombre maximum des caractéristiques de l\'article (options) par page');
define('MAX_ROW_LISTS_ATTR_VALUES_TITLE', 'Caractéristiques de l\'article: Nombre des valeurs d\'option par page');
define('MAX_ROW_LISTS_ATTR_VALUES_DESC', 'Nombre maximum des valeurs d\'option par page.');
define('MAX_DISPLAY_STATS_RESULTS_TITLE', 'Nombre des résultat des statistiques par page');
define('MAX_DISPLAY_STATS_RESULTS_DESC', 'Nombre maximum des résultats des statistiques par page.');
define('MAX_DISPLAY_COUPON_RESULTS_TITLE', 'Nombre des résultat des coupon par page');
define('MAX_DISPLAY_COUPON_RESULTS_DESC', 'Nombre maximum des résultat des coupon par page');

//whos online
define('WHOS_ONLINE_TIME_LAST_CLICK_TITLE', 'Qui est en ligne - Période d\'affichage en secondes');
define('WHOS_ONLINE_TIME_LAST_CLICK_DESC', 'Période d\'affichage des utilisateurs en ligne dans le tableau "Qui est en ligne". Après cette période les entrées seront supprimées. (Valeur minimale: 900)');

//sessions
define('SESSION_LIFE_ADMIN_TITLE', 'Durée de vie de la session de l\'administrateur');
define('SESSION_LIFE_ADMIN_DESC', 'Durée en secondes après laquelle le temps de session de l\'administrateur est écoulé (déconnexion automatique) - Standard 7200<br /> La valeur définie ici n\'est que valide si la manutention de session est basée sur la base de données (configure.php => define(\'STORE_SESSIONS\', \'mysql\');)<br />Valeur maximum: 14400');
define('SESSION_LIFE_CUSTOMERS_TITLE', 'Durée de vie de la session des clients');
define('SESSION_LIFE_CUSTOMERS_DESC', 'Durée en secondes après laquelle le temps de session des clientsest écoulé (déconnexion automatique) - Standard 1440<br /> La valeur définie ici n\'est que valide si la manutention de session est basée sur la base de données (configure.php => define(\'STORE_SESSIONS\', \'mysql\');)<br />Valeur maximum: 14400');

//checkout confirmation options
define('CHECKOUT_USE_PRODUCTS_SHORT_DESCRIPTION_TITLE', 'Page de validation de commande: brève description');
define('CHECKOUT_USE_PRODUCTS_SHORT_DESCRIPTION_DESC', 'Voulez-vous que la brève description de l\'article est affichée sur la page de validation de commande? Remarque : La brève description n\'est que affichée s\'il n\'y a pas de description de commande de l\'article. Si False la brève description n\'est pas affiché de manière générale !');
define('CHECKOUT_USE_PRODUCTS_DESCRIPTION_FALLBACK_LENGTH_TITLE', 'Longueur de la description si la brève description est vide');
define('CHECKOUT_USE_PRODUCTS_DESCRIPTION_FALLBACK_LENGTH_DESC', 'À partir de quelle longueur souhaitez vous couper la description si pas de brève description est disponible?');
define('CHECKOUT_SHOW_PRODUCTS_IMAGES_TITLE', 'Page de validation de commande: images de produits');
define('CHECKOUT_SHOW_PRODUCTS_IMAGES_DESC', 'Voulez-vous que les images des articles seront affichées sur la page de validation de commande?');
define('CHECKOUT_SHOW_PRODUCTS_MODEL_TITLE', 'Page de validation de commande: Numéro d\'article');
define('CHECKOUT_SHOW_PRODUCTS_MODEL_DESC', 'Voulez-vous que le numéro d\'article sera affiché sur la page de validation de commande?');

// email billing attachments
define('EMAIL_BILLING_ATTACHMENTS_TITLE', 'Pièces jointes des mails de compensation pour des commandes' );
define('EMAIL_BILLING_ATTACHMENTS_DESC', 'Exemple pour des pièces jointes – supposé que les fichiers se trouvent dans le répertoire des boutiques <b>/media/content/</b>. Séparer plusieurs pièces jointes par virgule sans espace:<br /> media/content/agb.pdf,media/content/widerruf.pdf');

// email images
define('SHOW_IMAGES_IN_EMAIL_TITLE', 'Insérer des images des articles dans le mail de commande');
define('SHOW_IMAGES_IN_EMAIL_DESC', 'Insérer les images des articles dans le mail HTML de validation de commande (augmente le risque que le mail sera considéré comme un spam)');
define('SHOW_IMAGES_IN_EMAIL_DIR_TITLE', 'Dossier d\'images des mails');
define('SHOW_IMAGES_IN_EMAIL_DIR_DESC', 'Sélection dossier d\'images des mails');
define('SHOW_IMAGES_IN_EMAIL_STYLE_TITLE', 'Style CSS des images des mails');
define('SHOW_IMAGES_IN_EMAIL_STYLE_DESC', 'Vous pouvez entrer des informations simples en style CSS ici - p.ex. largeur maximum: max-width:90px;');

//popup windows configuration
define('POPUP_SHIPPING_LINK_PARAMETERS_TITLE', 'Paramètres URL des fenêtres popup des frais de transport');
define('POPUP_SHIPPING_LINK_PARAMETERS_DESC', 'Vous pouvez entrer des paramètres URL ici - Standard: &KeepThis=true&TB_iframe=true&height=400&width=600');
define('POPUP_SHIPPING_LINK_CLASS_TITLE', 'Classe CSS des fenêtres popup des frais de transport');
define('POPUP_SHIPPING_LINK_CLASS_DESC', 'Vous pouvez entrer les classes CSS ici - Standard: thickbox');
define('POPUP_CONTENT_LINK_PARAMETERS_TITLE', 'Paramètres URL des fenêtres popup des pages contenu');
define('POPUP_CONTENT_LINK_PARAMETERS_DESC', 'Vous pouvez entrer le paramètre URL ici - Standard: &KeepThis=true&TB_iframe=true&height=400&width=600');
define('POPUP_CONTENT_LINK_CLASS_TITLE', 'Class CSS des fenêtres popup des pages contenu');
define('POPUP_CONTENT_LINK_CLASS_DESC', 'Vous pouvez entrer les classes CSS ici - Standard: thickbox');
define('POPUP_PRODUCT_LINK_PARAMETERS_TITLE', 'Paramètres URL des fenêtres popup des pages produit');
define('POPUP_PRODUCT_LINK_PARAMETERS_DESC', 'Vous pouvez entrer les paramètres URL ici - Standard: &KeepThis=true&TB_iframe=true&height=450&width=750');
define('POPUP_PRODUCT_LINK_CLASS_TITLE', 'Classe CSS des fenêtres popup des pages produit');
define('POPUP_PRODUCT_LINK_CLASS_DESC', 'Vous pouvez entrer les classes CSS ici - Standard: thickbox');
define('POPUP_COUPON_HELP_LINK_PARAMETERS_TITLE', 'Paramètres URL des fenêtres popup de l\'aide coupon');
define('POPUP_COUPON_HELP_LINK_PARAMETERS_DESC', 'Vous pouvez entrer les paramètres URL ici - Standard: &KeepThis=true&TB_iframe=true&height=400&width=600');
define('POPUP_COUPON_HELP_LINK_CLASS_TITLE', 'Classe CSS des fenêtres popup de l\'aide coupon');
define('POPUP_COUPON_HELP_LINK_CLASS_DESC', 'Vous pouvez entrer les classes CSS ici - Standard: thickbox');

define('POPUP_PRODUCT_PRINT_SIZE_TITLE', 'Taille de fenêtre de la vue d\'impression du produit');
define('POPUP_PRODUCT_PRINT_SIZE_DESC', 'Vous pouvez définir la taille de la fenêtre popup ici - Standard: width=640, height=600');
define('POPUP_PRINT_ORDER_SIZE_TITLE', 'Taille de fenêtre de la vue d\'impression de la commande');
define('POPUP_PRINT_ORDER_SIZE_DESC', 'Vous pouvez définir la taille de la fenêtre popup ici - Standard: width=640, height=600');

define('TRACKING_COUNT_ADMIN_ACTIVE_TITLE' , 'Compter les affichages de page du commerçant');
define('TRACKING_COUNT_ADMIN_ACTIVE_DESC' , 'Si l\'option est activée, les affichages de l\'administrateur sont comptés aussi. Cela peut falsifier la statistique des visiteurs (affichages plus fréquentes).');

define('TRACKING_GOOGLEANALYTICS_ACTIVE_TITLE' , 'Activer Google Analytics Tracking');
define('TRACKING_GOOGLEANALYTICS_ACTIVE_DESC' , 'Si l\'option est activée, tous les affichages de la pages seront transmises à et analysés par Googl Analytics. Avant il faut créer un compte chez <a href="http://www.google.com/analytics/" target="_blank"><b>Google Analytics</b></a>.');
define('TRACKING_GOOGLEANALYTICS_ID_TITLE' , 'Numéro de compte Google Analytics');
define('TRACKING_GOOGLEANALYTICS_ID_DESC' , 'Entrer le numéro de compte Google Analytics au format "UA-XXXXXXXX-X". Vous l\'avez reçu après la création du compte.');

define('TRACKING_PIWIK_ACTIVE_TITLE' , 'Activer Piwik Web-Analytics Tracking');
define('TRACKING_PIWIK_ACTIVE_DESC' , 'Avant de pouvoir utiliser Piwik il faut le télécharger et l\'installer dans votre espace web. Voir <a href="http://piwik.org/" target="_blank"><b>Piwik Web-Analytics</b></a>. Contrairement à Google Analytics les données sont sauvegardés localement. Le gérant de la boutique assure la responsabilité des données.');
define('TRACKING_PIWIK_LOCAL_PATH_TITLE' , 'Répertoire d\'installation Piwik (sans "http://")');
define('TRACKING_PIWIK_LOCAL_PATH_DESC' , 'Après avoir installé Piwik, entrez le répertoire ici. Il faut entrer le nom de domaine sans "http://", p.ex. "www.domain.de/piwik".');
define('TRACKING_PIWIK_ID_TITLE' , 'ID de page Piwik');
define('TRACKING_PIWIK_ID_DESC' , 'Un ID par domain est attribué sur l\'écran d\'administration de Piwik (la plus part du temps "1")');
define('TRACKING_PIWIK_GOAL_TITLE' , 'Numéro de campagne Piwik (optional)');
define('TRACKING_PIWIK_GOAL_DESC' , 'Si vous voulez suivre les objectifs prédifinis de la ligne d\'avant, entrez le numéro de campagne ici. Plus de détails sur <a href="http://piwik.org/docs/tracking-goals-web-analytics/" target="_blank"> <b>Piwik: Tracking Goal Conversions</b></a>');

define('CONFIRM_SAVE_ENTRY_TITLE', 'Un message de confirmation s\'affiche si vous enregistrez des articles/catégories');
define('CONFIRM_SAVE_ENTRY_DESC', 'Afficher un message de confirmation avant d\'enregistrer des articles/catégories? Standard: true (oui)');

define('WHOS_ONLINE_IP_WHOIS_SERVICE_TITLE', 'Qui est en ligne - Whois Lookup URL');
define('WHOS_ONLINE_IP_WHOIS_SERVICE_DESC', 'http://www.utrace.de/?query= oder http://whois.domaintools.com/');

define('STOCK_CHECKOUT_UPDATE_PRODUCTS_STATUS_TITLE', 'Fin de la commande - Désactiver des articles indisponibles');
define('STOCK_CHECKOUT_UPDATE_PRODUCTS_STATUS_DESC', 'Désactiver un articles indisponible automatiquement à la fin d\'une commande (stock 0)? L\'option devrait être désactivé pour des articles qui sont disponibles prochainement');

define('SEND_EMAILS_DOUBLE_OPT_IN_TITLE', 'Double-Opt-In pour inscription à la newsletter');
define('SEND_EMAILS_DOUBLE_OPT_IN_DESC', 'Si "true" un mail de confirmation sera envoyé au client. Il faut que l\'envoi des mails soit activé.');

define('USE_ADMIN_FIXED_TOP_TITLE', 'Fixer l\'en-tête de l\'administrateur?');
define('USE_ADMIN_FIXED_TOP_DESC', 'Voulez-vous que l\'en-tête soit toujours visible (aussi en parcourant la page)?');
define('USE_ADMIN_FIXED_SEARCH_TITLE', 'Afficher la barre de recherche de l\'administrateur?');
define('USE_ADMIN_FIXED_SEARCH_DESC', 'Voulez-vous que la barre de recherche soit toujours visible?');

define('SMTP_SECURE_TITLE' , 'SMTP SECURE');
define('SMTP_SECURE_DESC' , 'Est-ce que le serveur SMTP demande une connexion sécurisée? Vous trouvez les paramètres nécessaires auprès de votre fournisseur.');

define('DISPLAY_ERROR_REPORTING_TITLE', 'Error Reporting');
define('DISPLAY_ERROR_REPORTING_DESC', 'Voulez-vous que le Error Reporting soit affiché en tant que liste formatée dans le pied de page?');

define('DISPLAY_BREADCRUMB_OPTION_TITLE', 'Navigation breadcrumb');
define('DISPLAY_BREADCRUMB_OPTION_DESC', '<strong>name:</strong>Le nom complet de l‘article sera affiché dans la navigation Breadcrumb. <br /><strong>model:</strong> Si le numéro d‘article existe, il sera affiché dans la navigation Breadcrumb. Sinon fallback sur le nom d’article.');

define('EMAIL_WORD_WRAP_TITLE', 'WordWrap pour des textes des mails');
define('EMAIL_WORD_WRAP_DESC', 'Entrer le nombre de signe pour une ligne avant un saut de ligne dans un mail texte (seulement des nombres entiers).<br /><strong>Attention:</strong> Nombre de signe au dessus de 76 peut provoquer que le mail sera considéré comme SPAM par Spam Assassin ! Plus d’informations <a href="http://wiki.apache.org/spamassassin/Rules/MIME_QP_LONG_LINE" target="_blank">ici</a>.');

//define('USE_PAGINATION_LIST_TITLE', 'Pagination Liste'); // Tomcraft - 2017-07-12 - Not used anymore since r10840, see: http://trac.modified-shop.org/ticket/1238
//define('USE_PAGINATION_LIST_DESC', 'Verwende eine HTML Liste (ul / li Tag) f&uuml;r die Pagination / Seitenschaltung.<br/><b>Achtung:</b> Das funktioniert nur mit einem ab Shopversion 2.0.0.0 kompatiblem Template!'); // Tomcraft - 2017-07-12 - Not used anymore since r10840, see: http://trac.modified-shop.org/ticket/1238

define('ORDER_STATUSES_FOR_SALES_STATISTICS_TITLE', 'Statistiques de ventes des filtres');
define('ORDER_STATUSES_FOR_SALES_STATISTICS_DESC', 'Choisissez les statuts de commande qui sont nécessaires pour la statistiques de ventes sur la page d’accueil de l’administrateur et dans la fenêtre déroulante du statut en utilisant le statut (Statistique de ventes des filtres). .<br />(Afin de seulement afficher les ventes effectives, choisissez le statut qui est utilisé pour des commandes terminées.) <br /><b>Remarque:</b> Afin d’afficher le filtre "statistique de ventes des filtres" dans la fenêtre déroulante des statistiques de ventes, il faut sélectionner au moins deux statuts. Sinon il est possible de sélectionner le statut souhaité directement dans la fenêtre déroulante.');

define('SAVE_IP_LOG_TITLE', 'Enregistrer l\'adresse IP');
define('SAVE_IP_LOG_DESC', 'Enregistrer l\'adresse IP dans la base des données? <br/> Pour l\'option xxx les derniers trois places sont anonymisées.');

define('META_MAX_KEYWORD_LENGTH_TITLE', 'Longueur maximum des Meta-Keywords');
define('META_MAX_KEYWORD_LENGTH_DESC', 'Longueur maximum des Meta-Keywords générés automatiquement (description de l\'article)');
define('META_DESCRIPTION_LENGTH_TITLE', 'Longueur Meta-Description');
define('META_DESCRIPTION_LENGTH_DESC', 'Longueur maximum de la description (en lettres)');
define('META_STOP_WORDS_TITLE', 'Stop Words');
define('META_STOP_WORDS_DESC', 'Veuillez entrer les Keywords à ne pas utiliser en tant que liste séparé par virgule ici.');
define('META_GO_WORDS_TITLE', 'Go Words');
define('META_GO_WORDS_DESC', 'Veuillez entrer les Keywords à utiliser explicitement en tant que liste séparé par virgule ici.');

//BOC added text constants for group id 20, noRiddle
define('CSV_CATEGORY_DEFAULT_TITLE', 'Catégorie pour l\'import');
define('CSV_CATEGORY_DEFAULT_DESC', 'Tous les articles qui <b>ne sont pas</b> attribués à une catégorie dans le fichier d\'importation et qui ne sont pas encore disponible dans la boutique seront importées dans cette catégorie. <br/><b>Important:</b> Si vous ne voulez pas importer des articles sans catégorie dans le fichier d‘importation CSV, choisissez la catégorie „Top“. Pas d’articles sont importés dans cette catégorie.');
define('CSV_TEXTSIGN_TITLE', 'Signe de reconnaissance de texte');
define('CSV_TEXTSIGN_DESC', 'P.ex. "   |  <span style="color:#c00;">Si le signe de séparation est un point-virgule, il faut que le signe de reconnaissance de texte soit mise sur " !</span>');
define('CSV_SEPERATOR_TITLE', 'Signe de séparation');
define('CSV_SEPERATOR_DESC', 'P.ex. ;   |  <span style="color:#c00;">si le champ de saisie reste vide, il faut utiliser par défaut \t (= Tab) pour export/import!</span>');
define('COMPRESS_EXPORT_TITLE', 'Compression');
define('COMPRESS_EXPORT_DESC', 'Compression des données exportées');
define('CSV_CAT_DEPTH_TITLE', 'Profondeur de catégorie');
define('CSV_CAT_DEPTH_DESC', 'Quelle profondeur doit obtenir l\'arbre des catégories ? (p.ex. paramètre par défaut 4: catégorie principlae et trois sous-catégories) <br/> Ce paramètre est important afin d\'importer correctement les catégories en format CSV. La même chose vaut pour l\'export. <br /><span style="color:#c00;"> Plus que 4 peut mener à des pertes de performance est n\'est éventuellement pas favorable aux clients!');
//EOC added text constants for group id 20, noRiddle

define('MIN_GROUP_PRICE_STAFFEL_TITLE', 'Nombre supplémentaire des prix échelonnés');
define('MIN_GROUP_PRICE_STAFFEL_DESC', 'Nombre supplémentaire des prix échelonnés affichés');

define('MODULE_CAPTCHA_ACTIVE_TITLE', 'Activer Captcha');
define('MODULE_CAPTCHA_ACTIVE_DESC', 'Activer Captcha pour quelles sections de boutique?');
define('MODULE_CAPTCHA_LOGGED_IN_TITLE', 'Clients connectés');
define('MODULE_CAPTCHA_LOGGED_IN_DESC', 'Affichage Capta des clients connectés');
define('MODULE_CAPTCHA_USE_COLOR_TITLE', 'Couleurs aléatoires');
define('MODULE_CAPTCHA_USE_COLOR_DESC', 'Affichages des lignes et signes dans des couleurs aléatoires');
define('MODULE_CAPTCHA_USE_SHADOW_TITLE', 'Ombres');
define('MODULE_CAPTCHA_USE_SHADOW_DESC', 'Ombres supplémentaires des signes dans Captcha');
define('MODULE_CAPTCHA_CODE_LENGTH_TITLE', 'Longueur Captcha');
define('MODULE_CAPTCHA_CODE_LENGTH_DESC', 'Nombre des signes dans Captcha <br/>(défaut: 6)');
define('MODULE_CAPTCHA_NUM_LINES_TITLE', 'Nombre des lignes');
define('MODULE_CAPTCHA_NUM_LINES_DESC', 'Nombre des lignes dans Captcha <br/>(défault: 70)');
define('MODULE_CAPTCHA_MIN_FONT_TITLE', 'Police minimale');
define('MODULE_CAPTCHA_MIN_FONT_DESC', 'Indication des plus petits signes en pixel dans Captcha.<br/>(défaut: 24)');
define('MODULE_CAPTCHA_MAX_FONT_TITLE', 'Police maximale');
define('MODULE_CAPTCHA_MAX_FONT_DESC', 'Indication des plus grands signes en pixel dans Captcha.<br/>(défaut: 28)');
define('MODULE_CAPTCHA_BACKGROUND_RGB_TITLE', 'Couleur de fond');
define('MODULE_CAPTCHA_BACKGROUND_RGB_DESC', 'Indication de la couleur de fond dans RGB.<br/>(défaut: 192,192,192)');
define('MODULE_CAPTCHA_LINES_RGB_TITLE', 'Couleur de ligne');
define('MODULE_CAPTCHA_LINES_RGB_DESC', 'Indication de la couleur de ligne dans RGB.<br/>(défaut: 220,148,002)');
define('MODULE_CAPTCHA_CHARS_RGB_TITLE', 'Couleur de signe');
define('MODULE_CAPTCHA_CHARS_RGB_DESC', 'Indication de la couleur de signe dans RGB. <br/>(défaut: 112,112,112)');
define('MODULE_CAPTCHA_WIDTH_TITLE', 'Largeur');
define('MODULE_CAPTCHA_WIDTH_DESC', 'Indication de la largeur en pixel dans Captcha.');
define('MODULE_CAPTCHA_HEIGHT_TITLE', 'Hauteur');
define('MODULE_CAPTCHA_HEIGHT_DESC', 'Indication de la heuteur en pixel dans Captcha.');

define('SHIPPING_STATUS_INFOS_TITLE', 'Temps de livraison');
define('SHIPPING_STATUS_INFOS_DESC', 'Sélectionne le contenue d\'afficahge des information par rapport au temps de livraison');

define('USE_SHORT_DATE_FORMAT_TITLE', 'Afficher la date en format court');
define('USE_SHORT_DATE_FORMAT_DESC', 'Afficher la date toujours en format court: <b>01.03.2014</b> au lieu de <b>Samedi, 01. März 2014</b><br /> Recommandé s\'il y a des erreurs d\'affichage en utilisant le format de date longue (langue incorrect ou problème avec des caractères spéciaux)!');

define('MAX_DISPLAY_PRODUCTS_CATEGORY_TITLE', 'Nombre d\'articles maximal');
define('MAX_DISPLAY_PRODUCTS_CATEGORY_DESC', 'Nombre maximal d\'articles de la même catégorie');
define('MAX_DISPLAY_ADVANCED_SEARCH_RESULTS_TITLE', 'Nombre résultats de recherche');
define('MAX_DISPLAY_ADVANCED_SEARCH_RESULTS_DESC', 'Nombre des articles dans les résultats de recherche');
define('MAX_DISPLAY_PRODUCTS_HISTORY_TITLE' , 'Nombre de l\'historique');
define('MAX_DISPLAY_PRODUCTS_HISTORY_DESC' , 'Nombre maximal des articles visités dernièrement');

define('PRODUCT_IMAGE_SHOW_NO_IMAGE_TITLE', 'Article noimage.gif');
define('PRODUCT_IMAGE_SHOW_NO_IMAGE_DESC', 'Affichage de noimage.gif si pas d\'image d\'article');
define('CATEGORIES_IMAGE_SHOW_NO_IMAGE_TITLE', 'Catégorie noimage.gif');
define('CATEGORIES_IMAGE_SHOW_NO_IMAGE_DESC', 'Affichage de noimage.gif si pas d\'image de catégorie');
define('MANUFACTURER_IMAGE_SHOW_NO_IMAGE_TITLE', 'Commerçant noimage.gif');
define('MANUFACTURER_IMAGE_SHOW_NO_IMAGE_DESC', 'Affiche de noimage.gif si pas d\'image de commerçant');

define('MODULE_SMALL_BUSINESS_TITLE', 'Petit entrepreneur');
define('MODULE_SMALL_BUSINESS_DESC', 'Modifier la boutique vers petit entrepreneur en vertu de § 19 de lUStG.? <br /><b>Important:</b> Sous Unter "Modules" -> "Résumé" il faut que le module "ot_tax" <a href="'.xtc_href_link(FILENAME_MODULES, 'set=ordertotal&module=ot_tax').'"><b>hier</b></a> soit désactivé ou désinstallé. En plus il faut régler les "Prix incl TVA" dans <a href="'.xtc_href_link(FILENAME_CUSTOMERS_STATUS, '').'"><b>Groupes de clients </b></a> sur "Non".');

define('COMPRESS_HTML_OUTPUT_TITLE', 'Compression HTML');
define('COMPRESS_HTML_OUTPUT_DESC', 'Compresser le rendement HTML du modèle?');
define('COMPRESS_STYLESHEET_TITLE', 'Compression CSS');
define('COMPRESS_STYLESHEET_DESC', 'Compresser Stylesheet? <br/><b>Attention:</b> Cela ne fonctionne que avec un modèle compatible avec la version de boutique 2.0.0.0!');
define('COMPRESS_JAVASCRIPT_TITLE', 'Compression JavaScript');
define('COMPRESS_JAVASCRIPT_DESC', 'Compresser un ficher JavaScript? <br/><b>Attention:</b> Cela ne fonctionne que avec un modèle compatiible avec la version de boutique 2.0.1.0!');

define('USE_ATTRIBUTES_IFRAME_TITLE', 'Éditer les attributs sur iframe');
define('USE_ATTRIBUTES_IFRAME_DESC', 'Ouvre la gestion des attributs dans l\'affichages des catégories/articles sur iframe');

define('ADMIN_HEADER_X_FRAME_OPTIONS_TITLE', 'Protection d\'administrateur Clickjacking');
define('ADMIN_HEADER_X_FRAME_OPTIONS_DESC', 'Protéger l\'espace d\'administrateur avec l\'en-tête "X-Frame-Options: SAMEORIGIN" <br>Navigateurs supportés: FF 3.6.9+ Chrome 4.1.249.1042+ IE 8+ Safari 4.0+ Opera 10.50+ ');

define('SEND_MAIL_ACCOUNT_CREATED_TITLE', 'Email pour création de compte');
define('SEND_MAIL_ACCOUNT_CREATED_DESC', 'Envoyer un mail aux clients pour la création d\'un nouveau compte?');

define('STATUS_EMAIL_SENT_COPY_TO_ADMIN_TITLE', 'Email pour changement de statut');
define('STATUS_EMAIL_SENT_COPY_TO_ADMIN_DESC', 'Envoyer un mail à l\'administrateur si le statut de la commande est modifié');

define('STOCK_CHECK_SPECIALS_TITLE', 'Vérifier les offres spéciales');
define('STOCK_CHECK_SPECIALS_DESC', 'Vérifier s\'il y a assez d\'offres spéciales pour liver la commande. <br/><br/><b>ATTENTION:</b> S\'il n\'y a pas assez d\'offres spéciales disponibles, la commande se termine seulement après une baisse de la quantité.');

define('DOWNLOAD_SHOW_LANG_DROPDOWN_TITLE', 'Fenêtre dropdown des pays au panier');
define('DOWNLOAD_SHOW_LANG_DROPDOWN_DESC', 'Afficher la fenêtre dropdown des pays au panier si seulement des articles de téléchargement sont achetés');

define('GUEST_ACCOUNT_EDIT_TITLE', 'Éditer les comptes visiteurs');
define('GUEST_ACCOUNT_EDIT_DESC', 'Est-ce que les visiteurs ont le droit de voir et modifier leurs détails du compte?');

define('EMAIL_SIGNATURE_ID_TITLE', 'Signature email');
define('EMAIL_SIGNATURE_ID_DESC', 'Sélectionnez le contenu qui est utilisé pour les signatures email de la boutique.');

define('TEXT_PAYPAL_NOT_INSTALLED', '<div class="important_info">PayPal n\'as pas encore été installé. Vous pouvez le faire <a href="'.xtc_href_link(FILENAME_MODULES, 'set=payment&module=paypal').'">ici </a>.</div>' );

define('POLICY_MIN_LOWER_CHARS_TITLE', 'Minunscules mot de passe');
define('POLICY_MIN_LOWER_CHARS_DESC', 'Combien de minuscules doit contenir le mot de passe?');
define('POLICY_MIN_UPPER_CHARS_TITLE', 'Majuscules mot de passe');
define('POLICY_MIN_UPPER_CHARS_DESC', 'Combien de majuscules doit contenir le mot de passe?');
define('POLICY_MIN_NUMERIC_CHARS_TITLE', 'Nombres mot de passe');
define('POLICY_MIN_NUMERIC_CHARS_DESC', 'Combien de nombres doit contenir le mot de passe?');
define('POLICY_MIN_SPECIAL_CHARS_TITLE', 'Caractères spéciaux');
define('POLICY_MIN_SPECIAL_CHARS_DESC', 'Combien de caractères spéciaux doit contenir le mot de passe?');

define('SHOW_SHIPPING_EXCL_TITLE', 'Frais de transport excl.');
define('SHOW_SHIPPING_EXCL_DESC', 'Affichage les frais de transport excl. ou incl.');

define('ACCOUNT_TELEPHONE_OPTIONAL_TITLE', 'Numéro de téléphone optional');
define('ACCOUNT_TELEPHONE_OPTIONAL_DESC', 'Demander le numéro de téléphone en option?');

define('TRACKING_GOOGLEANALYTICS_UNIVERSAL_TITLE' , 'Google Universal Analytics');
define('TRACKING_GOOGLEANALYTICS_UNIVERSAL_DESC' , 'Utiliser le code Google Universal Analytics? <br/><br/><b>Attention:</b>Dès que vous réglez le nouveau code Google Universal Analytics dans votre compte Google Analytics, vous ne pouvez plus utiliser Google Analytics ! !<br/><b>Attention:</b> Cela ne fonctionne que avec un modèle compatible avec la version de boutique à partir de 2.0.0.0 !');
define('TRACKING_GOOGLEANALYTICS_DOMAIN_TITLE' , 'URL de boutique de Google Universal Analytics');
define('TRACKING_GOOGLEANALYTICS_DOMAIN_DESC' , 'Entrez l\'URL de boutique standard ici (example.com ou www.example.com). Fonctionne seulement pour Google Universal Analytics.');
define('TRACKING_GOOGLE_LINKID_TITLE' , 'LinkID Google Universal Analytics');
define('TRACKING_GOOGLE_LINKID_DESC' , 'Vous pouvez voir des informations séparés vers plusieurs liens avec le même objectif sur une page. Si p.ex. deux liens qui mènent vers la page contact apparaissent sur la même page, vous voyez des informations de clic pour chaque lien. Fonctionne seulement pour Google Universal Analytics.');
define('TRACKING_GOOGLE_DISPLAY_TITLE' , 'Fonctionnalité écran de Google Universal Analytics');
define('TRACKING_GOOGLE_DISPLAY_DESC' , 'Les espaces concernant les critères démographiques et les intéresses contiennent un aperçu ainsi que de nouveaux rapports sur l’efficacité selon âge, sexe, catégories d’intéresses. Fonctionne seulement pour Google Universal Analytics. ');
define('TRACKING_GOOGLE_ECOMMERCE_TITLE' , 'Suivi de l\' E-Commerce de Google');
define('TRACKING_GOOGLE_ECOMMERCE_DESC' , 'Appliquez le suivi de l‘ E-Commerce de Google afin de découvrir ce qu’achètent les visiteurs sur votre site web ou votre application. En plus vous obtenez les informations suivants : :<br><br><strong>Produits:</strong> Produits achetés ainsi que les quantités et les chiffres d’affaires générés avec<br><strong>Transactions:</strong> Informations concernant les chiffres d‘affaires, les impôts, les frais de transport et les quantités pour chaque transaction<br><strong>Temps jusqu’à l’achat:</strong> Nombre de jours et visites, de la campagne actuel à la conclusion de la transaction.'
);

define('NEW_ATTRIBUTES_STYLING_TITLE', 'Attribut gestion de Styling');
define('NEW_ATTRIBUTES_STYLING_DESC', 'Activer le Styling dans la gestions des attributs dans les cases à cocher ou les fenêtres déroulantes ? Régler beaucoup de’attributs et problèmes de performance sur Non/false.');

define('DB_CACHE_TYPE_TITLE', 'Moteur de la mémoire cache');
define('DB_CACHE_TYPE_DESC', 'Sélectionnez un des moteurs de la mémoire de cache disponible');

define('META_PRODUCTS_KEYWORDS_LENGTH_TITLE', 'Longueur des termes supplémentaires pour la recherche');
define('META_PRODUCTS_KEYWORDS_LENGTH_DESC', 'Longueur maximale des termes supplémentaires pour la recherche (lettres)');
define('META_KEYWORDS_LENGTH_TITLE', 'Longueur des Meta-Keywords');
define('META_KEYWORDS_LENGTH_DESC', 'Longueur maximale des Meta-Keywords (lettres)');
define('META_TITLE_LENGTH_TITLE', 'Longueur Meta-Title');
define('META_TITLE_LENGTH_DESC', 'Longueur maximale des titres (lettres)');
define('META_CAT_SHOP_TITLE_TITLE', 'Catégories du titre de la boutique');
define('META_CAT_SHOP_TITLE_DESC', 'Ajouter le titre de la boutique aux catégories?');
define('META_PROD_SHOP_TITLE_TITLE', 'Produits du titre de la boutique');
define('META_PROD_SHOP_TITLE_DESC', 'Ajouter le titre de la boutique aux produits?');
define('META_CONTENT_SHOP_TITLE_TITLE', 'Contenus du titre de la boutique');
define('META_CONTENT_SHOP_TITLE_DESC', 'Ajouter le titre de la boutique au contenus?');
define('META_SPECIALS_SHOP_TITLE_TITLE', 'Offres spéciales du titre de la boutique');
define('META_SPECIALS_SHOP_TITLE_DESC', 'Ajouter le titre de la boutique aux offres spéciales?');
define('META_NEWS_SHOP_TITLE_TITLE', 'Nouveaux produits du titre de la boutique');
define('META_NEWS_SHOP_TITLE_DESC', 'Ajouter le titre de la boutique aux nouveaux produits?');
define('META_SEARCH_SHOP_TITLE_TITLE', 'Recherche titre de la boutique');
define('META_SEARCH_SHOP_TITLE_DESC', 'Ajouter le titre de la boutique aux résultats de recherche?');
define('META_OTHER_SHOP_TITLE_TITLE', 'Titre de la boutique autres pages');
define('META_OTHER_SHOP_TITLE_DESC', 'Ajouter le titre de la boutique aux autres pages?');
define('META_GOOGLE_VERIFICATION_KEY_TITLE', 'Google Verification Key');
define('META_GOOGLE_VERIFICATION_KEY_DESC', '<meta name="verify-v1">');
define('META_BING_VERIFICATION_KEY_TITLE', 'Bing Verification Key');
define('META_BING_VERIFICATION_KEY_DESC', '<meta name="msvalidate.01">');

define('TRACKING_FACEBOOK_ACTIVE_TITLE', 'Activer suivi de Conversion de Facebook');
define('TRACKING_FACEBOOK_ACTIVE_DESC', 'Après l‘activation de cette option, tous achats sont transmises à Facebook et peuvent être évalués. Pour cela la création d’un compte est nécessaire sur <a href="https://www.facebook.com" target="_blank"><b>Facebook</b></a>.<br/><b>Attention:</b> Cela ne fonctionne que avec un modèle compatible avec la version de la boutique 2.0.0.0 (et plus récente)!');
define('TRACKING_FACEBOOK_ID_TITLE', 'ID de Conversion Facebook');
define('TRACKING_FACEBOOK_ID_DESC', 'Votre ID de Conversion Facebook');

define('NEW_SELECT_CHECKBOX_TITLE', 'Espace d\'administrateur Styling');
define('NEW_SELECT_CHECKBOX_DESC', 'Activer le styling pour les cases à chocher/ les fenêtres déroulantes dans l\'espace d\'administrateur?');
define('CSRF_TOKEN_SYSTEM_TITLE', 'Système d\'administrateurs des Token');
define('CSRF_TOKEN_SYSTEM_DESC', 'Utiliser le système Token dans l\'administrateur? <br/><b>Attention:</b> Le système des Token sert à l\'augmentation de la sécurité.' );

define('DISPLAY_FILTER_INDEX_TITLE', 'Filtres affichages par page - articles');
define('DISPLAY_FILTER_INDEX_DESC', 'Veuillez entrer une valeur possible pour la sélection, séparé par une virgule. Entrez all pour tous les articles. <br/>Ex.: 3,12,27,all');
define('DISPLAY_FILTER_SPECIALS_TITLE', 'Filtre affichages par page - offres spéciales');
define('DISPLAY_FILTER_SPECIALS_DESC', 'Veuillez entrer une valeur possible pour la sélection, séparée par une virgule. Entrez all pour tous les articles. <br/>Ex.: 3,12,27,all');
define('DISPLAY_FILTER_PRODUCTS_NEW_TITLE', 'Filtre affichages par page - nouveaux articles');
define('DISPLAY_FILTER_PRODUCTS_NEW_DESC', 'Veuillez entrer une valeur possible pour la sélection, séparée par une virgule. Entrez all pour tous les articles. <br/>Ex.: 3,12,27,all');
define('DISPLAY_FILTER_ADVANCED_SEARCH_RESULT_TITLE', 'Filtre affichages par page - résultats de recherche');
define('DISPLAY_FILTER_ADVANCED_SEARCH_RESULT_DESC', 'Veuillez entrer une valeur possible pour la sélection, séparée par une virgule. Entrez all pour tous les articles. <br/>Ex.: 3,12,27,all');

define('USE_BROWSER_LANGUAGE_TITLE' , 'Changer automatiquement vers la langue du navigateur');
define('USE_BROWSER_LANGUAGE_DESC' , 'Changer automatiquement vers la langue du navigateur du client.');

define('WYSIWYG_SKIN_TITLE' , 'Éditeur WYSIWYG de skin');
define('WYSIWYG_SKIN_DESC' , 'Sélectionnez le skin pour l\'éditeur WYSIWYG.');

define('CHECK_CHEAPEST_SHIPPING_MODUL_TITLE', 'Présélectionnez le mode de livraison le moins cher');
define('CHECK_CHEAPEST_SHIPPING_MODUL_DESC', 'Présélectionner le mode de livraison le moins cher pour le client dans checkout?');

define('DISPLAY_PRIVACY_CHECK_TITLE', 'Afficher case à cocher de la vie privée');
define('DISPLAY_PRIVACY_CHECK_DESC', 'Afficher la case à cocher de la vie privée pendant la création de compte? (obligatoire pour des transactions B2C!)');

define('SHOW_SELFPICKUP_FREE_TITLE', 'Module d\'expédition "enlèvement par vos soins" pour "sans frais d\'expédition"');
define('SHOW_SELFPICKUP_FREE_DESC', 'Afficher le module d\'expédition "enlèvement par vos soins" (selfpickup) une fois le module "frais d\'expédition (ot_shipping)"');

define('CHECK_FIRST_PAYMENT_MODUL_TITLE', 'Afficher le module d\'expédition "enlèvement par vos soins" (selfpickup) une fois le montant fixé pour "sans frais d’expédition" est atteint  dans le module "frais d\'expédition (ot_shipping)" ?'
);
define('CHECK_FIRST_PAYMENT_MODUL_DESC', 'Présélectionner la première option de paiement pour le client pendant le checkout?');

define('ATTRIBUTES_VALID_CHECK_TITLE', 'Valider les attributs');
define('ATTRIBUTES_VALID_CHECK_DESC', 'Vérifier la validités des attributs des produits dans le panier du client. <br/>((Cela peut arriver si un client se connecte à la boutique après un long temps et  veut acheter un article qui est resté dans le panier de la dernière commande.) <br/><b>Remarque:</b> Il faut désactiver cette case pour des additifs qui agrandissent les attributs a posteriori, p.ex. le champ de texte.');

define('ATTRIBUTE_MODEL_DELIMITER_TITLE', 'Signe de séparation du numéro d\'article/d\'attribut');
define('ATTRIBUTE_MODEL_DELIMITER_DESC', 'Signe de séparation entre numéro d\'article et numéro d\'article du attribut');

define('STORE_PAGE_PARSE_TIME_THRESHOLD_TITLE' , 'Seuil de tolérance pour la sauvegarde du temps de calcul de la construction de page');
define('STORE_PAGE_PARSE_TIME_THRESHOLD_DESC' , 'Définit le seuil de tolérence en secondes, à partir duquel il faut saisir le temps de calcul de la construction de page.');

define('SEARCH_IN_FILTER_TITLE', 'Recherche dans caractéristiques de l\'article');
define('SEARCH_IN_FILTER_DESC', 'Activer afin de rendre possible la recherche dans les caractéristiques de l\'article');
define('SEARCH_AC_STATUS_TITLE', 'recherche autocomplete');
define('SEARCH_AC_STATUS_DESC', 'Activer afin d\'activer la recherche autocomplete <br/><b>Attention:</b> Cela ne fonctionne que avec un modèle compatible avec la version de la boutique 2.0.0.0 (et plus récente)!');
define('SEARCH_AC_MIN_LENGTH_TITLE', 'Recerhce autocomplete nombre de caractères');
define('SEARCH_AC_MIN_LENGTH_DESC', 'À partir de quel nombre de caractères souhaitez vous afficher les premiers résltats de recherche? <br/><b>Attention:</b> Cela ne fonctionne que avec un modèle compatible avec la version de la boutique 2.0.0.0 (et plus récente)!');

define('DISPLAY_REVOCATION_VIRTUAL_ON_CHECKOUT_TITLE', 'Affichage droit de révocation des téléchargements');
define('DISPLAY_REVOCATION_VIRTUAL_ON_CHECKOUT_DESC', 'Afficher une case à cocher pendant le checkout  qui indique que le droit de révocation cesse?');
define('ORDER_STATUSES_DISPLAY_DEFAULT_TITLE', 'Affichage commandes');
define('ORDER_STATUSES_DISPLAY_DEFAULT_DESC', 'Affichage par défaut des commandes avec quel statut?');

define('INVOICE_INFOS_TITLE', 'Données de facturation');
define('INVOICE_INFOS_DESC', 'Sélectionnez une page de contenu. Le contenu sera affiché sur l\'impression de factures.');

define('CATEGORIES_SHOW_PRODUCTS_SUBCATS_TITLE', 'Afficher des articles des sous-catégories');
define('CATEGORIES_SHOW_PRODUCTS_SUBCATS_DESC', 'Afficher tous les articles des sous-catégories affichées dans le listing?');

define('SEO_URL_MOD_CLASS_TITLE', 'Module URL');
define('SEO_URL_MOD_CLASS_DESC', 'Sélectionnez un module URL.');

define('MODULE_BANNER_MANAGER_STATUS_TITLE', 'Bannière manager');
define('MODULE_BANNER_MANAGER_STATUS_DESC', 'Activer la bannière manager?');

define('MODULE_NEWSLETTER_STATUS_TITLE', 'Newsletter');
define('MODULE_NEWSLETTER_STATUS_DESC', 'Activer le système de Newsletter?');

define('GOOGLE_CERTIFIED_SHOPS_MERCHANT_ACTIVE_TITLE', 'Activer Google Certified Shops Merchant ');
define('GOOGLE_CERTIFIED_SHOPS_MERCHANT_ACTIVE_DESC', 'Utiliser le Google Certified Shops Merchant? <br/><br/><b>Attention:</b>Cela ne fonctionne que avec un modèle compatible avec la version de la boutique 2.0.0.0 (et plus récente)!');
define('GOOGLE_SHOPPING_ID_TITLE', 'ID Google Shooping');
define('GOOGLE_SHOPPING_ID_DESC', 'Votre ID Google Shopping');
define('GOOGLE_TRUSTED_ID_TITLE', 'ID Google Trusted');
define('GOOGLE_TRUSTED_ID_DESC', 'Votre ID Google Trusted');

define('EMAIL_ARCHIVE_ADDRESS_TITLE', 'Archive - adresses email');
define('EMAIL_ARCHIVE_ADDRESS_DESC', 'Veuillez entrer une adresse mail pour l’archivage des mails sortants. Ainsi les mails vous seront envoyés par BCC.');

define('DISPLAY_HEADQUARTER_ON_CHECKOUT_TITLE', 'Siège social dans Checkout');
define('DISPLAY_HEADQUARTER_ON_CHECKOUT_DESC', 'Afficher le siège social dans le checkout?');

//BOC new lang constants for guset status ids (group 17 (= Zusatzmodule)), noRiddle
define('GUEST_STATUS_IDS_TITLE', 'ID du statut invité');
define('GUEST_STATUS_IDS_DESC', 'Entrer tous les ID des groupes de clients invités séparé par virgule.');
//EOC new lang constants for guset status ids (group 17 (= Zusatzmodule)), noRiddle
?>

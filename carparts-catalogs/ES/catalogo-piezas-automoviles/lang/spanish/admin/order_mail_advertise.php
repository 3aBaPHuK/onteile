<?php
define('BOX_ORDER_MAIL_ADVERTISE', 'Publicidad en la confirmación de pedido');
define('TEXT_DISPLAY_NUMBER_OF_ORDER_MAIL_ADVERTISE', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> Wtextos publicitarios)');

define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_ID', 'ID');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_TITLE', 'Título de la publicidad');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_TEXT', 'Texto publicitario');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_STATUS', 'Estado');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_INFO_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_INFO_INSERT_ORDER_MAIL_ADVERTISE_INTRO', 'Por favor, introduzca un nuevo texto publicitario.');
define('TEXT_INFO_DELETE_ORDER_MAIL_ADVERTISE_INTRO', '¿Está seguro de que quiere borrar este texto publicitario?');
define('TEXT_INFO_HEADING_NEW_ORDER_MAIL_ADVERTISE', 'Nuevo texto publicitario');
define('TEXT_INFO_HEADING_EDIT_ORDER_MAIL_ADVERTISE', 'Editar texto publicitario');
define('TEXT_INFO_HEADING_DELETE_ORDER_MAIL_ADVERTISE', 'Borrar texto publicitario');
?>

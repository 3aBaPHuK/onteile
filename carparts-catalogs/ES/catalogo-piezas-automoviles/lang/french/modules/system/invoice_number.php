<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_INVOICE_NUMBER_STATUS_TITLE', 'Statut');
define('MODULE_INVOICE_NUMBER_STATUS_DESC', 'Statut de module');
define('MODULE_INVOICE_NUMBER_STATUS_INFO', 'Actif');
define('MODULE_INVOICE_NUMBER_TEXT_TITLE', 'Module Nouveau numéro de facture');
define('MODULE_INVOICE_NUMBER_TEXT_BTN', 'Éditer');
define('MODULE_INVOICE_NUMBER_TEXT_DESCRIPTION', 'Module Nouveau numéro de facture');
define('MODULE_INVOICE_NUMBER_IBN_BILLNR_TITLE', 'Numéro de facture suivant');
define('MODULE_INVOICE_NUMBER_IBN_BILLNR_DESC', 'Avec l\'attribution d\'une commande ce numéro sera attribué ensuite.');
define('MODULE_INVOICE_NUMBER_IBN_BILLNR_FORMAT_TITLE', 'Format numéro de facture');
define('MODULE_INVOICE_NUMBER_IBN_BILLNR_FORMAT_DESC', 'Schema de construction Numéro de facture : {n}=numéro séquentiel, {d}=jour, {m}=mois, {y}=année, <br>p.ex. "100{n}-{d}-{m}-{y}" donne "10099-28-02-2007"');

?>

<?php
/*************************************************************
* file: order_status_comments.php
* use: language constants for order_status_comments
* (c) noRiddle 12-2016
*************************************************************/

define('HEADING_TITLE', 'Order status comments texts');

define('TEXT_SORT_ASC', 'ascending');
define('TEXT_SORT_DESC', 'descending');

define('TABLE_HEADING_ORDER_STATUS_COMMENTS_ID', 'ID');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_TITLE', 'Comment title');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_TEXT', 'Comment text');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_SORT_ORDER', 'Sort order');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_STATUS', 'Status');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_ACTION', 'Action');

define('TEXT_ORDER_STATUS_COMMENT', 'Comment boilerplate %d');
define('TEXT_INFO_DELETE_COMMENT_INTRO', 'Are you sure you want to delete this boilerplate ?');
define('TEXT_DELETE_COMMENT', 'Delete this boilerplate ?');
define('TEXT_INFO_HEADING_DELETE_ORDER_STATUS_COMMENT', 'Delte this boilerplate');
define('TEXT_DATE_ADDED_ORDER_STATUS_COMMENT', 'Added the:');
define('TEXT_LAST_MODIFIED_ORDER_STATUS_COMMENT', 'Last change the:');

define('TEXT_ORDER_STATUS_COMMENTS_STATUS', 'Status:');
define('TEXT_ORDER_STATUS_COMMENTS_SORT_ORDER', 'Sort order:');
define('TEXT_ORDER_STATUS_COMMENTS_TITLE', 'Title of comment boilerplate:');
define('TEXT_ORDER_STATUS_COMMENTS_TEXT', 'Text of boilerplate:');

define('TEXT_DISPLAY_NUMBER_OF_ORDER_STYTUS_COMMENTS', 'Shown are <b>%d</b> to <b>%d</b> (of total <b>%d</b> comment boilerplates)');
?>
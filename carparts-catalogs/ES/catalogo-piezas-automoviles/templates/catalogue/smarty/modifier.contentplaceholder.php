<?php
/*************************************************
* file: modifier.contentplaceholder.php
* path: /templates/YOUR_TEMPLATE/smarty
* use: replace placeholders in contents
*
* (c) copyright 03-2021, noRiddle
*
* test query:
* SELECT ph_in_contents_type, ph_in_contents_placeholder, ph_in_contents_value FROM nr_placeholder_in_contents WHERE ph_in_contents_placeholder IN('[[LINK_KONTAKT]]', '[[TEXT_SHOP_ADRESSE]]');
* KEY on ph_in_contents_placeholder is used
*************************************************/

require_once(DIR_FS_INC.'contentplaceholder.inc.php');

function smarty_modifier_contentplaceholder ($html) {    
    $html = contentplaceholder_inc($html);

    return $html;
}
?>
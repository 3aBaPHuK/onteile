<?php
/* *******************************************************************************
* ah_module_export.php, noRiddle 07-2017
**********************************************************************************/

define('HEADING_TITLE_MODULES_AH_EXPORT', 'Customer- /orders-export');

define('TABLE_HEADING_AH_MODULES', 'Module');
define('TABLE_HEADING_AH_SORT_ORDER', 'Sort order');
define('TABLE_HEADING_AH_STATUS', 'Status');
define('TABLE_HEADING_AH_ACTION', 'Actiom');

define('TEXT_MODULE_AH_DIRECTORY', 'Module directory:');
define('TABLE_HEADING_AH_FILENAME', 'Module name (for internal use)');
define('ERROR_AH_EXPORT_FOLDER_NOT_WRITEABLE', '%s directory not writeable!');
define('TEXT_AH_MODULE_INFO', 'Please verify the modules being up-to-date!');

define('TABLE_HEADING_AH_MODULES_INSTALLED', 'The following modules have been installed');
define('TABLE_HEADING_AH_MODULES_NOT_INSTALLED', 'The following modules are available for installation');
?>
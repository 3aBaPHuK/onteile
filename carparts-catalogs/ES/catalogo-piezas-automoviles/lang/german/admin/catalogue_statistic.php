<?php
/***************************************************
* file: catalogue_statistic.php
* use: language file
*      
* (c) noRiddle 08-2018
***************************************************/

define('SC_PLEASE_CHOOSE_TXT', 'Bitte wählen');
define('SC_SEARCH_ONLY_VIN_TXT', 'Nur Suchen nach VIN anzeigen');
define('SC_SEARCH_ONLY_WIZARD_TXT', 'Nur Suchen mit Parametern anzeigen');
define('SC_SEARCH_VIN_FOUND_TXT', 'Nur gefundene VINs anzeigen');
define('SC_SEARCH_VIN_NOT_FOUND_TXT', 'Nur nicht gefunde VINs anzeigen');

define('SC_TABLE_HEADING_CUST_ID', 'customers_id');
define('SC_TABLE_HEADING_CUST_IP', 'customer IP');
define('SC_TABLE_HEADING_DATE_ADDED', 'date_added');
define('SC_TABLE_HEADING_BRAND', 'Marke');
define('SC_TABLE_HEADING_VINORWIZ', 'Suche VIN oder Parameter');
define('SC_TABLE_HEADING_VIN', 'VIN');
define('SC_TABLE_HEADING_VIN_FOUND', 'VIN gefunden ?');

define('SC_TEXT_SORT_ASC', 'aufsteigend sortieren');
define('SC_TEXT_SORT_DESC', 'absteigend sortieren');

define('SC_VARIOUS_FILTERS_TXT', 'Diverse Filter');
define('SC_FILTER_VINORWIZ_TXT', 'Filter Suche nach:');
define('SC_FILTER_BRAND_TXT', 'Filter Marke:');
define('SC_FILTER_CUSTMAIL_TXT', 'Filter Kunde (Mail-Adresse):');
define('SC_SHOW_BOUGHT_VINS', 'Gekaufte Kredite anzeigen: ');
define('SC_FILTER_FROM_DATE_TXT', 'von Datum (einschl.):');
define('SC_FILTER_TO_DATE_TXT', 'bis Datum (einschl.):');
define('SC_BOUGHT_TEXT', ' | gekauft: ');

define('SC_TAB_HEADING_STATISTIC', 'Statistik');
define('SC_TAB_HEADING_TABLE', 'Tabelle');
define('SC_CLICK_ON_FOR_STATS_TXT', 'Wenn bereits Daten per Filter oben geladen sind auf den Tab "Statistik" klicken um die Statistik zu laden.<br />Wenn im Tab Tabelle keine Daten erscheinen Filter oben anstossen (z.B. durch Klick auf "Go")');
define('SC_GRAPH_HEADING_SEARCHBY', 'Suchen %s %s bis %s');
define('SC_GRAPH_HEADING_BRAND_RELATIONS', '%s Suchen nach Marken %s bis %s');

define('SC_TEXT_DISPLAY_HOW_MANY_OF_HO_MANY', 'Angezeigt werden <b>%d</b> bis <b>%d</b> (von insgesamt <b>%d</b> Suchen)');
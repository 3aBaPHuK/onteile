<?php
/* -----------------------------------------------------------------------------------------
   $Id: shipcloud.php 2011-11-24 modified-shop $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('HEADING_SHIPCLOUD_PICKUP', 'shipcloud - récupération');

define('TABLE_HEADING_EDIT', 'Tous');
define('TABLE_HEADING_ORDERS_ID', 'Numéro de commande');
define('TABLE_HEADING_TRACKING_ID', 'Numéro de suivi');
define('TABLE_HEADING_DATE_ADDED', 'Date');

define('TEXT_SC_EARLIEST', 'au plus tôt');
define('TEXT_SC_LATEST', 'au plus tard');

define('BUTTON_PICKUP', 'Demande de récupération');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(banktransfer.php,v 1.9 2003/02/18 19:22:15); www.oscommerce.com
   (c) 2003	 nextcommerce (banktransfer.php,v 1.5 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   OSC German Banktransfer v0.85a       	Autor:	Dominik Guder <osc@guder.org>

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
define('MODULE_PAYMENT_TYPE_PERMISSION', 'bt');

define('MODULE_PAYMENT_BANKTRANSFER_TEXT_TITLE', 'Débit direct');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_DESCRIPTION', 'Débit direct');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_INFO', '');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK', 'Prélèvement automatique');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_EMAIL_FOOTER', 'Remarque : Vous pouvez télécharger notre formulaire de faxe sous ' . HTTP_SERVER . DIR_WS_CATALOG . MODULE_PAYMENT_BANKTRANSFER_URL_NOTE . ', le remplir et nous le renvoyer.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_INFO', ((MODULE_PAYMENT_BANKTRANSFER_IBAN_ONLY != 'True') ? 'Attention ! Le débit direct sans indiquer les codes IBAN/BIC est possible <b>uniquement</b> avec un <b>compte bancaire allemand</b>. En indiquant vos codes IBAN/BIC, vous pouvez utiliser le débit direct <b>dans toute l\'UE</b>.<br/>' : '') . 'Champs avec (*) sont des données obligatoires. Avec un code IBAN allemand, l\'indication du BIC est facultatif.<br/><br/>');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_OWNER', 'Propriétaire du compte :*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_OWNER_EMAIL', 'Adresse mail propriétaire du compte :*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_NUMBER', 'Numéro du compte / IBAN :*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_IBAN', 'IBAN :*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_BLZ', 'Code bancaire / BIC :*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_BIC', 'BIC :*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_NAME', 'Établissement bancaire :');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_FAX', 'Autorisation de prélèvement sera confirmée par fax');

// Note these MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_X texts appear also in the URL, so no html-entities are allowed here
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR', 'ERREUR :');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_1', 'Numéro de compte ou code bancaire incorrects, veuillez corriger votre saisie.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_2', 'Ce numéro de compte n\'est pas vérifiable, veuillez contrôler votre saisie.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_3', 'Ce numéro de compte n\'est pas vérifiable, veuillez contrôler votre saisie.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_4', 'Ce numéro de compte n\'est pas vérifiable, veuillez contrôler votre saisie.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_5', 'Ce code bancaire n\'existe pas, veuillez corriger votre saisie.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_8', 'Vous n\'avez pas saisi de code bancaire correct.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_9', 'Vous n\'avez pas saisi de numéro de compte correct.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_10', 'Vous n\'avez pas indiqué de propriétaire du compte.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_11', 'Vous n\'avez pas indiqué de BIC correct.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_12', 'Vous n\'avez pas indiqué d\'IBAN correct.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_13', 'Adresse mail invalide pour contacter propriétaire du compte.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_14', 'Débit direct pas possible avec ce pays.');

define('MODULE_PAYMENT_BANKTRANSFER_TEXT_NOTE', 'Remarque :');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_NOTE2', 'Si vous ne souhaitez pas transmettre vos<br />coordonnées bancaire sur Internet, vous pouvez également télécharger notre ');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_NOTE3', 'Formulaire de fax');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_NOTE4', 'le remplir et nous le renvoyer.');

define('JS_BANK_BLZ', '* Saisissez le code bancaire / BIC de votre banque!\n\n');
define('JS_BANK_NAME', '* Saisissez le nom de votre banque!\n\n');
define('JS_BANK_NUMBER', '* Saisissez votre numéro de compte / IBAN !\n\n');
define('JS_BANK_OWNER', '* Saisissez le nom du propriétaire du compte!\n\n');
define('JS_BANK_OWNER_EMAIL', '* Saisissez l\'adresse mail du propriétaire du compte!\n\n');

define('MODULE_PAYMENT_BANKTRANSFER_DATABASE_BLZ_TITLE', 'Utiliser base de données pour code bancaire ?');
define('MODULE_PAYMENT_BANKTRANSFER_DATABASE_BLZ_DESC', 'Souhaitez-vous utiliser la base des données pour le contrôle de plausibilité des codes bancaires ("true") ?<br/>Veillez à l\'état actuel des codes bancaires !<br/><a href="'.xtc_href_link(defined('FILENAME_BLZ_UPDATE')?FILENAME_BLZ_UPDATE:'').'" target="_blank"><strong>Link: --> BLZ UPDATE <-- </strong></a><br/><br/>Si "false" (standard), le fichier fourni blz.csv sera utilisé qui contient éventuellement des entrées erronées!');
define('MODULE_PAYMENT_BANKTRANSFER_URL_NOTE_TITLE', 'URL Fax');
define('MODULE_PAYMENT_BANKTRANSFER_URL_NOTE_DESC', 'Fichier de confirmation fax. Celui-ci doit se trouver dans le dossier du catalogue');
define('MODULE_PAYMENT_BANKTRANSFER_FAX_CONFIRMATION_TITLE', 'Autoriser confirmation fax');
define('MODULE_PAYMENT_BANKTRANSFER_FAX_CONFIRMATION_DESC', 'Souhaitez-vous autoriser une confirmation par fax ?');
define('MODULE_PAYMENT_BANKTRANSFER_SORT_ORDER_TITLE', 'Ordre de triage');
define('MODULE_PAYMENT_BANKTRANSFER_SORT_ORDER_DESC', 'Ordre d\'affichage. Chiffre le plus bas en premier.');
define('MODULE_PAYMENT_BANKTRANSFER_ORDER_STATUS_ID_TITLE', 'Déterminer statut de la commande');
define('MODULE_PAYMENT_BANKTRANSFER_ORDER_STATUS_ID_DESC', 'Mettre statut des commandes effectuées avec ce module');
define('MODULE_PAYMENT_BANKTRANSFER_ZONE_TITLE', 'Zone de paiement');
define('MODULE_PAYMENT_BANKTRANSFER_ZONE_DESC', 'Si un zone est sélectionné, le mode de paiement n\'est valable que pour ce zone-là.');
define('MODULE_PAYMENT_BANKTRANSFER_ALLOWED_TITLE', 'Zones autorisés');
define('MODULE_PAYMENT_BANKTRANSFER_ALLOWED_DESC', 'Saisissez les zones<b>un par un</b>, qui doivent être autorisés pour ce module. (p.ex. AT,DE (si vide, tous les zones seront autorisés))');
define('MODULE_PAYMENT_BANKTRANSFER_STATUS_TITLE', 'Autoriser paiements de transfer de banques');
define('MODULE_PAYMENT_BANKTRANSFER_STATUS_DESC', 'Souhaitez-vous autoriser des paiements de transfer de banques ?');
define('MODULE_PAYMENT_BANKTRANSFER_MIN_ORDER_TITLE', 'Commandes nécessaires');
define('MODULE_PAYMENT_BANKTRANSFER_MIN_ORDER_DESC', 'Nombre minimum de commandes pour activer cette option.');
define('MODULE_PAYMENT_BANKTRANSFER_IBAN_ONLY_TITLE', 'Mode IBAN');
define('MODULE_PAYMENT_BANKTRANSFER_IBAN_ONLY_DESC', 'Souhaitez-vous autoriser uniquement des paiement IBAN ?');

// SEPA
define('MODULE_PAYMENT_BANKTRANSFER_CI_TITLE', 'Numéro d\'identification de créancier');
define('MODULE_PAYMENT_BANKTRANSFER_CI_DESC', 'Saisissez votre numéro d\'identification de créancier');
define('MODULE_PAYMENT_BANKTRANSFER_REFERENCE_PREFIX_TITLE', 'Préfixe pour référence de mandat (facultatif)');
define('MODULE_PAYMENT_BANKTRANSFER_REFERENCE_PREFIX_DESC', 'Saisissez le préfixe pour la référence du mandat');
define('MODULE_PAYMENT_BANKTRANSFER_DUE_DELAY_TITLE', 'Échéance');
define('MODULE_PAYMENT_BANKTRANSFER_DUE_DELAY_DESC', 'Indiquez délai (en jours), dans lequel vous exécuter le débit');

define('MODULE_PAYMENT_BANKTRANSFER_TEXT_EXTENDED_DESCRIPTION', '<strong><font color="red">ATTENTION :</font></strong>Veuillez actualiser les codes bancaires sous "Programmes d\'aide" -> <a href="'.xtc_href_link('blz_update.php').'"><strong>"Actualiser codes bancaires"</strong></a>!');

?>

<?php
/***********************************************
*
*
*
*
***********************************************/
?>
<script>
$(document).ready(function(){
    $('.bxcarousel_bestseller').bxSlider({
        minSlides: 2,
        maxSlides: 8,
        pager: ($(this).children('li').length > 1), /*FIX for only one entry*/
        slideWidth: 124,
        slideMargin: 18
    });
    $('.bxcarousel_slider').bxSlider({
        adaptiveHeight: false,
        mode: 'fade',
        auto: true,
        speed: 2000,
        pause: 6000
    });
});
</script>
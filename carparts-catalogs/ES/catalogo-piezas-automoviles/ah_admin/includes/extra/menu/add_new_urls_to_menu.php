<?php
/***************************************
* file: add_new_urls_to_menu.php
* use: add new urls to admin menu
* (c) noRiddle 01-2017
***************************************/

switch ($_SESSION['language_code']) {
	case 'de':
		define('MENU_NAME_MASTER_ADMIN', 'MASTER ADMIN');
        define('MENU_NAME_IMAGESLIDERS', 'Image Slider');
        define('MENU_NAME_ORDER_STATUS_COMMENTS', 'Kommentar-Vorlagen');
        define('MENU_NAME_GENERATE_SHOPPING_CART_LINK', 'Warenkorbvorgabe');
        define('MENU_NAME_ORDER_MAIL_ADVERTISE', 'Werbung in Bestellbest.');
        define('MENU_NAME_CREATE_SITEMAP', 'Unsere Sitemaps');
        define('MENU_NAME_AH_MODULE_EXPORT', 'Kunden-/Bestellungen-Export');
        define('MENU_NAME_AH_PLACEHOLDER_LINKS_IN_CONTENTS', 'Platzhalter für Contents');
        //define('MENU_NAME_AH_NEW_STATISTICS', 'NEW STATISTICS');
        if (MODULE_PDF_BILL_STATUS == 'True') {
            define('MENU_NAME_PDFBILL','PDFBillNext Konfiguration ');
            define('MENU_NAME_PDFBILL_LANGS','PDFBillNext Texte');
            define('MENU_NAME_PDFBILL_DEL','PDFBillNext Dateien löschen ');
        }
        define('MENU_NAME_AH_CATALOGUE_STATISTIC', 'Katalog Statistik');
		break;
    case 'en':
		define('MENU_NAME_MASTER_ADMIN', 'MASTER ADMIN');
        define('MENU_NAME_IMAGESLIDERS', 'Image Slider');
        define('MENU_NAME_ORDER_STATUS_COMMENTS', 'Comments boilerplates');
        define('MENU_NAME_GENERATE_SHOPPING_CART_LINK', 'Shopping cart presetting');
        define('MENU_NAME_ORDER_MAIL_ADVERTISE', 'Advertis. in order mail');
        define('MENU_NAME_CREATE_SITEMAP', 'Our sitemaps');
        define('MENU_NAME_AH_MODULE_EXPORT', 'Customers-/orders-export');
        define('MENU_NAME_AH_PLACEHOLDER_LINKS_IN_CONTENTS', 'Placeholders for contents');
        //define('MENU_NAME_AH_NEW_STATISTICS', 'NEW STATISTICS');
        if (MODULE_PDF_BILL_STATUS == 'True') {
            define('MENU_NAME_PDFBILL','PDFBillNext Configuration ');
            define('MENU_NAME_PDFBILL_LANGS','PDFBillNext texts');
            define('MENU_NAME_PDFBILL_DEL','PDFBillNext Files Delete ');
        }
        define('MENU_NAME_AH_CATALOGUE_STATISTIC', 'Catalogue statistic');
		break;
	default:
		define('MENU_NAME_MASTER_ADMIN','MASTER ADMIN');
        define('MENU_NAME_IMAGESLIDERS', 'Image Slider');
        define('MENU_NAME_ORDER_STATUS_COMMENTS', 'Kommentar-Vorlagen');
        define('MENU_NAME_GENERATE_SHOPPING_CART_LINK', 'Warenkorbvorgabe');
        define('MENU_NAME_ORDER_MAIL_ADVERTISE', 'Werbung in Bestellbest.');
        define('MENU_NAME_CREATE_SITEMAP', 'Unsere Sitemaps');
        define('MENU_NAME_AH_MODULE_EXPORT', 'Kunden-/Bestellungen-Export');
        define('MENU_NAME_AH_PLACEHOLDER_LINKS_IN_CONTENTS', 'Platzhalter für Contents');
        //define('MENU_NAME_AH_NEW_STATISTICS', 'NEW STATISTICS');
        if (MODULE_PDF_BILL_STATUS == 'True') {
            define('MENU_NAME_PDFBILL','PDFBillNext Konfiguration ');
            define('MENU_NAME_PDFBILL_LANGS','PDFBillNext Texte');
            define('MENU_NAME_PDFBILL_DEL','PDFBillNext Dateien löschen ');
        }
        define('MENU_NAME_AH_CATALOGUE_STATISTIC', 'Catalogue statistic');
		break;
}

$add_contents[BOX_HEADING_TOOLS][] = array(
    'admin_access_name' => 'master_admin',
    'filename' => FILENAME_MASTER_ADMIN,
    'boxname' => MENU_NAME_MASTER_ADMIN,
    'parameters' => '',
	'ssl' => ''
);

$add_contents[BOX_HEADING_TOOLS][] = array(
    'admin_access_name' => 'imagesliders',
    'filename' => FILENAME_IMAGESLIDERS,
    'boxname' => MENU_NAME_IMAGESLIDERS,
    'parameters' => '',
	'ssl' => ''
);

$add_contents[BOX_HEADING_TOOLS][] = array(
    'admin_access_name' => 'order_status_comments',
    'filename' => FILENAME_ORDER_STATUS_COMMENTS,
    'boxname' => MENU_NAME_ORDER_STATUS_COMMENTS,
    'parameters' => '',
	'ssl' => ''
);

$add_contents[BOX_HEADING_TOOLS][] = array(
    'admin_access_name' => 'order_mail_advertise',
    'filename' => FILENAME_ORDER_MAIL_ADVERTISE,
    'boxname' => MENU_NAME_ORDER_MAIL_ADVERTISE,
    'parameters' => '',
	'ssl' => ''
);

$add_contents[BOX_HEADING_TOOLS][] = array(
    'admin_access_name' => 'generate_shoppingcart_link',
    'filename' => FILENAME_GENERATE_SHOPPING_CART_LINK,
    'boxname' => MENU_NAME_GENERATE_SHOPPING_CART_LINK,
    'parameters' => '',
	'ssl' => ''
);

$add_contents[BOX_HEADING_TOOLS][] = array(
    'admin_access_name' => 'placeholder_links_in_contents',
    'filename' => 'placeholder_links_in_contents.php',
    'boxname' => MENU_NAME_AH_PLACEHOLDER_LINKS_IN_CONTENTS,
    'parameters' => '',
	'ssl' => ''
);

$add_contents[BOX_HEADING_MODULES][] = array(
    'admin_access_name' => 'create_sitemap',
    'filename' => '../create_sitemap.php',
    'boxname' => MENU_NAME_CREATE_SITEMAP,
    'parameters' => '',
	'ssl' => ''
);

$add_contents[BOX_HEADING_CUSTOMERS][] = array(
    'admin_access_name' => 'ah_module_export',
    'filename' => 'ah_module_export.php',
    'boxname' => MENU_NAME_AH_MODULE_EXPORT,
    'parameters' => 'set=ah_export',
	'ssl' => ''
);

/*$add_contents[BOX_HEADING_STATISTICS][] = array(
    'admin_access_name' => 'mm_stats_sales_report',
    'filename' => 'mm_stats_sales_report.php',
    'boxname' => MENU_NAME_AH_NEW_STATISTICS,
    'parameters' => '',
	'ssl' => ''
);*/

if (MODULE_PDF_BILL_STATUS == 'True') {
    //BOC select configuration_group_id dynamically, noRiddle
    $pdf_gr_id_qu = xtc_db_query("SELECT configuration_group_id FROM ".TABLE_CONFIGURATION_GROUP." WHERE configuration_group_title = 'PDFBill Configuration'");
    if(xtc_db_num_rows($pdf_gr_id_qu)) {
        $pdf_gr_id_arr = xtc_db_fetch_array($pdf_gr_id_qu);
        $add_contents[BOX_HEADING_CONFIGURATION][] = array( 
            'admin_access_name' => 'configuration',   //Eintrag fuer Adminrechte
            'filename' => 'configuration.php?gID='.$pdf_gr_id_arr['configuration_group_id'],        //Dateiname der neuen Admindatei
            'boxname' => MENU_NAME_PDFBILL,     //Anzeigename im Menue
            'parameter' => '',                  //zusaetzliche Parameter z.B. 'set=export'
            'ssl' => ''                         //SSL oder NONSSL, kein Eintrag = NONSSL
        );
    }
    //EOC select configuration_group_id dybamically, noRiddle
    
    //texts in PDFs
    $add_contents[BOX_HEADING_CONFIGURATION][] = array( 
        'admin_access_name' => 'pdf_bill_languages',   //Eintrag fuer Adminrechte
        'filename' => 'pdf_bill_languages.php',        //Dateiname der neuen Admindatei
        'boxname' => MENU_NAME_PDFBILL_LANGS,     //Anzeigename im Menue
        'parameter' => '',                  //zusaetzliche Parameter z.B. 'set=export'
        'ssl' => ''                         //SSL oder NONSSL, kein Eintrag = NONSSL
    );
    
    //delete invoices and packing slips
    /*
    $add_contents[BOX_HEADING_CONFIGURATION][] = array( 
        'admin_access_name' => 'pdfbill_del',   //Eintrag fuer Adminrechte
        'filename' => 'pdfbill_del.php',        //Dateiname der neuen Admindatei
        'boxname' => MENU_NAME_PDFBILL_DEL,     //Anzeigename im Menue
        'parameter' => '',                  //zusaetzliche Parameter z.B. 'set=export'
        'ssl' => ''                         //SSL oder NONSSL, kein Eintrag = NONSSL
    );
    */
}

$add_contents[BOX_HEADING_STATISTICS][] = array(
    'admin_access_name' => 'catalogue_statistic',
    'filename' => 'catalogue_statistic.php',
    'boxname' => MENU_NAME_AH_CATALOGUE_STATISTIC,
    'parameters' => '',
	'ssl' => ''
);

//echo '<pre>'.print_r($add_contents, true).'</pre>';
?>
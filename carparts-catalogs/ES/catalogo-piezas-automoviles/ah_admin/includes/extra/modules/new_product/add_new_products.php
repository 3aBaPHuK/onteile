<?php
/************************************************
* file: add_new_products.php
* use: add new fields to products editing
* (c) noRiddle 07-2017
************************************************/

// new array for gm_price_status
$products_price_status_array = array(array('id' => '0', 'text' => TEXT_PRODUCTS_PS_NORMAL), array('id' => '1', 'text' => TEXT_PRODUCTS_PS_ONREQUEST), array('id' => '2', 'text' => TEXT_PRODUCTS_PS_NOPURCHASE));

//BOC gm_price_status
?>
<div style="float:left; width:57%; vertical-align:top">
    <table class="tableInput border0">
          <tr>
            <td style="width:260px"><span class="main"><?php echo TEXT_PRODUCTS_GM_PRICE_STATUS; ?></span></td>
            <td><span class="main"><?php echo xtc_draw_pull_down_menu('gm_price_status', $products_price_status_array, $pInfo->gm_price_status); ?></span></td>
          </tr>
    </table>
</div>
<?php
//EOC gm_price_status
//BOC bulk costs
?>
<div style="float:left;width:43%; vertical-align:top">
    <table class="tableInput border0">
          <tr>
            <td style="width:180px; line-height: 35px;"><span class="main"><?php echo TEXT_PRODUCTS_BULK; ?></span></td>
            <td><span class="main"><?php echo xtc_draw_input_field('products_bulk', $pInfo->products_bulk); ?></span></td>
          </tr>
    </table>
</div>
<div style="clear:both;"></div>
<?php
//EOC bulk costs
//BOC dealer price
?>
<div style="width:100%; vertical-align:top">
    <table class="tableInput border0">
        <tr class="prod-new-deal-price">
            <td style="border-top: 1px solid #cccccc;vertical-align:top;line-height:30px; color:#c00;" class="main"><?php echo DEALERS_PRICE; ?></td>
                <?php
                $products_dealer_price = xtc_round($pInfo->products_dealer_price, PRICE_PRECISION);
                ?>
            <td style="border-top: 1px solid #cccccc; vertical-align:top;line-height:30px;" class="main">
                <?php
                echo xtc_draw_input_field('products_dealer_price', $products_dealer_price, 'style="width: 155px"');
                ?>
            </td>
            <td style="border-top: 1px solid #cccccc; white-space: nowrap;vertical-align:top;line-height:30px;" class="main" colspan="2">(netto)</td>
        </tr>
    </table>
</div>
<?php
//EOC dealer price
?>
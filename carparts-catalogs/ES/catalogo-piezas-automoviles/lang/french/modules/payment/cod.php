<?php
/* -----------------------------------------------------------------------------------------
   $Id: cod.php 998 2005-07-07 14:18:20Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.7 2002/04/17); www.oscommerce.com 
   (c) 2003	 nextcommerce (cod.php,v 1.5 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/
define('MODULE_PAYMENT_TYPE_PERMISSION', 'cod');
define('MODULE_PAYMENT_COD_TEXT_TITLE', 'Nom de famille');
define('MODULE_PAYMENT_COD_TEXT_DESCRIPTION', 'Nom de famille');
define('MODULE_PAYMENT_COD_TEXT_INFO', 'Veuillez prendre en considération qu\'il faut payer des frais supplémentaires de 2 euros au facteur sur place.');
define('MODULE_PAYMENT_COD_ZONE_TITLE', 'Zone de paiement');
define('MODULE_PAYMENT_COD_ZONE_DESC', 'Si une zone est choisie, le méthode de paiement n\'est que valable pour cette zone.');
define('MODULE_PAYMENT_COD_ALLOWED_TITLE', 'Zone autorisées');
define('MODULE_PAYMENT_COD_ALLOWED_DESC', 'Indiquez <b>séparément</b> les zones qui sont autoriséess pour ce module.(p.ex. AT, DE (si vide, toutes les zones sont autorisées))');
define('MODULE_PAYMENT_COD_STATUS_TITLE', 'Activer module nom de famille');
define('MODULE_PAYMENT_COD_STATUS_DESC', 'Voulez-vous accepter des paiements par nom de famille?');
define('MODULE_PAYMENT_COD_SORT_ORDER_TITLE', 'Ordre d\'affichage');
define('MODULE_PAYMENT_COD_SORT_ORDER_DESC', 'Ordre d\'affichage. Chiffre le plus petit en premier.');
define('MODULE_PAYMENT_COD_ORDER_STATUS_ID_TITLE', 'Établir le statut de la commande');
define('MODULE_PAYMENT_COD_ORDER_STATUS_ID_DESC', 'Moduler les commandes effectuées avec module vers ce statut');
define('MODULE_PAYMENT_COD_LIMIT_ALLOWED_TITLE', 'Montant maximal');
define('MODULE_PAYMENT_COD_LIMIT_ALLOWED_DESC', 'À partir de quel montant ne plus autoriser le paiement par nom de famille ? <br/> La valeur saisie est comparée au total partiel (subtotal) approximatif <br/> Cela signifie que la valeur pure de la commande est considérée sans des frais d\'envoi et des surtaxes éventuels.');
define('MODULE_PAYMENT_COD_DISPLAY_INFO_TITLE', 'Affichage checkout');
define('MODULE_PAYMENT_COD_DISPLAY_INFO_DESC', 'Afficher une notification pour des frais supplémentaires au checkout?'
);
define('MODULE_PAYMENT_COD_DISPLAY_INFO_TEXT', 'Veuillez prendre en considération qu\'il faut payer des frais supplémentaires de 2 euros au facteur sur place.<br/>');
?>

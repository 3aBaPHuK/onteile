<?php
/* --------------------------------------------------------------
   $Id: start.php 2585 2012-01-03 14:25:49Z dokuman $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2003 nextcommerce (start.php,v 1.1 2003/08/19); www.nextcommerce.org
   (c) 2006 xt:Commerce (start.php 890 2005-04-27); www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('ATTENTION_TITLE', '! ATTENTION !');

// text for Warnings:
if (!defined('APS_INSTALL')) { //DokuMan - use alternative text for TEXT_FILE_WARNING when using APS package installation
define('TEXT_FILE_WARNING_WRITABLE', '<b>AVERTISSEMENT:</b><br />Les fichiers suivants sont inscriptibles par le serveur. Veuillez changer les privilèges d\'accès (permissions) de ce fichier pour des raisons de sécurité. <b>(444)</b> in unix, <b>(read-only)</b> in Win32.');
} else {
define('TEXT_FILE_WARNING_WRITABLE', '<b>AVERTISSEMENT:</b><br />Les fichiers suivants sont inscriptibles par le serveur. Veuillez changer les privilèges d\'accès (permissions) de ce fichier pour des raisons de sécurité.<b>(444)</b> in unix, <b>(read-only)</b> in Win32.<br /> Si l\'installation a été faite à partir d\'un progiciel dun fournisseur, il faut éventuellement ajuster les privilèges d\'accès différement (HostEurope: <b>CHMOD 400</b> oder <b>CHMOD 440</b>)');
}
define('TEXT_FILE_WARNING', '<b>AVERTISSEMENT:</b><br />Les fichiers suivants doivent être inscriptibles par le serveur. Veuillez changer les privilèges d\'accès (permissions) de ces fichier.<b>(777)</b> in unix, <b>(read-write)</b> in Win32.');
define('TEXT_FOLDER_WARNING', '<b>AVERTISSEMENT:</b><br />Les répértoires suivants doivent être inscriptibles par le serveur. Veuillez changer les privilèges d\'accès (permissions) pour ces répértoires.<b>(777)</b> in unix, <b>(read-write)</b> in Win32.');
define('REPORT_GENERATED_FOR', 'Rapport pour:');
define('REPORT_GENERATED_ON', 'Créé le:');
define('FIRST_VISIT_ON', 'Première visite:');
define('HEADING_QUICK_STATS', 'Aperçu');
define('VISITS_TODAY', 'Visites aujourdh\'hui:');
define('UNIQUE_TODAY', 'Visiteurs uniques:');
define('DAILY_AVERAGE', 'Moyenne quotidienne:');
define('TOTAL_VISITS', 'Total des visites:');
define('TOTAL_UNIQUE', 'Total des visiteurs uniques:' );
define('TOP_REFFERER', 'Top référant:');
define('TOP_ENGINE', 'Top moteur de recherche:');
define('DAY_SUMMARY', 'Aperçu de 30 jours:');
define('VERY_LAST_VISITORS', 'Derniers 10 visiteurs:');
define('TODAY_VISITORS', 'Visiteurs d\'aijourd\'hui:');
define('LAST_VISITORS', 'Derniers 100 visiteurs:');
define('ALL_LAST_VISITORS', 'Tous les visiteurs:');
define('DATE_TIME', 'Date / Heure:');
define('IP_ADRESS', 'Adresse IP:');
define('OPERATING_SYSTEM', 'Système d\'exploitation:');
define('REFFERING_HOST', 'Referer en fonction:');
define('ENTRY_PAGE', 'Page d\'entrée:');
define('HOURLY_TRAFFIC_SUMMARY', 'Résumé du traffique toutes les heures:');
define('WEB_BROWSER_SUMMARY', 'Apercu du navigateur web:');
define('OPERATING_SYSTEM_SUMMARY', 'Apercu du système d\'exploitation:');
define('TOP_REFERRERS', 'Top 10 Referer');
define('TOP_HOSTS', 'Top Ten Hosts');
define('LIST_ALL', 'Afficher tous');
define('SEARCH_ENGINE_SUMMARY', 'Apercu des moteurs de recherche');
define('SEARCH_ENGINE_SUMMARY_TEXT', ' ( Les pourcentages se basent sur le total des visites par des moteurs de recherche. )');
define('SEARCH_QUERY_SUMMARY', 'Apercu des questions recherchées');
define('SEARCH_QUERY_SUMMARY_TEXT', ' ) ( Les pourcentages se basent sur le total des questions recherchées qui ont été inscrits. )');
define('REFERRING_URL', 'Referer Ur1');
define('HITS', 'Hits');
define('PERCENTAGE', 'Pourcentage');
define('HOST', 'Host');

// NEU HINZUGEFUEGT 04.12.2008 - Neue Startseite im Admin BOF

// BOF - vr 2010-04-01 -  Added missing definitions, see below
define('HEADING_TITLE', 'Commandes');
// EOF - vr 2010-04-01 -  Added missing definitions
define('HEADING_TITLE_SEARCH', 'Numéro de commande :');
define('HEADING_TITLE_STATUS', 'Statut:');
define('TABLE_HEADING_AFTERBUY', 'Afterbuy');
define('TABLE_HEADING_CUSTOMERS', 'Clients');
define('TABLE_HEADING_ORDER_TOTAL', 'Valeur totale');
define('TABLE_HEADING_DATE_PURCHASED', 'Date de commande');
define('TABLE_HEADING_STATUS', 'Statut');
//define('TABLE_HEADING_ACTION', 'Aktion');
define('TABLE_HEADING_QUANTITY', 'Quantité');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Numéro d\'article');
define('TABLE_HEADING_PRODUCTS', 'Article');
define('TABLE_HEADING_TAX', 'TVA');
define('TABLE_HEADING_TOTAL', 'Somme totale');
define('TABLE_HEADING_DATE_ADDED', 'ajouté le:');
define('ENTRY_CUSTOMER', 'Client');
define('TEXT_DATE_ORDER_CREATED', 'Date de commande:');
define('TEXT_INFO_PAYMENT_METHOD', 'Mode de paiement:');
define('TEXT_VALIDATING', 'Pas validé');
define('TEXT_ALL_ORDERS', 'Toutes les commandes');
define('TEXT_NO_ORDER_HISTORY', 'Pas d\'historique de commande disponible');
define('TEXT_DATE_ORDER_LAST_MODIFIED', 'Dernière modification');

// BOF - Tomcraft - 2009-11-25 - Added missing definitions for /admin/start.php/
define('TOTAL_CUSTOMERS', 'Clients au total');
define('TOTAL_SUBSCRIBERS', 'Abonnements à la Newsletter');
define('TOTAL_PRODUCTS_ACTIVE', 'Articles actifs');
define('TOTAL_PRODUCTS_INACTIVE', 'Articles inactifs');
define('TOTAL_PRODUCTS', 'Articles au total');
define('TOTAL_SPECIALS', 'Promotions');
// EOF - Tomcraft - 2009-11-25 - Added missing definitions for /admin/start.php/
// BOF - Tomcraft - 2009-11-30 - Added missing definitions for /admin/start.php/
define('UNASSIGNED', 'Pas attribué');
define('TURNOVER_TODAY', 'Chiffre d\'affaires aujourd\'hui');
define('TURNOVER_YESTERDAY', 'Chiffre d\'affaires hier');
define('TURNOVER_THIS_MONTH', 'Ce mois');
define('TURNOVER_LAST_MONTH', 'Le mois dernier (tous)');
define('TURNOVER_LAST_MONTH_PAID', 'Le mois dernier (payé)');
define('TOTAL_TURNOVER', 'Chiffre d\'affaires au total');
// EOF - Tomcraft - 2009-11-30 - Added missing definitions for /admin/start.php/

// BOF - vr 2010-04-01 -  Added missing definitions
// main heading
define('HEADING_TITLE', 'Bienvenue à l\'espace administrateur');
// users online
define('TABLE_CAPTION_USERS_ONLINE', 'En ligne');
define('TABLE_CAPTION_USERS_ONLINE_HINT', '***pour voir les infos d\'un utilisateur cliquez sur le nom de l\'utilisateur');
define('TABLE_HEADING_USERS_ONLINE_SINCE', 'En ligne depuis');
define('TABLE_HEADING_USERS_ONLINE_NAME', 'Nom');
define('TABLE_HEADING_USERS_ONLINE_LAST_CLICK', 'Dernier Click');
define('TABLE_HEADING_USERS_ONLINE_INFO', 'Infomations');
define('TABLE_CELL_USERS_ONLINE_INFO', 'plus...');
// new customers
define('TABLE_CAPTION_NEW_CUSTOMERS', 'Clients');
define('TABLE_CAPTION_NEW_CUSTOMERS_COMMENT', '(les derniers 15)');
define('TABLE_HEADING_NEW_CUSTOMERS_LASTNAME', 'Nom');
define('TABLE_HEADING_NEW_CUSTOMERS_FIRSTNAME', 'Prénom');
define('TABLE_HEADING_NEW_CUSTOMERS_REGISTERED', 'inscrit le');
define('TABLE_HEADING_NEW_CUSTOMERS_EDIT', 'éditer');
define('TABLE_HEADING_NEW_CUSTOMERS_ORDERS', 'Commandes');
define('TABLE_CELL_NEW_CUSTOMERS_EDIT', 'éditer...');
define('TABLE_CELL_NEW_CUSTOMERS_DELETE', 'supprimer...');
define('TABLE_CELL_NEW_CUSTOMERS_ORDERS', 'montrer...');
// new orders
define('TABLE_CAPTION_NEW_ORDERS', 'Commandes');
define('TABLE_CAPTION_NEW_ORDERS_COMMENT', '(les derniers 20)');
define('TABLE_HEADING_NEW_ORDERS_ORDER_NUMBER', 'Numéro de commande');
define('TABLE_HEADING_NEW_ORDERS_ORDER_DATE', 'Date de commande');
define('TABLE_HEADING_NEW_ORDERS_CUSTOMERS_NAME', 'Nom de client');
define('TABLE_HEADING_NEW_ORDERS_EDIT', 'éditer');
define('TABLE_HEADING_NEW_ORDERS_DELETE', 'supprimer');
// newsfeed
define('TABLE_CAPTION_NEWSFEED', 'Visitez le');
// birthdays
define('TABLE_CAPTION_BIRTHDAYS', 'Liste d\'anniversaires');
define('TABLE_CELL_BIRTHDAYS_TODAY', 'Clients dont l\'anniversaire est aujourd\'hui');
define('TABLE_CELL_BIRTHDAYS_THIS_MONTH', 'Clients dont l\'anniversaire est ce mois-ci');
// EOF - vr 2010-04-01 -  Added missing definitions
define('HEADING_CAPTION_STATISTIC', 'Statistiques');
// security check

// DB version check
define('ERROR_DB_VERSION_UPDATE', '<strong>AVERTISSEMENT:</strong> Votre DB doit être actualisé, veuillez installer le <a href="'.DIR_WS_CATALOG.'_installer/">Installer</a>');
define('ERROR_DB_VERSION_UPDATE_INFO', 'DB doit être actualisé de version %s à version %s.');

// EMail check
define('ERROR_EMAIL_CHECK', '<strong>AVERTISSEMENT:</strong> Les adresses email suivantes semblent être erronées:');
define('ERROR_EMAIL_CHECK_INFO', '%s: <%s>');

// security check DB FILE permission
define('WARNING_DB_FILE_PRIVILEGES', '<strong>AVERTISSEMENT</strong> Les privilèges FICHIERS sond activés dans la base de données ’'.DB_DATABASE.'‘ pour l\'utilisateur de la boutique ’'.DB_SERVER_USERNAME.'‘ !');

// register_globals check
define('WARNING_REGISTER_GLOBALS', '<strong>AVERTISSEMENT:</strong> Cette fonction est   <strong>DEPRECATED</strong> (dépassée) depuis PHP 5.3.0 et  <strong>RETIREE</strong>depuis PHP 5.4.0. Veuillez contacter votre hébergeur pour désactiver "register_globals".');
?>

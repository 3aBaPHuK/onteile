<?php
/*********************************************************************
* file: generate_shoppingcart_link.php for use in backend of shop
* path: /lang/spanish/admin/
* use: language file for module
* 
* (c) copyright 10-2020, noRiddle
*********************************************************************/

define('GSCL_NO_MODEL_NO_TXT', 'No ha insertado un número de artículo o el artículo no existe en la tienda.');
define('GSCL_ONE_MODEL_NO_EXISTS_TXT', 'Al menos uno de los números ingresados no existe en la tienda.');
define('GSCL_STATUS_WRONG_TXT', 'Los artículos %s o no tienen product_status 1 (=> deactivado) o no gm_price_status 0 (=> Consulta de precio o no disponible). Por favor verifiquelo en la tienda.');
define('GSCL_EXPLANATION_TXT', '<h1>Generar un enlace para la cesta de compra</h1>
                                <ul>
                                    <li>Ingresa el número de artículo y la cantidad y luego haga click en "Generar el enlace"
                                        <ul>
                                            <li>Si se cotizan varios artículos::<br />crea un campo adicional con el butón "Más campos"</li>
                                            <li>Si ha creado demás campos:<br />remueva campos con el butón "Menos campos" </li>
                                         </ul>
                                    </li>
                                    <li>Para verificar un número de artículo en vico haga click dondequiera en el área blanco</li>
                                    <li>Copía el enlace y mandalo al cliente por el sistema de tickets</li>
                                </ul>');
define('GSCL_MODEL_NO_TXT', 'No. de artículo:');
define('GSCL_PCS_TXT', '- | - Cantidad:');
define('GSCL_MORE_INPUT_TXT', '+ Más campos');
define('GSCL_LESS_INPUT_TXT', '- Menos campos');
define('GSCL_GENERATE_LINK_TXT', '&raquo; Generar el enlace');
define('GSCL_DELETE_ALL_ENTRIES_TXT', 'x Borrar todas entradas');
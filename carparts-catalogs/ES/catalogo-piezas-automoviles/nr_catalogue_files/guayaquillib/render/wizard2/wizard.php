<?php
/**********************************************
* file reworked and adadapted by noRiddle
*
* (c) noRiddle 03-2018
**********************************************/

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'template.php';

class GuayaquilWizard extends GuayaquilTemplate
{
	var $wizard = NULL;
	var $catalog = NULL;

	function __construct(IGuayaquilExtender $extender)
	{
		parent::__construct($extender);
	}

	function Draw($catalog, $wizard)
	{
		$this->wizard = $wizard;
		$this->catalog = $catalog;

		$html = '<div class="DrawHeader">';
		$html .= '<script type="text/javascript">';
		$html .= 'function openWizard(ssd) {';
        $html .= 'var url = \''.$this->FormatLink('wizard', null, $catalog).'\'.replace(\'\\$ssd\\$\', ssd);';
        $html .= 'window.location = url;';
        $html .= '}';
		$html .= '</script>';
		$html .= '<form name="findByParameterIdentifocation" id="findByParameterIdentifocation">';
		$html .= '<table border="0" width="100%">';
		
		$html .= $this->DrawHeader();
        //BOC put year select first, (different sorting unfortunately not possible, hence two foreach loops), 05-2019, noRiddle
        /*if(is_object($wizard->row)) {
            foreach($wizard->row as $condition)
                $html .= $this->DrawConditionRow($catalog, $condition);
        }*/
        //BOC prevent error  "Invalid argument supplied for foreach() in /usr/www/users/onteile/oem-kataloge/nr_catalogue_files/guayaquillib/render/wizard2/wizard.php on line 32"
        //echo '<pre>'.print_r($wizard, true).'</pre>';

        if(is_object($wizard->row)) {
            //foreach($wizard->row as $condition) {
            for($i = 0, $cwr = count($wizard->row); $i < $cwr; $i++) {
                if($wizard->row[$i]['name'] == 'Year') {
                    $html .= $this->DrawConditionRow($catalog, $wizard->row[$i]);
                    unset($wizard->row[$i]);
                }
            }

            foreach($wizard->row as $condition2) {
                $html .= $this->DrawConditionRow($catalog, $condition2);
            }
        }
        //EOC put year select first, 05-2019, noRiddle
		
		$html .= '</table>';
		$html .= '</form>';

		if ($wizard->row['allowlistvehicles'] == 'true')
			$html .= $this->DrawVehiclesListLink($catalog, $wizard);

		$html .= '</div>';
        //show me, noRiddle
        //$html .= '<pre>'.print_r($wizard->row, true).'</pre>';

		return $html;
	}

	function DrawHeader()
	{
		return '';
	}

	function DrawConditionRow($catalog, $condition)
	{
		//$html = '<tr width="60%"'.($condition['automatic'] == 'false' ? ' class="guayaquil_SelectedRow"' : '').'>';
        $html = '<tr'.($condition['automatic'] == 'false' ? ' class="guayaquil guayaquil_SelectedRow"' : ' class="guayaquil"').'>'; //why width on tr ?, noRiddle
        $html .= '<td width="22%">'.$this->DrawConditionName($catalog, $condition).'</td>'; //set width here, noRiddle
		$html .= '<td width="78%">'; //set width here, noRiddle
		if ($condition['determined'] == 'false') {
			$html .= $this->DrawSelector($catalog, $condition);
        } else if ($condition['automatic'] == 'true') {
            $html .= $this->DrawAutomaticSelector($catalog, $condition);
        } else {
            $html .= $this->DrawManualSelector($catalog, $condition);
        }
		
		$html .= '</td></tr>';

		return $html;
	}

	function DrawConditionName($catalog, $condition)
	{
		
        //BOC overwrite engl. names with our language params, 22-11-2018, noRiddle
        $temp_cond_name = ($condition['name'] !=  $this->GetLocalizedString(mb_strtolower($condition['name'])) ? $this->GetLocalizedString(mb_strtolower($condition['name'])) : $condition['name']);
        $condition['name'] = $temp_cond_name;
        //BOC overwrite engl. names with our language params, 22-11-2018, noRiddle
        return $condition['name'];
	}

	function DrawSelector($catalog, $condition)
	{
		//$html = '<select style="width:250px" name="Select'.$condition['type'].'" onChange="openWizard(this.options[this.selectedIndex].value); return false;">';
        $html = '<select name="Select'.$condition['type'].'" onChange="openWizard(this.options[this.selectedIndex].value); return false;">'; //don't set width here, noRiddle

		$html .= '<option value="null">'.$this->GetLocalizedString('filter_choose_please').'</option>';

		foreach($condition->options->row as $row) {
			$html .= $this->DrawSelectorOption($row);
		}

		$html .= '</select>';

		return $html;
	}

	function DrawAutomaticSelector($catalog, $condition)
	{
		//$html = '<select disabled style="width:250px" name="Select'.$condition['type'].'">';
        $html = '<select disabled="disabled" name="Select'.$condition['type'].'">'; //don't set width here, noRiddle
		$html .= $this->DrawDisabledSelectorOption($condition);
		$html .= '</select>';

		return $html;
	}

	function DrawManualSelector($catalog, $condition)
	{
		//$html = '<select disabled style="width:250px" name="Select'.$condition['type'].'">';
        $html = '<select disabled="disabled" name="Select'.$condition['type'].'">'; //don't set width here, noRiddle
		$html .= $this->DrawDisabledSelectorOption($condition);
		$html .= $this->DrawDisabledSelectorOption($condition);
		$html .= '</select>';
        $removeFile = '<i class="fa fa-times"></i>'; //$this->Convert2uri(__DIR__ . DIRECTORY_SEPARATOR . 'images/remove.png'); //use font icon here, noRiddle
		$html .= ' <a href="'.str_replace('$ssd$', $condition['ssd'], $this->FormatLink('wizard', null, $catalog)).'">'.$removeFile.'</a>'; //<img src="'.$removeFile.'"></a>';  //use font icon here, noRiddle

		return $html;
	}

	function DrawSelectorOption($row)
	{
		//BOC overwrite values for VW Golf, they are unclear, 12-2018, noRiddle
        if(strtolower($_SESSION['nr_ctlg_brands'][$_GET['c']]['lax_brand']) == 'volkswagen') {
            if(strtolower($row['value']) == 'golf') {
                $temp_row_val = 'Golf I 17, Golf II 19, 1G, Golf III 1H &lt;br /&gt;(Market Europe: 1975-1998)';
            } else if(strtolower($row['value']) == 'golf variant') {
                $temp_row_val = 'Golf III Variant 1H &lt;br /&gt;(Market Europe: 1994-1998)';
            } else if(strtolower($row['value']) == 'golf/var.-syn./rall./coun.') {
                $temp_row_val = 'Golf Variant Synchro Rallye Country &lt;br /&gt;(Market Europe: 1986-1998)';
            } else if(strtolower($row['value']) == 'golf/variant/4motion') {
                $temp_row_val = 'Golf / Golf Variant / 4Motion &lt;br /&gt;(Market Europe: 1998-'.date('Y').')';
            } else {
                $temp_row_val = $row['value'];
            }
        } else {
            $temp_row_val = $row['value'];
        }
        
        //return '<option value="'.$row['key'].'">'.$row['value'].'</option>';
        return '<option value="'.$row['key'].'">'.$temp_row_val.'</option>';
        //EOC overwrite values for VW Golf, they are unclear, 12-2018, noRiddle
	}

	function DrawDisabledSelectorOption($condition)
	{
		//BOC overwrite values for VW Golf, they are unclear, 12-2018, noRiddle
        if(strtolower($_SESSION['nr_ctlg_brands'][$_GET['c']]['lax_brand']) == 'volkswagen') {
            if(strtolower($condition['value']) == 'golf') {
                $temp_cond_val = 'Golf I 17, Golf II 19, 1G, Golf III 1H (Market Europe: 1975-1998)';
            } else if(strtolower($condition['value']) == 'golf variant') {
                $temp_cond_val = 'Golf III Variant 1H (Market Europe: 1994-1998)';
            } else if(strtolower($condition['value']) == 'golf/var.-syn./rall./coun.') {
                $temp_cond_val = 'Golf Variant Synchro Rallye Country (Market Europe: 1986-1998)';
            } else if(strtolower($condition['value']) == 'golf/variant/4motion') {
                $temp_cond_val = 'Golf / Golf Variant / 4Motion (Market Europe: 1998-'.date('Y').')';
            } else {
                $temp_cond_val = $condition['value'];
            }
        } else {
            $temp_cond_val = $condition['value'];
        }
        
        //return '<option disabled selected value="null">'.$condition['value'].'</option>';
        return '<option disabled selected value="null">'.$temp_cond_val.'</option>';
        //EOC overwrite values for VW Golf, they are unclear, 12-2018, noRiddle
	}

	function DrawVehiclesListLink($catalog, $wizard)
	{
		//BOC build nice button, noRiddle
        //$html = '<br><a class="gWizardVehicleLink" href="'.$this->FormatLink('vehicles', $wizard, $catalog).'">';
        $html = '<p style="margin-top:15px">';
        $html .= '<a class="gWizardVehicleLink" href="'.$this->FormatLink('vehicles', $wizard, $catalog).'" title="'.$this->GetLocalizedString('List vehicles').'"><span class="cssButton cssButtonColor1"><i class="fa fa-eye"></i><span class="cssButtonText">';
        $html .= $this->GetLocalizedString('List vehicles');
        $html .= '</span></span></a>';
        $html .= '</p>';
        //EOC build nice button, noRiddle

		return $html;
	}
}

?>
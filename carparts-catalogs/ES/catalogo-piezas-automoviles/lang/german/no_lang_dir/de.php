<?php
/*******************************
* german language file
* (c) noRiddle 02-2017
*******************************/
define('NR_NOSCRIPT_TXT', 'Aktivieren Sie Javascript in Ihrem Browser um alle Features dieser Seite nutzen zu können.');
define('NR_BRANDS_ACC_TXT', 'Marken');
define('MORE_LANGS', 'Weitere Sprachen');
define('NR_401_HEAD', 'Bitte nochmal klicken');
define('NR_401_TXT', 'Nach welcher Automarke suchen Sie ?<br />Bitte klicken Sie auf das entsprechende Marken-Icon links oder rechts.');
define('NR_ALT_TAG_LOGO', 'Original Ersatzteile für alle Automarken');
define('NR_TXT_LOOKING_FOR_SHOP_OPERATOR', 'Wir suchen unseren %s-Partner um mit uns diesen Shop zu betreiben !<br /><br />Kennen Sie einen zukunftsorientierten %s-Vertragspartner in Ihrer Umgebung ? Können Sie uns diesen Partner vermitteln ?<br />Wenn es klappt, garantieren wir Ihnen einen kostenlosen Einkauf von 300 Euro im Markenshop Ihrer Wahl !<br /><br />Oder sind Sie vielleicht selber dieser %s-Vertragspartner und wollen Ihren Ersatzteilumsatz ausweiten ?<br /><br />Bitte <a href="mailto:ah@online-teile.com?subject=Interesse%%20an%%20einer%%20Zusammenarbeit%%20mit%%20online-teile.com&amp;body=Sehr%%20geehrter%%20Herr%%20Hoevel%%20!%%0A%%0A%%0AMit%%20freundlichen%%20Grüßen">schreiben&nbsp;</a>Sie mir.');
//<span style="color: rgb(0, 0, 255);"><u><strong>
?>
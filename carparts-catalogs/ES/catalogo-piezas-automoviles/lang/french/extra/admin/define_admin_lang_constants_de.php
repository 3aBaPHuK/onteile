<?php
/*************************************************
* file: define_admin_lang_constants_de.php
* use: define additional lang constants
* (c) noRiddle 12-2016 - 01-2017
*************************************************/

//BOC favorites top menu
define('NR_MASTER_ADMIN_FAV_ALT_TXT', 'MasterAdminTools');
//BOC favorites top menu

//BOC order_status_comments
define('TEXT_ORDER_STATUS_COMMENTS_DROPDOWN', 'Modèles commentaire');
define('TXT_ORDER_STATUS_COMMENTS_CHOOSE', 'veuillez choisir');
//EOC order_status_comments

//BOC edit gm_price_status
define('NR_EDITPROD_PLEASE_CHOOSE', 'Veuillez choisir');
define('NR_EDITPROD_PROD_NOTAVAIL', 'Pièce est supprimé');
define('NR_EDITPROD_PROD_WILL_REPLACED', 'Pièce sera remplacé');
define('NR_EDITPROD_HEADING', 'Article %s sera remplacé ou supprimé?');
define('NR_EDITPROD_HEADING_COMMENT', '(S\'il vous plaît avec prudence, ne peut plus être annulé après.)');
define('NR_EDITPROD_REPL_MODEL_TXT', 'Parce que l\'article sera remplacé, entrez le numéro d\'article de l\'article à remplacer <strong>sans espaces est signes spéciaux</strong> ici');
define('NR_EDITPROD_BUTTON', 'Mettre à jour l\'article');
define('NR_EDITPROD_ALERT_NO_SPECIALCHARS', 'Veuillez entrer le nouveau numéro d\'article (sans espaces et signes spécieaux !!) <br /> ou choisissez "Pièce est supprimé".');
define('NR_EDITPROD_ALERT_REPLPART_NO_EXISTS', 'La pièce mentionnée n\'existe pas dans la boutique. <br/> Veuillez vérifier avec la fonction de recherche dans le frontend de la boutique et éventuellement créer des articles.');
define('NR_EDITPROD_EDIT_SUCCESS', 'OKAY: l\'article avec le numéro d\'article %s a été modifié avec succès.');
define('NR_EDITPRODPRICE_HEADING', 'Article %s: Modifer le UVP ? (Clic pour ouvrir)');
define('NR_EDITPRODPRICE_HEADING_COMMENT', ' (Veuillez entrer le nouveau prix UVP <u>net</> et avec un point comme virgule)');
define('NR_EDITPRODPRICE_TXT', 'Nouveaux UVP');
define('NR_EDITPRODPRICE_EDIT_SUCCESS', 'OKAY: Le prix de l\'article avec le numéro d\'article %s a été modifié avec succès.');
//EOC edit gm_price_status

//BOC new buttons for admin access
define('ADMACC_BUTTON_SET_ALL', 'Mettre tous les droits');
define('ADMACC_BUTTON_UNSET_ALL', 'Prendre tous les droits');
define('ADMACC_BUTTON_SET_SW', 'Droits administrateur pour des magasiniers');
define('ADMACC_BUTTON_SET_TRANSL', 'Droits traducteurs');
define('ADMACC_BUTTON_SET_MSW', 'Droits magasinier principal');
define('ADMACC_BUTTON_SET_SEO', 'Droits agent SEO');
//BOC new buttons for admin access

//BOC gm_price_status, noRiddle
define('TEXT_PRODUCTS_PS_NORMAL', 'normal');
define('TEXT_PRODUCTS_PS_ONREQUEST', 'Prix sur demande');
define('TEXT_PRODUCTS_PS_NOPURCHASE', 'pas livrable');
define('TEXT_PRODUCTS_GM_PRICE_STATUS', 'Statut de prix:');
//EOC gm_price_status, noRiddle

//BOC bulk costs, noRiddle
define('TEXT_PRODUCTS_BULK', 'Frais des objets encombrants:');
//EOC bulk costs, noRiddle

//BOC inclusion for affiliate program, noRiddle
require(DIR_FS_LANGUAGES.'german/admin/'.'affiliate_configuration.php');
require(DIR_FS_LANGUAGES.'german/admin/'.'affiliate_german.php');
//EOC inclusion for affiliate program, noRiddle

//BOC heading for staff who edited order, noRiddle
define('TABLE_HEADING_CUSTOMER_STAFF', 'Collaborateur');
//EOC heading for staff who edited order, noRiddle

//BOC new contant for total shipping weight in order
define('AH_ORDER_TOT_WEIGHT', 'Poids de l\'expédition:');
//EOC new contant for total shipping weight in order

//BOC multi_order_status based on module by XTC-DELUXE.DE, noRiddle
define('TABLE_HEADING_EDIT', 'Edit all');
define('HEADING_MULTI_ORDER_STATUS', 'Pour toutes les commandes marquées:');
define('BUTTON_CLOSE_PRINT_PAGES', 'Fermer toutes les fenêtre de l\'imprimante.');
define('WARNING_ORDER_NOT_UPDATED_ALL', 'Remarque: Quelques commandes n\'ont pas été actualisée.');
define('TEXT_DO_STATUS_CHANGE', 'Actualiser le statut:');
define('TEXT_DO_PRINT_INVOICE', 'Imprimer la facture:');
define('TEXT_DO_PRINT_PACKINGSLIP', 'Imprimer le bon de livraison:');
define('MULTISTATUS_DELETE_EXPLAIN', 'Supprimer?<br />(<span style="font-size:8px;">Il faut cocher toutes les trois cases a cocher <br /> afin d\'éviter une suppression par erreur.</span>)');
//EOC multi_order_status based on module by XTC-DELUXE.DE, noRiddle

//BOC EK Preis
define('DEALERS_PRICE', 'Prix d\'achat');
//EOC EK Preis

//BOC vin search credit, noRiddle
define('TABLE_HEADING_CATALOG_CREDIT', 'Crédit de recherche VIN');
//EOC vin search credit, noRiddle

//BOC PDFBill NEXT
define('BUTTON_INVOICE_PDF', 'Facture PDF');
define('BUTTON_PACKINGSLIP_PDF', 'Bon de livraison PDF');
define('BUTTON_BILL_NR', 'Numéro de facture:');
define('BUTTON_SET_BILL_NR', 'Attribuer numéro de facture');

define('MODULE_PDF_BILL_STATUS_TITLE', 'Module activé?');
define('MODULE_PDF_BILL_STATUS_DESC', '');
define('PDF_BILL_LASTNR_TITLE', 'Dernier numéro de facture');
define('PDF_BILL_LASTNR_DESC', 'Le dernier numéro de facture pour l\'attribution automatique.');
define('PDF_USE_ORDERID_PREFIX_TITLE', 'Préfixe numéro de facture');
define('PDF_USE_ORDERID_PREFIX_DESC', 'Préfixe pour le numéro de facture si le numéro de la commande est utilisé comme numéro de facture.');
define('PDF_USE_ORDERID_TITLE', 'Numéro de commande comme numéro de facture');
define('PDF_USE_ORDERID_DESC', 'L\'option rend possible l\'utiisation du numéro de commande comme numéro de facture.');
define('PDF_STATUS_COMMENT_TITLE', 'Commentaire du statut du PDF de la commande');
define('PDF_STATUS_COMMENT_DESC', 'Commentaire ajouté au système lorsqu\'une facture est envoyé.');
define('PDF_STATUS_COMMENT_SLIP_TITLE', 'Commentaire du statut du PDF du bon de livraison');
define('PDF_STATUS_COMMENT_SLIP_DESC', 'Commentaire ajouté au sytème lorsqu\'un bon de livraison est envoyé.');
define('PDF_FILENAME_SLIP_TITLE', 'Nom de fichier bon de livraison');
define('PDF_FILENAME_SLIP_DESC', 'Nom de fichier du  bon de livraison. Espaces remplacés par tiret bas. Variables: <strong>{oID}</strong>, <strong>{bill}</strong>, <strong>{cID}</strong>. <strong>Sans .pdf</strong>.');
define('PDF_MAIL_SUBJECT_TITLE', 'Objet mail de facturation');
define('PDF_MAIL_SUBJECT_DESC', 'Veuillez entrer l\'objet  pour le mail de facturation. <strong>{oID}</strong> Sert comme texte de substitution pour le numéro de la commande.');
define('PDF_MAIL_SUBJECT_SLIP_TITLE', 'Objet du mail de bon de livraison');
define('PDF_MAIL_SUBJECT_SLIP_DESC', 'Veuillez entrer l\'objet pour le mail du bon de livraison. <strong>{oID}</strong> Sert comme texte de substitution pour le numéro de la commande.');
define('PDF_MAIL_SLIP_COPY_TITLE', 'Bon de livraison - Adresse de transfert');
define('PDF_MAIL_SLIP_COPY_DESC', 'Entrez une adresse mail si vous souhaitez recevoir une copie.');
define('PDF_MAIL_BILL_COPY_TITLE', 'Facture - Adresse de transfert');
define('PDF_MAIL_BILL_COPY_DESC', 'Entrez une adresse mail si vous souhaitez recevoir une copie.');
define('PDF_FILENAME_TITLE', 'Nom de fichier facture');
define('PDF_FILENAME_DESC', 'Nom de fichier de la facture. Des espaces sont substitués par un tiret bas. Variables:  <strong>{oID}</strong>, <strong>{bill}</strong>, <strong>{cID}</strong>. <strong> Sans .pdf</strong>.');
define('PDF_SEND_ORDER_TITLE', 'Envoyer automatiquement le PDF de facture');
define('PDF_SEND_ORDER_DESC', 'Si cette option est activée le PDF de facture sera envoyé automatiquement après la commande.');

define('PDF_USE_CUSTOMER_ID_TITLE', 'Utilise l\'ID client comme numéro de client');
define('PDF_USE_CUSTOMER_ID_DESC', 'L\'ID client sera utilisé comme numéro client. Veuillez régler sur false si un numéro de client sera attribué.');

define('PDF_STATUS_ID_BILL_TITLE', 'ID statut de la commande - PDF de la facture');
define('PDF_STATUS_ID_BILL_DESC', 'Vous trouvez l\'ID du statut de la commande après <strong>oID=</strong> si vous éditez le statut de la commande.');
define('PDF_STATUS_ID_SLIP_TITLE', 'ID du statut de la commande - bon de livraison');
define('PDF_STATUS_ID_SLIP_DESC', 'Vous trouvez l\'ID du statut de la commande après <strong>oID=</strong> si vous éditez le statut de la commande.');

define('PDF_PRODUCT_MODEL_LENGTH_TITLE', 'Nombre maximale des articles');
define('PDF_PRODUCT_MODEL_LENGTH_DESC', 'Nombre de signes après coupure du numéro d\'article. Veuillez prendre en considération que des numéros d\'articles trop longues peuvent détrure la mise en page du PDF.');
define('PDF_UPDATE_STATUS_TITLE', 'Actualiser le statut de la commande');
define('PDF_UPDATE_STATUS_DESC', 'Le statut de la commande sera actualisé automatiquement après l\'envoi du PDF par mail.');
define('PDF_USE_ORDERID_SUFFIX_TITLE', 'Suffixe numéro de facture');
define('PDF_USE_ORDERID_SUFFIX_DESC', 'Suffixe du numéro de facture si le numéro de la commande est utilisé comme numéro de facture.');
define('PDF_STATUS_SEND_TITLE', 'Envoyer la facture si le statut de la commande est modifié');
define('PDF_STATUS_SEND_DESC', 'Envoyer la facture automatiquement si statut de la commande en bas');
define('PDF_STATUS_SEND_ID_TITLE', 'Envoie PDF de la commande si ID du statut de la commande');
define('PDF_STATUS_SEND_ID_DESC', 'La facture sera envoyée en modifiant vers cet ID du statut de la commande. <br />(si "Envoyer la facture si le statut de la commande est modifié" est sur "OUI")');

define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_TITLE', 'Envoyer le bon de livraison uniquement au logisticien/ préparateur de commande');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_DESC', 'Si vous activez cette fonction, le bon de livraison ne sera pas envoyé au client mais à l\'adresse mail indiqué ci-dessous.');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME_TITLE', 'Nom du logisticien');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME_DESC', 'Entrez ici le nom du logisticien qui recoit le bon de livraison . <br /> (Seulement si "Envoyer le bon de livraison uniquement au logisticien/ préparateur de commande" est sur "Oui".)');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL_TITLE', 'Email logisticien');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL_DESC', 'Entrez ici l\'adresse mail du logisticien  qui recoit le bon de livraison. <br />(Seulement si "Envoyer le bon de livraison uniquement au logisticien/ préparateur de commande" est sur "Oui".)' );

define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_TITLE', 'Prochain numéro de facture');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_DESC', '<span style="color:#c00;">Changer avec précaution, le numéro de facture sera attribué continuellement par le système.<br /> Si "Numéro de commande comme numéro de facture" est sur "Oui", la modification n\'a évidemmenent pas d\'effet.</span>');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT_TITLE', 'Format numéro de facture');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT_DESC', 'Schéma de construction de la facture: {n}=numéro séquentiel, {d}=jour, {m}=mois, {y}=année,<br />p.ex. "100{n}-{d}-{m}-{y}" fait "10099-28-02-2007"');
define('PDF_COMMENT_ON_PACKING_SLIP_TITLE', 'Commentaire du client sur le bon de livraison?');
define('PDF_COMMENT_ON_PACKING_SLIP_DESC', 'Imprimer le commentaire que le client a laissé pendant la commande?');

//BOC - Innergemeinschaftliche Lieferungen
define('PDF_BILL_EU_CUSTOMERS_GROUP_ID_TITLE', 'Groupe de clients commerçant UE:');
define('PDF_BILL_EU_CUSTOMERS_GROUP_ID_DESC', '<b>ID du groupe de clients(cID)</b> des commerçants de l\'UE - La remarque sur la exonération fiscale selon § 4 numéro 1 du loi sur la TVA sera ajouté à la facture pour ces clients!<br /><b>Séparer plusieurs entrées par virgule!</b><br />L\'ID du groupe de clients se trouve dans la ligne du navigateur après <strong>cID=</strong> si vous vous éditez le groupe de clients.');
//EOC - Innergemeinschaftliche Lieferungen

define('TABLE_HEADING_BILL_NR', 'Numéro de facturation');
define('PDF_BILL_NR_INFO2', 'Veuillez ouvrir la commande afin de pouvoir la modifier <br /> et entrer le nouveau numéro de facture en bas à la fin!');
//EOC PDFBill NEXT

//BOC yandex heat map, noRiddle
define('ACTIVATE_YANDEX_HEATMAP_TITLE', 'Activate Yandex heat map ?');
define('ACTIVATE_YANDEX_HEATMAP_DESC', 'To use this the heat map must be integrated in <i>/templates/DEIN_TEMPLATE/javascript/general_bottom.js.php</i>.');
//EOC yandex heat map, noRiddle
?>

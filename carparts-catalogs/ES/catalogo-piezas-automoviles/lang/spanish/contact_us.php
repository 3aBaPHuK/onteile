<?php
 //additional fields
define('EMAIL_COMPANY', 'Empresa: ');
define('EMAIL_STREET', 'Calle: ');
define('EMAIL_POSTCODE', 'Código postal: ');
define('EMAIL_CITY', 'Ciudad: ');
define('EMAIL_PHONE', 'Teléfono: ');
define('EMAIL_FAX', 'Fax: ');
define('EMAIL_SENT_BY', 'Transmitido por %s %s el %s a las %s horas');
 //define('EMAIL_NOTIFY', 'ACHTUNG, diese E-Mail kann NICHT mit -ABSENDER ANTWORTEN- beantwortet werden!'); // Not used yet
 
 //default fields
define('EMAIL_NAME', 'Nombre: ');
define('EMAIL_EMAIL', 'E-mail: ');
define('EMAIL_MESSAGE', 'Mensaje: ');
 
 //contact-form error messages
 //BOC nicer error output, noRiddle
 //define('ERROR_EMAIL','<p><b>Ihre E-Mail-Adresse:</b> Keine oder ung&uuml;ltige Eingabe!</p>');
 //define('ERROR_VVCODE','<p><b>Sicherheitscode:</b> Keine &Uuml;bereinstimmung, bitte geben Sie den Sicherheitscode erneut ein!</p>');
 //define('ERROR_MSG_BODY','<p><b>Ihre Nachricht:</b> Keine Eingabe!</p>');	
define('ERROR_EMAIL', '<li><b>Su dirección de correo electrónico:</b> Ninguna o entrada no válida!</li>');
define('ERROR_VVCODE', '<li><b>Código de seguridad:</b> No coincide, por favor, vuelva a introducir el código de seguridad!</li>');
define('ERROR_MSG_BODY', '<li><b>Su mensaje:</b> Sin datos!</li>');
 //EOC nicer error output, noRiddle
 
 //BOC new constants for VIN, price question and file upload, noRiddle
define('AH_MAIL_PRICE_QUESTION_SUBJ', 'Consulta de precio para el artículo:');
define('CONTACT_UPLOAD_TEXT', 'Se permiten %s con un tamaño máximo de %s.<br />No se permiten caracteres de espacio en blanco dentro del nombre de archivo.');
define('CONTACT_ERROR_FILE_SIZE', '<li><b>Subir imagen:</b> Archivo demasiado grande. Se permiten %s.</li>');
define('CONTACT_ERROR_FILE_EXT', '<li><b>Subir imagen:</b> Extensión de archivo no permitida. Se permiten %s.</li>');
define('CONTACT_ERROR_FILE_LENGTH_AND_CHARS', '<li><b>Subir imagen:</b> Nombre de archivo demasiado largo (se permiten un máximo de %d caracteres más extensión de archivo)<br />o se han encontrado caracteres no permitidos en el nombre de archivo.<br />Se permiten a-z, A-Z, 0-9, guión bajo y guión.</li>');
 //EOC new constants for VIN, price question and file upload, noRiddle
?>

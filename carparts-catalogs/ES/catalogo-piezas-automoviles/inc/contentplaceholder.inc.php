<?php
/*************************************************************************************
* file: contentplaceholder.inc.php
* path: /inc/
* use: replace placeholders in contents
*      used in smarty modifier contentplaceholder and /includes/modules/metatags.php
*
* (c) copyright 03-2021, noRiddle
*
* test query:
* SELECT ph_in_contents_type, ph_in_contents_placeholder, ph_in_contents_value FROM nr_placeholder_in_contents WHERE ph_in_contents_placeholder IN('[[LINK_KONTAKT]]', '[[TEXT_SHOP_ADRESSE]]');
* KEY on ph_in_contents_placeholder is used
*************************************************************************************/

function contentplaceholder_inc($html) {
    if(preg_match_all('#(\[\[[a-z_0-9]+\]\])#i', $html, $matches)) {
        $in_str = implode("', '", $matches[1]);
        
        $ph_qu_str = "SELECT ph_in_contents_type, ph_in_contents_placeholder, ph_in_contents_value FROM nr_placeholder_in_contents WHERE ph_in_contents_placeholder IN('".$in_str."')";
        $ph_qu = xtc_db_query($ph_qu_str);
        
        while($ph_arr = xtc_db_fetch_array($ph_qu)) {
            switch($ph_arr['ph_in_contents_type']) {
                case '1':
                    $html = str_replace($ph_arr['ph_in_contents_placeholder'], $ph_arr['ph_in_contents_value'], $html);
                  break;
                case '2':
                    $html = str_replace($ph_arr['ph_in_contents_placeholder'], xtc_href_link($ph_arr['ph_in_contents_value']), $html);
                  break;
                case '3':
                    $prod_qu_str = "SELECT products_id FROM ".TABLE_PRODUCTS." WHERE products_model = '".xtc_db_input($ph_arr['ph_in_contents_value'])."'";
                    $prod_qu = xtc_db_query($prod_qu_str);
                    $prod = xtc_db_fetch_array($prod_qu);
                    
                    $html = str_replace($ph_arr['ph_in_contents_placeholder'], xtc_href_link(FILENAME_PRODUCT_INFO, xtc_product_link($prod['products_id'])), $html);
                  break;
                case '4':
                    $html = str_replace($ph_arr['ph_in_contents_placeholder'], xtc_href_link(FILENAME_DEFAULT, xtc_category_link($ph_arr['ph_in_contents_value'])), $html);
                  break;
                case '5':
                    $html = str_replace($ph_arr['ph_in_contents_placeholder'], xtc_href_link(FILENAME_CONTENT, 'coID='.$ph_arr['ph_in_contents_value']), $html);
                  break;
                case '6':
                    $used_const = nl2br(constant($ph_arr['ph_in_contents_value']));
                    $html = str_replace($ph_arr['ph_in_contents_placeholder'], $used_const, $html);
                  break;
                case '7':
                    if(strpos($ph_arr['ph_in_contents_placeholder'], 'COID5') !== false) {
                        $plh_vals_arr = explode('_', $ph_arr['ph_in_contents_placeholder']);

                        if(isset($_SESSION['customer_id'])) {
                            if($plh_vals_arr[1] == 'LOGGEDIN') {
                                $phv = str_replace('[{CUSTOMER_NAME}]', $_SESSION['customer_first_name'].' '.$_SESSION['customer_last_name'], $ph_arr['ph_in_contents_value']);
                                $html = str_replace($ph_arr['ph_in_contents_placeholder'], $phv, $html);
                            } else {
                                $html = str_replace($ph_arr['ph_in_contents_placeholder'], '', $html);
                            }
                        } else {
                            if($plh_vals_arr[1] == 'LOGGEDOFF') {
                                $html = str_replace($ph_arr['ph_in_contents_placeholder'], $ph_arr['ph_in_contents_value'], $html);
                            } else {
                                $html = str_replace($ph_arr['ph_in_contents_placeholder'], '', $html);
                            }
                        }
                    } else {
                        $html = str_replace($ph_arr['ph_in_contents_placeholder'], $ph_arr['ph_in_contents_value'], $html); //we have editor now
                    }
                  break;
            }
        }
    } else if(strpos($html, '[{shop_suffix}]') !== false) {
        $html = str_replace('[{shop_suffix}]', SHOP_SUFFIX, $html);
    }
    
    return $html;
}
?>
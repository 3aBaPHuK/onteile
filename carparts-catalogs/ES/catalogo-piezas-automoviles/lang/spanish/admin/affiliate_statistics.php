<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_statistics.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_statistics.php, v 1.2 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Estadística sobre un afiliado');
define('TEXT_SUMMARY_TITLE', 'Estadística de afiliado');
define('TEXT_IMPRESSIONS', 'Impresiones del banner: ');
define('TEXT_VISITS', 'Visitas por la página de afiliado: ');
define('TEXT_TRANSACTIONS', 'Ventas de afiliado: ');
define('TEXT_AFFILIATE_NAME', 'Nombre de afiliado: ');
define('TEXT_AFFILIATE_JOINDATE', 'Fecha de registro: ');
define('TEXT_CONVERSION', 'Ventas/clics: ');
define('TEXT_AMOUNT', 'Total del valor de pedido: ');
define('TEXT_AVERAGE', 'Promedio: ');
define('TEXT_COMMISSION_RATE', 'Comisión: ');
define('TEXT_PAYPERSALE_RATE', 'Comisión por venta: ');
define('TEXT_CLICKTHROUGH_RATE', 'Pago por clic: ');
define('TEXT_COMMISSION', 'Importe de comisión: ');
define('TEXT_SUMMARY_HELP', '[?]');
define('TEXT_SUMMARY', 'Haga clic en [?] para leer una descripción sobre cada categoría.');
define('HEADING_SUMMARY_HELP', 'Afiliado ayuda');
define('HEADING_SUMMARY_HELP', 'Ayuda');
define('TEXT_IMPRESSIONS_HELP', '<b>Impresiones del banner</b> muestra cuántas veces un banner se ha mostrado en el periodo de tiempo determinado.');
define('TEXT_VISITS_HELP', '<b>Visitantes</b> muestra cuántos clics se han efectuado por visitantes, que han venido a través de su página web.');
define('TEXT_TRANSACTIONS_HELP', '<b>Ventas de afiliado</b> muestra cuántas ventas se han realizado a través de las páginas de afiliado.');
define('TEXT_CONVERSION_HELP', '<b>Ventas/Clics</b> muestra en un porcentaje cuántos visitantes llegados a través de su página web han relizado una compra.');
define('TEXT_AMOUNT_HELP', '<b>Total valor de pedido</b> muestra el total de pedido de todas las ventas entregadas.');
define('TEXT_AVERAGE_HELP', '<b>Promedio</b> representa el valor promedio de ventas que se han generado a través de su cuenta.');
define('TEXT_COMMISSION_RATE_HELP', '<b>Comisión</b> corresponde a la comisión porcentual con la que facturamos las ventas realizadas a través de usted.');
define('TEXT_CLICKTHROUGH_RATE_HELP', '<b>Pago por Clic</b> corresponde a la comisión que recibe con una base de comisión "Pago por Clic".');
define('TEXT_PAY_PER_SALE_RATE_HELP', '<b>Pago por Venta</b> corresponde a la comisión que recibe con una base de comisión "Pago por Venta".');
define('TEXT_COMMISSION_HELP', '<b>Importe de comisión</b> muestra sus ingresos de comisión de todas las ventas entregadas.');
define('TEXT_CLOSE_WINDOW', 'Cerrar ventana [x]');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_API_IT_RECHT_KANZLEI_TEXT_TITLE', 'Cabinet d\'avocat droit de technologie informatique auto-updater');
define('MODULE_API_IT_RECHT_KANZLEI_TEXT_DESCRIPTION', 'Cabinet d\'avocat droit de technologie informatique - auto-updater pour textes juridiques automatiques<br/><br/><b>IMPORTANT :</b> Avant l\'utilisation du module l\'attribution des pages de contenu doit être assurée.<hr noshade>');
define('MODULE_API_IT_RECHT_KANZLEI_STATUS_TITLE', 'Statut');
define('MODULE_API_IT_RECHT_KANZLEI_STATUS_DESC', 'Statut de module');
define('MODULE_API_IT_RECHT_KANZLEI_TOKEN_TITLE', 'Jeton d\'authentification');
define('MODULE_API_IT_RECHT_KANZLEI_TOKEN_DESC', 'Jeton d\'authentification que vous allez communiquer au cabinet d\'avocat de droit informatique.');
define('MODULE_API_IT_RECHT_KANZLEI_VERSION_TITLE', 'Version API');
define('MODULE_API_IT_RECHT_KANZLEI_VERSION_DESC', 'Ceci est à modifier uniquement si vous y êtes demandé du cabinet d\'avocat du droit informatique. (Standard: 1.0)');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_AGB_TITLE', '<hr noshade>Texte juridique conditions générales');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_AGB_DESC', 'Veuillez indiquer sur dans quelle page vous souhaitez insérer ce texte juridique automatiquement.');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_DSE_TITLE', 'Texte juridique protection de données');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_DSE_DESC', 'Veuillez indiquer sur dans quelle page vous souhaitez insérer ce texte juridique automatiquement.');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_WRB_TITLE', 'Texte juridique révocation');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_WRB_DESC', 'Veuillez indiquer sur dans quelle page vous souhaitez insérer ce texte juridique automatiquement.');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_IMP_TITLE', 'Texte juridique mentions légales');
define('MODULE_API_IT_RECHT_KANZLEI_TYPE_IMP_DESC', 'Veuillez indiquer sur dans quelle page vous souhaitez insérer ce texte juridique automatiquement.');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_AGB_TITLE', '<hr noshade>Sélection conditions générales PDF texte juridique');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_AGB_DESC', 'Mention si conditions générales sera disponible en format PDF.');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_DSE_TITLE', 'Sélection protection de données texte juridique');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_DSE_DESC', 'Mention si texte sur la protection de données sera disponible en format PDF.');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_WRB_TITLE', 'Sélection révocation PDF texte juridique');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_WRB_DESC', 'Mention si texte sur la révocation sera disponible en format PDF.');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_FILE_TITLE', '<hr noshade>Emplacement de stockage PDF');
define('MODULE_API_IT_RECHT_KANZLEI_PDF_FILE_DESC', 'Mention de l\'emplacement du texte juridique PDF.');

?>

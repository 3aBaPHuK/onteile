<?php
/* --------------------------------------------------------------
   $Id: countries.php 4200 2013-01-10 19:47:11Z Tomcraft1980 $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(countries.php,v 1.8 2002/01/19); www.oscommerce.com 
   (c) 2003	 nextcommerce (countries.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Pays');

define('TABLE_HEADING_COUNTRY_NAME', 'Pays');
define('TABLE_HEADING_COUNTRY_CODES', 'Codes ISO');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Statut');

define('TEXT_INFO_EDIT_INTRO', 'Veuillez effectuer les modifications nécessaires');
define('TEXT_INFO_COUNTRY_NAME', 'Nom:');
define('TEXT_INFO_COUNTRY_CODE_2', 'Code ISO (2):');
define('TEXT_INFO_COUNTRY_CODE_3', 'Code ISO (3):');
define('TEXT_INFO_ADDRESS_FORMAT', 'Format d\'adresse:');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez entrer le nouveau pays avec tous les données importantes');
define('TEXT_INFO_DELETE_INTRO', 'Supprimer le pays?');
define('TEXT_INFO_HEADING_NEW_COUNTRY', 'Nouveau pays');
define('TEXT_INFO_HEADING_EDIT_COUNTRY', 'Modifier le pays');
define('TEXT_INFO_HEADING_DELETE_COUNTRY', 'Supprimer le pays');

define('TABLE_HEADING_REQUIRED_ZONES', 'Afficher les régions');
?>

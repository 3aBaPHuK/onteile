<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_affiliates.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_affiliates.php, v 1.9 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Participants au programme d\'affiliation');
define('HEADING_TITLE_SEARCH', 'Recherche:');

define('TABLE_HEADING_FIRSTNAME', 'Prénom');
define('TABLE_HEADING_LASTNAME', 'Nom');
define('TABLE_HEADING_COMMISSION', 'Provision');
define('TABLE_HEADING_ACCOUNT', 'Solde de compte');
define('TABLE_HEADING_USERHOMEPAGE', 'Page d\'accueil');
define('TABLE_HEADING_ACCOUNT_CREATED', 'Accès généré le');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_AFFILIATE_ID', 'ID d\'affilié');

define('TEXT_DATE_ACCOUNT_CREATED', 'Accès généré le:');
define('TEXT_DATE_ACCOUNT_LAST_MODIFIED', 'Dernier changement');
define('TEXT_INFO_DATE_LAST_LOGON', 'Dernière connexion:');
define('TEXT_INFO_NUMBER_OF_LOGONS', 'Nombre de connexions:');
define('TEXT_INFO_COMMISSION', 'Provision');
define('TEXT_INFO_TIERS_ALLOWED', 'A le droit d\'avoir de sous-affiliés: ');
define('TEXT_INFO_NUMBER_OF_SALES', 'Nombre de ventes:');
define('TEXT_INFO_COUNTRY', 'Pay:');
define('TEXT_INFO_SALES_TOTAL', 'Somme de toutes les ventes:');
define('TEXT_INFO_AFFILIATE_TOTAL', 'Provision');
define('TEXT_DELETE_INTRO', 'Voulez-vous vraiment supprimer ce partenaire?');
define('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'Supprimer le partenaire');
define('TEXT_DISPLAY_NUMBER_OF_AFFILIATES', 'Montrant <b>%d</b> à <b>%d</b> (d\' au total <b>%d</b> partenaires affiliés)');

define('ENTRY_AFFILIATE_PAYMENT_DETAILS', 'Payable à:');
define('ENTRY_AFFILIATE_PAYMENT_CHECK', 'chèque pour:');
define('ENTRY_AFFILIATE_PAYMENT_PAYPAL', 'email compte PayPal:');
define('ENTRY_AFFILIATE_PAYMENT_BANK_NAME', 'Nom de la banque:');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NAME', 'Propriétaire du compte:');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NUMBER', 'Numéro de compte:');
define('ENTRY_AFFILIATE_PAYMENT_BANK_BRANCH_NUMBER', 'Code bancaire:');
define('ENTRY_AFFILIATE_PAYMENT_BANK_SWIFT_CODE', 'Code SWIFT:');
define('ENTRY_AFFILIATE_COMPANY', 'Entreprise');
define('ENTRY_AFFILIATE_COMPANY_TAXID', 'Numéro IDE:');
define('ENTRY_AFFILIATE_HOMEPAGE', 'Page d\'accueil');
define('ENTRY_AFFILIATE_COMMISSION', 'Provision par vente en %');
define('ENTRY_AFFILIATE_TIERS_ALLOWED', 'A le droit d\'avoir des sous-affiliés: ');

define('CATEGORY_COMMISSION', 'Provision individuelle');
define('CATEGORY_PAYMENT_DETAILS', 'Paiement se fait par:');

define('TYPE_BELOW', 'Veuillez indiquer en-dessous');
define('PLEASE_SELECT', 'Choisir');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(banktransfer.php,v 1.9 2003/02/18 19:22:15); www.oscommerce.com
   (c) 2003	 nextcommerce (banktransfer.php,v 1.5 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   OSC German Banktransfer v0.85a       	Autor:	Dominik Guder <osc@guder.org>

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
define('MODULE_PAYMENT_TYPE_PERMISSION', 'bt');

define('MODULE_PAYMENT_BANKTRANSFER_TEXT_TITLE', 'Procedimiento de domiciliación bancaria');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_DESCRIPTION', 'Procedimiento de domiciliación bancaria');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_INFO', '');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK', 'Domiciliación bancaria');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_EMAIL_FOOTER', 'Aviso: Puede descargar nuestro formulario de fax bajo ' . HTTP_SERVER . DIR_WS_CATALOG . MODULE_PAYMENT_BANKTRANSFER_URL_NOTE . ' y devolvernoslo rellenado.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_INFO', ((MODULE_PAYMENT_BANKTRANSFER_IBAN_ONLY != 'True') ? 'Por favor, tenga en cuenta que el procedimiento de domiciliación bancaria sin indicación de IBAN/BIC <b>sólo</b> es posible desde una <b>cuenta corriente alemana</b> . Por indicación de IBAN/BIC puede utilizar el  procedimiento de domiciliación bancaria <b>en toda la UE</b> .<br/>' : '') . 'Campos marcados con (*) son obligatorios. Con un IBAN alemán, el BIC es opcional.<br/><br/>');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_OWNER', 'Titular de la cuenta:*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_OWNER_EMAIL', 'Titular de la cuenta de correo electrónico:*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_NUMBER', 'Número de cuenta / IBAN:*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_IBAN', 'IBAN:*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_BLZ', 'Código bancario / BIC:*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_BIC', 'BIC:*');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_NAME', 'Banco:');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_FAX', 'La autorización de domiciliación se confirmará por fax');

// Note these MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_X texts appear also in the URL, so no html-entities are allowed here
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR', 'ERROR: ');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_1', 'Si el número de cuenta y el código bancario no coinciden, por favor, corrija su entrada.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_2', 'Este número de cuenta no es verificable, por favor, compruebe su entrada de nuevo por seguridad.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_3', 'Este número de cuenta no es verificable, por favor, compruebe su entrada de nuevo por seguridad.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_4', 'Diese Kontonummer ist nicht prüfbar, bitte kontrollieren Sie zur Sicherheit Ihre Eingabe nochmals.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_5', 'Este código bancario no existe, por favor, corrija su entrada.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_8', 'No ha introducido un código bancario correcto.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_9', 'No ha introducido un número de cuenta correcto.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_10', 'No ha introducido un titular de cuenta.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_11', 'No ha introducido un BIC correcto.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_12', 'No ha introducido un IBAN correcto.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_13', 'Dirección de correo electrónico no válida para la notificación del titular de la cuenta.');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_BANK_ERROR_14', 'No hay autorización de domiciliación bancaria para este país de la SEPA.');

define('MODULE_PAYMENT_BANKTRANSFER_TEXT_NOTE', 'Aviso:');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_NOTE2', 'Si, por razones de seguridad, no desea transferir ningún dato bancario a través de Internet<br />puede descargar nuestro ');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_NOTE3', 'formulario de fax');
define('MODULE_PAYMENT_BANKTRANSFER_TEXT_NOTE4', ' y reenviarnoslo rellenado.');

define('JS_BANK_BLZ', '* Por favor, introduzca el código bancario / BIC de su banco!\n\n');
define('JS_BANK_NAME', '* Por favor, introduzca el nombre de su banco!\n\n');
define('JS_BANK_NUMBER', '* Por favor, introduzca su número de cuenta / IBAN!\n\n');
define('JS_BANK_OWNER', '* Por favor, introduzca el nombre del titular de la cuenta!\n\n');
define('JS_BANK_OWNER_EMAIL', '* Por favor, introduzca la dirección de correo electrónico del titular de la cuenta!\n\n');

define('MODULE_PAYMENT_BANKTRANSFER_DATABASE_BLZ_TITLE', '¿Usar la búsqueda en la base de datos para comprobar el código bancario?');
define('MODULE_PAYMENT_BANKTRANSFER_DATABASE_BLZ_DESC', '¿Desea utilizar la base de datos para la comprobación de la plausibilidad del código bancario ("true")?<br/>¡Asegúrese de que los códigos bancarios de la base de datos están al día!<br/><a href="'.xtc_href_link(defined('FILENAME_BLZ_UPDATE')?FILENAME_BLZ_UPDATE:'').'" target="_blank"><strong>Enlace: --> CÓDIGO BANCARIO ACTUALIZACIÓN <-- </strong></a><br/><br/>Con "false" (por defecto) se utiliza el archivo blz.csv proporcionado, que posiblemente contiene entradas obsoletas!');
define('MODULE_PAYMENT_BANKTRANSFER_URL_NOTE_TITLE', 'URL de Fax');
define('MODULE_PAYMENT_BANKTRANSFER_URL_NOTE_DESC', 'El archivo de confirmación de fax. Esto debe estar en el directorio del catálogo.');
define('MODULE_PAYMENT_BANKTRANSFER_FAX_CONFIRMATION_TITLE', 'Permitir confirmación de fax');
define('MODULE_PAYMENT_BANKTRANSFER_FAX_CONFIRMATION_DESC', '¿Desea permitir la confirmación por fax?');
define('MODULE_PAYMENT_BANKTRANSFER_SORT_ORDER_TITLE', 'Orden de visualización');
define('MODULE_PAYMENT_BANKTRANSFER_SORT_ORDER_DESC', 'El orden de visualización. Primero se muestra el dígito más pequeño.');
define('MODULE_PAYMENT_BANKTRANSFER_ORDER_STATUS_ID_TITLE', 'Determinar estado del pedido');
define('MODULE_PAYMENT_BANKTRANSFER_ORDER_STATUS_ID_DESC', 'Establecer los pedidos realizados con este módulo en este estado.');
define('MODULE_PAYMENT_BANKTRANSFER_ZONE_TITLE', 'Zona de pago');
define('MODULE_PAYMENT_BANKTRANSFER_ZONE_DESC', 'Si se selecciona una zona, el método de pago se aplica sólo a esa zona.');
define('MODULE_PAYMENT_BANKTRANSFER_ALLOWED_TITLE', 'Zonas permititdas');
define('MODULE_PAYMENT_BANKTRANSFER_ALLOWED_DESC', 'Indique <b>individualmente</b> las zonas que deben permitirse para este módulo. (por ejemplo AT,DE (si está vacío, se permiten todas las zonas))');
define('MODULE_PAYMENT_BANKTRANSFER_STATUS_TITLE', 'Permitir pagos por transferencia bancaria');
define('MODULE_PAYMENT_BANKTRANSFER_STATUS_DESC', '¿Desea permitir los pagos por transferencia bancaria?');
define('MODULE_PAYMENT_BANKTRANSFER_MIN_ORDER_TITLE', 'Pedidos necesarios');
define('MODULE_PAYMENT_BANKTRANSFER_MIN_ORDER_DESC', 'El número mínimo de pedidos que un cliente debe tener para que la opción esté disponible.');
define('MODULE_PAYMENT_BANKTRANSFER_IBAN_ONLY_TITLE', 'Modo IBAN');
define('MODULE_PAYMENT_BANKTRANSFER_IBAN_ONLY_DESC', '¿Sólo quiere permitir los pagos IBAN?');

// SEPA
define('MODULE_PAYMENT_BANKTRANSFER_CI_TITLE', 'Número de identificación del acreedor (CI)');
define('MODULE_PAYMENT_BANKTRANSFER_CI_DESC', 'Introduzca aquí su ID de acreedor SEPA.');
define('MODULE_PAYMENT_BANKTRANSFER_REFERENCE_PREFIX_TITLE', 'Prefijo de la referencia del mandato (opcional)');
define('MODULE_PAYMENT_BANKTRANSFER_REFERENCE_PREFIX_DESC', 'Introduzca aquí un prefijo para la referencia de mandato.');
define('MODULE_PAYMENT_BANKTRANSFER_DUE_DELAY_TITLE', 'vencimiento');
define('MODULE_PAYMENT_BANKTRANSFER_DUE_DELAY_DESC', 'Introduzca el período (en días) después del cual desea ejecutar la domiciliación bancaria.');

define('MODULE_PAYMENT_BANKTRANSFER_TEXT_EXTENDED_DESCRIPTION', '<strong><font color="red">ATENCIÓN:</font></strong> Por favor, actualice los códigos bancarios bajo "Programas de ayuda" -> <a href="'.xtc_href_link('blz_update.php').'"><strong>"actualizar códigos bancarios"</strong></a>!');

?>

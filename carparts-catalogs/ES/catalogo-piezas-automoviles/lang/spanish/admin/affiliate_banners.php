<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_banners.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_banners.php, v 1.3 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Gestión de Banners del Programa de Afiliados');

define('TABLE_HEADING_BANNERS', 'Banner');
define('TABLE_HEADING_GROUPS', 'Grupo');
define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_STATISTICS', 'Mostrar / Clics');
define('TABLE_HEADING_PRODUCT_ID', 'Enlace a');

define('TEXT_BANNERS_TITLE', 'Título del Banner:');
define('TEXT_BANNERS_GROUP', 'Grupo del Banner');
define('TEXT_BANNERS_NEW_GROUP', ', o introduzca abajo un nuevo grupo de Banner');
define('TEXT_BANNERS_IMAGE', 'Imagen (Archivo)');
define('TEXT_BANNERS_IMAGE_LOCAL', ', o introduzca abajo el archivo local en su servidor');
define('TEXT_BANNERS_IMAGE_TARGET', 'Destino de la imagen (Guardar en):');
define('TEXT_BANNERS_HTML_TEXT', 'Texto HTML:');
define('TEXT_BANNERS_TEXT', 'Enlace de texto:');
define('TEXT_AFFILIATE_BANNERS_NOTE', '<b>Nota de Banner:</b><ul><li>Puede utilizar Banner de imagen o de texto HTML, utilizar ambos simultáneamente no es posible.</li><li>Si utiliza ambos tipos de Banner simultáneamente, sólo se muestra el Banner de texto HTML.</li></ul>');

define('TEXT_BANNERS_LINKED_PRODUCT', 'Enlace a: ');
define('TEXT_BANNERS_LINKED_HOME', '--Página de inicio--');
define('TEXT_BANNERS_LINKED_PRODUCT_NOTE', 'Si desea vincular el Banner a una parte específica de su tienda, simplemente selecciónelo arriba. P - representa el producto, C - la categoría y M - el contenido de la tienda.');

define('TEXT_BANNERS_DATE_ADDED', 'agregado el:');
define('TEXT_BANNERS_STATUS_CHANGE', 'Estado cambiado: %s');

define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar este Banner?');
define('TEXT_INFO_DELETE_IMAGE', 'Eliminar imagen de Banner');

define('SUCCESS_BANNER_INSERTED', 'Éxito: El Banner ha sido insertado.');
define('SUCCESS_BANNER_UPDATED', 'Éxito: El Banner ha sido actualizado.');
define('SUCCESS_BANNER_REMOVED', 'Éxito: El Banner ha sido borrado.');

define('ERROR_BANNER_TITLE_REQUIRED', 'Error: Se necesita un título de Banner.');
define('ERROR_BANNER_GROUP_REQUIRED', 'Error: Se necesita un grupo de Banner.');
define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Error: El directorio de destino no existe %s.');
define('ERROR_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Error: El directorio de destino no es grabable %s.');
define('ERROR_IMAGE_DOES_NOT_EXIST', 'Error: La imagen no existe.');
define('ERROR_IMAGE_IS_NOT_WRITEABLE', 'Error: La imagen no se puede borrar.');
?>

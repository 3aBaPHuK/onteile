<?php
/* --------------------------------------------------------------
   $Id: modules.php 2957 2012-05-31 11:55:56Z Tomcraft1980 $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(modules.php,v 1.8 2002/04/09); www.oscommerce.com
   (c) 2003 nextcommerce (modules.php,v 1.5 2003/08/14); www.nextcommerce.org
   (c) 2006 XT-Commerce (modules.php 899 2005-04-29)

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE_MODULES_PAYMENT', 'Modes de paiement');
define('HEADING_TITLE_MODULES_SHIPPING', 'Modes de livraison');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Total des mdules de commande');

define('TABLE_HEADING_MODULES', 'Module');
define('TABLE_HEADING_SORT_ORDER', 'Ordre de tri');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_MODULE_DIRECTORY', 'Répértoire de module:');
define('TEXT_MODULE_FILE_MISSING', '<b>Fichier de langue"%s" manque, le module "%s" n\'est pas affiché!</b>');
define('TABLE_HEADING_FILENAME', 'Nom du module (pour l\'utilisation interne)');

// BOF - Tomcraft - 2009-10-03 - Paypal Express Modul
define('TEXT_INFO_DELETE_PAYPAL', 'Si vous désintallez le module maintenant les données de transactions PayPal seront supprimées!<br /> Si vous voulez garder ce fichier, annulez l\'action et désactivez simplement le module. (Activer le module = False)');
// EOF - Tomcraft - 2009-10-03 - Paypal Express Modul
define('TABLE_HEADING_MODULES_INSTALLED', 'Les modules suivants ont été installés');
define('TABLE_HEADING_MODULES_PREFERRED', 'Modules populaires');
define('TABLE_HEADING_MODULES_NOT_INSTALLED', 'Les modules suivants sont encore disponibles');
define('TEXT_MODULE_UPDATE_NEEDED', 'Les modules suivant ont été actualisés et nécessitent une mise à jour de la base de données. Veuillez enregistrer les réglages et réinstallez ces modules.');
?>

﻿<?php
if(!isset($_SESSION['customer_id'])) xtc_redirect(xtc_href_link(FILENAME_DEFAULT)); //redirect to start page if not logged in, noRiddle

// Include soap request class
include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'requestOem.php');
// Include view class
include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'render'.DIRECTORY_SEPARATOR.'wizard2'.DIRECTORY_SEPARATOR.'wizard.php');
include('nr_catalogue_files/extender.php');

class WizardExtender extends CommonExtender { 
	function FormatLink($type, $dataItem, $catalog, $renderer)
	{
		if ($type == 'vehicles') {
			//BOC set link to content 1001 vehicles.php, noRiddle
            //return 'vehicles.php?ft=findByWizard2&c='.$catalog.'&ssd='.$_GET['ssd'];
            return xtc_href_link(FILENAME_CONTENT, 'coID=1001').'?ft=findByWizard2&c='.$catalog.'&ssd='.$_GET['ssd'];
            //return xtc_href_link(FILENAME_CONTENT, 'coID=1001&ft=findByWizard2&c='.$catalog.'&ssd='.$_GET['ssd']);
            //EOC set link to content 1001 vehicles.php, noRiddle
        }
		else
			//BOC set link to content 1002 wizard2.php, noRiddle
            //return 'wizard2.php?c='.$catalog.'&ssd=$ssd$';
            return xtc_href_link(FILENAME_CONTENT, 'coID=1002').'?c='.$catalog.'&ssd=$ssd$';
            //return xtc_href_link(FILENAME_CONTENT, 'coID=1002&c='.$catalog.'&ssd=$ssd$');
            //EOC set link to content 1002 wizard2.php, noRiddle
	}	
}

// Create request object
$request = new GuayaquilRequestOEM($_GET['c'], $_GET['ssd'], Config::$catalog_data);
if (Config::$useLoginAuthorizationMethod) {
    $request->setUserAuthorizationMethod(Config::$userLogin, Config::$userKey);
}

// Append commands to request
$request->appendGetCatalogInfo();
$request->appendGetWizard2($_GET['ssd']);

// Execute request
$data = $request->query();

// Check errors
if ($request->error != '')
{
    echo $request->error;
}
else
{
  $wizard = $data[1];
  $cataloginfo = $data[0]->row;

	//echo '<h1>'.CommonExtender::LocalizeString('Search by wizard').' - '.$cataloginfo['name'].'</h1>';
    echo '<h1>'
         //.'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.Config::$nr_icon_arr[$_GET['c']].'" alt ="" />&nbsp;'
         .'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['brand_icon'].'" alt ="'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].'" />&nbsp;'
         .CommonExtender::LocalizeString('Search by wizard').' - '.$cataloginfo['name']
         .'</h1>'
         .'<span class="h-sml">'.CommonExtender::LocalizeString('Search by wizard sub').'</span>';
    
    //BOC new html, noRiddle
    echo '<div id="main-top cf">';
    echo '<div class="log-and-butt cf">';
    //echo '<div class="brand-logo"><img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/'.strtolower($cataloginfo['icon']).'" alt ="" /></div>';
    echo '<div class="top-buttons cf">';
    /*echo '<a href="/katalog_shop_test/"><span class="btn button-catalogue"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_brand_choice').'</span></a>&nbsp;'
         .'<a href="'.xtc_href_link(FILENAME_CATALOGUE_SHOPPING_CART).'"><span class="btn button-catalogue"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_shopp_cart').'</span></a>';*/
    echo '</div>';
    echo '</div>';
    echo '</div>';
    //EOC new html, noRiddle

	$renderer = new GuayaquilWizard(new WizardExtender());
	echo $renderer->Draw($_GET['c'], $wizard);
}
?>
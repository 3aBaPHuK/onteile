<?php
  /* --------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Trusted Shops');
define('HEADING_TECHNOLOGIE', 'Tecnología Trustbadge');
define('HEADING_PRODUCTS', 'Productos');

define('TABLE_HEADING_TRUSTEDSHOPS_ID', 'TS-ID');
define('TABLE_HEADING_LANGUAGE', 'Idioma');
define('TABLE_HEADING_STATUS', 'Estado');
define('TABLE_HEADING_ACTION', 'Acción');

define('HEADING_TRUSTBADGE', 'Trustbadge');
define('HEADING_ADVANCED', 'Avanzado');

define('TEXT_DATE_ADDED', 'Agregado el:');
define('TEXT_LAST_MODIFIED', 'Actualizado el:');
define('TEXT_TRUSTEDSHOPS_STATUS', 'Estado:');
define('TEXT_TRUSTEDSHOPS_ID', 'TS-ID:');
define('TEXT_TRUSTEDSHOPS_LANGUAGES', 'Seleccionar idioma:');
define('TEXT_TRUSTEDSHOPS_WIDGET', 'Activar el widget de calificación de clientes:');
define('TEXT_TRUSTEDSHOPS_SNIPPETS', 'Activar la visualización de Rich Snippets en Google:');
define('TEXT_TRUSTEDSHOPS_SNIPPETS_INFO', '<a href="https://developers.google.com/structured-data/rich-snippets/products" target="_blank">documentación de Goolge Rich Snippets</a>');
define('TEXT_WIDGET_INFO', 'El widget muestra sus estrellas de calificación y la calificación más reciente, además del Trustbadge en su tienda.');
define('TEXT_TRUSTBADGE_INFO', 'El Trustbadge muestra su sello de calidad y las calificaciones de sus clientes en la versión estándar y se encuentra en la esquina inferior derecha de su tienda. Aquí puede elegir si desea utilizar el Trustbadge en la variante recomendada (por defecto), si sólo desea mostrar sus calificaciones de cliente o si desea individualizar la visualización y la posición (se requieren conocimientos de programación).');
define('TEXT_SNIPPETS_PRODUCTS', 'Páginas de detalles del producto');
define('TEXT_SNIPPETS_CATEGORY', 'Páginas de categorías');
define('TEXT_SNIPPETS_INDEX', 'Página de inicio (no se recomienda)');

define('TEXT_TRUSTEDSHOPS_BADGE', 'Seleccione la variante:');
define('TEXT_BADGE_DEFAULT', 'Estándar');
define('TEXT_BADGE_SMALL', 'Estándar (pequeño)');
define('TEXT_BADGE_REVIEWS', 'Calificaciones');
define('TEXT_BADGE_CUSTOM', 'Personalizado');
define('TEXT_BADGE_CUSTOM_REVIEWS', 'Personalizado (Reseñas)');
define('TEXT_BADGE_OFFSET', 'Posición eje Y (máx. 250px):');
define('TEXT_BADGE_INSTRUCTION', 'En nuestro Centro de integración encontrará una <a href="%s" target="_blank">Guía paso a paso</a> para la configuración e incorporación individual.');
define('TEXT_BADGE_CUSTOM_CODE', 'Introducir aquí el código de Trustbadge');

define('TEXT_PRODUCT_STICKER_API', 'API de etiquetas de producto (Beta):');
define('TEXT_PRODUCT_STICKER_API_INFO', 'Con nuestra API, le proporcionamos la interfaz para utilizar sus reseñas de productos en su propio sistema.');
define('TEXT_PRODUCT_STICKER_STATUS', 'Estado de la etiqueta del producto:');
define('TEXT_PRODUCT_STICKER', 'Editar el código de etiqueta del producto:');
define('TEXT_PRODUCT_STICKER_INFO', 'La etiqueta de producto muestra las calificaciones actuales de los productos en su tienda.<br/>Con nuestra <a target="_blank" href="%s">Guía</a> puede configurar su etiqueta de producto.');
define('TEXT_PRODUCT_STICKER_INTRO', 'calificaciones de clientes');

define('TEXT_REVIEW_STICKER_STATUS', 'Estado de la etiqueta de la reseña:');
define('TEXT_REVIEW_STICKER', 'Editar el código de la etiqueta de la reseña:');
define('TEXT_REVIEW_STICKER_INFO', 'La etiqueta de reseña muestra las calificaciones actuales para su tienda.<br/>MCon nuestra <a target="_blank" href="%s">Guía</a> puede configurar su etiqueta de reseña.');
define('TEXT_REVIEW_STICKER_INTRO', 'calificaciones de clientes');

define('TEXT_HEADING_DELETE_TRUSTEDSHOPS', 'Borrar TS-ID');
define('TEXT_DELETE_INTRO', '¿Está seguro de que quiere borrar este TS-ID?');

define('TEXT_DISABLED', 'desactivado');
define('TEXT_ENABLED', 'activado');

define('TEXT_DISPLAY_NUMBER_OF_TRUSTEDSHOPS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> TS-ID)');

define('TEXT_TS_MAIN_INFO', '
<img src="images/trustedshops/trustmark-widget-de-AT-CH-DE-w300-h245-v1.png" style="width:200px;float:right;margin-top:30px;padding-left:30px;"/>
<h2>Trusted Shops</h2>
<b>Confianza: la clave al éxito</b><br/>
<br/>
Con más de 15 años de experiencia y más de 20.000 tiendas, Trusted Shops es la marca de confianza líder en Europa en comercio electrónico. Ofrezca a sus clientes más seguridad al comprar en línea con el módulo Trusted Shops, para una experiencia de compra positiva y un aumento de las ventas.
<ul>
  <li><b>Sello de calidad con garantía de devolución de dinero:</b>  Seguridad para una mayor venta/li>
  <li><b>Calificaciones de clientes y productos:</b> Confianza para más tráfico</li>
  <li><b>Redactor legal y protección de amonestaciones:</b> Experiencia para una mayor seguridad jurídica</li>
</ul>');
define('TEXT_TS_BADGE_INFO', '
<img src="images/trustedshops/trustbadge_Review_Trustmark_de-DE.png" style="width:115px;float:right;margin-top:10px;padding-left:30px;"/>
<h2>Tecnología Trustbadge</h2>
<b>Simple, flexible, innovador</b><br/>
<br/>
El módulo Trusted Shops integra la tecnología Trustbadge en su tienda: muestra sus elementos de confianza visibles en todas las páginas de la tienda online y funciona como un centro de control que ofrece a sus clientes la garantía de devolución de dinero en el momento adecuado o realiza una solicitud de evaluación después del pedido.<br/>
<br/>
Y como código dinámico, la Trustbadge está bien preparada para el futuro: Las funciones se pueden actualizar fácilmente y se pueden integrar nuevas funciones. Con la tecnología Trustbadge, puede estar seguro de que siempre está aprovechando al máximo las soluciones de confianza de Trusted Shops.');
define('TEXT_TS_PRODUCT_INFO', '
<h2>Productos</h2>
<b>Sus ventajas con Trusted Shops</b><br/>
<br/>
<ul>
  <li><b>Sello de calidad - más confianza, más ventas</b><br/>
      Más del 60% de los compradores en línea confían en los sellos de calidad. Utilice esta confianza para obtener tasas de conversión más altas.</li>
  <li><b>Reseñas de clientes - cinco estrellas dicen más de mil palabras</b><br/>
      Marketing de rating profesional para más atención en Google, más SEO y más posibilidades de análisis.</li>
  <li><b>Garantía de devolución de dinero - más seguridad, menos cancelaciones de compra</b><br/>
      Cuanto menor sea el riesgo financiero para el comprador, mayor será la cesta de compra y más fácil será el proceso de compra.</li>
</ul>');
define('TEXT_TS_SPECIAL_INFO', '
<b>Nuestro especial para usted:<br/>¡permanentemente más barato para los usuarios de modified!</b><br/>
<a target="_blank" href="http://www.trustedshops.de/shopbetreiber/index.html?shopsw=MODIFIED#offer?utm_source=Xtmodified&utm_medium=modul&utm_content=link1&utm_campaign=modultracking&a_aid=55cb437783a78"><img src="images/trustedshops/btn_330x40_de-DE_v2.png" style="margin-top:10px;"/></a>');
            
?>

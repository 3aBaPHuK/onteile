<?php
/* --------------------------------------------------------------
   $Id: mail.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(mail.php,v 1.9 2002/01/19); www.oscommerce.com 
   (c) 2003	 nextcommerce (mail.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Envoyer les courriels aux clients');

define('TEXT_CUSTOMER', 'Client:');
define('TEXT_SUBJECT', 'Sujet:');
define('TEXT_FROM', 'De:');
define('TEXT_MESSAGE', 'Message:');
define('TEXT_SELECT_CUSTOMER', 'Choisir les clients');
define('TEXT_ALL_CUSTOMERS', 'Tous les clients');
define('TEXT_NEWSLETTER_CUSTOMERS', 'Tous les abonnés de la Newsletter');

define('NOTICE_EMAIL_SENT_TO', 'Remarque: Le courriel à été envoyé à %s');
define('ERROR_NO_CUSTOMER_SELECTED', 'Erreur: Aucun client n\'a été séléctionné.');
?>

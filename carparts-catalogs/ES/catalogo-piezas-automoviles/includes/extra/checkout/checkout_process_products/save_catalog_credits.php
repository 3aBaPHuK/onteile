<?php
/****************************************************************************************
* file: save_catalog_credits.php
* use: if catalog_credits for VIN search were ordered update credits in table customers
*
* (c) noRiddle 04-2018
****************************************************************************************/

//BOC set catalog_credit for VIN if products_id = 1 (product must have this ID !!), noRiddle
$bought_vin_credit = false;
if($sql_data_array['products_id'] == '1') {
    $bought_vin_credit = true;
    $bought_qty_credits = $sql_data_array['products_quantity']; //store quantity in var
    $bought_how_many_credits = 0; //define this in /includes/etra/checkout/checkout_process_attributes/save_catalog_credits.php
}
//EOC set catalog_credit for VIN if products_id = 1 (product must have this ID !!), noRiddle
?>
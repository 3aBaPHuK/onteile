<?php
/* -----------------------------------------------------------------------------------------
   $Id: shop_content.php 10724 2017-05-10 09:20:22Z web28 $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(conditions.php,v 1.21 2003/02/13); www.oscommerce.com 
   (c) 2003 nextcommerce (shop_content.php,v 1.1 2003/08/19); www.nextcommerce.org
   (c) 2006 XT-Commerce (shop_content.php 1238 2005-09-24)

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

require_once ('includes/application_top.php');

// redirect contact form to SSL if available
if (ENABLE_SSL == true && $request_type == 'NONSSL' && !isset($_GET['action']) && $_GET['coID'] == '7') {
  xtc_redirect(xtc_href_link(FILENAME_CONTENT, 'coID='.(int) $_GET['coID'], 'SSL'));
}

// create smarty elements
$smarty = new Smarty;

// include boxes
require (DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/source/boxes.php');

// include needed functions
require_once (DIR_FS_INC.'xtc_validate_email.inc.php');

if ($language_not_found === true) {
  $site_error = TEXT_CONTENT_NOT_FOUND;
  include (DIR_WS_MODULES.FILENAME_ERROR_HANDLER);
  require (DIR_WS_INCLUDES.'header.php');

} else {

  $shop_content_query = xtc_db_query("SELECT content_id, 
                                             content_title, 
                                             content_heading, 
                                             content_text, 
                                             content_file,
                                             parent_id
                                        FROM ".TABLE_CONTENT_MANAGER."
                                       WHERE content_group='".(int) $_GET['coID']."'
                                             ".CONTENT_CONDITIONS."
                                         AND content_active = '1'
                                         AND trim(content_title) != ''
                                         AND languages_id=".(int)$_SESSION['languages_id']);
  
  $content_exists = xtc_db_num_rows($shop_content_query);
  if ($shop_content_data = xtc_db_fetch_array($shop_content_query)) {
    // sub content
    include (DIR_WS_MODULES.'sub_content_listing.php');

    $breadcrumb->add($shop_content_data['content_title'], xtc_href_link(FILENAME_CONTENT,'coID='.(int) $_GET['coID']));
  } else {
    $site_error = TEXT_CONTENT_NOT_FOUND;
    $shop_content_data['content_heading'] = TEXT_CONTENT_NOT_FOUND;
  }

  $smarty->assign('CONTENT_HEADING', (($shop_content_data['content_heading'] != '') ? $shop_content_data['content_heading'] : '')); //$shop_content_data['content_title'])); //we don't want a title if heading is empty in catalogue, noRiddle

  if ($_GET['coID'] == 7 && $content_exists == 1) {
    include (DIR_WS_INCLUDES.'contact_us.php');
  } else {
    $content_body = '';
    if ($content_exists == 1) {
      //BOC new redirect from search_results.php, noRiddle
      //$content_body = $shop_content_data['content_text'];
      $sr_error = '';
      if (isset($_SESSION['search_errorno'])) {
        if($_SESSION['search_errorno'] == 1) {
            $sr_error .= str_replace('\n', '<br />', AH_JS_AT_LEAST_ONE_INPUT);
        }
      }
      
      if(isset($sr_error) && $sr_error != '') {
        $content_body .= '<p style="margin:0 0 8px; padding:2px 4px; border:1px solid #999; border-radius:5px;"><span style="color:#c00;">'.$sr_error.'</span></p>';
        unset($_SESSION['search_errorno']);
      }
      
      $content_body .= $shop_content_data['content_text'];
      //EOC new redirect from search_results.php, noRiddle
      if ($shop_content_data['content_file'] != '' && is_file(DIR_FS_CATALOG.'media/content/'.$shop_content_data['content_file'])) {
        ob_start();
        if (strpos($shop_content_data['content_file'], '.txt'))
          echo '<pre>';
        include (DIR_FS_CATALOG.'media/content/'.$shop_content_data['content_file']);
        if (strpos($shop_content_data['content_file'], '.txt'))
          echo '</pre>';
        $smarty->assign('file', ob_get_contents());
        ob_end_clean();
      }
    }
    $smarty->assign('CONTENT_BODY', $content_body);

    $link = 'javascript:history.back(1)';
    if (!isset($_SERVER['HTTP_REFERER']) 
        || strpos($_SERVER['HTTP_REFERER'], HTTP_SERVER) === false
        )
    {
      $link = xtc_href_link(FILENAME_DEFAULT, '', 'NONSSL');
    } 
    $smarty->assign('BUTTON_CONTINUE', '<a href="'.$link.'">'.xtc_image_button('button_back.gif', IMAGE_BUTTON_BACK).'</a>');
    $smarty->assign('language', $_SESSION['language']);

    //BOC flag to show content or not, depending on just created account or admin or other content than coID NR_ACCOUNT_SUCCESS_CONTENT, 01-2020, noRiddle
    $jac_show_cont = 'true';
    if($_GET['coID'] == (int)NR_ACCOUNT_SUCCESS_CONTENT) {
      if(!isset($_SESSION['just_acc_created']) && $_SESSION['customers_status']['customers_status_id'] != '0') {
          $jac_show_cont = 'false';
      }
      unset($_SESSION['just_acc_created']); //session var set in /create_account.php
    }
    $smarty->assign('JAC_SHOW_CONT', $jac_show_cont);
    //EOC flag to show content or not, depending on just created account or admin or other content than coID NR_ACCOUNT_SUCCESS_CONTENT, 01-2020, noRiddle

    // set cache ID
     if (!CacheCheck()) {
      $smarty->caching = 0;
      $main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/content.html');
    } else {
      $smarty->caching = 1;
      $smarty->cache_lifetime = CACHE_LIFETIME;
      $smarty->cache_modified_check = CACHE_CHECK;
      $cache_id = md5($_SESSION['language'].$_SESSION['customers_status']['customers_status'].$shop_content_data['content_id'].((isset($_REQUEST['error'])) ? $_REQUEST['error'] : ''));
      $main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/content.html', $cache_id);
    }
  }

  if (($_GET['coID'] != 7) || (isset($_GET['action']) && $_GET['action'] == 'success') || $content_exists == 0) {
    require (DIR_WS_INCLUDES.'header.php');
  }
}
$smarty->assign('language', $_SESSION['language']);
$smarty->assign('main_content', $main_content);
$smarty->caching = 0;
if (!defined('RM'))
  $smarty->load_filter('output', 'note');
$smarty->display(CURRENT_TEMPLATE.'/index.html');
include ('includes/application_bottom.php');
?>
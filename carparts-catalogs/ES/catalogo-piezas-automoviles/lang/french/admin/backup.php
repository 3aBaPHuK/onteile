<?php
/* --------------------------------------------------------------
   $Id: backup.php 10762 2017-06-07 11:57:15Z web28 $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(backup.php,v 1.21 2002/06/15); www.oscommerce.com 
   (c) 2003	 nextcommerce (backup.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'sauvegarde de la base de données');

define('TABLE_HEADING_TITLE', 'Titre');
define('TABLE_HEADING_FILE_DATE', 'Date');
define('TABLE_HEADING_FILE_SIZE', 'Taille');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_HEADING_NEW_BACKUP', 'Nouvelle sauvegarde');
define('TEXT_INFO_HEADING_RESTORE_LOCAL', 'restaurer localement');
define('TEXT_INFO_NEW_BACKUP', 'Veuillez EN AUCUN CAS interrompre le processus de sauvegarde. Cela peut prendre quelques minutes.');
define('TEXT_INFO_UNPACK', '<br /><br />(après que les fichiers ont été extraits de l\'archive)');
define('TEXT_INFO_RESTORE', 'Veuillez EN AUCUN CAS interrompre le processus de restauration.<br /><br /> Plus le fichier de sauvegarde est grand, plus longtemps dure la restauration!<br /><br />Veuillez utiliser le client mysql si possible.<br /><br />Exemple:<br /><br /><b>mysql -h' . DB_SERVER . ' -u' . DB_SERVER_USERNAME . ' -p ' . DB_DATABASE . ' < %s </b> %s');
define('TEXT_INFO_RESTORE_LOCAL', 'Veuillez EN AUCUN CAS interrompre le processus de restauration. Plus le fichier de sauvegarde est grand, plus longtemps dure la restauration!');
define('TEXT_INFO_RESTORE_LOCAL_RAW_FILE', 'Le fichier à télécharger doit être un fichier raw sql (que du texte).');
define('TEXT_INFO_DATE', 'Date:');
define('TEXT_INFO_SIZE', 'Taille:');
define('TEXT_INFO_COMPRESSION', 'Comprimer:');
define('TEXT_INFO_USE_GZIP', 'Avec GZIP');
define('TEXT_INFO_USE_ZIP', 'Avec ZIP');
define('TEXT_INFO_USE_NO_COMPRESSION', 'Pas de compression(Raw SQL)');
define('TEXT_INFO_DOWNLOAD_ONLY', 'Seulement télécharger (ne pas enregistrer sur le serveur)');
define('TEXT_INFO_BEST_THROUGH_HTTPS', 'Utiliser une connexion sécurisé HTTPS!');
define('TEXT_NO_EXTENSION', 'Aucun');
define('TEXT_BACKUP_DIRECTORY', 'Répertoire de sauvegarde:');
define('TEXT_LAST_RESTORATION', 'Dernière restauration:');
define('TEXT_FORGET', '(<u>oublier</u>)');
define('TEXT_DELETE_INTRO', 'Vous en êtes sûr de supprimer cette sauvegarde?');

define('ERROR_BACKUP_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le répertoire de sauvegarde n\'existe pas.');
define('ERROR_BACKUP_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le répertoire de sauvegarde est verrouillé.');
define('ERROR_DOWNLOAD_LINK_NOT_ACCEPTABLE', 'Erreur: Téléchargement du lien n\'est pas acceptable.');

define('SUCCESS_LAST_RESTORE_CLEARED', 'Succès: La dernière date de restauration a été supprimé.');
define('SUCCESS_DATABASE_SAVED', 'Succès: La banque de données a été sauvegardée.');
define('SUCCESS_DATABASE_RESTORED', 'Succès: La banque de données a été restauré.');
define('SUCCESS_BACKUP_DELETED', 'Succès: Le fichier de sauvegarde a été supprimé.' );
define('SUCCESS_BACKUP_UPLOAD', 'Succès: Le fichier de sauvegarde téléchargé avec succès.');

//TEXT_COMPLETE_INSERTS
define('TEXT_COMPLETE_INSERTS', "<b>'INSERT's complètes</b><br> - Noms de champ sont inscrits sur toute ligne INSERT (agrandi la sauvegarde)");

define('TEXT_INFO_TABLES_IN_BACKUP', '<br />' . "\n" .'<b>Tableaux dans cette sauvegarde:</b>' . "\n");
define('TEXT_INFO_NO_INFORMATION', 'Pas d\'informations disponibles');
//UTF-8 convert
define('TEXT_CONVERT_TO_UTF', 'Convertir la banque de données en UTF-8');
define('TEXT_IMPORT_UTF', 'Restaurer la banque de données UTF-8');

//TEXT_REMOVE_COLLATE
define('TEXT_REMOVE_COLLATE', "<b>Sans codage de caractères  'COLLATE' et 'DEFAULT CHARSET'</b><br> - Les données pour le codage de caractères ne seront pas saisies. Raisonnable en cas de migration vers un codage de caractères DB.");

//TEXT_REMOVE_ENGINE
define('TEXT_REMOVE_ENGINE', "<b>Sans moteur de sauvegarde  'ENGINE'</b><br> - Les données par rapport au moteur de sauvegarde (MyISAM, InnoDB) ne seront pas inserées.");
?>

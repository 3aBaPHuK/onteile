<?php
/* -----------------------------------------------------------------------------------------
   $Id:$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Saldo del cliente');

define('TABLE_HEADING_GV_ID', 'ID');
define('TABLE_HEADING_GV_NAME', 'Nombre');
define('TABLE_HEADING_GV_AMOUNT', 'Saldo');
define('HEADING_TITLE_TOTAL', 'Saldos totales de todos los clientes: ');
?>

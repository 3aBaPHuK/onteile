<?php
/* *******************************************************************************
* ah_module_export.php, noRiddle 07-2017
**********************************************************************************/

define('HEADING_TITLE_MODULES_AH_EXPORT', 'Exportation des clients/ des commandes');

define('TABLE_HEADING_AH_MODULES', 'Module');
define('TABLE_HEADING_AH_SORT_ORDER', 'Ordre');
define('TABLE_HEADING_AH_STATUS', 'Statut');
define('TABLE_HEADING_AH_ACTION', 'Action');

define('TEXT_MODULE_AH_DIRECTORY', 'Annuaire des modules:');
define('TABLE_HEADING_AH_FILENAME', 'Nom du module (utilisation interne)');
define('ERROR_AH_EXPORT_FOLDER_NOT_WRITEABLE', '%s annuaire pas enregistrable!');
define('TEXT_AH_MODULE_INFO', 'Veuillez vérifier l\'actualité des modules chez le fournisseur respectif!');

define('TABLE_HEADING_AH_MODULES_INSTALLED', 'Les modules suivants ont été installés');
define('TABLE_HEADING_AH_MODULES_NOT_INSTALLED', 'Les modules suivants sont disponible');
?>

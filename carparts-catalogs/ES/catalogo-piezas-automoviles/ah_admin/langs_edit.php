<?php
/***********************************************************************************************************
* file: langs_edit.php
* use: write all language constants and smarty conf variables into database for translation purposes
*      display them, make them editable and generate files
*
* (c) noRiddle 06-2018
*
* examples with multi line defines: ot_ps_fee, ot_cod_fee, ot_payment
***********************************************************************************************************/

require('includes/application_top.php');

//******* BOC config *******
@ini_set('max_input_vars', 5000); //for big files and two, three languages //doesn't work, set it in .htaccess of /ah_admin/

define('NR_LANG_INTO_CSV_CSV_SEP', '|');
define('NR_LANG_INTO_CSV_TXT_SEP', '');

define('NR_LT_BASIC_LANG_PATH', DIR_FS_CATALOG.'lang/');
define('NR_LT_HELP_LANG_PATH', NR_LT_BASIC_LANG_PATH.'helpfiles/');
define('NR_LT_TRANSL_LANG_PATH', 'translated/');
//******* EOC config *******

require(DIR_WS_CLASSES.'class_langs_edit.php');
$cle = new class_langs_edit($messageStack);

$helper_lang_dir = false;
$nrl_load_basic = false;
$nrl_load_helper = false;
$nrl_load_exist = false;

$nrl_basic_langs = $cle->build_basic_lang_arr(); //for basic lang
$nrl_helper_langs = $cle->build_helper_lang_arr(); //for helper languages dropdown
$nrl_exist_langs = $cle->build_basic_lang_arr(true);
$nrl_file_dropdown = $cle->file_dropdown();

if(isset($_POST['load_basic_lang'])) {
    if($_POST['load_basic_lang'] != '?' && in_array($_POST['load_basic_lang'], $nrl_basic_langs)) {
        $nrl_beginpath = NR_LT_BASIC_LANG_PATH.$_POST['load_basic_lang'];

        $nrl_load_basic = true;
    } else {
        $messageStack->add(NR_LANGS_EDIT_NOLANG_CHOOSEN_OR_NO_DIR, 'warning');
    }
}

if(isset($_POST['load_exist_lang'])) {
    if($_POST['load_exist_lang'] != '?' && in_array($_POST['load_exist_lang'], $nrl_exist_langs)) {
        $nrl_beginpath = NR_LT_BASIC_LANG_PATH.$_POST['load_exist_lang'];

        $nrl_load_exist = true;
    } else {
        $messageStack->add(NR_LANGS_EDIT_NOLANG_CHOOSEN_OR_NO_DIR, 'warning');
    }
}

if(is_dir(NR_LT_HELP_LANG_PATH)) {
    $helper_lang_dir = true;
    if(isset($_POST['load_helper_lang'])) {
        if($_POST['load_helper_lang'] != '?' && in_array($_POST['load_helper_lang'], $nrl_helper_langs)) {
            $nrl_beginpath = NR_LT_HELP_LANG_PATH.$_POST['load_helper_lang'];

            $nrl_load_helper = true;
        } else {
            $messageStack->add(NR_LANGS_EDIT_NOLANG_CHOOSEN_OR_NO_DIR, 'warning');
        }
    }
}

if(isset($nrl_beginpath) && $nrl_beginpath != '') {
    $nrl_dirs = $cle->dir_lang_iterate($nrl_beginpath);
}
//echo '<pre>'.print_r($nrl_dirs, true).'</pre>';
//echo '<pre>'.print_r($cle->dir_lang_iterate(NR_LT_BASIC_LANG_PATH.'german'), true).'</pre>';

//BOC let's do it via ajax
if(isset($_POST['transl_lang_data']) && !empty($_POST['transl_lang_data'])) {
    $arr_srch = array('%5Cn', '%5Cr', '%5Ct'); // %5C = urlencoded \
    $arr_repl = array('%5C\n', '%5C\r', '%5C\t');
    $_POST['transl_lang_data'] = str_replace($arr_srch, $arr_repl, $_POST['transl_lang_data']);
    parse_str($_POST['transl_lang_data'], $post_data);
    foreach($post_data['transl_lang'] as $pdlne => $pdval_arr) {
        foreach($pdval_arr as $pdkey => $pdval) {
            $post_data['transl_lang'][$pdlne][$pdkey] = stripslashes($pdval);
        }
    }
    //echo '<pre>'.print_r($post_data['transl_lang'], true).'</pre>';
    //echo '<pre>'.$_POST['transl_lang_data'].'</pre>';

    if($cle->write_transl_vals($post_data['transl_lang'])) {
        echo 'saved / gespeichert';
    }
    exit();
}
//EOC let's do it via ajax

if(isset($_POST['gener_files'])) {
    $cle->generate_transl_files($_POST['gener_files'], $_GET['load_foredit']);
    //echo '<pre>'.$cle->generate_transl_files($_POST['gener_files'], $_GET['load_foredit']).'</pre>';
    
    $cle->build_messageStack($cle->mstck);
}

if($nrl_load_basic === true || $nrl_load_helper === true || $nrl_load_exist === true) {
    //***** BOC read files *****
    $nrl_files = array();
    $nrl_cont = array();
    $extensions = implode(',', $cle->file_ext_arr);
    //echo '<pre>$nrl_beginpath: '.$nrl_beginpath.'</pre>';
    //echo '<pre>$nrl_dirs: '.print_r($nrl_dirs, true).'</pre>';

    foreach($nrl_dirs as $key => $nrl_dir) {
        $nrl_files[$nrl_dir] = glob($nrl_dirs[$key].'*.{'.$extensions.'}', GLOB_BRACE);
    }

    foreach($nrl_files as $dir_path => $file_path) {
        if(!empty($file_path)) {
            foreach($file_path as $fk => $fp) {
                $f_name = basename($fp);
                $nrl_cont[$dir_path][$f_name] = file($fp, FILE_IGNORE_NEW_LINES);
            }
        }
    }
    //echo '<pre>'.print_r($nrl_files, true).'</pre>';
    //echo '<pre>'.print_r($nrl_cont, true).'</pre>';
    //***** EOC read files *****

    //***** BOC write into DB *****
    if(!empty($nrl_cont)) {
        if($nrl_load_basic === true) {
            $cle->prepare_db_basis_table($_POST['load_basic_lang']);
        } else if($nrl_load_helper === true) {
            $cle->prepare_db_help_table($_POST['load_helper_lang']);
        } else if($nrl_load_exist === true) {
            $cle->prepare_db_help_table($_POST['load_exist_lang'], true);
        }

        foreach($nrl_cont as $dirp => $fn) {
            if($nrl_load_basic === true) {
                $nr_p_id = $cle->basic_load_path($dirp, $_POST['load_basic_lang']); //store path
            }

            foreach($fn as $fk => $lines) {
                if($nrl_load_basic === true) {
                    $nr_f_id = $cle->basic_load_file($fk, $dirp, $nr_p_id, $_POST['load_basic_lang']); //store file name
                }

                if(substr($fk, -4) == '.php') { // we have a *.php file
                    if($nrl_load_basic === true) {
                        if($nr_p_id != 0 && $nr_f_id != 0) {
                            $transl_line_var_sql = '';
                        }
                    } else if($nrl_load_helper === true) {
                        $transl_line_var_sql = '';
                    } else if($nrl_load_exist === true) {
                        $transl_line_var_sql = '';
                    }

                    foreach($lines as $lk => $lv) {
                        $val_pairs = array();
                        $ml_def[$lk] = 'false';

                        if(strpos($lv, "define('") !== false && strpos($lv, '//define') === false) { //we have a constant definition
                            preg_match('#define\(([\']?[^,]*[\']?),[ ]*(.*)#i', $lv, $val_pairs);

                            //BOC overwrite $val_pairs[0] with information whether define is in one single line or multi line
                            if(strrpos($val_pairs[0], ');') !== false) {
                                $val_pairs[0] = 'sl_cns'; //sl = single line

                                $pos_close = strrpos($val_pairs[2], ');');
                                $val_pairs[2] = substr($val_pairs[2], 0, $pos_close);
                            } else {
                                $val_pairs[0] = 'ml_cns'; //ml = multi line
                                $ml_def[$lk] = 'true'; //set flag to remember we have multi line
                                $ml_cns[$lk] = $val_pairs[1]; //store constant name for consecutive lines with multi line definition
                            }
                            //EOC overwrite $val_pairs[0] with information whether define is in one single line or multi line
                        } else if(preg_match('#\$[a-z_]+\[\'.+\'\][ ]*=[ ]*.+#i', $lv)) { //we have an array with [key]-notation as definition (e.g. masterpayment_callback.php)
                            if(strpos($lv, '=') !== false) { // we have a definition
                                preg_match('#([^=]*)[ ]*=[ ]*(.*)#i', $lv, $val_pairs);

                                $val_pairs[0] = 'arrk';
                                $val_pairs[1] = trim($val_pairs[1]);
                            }
                        } else if(preg_match('#\'[a-z_]+\'[ ]*=\>[ ]*.+#i', $lv)) { //we have an array whose keys will be defined as constants (e.g. paypalpluslink.php)
                            if(strpos($lv, '=>') !== false) {
                                preg_match('#([^=\>]+)[ ]*=\>[ ]*(.*)#i', $lv, $val_pairs);

                                $val_pairs[0] = 'arr';
                                $val_pairs[1] = trim($val_pairs[1]);
                            }
                        } else { //we have some code, comment or empty lines
                            if($ml_def[$lk-1] == 'true') {
                                if(strrpos($lv, ');') === false || (strrpos($lv, ');') !== false && strpos($lv, 'onclick') !== false)) {
                                    $val_pairs[0] = 'ccns'; //consecutive multi line
                                    $val_pairs[1] = $ml_cns[$lk-1]; //constant name of multi line defined constant
                                    
                                    $ml_def[$lk] = 'true'; //set flag to remember we have multi line
                                    $ml_cns[$lk] = $val_pairs[1]; //store constant name for consecutive lines with multi line definition
                                    $new_lv = $lv;
                                } else if(strrpos($lv, ');') !== false && strpos($lv, 'onclick') === false) {
                                    $val_pairs[0] = 'ccns_l'; //last consecutive multi line
                                    $val_pairs[1] = $ml_cns[$lk-1]; //constant name of multi line defined constant
                                    
                                    $pos_close = strrpos($lv, ');');
                                    $new_lv = substr($lv, 0, $pos_close);
                                }
                            } else {
                                $val_pairs[0] = 'lne';
                                $val_pairs[1] = '';
                                $new_lv = $lv;
                            }

                            //$val_pairs[0] .= ' | '.$ml_def[$lk]; //verify for print_r()
                            $val_pairs[2] = $new_lv;
                        }

                        //$nrl_cont[$dirp][$fk][$lk] = $val_pairs;

                        if($nrl_load_basic === true) {
                            if($nr_p_id != 0 && $nr_f_id != 0) {
                                $transl_line_var_sql .= $cle->basic_load_var($nr_p_id, $nr_f_id, $lk, $val_pairs);
                            }
                            //echo '<pre>'.print_r($val_pairs, true).'</pre>';
                        } else if($nrl_load_helper === true) {
                            $transl_line_var_sql .= $cle->helper_load_var($dirp, $fk, $lk, $val_pairs, $_POST['load_helper_lang'], 'help');
                        } else if($nrl_load_exist === true) {
                            $transl_line_var_sql .= $cle->helper_load_var($dirp, $fk, $lk, $val_pairs, $_POST['load_exist_lang'], 'exists');
                        }
                    }

                    if($nrl_load_basic === true) {
                        if($transl_line_var_sql != '') {
                            if($nr_p_id != 0 && $nr_f_id != 0) {
                                $transl_line_var_eff_sql = "INSERT INTO nr_lang_transl_basis (nr_lt_line_no, nr_lt_const_or_conf, nr_lt_var, nr_lt_value_".$_POST['load_basic_lang'].", nr_lt_path_id, nr_lt_file_id, nr_lt_date_loaded) VALUES ";
                                $transl_line_var_eff_sql .= $transl_line_var_sql;

                                if(xtc_db_query($transl_line_var_eff_sql)) {
                                    $cle->mstck['succ'][] = sprintf(NR_LANGS_EDIT_CONSTS_LOADED, $fk, ucwords($_POST['load_basic_lang'])); //'Definierte Konstanten aus '.$fk.' für '.ucwords($_POST['load_basic_lang']).' wurden eingelesen';
                                }
                            }
                        }
                    } else if($nrl_load_helper === true) {
                        if($transl_line_var_sql != '') {
                            $transl_line_var_eff_sql = "INSERT INTO nr_lang_transl_help (nr_lt_id, nr_lt_const_or_conf, nr_lt_var, nr_lt_value_".$_POST['load_helper_lang'].") VALUES ";
                            $transl_line_var_eff_sql .= $transl_line_var_sql;
                            $transl_line_var_eff_sql = rtrim($transl_line_var_eff_sql, ',');
                            $transl_line_var_eff_sql .= " ON DUPLICATE KEY UPDATE nr_lt_value_".$_POST['load_helper_lang']." = VALUES(nr_lt_value_".$_POST['load_helper_lang'].")";

                            //echo '<pre>'.$transl_line_var_eff_sql.'</pre>';
                            if(xtc_db_query($transl_line_var_eff_sql)) {
                                $cle->mstck['succ'][] = sprintf(NR_LANGS_EDIT_CONSTS_LOADED, $fk, ucwords($_POST['load_helper_lang'])); //'Definierte Konstanten aus '.$fk.' für '.ucwords($_POST['load_helper_lang']).' wurden eingelesen';
                            }
                        }
                    } else if($nrl_load_exist === true) {
                        if($transl_line_var_sql != '') {
                            $transl_line_var_eff_sql = "INSERT INTO nr_lang_transl_help (nr_lt_id, nr_lt_const_or_conf, nr_lt_var, nr_lt_value_".$_POST['load_exist_lang'].") VALUES ";
                            $transl_line_var_eff_sql .= $transl_line_var_sql;
                            $transl_line_var_eff_sql = rtrim($transl_line_var_eff_sql, ',');
                            $transl_line_var_eff_sql .= " ON DUPLICATE KEY UPDATE nr_lt_value_".$_POST['load_exist_lang']." = VALUES(nr_lt_value_".$_POST['load_exist_lang'].")";

                            //echo '<pre>'.$transl_line_var_eff_sql.'</pre>';
                            if(xtc_db_query($transl_line_var_eff_sql)) {
                                $cle->mstck['succ'][] = sprintf(NR_LANGS_EDIT_CONSTS_LOADED, $fk, ucwords($_POST['load_exist_lang'])); //'Definierte Konstanten aus '.$fk.' für '.ucwords($_POST['load_exist_lang']).' wurden eingelesen';
                            }
                        }
                    }
                } else if(substr($fk, -5) == '.conf' || substr($fk, -7) == '.custom') { //we have a smarty *.conf file or a *.custom file in the template dir /lang/
                    if($nrl_load_basic === true) {
                        $transl_line_var_sql = '';
                    } else if($nrl_load_helper === true) {
                        $transl_line_var_sql = '';
                    } else if($nrl_load_exist === true) {
                        $transl_line_var_sql = '';
                    }

                    foreach($lines as $lk => $lv) {
                        $val_pairs = array();

                        if(strpos($lv, '=') !== false) { // we have a definition
                            //$val_pairs = explode('=', $lv);
                            preg_match('#([^=]*)[ ]*=[ ]*(.*)#i', $lv, $val_pairs);
                            
                            $val_pairs[0] = 'cnf';
                            $val_pairs[1] = trim($val_pairs[1]); //take off any white space

                            $line_ins_sql = "";
                        } else { // we have a section, comnment or empty line
                            $val_pairs[0] = 'lne';
                            $val_pairs[1] = '';
                            $val_pairs[2] = $lv;
                        }

                        //$nrl_cont[$dirp][$fk][$lk] = $val_pairs;
                        
                        if($nrl_load_basic === true) {
                            if($nr_p_id != 0 && $nr_f_id != 0) {
                                $transl_line_var_sql .= $cle->basic_load_var($nr_p_id, $nr_f_id, $lk, $val_pairs);
                            }
                        } else if($nrl_load_helper === true) {
                            $transl_line_var_sql .= $cle->helper_load_var($dirp, $fk, $lk, $val_pairs, $_POST['load_helper_lang'], 'help');
                        } else if($nrl_load_exist === true) {
                            $transl_line_var_sql .= $cle->helper_load_var($dirp, $fk, $lk, $val_pairs, $_POST['load_exist_lang'], 'exists');
                        }
                    }

                    if($nrl_load_basic === true) {
                        if($transl_line_var_sql != '') {
                            if($nr_p_id != 0 && $nr_f_id != 0) {
                                $transl_line_var_eff_sql = "INSERT INTO nr_lang_transl_basis (nr_lt_line_no, nr_lt_const_or_conf, nr_lt_var, nr_lt_value_".$_POST['load_basic_lang'].", nr_lt_path_id, nr_lt_file_id, nr_lt_date_loaded) VALUES ";
                                $transl_line_var_eff_sql .= $transl_line_var_sql;

                                if(xtc_db_query($transl_line_var_eff_sql)) {
                                    $cle->mstck['succ'][] = sprintf(NR_LANGS_EDIT_CONFS_LOADED, $fk, ucwords($_POST['load_basic_lang'])); //'Definierte Smarty-Conf Variablen aus '.$fk.' für '.ucwords($_POST['load_basic_lang']).' wurden eingelesen';
                                }
                            }
                        }
                    } else if($nrl_load_helper === true) {
                        if($transl_line_var_sql != '') {
                            $transl_line_var_eff_sql = "INSERT INTO nr_lang_transl_help (nr_lt_id, nr_lt_const_or_conf, nr_lt_var, nr_lt_value_".$_POST['load_helper_lang'].") VALUES ";
                            $transl_line_var_eff_sql .= $transl_line_var_sql;
                            $transl_line_var_eff_sql = rtrim($transl_line_var_eff_sql, ',');
                            $transl_line_var_eff_sql .= " ON DUPLICATE KEY UPDATE nr_lt_value_".$_POST['load_helper_lang']." = VALUES(nr_lt_value_".$_POST['load_helper_lang'].")";

                            //echo '<pre>'.$transl_line_var_eff_sql.'</pre>';
                            if(xtc_db_query($transl_line_var_eff_sql)) {
                                $cle->mstck['succ'][] = sprintf(NR_LANGS_EDIT_CONFS_LOADED, $fk, ucwords($_POST['load_helper_lang'])); //'Definierte Smarty-Conf Variablen aus '.$fk.' für '.ucwords($_POST['load_helper_lang']).' wurden eingelesen';
                            }
                        }
                    } else if($nrl_load_exist === true) {
                        if($transl_line_var_sql != '') {
                            $transl_line_var_eff_sql = "INSERT INTO nr_lang_transl_help (nr_lt_id, nr_lt_const_or_conf, nr_lt_var, nr_lt_value_".$_POST['load_exist_lang'].") VALUES ";
                            $transl_line_var_eff_sql .= $transl_line_var_sql;
                            $transl_line_var_eff_sql = rtrim($transl_line_var_eff_sql, ',');
                            $transl_line_var_eff_sql .= " ON DUPLICATE KEY UPDATE nr_lt_value_".$_POST['load_exist_lang']." = VALUES(nr_lt_value_".$_POST['load_exist_lang'].")";

                            //echo '<pre>'.$transl_line_var_eff_sql.'</pre>';
                            if(xtc_db_query($transl_line_var_eff_sql)) {
                                $cle->mstck['succ'][] = sprintf(NR_LANGS_EDIT_CONFS_LOADED, $fk, ucwords($_POST['load_exist_lang'])); //'Definierte Smarty-Conf Variablen aus '.$fk.' für '.ucwords($_POST['load_exist_lang']).' wurden eingelesen';
                            }
                        }
                    }
                }
            }
        }
    }
    //***** EOC write into DB *****

    $cle->build_messageStack($cle->mstck);
}

//BOC HTML
require (DIR_WS_INCLUDES.'head.php');
echo '<link rel="stylesheet" type="text/css" href="/'.$shop.'/templates/'.CURRENT_TEMPLATE.'/css/font-awesome.css">';
?>
<style>
body {position:relative;}
.working-gif {display:none; position:fixed; top:50%; left:50%; margin:-17px 0 0 -17px; z-index:3; font-size:50px; color:#bd6060; animation:load_rot 1.5s linear infinite;}
@keyframes load_rot {100% {transform:rotate(360deg);}}
h4 {font-size: 12px; margin: 6px 0 0;}
#green-alert {
width:600px;
display:none;
position:fixed;
left:50%;
top:50%;
z-index:2;
padding:6px;
margin:-33px 0 0 -300px;
border:2px solid #fff;
border-radius:6px;
background-color:#75BD68;
box-shadow:0 0 6px #636363;
font-size:14px;
line-height:17px;
font-weight:bold;
color:#fff;
text-align:center;
}
.cl-alert {position:absolute; right:-10px; top:-10px; width:24px; height:24px; line-height:24px; text-align:center; background:#000; color:#fff; border-radius:50%; cursor:pointer;}
#maini18n {margin:10px 8px;}
#maini18n p {margin:8px 0;}
.maini18n-buttons {margin:6px 0 0; padding:0; border:1px solid #ccc;}
.maini18n-edit-langs {margin:6px 0 0; padding:0; border:1px solid #ccc;}
.maini18n-edit-langs h3 {margin:0; padding:4px; background:#ccc; font-size:14px;}
.info {margin:0 0 6px; padding:6px; border:1px solid #ccc;}
.info h2, .tog-main h2 {margin:0; padding:4px; background:#ddd; border:1px solid #ccc; font-size:16px; cursor:pointer;}
.info h2 i.fa:before, .tog-main h2 i.fa:before {content:"\f063";} /* \f0a7 \f0ab */
.info h2.open i.fa:before, .tog-main h2.open i.fa:before {content:"\f062";} /* \f0a6 \f0aa */
.tog-cont {display:none;}
.info .tog-cont {padding:4px;}
.tog-main {padding:6px;}
table {border-collapse: separate; border-spacing: 6px;}
table.tw100 {width:100%;}
.ttr {background:#ddd;}
.ttd {padding:5px; vertical-align:middle;}
.tdw10 {width:10%;}
.tdw17 {width:16.666%;}
.tdw20 {width:20%;}
.tdw25 {width:25%;}
.tdw30 {width:30%;}
.tdw33 {width:33.333%;}
.tdw40 {width:40%;}
.tdw50 {width:50%;}
.tdw60 {width:60%;}
.tdw75 {width:75%;}
.ttd div {margin:3px 0;}
.inl-bl-elm {display:inline-block; padding:0 8px 0 0;}
button.button, input.button, a.button {
    display:inline-block;
    padding: 6px 15px;
    color:#ddd;
    background-color:#444;
    font-size:10px;
    line-height:12px;
    font-weight:bold;
    border-color: #333;
    border-style: solid;
    border-width: 1px;
    vertical-align: middle;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    /*margin: 4px 4px 0px 0px;  */
    cursor:pointer;
}
button .fa {padding:0 8px 0 0; font-size:12px;}
button.button.b-static {position:fixed; top:50%; left:-2px; padding:8px; /*transform:rotate(-90deg);*/ }
button.button.b-static .fa {padding:0; font-size:18px;}
button#to-top {display:none; left:auto; right:-2px;}
button.button:hover, input.button:hover, a.button:hover {
    color:#fff;
    background-color:#333;
    border-color: #222;
    text-decoration:none !important;
    cursor:pointer;
}
label {cursor:pointer;}
input[type="checkbox"] {vertical-align:middle;}
.maini18n-edit-langs input[readonly="readonly"], .maini18n-edit-langs textarea[readonly="readonly"] {background:#ccc;}
.maini18n-edit-langs input, .maini18n-edit-langs textarea {font-family:'Courier New', monospace;}
.maini18n-edit-langs input {width:100%;}
.maini18n-edit-langs textarea {width:97%; padding:6px 4px; resize:both;}

.red {color:#c00;}
.dred {color:#960000;}
.green, .green-a {color:green;}
.dgreen {color:#015001;}
.orange {color:#ae6800;}
.tooltip {position:relative;}
.tt-cont {
    position:absolute;
    left:0; 
    bottom:485%;
    width:220%;
    padding:4px 5px;
    /*color:#333;*/
    font-size:10px;
    border:1px solid #ccc;
    border-radius:5px;
    /*box-shadow:1px 1px 5px #b4b4b4; /*0 0 4px #9c9c9c;*/
    background:#fff;
    visibility:hidden;
    opacity:0;
    z-index:101;
    transition:all 0.4s ease 0.1s;
}
.tt-cont:after {
    content:'';
    position:absolute;
    top:100%;
    left:215px;
    height:0;
    width:0;
    border:10px solid;
    border-color:#fff transparent transparent transparent;
}
.tt-cont:before {
    content:'';
    position:absolute;
    top:100%;
    left:216px;
    height:0;
    width:0;
    border:10px solid;
    border-color:#ccc transparent transparent transparent;
}
.tooltip:hover {z-index:101;}
.tooltip:hover .tt-cont {bottom:355%; visibility:visible; opacity:1;}
/* ***** file tree structure ***** */
.nr-tree-h {padding:10px;}
.nr-tree-h table {border-spacing:20px 0;}
.nr-tree-h table td {position:relative; padding:2px 0; vertical-align:middle; /*border-right:1px solid #e6e6e6;*/}
.nr-tree-h table td:before {
    content:'';
    position:absolute;
    top:50%;
    left:-20px;
    width:20px;
    border-top:1px solid #ccc;
}
.nr-tree-h table td:after {
    content:'';
    position:absolute;
    top:0;
    left:-20px;
    height:100%;
    border-left:1px solid #ccc;
}

/*take of connector lines for first, last and only elements*/
.nr-tree-h table tbody > tr:first-child > td:first-child:after {
    top:50%;
    height:50%;
}
.nr-tree-h table tbody > tr:last-child > td:first-child:after {
    top:0;
    height:50%;
}
.nr-tree-h table tbody > tr:only-child > td:only-child:after {
    top:0;
    height:0;
}
.nr-tree-h table td:nth-child(2) {padding:0; border:0 none;}
.nr-tree-h table td:nth-child(2):after {border:0 none;}

.nr-tree-h td span {
	position:relative;
    display:block;
	padding:2px 4px;
	text-decoration:none;
	color:#666;
	font-size:12px;
	border:1px solid #ccc;
	border-radius:5px;
	transition:all 0.5s;
    cursor:pointer;
}
.nr-tree-h td span.red-sp {color:#c00;}
.nr-tree-h td span.green-sp {color:green;}
.nr-tree-h td span.dir-sp {background:#ffffb3;}
.nr-tree-h td span:hover {
	background:#c8e4f8; border: 1px solid #94a0b4;
}
.nr-tree-h td .test-file-butt {position:absolute; right:-22px; top:-2px; padding: 2px 3px; font-size:10px; background:#fff; border:1px solid #ccc;}
</style>
</head>
<body>
    <?php
    require(DIR_WS_INCLUDES . 'header.php'); 
    //echo '<pre>'.print_r($nrl_helper_langs, true).'</pre>';
    //echo '<pre>'.print_r($nrl_basic_langs, true).'</pre>';
    //echo '<pre>'.print_r($nrl_cont, true).'</pre>';
    //echo '<pre>'.print_r($_GET['load_foredit'], true).'</pre>';
    ?>
    <table class="tableBody">
        <tr>
            <?php //left_navigation
            if (USE_ADMIN_TOP_MENU == 'false') {
            echo '<td class="columnLeft2">'.PHP_EOL;
            echo '<!-- left_navigation //-->'.PHP_EOL;       
            require_once(DIR_WS_INCLUDES . 'column_left.php');
            echo '<!-- left_navigation eof //-->'.PHP_EOL; 
            echo '</td>'.PHP_EOL;      
            }
            ?>
            <td class="boxCenter">
                <div class="pageHeading pdg2 mrg5"><?php //echo xtc_image(DIR_WS_ICONS.'fastnav/icon_i18n.png', 'i18n', 44, 22); ?> i18n - internationalization</div>
                <div id="maini18n">
                    <div class="info">
                        <h2 class="green"><i class="fa"></i> <?php echo NR_LANGS_EDIT_GUIDE_HEADING ?> <i class="fa"></i></h2> <!-- <i class="fa fa-hand-o-down"></i> -->
                        <div class="tog-cont">
                            <?php echo NR_LANGS_EDIT_GUIDE_TXT; ?>
                        </div>
                    </div>
                    <div class="maini18n-buttons">
                        <div class="tog-main">
                            <h2 class=""><i class="fa"></i> <?php echo NR_LANGS_EDIT_LOAD_LANGS_HEADING; ?> <i class="fa"></i></h2>
                            <div class="tog-cont">
                                <table class="tw100">
                                    <tr class="ttr">
                                        <td class="ttd tdw30">
                                            <?php $conf_js_basic = $cle->has_basic_data !== false ? ' onsubmit="return confirm(\'NR_LANGS_EDIT_WARNING_LOAD_BASIS_JS\');"' : '';
                                            echo '<form name="basic_lang_intodb"'.$conf_js_basic.' action="'.xtc_href_link('langs_edit.php').'" method="post">';
                                            ?>
                                                <div>
                                                    <?php echo NR_LANGS_EDIT_LOAD_BASIS_LANG; ?> 
                                                    <select class="SlectBox" name="load_basic_lang">
                                                        <option value="?">-- | --</option>
                                                        <?php
                                                            foreach($nrl_basic_langs as $hl_key => $hl_val) {
                                                                echo '<option value="'.$hl_val.'">'.$hl_key.'</option>';
                                                            }
                                                        ?>
                                                    </select> 
                                                    <button class="button" type="submit"><i class="fa fa-upload"></i> <span class="which-l">?</span> <?php echo NR_LANGS_EDIT_LOAD_IN_DB_TXT; ?></button>
                                                </div>
                                                <div>
                                                    <span class="tooltip red">
                                                        <span><?php echo NR_LANGS_EDIT_ATTENTION_READ_TXT; ?> <i class="fa fa-info-circle"></i></span>
                                                        <span class="tt-cont"><?php echo NR_LANGS_EDIT_EXPL_LOAD_BASIC_TXT; ?></span>
                                                    </span> 
                                                    <?php
                                                    if($cle->has_basic_data !== false) {
                                                        echo ' - | - '.NR_LANGS_EDIT_LOADED_TXT.' '.$cle->has_basic_data;
                                                    }
                                                    ?>
                                                </div>
                                            </form>
                                        </td>
                                        <td class="ttd tdw30">
                                          <?php if($helper_lang_dir === true) { ?>
                                                <form name="helper_lang_intodb" action="<?php echo xtc_href_link('langs_edit.php'); ?>" method="post">
                                                    <div>
                                                        <?php
                                                        echo NR_LANGS_EDIT_LOAD_HELPER_LANG;
                                                        if($cle->has_basic_data !== false) {
                                                        ?>
                                                        <select class="SlectBox" name="load_helper_lang">
                                                            <option value="?">-- | --</option>
                                                            <?php
                                                                foreach($nrl_helper_langs as $hl_key => $hl_val) {
                                                                    echo '<option value="'.$hl_val.'">'.$hl_key.'</option>';
                                                                }
                                                            ?>
                                                        </select> 
                                                        <button class="button" type="submit"><i class="fa fa-upload"></i> <span class="which-l">?</span> <?php echo NR_LANGS_EDIT_LOAD_IN_DB_TXT; ?></button>
                                                        <?php
                                                        } else {
                                                        ?>
                                                        <span class="red"><?php echo NR_LANGS_EDIT_WARN_NO_BASIS_LANG; ?></span>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div>
                                                        <span class="tooltip red">
                                                            <span><?php echo NR_LANGS_EDIT_ATTENTION_READ_TXT; ?> <i class="fa fa-info-circle"></i></span>
                                                            <span class="tt-cont"><?php echo NR_LANGS_EDIT_EXPL_LOAD_HELPER_TXT; ?></span>
                                                        </span>
                                                        <?php
                                                        if(!empty($cle->has_langs_help)) {
                                                            //$cle->has_langs_help = array_map('strtoupper', $cle->has_langs_help);
                                                            echo ' - | - '.NR_LANGS_EDIT_LOADED_TXT.' '.implode(', ', $cle->has_langs_help);
                                                        }
                                                        ?>
                                                    </div>
                                                </form>
                                              <?php 
                                              } else {
                                                echo '<span class="red">'.NR_LANGS_EDIT_WARN_HELP_DIR_NOT_EXISTS.'</span>';
                                              }
                                              ?>
                                        </td>
                                        <td class="ttd tdw30">
                                            <form name="exist_lang_intodb" action="<?php echo xtc_href_link('langs_edit.php'); ?>" method="post">
                                                <div>
                                                    <?php
                                                    echo NR_LANGS_EDIT_LOAD_EXIST_LANG;
                                                    if($cle->has_basic_data !== false) {
                                                    ?>
                                                    <select class="SlectBox" name="load_exist_lang">
                                                        <option value="?">-- | --</option>
                                                        <?php
                                                            foreach($nrl_exist_langs as $hl_key => $hl_val) {
                                                                echo '<option value="'.$hl_val.'">'.$hl_key.'</option>';
                                                            }
                                                        ?>
                                                    </select> 
                                                    <button class="button" type="submit"><i class="fa fa-upload"></i> <span class="which-l">?</span> <?php echo NR_LANGS_EDIT_LOAD_IN_DB_TXT; ?></button>
                                                    <?php
                                                    } else {
                                                    ?>
                                                    <span class="red"><?php echo NR_LANGS_EDIT_WARN_NO_BASIS_LANG; ?></span>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                                <div>
                                                    <span class="tooltip red">
                                                        <span><?php echo NR_LANGS_EDIT_ATTENTION_READ_TXT; ?> <i class="fa fa-info-circle"></i></span>
                                                        <span class="tt-cont"><?php echo NR_LANGS_EDIT_EXPL_LOAD_EXIST_TXT; ?></span>
                                                    </span>
                                                    <?php
                                                    if(!empty($cle->has_langs_exist)) {
                                                        //$cle->has_langs_exist = array_map('strtoupper', $cle->has_langs_exist);
                                                        echo ' - | - '.NR_LANGS_EDIT_LOADED_TXT.' '.implode(', ', $cle->has_langs_exist);
                                                    }
                                                    ?>
                                                </div>
                                            </form>
                                        </td>
                                        <td class="ttd tdw10">
                                            <form name="del_all_db_tables" action="<?php echo xtc_href_link('langs_edit.php'); ?>" method="post">
                                                <div>
                                                    
                                                </div>
                                                <div>
                                                
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="maini18n-buttons">
                        <div class="tog-main">
                            <h2 id="l-edit" class=""><i class="fa"></i> <?php echo NR_LANGS_EDIT_LOAD_LANGS_FILETREE_HEADING; ?> <i class="fa"></i></h2>
                            <div class="tog-cont">
                                <ul>
                                    <li>Bei Klick auf die gelben Ordner öffnen/schließen sich ggfl. Unterordner und/oder Dateien in dem Ordner</li>
                                    <li>
                                        <span class="green">grün = Datei wurde bereits generiert</span> | <span class="red">rot = Datei wurde noch nicht generiert</span> | schwarz = Datei muß vorerst nicht bearbeitet werden (findet sich auch nicht im Dropdown der Datei-Auswahl unten)</li>
                                    <li>Per Klick auf einen Dateinamen kann man diese zum Bearbeiten im Dropdown "Datei bearbeiten:" unten anzeigen. (Die anzuzeigenden Sprachen müssen dann noch ausgewählt werden.)</li>
                                </ul>
                                <?php 
                                  echo $cle->build_file_tree(); 
                                  //echo '<pre>'.print_r($cle->dir_iterate(DIR_FS_DOCUMENT_ROOT.'lang/'.$cle->has_basic_data), true).'</pre>';
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="maini18n-buttons">
                        <table class="tw100">
                            <tr class="ttr">
                                <td class="ttd tdw75">
                                    <?php if(!empty($nrl_file_dropdown) && $cle->has_basic_data !== false) { ?>
                                    <form name="load_editable_langs" id="load-editable-langs" action="<?php echo xtc_href_link('langs_edit.php'); ?>" method="get">
                                        <div>
                                            <?php
                                            echo '<span class="inl-bl-elm">';
                                            echo 'Datei bearbeiten: <select class="SlectBox" name="load_file">';
                                            echo '<option value="?">-- | --</option>';
                                            foreach($nrl_file_dropdown as $fl_k => $fl_name) {
                                                $hve_selected = isset($_GET['load_file']) && $_GET['load_file'] == $fl_k ? ' selected="selected"' : '';
                                                echo '<option value="'.$fl_k.'"'.$hve_selected.'>'.$fl_name.' ('.$fl_k.')</option>';
                                            }
                                            echo '</select> ';
                                            echo '<input type="hidden" name="action" value="langs_edit">';
                                            echo '</span>';
                                            ?>
                                            <span class="inl-bl-elm"><label for="fill-empty"><?php echo NR_LANGS_EDIT_FILL_EMPTY_WITH_ENGL; ?></label> <input id="fill-empty" type="checkbox" name="fill_empty" value="on"></span>
                                            <span class="inl-bl-elm"><button class="button" type="submit"><i class="fa fa-download"></i><?php echo NR_LANGS_EDIT_SHOW_MARKED_LANGS_TO_EDIT; ?></button></span>
                                        </div>
                                        <div>
                                            <?php
                                            if(!empty($cle->has_langs_help)) {
                                                foreach($cle->has_langs_help as $k => $lng) {
                                                    $chkd = ''; //isset($_GET['load_foredit']['help']) && in_array($lng, $_GET['load_foredit']['help']) ? ' checked="checked"' : '';
                                                    echo '<label><input type="checkbox" name="load_foredit[help][]" value="'.$lng.'"'.$chkd.' class="ChkBox"><em>&nbsp;</em><span class="dred">'.$lng.' ('.NR_LANGS_EDIT_HELPINGLANG_TXT.')</span></label> &nbsp;';
                                                }
                                            }
                                            if(!empty($cle->has_langs_exist)) {
                                                foreach($cle->has_langs_exist as $k => $lng) {
                                                    $chkd = ''; //isset($_GET['load_foredit']['exists']) && in_array($lng, $_GET['load_foredit']['exists']) ? ' checked="checked"' : '';
                                                    echo '<label><input type="checkbox" name="load_foredit[exists][]" value="'.$lng.'"'.$chkd.' class="ChkBox"><em>&nbsp;</em><span class="dgreen">'.$lng.' ('.NR_LANGS_EDIT_EXISTINGLANG_TXT.')</span></label> &nbsp;';
                                                }
                                            }
                                            /*if(!empty($cle->has_langs_transl)) {
                                                foreach($cle->has_langs_transl as $k => $lng) {
                                                    $chkd = ''; //isset($_GET['load_foredit']['transl']) && in_array($lng, $_GET['load_foredit']['transl']) ? ' checked="checked"' : '';
                                                    echo '<label><input type="checkbox" name="load_foredit[transl][]" value="'.$lng.'"'.$chkd.' class="ChkBox"><em>&nbsp;</em><span class="orange">'.$lng.' (wurde bereits bearbeitet)<span></label> &nbsp;';
                                                }
                                            }*/
                                            ?>
                                        </div>
                                        <div>
                                            <span class="tooltip red">
                                                <span><?php echo NR_LANGS_EDIT_ATTENTION_READ_TXT; ?> <i class="fa fa-info-circle"></i></span>
                                                <span class="tt-cont"><?php echo NR_LANGS_EDIT_EXPL_SHOW_LANGS_TXT; ?></span>
                                            </span>
                                        </div>
                                    </form>
                                    <?php } ?>
                                </td>
                                <td class="ttd tdw25">
                                    <?php
                                    if((isset($_GET['load_file']) && !empty($_GET['load_file'])) && (isset($_GET['load_foredit']) && !empty($_GET['load_foredit']))) {
                                        $generate_file_params = http_build_query($_GET);
                                    ?>
                                    <div>
                                        <form name="generate_transl_file" action="<?php echo xtc_href_link('langs_edit.php', $generate_file_params); ?>" method="post">
                                            <input type="hidden" name="gener_files" value="<?php echo (int)$_GET['load_file']; ?>">
                                            <button class="button" type="submit"><i class="fa fa-save"></i><?php echo NR_LANGS_EDIT_GENERATE_FILES; ?></button>
                                        </form>
                                    </div>
                                    <div>
                                        <span class="tooltip red">
                                            <span><?php echo NR_LANGS_EDIT_ATTENTION_READ_TXT; ?> <i class="fa fa-info-circle"></i></span>
                                            <span class="tt-cont"><?php echo NR_LANGS_EDIT_EXPL_GENERATE_FILES; ?></span>
                                        </span>
                                     </div>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php
                    if(isset($_GET['load_file']) && !empty($_GET['load_file'])) {
                        $heading_path = $cle->give_path_of_edited_file($_GET['load_file']);
                        if(isset($_GET['load_foredit']) && !empty($_GET['load_foredit'])) {
                            $edit_arr = $cle->display_langs_to_edit($_GET['load_file'], $_GET['load_foredit']);
                            //echo '<pre>'.print_r($cle->display_langs_to_edit($_GET['load_file'], $_GET['load_foredit']), true).'</pre>';
                            $save_params = http_build_query($_GET);
                            //$save_params = str_replace('help', 'transl', $save_params);
                    ?>
                    <div class="maini18n-edit-langs">
                        <form name="save_transl_langs" id="save-transl-langs" action="<?php echo xtc_href_link('langs_edit.php', $save_params); ?>" method="post">
                        <h3><?php echo $heading_path; ?> &nbsp;<button class="button" type="submit"><i class="fa fa-save"></i><?php echo NR_LANGS_EDIT_SAVE_TRANSL_TO_DB; ?></button></h3>
                        <table class="tw100">
                            <?php
                            //echo '<pre>'.print_r($edit_arr, true).'</pre>';
                            $cnt_lines = 1;
                            foreach($edit_arr as $line => $data_arr) {
                                if($cnt_lines == 1) {
                                    $how_many_langs = sizeof($data_arr) - 3;
                                    $how_many_fields = $how_many_langs + 1;
                                    $perc = round(100/$how_many_fields);
                                    
                                    $dat_keys = array_keys($data_arr);
                                    
                                    echo '<tr class="ttr">';
                                    foreach($dat_keys as $key) {
                                        if($key != 'nr_lt_id' && $key != 'nr_lt_line_no' && $key != 'nr_lt_file_id') {
                                            echo '<th>'.$key.'</th>';
                                        }
                                    }
                                    echo '</tr>';
                                }
                                $cnt_lines++;
                            ?>
                            <tr class="ttr" data-line="<?php echo $line; ?>">
                                <?php
                                $temp_out_field = '';
                                foreach($data_arr as $dat_key => $dat) {
                                    if($dat_key == 'nr_lt_line_no') {
                                        $temp_out_field .= '<input type="hidden" name="transl_lang['.$line.'][nr_lt_line_no]" value="'.$dat.'">';
                                    } else if($dat_key == 'nr_lt_file_id') {
                                        $temp_out_field .= '<input type="hidden" name="transl_lang['.$line.'][nr_lt_file_id]" value="'.$dat.'">';
                                    } else if($dat_key == 'nr_lt_var') {
                                        $out_field = '<input type="text" readonly="readonly" value="'.$dat.'">'.$temp_out_field;
                                    } else if(strpos($dat_key, 'base') !== false) {
                                        $out_field = '<textarea readonly="readonly">'.$dat.'</textarea>';
                                    } else if(strpos($dat_key, 'help') !== false) {
                                        $dat_lang_arr = explode('_', $dat_key);
                                        $dat_lang = $dat_lang_arr['1'];
                                        $out_field = '<textarea name="transl_lang['.$line.']['.$dat_lang.']">'.(($dat == '' && isset($_GET['fill_empty']) && $_GET['fill_empty'] == 'on') ? $data_arr['exists_english'] : $dat).'</textarea>';
                                    } else if(strpos($dat_key, 'exists') !== false) {
                                        $out_field = '<textarea readonly="readonly">'.$dat.'</textarea>';
                                    } else if(strpos($dat_key, 'transl') !== false) {
                                        $dat_lang_arr = explode('_', $dat_key);
                                        $dat_lang = $dat_lang_arr['1'];
                                        $out_field = '<textarea name="transl_lang['.$line.']['.$dat_lang.']">'.$dat.'</textarea>';
                                    }

                                    if($dat_key != 'nr_lt_id' && $dat_key != 'nr_lt_line_no' && $dat_key != 'nr_lt_file_id') {
                                        echo '<td class="ttd tdw'.$perc.'">'.$out_field.'</td>';
                                    }
                                }
                                ?>
                            </tr>
                            <?php
                            }
                            ?>
                        </table>
                        <h3><?php echo $heading_path; ?> &nbsp;<button class="button" type="submit"><i class="fa fa-save"></i><?php echo NR_LANGS_EDIT_SAVE_TRANSL_TO_DB; ?></button></h3>
                        <button class="button b-static" type="submit" title="<?php echo NR_LANGS_EDIT_SAVE_TRANSL_TO_DB; ?>"><i class="fa fa-save"></i></button>
                        </form>
                    </div>
                    <?php
                        }
                    }
                    ?>
                </div>
            </td>
        </tr>
    </table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<br />
<?php //BOC save edited languages (session timeout cheater) ?>
<script>
//BOC scroll to position
function scrollToTop(a) {
    var off_up = $('#fixed-header').height() + 10;
    setTimeout(function() {
        var b = parseInt($(a).offset().top),
            c = b - off_up;
        a.scrollTop() != c && $("html, body").animate({
            scrollTop: c
        }, 500)
    }, 400)
}
//EOC scroll to position
//BOC success alert function
function success_alert(dataval, fo = true) {
    $('#green-alert .inner-alert').html(dataval);
    if($('#green-alert').is(':visible')) $('#green-alert').hide();
    $('#green-alert').fadeIn();
    if(fo === true) $('#green-alert').delay(1500).fadeOut();
}
//EOC success alert function
<?php
if((isset($_GET['action']) && $_GET['action'] == 'langs_edit') && (isset($_GET['load_foredit']) && is_array($_GET['load_foredit']))) {
?>
$(function() {
    var $wrk_gif = $('.working-gif');

    setInterval(function() {
        $('#save-transl-langs').submit();
    }, 600000); //10000 600000

    $('#save-transl-langs').on('submit', function(e) {
        e.preventDefault();
        $wrk_gif.show();
        var $post_dat = $(this).find('[name^="transl_lang"]').serialize();

        $.post("langs_edit.php",
            {
            transl_lang_data: $post_dat
            },
            function(response){
                //$('#save-transl-langs').prepend(response);
                //var data = JSON.parse(response);
                //console.log(response);
                //console.log(data);
                /*$.each(data, function(line, values) {
                    for(var key in values) {
                        if(key != 'nr_lt_line_no' && key != 'nr_lt_file_id') {
                            //console.log(key+': '+values[key]+"\n");
                            $('textarea[name^="transl_lang['+line+']['+key+']"]').val(values[key]);
                        }
                    }
                });*/ //we don't need this, browser doesn't reload, it's ajax, idiot :-D{
                success_alert(response);
                $wrk_gif.hide();
            }
        );
    });
});
<?php
}
?>
</script>
<?php //EOC save edited languages (session timeout cheater) ?>
<script>
$(function() {
    var $wrk_gif = $('.working-gif');

    //toggle info boxes
    $('.info h2, .tog-main h2').click(function() {
        $(this).toggleClass('open').next('.tog-cont').slideToggle();
        scrollToTop($(this));
    });
    
    //toggle files tree elements
    $('.nr-tree-h td:not(.root-td)').next('td').css('display', 'none');
    $('.nr-tree-h td.f-td span').click(function(e) {
        e.preventDefault();
        $(this).parent('td').next('td').toggle();
        scrollToTop($('#l-edit'));
    });
    
    //place file from file tree as selected in file dropdown
    $('.nr-tree-h td span.red-sp, .nr-tree-h td span.green-sp').on('click', function() {
        var flid = $(this).data('file'),
            $drpd = $('select[name="load_file"]');
        
        $drpd.val(flid).change();
        scrollToTop($('#load-editable-langs'));
    });
    
    //toggle language name on button text
    $('select[name="load_basic_lang"], select[name="load_helper_lang"], select[name="load_exist_lang"]').on('change', function() {
        //console.log('Sprache: '+$(this).val());
        $(this).next('button').find('.which-l').text($(this).val().toUpperCase()); //.css('textTransform', 'capitalize')
    });
    
    //close alert
    $('.cl-alert').on('click', function() {
        $('#green-alert').fadeOut();
    });

    //test file
    $('.test-file-butt').on('click', function() {
        var file_path = $(this).data('testpath'),
            file_no_el = $(this).prev('.green-sp'),
            file_no = file_no_el.data('file');
        $wrk_gif.show();

        $.post("langs_edit_ajax.php",
            {
            test_file: ""+file_path+"",
            test_file_no: ""+file_no+""
            },
            function(response){
                success_alert(response, false);
                $wrk_gif.hide();
            }
        );
    });
});

//BOC conf
var ss = 220; //start to show when x px scrolled down
var fi = 1000; //fade in time
var st = 0; //scroll to
var sti = 800; //scroll time
//EOC conf
function getIEversion(){var a=-1;if("Microsoft Internet Explorer"==navigator.appName){var b=navigator.userAgent,c=new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");null!=c.exec(b)&&(a=parseFloat(RegExp.$1))}return a}$(function(){$(window).scroll(function(){var a=$(this).scrollTop(),b=getIEversion();b>-1&&8>=b?a>ss?$("#to-top").show():$("#to-top").hide():a>ss?$("#to-top").fadeIn(fi):$("#to-top").fadeOut(fi)}),$("#to-top").click(function(){$("body,html").animate({scrollTop:st},sti)})});
</script>
<!-- <img class="working-gif" src="<?php echo DIR_WS_ICONS; ?>loading.gif" alt="loading" /> -->
<div id="green-alert"><span class="inner-alert"></span><span class="cl-alert"><i class="fa fa-close"></i></span></div>
<i class="fa fa-spinner working-gif"></i>
<button id="to-top" class="button b-static" type="button"><i class="fa fa-arrow-up"></i></button>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
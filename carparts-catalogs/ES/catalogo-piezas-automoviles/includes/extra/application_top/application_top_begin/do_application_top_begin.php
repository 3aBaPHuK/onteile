<?php
/*******************************************
* file: do_application_top_begin.php
* use: define was prices
* (c) noRiddle 01-2017
* 1 - 1, 2 - 1, 6 - 1, 8 - 8, 9 - 8, 10 - 8, 11 - 1, 12 - 1, 13 - 8, 14 - 8, 17 - 1

Zur Kontrolle:

    Kundengruppen '3','4','5','16' haben und sehen Preisgegenüberstellung aus KG 1
    Kundengruppen '7','15' haben und sehen Preisgegenüberstellung aus KG 8
    Kundengruppen '1', '2', '6', '11', '12', '17' haben aber sehen nicht Preisgegenüberstellung aus KG 1
    Kundengruppen '8', '9', '10', '13', '14' haben aber sehen nicht Preisgegenüberstellung aus KG 8
    Falleback (theoretisch): Falls irgend etwas nicht zuordbar ist: haben und sehen nicht Preisgegenüberstellung aus KG 10
*******************************************/

//BOC new was-is-prices
$groups_show_other_price_arr_1 = array('3','4','5','16'); //should get and show normal price from customer status 1
$groups_show_other_price_arr_8 = array('7','15'); //should get and show normal price from customer status 8
$groups_have_other_price_arr_1 = array('1', '2', '6', '11', '12', '17'); //should get and NOT show normal price from customer status 1
$groups_have_other_price_arr_8 = array('8', '9', '10', '13', '14'); //should get and NOT show normal price from customer status 8
//EOC new was-is-prices

//BOC catalogue for BMW group
$bmw_group_shops_ctlg = array('bmw'.$shop_suffix, 'bmw-motorrad'.$shop_suffix, 'mini'.$shop_suffix, 'ssangyong'.$shop_suffix);
//EOC catalogue for BMW group
//BOC build array to link to shop internal catalogue for bmw, mini and rolls-royce
$int_cat_arr = array('bmw', 'bmw-motorrad', 'mini');
//EOC build array to link to shop internal catalogue for bmw, mini and rolls-royce
?>
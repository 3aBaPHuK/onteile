<?php
/* --------------------------------------------------------------
   $Id: index.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(index.php,v 1.5 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (index.php,v 1.5 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Choisir l\'option...');

define('BOX_TITLE_ORDERS', 'Commandes');
define('BOX_TITLE_STATISTICS', 'Statistiques');

define('BOX_ENTRY_SUPPORT_SITE', 'Page d\'assistance');
define('BOX_ENTRY_SUPPORT_FORUMS', 'Forum d\'assistance');
define('BOX_ENTRY_MAILING_LISTS', 'Listes de publipostage');
define('BOX_ENTRY_BUG_REPORTS', 'Rapports d\'erreurs');
define('BOX_ENTRY_FAQ', 'Questions et réponses');
define('BOX_ENTRY_LIVE_DISCUSSIONS', 'Discussion en direct');
define('BOX_ENTRY_CVS_REPOSITORY', 'CVS Repository');
define('BOX_ENTRY_INFORMATION_PORTAL', 'Portail d\'information');

define('BOX_ENTRY_CUSTOMERS', 'Clients:');
define('BOX_ENTRY_PRODUCTS', 'Articles:');
define('BOX_ENTRY_REVIEWS', 'Critiques:');

define('BOX_CONNECTION_PROTECTED', 'Vous êtes sécurisé par une connexion SSL %s sécurisée.');
define('BOX_CONNECTION_UNPROTECTED', 'Vous êtes <span class="col-red">nicht</span> sécurisé par une connexion SSL sécurisée.');
define('BOX_CONNECTION_UNKNOWN', 'inconnu');

define('BOX_HEADING_CONFIGURATION', 'Configuration');
define('BOX_CONFIGURATION_MYSTORE', 'Ma boutique');
define('BOX_CONFIGURATION_LOGGING', 'Informations de connexion');
define('BOX_CONFIGURATION_CACHE', 'Options de cache');

define('BOX_MODULES_PAYMENT', 'Mode de paiement');
define('BOX_MODULES_SHIPPING', 'Mode de livraison');

define('BOX_CATALOG_MANUFACTURERS', 'Fabricant');

define('BOX_CUSTOMERS_CUSTOMERS', 'Clients');
define('BOX_CUSTOMERS_ORDERS', 'Commandes');
define('BOX_CUSTOMERS_ACCOUNTING', 'Décompte');

define('BOX_TAXES_COUNTRIES', 'Pays');
define('BOX_TAXES_GEO_ZONES', 'Zone d\'imposition');

define('BOX_LOCALIZATION_CURRENCIES', 'Devises');
define('BOX_LOCALIZATION_LANGUAGES', 'Langues');

define('CATALOG_CONTENTS', 'Contenu');

define('REPORTS_PRODUCTS', 'Articles');
define('REPORTS_ORDERS', 'Commandes');

define('TOOLS_BACKUP', 'Sécurisation de données');
define('TOOLS_BANNERS', 'Gestion de bannières');
define('TOOLS_FILES', 'Gestion de données');
?>

<?php
/**
 * Sprachdatei für globale Kundengruppenberechtigungen
 * @author André Estel
 * @copyright 2008 Estelco
 * @license GPLv2
 */

define('HEADING_TITLE', 'Établir des droits des groupes de clients');

define('TEXT_CATEGORIES', 'Catégories');
define('TEXT_PRODUCTS', 'Produits');
define('TEXT_CONTENT', 'Contenu de page (gestionnaire du contenu)');
define('TEXT_PRODUCTS_CONTENT', 'Contenu d\'articles (gestionnaire du contenu)');
define('TEXT_PERMISSION', 'Autorisation');
define('TEXT_SET', 'donner');
define('TEXT_UNSET', 'priver de');
define('TEXT_SEND', 'Envoyer');

define('ERROR_PLEASE_SELECT_CUSTOMER_GROUP', 'Veuillez choisir au moins un groupe de clients');
define('ERROR_PLEASE_SELECT_SHOP_AREA', 'Veuillez choisir au moins un espace de boutique');
define('TEXT_CATEGORIES_SUCCESSFULLY_SET', 'Autorisations de catégorie établis avec succès.');
define('TEXT_CATEGORIES_SUCCESSFULLY_UNSET', 'Autorisations de catégorie supprimé avec succès');
define('TEXT_PRODUCTS_SUCCESSFULLY_SET', 'Autorisations de produit établis avec succès.');
define('TEXT_PRODUCTS_SUCCESSFULLY_UNSET', 'Autorisations de produit supprimé avec succès');
define('TEXT_CONTENT_SUCCESSFULLY_SET', 'Autorisations de contenu de page établis avec succès.');
define('TEXT_CONTENT_SUCCESSFULLY_UNSET', 'Autorisations de contenu de page supprimé avec succès');
define('TEXT_PRODUCTS_CONTENT_SUCCESSFULLY_SET', 'Autorisations de contenu d\'articles établis avec succès.');
define('TEXT_PRODUCTS_CONTENT_SUCCESSFULLY_UNSET', 'Autorisations de contenu d\'articles supprimé avec succès');
?>

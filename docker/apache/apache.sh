#! /bin/bash

sed -i -e "s/\[VIRTUALHOST\]/$VIRTUALHOST/g" /etc/apache2/sites-available/onteile.conf

a2enmod proxy
a2enmod proxy_fcgi
a2enmod fcgid
a2enconf servername
a2ensite onteile.conf

/usr/sbin/apache2ctl -D FOREGROUND

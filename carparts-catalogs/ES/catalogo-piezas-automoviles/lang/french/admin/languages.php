<?php
/* --------------------------------------------------------------
   $Id: languages.php 5011 2013-07-04 15:22:37Z web28 $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(languages.php,v 1.10 2002/01/19); www.oscommerce.com 
   (c) 2003	 nextcommerce (languages.php,v 1.5 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Langues');

define('TABLE_HEADING_LANGUAGE_NAME', 'Langue');
define('TABLE_HEADING_LANGUAGE_CODE', 'Codage');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Veuillez faire tous les changements nécessaires');
define('TEXT_INFO_LANGUAGE_NAME', 'Nom:');
define('TEXT_INFO_LANGUAGE_CODE', 'Codage:');
define('TEXT_INFO_LANGUAGE_IMAGE', 'Symbole:');
define('TEXT_INFO_LANGUAGE_DIRECTORY', 'Répértoire:');
define('TEXT_INFO_LANGUAGE_SORT_ORDER', 'Ordre de tri:');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez entrer la nouvelle langue avec toutes les données pertinentes.');
define('TEXT_INFO_DELETE_INTRO', 'Êtes-vous sûr de vouloir supprimer l\'ancienne langue?');
define('TEXT_INFO_HEADING_NEW_LANGUAGE', 'Nouvelle langue');
define('TEXT_INFO_HEADING_EDIT_LANGUAGE', 'Editer la langue');
define('TEXT_INFO_HEADING_DELETE_LANGUAGE', 'Supprimer la langue');
define('TEXT_INFO_LANGUAGE_CHARSET', 'Charset');
define('TEXT_INFO_LANGUAGE_CHARSET_INFO', 'meta-content');

define('ERROR_REMOVE_DEFAULT_LANGUAGE', 'Erreur: La langue par défaut ne peut pas être supprimée. Veuillez définir une nouvelle langue par défaut et réessayez.');

define('TEXT_INFO_LANGUAGE_STATUS', 'Statut:');
define('TABLE_HEADING_LANGUAGE_STATUS', 'Statut:');
define('TEXT_INFO_LANGUAGE_STATUS_ADMIN', 'Statut administrateur:');
define('TABLE_HEADING_LANGUAGE_STATUS_ADMIN', 'Statut administrateur:');

define('TEXT_LANGUAGE_TRANSFER_INFO', 'Transfert des tableaux de langue suivants');
define('TEXT_LANGUAGE_TRANSFER_BTN', 'Transfert');
define('TEXT_LANGUAGE_TRANSFER_FROM', 'De ');
define('TEXT_LANGUAGE_TRANSFER_TO', ' vers ' );
define('TEXT_LANGUAGE_TRANSFER_OK', 'Transferé avec succès!');
define('TEXT_LANGUAGE_TRANSFER_ERR', 'Veuillez choisir des langues différentes!');
define('TEXT_LANGUAGE_TRANSFER_INFO2', '<strong>ATTENTION:</strong> Lors du transfert les textes existants seront écrasés, veuillez donc sécuriser la base de données en amont!');
?>

<?php
/* --------------------------------------------------------------
   $Id: products_attributes.php 1101 2005-07-24 14:51:13Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(products_attributes.php,v 1.9 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (products_attributes.php,v 1.4 2003/08/1); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE_OPT', 'Características del artículo');
define('HEADING_TITLE_VAL', 'Valor de la opción');
define('HEADING_TITLE_ATRIB', 'Características del artículo');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_PRODUCT', 'Nombre del artículo');
define('TABLE_HEADING_OPT_NAME', 'Nombre de la opción');
define('TABLE_HEADING_OPT_VALUE', 'Valor de la opción');
define('TABLE_HEADING_OPT_PRICE', 'Precio');
define('TABLE_HEADING_OPT_PRICE_PREFIX', 'Signo (+/-)');
define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_DOWNLOAD', 'Artículos descargables:');
define('TABLE_TEXT_FILENAME', 'Nombre de archivo:');
define('TABLE_TEXT_MAX_DAYS', 'Período de tiempo:');
define('TABLE_TEXT_MAX_COUNT', 'Número máximo de descargas:');

define('MAX_ROW_LISTS_OPTIONS', 10);

define('TEXT_WARNING_OF_DELETE', 'Esta opción está asociada a artículos y características de la opción; no se recomienda borrarla.');
define('TEXT_OK_TO_DELETE', 'Esta opción no está asociada a artículos y características de la opción; se puede borrarla.');
define('TEXT_SEARCH', 'Búsqueda:');
define('TEXT_OPTION_ID', 'ID de la opción');
define('TEXT_OPTION_NAME', 'Nombre de la opción');

// BOF - Tomcraft - 2009-11-07 - Added sortorder to products_options
define('TABLE_HEADING_SORTORDER', 'Clasificación');
define('TEXT_SORTORDER', 'Clasificación');
// EOF - Tomcraft - 2009-11-07 - Added sortorder to products_options
define('TEXT_OPTION_ID_FILTER', 'Filtro:');
?>

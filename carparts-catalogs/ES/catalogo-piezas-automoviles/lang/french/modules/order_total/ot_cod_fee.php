<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_cod_fee.php 10553 2017-01-11 13:45:14Z web28 $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ot_cod_fee.php,v 1.02 2003/02/24); www.oscommerce.com
   (C) 2001 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_COD_FEE_TITLE', 'Tarif de contre-remboursement');
define('MODULE_ORDER_TOTAL_COD_FEE_DESCRIPTION', 'Calcul du tarif de contre-remboursement');

define('MODULE_ORDER_TOTAL_COD_FEE_STATUS_TITLE', 'Tarif de contre-remboursement');
define('MODULE_ORDER_TOTAL_COD_FEE_STATUS_DESC', 'Calcul du tarif de contre-remboursement');

define('MODULE_ORDER_TOTAL_COD_FEE_SORT_ORDER_TITLE', 'Ordre de triage');
define('MODULE_ORDER_TOTAL_COD_FEE_SORT_ORDER_DESC', 'Ordre de l\'affichage');

define('MODULE_ORDER_TOTAL_COD_FEE_TAX_CLASS_TITLE', 'Catégorie fiscale');
define('MODULE_ORDER_TOTAL_COD_FEE_TAX_CLASS_DESC', 'Sélectionnez une catégorie fiscale');

  function define_shipping_titles_cod() {
    $module_keys = str_replace('.php','',MODULE_SHIPPING_INSTALLED);
    $installed_shipping_modules = explode(';',$module_keys);
    //support for ot_shipping
    $installed_shipping_modules[] = 'free';

    if (count($installed_shipping_modules) > 0) {
      foreach($installed_shipping_modules as $shipping_code) {
        $module_type = 'shipping';
        $file = $shipping_code.'.php';
        $shipping_code = strtoupper($shipping_code);
        $title = '';

        if (defined('DIR_FS_LANGUAGES') && file_exists(DIR_FS_LANGUAGES . 'german/modules/' . $module_type . '/' . $file)) {
            include_once(DIR_FS_LANGUAGES . 'german/modules/' . $module_type . '/' . $file);
            $title = constant('MODULE_SHIPPING_'.$shipping_code.'_TEXT_TITLE');
        }
        //support for ot_shipping
        $title = $shipping_code == 'FREE' ? 'Versandkostenfrei (Zusammenfassung Modul ot_shipping)' : $title;
        
        $shipping_code = ($shipping_code == 'FREEAMOUNT') ? 'FREEAMOUNT_FREE' : 'FEE_' . $shipping_code;

define('MODULE_ORDER_TOTAL_COD_'.$shipping_code.'_TITLE', $title);
define('MODULE_ORDER_TOTAL_COD_'.$shipping_code.'_DESC', '<ISO2-Code>:<Prix>, ....<br />
        00 comme Code ISO2 permet la livraison par contre-remboursement dans tous les pays. Si
        00 est utilisé, il faut que celui-ci soit indiqué comme dernier argument. Si
       00:9.99 n\'est pas indiqué, la livraison par contre-remboursement à l\'étranger ne sera pas facturée
        (pas possible). Pour exclure un pays, n\'indiquez pas de frais pour ce pays
        Exemple: DE:4.00,CH:,00:9.99<br />-> Explication: Livraison vers DE : 4€ /
        Livraison vers CH : pas possible / Livraison vers le reste du monde : 9,99€');
      }
    }
  }
  define_shipping_titles_cod();
?>

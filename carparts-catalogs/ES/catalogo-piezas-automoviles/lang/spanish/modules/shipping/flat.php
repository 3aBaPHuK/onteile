<?php
/* -----------------------------------------------------------------------------------------
   $Id: flat.php 4200 2013-01-10 19:47:11Z Tomcraft1980 $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(flat.php,v 1.6 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (flat.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_FLAT_TEXT_TITLE', 'Tarifa plana de costos de envío');
define('MODULE_SHIPPING_FLAT_TEXT_DESCRIPTION', 'Tarifa plana de costos de envío');
define('MODULE_SHIPPING_FLAT_TEXT_WAY', 'El mejor camino');

define('MODULE_SHIPPING_FLAT_STATUS_TITLE' , 'Activar Tarifa plana de costos de envío');
define('MODULE_SHIPPING_FLAT_STATUS_DESC' , '¿Desea ofrecer Tarifa plana de costos de envío?');
define('MODULE_SHIPPING_FLAT_ALLOWED_TITLE' , 'Zonas de envío permitidas');
define('MODULE_SHIPPING_FLAT_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE (deje este campo vacío si desea permitir todas las zonas))');
define('MODULE_SHIPPING_FLAT_COST_TITLE' , 'Costos de envío');
define('MODULE_SHIPPING_FLAT_COST_DESC' , 'Costos de envío para todos los pedidos bajo este método de envío.');
define('MODULE_SHIPPING_FLAT_TAX_CLASS_TITLE' , 'Zona de impuestos');
define('MODULE_SHIPPING_FLAT_TAX_CLASS_DESC' , 'Aplicar la siguiente clase de impuestos a los costos de envío.');
define('MODULE_SHIPPING_FLAT_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_FLAT_ZONE_DESC' , 'Si se ha seleccionado una zona, este método de envío se utilizará sólo para esa zona.');
define('MODULE_SHIPPING_FLAT_SORT_ORDER_TITLE' , 'Orden de clasificación');
define('MODULE_SHIPPING_FLAT_SORT_ORDER_DESC' , 'Orden de la visualización');
?>

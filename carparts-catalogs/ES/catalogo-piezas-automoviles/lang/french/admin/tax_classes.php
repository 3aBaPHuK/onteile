<?php
/* --------------------------------------------------------------
   $Id: tax_classes.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(tax_classes.php,v 1.5 2002/01/28); www.oscommerce.com 
   (c) 2003	 nextcommerce (tax_classes.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Catégories fiscales');

define('TABLE_HEADING_TAX_CLASSES', 'Catégories fiscales');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Veuillez faire tous les changements nécessaires');
define('TEXT_INFO_CLASS_TITLE', 'Nom de la catégorie fiscale');
define('TEXT_INFO_CLASS_DESCRIPTION', 'Description');
define('TEXT_INFO_DATE_ADDED', 'Ajouté le:');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez saisir la nouvelle catégorie fiscale avec toutes les données pertinentes');
define('TEXT_INFO_DELETE_INTRO', 'Êtes-vous sûr de voulour supprimer cette catégorie fiscale?');
define('TEXT_INFO_HEADING_NEW_TAX_CLASS', 'Nouvelle catégorie fiscale');
define('TEXT_INFO_HEADING_EDIT_TAX_CLASS', 'Editer la catégorie fiscale');
define('TEXT_INFO_HEADING_DELETE_TAX_CLASS', 'Supprimer la catégorie fiscale');
?>

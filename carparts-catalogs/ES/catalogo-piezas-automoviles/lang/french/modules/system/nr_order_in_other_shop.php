<?php
/************************************************************
* file: nr_order_in_other_shop.php
* use: language file for system file
* (c) noRiddle 04-2018
************************************************************/

define('MODULE_NR_ORDER_IN_OTHER_SHOP_TITLE', 'Commander dans une autre boutique');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_DESCRIPTION', 'Ce module sert à commander des produits dans le panier d\'une autre boutique (p.ex. lors des difficultés logistiques).');

define('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS_TITLE', 'Statut');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS_DESC', 'Activer module ?');

define('MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP_TITLE', 'Dans quelle boutique souhaitez-vous effectuer la commande ?');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP_DESC', 'Veuillez saisir ici le nom de la boutique uniquement sans des suffixes comme "-ersatzteile" ou "-spare-parts".<br />exemples : skoda, bmw, bmw-motorrad, chrysler-dodge-jeep-ram');

$nr_oios_lngs = xtc_get_languages();
for($oios = 0, $coios = count($nr_oios_lngs); $oios < $coios; $oios++) {
define('MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_'.strtoupper($nr_oios_lngs[$oios]['code']).'_TITLE', xtc_image(DIR_WS_LANGUAGES . $nr_oios_lngs[$oios]['directory'] .'/admin/images/'. $nr_oios_lngs[$oios]['image'], $nr_oios_lngs[$oios]['name']).' Texte d\'explication sur bouton "payer" '.strtoupper($nr_oios_lngs[$oios]['code']));
define('MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_'.strtoupper($nr_oios_lngs[$oios]['code']).'_DESC', 'Saisissez le texte (explication) pourquoi vous souhaitez compléter la commande dans une autre boutique.<br />Utiliser <i>%s</i> comme caractère de remplacement pour la boutique indiquée ci-dessus.<br /><span style="display:inline-block; font-size:11px; line-height:12px;“>(Chaque texte peut être sauvegardé avec la fonction backup.<br />Lors d’une réinstallation et non-restauration du backup le texte standard réapparaîtra de nouveau ici.)</span>');
}
?>

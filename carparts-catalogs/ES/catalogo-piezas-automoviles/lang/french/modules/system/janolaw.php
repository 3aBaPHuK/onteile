<?php
/* -----------------------------------------------------------------------------------------
   $Id: janolaw.php 2011-11-24 modified-shop $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com
   (c) 2003   nextcommerce (invoice.php,v 1.6 2003/08/24); www.nextcommerce.org
   (c) 2005 XT-Commerce - community made shopping http://www.xt-commerce.com ($Id: billiger.php 950 2005-05-14 16:45:21Z mz $)
   (c) 2008 Gambio OHG (billiger.php 2008-11-11 gambio)

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_JANOLAW_TEXT_TITLE', 'janolaw conditions générales service d\'hébergement');
define('MODULE_JANOLAW_TEXT_DESCRIPTION', '<a href="http://www.janolaw.de/internetrecht/agb/agb-hosting-service/modified/index.html?partnerid=8764#menu" target="_blank"><img src="images/janolaw/janolaw_185x35.png" border=0></a><br /><br />janolow, un grand portail juridique allemand offre des solutions sur mesure pour vos questions juridiques, que ce soit la hotline de l’avocat ou des contrats individuels avec garantie d\'avocat. Avec les conditions générales service d’hébergement pour des boutiques en ligne vous pouvez individuellement adapter des documents de base conditions générales, révocation, mentions légales et protection de données sur votre boutique et faire actualiser par l\'équipe janolow. Il n\'y a pas plus de protection.<br /><br /><a href="http://www.janolaw.de/internetrecht/agb/agb-hosting-service/modified/index.html?partnerid=8764#menu" target="_blank"><strong><u>Ici notre offre<u></strong></a>');
define('MODULE_JANOLAW_USER_ID_TITLE', '<hr noshade>ID utilisateur');
define('MODULE_JANOLAW_USER_ID_DESC', 'Votre ID utilisateur');
define('MODULE_JANOLAW_SHOP_ID_TITLE', 'ID boutique');
define('MODULE_JANOLAW_SHOP_ID_DESC', 'L\'ID de votre boutique en ligne');
define('MODULE_JANOLAW_STATUS_DESC', 'Activer module ?');
define('MODULE_JANOLAW_STATUS_TITLE', 'Statut');
define('MODULE_JANOLAW_TYPE_TITLE', '<hr noshade>Enregistrer sous');
define('MODULE_JANOLAW_TYPE_DESC', 'Enregistrer les données dans un fichier ou dans la base de données ?');
define('MODULE_JANOLAW_FORMAT_TITLE', 'Type format');
define('MODULE_JANOLAW_FORMAT_DESC', 'Enregistrer les données comme texte ou comme HTML ?');
define('MODULE_JANOLAW_UPDATE_INTERVAL_TITLE', '<hr noshade>Mettre à jour intervalle');
define('MODULE_JANOLAW_UPDATE_INTERVAL_DESC', 'Mettre à jour les données dans quelle intervalle ?');
define('MODULE_JANOLAW_ERROR', 'Veuillez vérifier l\'attribution des documents.');

define('MODULE_JANOLAW_TYPE_DATASECURITY_TITLE', '<hr noshade>Texte juridique protection de données');
define('MODULE_JANOLAW_TYPE_DATASECURITY_DESC', 'Veuillez indiquer dans quelle page vous souhaitez insérer le texte juridique automatiquement.');
define('MODULE_JANOLAW_PDF_DATASECURITY_TITLE', 'Télécharger en format PDF');
define('MODULE_JANOLAW_PDF_DATASECURITY_DESC', 'Souhaitez-vous enregistrer les données en plus en format PDF et ajouter un lien ?<br/><b>Attention :</b> Ceci fonctionne dans la version HTML uniquement !');
define('MODULE_JANOLAW_MAIL_DATASECURITY_TITLE', 'PDF en pièce jointe');
define('MODULE_JANOLAW_MAIL_DATASECURITY_DESC', 'Souhaitez-vous que le PDF en pièce jointe sera envoyé pour la confirmation de la commande ?');

define('MODULE_JANOLAW_TYPE_TERMS_TITLE', '<hr noshade>Texte juridique Conditions légales');
define('MODULE_JANOLAW_TYPE_TERMS_DESC', 'Veuillez indiquer dans quelle page vous souhaitez insérer le texte juridique automatiquement.');
define('MODULE_JANOLAW_PDF_TERMS_TITLE', 'PDF en téléchargement');
define('MODULE_JANOLAW_PDF_TERMS_DESC', 'Souhaitez-vous enregistrer les données en plus en format PDF et ajouter un lien ?<br/><b>Attention :</b> Ceci fonctionne dans la version HTML uniquement !');
define('MODULE_JANOLAW_MAIL_TERMS_TITLE', 'PDF en pièce jointe');
define('MODULE_JANOLAW_MAIL_TERMS_DESC', 'Souhaitez-vous que le PDF en pièce jointe sera envoyé pour la confirmation de la commande ?');

define('MODULE_JANOLAW_TYPE_LEGALDETAILS_TITLE', '<hr noshade>Texte juridique mentions légales');
define('MODULE_JANOLAW_TYPE_LEGALDETAILS_DESC', 'Veuillez indiquer dans quelle page vous souhaitez insérer le texte juridique automatiquement.');
define('MODULE_JANOLAW_PDF_LEGALDETAILS_TITLE', 'PDF en téléchargement');
define('MODULE_JANOLAW_PDF_LEGALDETAILS_DESC', 'Souhaitez-vous enregistrer les données en plus en format PDF et ajouter un lien ?<br/><b>Attention :</b> Ceci fonctionne dans la version HTML uniquement !');
define('MODULE_JANOLAW_MAIL_LEGALDETAILS_TITLE', 'PDF en pièce jointe');
define('MODULE_JANOLAW_MAIL_LEGALDETAILS_DESC', 'Souhaitez-vous que le PDF en pièce jointe sera envoyé pour la confirmation de la commande ?');

define('MODULE_JANOLAW_TYPE_REVOCATION_TITLE', '<hr noshade>Texte juridique révocation');
define('MODULE_JANOLAW_TYPE_REVOCATION_DESC', 'Veuillez indiquer dans quelle page vous souhaitez insérer le texte juridique automatiquement.');
define('MODULE_JANOLAW_PDF_REVOCATION_TITLE', 'PDF en téléchargement');
define('MODULE_JANOLAW_PDF_REVOCATION_DESC', 'Souhaitez-vous enregistrer les données en plus en format PDF et ajouter un lien ?<br/><b>Attention :</b> Ceci fonctionne dans la version HTML uniquement !');
define('MODULE_JANOLAW_MAIL_REVOCATION_TITLE', 'PDF en pièce jointe');
define('MODULE_JANOLAW_MAIL_REVOCATION_DESC', 'Souhaitez-vous que le PDF en pièce jointe sera envoyé pour la confirmation de la commande ?');

define('MODULE_JANOLAW_TYPE_WITHDRAWAL_TITLE', '<hr noshade>Texte juridique formulaire de révocation');
define('MODULE_JANOLAW_TYPE_WITHDRAWAL_DESC', 'Veuillez indiquer dans quelle page vous souhaitez insérer le texte juridique automatiquement. <br/><br/><b>Attention :</b> ceci ne fonctionne qu\'à partir de la version 3. Le changement peut être effectué auprès de Janolaw.');
define('MODULE_JANOLAW_PDF_WITHDRAWAL_TITLE', 'PDF en téléchargement');
define('MODULE_JANOLAW_PDF_WITHDRAWAL_DESC', 'Souhaitez-vous enregistrer les données en plus en format PDF et ajouter un lien ?<br/><b>Attention :</b> Ceci fonctionne dans la version HTML uniquement !');
define('MODULE_JANOLAW_MAIL_WITHDRAWAL_TITLE', 'PDF en pièce jointe');
define('MODULE_JANOLAW_MAIL_WITHDRAWAL_DESC', 'Souhaitez-vous que le PDF en pièce jointe sera envoyé pour la confirmation de la commande ?');
define('MODULE_JANOLAW_WITHDRAWAL_COMBINE_TITLE', 'Révocation / Formulaire de révocation combiné');
define('MODULE_JANOLAW_WITHDRAWAL_COMBINE_DESC', 'Souhaitez vous créer une révocation / formulaire de révocation combiné ?');

?>

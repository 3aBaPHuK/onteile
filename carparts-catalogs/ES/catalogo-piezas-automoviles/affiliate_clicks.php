<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_clicks.php 40 2013-01-08 16:36:44Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_clicks.php, v 1.12 2003/09/22);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

require('includes/application_top.php');

if (!isset($_SESSION['affiliate_id'])) {
    xtc_redirect(xtc_href_link(FILENAME_AFFILIATE, '', 'SSL'));
}

// create smarty elements
$smarty = new Smarty;

// include needed functions
require_once(DIR_FS_INC . 'xtc_date_short.inc.php');

$breadcrumb->add(NAVBAR_TITLE, xtc_href_link(FILENAME_AFFILIATE, '', 'SSL'));
$breadcrumb->add(NAVBAR_TITLE_CLICKS, xtc_href_link(FILENAME_AFFILIATE_CLICKS, '', 'SSL'));

if (!isset($_GET['page'])) $_GET['page'] = 1;

$affiliate_clickthroughs_raw = "select a.*, pd.products_name from " . TABLE_AFFILIATE_CLICKTHROUGHS . " a
                                    left join " . TABLE_PRODUCTS . " p on (p.products_id = a.affiliate_products_id)
                                    left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (pd.products_id = p.products_id and pd.language_id = '" . $_SESSION['languages_id'] . "')
                                    where a.affiliate_id = '" . $_SESSION['affiliate_id'] . "'  ORDER BY a.affiliate_clientdate desc";
$affiliate_clickthroughs_split = new splitPageResults($affiliate_clickthroughs_raw, $_GET['page'], MAX_DISPLAY_SEARCH_RESULTS);

require(DIR_FS_CATALOG .'templates/'.CURRENT_TEMPLATE. '/source/boxes.php');
require(DIR_WS_INCLUDES . 'header.php');

$smarty->assign('affiliate_clickthroughs_split_number', $affiliate_clickthroughs_split->number_of_rows);

$affiliate_clickthrough_table = '';

if ($affiliate_clickthroughs_split->number_of_rows > 0) {
	$ct_array = array();
    $affiliate_clickthroughs_values = xtc_db_query($affiliate_clickthroughs_split->sql_query);
    
    while ($affiliate_clickthroughs = xtc_db_fetch_array($affiliate_clickthroughs_values)) {
    	
    	if ($affiliate_clickthroughs['affiliate_products_id'] > 0) {
            $link_to = '<a href="' . xtc_href_link (FILENAME_PRODUCT_INFO, 'products_id=' . $affiliate_clickthroughs['affiliate_products_id']) . '" target="_blank">' . $affiliate_clickthroughs['products_name'] . '</a>';
        }
        else {
            $link_to = "Contentpage";
        }
    	
    	$ct_array[] = array('date' => xtc_date_short($affiliate_clickthroughs['affiliate_clientdate']),
    				  'products_id' => $affiliate_clickthroughs['affiliate_products_id'],
    				  'products_name' => $link_to,
    				  'client_referer' => $affiliate_clickthroughs['affiliate_clientreferer']);
    }
    $smarty->assign('ct_array', $ct_array);
}

if ($affiliate_clickthroughs_split->number_of_rows > 0) {
    $smarty->assign('affiliate_clickthroughs_split_count', $affiliate_clickthroughs_split->display_count(TEXT_DISPLAY_NUMBER_OF_CLICKS));
    $smarty->assign('affiliate_clickthroughs_split_links', $affiliate_clickthroughs_split->display_links(MAX_DISPLAY_PAGE_LINKS, xtc_get_all_get_params(array('page', 'info', 'x', 'y'))));
}

$smarty->assign('language', $_SESSION['language']);
$smarty->caching = 0;
$main_content = $smarty->fetch(CURRENT_TEMPLATE . '/module/affiliate_clicks.html');
$smarty->assign('main_content',$main_content);

if (!defined(RM))
	$smarty->load_filter('output', 'note');
	
$smarty->display(CURRENT_TEMPLATE . '/index.html');

include ('includes/application_bottom.php');
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id: freeamount.php 4855 2013-06-03 12:15:20Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce( freeamount.php,v 1.01 2002/01/24 03:25:00); www.oscommerce.com 
   (c) 2003	 nextcommerce (freeamount.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   freeamountv2-p1         	Autor:	dwk

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_FREEAMOUNT_TEXT_TITLE', 'Libre de costos de envío');
define('MODULE_SHIPPING_FREEAMOUNT_TEXT_DESCRIPTION', 'Entrega libre de costos de envío');
define('MODULE_SHIPPING_FREEAMOUNT_TEXT_WAY', 'A partir de un valor de pedido %s enviamos su pedido libre de costos de envío.');
define('MODULE_SHIPPING_FREEAMOUNT_INVALID_ZONE', 'No es posible realizar un envío a este país.');
define('MODULE_SHIPPING_FREEAMOUNT_SORT_ORDER', 'Orden de clasificación');

define('MODULE_SHIPPING_FREEAMOUNT_ALLOWED_TITLE' , 'Zonas de envío permitidas');
define('MODULE_SHIPPING_FREEAMOUNT_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE (deje este campo vacío si desea permitir todas las zonas))');
define('MODULE_SHIPPING_FREEAMOUNT_STATUS_TITLE' , 'Activar entrega libre de costos de envío');
define('MODULE_SHIPPING_FREEAMOUNT_STATUS_DESC' , '¿Desea ofrecer la entrega libre de costos de envío?');
define('MODULE_SHIPPING_FREEAMOUNT_DISPLAY_TITLE' , 'Activar visualización');
define('MODULE_SHIPPING_FREEAMOUNT_DISPLAY_DESC' , '¿Desea mostrar si no se alcanza el importe mínimo para la entrega libre de costos de envío?');
define('MODULE_SHIPPING_FREEAMOUNT_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_FREEAMOUNT_ZONE_DESC' , 'Si selecciona una zona, este modo de envío sólo se ofrecerá en esa zona.');
define('MODULE_SHIPPING_FREEAMOUNT_SORT_ORDER_TITLE' , 'Orden de clasificación');
define('MODULE_SHIPPING_FREEAMOUNT_SORT_ORDER_DESC' , 'Orden de la visualización');
define('MODULE_SHIPPING_FREEAMOUNT_NUMBER_ZONES_TITLE' , 'Número de zonas');
define('MODULE_SHIPPING_FREEAMOUNT_NUMBER_ZONES_DESC' , 'Número de zonas proporcionadas');
define('MODULE_SHIPPING_FREEAMOUNT_DISPLAY_TITLE' , 'Activar la visualización');
define('MODULE_SHIPPING_FREEAMOUNT_DISPLAY_DESC' , '¿Desea mostrar si no es posible realizar envíos a ese país o si no se pueden calcular los costos de envío?');

for ($module_shipping_freeamount_i = 1; $module_shipping_freeamount_i <= MODULE_SHIPPING_FREEAMOUNT_NUMBER_ZONES; $module_shipping_freeamount_i ++) {
define('MODULE_SHIPPING_FREEAMOUNT_COUNTRIES_'.$module_shipping_freeamount_i.'_TITLE' , '<hr/>Zona '.$module_shipping_freeamount_i.' países');
define('MODULE_SHIPPING_FREEAMOUNT_COUNTRIES_'.$module_shipping_freeamount_i.'_DESC' , 'Lista separada por comas de los códigos de países ISO (2 caracteres) que forman parte de la zona '.$module_shipping_freeamount_i.' (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_FREEAMOUNT_AMOUNT_'.$module_shipping_freeamount_i.'_TITLE' , 'Zona '.$module_shipping_freeamount_i.' importe mínimo');
define('MODULE_SHIPPING_FREEAMOUNT_AMOUNT_'.$module_shipping_freeamount_i.'_DESC' , 'Valor mínimo de pedido para la zona  '.$module_shipping_freeamount_i.' para que el envío sea gratuito.');
}
?>

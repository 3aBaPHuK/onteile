<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_contact.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_contact.php, v 1.3 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Mandar correo electrónico a los participantes del programa de afiliados');

define('TEXT_AFFILIATE', 'Afiliado:');
define('TEXT_SUBJECT', 'Asunto:');
define('TEXT_FROM', 'Remitente:');
define('TEXT_MESSAGE', 'Mensaje:');
define('TEXT_SELECT_AFFILIATE', 'Seleccionar afiliado');
define('TEXT_ALL_AFFILIATES', 'Todos los afiliados');
define('TEXT_NEWSLETTER_AFFILIATES', 'A todos los suscriptores del newsletter de afiliados');

define('NOTICE_EMAIL_SENT_TO', 'Aviso: correo electrónico a sido enviado a: %s');
define('ERROR_NO_AFFILIATE_SELECTED', 'Error: No se ha seleccionado ningún afiliado.');
?>

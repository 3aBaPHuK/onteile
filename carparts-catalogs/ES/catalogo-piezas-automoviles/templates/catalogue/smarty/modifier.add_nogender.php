<?php
/**************************************
* modifier.add_nogender.php
*
* � copyright 01-2021, noRiddle
**************************************/

function smarty_modifier_add_nogender ($html) {
    $html = str_replace('</select>', '<option value="i">'.NO_GENDER.'</option></select>', $html);

    return $html;
}
?>
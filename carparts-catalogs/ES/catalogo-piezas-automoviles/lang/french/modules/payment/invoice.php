<?php
/* -----------------------------------------------------------------------------------------
   $Id: invoice.php 998 2005-07-07 14:18:20Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com
   (c) 2003	 nextcommerce (invoice.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_INVOICE_TEXT_DESCRIPTION', 'Facture');
define('MODULE_PAYMENT_INVOICE_TEXT_TITLE', 'Facture');
define('MODULE_PAYMENT_INVOICE_TEXT_INFO', '');
define('MODULE_PAYMENT_INVOICE_STATUS_TITLE' , 'Activer le module de paiement');
define('MODULE_PAYMENT_INVOICE_STATUS_DESC' , 'Accepter des paiements par facture?');
define('MODULE_PAYMENT_INVOICE_ORDER_STATUS_ID_TITLE' , 'Établir le statut de commande');
define('MODULE_PAYMENT_INVOICE_ORDER_STATUS_ID_DESC' , 'Moduler les commandes qui sont effectuées avec ce module vers ce statut');
define('MODULE_PAYMENT_INVOICE_SORT_ORDER_TITLE' , 'Ordre d\'affichage');
define('MODULE_PAYMENT_INVOICE_SORT_ORDER_DESC' , 'Ordre d\'affichage. Afficher le chiffre le plus petit en premier.');
define('MODULE_PAYMENT_INVOICE_ZONE_TITLE' , 'Zone de paiement');
define('MODULE_PAYMENT_INVOICE_ZONE_DESC' , 'Si une zone de paiement est choisie, la méthode de paiement n\'est que valable pour cette zone.');
define('MODULE_PAYMENT_INVOICE_ALLOWED_TITLE' , 'Zones autorisées');
define('MODULE_PAYMENT_INVOICE_ALLOWED_DESC' , 'Indiquez <b>séparément</b> les zones qui sont autorisées pour ce module. (p.ex. AT, DE (si vide, toutes les zone sont autorisées))');
define('MODULE_PAYMENT_INVOICE_MIN_ORDER_TITLE' , 'Commandes nécessaires');
define('MODULE_PAYMENT_INVOICE_MIN_ORDER_DESC' , 'Le nombre minimum des commandes d\'un client pour que l\'option soit disponible.');
define('MODULE_PAYMENT_INVOICE_MIN_ORDER_STATUS_ID_TITLE' , 'Statut de commande pour nombre de commandes');
define('MODULE_PAYMENT_INVOICE_MIN_ORDER_STATUS_ID_DESC' , 'Statut de commande d\'une commande pour qu\'elle soit utilisée pour la calculation d\'une commande effectuée.');
define('MODULE_PAYMENT_INVOICE_MAX_AMOUNT_TITLE' , 'Montant maximal');
define('MODULE_PAYMENT_INVOICE_MAX_AMOUNT_DESC' , 'Entrez le montant maximal pour paiement par facture.');
?>

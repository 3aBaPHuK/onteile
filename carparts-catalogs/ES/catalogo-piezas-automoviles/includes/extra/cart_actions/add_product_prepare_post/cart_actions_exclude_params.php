<?php
/**********************************************************************
* file: cart_actions_exclude_params.php
* use: exclude GET parameter model from redirect in case 'add_product'
* (c) noRiddle 2016 / 04-2017
**********************************************************************/

$parameters[] = 'model';

?>
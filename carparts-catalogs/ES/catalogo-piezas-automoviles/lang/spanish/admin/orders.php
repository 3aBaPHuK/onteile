<?php
/* --------------------------------------------------------------
   $Id: orders.php 10896 2017-08-11 11:31:54Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce; www.oscommerce.com 
   (c) 2003      nextcommerce; www.nextcommerce.org
   (c) 2006      xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/
define('TEXT_BANK', 'Domiciliación bancaria');
define('TEXT_BANK_OWNER', 'Titular de la cuenta:');
define('TEXT_BANK_NUMBER', 'Número de cuenta:');
define('TEXT_BANK_BLZ', 'Código bancario:');
define('TEXT_BANK_NAME', 'Banco:');
define('TEXT_BANK_BIC', 'BIC:');
define('TEXT_BANK_IBAN', 'IBAN:');
define('TEXT_BANK_FAX', 'La autorización de domiciliación se confirmará por fax');
define('TEXT_BANK_STATUS', 'Estado de comprobación:');
define('TEXT_BANK_PRZ', 'Procedimiento de comprobación:');
define('TEXT_BANK_OWNER_EMAIL', 'Dirección de correo electrónico del titular de la cuenta:');

define('TEXT_BANK_ERROR_1', '¡El número de cuenta no coincide con el código bancario!');
define('TEXT_BANK_ERROR_2', '¡No se ha definido ningún procedimiento de comprobación para este número de cuenta!');
define('TEXT_BANK_ERROR_3', '¡Número de cuenta no verificable! Procedimiento de comprobación no implementado.');
define('TEXT_BANK_ERROR_4', '¡Número de cuenta técnicamente no verificable!');
define('TEXT_BANK_ERROR_5', '¡No se ha encontrado el código bancario!');
define('TEXT_BANK_ERROR_8', '¡No se ha especificado ningún código bancario!');
define('TEXT_BANK_ERROR_9', '¡No se ha especificado ningún número de cuenta!');
define('TEXT_BANK_ERRORCODE', 'Código de error:');

define('HEADING_TITLE', 'Pedidos');
define('HEADING_TITLE_SEARCH', 'Número de pedido:');
define('HEADING_TITLE_STATUS', 'Estado:');

define('TABLE_HEADING_COMMENTS', 'Comentario');
define('TABLE_HEADING_CUSTOMERS', 'Clientes');
define('TABLE_HEADING_ORDER_TOTAL', 'Valor total');
define('TABLE_HEADING_DATE_PURCHASED', 'Fecha de pedido');
define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_QUANTITY', 'Cantidad');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Número del artículo');
define('TABLE_HEADING_PRODUCTS', 'Artículo');
define('TABLE_HEADING_TAX', 'IVA');
define('TABLE_HEADING_TOTAL', 'Suma total');
define('TABLE_HEADING_STATUS', 'Estado');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Precio (excl.)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Precio (incl.)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total (excl.)');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total');
define('TABLE_HEADING_AFTERBUY', 'Afterbuy');
define('TABLE_HEADING_CUSTOMER_NOTIFIED', 'Cliente notificado');
define('TABLE_HEADING_DATE_ADDED', 'agregado el:');

define('ENTRY_CUSTOMER', 'Cliente:');
define('ENTRY_SOLD_TO', 'Dirección de facturación:');
define('ENTRY_STREET_ADDRESS', 'Calle:');
define('ENTRY_SUBURB', 'Dirección adicional:');
define('ENTRY_CITY', 'Ciudad:');
define('ENTRY_POST_CODE', 'Código postal:');
define('ENTRY_STATE', 'Estado federal:');
define('ENTRY_COUNTRY', 'País:');
define('ENTRY_TELEPHONE', 'Teléfono:');
define('ENTRY_EMAIL_ADDRESS', 'Dirección de correo electrónico:');
define('ENTRY_DELIVERY_TO', 'Dirección de entrega:');
define('ENTRY_SHIP_TO', 'Dirección de entrega:');
define('ENTRY_SHIPPING_ADDRESS', 'Dirección de envío:');
define('ENTRY_BILLING_ADDRESS', 'Dirección de facturación:');
define('ENTRY_PAYMENT_METHOD', 'Modalidad de pago:');
define('ENTRY_SHIPPING_METHOD', 'Modo de envío:');
define('ENTRY_CREDIT_CARD_TYPE', 'Tipo de tarjeta de crédito:');
define('ENTRY_CREDIT_CARD_OWNER', 'Titular de la tarjeta de crédito:');
define('ENTRY_CREDIT_CARD_NUMBER', 'Número de tarjeta de crédito:');
define('ENTRY_CREDIT_CARD_CVV', 'Código de seguridad (CVV):');
define('ENTRY_CREDIT_CARD_EXPIRES', 'La tarjeta de crédito vence el:');
define('ENTRY_SUB_TOTAL', 'Subtotal:');
define('ENTRY_TAX', 'IVA');
define('ENTRY_SHIPPING', 'Costos de envío:');
define('ENTRY_TOTAL', 'Suma total:');
define('ENTRY_DATE_PURCHASED', 'Fecha de pedido:');
define('ENTRY_STATUS', 'Estado:');
define('ENTRY_DATE_LAST_UPDATED', 'Actualizado por última vez el:');
define('ENTRY_NOTIFY_CUSTOMER', 'Notificar al cliente:');
define('ENTRY_NOTIFY_COMMENTS', 'Enviar comentarios:');
define('ENTRY_PRINTABLE', 'Imprimir factura');

define('TEXT_INFO_HEADING_DELETE_ORDER', 'Borrar pedido');
define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar este pedido?');
define('TEXT_INFO_RESTOCK_PRODUCT_QUANTITY', 'Abonar el número de artículos al stock');
define('TEXT_DATE_ORDER_CREATED', 'creado el:');
define('TEXT_DATE_ORDER_LAST_MODIFIED', 'Última modificación:');
define('TEXT_INFO_PAYMENT_METHOD', 'Modalidad de pago:');
define('TEXT_INFO_SHIPPING_METHOD', 'Modo de envío:');

define('TEXT_ALL_ORDERS', 'Todos los pedidos');
define('TEXT_NO_ORDER_HISTORY', 'No hay historial de pedidos disponible');

define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_SUBJECT', 'Cambio del estado de su pedido');
define('EMAIL_TEXT_ORDER_NUMBER', 'Número de pedido');
define('EMAIL_TEXT_INVOICE_URL', 'Su pedido se puede ver bajo la siguiente dirección:');
define('EMAIL_TEXT_DATE_ORDERED', 'Fecha de pedido:');
define('EMAIL_TEXT_STATUS_UPDATE', 'El estado de su pedido ha sido actualizado.' . "\n\n" . 'Nuevo estado: %s' . "\n\n" . 'Si tiene preguntas sobre su pedido, por favor responda a este correo electrónico.' . "\n\n" . 'Con saludos cordiales' . "\n");
define('EMAIL_TEXT_COMMENTS_UPDATE', 'Observaciones y comentarios sobre su pedido:' . "\n\n%s\n\n");

define('ERROR_ORDER_DOES_NOT_EXIST', 'Error: ¡El pedido no existe!.');
define('SUCCESS_ORDER_UPDATED', 'Éxito: El pedido se ha actualizado exitosamente.');
define('WARNING_ORDER_NOT_UPDATED', 'Nota: No se ha cambiado nada. Por lo tanto, este orden no ha sido actualizado.');

define('TABLE_HEADING_DISCOUNT', 'Descuento');
define('ENTRY_CUSTOMERS_GROUP', 'Grupo de clientes:');
define('ENTRY_CUSTOMERS_VAT_ID', 'Número de IVA');
define('TEXT_VALIDATING', 'No confirmado');

// BOF - Tomcraft - 2009-10-03 - Paypal Express Modul
define('TEXT_INFO_PAYPAL_DELETE', 'Borrar también datos de transacción de PayPal.');
// EOF - Tomcraft - 2009-10-03 - Paypal Express Modul

// BOF - Tomcraft - 2010-04-22 - Added a missing language definition
define('TEXT_PRODUCTS', 'Artículo');
// EOF - Tomcraft - 2010-04-22 - Added a missing language definition

//BOF - web28 - 2010-03-20 - Send Order by Admin
define('COMMENT_SEND_ORDER_BY_ADMIN' , 'Confirmación de pedido ha sido enviado.');
define('BUTTON_ORDER_CONFIRMATION', 'Enviar confirmación de pedido');
define('SUCCESS_ORDER_SEND', 'Confirmación de pedido enviado exitosamente.');
//EOF - web28 - 2010-03-20 - Send Order by Admin

// web28 2010-12-07 add new defines
define('ENTRY_CUSTOMERS_ADDRESS', 'Dirección del cliente:');
define('TEXT_ORDER', 'Pedido:');
define('TEXT_ORDER_HISTORY', 'Historial de pedidos:');
define('TEXT_ORDER_STATUS', 'Estado del pedido:');

define('TABLE_HEADING_ORDERS_ID', 'Número del pedido:');
define('TEXT_SHIPPING_TO', 'Envío a');

define('TABLE_HEADING_COMMENTS_SENT', 'Comentario enviado');

define('TABLE_HEADING_TRACK_TRACE', 'Track & Trace:');
define('TABLE_HEADING_CARRIER', 'Modo de envío');
define('TABLE_HEADING_PARCEL_LINK', 'Número de envío / Número de etiqueta del paquete / Número del encargo / ID del envío / Número de seguimiento');

define('TEXT_INFO_HEADING_REVERSE_ORDER', 'Cancelar pedido');
define('TEXT_INFO_REVERSE_INTRO', '¿Está seguro de que quiere cancelar este pedido?');

define('TABLE_HEADING_SHIPCLOUD', 'Shipcloud:');
define('TABLE_HEADING_PARCEL_ID', 'Número de etiqueta del paquete');
define('TEXT_SHIPCLOUD_STANDARD', 'Estándar');
define('TEXT_SHIPCLOUD_ONE_DAY', 'Urgente');
define('TEXT_SHIPCLOUD_ONE_DAY_EARLY', 'Urgente 10:00');
define('TEXT_SHIPCLOUD_RETURNS', 'Devolución');
define('TEXT_SHIPCLOUD_LETTER', 'Carta por correo');
define('TEXT_SHIPCLOUD_BOOKS', 'Envío de libros por correo');
define('TEXT_SHIPCLOUD_PARCEL_LETTER', 'Envío de mercancía por correo');
define('TEXT_WEIGHT_PLACEHOLDER', 'Peso / Kg');
define('TEXT_SHIPCLOUD_INSURANCE_NO', 'Seguro superior No');
define('TEXT_SHIPCLOUD_INSURANCE_YES', 'Seguro superior Sí');
define('TEXT_SHIPCLOUD_BULK', 'mercancías voluminosas');
define('TEXT_SHIPCLOUD_PARCEL', 'Paquete');

define('DOWNLOAD_LABEL', 'Descargar etiqueta del paquete');
define('CREATE_LABEL', 'Crear etiqueta del paquete');
define('TEXT_DELETE_SHIPMENT_SUCCESS', 'Se ha eliminado la etiqueta del paquete shipcloud');
define('TEXT_LABEL_CREATED', 'Se ha creado la etiqueta del paquete exitosamente.');
define('TEXT_CARRIER_ERROR', 'El proveedor de servicios de paquetes no está desbloqueado en su cuenta de shipcloud o la clave de API no es válida.');
define('TEXT_CARRIER_PLACEHOLDER_1', 'Descripción del paquete');
define('TEXT_CARRIER_PLACEHOLDER_2', 'Descripción del envío');

define('TEXT_DOWNLOADS', 'Descargas');
define('TABLE_HEADING_FILENAME', 'Nombre de archivo');
define('TABLE_HEADING_EXPIRES', 'Fecha de caducidad');
define('TABLE_HEADING_DOWNLOADS', 'Cantidad de descargas');
define('TABLE_HEADING_DAYS', 'Cantidad de días');

define('ENTRY_SEND_TRACKING_INFO', 'Información de envío:');

define('TEXT_ORDERS_STATUS_FILTER', 'Filtro de estado del pedido');

define('TABLE_HEADING_DATE', 'Fecha');

define('BUTTON_ORDER_MAIL_STEP', 'Enviar confirmación de pedido');
define('COMMENT_SEND_ORDER_MAIL_STEP' , 'Confirmación de pedido ha sido enviada');
define('SUCCESS_ORDER_MAIL_STEP_SEND', 'Confirmación de pedido enviada con éxito');
?>

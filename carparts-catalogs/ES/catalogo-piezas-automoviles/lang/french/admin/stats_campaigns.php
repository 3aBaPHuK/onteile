<?php
/* --------------------------------------------------------------
   $Id: stats_campaigns.php 1118 2005-07-25 21:11:34Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2005 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(stats_sales_report.php,v 1.6 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (stats_sales_report.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Rapport de campagne');

define('REPORT_TYPE_YEARLY', 'Annuel');
define('REPORT_TYPE_MONTHLY', 'Mensuel');
define('REPORT_TYPE_WEEKLY', 'Hebdomadaire');
define('REPORT_TYPE_DAILY', 'Quotidien');
define('REPORT_START_DATE', 'de date');
define('REPORT_END_DATE', 'à date (inclus)');

define('REPORT_ALL', 'Tous');
define('REPORT_STATUS_FILTER', 'Statut de commande');
define('REPORT_CAMPAIGN_FILTER', 'Campagne');

define('HEADING_TOTAL', 'Total :');
define('HEADING_LEADS', 'Leads');
define('HEADING_SELLS', 'Sells');
define('HEADING_HITS', 'Hits');
define('HEADING_LATESELLS', 'Sells tardives');
define('HEADING_SUM', 'Somme');
define('TEXT_REFERER', 'Référent');
?>

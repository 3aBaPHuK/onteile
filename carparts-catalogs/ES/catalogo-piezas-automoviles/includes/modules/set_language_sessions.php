<?php
/* -----------------------------------------------------------------------------------------
   $Id: set_language_sessions.php 10876 2017-07-25 06:39:20Z GTB $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
   
$language_not_found = false;

foreach(auto_include(DIR_FS_CATALOG.'includes/extra/modules/set_language_sessions/','php') as $file) require_once ($file); 

if (!isset($_SESSION['language']) || isset($_GET['language']) || (isset($_SESSION['language']) && !isset($_SESSION['language_charset']))) {
  require_once (DIR_WS_CLASSES.'language.php');
  if (isset($_GET['language'])) {
    $_GET['language'] = xtc_input_validation($_GET['language'], 'lang', '');
    $_SESSION['customer_clicked_lang'] = true; //store language click in session to be able to track voluntary language change (for laximo languages implementation below), noRiddle
    $lng = new language($_GET['language']);
  } elseif (isset($_SESSION['language'])) {
    $lng = new language(xtc_input_validation($_SESSION['language'], 'lang', ''));
  } else {
    $lng = new language(xtc_input_validation(DEFAULT_LANGUAGE, 'lang', ''));
    if (USE_BROWSER_LANGUAGE == 'true') {
      $lng->get_browser_language();
    }
  }
  $_SESSION['language'] = $lng->language['directory'];
  $_SESSION['languages_id'] = $lng->language['id'];
  $_SESSION['language_charset'] = $lng->language['language_charset'];
  $_SESSION['language_code'] = $lng->language['code'];

  if (isset($_GET['language']) && !isset($lng->catalog_languages[$_GET['language']])) {
    $_GET['language'] = DEFAULT_LANGUAGE;
    $language_not_found = true;
  }
}

// set default charset
@ini_set('default_charset', $_SESSION['language_charset']);

//BOC configure laximo languages (finally define it in nr_catalogue_files/config.php), noRiddle
if(!defined('RUN_MODE_ADMIN')) {
    require_once(DIR_FS_INC.'get_cust_status_from_country.inc.php');
    $general_brows_langs = detect_browser_languages();
    //BOC verify whether class is instantiated
    if(!class_exists('language')) {
        require_once(DIR_WS_CLASSES.'language.php');
    }
    if(!is_object($lng) && !($lng instanceof language)) {
        $lng = new language();
    }
    //EOC verify whether class is instantiated

    if(strpos($_SESSION['language_code'], '_') !== false) {
        $sess_lang_cd_arr = explode('_', $_SESSION['language_code']);
        $sess_lang_cd = $sess_lang_cd_arr[0];
    } else {
        $sess_lang_cd = $_SESSION['language_code'];
    }

    $scan_lang_arr_raw = array_keys($lng->catalog_languages);
    $scan_lang_arr = str_replace('_', '-', $scan_lang_arr_raw);

    if(!isset($_SESSION['customer_clicked_lang'])) {
        if(file_exists(DIR_FS_CATALOG.'nr_catalogue_files/localization_'.$_SESSION['language_code'].'.php')) {
            $lax_ui_loc = $sess_lang_cd;
        } else if($general_brows_langs !== false && file_exists(DIR_FS_CATALOG.'nr_catalogue_files/localization_'.$general_brows_langs['single_lng'][0].'.php')) {
            $lax_ui_loc = $general_brows_langs['single_lng'][0];
        } else if($general_brows_langs !== false && file_exists(DIR_FS_CATALOG.'nr_catalogue_files/localization_'.$general_brows_langs['composed_lng'][0].'.php')) {
            $lax_ui_loc = $general_brows_langs['composed_lng'][0];
        } else {
            $lax_ui_loc = 'en';
        }

        if($general_brows_langs !== false && in_array($general_brows_langs['single_lng'][0], $lax_avail_langs)) { //get laximo data in browser language first because of google language dropdown
            $lax_cat_dat = $general_brows_langs['single_lng'][0].'_'.($sess_lang_cd == 'en' ? 'GB' : strtoupper($general_brows_langs['single_lng'][0]));
        } else if(in_array($sess_lang_cd, $lax_avail_langs)) { //alternatively shop language if available
            $lax_cat_dat = $sess_lang_cd.'_'.($sess_lang_cd == 'en' ? 'GB' : strtoupper($sess_lang_cd));
        } else { //alternatively english
            $lax_cat_dat = 'en_GB';
        }
    } else {
        if(file_exists(DIR_FS_CATALOG.'nr_catalogue_files/localization_'.$_SESSION['language_code'].'.php')) {
            $lax_ui_loc = $_SESSION['language_code'];
        } else if(file_exists(DIR_FS_CATALOG.'nr_catalogue_files/localization_'.$sess_lang_cd.'.php')) {
            $lax_ui_loc = $sess_lang_cd;
        } else {
            $lax_ui_loc = 'en';
        }

        if(in_array($sess_lang_cd, $lax_avail_langs)) { //here, as oposed to above, clicked language first
            $lax_cat_dat = $sess_lang_cd.'_'.($sess_lang_cd == 'en' ? 'GB' : strtoupper($sess_lang_cd));
        } else if($general_brows_langs !== false && in_array($general_brows_langs['single_lng'][0], $lax_avail_langs)) { //alternatively browser language
            $lax_cat_dat = $general_brows_langs['single_lng'][0].'_'.($sess_lang_cd == 'en' ? 'GB' : strtoupper($general_brows_langs['single_lng'][0]));
        } else { //alternatively english
            $lax_cat_dat = 'en_GB';
        }
    }
}
//EOC configure laximo languages (finally define it in nr_catalogue_files/config.php), noRiddle
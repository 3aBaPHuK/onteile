<?php
/* -----------------------------------------------------------------------------------------
   $Id: configuration.php 10896 2017-08-11 11:31:54Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(configuration.php,v 1.8 2002/01/04); www.oscommerce.com
   (c) 2003	 nextcommerce (configuration.php,v 1.16 2003/08/25); www.nextcommerce.org
   (c) 2006 XT-Commerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('TABLE_HEADING_CONFIGURATION_TITLE', 'Nombre');
define('TABLE_HEADING_CONFIGURATION_VALUE', 'Valor');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_INFO_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_INFO_DATE_ADDED', 'agregado el:');
define('TEXT_INFO_LAST_MODIFIED', 'última modificación:');

// language definitions for config
define('STORE_NAME_TITLE' , 'Nombre de la tienda');
define('STORE_NAME_DESC' , 'El nombre de esta tienda online');
define('STORE_OWNER_TITLE' , 'Propietario');
define('STORE_OWNER_DESC' , 'El nombre del operador de la tienda');
define('STORE_OWNER_EMAIL_ADDRESS_TITLE' , 'Dirección de correo electrónico');
define('STORE_OWNER_EMAIL_ADDRESS_DESC' , 'La dirección de correo electrónico del operador de la tienda');

define('EMAIL_FROM_TITLE' , 'Correo electrónico de');
define('EMAIL_FROM_DESC' , 'Dirección de correo electrónico que se debe utilizar al enviar (sendmail).');

define('STORE_COUNTRY_TITLE' , 'País');
define('STORE_COUNTRY_DESC' , 'La ubicación de la tienda online <br /><br /><b>Nota: No olvide ajustar la región correctamente.</b>');
define('STORE_ZONE_TITLE' , 'Región');
define('STORE_ZONE_DESC' , 'La región de la tienda online');

define('EXPECTED_PRODUCTS_SORT_TITLE' , 'Orden para anuncios de artículos');
define('EXPECTED_PRODUCTS_SORT_DESC' , 'Este es el orden en el que se muestran los artículos anunciados.');
define('EXPECTED_PRODUCTS_FIELD_TITLE' , 'Campo de clasificación para anuncios de artículos');
define('EXPECTED_PRODUCTS_FIELD_DESC' , 'Esta es la columna que se usa para ordenar los artículos anunciados.');

define('USE_DEFAULT_LANGUAGE_CURRENCY_TITLE' , 'Convertir a moneda local automáticamente');
define('USE_DEFAULT_LANGUAGE_CURRENCY_DESC' , 'Convertir automáticamente a la moneda local del cliente si está disponible.');

define('SEND_EXTRA_ORDER_EMAILS_TO_TITLE' , 'Enviar un correo electrónico de pedido extra a:');
define('SEND_EXTRA_ORDER_EMAILS_TO_DESC' , 'Si se debe enviar una copia adicional del correo electrónico del pedido, por favor, indique las direcciones de los destinatarios de la siguiente manera: Nombre 1 <E-Mail@address1>, Nombre 2 <E-Mail@address2>');

define('SEARCH_ENGINE_FRIENDLY_URLS_TITLE' , '¿Utilizando URLs amigables para los motores de búsqueda?');
define('SEARCH_ENGINE_FRIENDLY_URLS_DESC' , 'Las URLs de las páginas se pueden mostrar automáticamente optimizadas para los motores de búsqueda.<br /><br /><strong>Atención:</strong> ¡Para las URLs optimizadas para los motores de búsqueda, el archivo _.htaccess en el directorio principal de la tienda debe ser activado o renombrado a .htaccess! ¡Además, el servidor web debe soportar el <a href="http://www.modrewrite.de/" target="_blank">mod_rewrite</a> módulo! (Pregunte a su proveedor web si no puede verificar esto.)');

define('DISPLAY_CART_TITLE' , '¿Debe mostrarse la cesta de compra después de la inserción?');
define('DISPLAY_CART_DESC' , '¿Después de añadir un artículo, ir a la cesta o volver al artículo?');

define('ALLOW_GUEST_TO_TELL_A_FRIEND_TITLE' , '¿Permitir que los invitados informen a sus conocidos por correo electrónico?');
define('ALLOW_GUEST_TO_TELL_A_FRIEND_DESC' , '¿Permitir que los invitados informen a sus conocidos por correo electrónico sobre los artículos?');

define('ADVANCED_SEARCH_DEFAULT_OPERATOR_TITLE' , 'Vinculaciones de búsqueda');
define('ADVANCED_SEARCH_DEFAULT_OPERATOR_DESC' , 'Operador por defecto para vincular palabras de búsqueda.');

define('STORE_NAME_ADDRESS_TITLE' , 'Dirección comercial y número de teléfono, etc.');
define('STORE_NAME_ADDRESS_DESC' , 'Escriba su dirección comercial aquí como en un membrete.');

define('SHOW_COUNTS_TITLE' , '¿Cantidad de artículos después de los nombres de categoría?');
define('SHOW_COUNTS_DESC' , 'Cuenta de forma recurrente la cantidad de artículos diferentes por cada grupo de mercancías y muestra la cantidad (x) detrás de cada nombre de categoría.');

define('DISPLAY_PRICE_WITH_TAX_TITLE' , 'Mostrar precio con IVA incluido');
define('DISPLAY_PRICE_WITH_TAX_DESC' , 'Mostrar precios con impuestos incluidos (true) o compensar al final (false)');

define('DEFAULT_CUSTOMERS_STATUS_ID_ADMIN_TITLE' , 'Estado de cliente (grupo de clientes) para administradores en el Frontend');
define('DEFAULT_CUSTOMERS_STATUS_ID_ADMIN_DESC' , 'Seleccione el estado de cliente (grupo) con qué permisos de grupo de clientes se encuentra el administrador en el Frontend.');
define('DEFAULT_CUSTOMERS_STATUS_ID_GUEST_TITLE' , 'Estado de cliente (grupo de clientes) para invitados');
define('DEFAULT_CUSTOMERS_STATUS_ID_GUEST_DESC' , '¡Seleccione el estado del cliente (grupo) para los invitados mediante el respectivo ID!');
define('DEFAULT_CUSTOMERS_STATUS_ID_TITLE' , 'Estado de cliente para nuevos clientes');
define('DEFAULT_CUSTOMERS_STATUS_ID_DESC' , '¡Seleccione el estado del cliente (grupo) para los nuevos clientes mediante el respectivo ID!<br />CONSEJO: Puede configurar más grupos en el menú Grupos de clientes y, por ejemplo, realizar semanas de promoción: Esta semana 10% de descuento para todos los nuevos clientes.');

define('ALLOW_ADD_TO_CART_TITLE' , 'Permite añadir artículos a la cesta de compra');
define('ALLOW_ADD_TO_CART_DESC' , 'Permite añadir artículos a la cesta de la compra incluso si "Mostrar precios" está configurado en "No" en el grupo de clientes.');
define('ALLOW_DISCOUNT_ON_PRODUCTS_ATTRIBUTES_TITLE' , '¿Usar descuentos también en los atributos del artículo?');
define('ALLOW_DISCOUNT_ON_PRODUCTS_ATTRIBUTES_DESC' , 'Permite aplicar el descuento configurado del grupo de clientes a los atributos del artículo (sólo si el artículo no está indicado como "oferta especial").');
define('CURRENT_TEMPLATE_TITLE' , 'Conjunto de plantillas (Theme)');
define('CURRENT_TEMPLATE_DESC' , 'Seleccione un conjunto de plantillas (Theme). La Theme debe estar en la carpeta www.Ihre-Domain.com/templates/ .');

define('ENTRY_FIRST_NAME_MIN_LENGTH_TITLE' , 'Nombre');
define('ENTRY_FIRST_NAME_MIN_LENGTH_DESC' , 'Longitud mínima del nombre'
);
define('ENTRY_LAST_NAME_MIN_LENGTH_TITLE' , 'Apellido');
define('ENTRY_LAST_NAME_MIN_LENGTH_DESC' , 'Longitud mínima del apellido');
define('ENTRY_DOB_MIN_LENGTH_TITLE' , 'Fecha de nacimiento');
define('ENTRY_DOB_MIN_LENGTH_DESC' , 'Longitud mínima la fecha de nacimiento');
define('ENTRY_EMAIL_ADDRESS_MIN_LENGTH_TITLE' , 'Dirección de correo electrónico');
define('ENTRY_EMAIL_ADDRESS_MIN_LENGTH_DESC' , 'Longitud mínima la dirección de correo electrónico');
define('ENTRY_STREET_ADDRESS_MIN_LENGTH_TITLE' , 'Calle');
define('ENTRY_STREET_ADDRESS_MIN_LENGTH_DESC' , 'Longitud mínima la dirección de calle');
define('ENTRY_COMPANY_MIN_LENGTH_TITLE' , 'Empresa');
define('ENTRY_COMPANY_MIN_LENGTH_DESC' , 'Longitud mínima del nombre de la empresa');
define('ENTRY_POSTCODE_MIN_LENGTH_TITLE' , 'Código postal');
define('ENTRY_POSTCODE_MIN_LENGTH_DESC' , 'Longitud mínima del código postal');
define('ENTRY_CITY_MIN_LENGTH_TITLE' , 'Ciudad');
define('ENTRY_CITY_MIN_LENGTH_DESC' , 'Longitud mínima la ciudad');
define('ENTRY_STATE_MIN_LENGTH_TITLE' , 'Estado federal');
define('ENTRY_STATE_MIN_LENGTH_DESC' , 'Longitud mínima del estado federal');
define('ENTRY_TELEPHONE_MIN_LENGTH_TITLE' , 'Número de teléfono');
define('ENTRY_TELEPHONE_MIN_LENGTH_DESC' , 'Longitud mínima del número de teléfono');
define('ENTRY_PASSWORD_MIN_LENGTH_TITLE' , 'Contraseña');
define('ENTRY_PASSWORD_MIN_LENGTH_DESC' , 'Longitud mínima de la contraseña');

define('REVIEW_TEXT_MIN_LENGTH_TITLE' , 'Reseñas');
define('REVIEW_TEXT_MIN_LENGTH_DESC' , 'Longitud mínima de entrada de texto para las reseñas');

define('MIN_DISPLAY_BESTSELLERS_TITLE' , 'Bestseller');
define('MIN_DISPLAY_BESTSELLERS_DESC' , 'Número mínimo de bestsellers a mostrar');
define('MIN_DISPLAY_ALSO_PURCHASED_TITLE' , 'También se compró');
define('MIN_DISPLAY_ALSO_PURCHASED_DESC' , 'Número mínimo de artículos también comprados a mostrar en la vista de artículos.');

define('MAX_ADDRESS_BOOK_ENTRIES_TITLE' , 'Entradas de la libreta de direcciones');
define('MAX_ADDRESS_BOOK_ENTRIES_DESC' , 'Número máximo de entradas en la libreta de direcciones por cliente');
define('MAX_DISPLAY_SEARCH_RESULTS_TITLE' , 'Número de artículos');
define('MAX_DISPLAY_SEARCH_RESULTS_DESC' , 'Número de artículos en el listado de productos');
define('MAX_DISPLAY_PAGE_LINKS_TITLE' , 'Navegar páginas');
define('MAX_DISPLAY_PAGE_LINKS_DESC' , 'Número de páginas individuales para las que se debe mostrar un enlace en el menú de navegación de la página');
define('MAX_DISPLAY_SPECIAL_PRODUCTS_TITLE' , 'Ofertas especiales');
define('MAX_DISPLAY_SPECIAL_PRODUCTS_DESC' , 'Número máximo de ofertas especiales a mostrar');
define('MAX_DISPLAY_NEW_PRODUCTS_TITLE' , 'Módulo de visualización de nuevos artículos');
define('MAX_DISPLAY_NEW_PRODUCTS_DESC' , 'Número máximo de artículos nuevos a mostrar en las categorías de productos');
define('MAX_DISPLAY_UPCOMING_PRODUCTS_TITLE' , 'Módulo de visualización de artículos esperados');
define('MAX_DISPLAY_UPCOMING_PRODUCTS_DESC' , 'Número máximo de artículos esperados a mostrar en la página de inicio');
define('MAX_DISPLAY_MANUFACTURERS_IN_A_LIST_TITLE' , 'Valor límite de la lista de fabricantes');
define('MAX_DISPLAY_MANUFACTURERS_IN_A_LIST_DESC' , 'En la caja del fabricante; Si el número de fabricantes supera este valor límite, se muestra una lista desplegable o un cuadro de lista en lugar de la lista de enlaces habitual (dependiendo de lo que se haya introducido en "Lista de fabricantes").');
define('MAX_MANUFACTURERS_LIST_TITLE' , 'Lista de fabricantes');
define('MAX_MANUFACTURERS_LIST_DESC' , 'En la caja del fabricante; Si el valor se ajusta a "1", el cuadro del fabricante se muestra como una lista desplegable. De lo contrario, como un cuadro de lista con el número especificado de líneas.');
define('MAX_DISPLAY_MANUFACTURER_NAME_LEN_TITLE' , 'Longitud del nombre del fabricante');
define('MAX_DISPLAY_MANUFACTURER_NAME_LEN_DESC' , 'En la caja del fabricante; longitud máxima de los nombres en la caja del fabricante');
define('MAX_DISPLAY_NEW_REVIEWS_TITLE' , 'Nuevas reseñas');
define('MAX_DISPLAY_NEW_REVIEWS_DESC' , 'Número máximo de reseñas nuevas a mostrar');
define('MAX_RANDOM_SELECT_REVIEWS_TITLE' , 'Agrupacion a seleccionar de las reseñas');
define('MAX_RANDOM_SELECT_REVIEWS_DESC' , '¿De cuántas reseñas se deben seleccionar las reseñas mostradas al azar en la caja?');
define('MAX_RANDOM_SELECT_NEW_TITLE' , 'Agrupacion a seleccionar de los nuevos artículos');
define('MAX_RANDOM_SELECT_NEW_DESC' , '¿De cuántos artículos nuevos se deben seleccionar los nuevos artículos mostrados al azar en la caja?');
define('MAX_RANDOM_SELECT_SPECIALS_TITLE' , 'Agrupacion a seleccionar de las ofertas especiales');
define('MAX_RANDOM_SELECT_SPECIALS_DESC' , '¿De cuántas ofertas especiales se deben seleccionar las ofertas mostradas al azar en la caja?');
define('MAX_DISPLAY_CATEGORIES_PER_ROW_TITLE' , 'Número de categorías por línea');
define('MAX_DISPLAY_CATEGORIES_PER_ROW_DESC' , 'Número de categorías a mostrar por línea en los resúmenes');
define('MAX_DISPLAY_PRODUCTS_NEW_TITLE' , 'Lista de nuevos artículos');
define('MAX_DISPLAY_PRODUCTS_NEW_DESC' , 'Número máximo de artículos nuevos a mostrar en la lista');
define('MAX_DISPLAY_BESTSELLERS_TITLE' , 'Bestsellers');
define('MAX_DISPLAY_BESTSELLERS_DESC' , 'Número máximo de bestsellers a mostrar');
define('MAX_DISPLAY_BESTSELLERS_DAYS_TITLE' , 'Número de días para los bestsellers');
define('MAX_DISPLAY_BESTSELLERS_DAYS_DESC' , 'Número máximo de días a mostrar los artículos bestseller');
define('MAX_DISPLAY_ALSO_PURCHASED_TITLE' , 'También se compró');
define('MAX_DISPLAY_ALSO_PURCHASED_DESC' , 'Número máximo de artículos también comprados a mostrar en la vista de artículos');
define('MAX_DISPLAY_PRODUCTS_IN_ORDER_HISTORY_BOX_TITLE' , 'Cuadro de resumen de pedidos');
define('MAX_DISPLAY_PRODUCTS_IN_ORDER_HISTORY_BOX_DESC' , 'Número máximo de artículos a mostrar en el cuadro de resumen de pedidos personal del cliente.');
define('MAX_DISPLAY_ORDER_HISTORY_TITLE' , 'Resumen de pedidos');
define('MAX_DISPLAY_ORDER_HISTORY_DESC' , 'Número máximo de pedidos a mostrar en el resumen en el área de clientes de la tienda.');
define('MAX_PRODUCTS_QTY_TITLE', 'Número máximo de productos');
define('MAX_PRODUCTS_QTY_DESC', 'Número máximo de artículos en la cesta de compra');
define('MAX_DISPLAY_NEW_PRODUCTS_DAYS_TITLE' , 'Número de días para nuevos productos');
define('MAX_DISPLAY_NEW_PRODUCTS_DAYS_DESC' , 'Número máximo de días a mostrar nuevos artículos.');

define('PRODUCT_IMAGE_THUMBNAIL_WIDTH_TITLE' , 'Ancho de las miniaturas de los artículos');
define('PRODUCT_IMAGE_THUMBNAIL_WIDTH_DESC' , 'Ancho máximo de las miniaturas de los artículos en píxeles (por defecto: 160). Para valores mayores, puede ser necesario adaptar "productPreviewImage" en el fichero stylesheet.css de la plantilla.');
define('PRODUCT_IMAGE_THUMBNAIL_HEIGHT_TITLE' , 'Altura de las miniaturas de los artículos');
define('PRODUCT_IMAGE_THUMBNAIL_HEIGHT_DESC' , 'Altura máxima de las miniaturas de los artículos en píxeles (por defecto: 160).');

define('PRODUCT_IMAGE_INFO_WIDTH_TITLE' , 'Ancho de las imágenes de la información del artículo');
define('PRODUCT_IMAGE_INFO_WIDTH_DESC' , 'Ancho máximo de las imágenes de información de artículo en píxeles (por defecto: 230).');
define('PRODUCT_IMAGE_INFO_HEIGHT_TITLE' , 'Altura de las imágenes de la información del artículo');
define('PRODUCT_IMAGE_INFO_HEIGHT_DESC' , 'Altura máxima de las imágenes de información de artículo en píxeles (por defecto: 230).');

define('PRODUCT_IMAGE_POPUP_WIDTH_TITLE' , 'Ancho de las imágenes popup del artículo');
define('PRODUCT_IMAGE_POPUP_WIDTH_DESC' , 'Ancho máximo de las imágenes popup del artículo en píxeles (por defecto: 800).');
define('PRODUCT_IMAGE_POPUP_HEIGHT_TITLE' , 'Altura de las imágenes popup del artículo');
define('PRODUCT_IMAGE_POPUP_HEIGHT_DESC' , 'Altura máxima de las imágenes popup del artículo en píxeles (por defecto: 800).');

define('SMALL_IMAGE_WIDTH_TITLE' , 'Ancho de las imágenes del artículo');
define('SMALL_IMAGE_WIDTH_DESC' , 'Ancho máximo de las imágenes de los artículos en píxeles');
define('SMALL_IMAGE_HEIGHT_TITLE' , 'Altura de las imágenes del artículo');
define('SMALL_IMAGE_HEIGHT_DESC' , 'Altura máxima de las imágenes de los artículos en píxeles');

define('SUBCATEGORY_IMAGE_WIDTH_TITLE' , 'Ancho de las imágenes de la subcategoría (grupo de productos)');
define('SUBCATEGORY_IMAGE_WIDTH_DESC' , 'Ancho máximo de las imágenes de subcategoría (grupo de productos) en píxeles');
define('SUBCATEGORY_IMAGE_HEIGHT_TITLE' , 'Altura de las imágenes de la subcategoría (grupo de productos)');
define('SUBCATEGORY_IMAGE_HEIGHT_DESC' , 'Altura máxima de las imágenes de subcategoría (grupo de productos) en píxeles');

define('CONFIG_CALCULATE_IMAGE_SIZE_TITLE' , 'Calcular el tamaño de la imagen');
define('CONFIG_CALCULATE_IMAGE_SIZE_DESC' , '¿Deben calcularse los tamaños de las imágenes?');

define('IMAGE_REQUIRED_TITLE' , '¿Se necesitan imágenes?');
define('IMAGE_REQUIRED_DESC' , 'Si lo ajusta aquí a "1", las imágenes que no existen se mostrarán como cuadros. Bueno para los desarrolladores.');

define('MO_PICS_TITLE', 'Número de imágenes de productos adicionales');
define('MO_PICS_DESC', 'Número de imágenes de productos que deben estar disponibles además de la imagen principal del producto.');

//This is for the Images showing your products for preview. All the small stuff.

define('PRODUCT_IMAGE_THUMBNAIL_BEVEL_TITLE' , 'Miniaturas de artículos:Bevel<br /><img src="images/config_bevel.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_BEVEL_DESC' , 'Miniaturas de artículos:Bevel<br /><br />Default Valor: (8,FFCCCC,330000)<br /><br />shaded bevelled edges<br />Uso:<br />(edge width,hex light colour,hex dark colour)');

define('PRODUCT_IMAGE_THUMBNAIL_GREYSCALE_TITLE' , 'Miniaturas de artículos:Greyscale<br /><img src="images/config_greyscale.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_GREYSCALE_DESC' , 'Miniaturas de artículos:Greyscale<br /><br />Default Valor: (32,22,22)<br /><br />basic black n white<br />Uso:<br />(int red,int green,int blue)');

define('PRODUCT_IMAGE_THUMBNAIL_ELLIPSE_TITLE' , 'Miniaturas de artículos:Ellipse<br /><img src="images/config_eclipse.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_ELLIPSE_DESC' , 'Miniaturas de artículos:Ellipse<br /><br />Default Valor: (FFFFFF)<br /><br />ellipse on bg colour<br />Uso:<br />(hex background colour)');

define('PRODUCT_IMAGE_THUMBNAIL_ROUND_EDGES_TITLE' , 'Miniaturas de artículos:Round-edges<br /><img src="images/config_edge.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_ROUND_EDGES_DESC' , 'Miniaturas de artículos:Round-edges<br /><br />Default Valor: (5,FFFFFF,3)<br /><br />corner trimming<br />Uso:<br />(edge_radius,background colour,anti-alias width)');

define('PRODUCT_IMAGE_THUMBNAIL_MERGE_TITLE' , 'Miniaturas de artículos:Merge<br /><img src="images/config_merge.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_MERGE_DESC' , 'Miniaturas de artículos:Merge<br /><br />Default Valor: (overlay.gif,10,-50,60,FF0000)<br /><br />overlay merge image<br />Uso:<br />(merge image,x start [neg = from right],y start [neg = from base],opacity, transparent colour on merge image)');

define('PRODUCT_IMAGE_THUMBNAIL_FRAME_TITLE' , 'Miniaturas de artículos:Frame<br /><img src="images/config_frame.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_FRAME_DESC' , 'Miniaturas de artículos:Frame<br /><br />Default Valor: (FFFFFF,000000,3,EEEEEE)<br /><br />plain raised border<br />Uso:<br />(hex light colour,hex dark colour,int width of mid bit,hex frame colour [optional - defaults to half way between light and dark edges])');

define('PRODUCT_IMAGE_THUMBNAIL_DROP_SHADOW_TITLE' , 'Miniaturas de artículos:Drop-Shadow<br /><img src="images/config_shadow.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_DROP_SHADOW_DESC' , 'Miniaturas de artículos:Drop-Shadow<br /><br />Default Valor: (3,333333,FFFFFF)<br /><br />more like a dodgy motion blur [semi buggy]<br />Uso:<br />(shadow width,hex shadow colour,hex background colour)');

define('PRODUCT_IMAGE_THUMBNAIL_MOTION_BLUR_TITLE' , 'Miniaturas de artículos:Motion-Blur<br /><img src="images/config_motion.gif">');
define('PRODUCT_IMAGE_THUMBNAIL_MOTION_BLUR_DESC' , 'Miniaturas de artículos:Motion-Blur<br /><br />Default Valor: (4,FFFFFF)<br /><br />fading parallel lines<br />Uso:<br />(int number of lines,hex background colour)');

//And this is for the Images showing your products in single-view

define('PRODUCT_IMAGE_INFO_BEVEL_TITLE' , 'Imágenes de la información de artículos:Bevel');
define('PRODUCT_IMAGE_INFO_BEVEL_DESC' , 'Imágenes de la información de artículos:Bevel<br /><br />Default Valor: (8,FFCCCC,330000)<br /><br />shaded bevelled edges<br />Uso:<br />(edge width, hex light colour, hex dark colour)');

define('PRODUCT_IMAGE_INFO_GREYSCALE_TITLE' , 'Imágenes de la información de artículos:Greyscale');
define('PRODUCT_IMAGE_INFO_GREYSCALE_DESC' , 'Imágenes de la información de artículos:Greyscale<br /><br />Default Valor: (32,22,22)<br /><br />basic black n white<br />Uso:<br />(int red, int green, int blue)');

define('PRODUCT_IMAGE_INFO_ELLIPSE_TITLE' , 'Imágenes de la información de artículos:Ellipse');
define('PRODUCT_IMAGE_INFO_ELLIPSE_DESC' , 'Imágenes de la información de artículos:Ellipse<br /><br />Default Valor: (FFFFFF)<br /><br />ellipse on bg colour<br />Uso:<br />(hex background colour)');

define('PRODUCT_IMAGE_INFO_ROUND_EDGES_TITLE' , 'Imágenes de la información de artículos:Round-edges');
define('PRODUCT_IMAGE_INFO_ROUND_EDGES_DESC' , 'Imágenes de la información de artículos:Round-edges<br /><br />Default Valor: (5,FFFFFF,3)<br /><br />corner trimming<br />Uso:<br />( edge_radius, background colour, anti-alias width)');

define('PRODUCT_IMAGE_INFO_MERGE_TITLE' , 'Imágenes de la información de artículos:Merge');
define('PRODUCT_IMAGE_INFO_MERGE_DESC' , 'Imágenes de la información de artículos:Merge<br /><br />Default Valor: (overlay.gif,10,-50,60,FF0000)<br /><br />overlay merge image<br />Uso:<br />(merge image,x start [neg = from right],y start [neg = from base],opacity,transparent colour on merge image)');

define('PRODUCT_IMAGE_INFO_FRAME_TITLE' , 'Imágenes de la información de artículos:Frame');
define('PRODUCT_IMAGE_INFO_FRAME_DESC' , 'Imágenes de la información de artículos:Frame<br /><br />Default Valor: (FFFFFF,000000,3,EEEEEE)<br /><br />plain raised border<br />Uso:<br />(hex light colour,hex dark colour,int width of mid bit,hex frame colour [optional - defaults to half way between light and dark edges])');

define('PRODUCT_IMAGE_INFO_DROP_SHADOW_TITLE' , 'Imágenes de la información de artículos:Drop-Shadow');
define('PRODUCT_IMAGE_INFO_DROP_SHADOW_DESC' , 'Imágenes de la información de artículos:Drop-Shadow<br /><br />Default Valor: (3,333333,FFFFFF)<br /><br />more like a dodgy motion blur [semi buggy]<br />Uso:<br />(shadow width,hex shadow colour,hex background colour)');

define('PRODUCT_IMAGE_INFO_MOTION_BLUR_TITLE' , 'Imágenes de la información de artículos:Motion-Blur');
define('PRODUCT_IMAGE_INFO_MOTION_BLUR_DESC' , 'Imágenes de la información de artículos:Motion-Blur<br /><br />Default Valor: (4,FFFFFF)<br /><br />fading parallel lines<br />Uso:<br />(int number of lines,hex background colour)');

define('PRODUCT_IMAGE_POPUP_BEVEL_TITLE' , 'Imágenes popup de artículos:Bevel');
define('PRODUCT_IMAGE_POPUP_BEVEL_DESC' , 'Imágenes popup de artículos:Bevel<br /><br />Default Valor: (8,FFCCCC,330000)<br /><br />shaded bevelled edges<br />Uso:<br />(edge width,hex light colour,hex dark colour)');

define('PRODUCT_IMAGE_POPUP_GREYSCALE_TITLE' , 'Imágenes popup de artículos:Greyscale');
define('PRODUCT_IMAGE_POPUP_GREYSCALE_DESC' , 'Imágenes popup de artículos:Greyscale<br /><br />Default Valor: (32,22,22)<br /><br />basic black n white<br />Uso:<br />(int red,int green,int blue)');

define('PRODUCT_IMAGE_POPUP_ELLIPSE_TITLE' , 'Imágenes popup de artículos:Ellipse');
define('PRODUCT_IMAGE_POPUP_ELLIPSE_DESC' , 'Imágenes popup de artículos:Ellipse<br /><br />Default Valor: (FFFFFF)<br /><br />ellipse on bg colour<br />Uso::<br />(hex background colour)');

define('PRODUCT_IMAGE_POPUP_ROUND_EDGES_TITLE' , 'Imágenes popup de artículos:Round-edges');
define('PRODUCT_IMAGE_POPUP_ROUND_EDGES_DESC' , 'Imágenes popup de artículos:Round-edges<br /><br />Default Valor: (5,FFFFFF,3)<br /><br />corner trimming<br />Uso:<br />(edge_radius,background colour,anti-alias width)');

define('PRODUCT_IMAGE_POPUP_MERGE_TITLE' , 'Imágenes popup de artículos:Merge');
define('PRODUCT_IMAGE_POPUP_MERGE_DESC' , 'Imágenes popup de artículos:Merge<br /><br />Default Valor: (overlay.gif,10,-50,60,FF0000)<br /><br />overlay merge image<br />Uso:<br />(merge image,x start [neg = from right],y start [neg = from base],opacity,transparent colour on merge image)');

define('PRODUCT_IMAGE_POPUP_FRAME_TITLE' , 'Imágenes popup de artículos:Frame');
define('PRODUCT_IMAGE_POPUP_FRAME_DESC' , 'Imágenes popup de artículos:Frame<br /><br />Default Valor: (FFFFFF,000000,3,EEEEEE)<br /><br />plain raised border<br />Uso:<br />(hex light colour,hex dark colour,int width of mid bit,hex frame colour [optional - defaults to half way between light and dark edges])');

define('PRODUCT_IMAGE_POPUP_DROP_SHADOW_TITLE' , 'Imágenes popup de artículos:Drop-Shadow');
define('PRODUCT_IMAGE_POPUP_DROP_SHADOW_DESC' , 'Imágenes popup de artículos:Drop-Shadow<br /><br />Default Valor: (3,333333,FFFFFF)<br /><br />more like a dodgy motion blur [semi buggy]<br />Uso:<br />(shadow width,hex shadow colour,hex background colour)');

define('PRODUCT_IMAGE_POPUP_MOTION_BLUR_TITLE' , 'Imágenes popup de artículos:Motion-Blur');
define('PRODUCT_IMAGE_POPUP_MOTION_BLUR_DESC' , 'Imágenes popup de artículos:Motion-Blur<br /><br />Default Valor: (4,FFFFFF)<br /><br />fading parallel lines<br />Uso:<br />(int number of lines,hex background colour)');

define('IMAGE_MANIPULATOR_TITLE', 'GDlib processing');
define('IMAGE_MANIPULATOR_DESC', 'Image Manipulator para GD2 o GD1<br /><br /><b>AVISO:</b> image_manipulator_GD2_advanced.php apoya PNG\'s transparentes');


define('ACCOUNT_GENDER_TITLE' , 'Género');
define('ACCOUNT_GENDER_DESC' , 'Consultar el género al abrir o editar la cuenta');
define('ACCOUNT_DOB_TITLE' , 'Fecha de nacimiento');
define('ACCOUNT_DOB_DESC' , 'Consultar la fecha de nacimiento al abrir o editar la cuenta');
define('ACCOUNT_COMPANY_TITLE' , 'Empresa');
define('ACCOUNT_COMPANY_DESC' , 'Consultar la empresa al abrir o editar la cuenta');
define('ACCOUNT_SUBURB_TITLE' , 'Dirección adicional');
define('ACCOUNT_SUBURB_DESC' , 'Consultar la dirección adicional al abrir o editar la cuenta');
define('ACCOUNT_STATE_TITLE' , 'Estado federal');
define('ACCOUNT_STATE_DESC' , 'Consultar el estado federal al abrir o editar la cuenta');

define('DEFAULT_CURRENCY_TITLE' , 'Moneda por defecto');
define('DEFAULT_CURRENCY_DESC' , 'Moneda que se utiliza por defecto');
define('DEFAULT_LANGUAGE_TITLE' , 'Idioma por defecto');
define('DEFAULT_LANGUAGE_DESC' , 'Idioma que se utiliza por defecto');
define('DEFAULT_ORDERS_STATUS_ID_TITLE' , 'Estado de pedido estándar para nuevos pedidos');
define('DEFAULT_ORDERS_STATUS_ID_DESC' , 'Cuando se recibe un nuevo pedido, este estado se fija como el estado del pedido.');

define('SHIPPING_MAX_WEIGHT_TITLE' , 'Peso máximo que puede enviarse en un solo paquete');
define('SHIPPING_MAX_WEIGHT_DESC' , 'Los socios de envío (Correo/UPS, etc.) tienen un peso máximo del paquete. Introduzca un valor para ello.');
define('SHIPPING_BOX_WEIGHT_TITLE' , 'Peso del paquete vacío');
define('SHIPPING_BOX_WEIGHT_DESC' , '¿Cuál es el peso de un paquete vacío promedio de pequeño a mediano?');
define('SHIPPING_BOX_PADDING_TITLE' , 'Para envases vacíos de mayor tamaño - aumento de peso en %');
define('SHIPPING_BOX_PADDING_DESC' , 'Para aproximadamente 10%, ingrese 10');
define('SHOW_SHIPPING_TITLE' , 'Visualización costos de envío');
define('SHOW_SHIPPING_DESC' , 'Visualización enlazada para "más costos de envío"');
define('SHIPPING_INFOS_TITLE' , 'Costos de envío');
define('SHIPPING_INFOS_DESC' , 'Seleccione el contenido para la visualización de los costos de envío');
define('SHIPPING_DEFAULT_TAX_CLASS_METHOD_TITLE' , 'Método de cálculo de la categoría fiscal por defecto');
define('SHIPPING_DEFAULT_TAX_CLASS_METHOD_DESC' , 'ninguno: no mostrar ningún impuesto de costos de envío<br />auto proporcional: mostrar impuesto de costos de envío proporcionalmente al pedido<br />auto max: mostrar la tasa de impuesto del mayor grupo de ventas como impuesto de costos de envío');

define('PRODUCT_LIST_FILTER_TITLE' , '¿Visualización de los filtros de clasificación en las listas de productos?');
define('PRODUCT_LIST_FILTER_DESC' , 'Visualización de los filtros de clasificación para grupos de productos/fabricantes, etc. Filtro (false=inactivo; true=activo)');

define('STOCK_CHECK_TITLE' , 'Comprobar el inventario');
define('STOCK_CHECK_DESC' , 'Comprobar si se dispone de mercancía suficiente para entregar los pedidos.');

define('ATTRIBUTE_STOCK_CHECK_TITLE' , 'Comprobar el inventario de atributos de artículo');
define('ATTRIBUTE_STOCK_CHECK_DESC' , 'Comprobar el inventario de mercancías con determinados atributos de artículo.');
define('STOCK_LIMITED_TITLE' , 'Deducir cantidad de mercancía');
define('STOCK_LIMITED_DESC' , 'Deducir la cantidad de mercancías en stock cuando se hayan pedido las mercancías.');
define('STOCK_ALLOW_CHECKOUT_TITLE' , 'Permitir compras de mercancía agotada');
define('STOCK_ALLOW_CHECKOUT_DESC' , '¿Quiere permitir el pedido incluso si cierta mercancía no está disponible según el inventario?');
define('STOCK_MARK_PRODUCT_OUT_OF_STOCK_TITLE' , 'Identificación de artículos agotados');
define('STOCK_MARK_PRODUCT_OUT_OF_STOCK_DESC' , 'Señalar al cliente los artículos que ya no están disponibles.');
define('STOCK_REORDER_LEVEL_TITLE' , 'Notificación al admin que un artículo tiene que ser reordenado.');
define('STOCK_REORDER_LEVEL_DESC' , '¿Cuál es la cantidad de unidades para que aparezca esta notificación? (FUNCIÓN PLANIFICADA)');
define('STORE_PAGE_PARSE_TIME_TITLE' , 'Guardar el tiempo de calculación para la carga de la página de la tienda');
define('STORE_PAGE_PARSE_TIME_DESC' , 'Guarda el tiempo que se necesita para calcular los scripts hasta la aparición de la página.');
define('STORE_PARSE_DATE_TIME_FORMAT_TITLE' , 'Formato de fecha en el fichero log');
define('STORE_PARSE_DATE_TIME_FORMAT_DESC' , 'El formato de fecha para el registro (predeterminado: %d/%m/%Y %H:%M:%S)');
define('STORE_DB_SLOW_QUERY_TITLE' , 'Slow Query Log');
define('STORE_DB_SLOW_QUERY_DESC' , 'Sólo se deben guardar las SQL Queries que tardan mucho tiempo.<br/><strong>Atención: ¡El almacenamiento de las consultas de la base de datos debe estar activado!</strong>.<br/><strong>Atención: ¡El archivo puede volverse muy grande mientras más largo sea el tiempo de ejecución!</strong>.<br/><br/>El archivo log se almacena en la carpeta /log en el directorio principal.');
define('STORE_DB_SLOW_QUERY_TIME_TITLE' , 'Slow Query Log - Tiempo');
define('STORE_DB_SLOW_QUERY_TIME_DESC' , 'Por favor, introduzca el tiempo a partir del cual las SQL Queries se escriben en el archivo log.');

define('DISPLAY_PAGE_PARSE_TIME_TITLE' , 'Mostrar tiempos de cálculo de las páginas');
define('DISPLAY_PAGE_PARSE_TIME_DESC' , 'Si está activada la opción de guardar los tiempos de cálculo para las páginas, pueden mostrarse en el pie de página.<br /><strong>deactivado</strong>: Desactiva la visualización completamente <br /><strong>admin</strong>: Sólo el admin ve los tiempos de cálculo<br /><strong>todos</strong>: Todos ven los tiempos de cálculo.');
define('STORE_DB_TRANSACTIONS_TITLE' , 'Almacenamiento de las consultas de la base de datos');
define('STORE_DB_TRANSACTIONS_DESC' , 'Almacenamiento de cada consulta de la base de datos en el archivo log para los tiempos de cálculo <br/><strong>Atención: ¡El archivo puede volverse muy grande mientras más largo sea el tiempo de ejecución!</strong>.<br/><br/>El archivo log se almacena en la carpeta /log en el directorio principal.');

define('USE_CACHE_TITLE' , 'Usar Caché');
define('USE_CACHE_DESC' , 'Utilizar funciones de Caché');

define('DB_CACHE_TITLE', 'Caché DB');
define('DB_CACHE_DESC', 'Las consultas a la base de datos pueden ser almacenadas en caché por la tienda para reducir la carga de la base de datos y aumentar la velocidad.');

define('DB_CACHE_EXPIRE_TITLE', 'Vida útil de la caché DB');
define('DB_CACHE_EXPIRE_DESC', 'Tiempo en segundos antes de que los archivos caché se sobrescriban automáticamente con datos de la base de datos.');

define('DIR_FS_CACHE_TITLE' , 'Carpeta de caché');
define('DIR_FS_CACHE_DESC' , 'La carpeta donde se deben guardar los archivos obtenidos del caché');

define('ACCOUNT_OPTIONS_TITLE', 'Tipo de creación de la cuenta');
define('ACCOUNT_OPTIONS_DESC', '¿Cómo le gustaría diseñar el procedimiento de inicio de sesión en su tienda?<br />Usted tiene la opción entre las cuentas de clientes regulares y los "pedidos únicos" sin crear una cuenta de cliente (se crea una cuenta, pero esto no es visible para el cliente)');

define('EMAIL_TRANSPORT_TITLE' , 'Método de transporte del correo electrónico');
define('EMAIL_TRANSPORT_DESC' , '<b>Recomendación: smtp</b> - Define si el servidor utiliza una conexión local al "programa sendmail" o si necesita una conexión SMTP vía TCP/IP. Los servidores que se ejecutan en Windows o Mac OS deberían utilizar SMTP.');

define('EMAIL_LINEFEED_TITLE' , 'feeds de línea de correo electrónico');
define('EMAIL_LINEFEED_DESC' , 'Define los caracteres que se deben utilizar para separar las cabeceras de los correos electrónicos.');
define('EMAIL_USE_HTML_TITLE' , 'Uso de MIME HTML al enviar correos electrónicos');
define('EMAIL_USE_HTML_DESC' , 'Enviar correos electrónicos en formato HTML');
define('ENTRY_EMAIL_ADDRESS_CHECK_TITLE' , 'Comprobar direcciones de correo electrónico a través de DNS');
define('ENTRY_EMAIL_ADDRESS_CHECK_DESC' , 'Las direcciones de correo electrónico se pueden comprobar a través de un servidor DNS');
define('SEND_EMAILS_TITLE' , 'Envío de correos electrónicos');
define('SEND_EMAILS_DESC' , 'Enviar correos electrónicos a los clientes (para pedidos, etc.)');
define('SENDMAIL_PATH_TITLE' , 'La ruta a Sendmail');
define('SENDMAIL_PATH_DESC' , 'Si utiliza Sendmail, especifique la ruta al programa Sendmail (normalmente: /usr/bin/sendmail):');
define('SMTP_MAIN_SERVER_TITLE' , 'Dirección del servidor SMTP');
define('SMTP_MAIN_SERVER_DESC' , 'Introduzca la dirección de su servidor SMTP principal');
define('SMTP_BACKUP_SERVER_TITLE' , 'Dirección del servidor de respaldo SMTP');
define('SMTP_BACKUP_SERVER_DESC' , 'Introduzca la dirección de su servidor SMTP de respaldo');
define('SMTP_USERNAME_TITLE' , 'Nombre de usuario SMTP');
define('SMTP_USERNAME_DESC' , 'Por favor, introduzca el nombre de usuario de su cuenta SMTP aquí.');
define('SMTP_PASSWORD_TITLE' , 'Contraseña SMTP');
define('SMTP_PASSWORD_DESC' , 'Por favor, introduzca la contraseña de su cuenta SMTP aquí.');
define('SMTP_AUTH_TITLE' , 'Autorización SMTP');
define('SMTP_AUTH_DESC' , '¿El servidor SMTP requiere una autentificación segura?');
define('SMTP_PORT_TITLE' , 'Port SMTP');
define('SMTP_PORT_DESC' , 'Introduzca el Port SMTP de su servidor SMTP (por defecto: 25)');

//DokuMan - 2011-09-20 - E-Mail SQL errors
define('EMAIL_SQL_ERRORS_TITLE', 'Enviar notificaciones de error SQL como correo electrónico');
define('EMAIL_SQL_ERRORS_DESC', 'Si es "true", se envía un correo electrónico con la notificación de error SQL a la dirección de correo electrónico del operador de la tienda. Por otro lado, la notificación de error SQL se oculta al cliente.<br />Si es "false", la notificación de error correspondiente se muestra directamente y es visible para todos (por defecto).');

//Constants for contact_us
define('CONTACT_US_EMAIL_ADDRESS_TITLE' , 'Contacto - Dirección de correo electrónico');
define('CONTACT_US_EMAIL_ADDRESS_DESC' , 'Por favor, introduzca una dirección de remitente correcta para enviar los correos electrónicos a través del formulario "Contacto".');
define('CONTACT_US_NAME_TITLE' , 'Contacto - Dirección de correo electrónico, nombre');
define('CONTACT_US_NAME_DESC' , 'Por favor, introduzca un nombre de remitente para enviar los correos electrónicos a través del formulario "Contacto".');
define('CONTACT_US_FORWARDING_STRING_TITLE' , 'Contacto - Direcciones de correo electrónico de reenvío');
define('CONTACT_US_FORWARDING_STRING_DESC' , 'Introduzca las direcciones de correo electrónico adicionales a las que se deben enviar también los correos electrónicos del formulario "Contacto" (separadas por , )');
define('CONTACT_US_REPLY_ADDRESS_TITLE' , 'Contacto - Dirección de correo electrónico de respuesta');
define('CONTACT_US_REPLY_ADDRESS_DESC' , 'Por favor, introduzca una dirección de correo electrónico a la que sus clientes puedan responder.');
define('CONTACT_US_REPLY_ADDRESS_NAME_TITLE' , 'Contacto - Dirección de correo electrónico de respuesta, nombre');
define('CONTACT_US_REPLY_ADDRESS_NAME_DESC' , 'Nombre del remitente para los correos de respuesta.');
define('CONTACT_US_EMAIL_SUBJECT_TITLE' , 'Contacto - Asunto del correo electrónico');
define('CONTACT_US_EMAIL_SUBJECT_DESC' , 'Asunto de los correos electrónicos del formulario de contacto de la tienda');

//Constants for support system
define('EMAIL_SUPPORT_ADDRESS_TITLE' , 'Soporte Técnico - Dirección de correo electrónico');
define('EMAIL_SUPPORT_ADDRESS_DESC' , 'Por favor, introduzca una dirección de remitente correcta para enviar los correos electrónicos a través del <b>Sistema de soporte</b>(creación de cuenta, contraseña olvidada).');
define('EMAIL_SUPPORT_NAME_TITLE' , 'Soporte Técnico - Dirección de correo electrónico, nombre');
define('EMAIL_SUPPORT_NAME_DESC' , 'Por favor, introduzca un nombre de remitente para enviar los correos electrónicos a través del <b>Sistema de soporte</b> (creación de cuenta, contraseña olvidada).');
define('EMAIL_SUPPORT_FORWARDING_STRING_TITLE' , 'Soporte Técnico - Direcciones de correo electrónico de reenvío');
define('EMAIL_SUPPORT_FORWARDING_STRING_DESC' , 'Introduzca las direcciones de correo electrónico adicionales a las que se deben enviar también los correos electrónicos del <b>Sistema de soporte</b> (separadas por , )');
define('EMAIL_SUPPORT_REPLY_ADDRESS_TITLE' , 'Soporte Técnico - Dirección de correo electrónico de respuesta');
define('EMAIL_SUPPORT_REPLY_ADDRESS_DESC' , 'Por favor, introduzca una dirección de correo electrónico a la que sus clientes puedan responder.');
define('EMAIL_SUPPORT_REPLY_ADDRESS_NAME_TITLE' , 'Soporte Técnico - Dirección de correo electrónico de respuesta, nombre');
define('EMAIL_SUPPORT_REPLY_ADDRESS_NAME_DESC' , 'Nombre del remitente para los correos de respuesta.');
define('EMAIL_SUPPORT_SUBJECT_TITLE' , 'Soporte técnico - Asunto del correo electrónico');
define('EMAIL_SUPPORT_SUBJECT_DESC' , 'Asunto de los correos electrónicos de <b>Sistema de soporte</b>.');

//Constants for Billing system
define('EMAIL_BILLING_ADDRESS_TITLE' , 'Facturación - Dirección de correo electrónico');
define('EMAIL_BILLING_ADDRESS_DESC' , 'Por favor, introduzca una dirección de remitente correcta para enviar los correos electrónicos a través del <b>Sistema de facturación</b> (confirmación de pedido, cambios de estado,...).');
define('EMAIL_BILLING_NAME_TITLE' , 'Facturación - Dirección de correo electrónico, nombre');
define('EMAIL_BILLING_NAME_DESC' , 'Por favor, introduzca un nombre de remitente para enviar los correos electrónicos a través del <b>Sistema de facturación</b> (confirmación de pedido, cambios de estado,...).');
define('EMAIL_BILLING_FORWARDING_STRING_TITLE' , 'Facturación - Direcciones de correo electrónico de reenvío');
define('EMAIL_BILLING_FORWARDING_STRING_DESC' , 'Introduzca las direcciones de correo electrónico adicionales a las que se deben enviar también los correos electrónicos del <b>Sistema de facturación</b> (separadas por , )');
define('EMAIL_BILLING_REPLY_ADDRESS_TITLE' , 'Facturación - Dirección de correo electrónico de respuesta');
define('EMAIL_BILLING_REPLY_ADDRESS_DESC' , 'Por favor, introduzca una dirección de correo electrónico a la que sus clientes puedan responder.');
define('EMAIL_BILLING_REPLY_ADDRESS_NAME_TITLE' , 'Facturación - Dirección de correo electrónico de respuesta, nombre');
define('EMAIL_BILLING_REPLY_ADDRESS_NAME_DESC' , 'Nombre del remitente para los correos de respuesta.');
define('EMAIL_BILLING_SUBJECT_TITLE' , 'Facturación - Asunto del correo electrónico para cambios de estado');
define('EMAIL_BILLING_SUBJECT_DESC' , 'Por favor, introduzca un asunto de correo electrónico para los correos electrónicos del <b>sistema de facturación </b> de su tienda (cambios de estado).');
define('EMAIL_BILLING_SUBJECT_ORDER_TITLE', 'Facturación - Asunto del correo electrónico para pedidos');
define('EMAIL_BILLING_SUBJECT_ORDER_DESC', 'Por favor, introduzca un asunto de correo electrónico para sus correos electrónicos de pedido. (p.ej.: <b>Su pedido {$nr},del {$date}</b>) pd: están disponibles las siguientes variables, {$nr},{$date},{$firstname},{$lastname}');
define('MODULE_ORDER_MAIL_STEP_SUBJECT_TITLE', 'Facturación - Asunto del correo electrónico para confirmación de pedido');
define('MODULE_ORDER_MAIL_STEP_SUBJECT_DESC', 'Por favor, introduzca un asunto de correo electrónico para su confirmación de pedido. (p.ej.: <b>Su pedido {$nr},del {$date}</b>) pd: están disponibles las siguientes variables, {$nr},{$date},{$firstname},{$lastname}');

define('DOWNLOAD_ENABLED_TITLE' , 'Permitir descarga de artículos');
define('DOWNLOAD_ENABLED_DESC' , 'Habilitar las funciones de descarga de artículos (software, etc.).');
define('DOWNLOAD_BY_REDIRECT_TITLE' , 'Descarga por redirección');
define('DOWNLOAD_BY_REDIRECT_DESC' , 'Utilizar la redirección del navegador para la descarga de artículos. Desactivar sistemas que no sean Linux/Unix.');
define('DOWNLOAD_MAX_DAYS_TITLE' , 'Fecha de caducidad de los enlaces de descarga (días)');
define('DOWNLOAD_MAX_DAYS_DESC' , 'Número de días que un enlace de descarga permanece activo para el cliente. 0 significa sin límite.');
define('DOWNLOAD_MAX_COUNT_TITLE' , 'Número máximo de descargas de un producto multimedia adquirido');
define('DOWNLOAD_MAX_COUNT_DESC' , 'Configure el número máximo de descargas que permite al cliente que ha comprado un artículo de este tipo. 0 significa que no hay descarga.');
define('DOWNLOAD_MULTIPLE_ATTRIBUTES_ALLOWED_TITLE' , 'Atributos múltiples para descargas');
define('DOWNLOAD_MULTIPLE_ATTRIBUTES_ALLOWED_DESC' , 'Se deben permitir atributos múltiples para artículos de descarga de modo que se salte el modo de envío.');

define('GZIP_COMPRESSION_TITLE' , 'Habilitar la compresión GZip');
define('GZIP_COMPRESSION_DESC' , 'Active la compresión HTTP GZip para optimizar la velocidad de la carga de la página.');
define('GZIP_LEVEL_TITLE' , 'Nivel de compresión');
define('GZIP_LEVEL_DESC' , 'Seleccione un nivel de compresión entre 0-9 (0 = mínimo, 9 = máximo).');

define('SESSION_WARNING', '<br /><br /><span class="col-red"><strong>ATENCIÓN:</strong></span> Esta función puede afectar a la funcionalidad de la tienda. ¡Por favor, sólo cambie si es consciente de las posibles consecuencias y el servidor realmente soporta esta función!');

define('SESSION_WRITE_DIRECTORY_TITLE' , 'Ubicación de memoria de la sesión');
define('SESSION_WRITE_DIRECTORY_DESC' , 'En caso de que se deben guardar las sesiones como archivos, utilice la siguiente carpeta.');
define('SESSION_FORCE_COOKIE_USE_TITLE' , 'Preferir el uso de cookies');
define('SESSION_FORCE_COOKIE_USE_DESC' , 'Iniciar sesión si el navegador permite cookies. (por defecto "false")'.SESSION_WARNING);
define('SESSION_CHECK_SSL_SESSION_ID_TITLE' , 'Comprobar el ID de la sesión SSL');
define('SESSION_CHECK_SSL_SESSION_ID_DESC' , 'Comprobar el SSL_SESSION_ID cada vez que se accede a la página HTTPS. (por defecto "false")'.SESSION_WARNING);
define('SESSION_CHECK_USER_AGENT_TITLE' , 'Comprobar el Agente de Usuario');
define('SESSION_CHECK_USER_AGENT_DESC' , 'Comprobar el agente de usuario del navegador del usuario cada vez que se accede a la página (por defecto "false")'.SESSION_WARNING);
define('SESSION_CHECK_IP_ADDRESS_TITLE' , 'Comprobar la dirección IP');
define('SESSION_CHECK_IP_ADDRESS_DESC' , 'Comprobar la dirección IP del usuario cada vez que se accede a la página. (por defecto "false")'.SESSION_WARNING);
define('SESSION_RECREATE_TITLE' , 'Renovar sesión');
define('SESSION_RECREATE_DESC' , 'Renovar la sesión y asignar un nuevo ID de sesión tan pronto como el usuario inicie sesión o se registre (PHP >=4.1 necesario). (por defecto "false")'.SESSION_WARNING);

define('DISPLAY_CONDITIONS_ON_CHECKOUT_TITLE' , 'Firma de los Términos y Condiciones');
define('DISPLAY_CONDITIONS_ON_CHECKOUT_DESC' , 'Visualización y firma de los Términos y Condiciones durante el proceso de pedido');

define('META_MIN_KEYWORD_LENGTH_TITLE' , 'Longitud mínima de las meta-palabras clave');
define('META_MIN_KEYWORD_LENGTH_DESC' , 'Longitud mínima de las meta-palabras clave generadas automáticamente (descripción del artículo)');
define('META_KEYWORDS_NUMBER_TITLE' , 'Número de meta-palabras clave');
define('META_KEYWORDS_NUMBER_DESC' , 'Número de meta-palabras clave');
define('META_AUTHOR_TITLE' , 'author');
define('META_AUTHOR_DESC' , '<meta name="author">');
define('META_PUBLISHER_TITLE' , 'publisher');
define('META_PUBLISHER_DESC' , '<meta name="publisher">');
define('META_COMPANY_TITLE' , 'company');
define('META_COMPANY_DESC' , '<meta name="company">');
define('META_TOPIC_TITLE' , 'page-topic');
define('META_TOPIC_DESC' , '<meta name="page-topic">');
define('META_REPLY_TO_TITLE' , 'reply-to');
define('META_REPLY_TO_DESC' , '<meta name="reply-to">');
define('META_REVISIT_AFTER_TITLE' , 'revisit-after');
define('META_REVISIT_AFTER_DESC' , '<meta name="revisit-after">');
define('META_ROBOTS_TITLE' , 'robots');
define('META_ROBOTS_DESC' , '<meta name="robots">');
define('META_DESCRIPTION_TITLE' , 'Description');
define('META_DESCRIPTION_DESC' , '<meta name="description">');
define('META_KEYWORDS_TITLE' , 'Keywords');
define('META_KEYWORDS_DESC' , '<meta name="keywords">');

define('MODULE_PAYMENT_INSTALLED_TITLE' , 'Módulos de pago instalados');
define('MODULE_PAYMENT_INSTALLED_DESC' , 'Lista de nombres de archivos del módulo de pago (separados por punto y coma (;)). Se actualiza automáticamente, por lo que no es necesario editarla. (Ejemplo: cc.php;cod.php;paypal.php)');
define('MODULE_ORDER_TOTAL_INSTALLED_TITLE' , 'Módulos de Order Total instalados');
define('MODULE_ORDER_TOTAL_INSTALLED_DESC' , 'Lista de nombres de archivos del módulo de Order Total (separados por punto y coma (;)). Se actualiza automáticamente, por lo que no es necesario editarla. (Ejemplo: ot_subtotal.php;ot_tax.php;ot_shipping.php;ot_total.php)');
define('MODULE_SHIPPING_INSTALLED_TITLE' , 'Módulos de envío instalados');
define('MODULE_SHIPPING_INSTALLED_DESC' , 'Lista de nombres de archivos del módulo de envío (separados por punto y coma (;)). Se actualiza automáticamente, por lo que no es necesario editarla. (Ejemplo:  ups.php;flat.php;item.php)');

define('CACHE_LIFETIME_TITLE', 'Vida útil de la caché');
define('CACHE_LIFETIME_DESC', 'Tiempo en segundos antes de que los archivos de caché se sobrescriban automáticamente.');
define('CACHE_CHECK_TITLE', 'Comprueba si la caché ha sido modificada');
define('CACHE_CHECK_DESC', 'Si es "true", entonces se consideran las cabeceras If-Modified-Since para el contenido en caché, y se emiten las cabeceras HTTP correspondientes. Esto significa que las páginas que se visitan regularmente no se envían cada vez de nuevo al cliente.');

define('PRODUCT_REVIEWS_VIEW_TITLE', 'Reseñas en los detalles del artículo');
define('PRODUCT_REVIEWS_VIEW_DESC', 'Número de reseñas mostradas en la vista detallada del artículo');

define('DELETE_GUEST_ACCOUNT_TITLE', 'Borrar cuentas de invitado');
define('DELETE_GUEST_ACCOUNT_DESC', '¿Deben borrarse las cuentas de invitados una vez realizado el pedido? (Se conservan los datos del pedido)');

define('USE_WYSIWYG_TITLE', 'Activar el editor WYSIWYG');
define('USE_WYSIWYG_DESC', '¿Activar el editor WYSIWYG para CMS y artículos?');

define('PRICE_IS_BRUTTO_TITLE', 'Bruto Admin');
define('PRICE_IS_BRUTTO_DESC', 'Permite introducir los precios brutos en el Admin');

define('PRICE_PRECISION_TITLE', 'Decimales brutos/netos');
define('PRICE_PRECISION_DESC', 'Precisión de conversión (no afecta a la visualización en la tienda, siempre se visualizan 2 decimales allí.)');

define('CHECK_CLIENT_AGENT_TITLE', '¿Evitar las sesiones de Spider?');
define('CHECK_CLIENT_AGENT_DESC', 'Dejar Spiders conocidos de los motores de búsqueda a la página sin sesión.');
define('SHOW_IP_LOG_TITLE', 'Log de IP durante Checkout');
define('SHOW_IP_LOG_DESC', '¿Mostrar el texto "Su IP se guarda por razones de seguridad", durante el Checkout?');

define('ACTIVATE_GIFT_SYSTEM_TITLE', '¿Activar el sistema de cupones?');
define('ACTIVATE_GIFT_SYSTEM_DESC', '¿Activar el sistema de cupones?<br/><br/><b>Aviso: </b>Todavía hay que activar los módulos ot_coupon <a href="'.xtc_href_link(FILENAME_MODULES, 'set=ordertotal&module=ot_coupon').'"><b>aquí</b></a> y ot_gv <a href="'.xtc_href_link(FILENAME_MODULES, 'set=ordertotal&module=ot_gv').'"><b>aquí</b></a> .');

define('ACTIVATE_SHIPPING_STATUS_TITLE', '¿Activar la visualización del estado del envío?');
define('ACTIVATE_SHIPPING_STATUS_DESC', '¿Activar la visualización del estado del envío? (Se pueden establecer diferentes tiempos de envío para artículos individuales. Después de la activación aparece un nuevo elemento <b>Estado de entrega</b> al introducir el artículo)');

define('IMAGE_QUALITY_TITLE', 'Calidad de imagen');
define('IMAGE_QUALITY_DESC', 'Calidad de imagen (0= compresión máxima, 100= calidad óptima)');

define('GROUP_CHECK_TITLE', 'Verificación de grupo de clientes');
define('GROUP_CHECK_DESC', '¿Permitir sólo a ciertos grupos de clientes el acceso a categorías individuales, productos, elementos de contenido? (Después de la activación, las opciones de entrada aparecen para los artículos, categorías y en el gestor de contenidos)');

define('ACTIVATE_NAVIGATOR_TITLE', '¿Activar el navegador de artículos?');
define('ACTIVATE_NAVIGATOR_DESC', 'Activar/desactivar el navegador de artículos en la vista detallada de artículos (por razones de rendimiento al tener un número elevado de artículos)');

define('QUICKLINK_ACTIVATED_TITLE', 'Activar la función multienlace/copia');
define('QUICKLINK_ACTIVATED_DESC', 'La función multienlace/copia facilita la copia/el enlace de un artículo en varias categorías por permitir que se seleccionen categorías individuales mediante una casilla de verificación.');

define('ACTIVATE_REVERSE_CROSS_SELLING_TITLE', 'Comercialización cruzada inversa');
define('ACTIVATE_REVERSE_CROSS_SELLING_DESC', '¿Activar la función de comercialización cruzada inversa?');

define('DOWNLOAD_UNALLOWED_PAYMENT_TITLE', 'Módulos de pago de descarga no autorizados');
define('DOWNLOAD_UNALLOWED_PAYMENT_DESC', '<strong>NO</strong> Métodos de pago permitidos para productos de descarga separados por comas. Por ejemplo, {banktransfer,cod,invoice,moneyorder}');
define('DOWNLOAD_MIN_ORDERS_STATUS_TITLE', 'Estado de pedido');
define('DOWNLOAD_MIN_ORDERS_STATUS_DESC', 'Estado del pedido con el que se liberan las descargas ordenadas.');

// Vat ID
define('STORE_OWNER_VAT_ID_TITLE' , 'ID de IVA del operador de la tienda');
define('STORE_OWNER_VAT_ID_DESC' , 'El impuesto sobre las ventas de su empresa');
define('DEFAULT_CUSTOMERS_VAT_STATUS_ID_TITLE' , 'Estado del cliente para clientes verificados con ID de IVA (en el extranjero)');
define('DEFAULT_CUSTOMERS_VAT_STATUS_ID_DESC' , '¡Seleccione el estado de cliente (grupo) para clientes verificados con ID de IVA!');
define('ACCOUNT_COMPANY_VAT_CHECK_TITLE' , 'Consultar ID del impuesto sobre las ventas');
define('ACCOUNT_COMPANY_VAT_CHECK_DESC' , 'El ID del impuesto sobre las ventas debe poder ser introducido por los clientes. Si es false, el campo de entrada ya no se muestra.');
define('ACCOUNT_COMPANY_VAT_LIVE_CHECK_TITLE' , 'Comprobar la verosimilitud del ID del impuesto sobre las ventas en línea.');
define('ACCOUNT_COMPANY_VAT_LIVE_CHECK_DESC' , 'Se comprueba la verosimilitud en línea del ID del impuesto sobre las ventas. Para ello, se utiliza el servicio web del portal fiscal de la UE (<a href="http://ec.europa.eu/taxation_customs" style="font-style:italic">http://ec.europa.eu/taxation_customs</a>).<br/>¡Requiere PHP5 con el soporte "SOAP" activado!<br/><br/><span class="messageStackSuccess">¡Actualmente el soporte "PHP5 SOAP" '.(in_array ('soap', get_loaded_extensions()) ? '' : '<span class="messageStackError">NO</span>').' es activado!</span><br/><br/>');
define('ACCOUNT_COMPANY_VAT_GROUP_TITLE' , '¿Ajustar el grupo de clientes después de la comprobación del ID de IVA?');
define('ACCOUNT_COMPANY_VAT_GROUP_DESC' , 'Al habilitar esta opción, se cambia el grupo de clientes después de una comprobación positiva del ID de IVA.');
define('ACCOUNT_VAT_BLOCK_ERROR_TITLE' , '¿Bloquear la introducción de números de ID de IVA incorrectos o no verificados?');
define('ACCOUNT_VAT_BLOCK_ERROR_DESC' , 'Al habilitar esta opción, sólo se introducen los números de ID de IVA verificados y correctos.');
define('DEFAULT_CUSTOMERS_VAT_STATUS_ID_LOCAL_TITLE', 'Estado del cliente para clientes verificados con ID de IVA (nacionales)');
define('DEFAULT_CUSTOMERS_VAT_STATUS_ID_LOCAL_DESC', '¡Seleccione el estado de cliente (grupo) para clientes verificados con ID de IVA!');

// Google Conversion
define('GOOGLE_CONVERSION_TITLE', 'Google Conversion-Tracking');
define('GOOGLE_CONVERSION_DESC', 'Registro de palabras clave de Conversion para pedidos');
define('GOOGLE_CONVERSION_ID_TITLE', 'Conversion ID');
define('GOOGLE_CONVERSION_ID_DESC', 'SU Google Conversion ID');
define('GOOGLE_LANG_TITLE', 'Google Idioma');
define('GOOGLE_LANG_DESC', 'Código ISO del idioma utilizado');
define('GOOGLE_CONVERSION_LABEL_TITLE', 'Google Conversion Label');
define('GOOGLE_CONVERSION_LABEL_DESC', 'Su Google Conversion Label');

// Afterbuy
define('AFTERBUY_ACTIVATED_TITLE', 'Activado');
define('AFTERBUY_ACTIVATED_DESC', 'Activar la interfaz de Afterbuy');
define('AFTERBUY_PARTNERID_TITLE', 'ID de afiliado');
define('AFTERBUY_PARTNERID_DESC', 'Su ID de afiliado de Afterbuy');
define('AFTERBUY_PARTNERPASS_TITLE', 'Contraseña de afiliado');
define('AFTERBUY_PARTNERPASS_DESC', 'Su contraseña de afiliado para la interfaz XML de Afterbuy');
define('AFTERBUY_USERID_TITLE', 'ID de usuario');
define('AFTERBUY_USERID_DESC', 'Su ID dde usuario de Afterbuy');
define('AFTERBUY_ORDERSTATUS_TITLE', 'Estado de pedido');
define('AFTERBUY_ORDERSTATUS_DESC', 'Estado del pedido después de la transferencia exitosa de los datos del pedido');
define('AFTERBUY_URL', 'Una descripción de Afterbuy la puede encontrar aquí: <a href="http://www.afterbuy.de" target="new">http://www.afterbuy.de</a>');
define('AFTERBUY_DEALERS_TITLE', 'Marcar como Comerciante');
define('AFTERBUY_DEALERS_DESC', 'Introduzca aquí los ID de grupo de los comerciantes que van a entrar en Afterbuy como comerciantes. <br />Ejemplo: <em>6,5,8</em>. ¡No debe haber ningún espacio!');
define('AFTERBUY_IGNORE_GROUPE_TITLE', 'Ignorar grupo de clientes');
define('AFTERBUY_IGNORE_GROUPE_DESC', '¿Qué grupos de clientes deben ignorarse?<br />Ejemplo: <em>6,5,8</em>. ¡No debe haber ningún espacio!');

// Search-Options
define('SEARCH_IN_DESC_TITLE', 'Búsqueda en las descripciónes de los productos');
define('SEARCH_IN_DESC_DESC', 'Activar para permitir la búsqueda en las descripciónes de los productos (corto + largo)');
define('SEARCH_IN_ATTR_TITLE', 'Búsqueda en los atributos del producto');
define('SEARCH_IN_ATTR_DESC', 'Activar para permitir la búsqueda en los atributos del producto (por ejemplo, color, longitud)');
define('SEARCH_IN_MANU_TITLE', 'Búsqueda en fabricante');
define('SEARCH_IN_MANU_DESC', 'Activar para permitir la búsqueda en fabricantes');

// changes for 3.0.4 SP2
define('REVOCATION_ID_TITLE', 'Derecho de revocación');
define('REVOCATION_ID_DESC', 'Seleccione el contenido para mostrar el derecho de revocación');
define('DISPLAY_REVOCATION_ON_CHECKOUT_TITLE', '¿Mostrar derecho de revocación?');
define('DISPLAY_REVOCATION_ON_CHECKOUT_DESC', '¿Mostrar derecho de revocación en checkout_confirmation?');

// BOF - Tomcraft - 2009-10-03 - Paypal Express Modul
define('PAYPAL_MODE_TITLE', 'Modo PayPal:');
define('PAYPAL_MODE_DESC', 'Modo en vivo (normal) o de prueba (sandbox). Según el modo, primero debe crearse un acceso a la API para PayPal:  <br/>Enlace: <a href="https://www.paypal.com/de/cgi-bin/webscr?cmd=_get-api-signature&generic-flow=true" target="_blank"><strong>Configurar acceso a la API para el modo en vivo</strong></a><br/>Enlace: <a href="https://www.sandbox.paypal.com/de/cgi-bin/webscr?cmd=_get-api-signature&generic-flow=true" target="_blank"><strong>¿Aún no tiene una cuenta PayPal? <a href="https://www.paypal.com/de/cgi-bin/webscr?cmd=_registration-run" target="_blank"><strong>Haga clic aquí para crear una.</strong></a>');
define('PAYPAL_API_USER_TITLE', 'Usuario de la API de PayPal (en vivo)');
define('PAYPAL_API_USER_DESC', 'Introduzca aquí el nombre de usuario.');
define('PAYPAL_API_PWD_TITLE', 'Contraseña de la API de PayPal (en vivo)');
define('PAYPAL_API_PWD_DESC', 'Introduzca aquí la contraseña.');
define('PAYPAL_API_SIGNATURE_TITLE', 'Firma de la API de PayPal (en vivo)');
define('PAYPAL_API_SIGNATURE_DESC', 'Introduzca aquí la firma.');
define('PAYPAL_API_SANDBOX_USER_TITLE', 'Usuario de la API de PayPal (sandbox)');
define('PAYPAL_API_SANDBOX_USER_DESC', 'Introduzca aquí el nombre de usuario.');
define('PAYPAL_API_SANDBOX_PWD_TITLE', 'Contraseña de la API de PayPal (sandbox)');
define('PAYPAL_API_SANDBOX_PWD_DESC', 'Introduzca aquí la contraseña.');
define('PAYPAL_API_SANDBOX_SIGNATURE_TITLE', 'Firma de la API de PayPal (sandbox)');
define('PAYPAL_API_SANDBOX_SIGNATURE_DESC', 'Introduzca aquí la firma.');
define('PAYPAL_API_VERSION_TITLE', 'Versión de la API de PayPal');
define('PAYPAL_API_VERSION_DESC', 'Introduzca aquí la versión actual de la API de PayPal - por ejemplo: 119.0');
define('PAYPAL_API_IMAGE_TITLE', 'Logotipo de la tienda de PayPal');
define('PAYPAL_API_IMAGE_DESC', 'Introduzca aquí el archivo del logotipo que se debe mostrar en PayPal.<br />Atención: Sólo se transfiere si la tienda opera con SSL.<br />La imagen puede tener un ancho máximo de 750px y una altura máxima de 90px.<br />El archivo se abre desde: '.DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/img/');
define('PAYPAL_API_CO_BACK_TITLE', 'Color de fondo de PayPal');
define('PAYPAL_API_CO_BACK_DESC', 'Introduzca aquí el color de fondo que se debe mostrar en PayPal, por ejemplo, FEE8B9');
define('PAYPAL_API_CO_BORD_TITLE', 'Color de marco de PayPal');
define('PAYPAL_API_CO_BORD_DESC', 'Introduzca el color del marco que se debe mostrar en PayPal, por ejemplo, E4C558');
define('PAYPAL_ERROR_DEBUG_TITLE', 'Visualización de errores de PayPal');
define('PAYPAL_ERROR_DEBUG_DESC', '¿Debe mostrarse el error original de PayPal? por defecto=false');
define('PAYPAL_ORDER_STATUS_TMP_ID_TITLE', 'Estado del pedido "cancelado"');
define('PAYPAL_ORDER_STATUS_TMP_ID_DESC', 'cione el estado del pedido para una acción cancelada (por ejemplo, cancelación de PayPal)');
define('PAYPAL_ORDER_STATUS_SUCCESS_ID_TITLE', 'Estado del pedido OK');
define('PAYPAL_ORDER_STATUS_SUCCESS_ID_DESC', 'Seleccione el estado del pedido para una transacción exitosa (por ejemplo, Abierto PP pagado)');
define('PAYPAL_ORDER_STATUS_PENDING_ID_TITLE', 'Estado del pedido "en proceso"');
define('PAYPAL_ORDER_STATUS_PENDING_ID_DESC', 'Seleccione el estado del pedido para una transacción que todavía no haya sido procesada por PayPal (por ejemplo, Abierto PP en espera)');
define('PAYPAL_ORDER_STATUS_REJECTED_ID_TITLE', 'Estado del pedido "Rechazado"');
define('PAYPAL_ORDER_STATUS_REJECTED_ID_DESC', 'Seleccione el estado del pedido para una transacción rechazada (por ejemplo, PayPal rechazado)');
define('PAYPAL_COUNTRY_MODE_TITLE', 'Modo de país de PayPal');
define('PAYPAL_COUNTRY_MODE_DESC', 'Seleccione aquí el ajuste para el modo de país. Varias funciones de PayPal sólo están disponibles en el Reino Unido (por ejemplo, DirectPayment)');
define('PAYPAL_EXPRESS_ADDRESS_CHANGE_TITLE', 'Datos de dirección de PayPal Express');
define('PAYPAL_EXPRESS_ADDRESS_CHANGE_DESC', 'Permite modificar los datos de dirección transmitidos por PayPal');
define('PAYPAL_EXPRESS_ADDRESS_OVERRIDE_TITLE', 'Sobrescribir la dirección de entrega');
define('PAYPAL_EXPRESS_ADDRESS_OVERRIDE_DESC', 'Permite modificar los datos de dirección transmitidos por PayPal (cuenta existente)');
define('PAYPAL_INVOICE_TITLE', 'Prefijo de la tienda para el número de factura de PayPal');
define('PAYPAL_INVOICE_DESC', 'Secuencia de letras libremente seleccionable (prefijo), que se coloca delante del número de pedido y se utiliza para generar el número de factura de PayPal.<br />Esto permite que varias tiendas trabajen con una sola cuenta PayPal. Se evitan conflictos con los mismos números de orden. Cada pedido tendrá su propio número de factura en la cuenta PayPal.');
define('PAYPAL_BRANDNAME_TITLE', 'Nombre de tienda de PayPal');
define('PAYPAL_BRANDNAME_DESC', 'Introduzca aquí el nombre que debe mostrarse en PayPal.');
// EOF - Tomcraft - 2009-10-03 - Paypal Express Modul

// BOF - Tomcraft - 2009-11-02 - New admin top menu
define('USE_ADMIN_TOP_MENU_TITLE' , 'Navegación superior del Admin');
define('USE_ADMIN_TOP_MENU_DESC' , '¿Activar la navegación superior del Admin? De lo contrario, el menú se muestra en el margen izquierdo (clásico).');
// EOF - Tomcraft - 2009-11-02 - New admin top menu

// BOF - Tomcraft - 2009-11-02 - Admin language tabs
define('USE_ADMIN_LANG_TABS_TITLE' , 'Pestañas de idioma para categorías/artículos');
define('USE_ADMIN_LANG_TABS_DESC' , '¿Activar pestañas de idioma en los campos de entrada para categorías/artículos?');
// EOF - Tomcraft - 2009-11-02 - Admin language tabs

// BOF - Hendrik - 2010-08-11 - Thumbnails in admin products list
define('USE_ADMIN_THUMBS_IN_LIST_TITLE' , 'Imágenes de Listas de Productos');
define('USE_ADMIN_THUMBS_IN_LIST_DESC' , '¿Mostrar una columna adicional con imágenes de las categorías / artículos en la lista de productos del Admin?');
define('USE_ADMIN_THUMBS_IN_LIST_STYLE_TITLE', 'Imágenes de Listas de Productos Estilo CSS');
define('USE_ADMIN_THUMBS_IN_LIST_STYLE_DESC', 'Aquí puede introducir información sencilla de estilo CSS, por ejemplo, para el ancho máximo: max-width:90px;');
// EOF - Hendrik - 2010-08-11 - Thumbnails in admin products list

// BOF - Tomcraft - 2009-11-05 - Advanced contact form
//define('USE_CONTACT_EMAIL_ADDRESS_TITLE' , 'Kontaktformular - Sendeoption'); // not needed anymore!
//define('USE_CONTACT_EMAIL_ADDRESS_DESC' , 'Kontakt-E-Mail-Adresse des Shops zum Versenden des Kontaktformulars verwenden (wichtig f&uuml;r einige Provider z.B Hosteurope)'); // not needed anymore!
// EOF - Tomcraft - 2009-11-05 - Advanced contact form

// BOF - Dokuman - 2010-02-04 - delete cache files in admin section
define('DELETE_CACHE_SUCCESSFUL', 'Caché vaciada con éxito.');
define('DELETE_TEMP_CACHE_SUCCESSFUL', 'Caché de la plantilla vaciada con éxito.');
// EOF - Dokuman - 2010-02-04 - delete cache files in admin section

// BOF - DokuMan - 2010-08-13 - set Google RSS Feed in admin section
define('GOOGLE_RSS_FEED_REFID_TITLE' , 'Google RSS Feed - refID');
define('GOOGLE_RSS_FEED_REFID_DESC' , 'Introduzca aquí el ID de la campaña. Este se añade automáticamente a cada enlace del Feed RSS de Google.');
// EOF - DokuMan - 2010-08-13 - set Google RSS Feed in admin section

// BOF - web28 - 2010-08-17 -  Bildgroessenberechnung kleinerer Bilder
define('PRODUCT_IMAGE_NO_ENLARGE_UNDER_DEFAULT_TITLE', 'Escalamiento de imágenes con baja resolución');
define('PRODUCT_IMAGE_NO_ENLARGE_UNDER_DEFAULT_DESC', 'Active la configuración <strong>No</strong> para evitar que las imágenes de producto de menor resolución se escalen a los valores predeterminados de anchura y altura.  Si activa la configuración <strong>Sí</strong>, las imágenes con menor resolución también se escalarán a los valores de tamaño de imagen predeterminados. En este caso, sin embargo, estas imágenes pueden estar muy borrosas y pixeladas.');
// EOF - web28 - 2010-08-17 -  Bildgroessenberechnung kleinerer Bilder

//BOF - hendrik - 2011-05-14 - independent invoice number and date
//define('IBN_BILLNR_TITLE', 'N&auml;chste Rechnungsnummer');
//define('IBN_BILLNR_DESC', 'Beim Zuweisung einer Bestellung wird diese Nummer als n&auml;chstes vergeben.');
//define('IBN_BILLNR_FORMAT_TITLE', 'Rechnungsnummer Format');       //ibillnr
//define('IBN_BILLNR_FORMAT_DESC', 'Aufbauschema Rechn.Nr.: {n}=laufende Nummer, {d}=Tag, {m}=Monat, {y}=Jahr, <br>z.B. "100{n}-{d}-{m}-{y}" ergibt "10099-28-02-2007"');
//EOF - hendrik - 2011-05-14 - independent invoice number and date

//BOC - h-h-h - 2011-12-23 - Button "Buy Now" optional - default off
define('SHOW_BUTTON_BUY_NOW_TITLE', 'Mostrar el botón "Cesta de compra" en las listas de productos');
define('SHOW_BUTTON_BUY_NOW_DESC', '<span class="col-red"><strong>ATENCIÓN:</strong></span> ¡Esto puede dar lugar a amonestaciones si no se muestran al cliente todas las características esenciales del artículo en las páginas de la lista de productos!');
//EOC - h-h-h - 2011-12-23 - Button "Buy Now" optional - default off

//split page results
define('MAX_DISPLAY_ORDER_RESULTS_TITLE', 'Número de pedidos por página');
define('MAX_DISPLAY_ORDER_RESULTS_DESC', 'Número máximo de pedidos a mostrar en el resumen por página.');
define('MAX_DISPLAY_LIST_PRODUCTS_TITLE', 'Número de artículos por página');
define('MAX_DISPLAY_LIST_PRODUCTS_DESC', 'Número máximo de artículos a mostrar en el resumen por página.');
define('MAX_DISPLAY_LIST_CUSTOMERS_TITLE', 'Número de clientes por página');
define('MAX_DISPLAY_LIST_CUSTOMERS_DESC', 'Número máximo de clientes a mostrar en el resumen por página.');
define('MAX_ROW_LISTS_ATTR_OPTIONS_TITLE', 'Características del artículo: Número de características del artículo por página');
define('MAX_ROW_LISTS_ATTR_OPTIONS_DESC', 'Número máximo de características del artículo (opciones) a mostrar por página.');
define('MAX_ROW_LISTS_ATTR_VALUES_TITLE', 'Características del artículo: Número de valores de opción por página');
define('MAX_ROW_LISTS_ATTR_VALUES_DESC', 'Número máximo de valores de opción a mostrar por página.');
define('MAX_DISPLAY_STATS_RESULTS_TITLE', 'Número de resultados de estadística por página');
define('MAX_DISPLAY_STATS_RESULTS_DESC', 'Número máximo de resultados a mostrar en las estadísticas por página.');
define('MAX_DISPLAY_COUPON_RESULTS_TITLE', 'Número de resultados de cupones por página');
define('MAX_DISPLAY_COUPON_RESULTS_DESC', 'Número máximo de resultados a mostrar en los cupones por página.');

//whos online
define('WHOS_ONLINE_TIME_LAST_CLICK_TITLE', 'Quién está en línea: período de visualización en segundos');
define('WHOS_ONLINE_TIME_LAST_CLICK_DESC', 'Tiempo de visualización de los usuarios en línea en la tabla "Quién está en línea", después de este tiempo se borran las entradas. (valor mín.: 900)');

//sessions
define('SESSION_LIFE_ADMIN_TITLE', 'Duración de la sesión del Admin');
define('SESSION_LIFE_ADMIN_DESC', 'Tiempo en segundos tras el cual expira el tiempo de sesión para Admins (se cerrará la sesión) - Por defecto 7200<br />El valor establecido aquí sólo se aplica si el tratamiento de la sesión está basado en db (configure.php => define(\'STORE_SESSIONS\', \'mysql\');)<br />Valor máximo: 14400');
define('SESSION_LIFE_CUSTOMERS_TITLE', 'Duración de la sesión de clientes');
define('SESSION_LIFE_CUSTOMERS_DESC', 'Tiempo en segundos tras el cual expira el tiempo de sesión para clientes (se cerrará la sesión) - Por defecto 1440<br />El valor establecido aquí sólo se aplica si el tratamiento de la sesión está basado en db (configure.php => define(\'STORE_SESSIONS\', \'mysql\');)<br />Valor máximo: 14400');

//checkout confirmation options
define('CHECKOUT_USE_PRODUCTS_SHORT_DESCRIPTION_TITLE', 'Página de confirmación de pedido: Breve descripción');
define('CHECKOUT_USE_PRODUCTS_SHORT_DESCRIPTION_DESC', '¿Debe mostrarse la descripción breve del artículo en la página de confirmación del pedido? Nota: La descripción breve se muestra si NO hay descripción de pedido del artículo. ¡Con False no se muestra la descripción breve en general!');
define('CHECKOUT_USE_PRODUCTS_DESCRIPTION_FALLBACK_LENGTH_TITLE', 'Longitud de la descripción, si la descripción breve está vacía');
define('CHECKOUT_USE_PRODUCTS_DESCRIPTION_FALLBACK_LENGTH_DESC', '¿A partir de que longitud debe ser cortada la descripción si no hay una descripción breve disponible?');
define('CHECKOUT_SHOW_PRODUCTS_IMAGES_TITLE', 'Página de confirmación de pedido: Imágenes de productos');
define('CHECKOUT_SHOW_PRODUCTS_IMAGES_DESC', '¿Deben mostrarse las imágenes del artículo en la página de confirmación del pedido?');
define('CHECKOUT_SHOW_PRODUCTS_MODEL_TITLE', 'Página de confirmación de pedido: Número de artículo');
define('CHECKOUT_SHOW_PRODUCTS_MODEL_DESC', '¿Debe mostrarse el número de artículo en la página de confirmación del pedido?');

// email billing attachments
define('EMAIL_BILLING_ATTACHMENTS_TITLE', 'Archivos adjuntos de facturación por correo electrónico para pedidos');
define('EMAIL_BILLING_ATTACHMENTS_DESC', 'Ejemplo de archivos adjuntos - siempre y cuando los archivos estén en el directorio de la tienda <b>/media/content/</b>. Separar varios archivos adjuntos con coma sin espacios:<br /> media/content/agb.pdf,media/content/widerruf.pdf');

// email images
define('SHOW_IMAGES_IN_EMAIL_TITLE', 'Insertar imágenes de artículos en el correo electrónico de pedido');
define('SHOW_IMAGES_IN_EMAIL_DESC', 'Insertar imágenes de artículos en el correo electrónico de confirmación de pedido HTML (aumenta el riesgo de que el correo electrónico sea clasificado como SPAM)');
define('SHOW_IMAGES_IN_EMAIL_DIR_TITLE', 'Carpeta de imágenes de correo electrónico ');
define('SHOW_IMAGES_IN_EMAIL_DIR_DESC', 'Selección de la carpeta de imágenes de correo electrónico');
define('SHOW_IMAGES_IN_EMAIL_STYLE_TITLE', 'Imágenes de correo electrónico Estilo CSS');
define('SHOW_IMAGES_IN_EMAIL_STYLE_DESC', 'Aquí se puede introducir información sencilla de estilo CSS, por ejemplo, para el ancho máximo: max-width:90px;');

//popup windows configuration
define('POPUP_SHIPPING_LINK_PARAMETERS_TITLE', 'Parámetros URL de la ventana popup de costos de envío');
define('POPUP_SHIPPING_LINK_PARAMETERS_DESC', 'Aquí se puede introducir los parámetros de URL - por defecto: &KeepThis=true&TB_iframe=true&height=400&width=600');
define('POPUP_SHIPPING_LINK_CLASS_TITLE', 'Clase CSS de la ventana popup de costos de envío');
define('POPUP_SHIPPING_LINK_CLASS_DESC', 'Aquí se puede introducir clases CSS - por defecto: thickbox');
define('POPUP_CONTENT_LINK_PARAMETERS_TITLE', 'Parámetros de URL de la ventana popup de las páginas de contenido');
define('POPUP_CONTENT_LINK_PARAMETERS_DESC', 'Aquí se puede introducir los parámetros de URL - por defecto: &KeepThis=true&TB_iframe=true&height=400&width=600');
define('POPUP_CONTENT_LINK_CLASS_TITLE', 'Clase CSS de la ventana popup de las páginas de contenido');
define('POPUP_CONTENT_LINK_CLASS_DESC', 'Aquí se puede introducir clases CSS - por defecto: thickbox');
define('POPUP_PRODUCT_LINK_PARAMETERS_TITLE', 'Parámetros de URL de la ventana popup de las páginas de producto');
define('POPUP_PRODUCT_LINK_PARAMETERS_DESC', 'Aquí se puede introducir los parámetros de URL - por defecto: &KeepThis=true&TB_iframe=true&height=450&width=750');
define('POPUP_PRODUCT_LINK_CLASS_TITLE', 'Clase CSS de la ventana popup de las páginas de producto');
define('POPUP_PRODUCT_LINK_CLASS_DESC', 'Aquí se puede introducir clases CSS - por defecto: thickbox');
define('POPUP_COUPON_HELP_LINK_PARAMETERS_TITLE', 'Parámetros de URL de la ventana popup de la ayuda de cupones');
define('POPUP_COUPON_HELP_LINK_PARAMETERS_DESC', 'Aquí se puede introducir los parámetros de URL - por defecto: &KeepThis=true&TB_iframe=true&height=400&width=600');
define('POPUP_COUPON_HELP_LINK_CLASS_TITLE', 'Clase CSS de la ventana popup de la ayuda de cupones');
define('POPUP_COUPON_HELP_LINK_CLASS_DESC', 'Aquí se puede introducir clases CSS - por defecto: thickbox');

define('POPUP_PRODUCT_PRINT_SIZE_TITLE', 'Tamaño de la ventana de visualización de impresión del producto');
define('POPUP_PRODUCT_PRINT_SIZE_DESC', 'Aquí se puede definir el tamaño de la ventana popup - por defecto:  width=640, height=600');
define('POPUP_PRINT_ORDER_SIZE_TITLE', 'Tamaño de la ventana de visualización de impresión del pedido');
define('POPUP_PRINT_ORDER_SIZE_DESC', 'Aquí se puede definir el tamaño de la ventana popup - por defecto:   width=640, height=600');

define('TRACKING_COUNT_ADMIN_ACTIVE_TITLE' , 'Contar las visitas de página del operador de la tienda');
define('TRACKING_COUNT_ADMIN_ACTIVE_DESC' , 'Si se activa esta opción, también se cuentan todos los accesos del administrador, lo que (debido al acceso más frecuente a la tienda) puede falsificar las estadísticas de visitantes.');

define('TRACKING_GOOGLEANALYTICS_ACTIVE_TITLE' , 'Activar el seguimiento de Google Analytics');
define('TRACKING_GOOGLEANALYTICS_ACTIVE_DESC' , 'Si se activa esta opción, todas las visitas de página se transmiten a Google Analytics y pueden evaluarse más tarde. Esto requiere la creación de una cuenta en <a href="http://www.google.com/analytics/" target="_blank"><b>Google Analytics</b></a> .');
define('TRACKING_GOOGLEANALYTICS_ID_TITLE' , 'Número de cuenta de Google Analytics');
define('TRACKING_GOOGLEANALYTICS_ID_DESC' , 'Introduzca aquí el número de cuenta de Google Analytics en el formato "UA-XXXXXXXX-X", que ha recibido tras una exitosa creación de la cuenta.');

define('TRACKING_PIWIK_ACTIVE_TITLE' , 'Activar el seguimiento de Piwik Web-Analytics');
define('TRACKING_PIWIK_ACTIVE_DESC' , 'Para usar Piwik, primero debe descargarlo e instalarlo en su espacio web, vea también <a href="http://piwik.org/" target="_blank"><b>Piwik Web-Analytics</b></a>. A diferencia de Google Analytics, los datos se almacenan localmente, es decir, usted, como operador de la tienda, tiene la soberanía de los datos.');
define('TRACKING_PIWIK_LOCAL_PATH_TITLE' , 'Directorio de instalación Piwik (sin "http://")');
define('TRACKING_PIWIK_LOCAL_PATH_DESC' , 'Introduzca aquí el directorio después de que Piwik se haya instalado exitosamente. Ha de introducir el nombre de dominio completo sin "http://" como ruta, por ejemplo, "www.domain.de/piwik".');
define('TRACKING_PIWIK_ID_TITLE' , 'ID de página Piwik');
define('TRACKING_PIWIK_ID_DESC' , 'En la interfaz de administración de Piwik, se asigna un ID por cada dominio creado (normalmente "1")');
define('TRACKING_PIWIK_GOAL_TITLE' , 'Número de campaña Piwik (opcional)');
define('TRACKING_PIWIK_GOAL_DESC' , 'Introduzca aquí un número de campaña si desea hacer un seguimiento de destinos predefinidos. Detalles vea <a href="http://piwik.org/docs/tracking-goals-web-analytics/" target="_blank"><b>Piwik: Tracking Goal Conversions</b></a>');

define('CONFIRM_SAVE_ENTRY_TITLE', 'Consulta de confirmación al guardar artículos/categorías');
define('CONFIRM_SAVE_ENTRY_DESC', '¿Debería producirse una consulta de confirmación al guardar artículos/categorías? Por defecto: true (sí)');

define('WHOS_ONLINE_IP_WHOIS_SERVICE_TITLE', 'Quién está en línea - Whois Lookup URL');
define('WHOS_ONLINE_IP_WHOIS_SERVICE_DESC', 'http://www.utrace.de/?query= o http://whois.domaintools.com/');

define('STOCK_CHECKOUT_UPDATE_PRODUCTS_STATUS_TITLE', 'Cierre del pedido - Desactivar artículos agotados');
define('STOCK_CHECKOUT_UPDATE_PRODUCTS_STATUS_DESC', '¿Debe desactivarse automáticamente un artículo agotado (cantidad de stock 0) al final del pedido? ¡El artículo entonces ya no es visible en la tienda!<br />Para los artículos que volverán a estar disponibles en breve, la opción debería configurarse como "false".');

define('SEND_EMAILS_DOUBLE_OPT_IN_TITLE', 'Double-Opt-In para suscripción al newsletter');
define('SEND_EMAILS_DOUBLE_OPT_IN_DESC', 'Si es "true", se envía un correo electrónico al cliente en el que se debe confirmar la suscripción al newsletter. Para ello debe estar activado el envío de correos electrónicos.');

define('USE_ADMIN_FIXED_TOP_TITLE', '¿Fijar el encabezado de la página del Admin?');
define('USE_ADMIN_FIXED_TOP_DESC', '¿Debe estar visible siempre el encabezado de la página cuando se desplaza?');
define('USE_ADMIN_FIXED_SEARCH_TITLE', '¿Mostrar barra de búsqueda del Admin?');
define('USE_ADMIN_FIXED_SEARCH_DESC', '¿Debe estar visible siempre la barra de búsqueda?');

define('SMTP_SECURE_TITLE' , 'SMTP SECURE');
define('SMTP_SECURE_DESC' , '¿El servidor SMTP requiere una conexión segura? Puede averiguar los ajustes necesarios con su proveedor.');

define('DISPLAY_ERROR_REPORTING_TITLE', 'Error Reporting');
define('DISPLAY_ERROR_REPORTING_DESC', '¿Debe mostrarse el Error Reporting como una lista en el Footer de la página?');

define('DISPLAY_BREADCRUMB_OPTION_TITLE', 'Breadcrumb Navigation');
define('DISPLAY_BREADCRUMB_OPTION_DESC', '<strong>name:</strong> En la Breadcrumb Navigation se muestra el nombre completo del artículo.<br /><strong>model:</strong> En la Breadcrumb Navigation se muestra el número de artículo, si existe. De lo contrario, se regresa al nombre del artículo.');

define('EMAIL_WORD_WRAP_TITLE', 'WordWrap para correos electrónicos de texto');
define('EMAIL_WORD_WRAP_DESC', 'Introduzca aquí el número de caracteres para una línea en los correos electrónicos de texto antes de fracturar el texto (sólo números enteros).<br /><strong>Atención:</strong> ¡Un número de caracteres superior a 76 puede hacer que los correos electrónicos de la tienda sean clasificados como SPAM por SpamAssassin! Más información <a href="http://wiki.apache.org/spamassassin/Rules/MIME_QP_LONG_LINE" target="_blank">aquí</a>.');

//define('USE_PAGINATION_LIST_TITLE', 'Pagination Liste'); // Tomcraft - 2017-07-12 - Not used anymore since r10840, see: http://trac.modified-shop.org/ticket/1238
//define('USE_PAGINATION_LIST_DESC', 'Verwende eine HTML Liste (ul / li Tag) f&uuml;r die Pagination / Seitenschaltung.<br/><b>Achtung:</b> Das funktioniert nur mit einem ab Shopversion 2.0.0.0 kompatiblem Template!'); // Tomcraft - 2017-07-12 - Not used anymore since r10840, see: http://trac.modified-shop.org/ticket/1238

define('ORDER_STATUSES_FOR_SALES_STATISTICS_TITLE', 'Filtro de estadísticas de ventas');
define('ORDER_STATUSES_FOR_SALES_STATISTICS_DESC', 'Seleccione aquí el estado del pedido que se debe considerar para las estadísticas de ventas en la página de inicio de Admin y en el menú desplegable de estados cuando se utiliza el estado "Filtro de estadísticas de ventas".<br />(Para mostrar sólo lo que se ha hecho realmente en ventas, seleccione el estado que se utiliza para el pedido terminado.)<br /><b>Aviso:</b> Para mostrar el filtro "Filtro de estadísticas de ventas" en el menú desplegable de estadísticas de ventas, se deben seleccionar al menos dos estados. De lo contrario, el estado deseado puede seleccionarse directamente utilizando el menú desplegable.');

define('SAVE_IP_LOG_TITLE', 'Guardar dirección IP');
define('SAVE_IP_LOG_DESC', '¿Si se debe guardar la dirección IP en la base de datos?<br/>Con la opción xxx, los últimos dígitos de la IP se hacen anónimos.');

define('META_MAX_KEYWORD_LENGTH_TITLE', 'Longitud máxima de las meta-palabras clave');
define('META_MAX_KEYWORD_LENGTH_DESC', 'Longitud máxima de las meta-palabras clave generadas automáticamente (descripción del artículo)');
define('META_DESCRIPTION_LENGTH_TITLE', 'Longitud de la meta descripción');
define('META_DESCRIPTION_LENGTH_DESC', 'Longitud máxima de la descripción (en letras)');
define('META_STOP_WORDS_TITLE', 'Stop Words');
define('META_STOP_WORDS_DESC', 'Por favor, introduzca aquí las palabras clave como una lista separada por comas que no se deben utilizar.');
define('META_GO_WORDS_TITLE', 'Go Words');
define('META_GO_WORDS_DESC', 'Por favor, introduzca aquí las palabras clave como una lista separada por comas que están explícitamente permitidas.');

//BOC added text constants for group id 20, noRiddle
define('CSV_CATEGORY_DEFAULT_TITLE', 'Categoría para la importación');
define('CSV_CATEGORY_DEFAULT_DESC', 'Todos los artículos que no se han asignado <b>ninguna</b> categoría en el archivo de importación CSV y que todavía no existen en la tienda, se importarán a esta categoría.<br/><b>Importante:</b> Si no desea importar artículos sin categoría en el archivo de importación CSV, seleccione la categoría "Superior" porque no se importará ningún artículo a esta categoría.');
define('CSV_TEXTSIGN_TITLE', 'Carácter de reconocimiento de texto');
define('CSV_TEXTSIGN_DESC', 'P.ej. "   |  <span style="color:#c00;">¡Con punto y coma como separador, el carácter de reconocimiento de texto debe establecerse en " !</span>');
define('CSV_SEPERATOR_TITLE', 'Separador');
define('CSV_SEPERATOR_DESC', 'P.ej. ;   |  <span style="color:#c00;">si el campo de entrada se deja vacío se utiliza por defecto t (= pestaña) durante  la exportación/importación!</span>');
define('COMPRESS_EXPORT_TITLE', 'Compresión');
define('COMPRESS_EXPORT_DESC', 'Compresión de datos exportados');
define('CSV_CAT_DEPTH_TITLE', 'Profundidad de categoría');
define('CSV_CAT_DEPTH_DESC', '¿Qué tan profundo debe ir el árbol de categorías? (p.ej. con el ajuste por defecto 4: categoría principal y tres subcategorías)<br />Este ajuste es importante para importar correctamente las categorías creadas en el CSV. Lo mismo se aplica para la exportación.<br /><span style="color:#c00;">¡Más de 4 puede llevar a pérdidas de rendimiento y quizás no sea agradable para el cliente!');
//EOC added text constants for group id 20, noRiddle

define('MIN_GROUP_PRICE_STAFFEL_TITLE', 'Número adicional de precios escalonados');
define('MIN_GROUP_PRICE_STAFFEL_DESC', 'Número adicional de precios escalonados a mostrar');

define('MODULE_CAPTCHA_ACTIVE_TITLE', 'Activar Captcha');
define('MODULE_CAPTCHA_ACTIVE_DESC', '¿Para qué secciones de la tienda debe activarse el captcha?');
define('MODULE_CAPTCHA_LOGGED_IN_TITLE', 'Clientes conectados');
define('MODULE_CAPTCHA_LOGGED_IN_DESC', 'Visualización del captcha para clientes conectados');
define('MODULE_CAPTCHA_USE_COLOR_TITLE', 'Colores aleatorios');
define('MODULE_CAPTCHA_USE_COLOR_DESC', 'Visualización de líneas y caracteres en colores aleatorios');
define('MODULE_CAPTCHA_USE_SHADOW_TITLE', 'Sombra');
define('MODULE_CAPTCHA_USE_SHADOW_DESC', 'Sombras adicionales de los caracteres en el Captcha');
define('MODULE_CAPTCHA_CODE_LENGTH_TITLE', 'Longitud del Captcha');
define('MODULE_CAPTCHA_CODE_LENGTH_DESC', 'Número de caracteres en el Captcha<br/>(por defecto: 6)');
define('MODULE_CAPTCHA_NUM_LINES_TITLE', 'Número de líneas');
define('MODULE_CAPTCHA_NUM_LINES_DESC', 'Número de líneas en el Captcha<br/>(por defecto: 70)');
define('MODULE_CAPTCHA_MIN_FONT_TITLE', 'Tamaño mínimo de letra');
define('MODULE_CAPTCHA_MIN_FONT_DESC', 'Especificación en píxeles para los caracteres más pequeños en el Captcha.<br/>(por defecto: 24)');
define('MODULE_CAPTCHA_MAX_FONT_TITLE', 'Tamaño máximo de letra');
define('MODULE_CAPTCHA_MAX_FONT_DESC', 'Especificación en píxeles para los caracteres más grandes en el Captcha.<br/>(por defecto: 28)');
define('MODULE_CAPTCHA_BACKGROUND_RGB_TITLE', 'Color de fondo');
define('MODULE_CAPTCHA_BACKGROUND_RGB_DESC', 'Especificación del color de fondo se realiza en el RGB.<br/>(por defecto: 192,192,192)');
define('MODULE_CAPTCHA_LINES_RGB_TITLE', 'Color de línea');
define('MODULE_CAPTCHA_LINES_RGB_DESC', 'Especificación del color de línea se realiza en el RGB.<br/>(por defecto: 220,148,002)');
define('MODULE_CAPTCHA_CHARS_RGB_TITLE', 'Color del carácter');
define('MODULE_CAPTCHA_CHARS_RGB_DESC', 'Especificación del color del carácter se realiza en el RGB.<br/>(por defecto: 112,112,112)');
define('MODULE_CAPTCHA_WIDTH_TITLE', 'Ancho');
define('MODULE_CAPTCHA_WIDTH_DESC', 'Especificación en píxeles del ancho del Captcha');
define('MODULE_CAPTCHA_HEIGHT_TITLE', 'Altura');
define('MODULE_CAPTCHA_HEIGHT_DESC', 'Especificación en píxeles de la altura del Captcha');

define('SHIPPING_STATUS_INFOS_TITLE', 'Tiempo de entrega');
define('SHIPPING_STATUS_INFOS_DESC', 'Seleccione el contenido para mostrar información sobre el tiempo de entrega');

define('USE_SHORT_DATE_FORMAT_TITLE', 'Mostrar fecha en formato corto');
define('USE_SHORT_DATE_FORMAT_DESC', 'Mostrar fecha siempre en formato corto: <b>01.03.2014</b> en vez de <b>Sábado, 01. Marzo 2014</b><br />¡Recomendado para errores de visualización con el formato de fecha largo, como problemas de idioma o de vocal!');

define('MAX_DISPLAY_PRODUCTS_CATEGORY_TITLE', 'Máximo de artículos');
define('MAX_DISPLAY_PRODUCTS_CATEGORY_DESC', 'Número máximo de artículos de la misma categoría');
define('MAX_DISPLAY_ADVANCED_SEARCH_RESULTS_TITLE', 'Número de resultados de búsqueda');
define('MAX_DISPLAY_ADVANCED_SEARCH_RESULTS_DESC', 'Número de artículos en los resultados de búsqueda');
define('MAX_DISPLAY_PRODUCTS_HISTORY_TITLE' , 'Número de historial');
define('MAX_DISPLAY_PRODUCTS_HISTORY_DESC' , 'Número máximo de artículos que han sido visitados por último en la cuenta');

define('PRODUCT_IMAGE_SHOW_NO_IMAGE_TITLE', 'Artículo noimage.gif');
define('PRODUCT_IMAGE_SHOW_NO_IMAGE_DESC', 'Visualización de noimage.gif si no se especifica ninguna imágen de artículo');
define('CATEGORIES_IMAGE_SHOW_NO_IMAGE_TITLE', 'Categoría noimage.gif');
define('CATEGORIES_IMAGE_SHOW_NO_IMAGE_DESC', 'Visualización de noimage.gif si no se especifica ninguna imágen de categoría');
define('MANUFACTURER_IMAGE_SHOW_NO_IMAGE_TITLE', 'Fabricante noimage.gif');
define('MANUFACTURER_IMAGE_SHOW_NO_IMAGE_DESC', 'Visualización de noimage.gif si no se especifica ninguna imágen del fabricante');

define('MODULE_SMALL_BUSINESS_TITLE', 'pequeño empresario');
define('MODULE_SMALL_BUSINESS_DESC', '¿Debe la tienda ser convertida a pequeño empresario según § 19 UStG?<br /><b>Importante:</b> Bajo "Módulos" -> "Resumen" el módulo  "ot_tax" <a href="'.xtc_href_link(FILENAME_MODULES, 'set=ordertotal&module=ot_tax').'"><b>aquí</b></a> debe ser desactivado o desinstalado. Además, en los <a href="'.xtc_href_link(FILENAME_CUSTOMERS_STATUS, '').'"><b>grupos de clientes</b></a> individuales se debe ajustar los "Precios con IVA incluido" a "No".');

define('COMPRESS_HTML_OUTPUT_TITLE', 'Compresión HTML');
define('COMPRESS_HTML_OUTPUT_DESC', '¿Debe el resultado HTML ser entregado comprimido por la plantilla?');
define('COMPRESS_STYLESHEET_TITLE', 'Compresión CSS');
define('COMPRESS_STYLESHEET_DESC', '¿Debe entregarse un stylesheet comprimido?<br/><b>Atención:</b> ¡Esto sólo funciona con una plantilla compatible a partir de la versión 2.0.0.0 de la tienda!');
define('COMPRESS_JAVASCRIPT_TITLE', 'Compresión JavaScript');
define('COMPRESS_JAVASCRIPT_DESC', '¿Debe entregarse un archivo JavaScript comprimido?<br/><b>Atención:</b> ¡Esto sólo funciona con una plantilla compatible a partir de la versión 2.0.1.0 de la tienda!');

define('USE_ATTRIBUTES_IFRAME_TITLE', 'Editar atributos en iframe');
define('USE_ATTRIBUTES_IFRAME_DESC', 'Abre la gestión de los atributos en la vista de categoría/artículo en un iframe.');

define('ADMIN_HEADER_X_FRAME_OPTIONS_TITLE', 'Protección de Admin Clickjacking');
define('ADMIN_HEADER_X_FRAME_OPTIONS_DESC', 'Proteger el área del Admin con el encabezado "X-Frame-Options: SAMEORIGIN" <br>Supported Browsers: FF 3.6.9+ Chrome 4.1.249.1042+ IE 8+ Safari 4.0+ Opera 10.50+ ');

define('SEND_MAIL_ACCOUNT_CREATED_TITLE', 'Correo electrónico al crear una cuenta');
define('SEND_MAIL_ACCOUNT_CREATED_DESC', '¿Debe enviarse un correo electrónico al cliente cuando se crea una nueva cuenta de cliente?');

define('STATUS_EMAIL_SENT_COPY_TO_ADMIN_TITLE', 'Correo electrónico al cambiar de estado');
define('STATUS_EMAIL_SENT_COPY_TO_ADMIN_DESC', '¿Debe enviarse un correo electrónico al Admin cuando se cambia el estado de un pedido?');

define('STOCK_CHECK_SPECIALS_TITLE', 'Comprobar las ofertas especiales');
define('STOCK_CHECK_SPECIALS_DESC', 'Comprobar si todavía hay suficientes ofertas especiales disponibles para la entrega del pedido.<br/><br/><b>ATENCIÓN:</b> Si no hay suficientes ofertas especiales disponibles, el pedido sólo puede cerrarse después de que se haya reducido la cantidad.');

define('DOWNLOAD_SHOW_LANG_DROPDOWN_TITLE', 'Menú desplegable de países en la cesta de compra');
define('DOWNLOAD_SHOW_LANG_DROPDOWN_DESC', '¿Se debe mostrar el menú desplegable de países en la cesta de compra si sólo se compran artículos para la descarga?');

define('GUEST_ACCOUNT_EDIT_TITLE', 'Editar cuentas de invitados');
define('GUEST_ACCOUNT_EDIT_DESC', '¿Se permite a los invitados ver y editar los detalles de su cuenta?');

define('EMAIL_SIGNATURE_ID_TITLE', 'Firma de correo electrónico');
define('EMAIL_SIGNATURE_ID_DESC', 'Seleccione el contenido que se utilizará como firma en los correos electrónicos de la tienda.');

define('TEXT_PAYPAL_NOT_INSTALLED', '<div class="important_info">PayPal aún no se ha instalado. Esto se puede realizar <a href="'.xtc_href_link(FILENAME_MODULES, 'set=payment&module=paypal').'">aquí</a> .</div>');

define('POLICY_MIN_LOWER_CHARS_TITLE', 'Minúsculas en contraseña');
define('POLICY_MIN_LOWER_CHARS_DESC', '¿Cuántas letras minúsculas debe tener al menos la contraseña?');
define('POLICY_MIN_UPPER_CHARS_TITLE', 'Mayúsculas en contraseña');
define('POLICY_MIN_UPPER_CHARS_DESC', '¿Cuántas letras mayúsculas debe tener al menos la contraseña?');
define('POLICY_MIN_NUMERIC_CHARS_TITLE', 'Números en contraseña');
define('POLICY_MIN_NUMERIC_CHARS_DESC', '¿Cuántos números debe tener al menos la contraseña?');
define('POLICY_MIN_SPECIAL_CHARS_TITLE', 'Caracteres especiales en contraseña');
define('POLICY_MIN_SPECIAL_CHARS_DESC', '¿Cuántos caracteres especiales debe tener al menos la contraseña?');

define('SHOW_SHIPPING_EXCL_TITLE', 'Más costos de envío');
define('SHOW_SHIPPING_EXCL_DESC', 'Visualización de excl. o incl. costos de envío');

define('ACCOUNT_TELEPHONE_OPTIONAL_TITLE', 'Número de teléfono opcional');
define('ACCOUNT_TELEPHONE_OPTIONAL_DESC', '¿Debe solicitarse el número de teléfono sólo de forma opcional?');

define('TRACKING_GOOGLEANALYTICS_UNIVERSAL_TITLE' , 'Google Universal Analytics');
define('TRACKING_GOOGLEANALYTICS_UNIVERSAL_DESC' , '¿Debe utilizarse el código de Google Universal Analytics?<br/><br/><b>Atención:</b> ¡Tan pronto como cambie al nuevo código de Google Universal Analytics en su cuenta de Google Analytics, ya no podrá utilizar el anterior Google Analytics!<br/><b>Atención:</b> ¡Esto sólo funciona con una plantilla compatible a partir de la versión 2.0.0.0.0 de la tienda!');
define('TRACKING_GOOGLEANALYTICS_DOMAIN_TITLE' , 'URL de la tienda de Google Universal Analytics');
define('TRACKING_GOOGLEANALYTICS_DOMAIN_DESC' , 'Introduzca aquí la URL predeterminada de la tienda (ejemplo.com o www.example.com). Funciona sólo para Google Universal Analytics.');
define('TRACKING_GOOGLE_LINKID_TITLE' , 'ID de enlace de Google Universal Analytics');
define('TRACKING_GOOGLE_LINKID_DESC' , 'Puede ver información separada sobre múltiples enlaces en una página que todos tienen el mismo destino. Por ejemplo, si hay dos enlaces en la misma página que conducen a la página de Contacto, verá información de clic por separado para cada enlace. Funciona sólo para Google Universal Analytics.');
define('TRACKING_GOOGLE_DISPLAY_TITLE' , 'Función de visualización de Google Universal Analytics');
define('TRACKING_GOOGLE_DISPLAY_DESC' , 'Las áreas sobre características demográficas y sobre los intereses incluyen un resumen y también nuevos informes sobre el desempeño por edad, género y categorías de interés. Funciona sólo para Google Universal Analytics.');
define('TRACKING_GOOGLE_ECOMMERCE_TITLE' , 'Seguimiento de Google E-Commerce');
define('TRACKING_GOOGLE_ECOMMERCE_DESC' , 'Utilice el seguimiento de E-Commerce para averiguar qué compran los visitantes a través de su sitio web o aplicación. También recibirá la siguiente información:<br><br><strong>Productos:</strong> Productos comprados como también las cantidades y ventas generadas con estos productos<br><strong>Transacciones:</strong> Información sobre ventas, impuestos, costos de envío y cantidades para cada transacción<br><strong>Tiempo hasta la compra:</strong> Número de días y visitas, empezando por la campaña actual hasta la finalización de la transacción.');

define('NEW_ATTRIBUTES_STYLING_TITLE', 'Gestión de atributos Styling');
define('NEW_ATTRIBUTES_STYLING_DESC', '¿Activar el Styling para las casillas de verificación/menús desplegables en la gestión del atributo? Ajustar a No/false cuando haya muchos atributos y problemas de rendimiento.');

define('DB_CACHE_TYPE_TITLE', 'Caché Engine');
define('DB_CACHE_TYPE_DESC', 'Seleccione uno de los Engines disponibles para el almacenamiento en caché.');

define('META_PRODUCTS_KEYWORDS_LENGTH_TITLE', 'Longitud de los términos adicionales para la búsqueda');
define('META_PRODUCTS_KEYWORDS_LENGTH_DESC', 'Longitud máxima de los términos de búsqueda adicionales (en letras)');
define('META_KEYWORDS_LENGTH_TITLE', 'Longitud de las meta-palabras clave');
define('META_KEYWORDS_LENGTH_DESC', 'Longitud máxima de las palabras clave (en letras)');
define('META_TITLE_LENGTH_TITLE', 'Longitud del meta título');
define('META_TITLE_LENGTH_DESC', 'Longitud máxima del título (en letras)');
define('META_CAT_SHOP_TITLE_TITLE', 'Título de la tienda Categorías');
define('META_CAT_SHOP_TITLE_DESC', '¿Añadir el título de la tienda a las categorías?');
define('META_PROD_SHOP_TITLE_TITLE', 'Título de la tienda Productos');
define('META_PROD_SHOP_TITLE_DESC', '¿Añadir el título de la tienda a los productos?');
define('META_CONTENT_SHOP_TITLE_TITLE', 'Título de la tienda Contenidos');
define('META_CONTENT_SHOP_TITLE_DESC', '¿Añadir el título de la tienda a los contenidos?');
define('META_SPECIALS_SHOP_TITLE_TITLE', 'Título de la tienda Ofertas Especiales');
define('META_SPECIALS_SHOP_TITLE_DESC', '¿Añadir el título de la tienda a las ofertas especiales?');
define('META_NEWS_SHOP_TITLE_TITLE', 'Título de la tienda Nuevos Productos');
define('META_NEWS_SHOP_TITLE_DESC', '¿Añadir el título de la tienda a los nuevos productos?');
define('META_SEARCH_SHOP_TITLE_TITLE', 'Título de la tienda Búsqueda');
define('META_SEARCH_SHOP_TITLE_DESC', '¿Añadir el título de la tienda a los resultados de la búsqueda?');
define('META_OTHER_SHOP_TITLE_TITLE', 'Título de la tienda Otras Páginas');
define('META_OTHER_SHOP_TITLE_DESC', '¿Añadir el título de la tienda a todas las demás páginas?');
define('META_GOOGLE_VERIFICATION_KEY_TITLE', 'Google Verification Key');
define('META_GOOGLE_VERIFICATION_KEY_DESC', '<meta name="verify-v1">');
define('META_BING_VERIFICATION_KEY_TITLE', 'Bing Verification Key');
define('META_BING_VERIFICATION_KEY_DESC', '<meta name="msvalidate.01">');

define('TRACKING_FACEBOOK_ACTIVE_TITLE', 'Activar el seguimiento de Facebook Conversion');
define('TRACKING_FACEBOOK_ACTIVE_DESC', 'Si se activa esta opción, todas las compras se envían a Facebook y se pueden evaluar más tarde. Esto requiere anteriormente la creación de una cuenta en <a href="https://www.facebook.com" target="_blank"><b>Facebook</b></a> .<br/><b>Atención:</b> ¡Esto sólo funciona con una plantilla compatible desde la versión 2.0.0.0.0!');
define('TRACKING_FACEBOOK_ID_TITLE', 'ID de Facebook Conversion');
define('TRACKING_FACEBOOK_ID_DESC', 'Su ID de Facebook Conversion');

define('NEW_SELECT_CHECKBOX_TITLE', 'Área del Admin Styling');
define('NEW_SELECT_CHECKBOX_DESC', '¿Activar el Styling para las casillas de verificación/menús desplegables en el área de Admin?');
define('CSRF_TOKEN_SYSTEM_TITLE', 'Admin Token System');
define('CSRF_TOKEN_SYSTEM_DESC', '¿Debe utilizarse el Admin Token System? <br/><b>Atención:</b> El Token System se introdujo para aumentar la seguridad.');

define('DISPLAY_FILTER_INDEX_TITLE', 'Visualización de filtros por página - artículo');
define('DISPLAY_FILTER_INDEX_DESC', 'Por favor, introduzca los posibles valores para la selección separados por una coma. Para todos los artículos escriba all.<br/>Ejemplo: 3,12,27,all');
define('DISPLAY_FILTER_SPECIALS_TITLE', 'Visualización de filtros por página - ofertas especiales');
define('DISPLAY_FILTER_SPECIALS_DESC', 'Por favor, introduzca los posibles valores para la selección separados por una coma. Para todos los artículos escriba all.<br/>Ejemplo: 3,12,27,all');
define('DISPLAY_FILTER_PRODUCTS_NEW_TITLE', 'Visualización de filtros por página - nuevos artículos');
define('DISPLAY_FILTER_PRODUCTS_NEW_DESC', 'Por favor, introduzca los posibles valores para la selección separados por una coma. Para todos los artículos escriba all.<br/>Ejemplo: 3,12,27,all');
define('DISPLAY_FILTER_ADVANCED_SEARCH_RESULT_TITLE', 'Visualización de filtros por página - resultados de búsqueda');
define('DISPLAY_FILTER_ADVANCED_SEARCH_RESULT_DESC', 'Por favor, introduzca los posibles valores para la selección separados por una coma. Para todos los artículos escriba all.<br/>Ejemplo: 4,12,32,all');

define('USE_BROWSER_LANGUAGE_TITLE' , 'Cambiar automáticamente al idioma del navegador');
define('USE_BROWSER_LANGUAGE_DESC' , 'Cambie automáticamente el idioma al idioma del navegador del cliente.');

define('WYSIWYG_SKIN_TITLE' , 'WYSIWYG-Editor Skin');
define('WYSIWYG_SKIN_DESC' , 'Seleccione el WYSIWYG-Editor Skin');

define('CHECK_CHEAPEST_SHIPPING_MODUL_TITLE', 'Preseleccionar el método de envío más barato');
define('CHECK_CHEAPEST_SHIPPING_MODUL_DESC', '¿Debe preseleccionarse el método de envío más barato para el cliente al realizar el checkout?');

define('DISPLAY_PRIVACY_CHECK_TITLE', 'Mostrar casilla de verificación de privacidad');
define('DISPLAY_PRIVACY_CHECK_DESC', '¿Debe mostrarse la casilla de verificación de privacidad durante la creación de la cuenta? (Obligatorio para las transacciones B2C!)');

define('SHOW_SELFPICKUP_FREE_TITLE', 'Módulo de envío "Auto-Recogida" para "envío gratuito"');
define('SHOW_SELFPICKUP_FREE_DESC', '¿Debe mostrarse el módulo de envío "Auto-Recogida" cuando se alcanza el importe fijado para "envío gratuito" en el módulo "Costos de envío (ot_shiping)"?');

define('CHECK_FIRST_PAYMENT_MODUL_TITLE', 'Preseleccionar la primera opción de pago');
define('CHECK_FIRST_PAYMENT_MODUL_DESC', '¿Debe preseleccionarse la primera opción de pago para el cliente al realizar el checkout?');

define('ATTRIBUTES_VALID_CHECK_TITLE', 'Validar atributos');
define('ATTRIBUTES_VALID_CHECK_DESC', 'Comprueba los artículos de la cesta de compra del cliente en busca de atributos que ya no son válidos.<br/>(Esto puede suceder si un cliente vuelve a entrar en la tienda después de mucho tiempo y desea comprar un artículo que ha permanecido en la cesta de compra de una visita anterior.)<br/><b>Aviso:</b> En el caso de extensiones que posteriormente amplíen atributos, como el campo de texto, esta comprobación debe desactivarse.');

define('ATTRIBUTE_MODEL_DELIMITER_TITLE', 'Separador de número de artículo/atributo');
define('ATTRIBUTE_MODEL_DELIMITER_DESC', 'Separador entre número de artículo y número de atributo de artículo');

define('STORE_PAGE_PARSE_TIME_THRESHOLD_TITLE' , 'Valor umbral el almacenamiento del tiempo de cálculo de la carga de la página');
define('STORE_PAGE_PARSE_TIME_THRESHOLD_DESC' , 'Establece el valor umbral en segundos a partir del cual se debe escribir una entrada para el tiempo de cálculo de la carga de la página.');

define('SEARCH_IN_FILTER_TITLE', 'Búsqueda en las características del artículo');
define('SEARCH_IN_FILTER_DESC', 'Activar para habilitar la búsqueda en las características del artículo');
define('SEARCH_AC_STATUS_TITLE', 'Búsqueda de autocompletación');
define('SEARCH_AC_STATUS_DESC', 'Activar para activar la búsqueda de autocompletación<br/><b>Atención:</b> ¡Esto sólo funciona con una plantilla compatible a partir de la versión 2.0.0.0.0!');
define('SEARCH_AC_MIN_LENGTH_TITLE', 'Número de caracteres en la búsqueda de autocompletación');
define('SEARCH_AC_MIN_LENGTH_DESC', '¿A partir de qué número de caracteres deben mostrarse los primeros resultados de búsqueda?<br/><b>Atención:</b> ¡Esto sólo funciona con una plantilla compatible a partir de la versión 2.0.0.0.0!');

define('DISPLAY_REVOCATION_VIRTUAL_ON_CHECKOUT_TITLE', 'Visualización del derecho de revocación para descargas');
define('DISPLAY_REVOCATION_VIRTUAL_ON_CHECKOUT_DESC', '¿Debe mostrarse una casilla de verificación al realizar el checkout indicando que el derecho de revocación expira?');
define('ORDER_STATUSES_DISPLAY_DEFAULT_TITLE', 'Visualización pedidos');
define('ORDER_STATUSES_DISPLAY_DEFAULT_DESC', '¿Pedidos con qué estado del pedido deben mostrarse por defecto?');

define('INVOICE_INFOS_TITLE', 'Datos de la factura');
define('INVOICE_INFOS_DESC', 'Seleccione una página de contenido. El contenido se muestra en la impresión de la factura.');

define('CATEGORIES_SHOW_PRODUCTS_SUBCATS_TITLE', 'Mostrar artículos de subcategorías');
define('CATEGORIES_SHOW_PRODUCTS_SUBCATS_DESC', '¿Deben mostrarse todos los artículos de las subcategorías existentes en el listado?');

define('SEO_URL_MOD_CLASS_TITLE', 'Módulo URL');
define('SEO_URL_MOD_CLASS_DESC', 'Seleccione un módulo URL.');

define('MODULE_BANNER_MANAGER_STATUS_TITLE', 'Administrador de Banners');
define('MODULE_BANNER_MANAGER_STATUS_DESC', '¿Activar Administrador de Banners?');

define('MODULE_NEWSLETTER_STATUS_TITLE', 'Newsletter');
define('MODULE_NEWSLETTER_STATUS_DESC', '¿Activar el sistema de newsletter?');

define('GOOGLE_CERTIFIED_SHOPS_MERCHANT_ACTIVE_TITLE', 'Activar Google Certified Shops Merchant');
define('GOOGLE_CERTIFIED_SHOPS_MERCHANT_ACTIVE_DESC', '¿Debe utilizarse el código del Google Certified Shops Merchant?<br/><br/><b>Atención:</b> ¡Esto sólo funciona con una plantilla compatible a partir de la versión 2.0.1.0!');
define('GOOGLE_SHOPPING_ID_TITLE', 'ID de Google Shopping');
define('GOOGLE_SHOPPING_ID_DESC', 'Su ID de Google Shopping');
define('GOOGLE_TRUSTED_ID_TITLE', 'ID de Google Trusted');
define('GOOGLE_TRUSTED_ID_DESC', 'Su ID de Google Trusted');

define('EMAIL_ARCHIVE_ADDRESS_TITLE', 'Archivo - Dirección de correo electrónico');
define('EMAIL_ARCHIVE_ADDRESS_DESC', 'Por favor, introduzca una dirección de correo electrónico para archivar todos los correos electrónicos salientes. De este modo, los correos electrónicos se enviarán a un buzón de archivo a través de BCC.');

define('DISPLAY_HEADQUARTER_ON_CHECKOUT_TITLE', 'Sede de la empresa en Checkout');
define('DISPLAY_HEADQUARTER_ON_CHECKOUT_DESC', '¿Debe mostrarse la sede de la empresa al realizar el checkout?');

//BOC new lang constants for guset status ids (group 17 (= Zusatzmodule)), noRiddle
define('GUEST_STATUS_IDS_TITLE', 'IDs de estado de invitados');
define('GUEST_STATUS_IDS_DESC', 'Introduzca todos los ID de grupos de clientes de invitados separados por comas.');
//EOC new lang constants for guset status ids (group 17 (= Zusatzmodule)), noRiddle
?>

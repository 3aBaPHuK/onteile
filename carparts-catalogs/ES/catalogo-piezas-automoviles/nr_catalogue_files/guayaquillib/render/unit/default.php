<?php

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'template.php';
require_once 'image.php';
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'details'.DIRECTORY_SEPARATOR.'detailslist.php';

class GuayaquilUnit extends GuayaquilTemplate
{
	var $categories = NULL;
	var $catalog = NULL;
	var $ssd = NULL;

	var $unitimagerenderer = NULL;
	var $detaillistrenderer = NULL;

	var $closedimage = '../details/images/closed.gif';
	var $cartimage = '../details/images/cart.gif';
	var $detailinfoimage = '../details/images/info.gif';
	var $mouse_wheel = '../images/mouse_wheel.png';
	var $mouse = '../images/mouse.png';
	var $lmb = '../images/lmb.png';
	var $move = '../images/move.png';
	var $arrow = '../images/pointer.png';

	var $containerwidth = 960;
	var $containerheight = 600;
	var $drawlegend = 1;

    var $drawtoolbar = true;

	function __construct(IGuayaquilExtender $extender)
	{
		parent::__construct($extender);

		$this->unitimagerenderer = $this->CreateUnitImageRenderer();
		$this->detaillistrenderer = $this->CrateDetailListRenderer();

		$this->closedimage = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->closedimage);
		$this->cartimage = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->cartimage);
		$this->detailinfoimage = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->detailinfoimage);
		$this->mouse_wheel = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->mouse_wheel);
		$this->mouse = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->mouse);
		$this->lmb = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->lmb);
		$this->move = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->move);
		$this->arrow = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->arrow);
	}

	function CreateUnitImageRenderer()
	{
		return new GuayaquilUnitImage($this->extender);
	}

	function CrateDetailListRenderer()
	{
		return new GuayaquilDetailsList($this->extender);
	}

	function Draw($catalog, $unit, $imagemap, $details, $replacements, $cataloginfo, $prices = array(), $availability = array())
	{
		$this->catalog = $catalog;
		$this->ConfigureUnitImageRenderer($this->unitimagerenderer);
		$this->ConfigureDetailListRenderer($this->detaillistrenderer);

        if (CommonExtender::isFeatureSupported($cataloginfo, 'quickgroups'))
            GuayaquilToolbar::AddButton($this->GetLocalizedString('QuickGroupsLink'), $this->FormatLink('quickgroup', null, $this->catalog));
        $unit['note']='';
        foreach ($unit->attribute as $attr)
            $unit['note'].='<b>'.(string) ($attr->attributes()->name) .'</b>: ' .(string) $attr->attributes()->value.'<br/>';
        $note = $this->GetNote($unit['note']);
        if ($note)
            echo '<p class="guayaquil_unit_note">'.str_replace(array('<br>', '<br />'), ' | ', $note).'</p>';

        //BOC extend toolbar, noRiddle
        $html = '<div id="main-top cf">';
        $html .= '<div class="log-and-butt cf">';
        //$html .= '<div class="brand-logo"><img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/'.Config::$nr_icon_arr[$_GET['c']].'" alt ="" /></div>';
        $html .= '<div class="top-buttons cf">';
        
        //$html = $this->drawtoolbar ? GuayaquilToolbar::Draw() : '';
        $html .= $this->drawtoolbar ? GuayaquilToolbar::Draw() : '';

        $html .= '<a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=1004&c='.$_GET['c'].'&vid='.$_GET['vid'].'&ssd='.$_GET['ssd']).'"><span class="cssButton cssButtonColor1 cssbtn-fl"><i class="fa fa-eye"></i><span class="cssButtonText">'.CommonExtender::FormatLocalizedString('vehiclelink').'</span></span></a>';
        $html .= '<a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=1003&c='.$_GET['c'].'&vid='.$_GET['vid'].'&ssd='.$_GET['ssd']).'"><span class="cssButton cssButtonColor1 cssbtn-fr"><i class="fa fa-eye"></i><span class="cssButtonText">'.CommonExtender::FormatLocalizedString('quickgroupslink').'</span></span></a>';
        /*.'<a href="/katalog_shop_test/"><span class="btn button-catalogue"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_brand_choice').'</span></a>&nbsp;'
        .'<a href="'.xtc_href_link(FILENAME_CATALOGUE_SHOPPING_CART).'"><span class="btn button-catalogue"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_shopp_cart').'</span></a>';*/

        if ($this->drawlegend)
			$html .= $this->DrawLegend();
        
        $html .= '</div>';
        $html .= '</div>'; //end log-and-butt
        $html .= '</div>'; //end main-top
        //EOC extend toolbar, noRiddle
        
        $html .= $this->DrawContainer($catalog, $unit, $imagemap, $details, $replacements, $prices, $availability);

		/*if ($this->drawlegend)
			$html .= $this->DrawLegend();*/

		//BOC don't need link to laximo, noRiddle
        //$html .= '<div style="visibility: visible; display: block; height: 20px; text-align: right;"><a href="http://dev.laximo.ru" rel="follow" style="visibility: visible; display: inline; font-size: 10px; font-weight: normal; text-decoration: none;">guayaquil</a></div>';
        //$html .= '<div style="display:block; height:20px; text-align:right;"></div>';
        //EOC don't need link to laximo, noRiddle

		return $html;
	}

    function GetNote($note)
    {
        return str_replace("\n", '<br>', (string)$note);
    }

	function ConfigureUnitImageRenderer($unitimagerenderer)
	{
		$unitimagerenderer->containerwidth = $this->containerwidth;
		$unitimagerenderer->containerheight = $this->containerheight;
	}

	function ConfigureDetailListRenderer($detaillistrenderer)
	{

	}

	function DrawContainer($catalog, $unit, $imagemap, $details, $replacements, $prices, $availability)
	{
		//$html = '<div id="g_container" style="vertical-align:top;height:'.($this->containerheight + 2).'px;width:100%; overflow:hidden;">';
        $html = '<div class="hr_15"></div>';
        //$html .= '<div id="g_container" style="vertical-align:top; height:'.($this->containerheight + 2).'px; width:100%;">'; //no overflow:hidden here for fixed table head, noRiddle
        $html .= '<div id="g_container" class="cf" style="vertical-align:top; width:100%;">'; //no overflow:hidden here for fixed table head, noRiddle

		$html .= $this->DrawUnitImage($catalog, $unit, $imagemap);
		$html .= $this->DrawDetailList($catalog, $details, $replacements, $prices, $availability);

		$html .= '</div>';

		return $html;
	}

	function DrawUnitImage($catalog, $unit, $imagemap)
	{
		//$html = '<div style="width:49%; height:100%;" class="inline_block">';
        $html = '<div style="height:100%;" class="gen-fl w49perc u-img">';

		$html .= $this->unitimagerenderer->Draw($catalog, $unit, $imagemap);
		
		$html .= '</div>';

		return $html;
	}

	function DrawDetailList($catalog, $details, $replacements, $prices, $availability)
	{
		//$html = '<div id="viewtable" style="overflow:auto; width:49%; height:100%;" class="inline_block">';
        //$html = '<div id="viewtable" style="width:49%; height:100%;" class="inline_block">'; //no overflow:auto here for fixed header table, noRiddle
        $html = '<div id="viewtable" style="height:100%;" class="gen-fr w49perc u-tbl">'; //no overflow:auto here for fixed header table, noRiddle

		$html .= $this->detaillistrenderer->Draw($catalog, $details, $replacements, $prices, $availability);

        $html .= '</div>';

		return $html;
	}

	function DrawLegend()
	{
        //BOC reformat, noRiddle
        /*$html  = '<table width="100%" border="0" style="margin-top:15px">'; //increase margin-top from 5 to 15px, noRiddle
        $html .= '<tr>';
        //$html .= '<th align=center colspan=4>'.$this->GetLocalizedString('UNIT_LEGEND').'</th>';
        $html .= '<th align="left" colspan="4">'.$this->GetLocalizedString('UNIT_LEGEND').'</th>'; //align left, noRiddle
        $html .= '</tr><tr>';
        $html .= '<td align="left"><img src="'.$this->mouse_wheel.'" alt="" title="'.$this->GetLocalizedString('unit_legend_mouse_wheel').'"></td>';
        $html .= '<td> - '.$this->GetLocalizedString('UNIT_LEGEND_IMAGE_RESIZING').'</td>';
        $html .= '<td align="left"><img src="'.$this->lmb.'"> <img src="'.$this->move.'"></td>';
        $html .= '<td> - '.$this->GetLocalizedString('UNIT_LEGEND_MOUSE_SCROLL_IMAGE').'</td>';
        //$html .= '</tr><tr>';
        //$html .= '<td align=center><img src="'.$this->mouse.'"> <img src="'.$this->arrow.'"></td>';
        $html .= '<td align="left"><img src="'.$this->mouse.'"> <img src="'.$this->arrow.'"></td>'; //align left, noRiddle
        $html .= '<td> - '.$this->GetLocalizedString('UNIT_LEGEND_HIGHLIGHT_PARTS').'</td>';
        //BOC not needed since we display details in table, noRiddle
        //$html .= '<td align="left"><img src="'.$this->lmb.'">   <img src="'.$this->detailinfoimage.'"></td>';
        //$html .= '<td> - '.$this->GetLocalizedString('UNIT_LEGEND_SHOW_HIND').'</td>';
        //$html .= '<td></td><td></td>';
        //EOC not needed since we display details in table, noRiddle
        $html .= '</tr></table>';*/
        
        $html .= '<div id="legend" class="cssbtn-fl w48perc">';
        //$html .= '<p><button class="btn button-catalogue" type="button"><i class="glyphicon glyphicon-plus"></i> <strong>'.$this->GetLocalizedString('UNIT_LEGEND').'</strong></button></p>';
        $html .= '<p>';
        $html .= '<span class="cssButton cssButtonColor1"><i class="fa fa-plus"></i>';
        $html .= '<span class="cssButtonText">'.$this->GetLocalizedString('UNIT_LEGEND').'</span>';
        $html .= '<button class="cssButtonText" type="button" title="'.$this->GetLocalizedString('UNIT_LEGEND').'">'.$this->GetLocalizedString('UNIT_LEGEND').'</button>';
        $html .= '<i class="fa fa-plus"></i></span>';
        $html .= '</p>';

        $html  .= '<div><table width="100%" border="0">';
        $html .= '<tr>';
        $html .= '<td align="left"><img src="'.$this->mouse_wheel.'" alt="" title="'.$this->GetLocalizedString('unit_legend_mouse_wheel').'"></td>';
        $html .= '<td> '.$this->GetLocalizedString('UNIT_LEGEND_IMAGE_RESIZING').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="left"><img src="'.$this->lmb.'"> <img src="'.$this->move.'"></td>';
        $html .= '<td> '.$this->GetLocalizedString('UNIT_LEGEND_MOUSE_SCROLL_IMAGE').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="left"><img src="'.$this->mouse.'"> <img src="'.$this->arrow.'"></td>';
        $html .= '<td> '.$this->GetLocalizedString('UNIT_LEGEND_HIGHLIGHT_PARTS').'</td>';
        $html .= '</tr>';
        $html .= '</table></div></div>';
        //EOC reformat, noRiddle
        
		return $html;
	}

    function DrawQuickGroupsLink()
    {
        $link = $this->FormatLink('quickgroup', null, $this->catalog);
        return '<div class="gQuickGroupsLink"><a href="'.$link.'">'.$this->GetLocalizedString('QuickGroupsLink').'</a></div>';
    }
}

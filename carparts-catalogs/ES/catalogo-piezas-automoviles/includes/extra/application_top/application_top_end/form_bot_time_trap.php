<?php
/*************************************************************
* file: form_bot_time_trap.php
* use: prevent bots from sending contact form
* extended for create_account, 03-2020, noRiddle
*
* (c) p3e, h-h-h: https://www.modified-shop.org/forum/index.php?topic=38497.msg356053#msg356053 und folgende
* ebenfalls interessant unter meiner Mithilfe: https://www.modified-shop.org/forum/index.php?topic=40400.msg367662#msg367662
*************************************************************/
// entspricht der Zeit in Sekunden, die der Kunde zum Ausfüllen mindestens benötigen soll 
// (default=2) - falls Bots durchkommen schrittweise erhöhen
defined('VV_TIME_MIN') || define('VV_TIME_MIN', 4);

if (basename($_SERVER["SCRIPT_NAME"], '.php') == 'display_vvcodes') {
        $_SESSION['vvtime'] = time();   // p3e Captcha Zeitstempel 2018-08-15
}

if (isset($_GET['coID']) && $_GET['coID'] == 7 && isset($_GET['action']) && $_GET['action'] == 'send') {
    if (isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['message_body']) && !empty($_POST['message_body'])) {
        // BOF p3e Captcha Zeitstempel 2018-08-15
        if (isset($_SESSION['vvtime'])) {
            if (time() - $_SESSION['vvtime'] < VV_TIME_MIN) {
                // Erfolgsmeldung ohne Versand der Mail
                xtc_redirect(xtc_href_link(FILENAME_CONTENT, 'action=success&coID='.(int) $_GET['coID']));
            }
            unset($_SESSION['vvtime']); //unset measured time, noRiddle
        }
        // EOF p3e Captcha Zeitstempel 2018-08-15
    }
}


//BOC time trap for create_account against bots, 20-2020, noRiddle
defined('CHK_TIME_MIN') || define('CHK_TIME_MIN', 5);

if(basename($_SERVER["SCRIPT_NAME"], '.php') == 'create_account' || basename($_SERVER["SCRIPT_NAME"], '.php') == 'create_guest_account') {
    if(!isset($_POST['action']) || (isset($_POST['action']) && $_POST['action'] != 'process')) {
        $_SESSION['timechk'] = time();   // Zeitstempel
    }
    if(isset($_POST['action']) && ($_POST['action'] == 'process')) {
        if(isset($_SESSION['timechk'])) {
            if((time() - $_SESSION['timechk']) <= (int)CHK_TIME_MIN) {
                // stinkt nach Bot - ab auf die Startseite
                xtc_redirect(xtc_href_link(FILENAME_DEFAULT));
            }
            unset($_SESSION['timechk']);
        } /*else {
            xtc_redirect(xtc_href_link(FILENAME_DEFAULT));
        }*/
    }
} else {
    if(isset($_SESSION['timechk']))
        unset($_SESSION['timechk']);
}
//EOC time trap for create_account against bots, 20-2020, noRiddle
?>
#/* -----------------------------------------------------------------------------------------
# $Id: lang_german.conf 10896 2017-08-11 11:31:54Z Tomcraft $
#
# modified eCommerce Shopsoftware
# http://www.modified-shop.org
#
# Copyright (c) 2009 - 2013 [www.modified-shop.org]
# -----------------------------------------------------------------------------------------
# based on:
# (c) 2003 nextcommerce (lang_german.conf,v 1.3 2003/08/24); www.nextcommerce.org
# (c) 2003 XT-Commerce
# Released under the GNU General Public License
# ---------------------------------------------------------------------------------------*/

[sitemap]
heading_sitemap = 'Mapa del sitio'

[error_handler]
text_search = 'Término de búsqueda:'
text_search_again = 'La búsqueda no ha producido un resultado exacto.<br /><strong>¿Quiere volver a buscar?</strong>'
text_search_advanced1 = 'Alternativamente también puede utilizar la'
text_search_advanced2 = 'búsqueda avanzada'
text_search_advanced3 = 'con más parametros de búsqueda.'
 
[upcoming_products]
heading_upcoming = '¡Disponible en breve!'

[new_password]
heading_password = 'Mi contraseña'
text_introduction = 'Cambie su contraseña en tres simples pasos.'
text_step1 = 'Primer paso:'
text_to_step2 = ' ir a segundo paso'
text_info_pre = 'Por favor, introduzca abajo el código de seguridad y la dirección de correo electrónico con la que ha creado su cuenta en'
text_info_post = '.<br /><br /><strong>Paso 2:</strong><br />Por favor, confirme el enlace que le hacemos llegar en un correo electrónico. En la página que se abrirá ahora puede asignar una nueva contraseña.<br /><br /><strong>Paso 3:</strong><br />Inicie la sesión con esta nueva contraseña.'
text_continue = '¡Haga clic en Continuar y recibirá inmediatamente un correo electrónico con un enlace de confirmación válido!'
text_email = 'Dirección de correo electrónico:'
text_error = '<span class="color_error_message"><strong>ATENCIÓN:</strong></span>La dirección de correo electrónico introducida no es registrada. Por favor, inténtalo de nuevo.'
text_sec_code = 'Código de seguridad'
text_inp_code = 'por favor introducir aquí'

[newsletter]
heading_title = 'Newsletter'
heading_personal = 'Sus datos'
heading_text = 'Nuestro Newsletter'
text_email = 'Su dirección de correo electrónico'
text_in = 'Deseo suscribirme al newsletter (retiro es posible en todo momento).'
text_zusatz = 'Deseo recibir regularmente ofertas interesantes por correo electrónico. Mi dirección de correo electrónico no se transmitirá a otras empresas. Puedo revocar este consentimiento para el uso de mi dirección de correo electrónico para fines publicitarios en cualquier momento con efecto para el futuro.'
text_out = 'cancelar la suscripción'
text_sec_code = 'Código de seguridad'
text_inp_code = 'Introducir aquí'

[popup_search_help]
title_help = 'Ayuda para búsqueda avanzada'
text_help = 'La función de búsqueda le permite buscar dentro de nombres de artículos, descripciones de artículos, fabricantes y números de artículo.<br /><br />Tiene la posibilidad de utilizar operadores lógicos como "AND" (Y) y "OR" (o).<br /><br />Zum Por ejemplo, podría especificar: <span class="underline">Microsoft AND Ratón</span>.<br /><br />Además, puede utilizar paréntesis para jerarquizar la búsqueda, por ejemplo:<br /><br /><span class="underline">Microsoft AND (Ratón OR Teclado OR "Visual Basic")</span>.<br /><br />Con las comillas puede agrupar varias palabras en un solo término de búsqueda.'
text_close = '<span class="underline">Cerrar ventana</span> [x]'

[popup_coupon_help]
title_help = 'Información acerca de su bono/cupón'
text_close = '<span class="underline">Cerrar ventana</span> [x]'

[reviews]
heading_reviews = 'Reseñas de clientes:'
heading_product_reviews = 'Reseñas sobre:'
heading_reviews_write = '¿Qué opina Usted?'
text_input = 'Su reseña:'
text_author = 'Autor:'
text_rating = 'Evaluación:'
text_all = 'mostrar todas las reseñas'
title_nr = 'N°.'
title_author = 'Autor'
title_rating = 'Evaluación'
title_date = 'Fecha'
text_author = 'Autor:'
text_date = 'Fecha: '
text_rating = 'Evaluación:'
text_text = 'Reseña:'
text_product = 'Artículo:'
text_note = '<span class="color_error_message"><strong>ATENCIÓN:</strong></span> ¡HTML no está soportado!'
text_bad = 'MAL'
text_good = 'MUY BIEN'
text_write_first_review = '¡Escriba la primer reseña de cliente!'

[checkout_new_address]
title_new_address = 'Nueva dirección'
text_new_address = 'Por favor utilice este formulario para registrar una nueva dirección para su pedido'
text_gender = 'Saludo:'
text_male = 'Sr.'
text_female = 'Sra.'
text_firstname = 'Nombre:'
text_lastname = 'Apellido:'
text_company = 'Nombre de la empresa:'
text_ustid = 'RFC:'
title_address = 'Su dirección'
text_street = 'Calle/N°:'
text_suburb = 'Dirección adicional:'
text_code = 'Código postal:'
text_city = 'Ciudad:'
text_state = 'Estado federal:'
text_country = 'País:'
text_must = '* información necesaria'

[cross_selling]
heading_text = 'Recomendamos los siguientes productos:'

[reverse_cross_selling]
heading_text = 'Este producto es, por ejemplo, compatible con:'

[checkout_payment_address]
heading_address = 'Información sobre la dirección de factura'
title_current_address = 'Dirección de factura'
text_current_address = 'Esta es la dirección actualmente seleccionada a la que se emitirá su factura.'
title_old_addresses = 'Direcciones de su libreta de direcciones'
text_old_addresses = 'Tiene la posibilidad de seleccionar direcciones ya existentes de su libreta de direcciones.'
title_continue = 'Continuación del proceso de pedido'
text_continue = 'A la selección de la modalidad de pago deseada.'
text_shipping_info = 'Información de envío'
text_payment_info = 'Modalidad de pago'
text_confirm = 'Confirmación'
text_finished = '¡Listo!'
text_current_address_short = 'Su dirección de factura actualmente seleccionada'

[checkout_shipping_address]
heading_address = 'Información sobre la dirección de envío'
title_current_address = 'Dirección de envío'
text_current_address = 'Esta es la dirección actualmente seleccionada a la que se enviará su pedido:'
title_old_addresses = 'Direcciones de su libreta de direcciones'
text_old_addresses = 'Tiene la posibilidad de seleccionar direcciones ya existentes de su libreta de direcciones.'
title_continue = 'Continuación del proceso de pedido'
text_continue = 'A la selección del modo de envío deseado.'
text_shipping_info = 'Información de envío'
text_payment_info = 'Modalidad de pago'
text_confirm = 'Confirmación'
text_finished = '¡Listo!'
text_current_address_short = 'Su dirección de envío actualmente seleccionada'

[ssl_check]
heading_ssl_check = 'Aviso de seguridad'
text_information = 'Nuestra comprobación de seguridad ha demostrado que su navegador utilizado ha cambiado el ID de la sesión SSL durante su visita.<br /><br />Por razones de seguridad le pedimos que vuelva a iniciar la sesión.<br /><br />Algunos navegadores, como por ejemplo Konqueror 3.1, no pueden generar automáticamente el ID de sesión Secure SSL; sin embargo, esto es un requisito previo para nosotros. Si utiliza este tipo de navegador, le pedimos que instale una versión actualizada o que utilice otro navegador. Por ejemplo <a href="http://www.microsoft.com/ie/" target="_blank">Microsoft Internet Explorer</a> o <a href="http://www.mozilla-europe.org/" target="_blank">Mozilla Firefox</a>. Después de esta actualización usted debería poder comprar con nosotros sin ningún problema, con la certeza de estar en el lado seguro.<br /><br />Esta medida de seguridad sirve para su propia seguridad. Si todavía tiene algún problema, le rogamos que nos disculpe y estaremos encantados de responder a su consulta.'
title_infobox = 'Privacidad y seguridad'
text_infobox = 'El ID de sesión SSL generado por su navegador es comprobado por nosotros con cada acceso a una página segura.<br /><br />Esta comprobación garantiza que el acceso ha sido realmente realizado por su navegador y que no se ha originado en otro navegador.'

[cookie_usage]
heading_cookie_usage = 'Uso de cookies'
text_information = 'Antes de enviar su pedido a %s verá nuevamente los datos y artículos que ha introducido. Haciendo clic en <span class="color_edit_info">(Editar)</span> A la izquierda de los datos individuales tiene de nuevo la posibilidad de corregirlos.<br />Puede cancelar el proceso de pedido en cualquier momento cerrando la ventana del navegador, o puede finalizarlo pulsando el botón <strong>Comprar</strong>.'
title_infobox = 'Cookies y privacidad'
text_infobox = 'Mediante el uso de cookies se mejora la seguridad y se protege mejor su privacidad.<br /><br />Los cookies no contienen datos personales.'

[address_book_process]
heading_address = 'Libreta de direcciones:'
text_gender = 'Saludo:'
text_male = 'Sr.'
text_female = 'Sra.'
text_firstname = 'Nombre:'
text_lastname = 'Apellido:'
text_company = 'Nombre de la empresa:'
text_ustid = 'RFC:'
title_address = 'Su dirección'
text_street = 'Calle/N°:'
text_suburb = 'Dirección adicional:'
text_code = 'Código postal:'
text_city = 'Ciudad:'
text_state = 'Estado federal:'
text_country = 'País:'
text_must = '* información necesaria'
text_standard = 'Dirección estándar:'
title_delete = 'Borrar entrada'
text_delete = '¿Desea borrar irrevocablemente esta dirección de su libreta de direcciones?'
text_address = 'Dirección estándar'

[address_book]
heading_address = 'Mi libreta de direcciones personal'
title_standard = 'Dirección estándar'
text_standard = 'Esta dirección se elige automáticamente como dirección de entrega y de factura al realizar un pedido.<br /><br />Esta dirección también se utiliza como base para el cálculo de posibles impuestos y costos de envío.'
title_addresses = 'Entradas en la libreta de direcciones'

[account_edit]
heading_account = 'Cambiar sus datos personales'
title_personal = 'Sus datos personales'
title_contact = 'Su datos de contacto'
text_gender = 'Saludo:'
text_male = 'Sr.'
text_female = 'Sra.'
text_firstname = 'Nombre:'
text_lastname = 'Apellido:'
text_birthdate = 'Fecha de nacimiento:'
text_email = 'Dirección de correo electrónico:'
text_confirm_email = 'Repetir dirección de correo electrónico:'
text_tel = 'Número de teléfono:'
text_fax = 'Fax:'
text_must = '* información necesaria'
csID = 'Número de cliente:'
text_ustid = 'RFC:'

#password aendern / kundenkonto
[account_password]
heading_password = 'Mi contraseña'
text_actual = 'Contraseña actual:'
text_new = 'Nueva contraseña:'
text_confirm = 'Confirmación'
text_must = '* información necesaria'
text_account_info = 'Sus datos'
text_password_policy = 'Una contraseña segura debe tener %s caracteres y debe contener no sólo letras mayúsculas y minúsculas, sino también números y caracteres especiales.'

#erweiterte suche
[advanced_search]
heading_search = 'Otros criterios de búsqueda'
title_keywords = 'Introduzca sus términos de búsqueda:'
text_description = 'También buscar en los descripciones'
text_help = 'Ayuda para búsqueda avanzada'
text_categories = 'Categorías:'
text_subkategories = 'Incluir subcategorías'
text_manufacturer = 'Fabricante:'
text_min = 'Precio desde:'
text_max = 'Precio hasta:'
text_from = 'añadido por:'
text_to = 'añadido hasta:'

[contact_us]
#BOF - Tomcraft - 2009-11-05 - Advanced contact form
text_phone = 'Teléfono:'
text_company = 'Empresa:'
text_street = 'Calle:'
text_fax = 'Fax:'
text_city = 'Ciudad:'
text_postcode = 'Código postal:'
text_hint = 'Campos marcados con * son obligatorios.'
#EOF - Tomcraft - 2009-11-05 - Advanced contact form
text_name = 'Su nombre:'
text_email = 'Su dirección de correo electrónico:'
text_confirm_email = 'Repetir dirección de correo electrónico:'
text_message = 'Su mensaje:'
text_success = 'Su solicitud nos ha sido enviada con éxito.'
text_contact_us = 'Nuestros expertos le ayudan'
text_thanks = 'Muchas gracias'
text_data = 'Sus datos'

#abmelden
[logoff]
heading_logoff = '¡Hasta la próxima!'
text_logoff = 'Ha cerrado su sesión con éxito. Muchas gracias por haber visitado nuestra tienda.'

# accounterstellung
[create_account]
heading_create_account = 'Información sobre su cuenta de cliente'
heading_create_guest_account = 'Su dirección de cliente'
title_personal = 'Sus datos personales'
text_gender = 'Saludo:'
text_male = 'Sr.'
text_female = 'Sra.'
text_firstname = 'Nombre:'
text_lastname = 'Apellido:'
text_birthdate = 'Fecha de nacimiento:'
text_email = 'Dirección de correo electrónico:'
text_confirm_email = 'Repetir dirección de correo electrónico:'
title_company = 'Datos de la empresa (sólo clientes B2B)'
text_company = 'Nombre de la empresa:'
text_ustid = 'RFC:'
title_address = 'Su dirección'
text_street = 'Calle/N°:'
text_suburb = 'Dirección adicional:'
text_code = 'Código postal:'
text_city = 'Ciudad:'
text_state = 'Estado federal:'
text_country = 'País:'
title_contact = 'Su información de contacto'
text_tel = 'Teléfono:'
text_fax = 'Fax:'
title_options = 'Opciones'
title_newsletter = 'Newsletter'
text_newsletter = 'Deseo recibir regularmente ofertas interesantes por correo electrónico. Mi dirección de correo electrónico no será compartida con otras empresas. Puedo revocar este consentimiento para el uso de mi dirección de correo electrónico con fines publicitarios en cualquier momento con efecto para el futuro.'
title_password = 'Asegura su informacion con una contraseña.'
text_intro_password = ''
text_password = 'Su contraseña:'
text_confirmation = 'Confirmar contraseña'
text_must = '* información necesaria'
text_privacy_accept = 'Acepto la política de privacidad'
text_password_policy = 'Una contraseña segura debe tener %s caracteres y debe contener no sólo letras mayúsculas y minúsculas, sino también números y caracteres especiales.'
text_sec_code = 'Código de seguridad'
text_inp_code = 'Introducir aquí'


#bestelluebersicht
[account_history]
heading_history = 'Sus pedidos realizados'
text_oid = 'Número de pedido: '
text_date = 'Fecha del pedido: '
text_shipped = 'Enviar a: '
text_articles = 'Artículo: '
text_total = 'Total'
text_status = 'Estado de pedido: '
label_tracking = 'Seguimiento de envíos:'

#bestelldetails
[account_history_info]
heading_history_info = 'Información de pedido'
text_date = 'Fecha de pedido: '
text_oid = 'Número de pedido: '
text_shipping_address = 'Dirección de envío:'
text_shipping_method = 'Modo de envío:'
title_payment_info = 'Información de factura'
text_payment_info = 'Información de pago'
text_payment_address = 'Dirección de factura:'
text_payment_method = 'Modalidad de pago:'
text_products = 'Artículos:'
text_comments = 'Sus comentarios:'
title_history = 'Historial de pedidos'
title_print = 'Versión impresa del pedido'
text_print = 'Para su confirmación puede imprimir el pedido.'
label_tracking = 'Seguimiento de envíos:'

[checkout_success]
heading_success = '¡Su pedido ha sido procesado con éxito!'
text_success = 'Su pedido ha sido recibido y será procesado inmediatamente. ¡Ha recibido un correo electrónico de confirmación con los datos del pedido para su comprobación!'
text_notifications = 'Por favor, infórmeme sobre novedades acerca de los siguientes artículos:'
title_printorder = 'Versión impresa del pedido'
text_printorder = 'Para su control puede imprimir el pedido.'
text_thankyou = '¡Le agradecemos por su compra online!'
text_shipping_info = 'Información de envío'
text_payment_info = 'Modalidad de pago'
text_confirm = 'Confirmación'
text_finished = '¡Listo!'
gv_has_vouchera = 'Dispone de un saldo. Este lo puede enviar <br />por <a class="pageResults" href="'
gv_has_voucherb = '"><strong>correo electrónico</strong></a> .'


#bestellbestaetigungsseite
[checkout_confirmation]
heading_confirmation = 'Comprobar pedido:'
text_shipping_address = 'Dirección de envío:'
text_shipping_method = 'Modo de envío:'
title_payment_info = 'Información de factura'
text_payment_info = 'Información de pago:'
text_payment_address = 'Dirección de factura:'
text_payment_method = 'Modalidad de pago:'
text_products = 'Artículos:'
text_edit = '(Editar)'
text_comments = 'Sus comentarios:'
text_shipping_info = 'Información de envío:'
text_payment_info = 'Información de pago:'
text_confirm = 'Confirmación:'
text_finished = '¡Listo!'
text_ip = 'Para evitar el mal uso de nuestra oferta, almacenamos su dirección IP mientras protegemos su privacidad:'
text_information = 'Antes de enviar su pedido a %s verá nuevamente los datos y artículos que ha introducido. Haciendo clic en <span class="color_edit_info">(Editar)</span> A la izquierda de los datos individuales tiene de nuevo la posibilidad de corregirlos.<br />Puede cancelar el proceso de pedido en cualquier momento cerrando la ventana del navegador, o puede finalizarlo pulsando el botón <strong>Comprar</strong>.'
text_express_checkout_information = 'Aquí puede guardar sus datos de envío y factura para su próximo pedido.'
heading_checkout_express = 'Configuración para Mi Compra Rápida'

#Auswahl der Zahlungsweise
[checkout_payment]
heading_payment = 'Modalidad de pago'
title_address = 'Dirección de factura'
text_address = 'Por favor, seleccione de su libreta de direcciones la dirección de factura para su pedido:'
title_payment = 'Modalidad de pago'
text_payment = 'Por favor seleccione la modalidad de pago deseada para su pedido:'
title_comments = 'Agregue aquí sus comentarios a este pedido:'
title_continue = 'Continuación del proceso de pedido'
text_continue = 'A la confirmación de su pedido'
text_shipping_info = 'Información de envío'
text_payment_info = 'Modalidad de pago'
text_confirm = 'Confirmación'
text_finished = '¡Listo!'
title_agb = 'Términos y condiciones generales:'
text_accept_agb = 'Acepto los términos y condiciones generales y de envío'
text_gccover = 'Su cupón cubre el precio del artículo, ahora sólo tiene que aceptar nuestros términos y condiciones generales y hacer clic en Continuar para seguir con el proceso de pedido.'
title_comments_short = 'Sus comentarios'
text_accept_revocation = 'Por la presente reconozco que el derecho de revocación para los artículos digitales expira.'
text_nopayment = 'Ahora sólo tiene que aceptar nuestros términos y condiciones generales y hacer clic en Continuar para seguir con el proceso de pedido.'

# Auswahl der Versandart
[checkout_shipping]
heading_shipping = 'Opciones de envío'
title_address = 'Dirección de envío'
text_addressbook = 'Por favor seleccione de su libreta de direcciones la dirección de envío para su pedido.'
title_shipping = 'Modo de envío'
text_shipping = 'Por favor seleccione el modo de envío deseado para su pedido.'
title_continue = 'Continuación del proceso de pedido'
text_continue = 'A la selección de la modalidad de pago deseada.'
text_shipping_info = 'Información de envío'
text_payment_info = 'Modalidad de pago'
text_confirm = 'Confirmación'
text_finished = '¡Listo!'

#account Uebersicht
[account]
heading_account = 'Su página personal'
title_main = 'Vista general'
text_all = 'Mostrar todos los pedidos'
text_current = 'Pedidos anteriores'
title_welcome = 'Bienvenida/o!'
text_welcome = 'Aquí se crea su página personal, donde puede ver cómodamente todos sus pedidos, así como una lista de los productos que ha visitado por último.<br /><br />En el caso de tener una cuenta con nosotros, puede además cambiar aquí sus datos personales.'
title_account = 'Sus configuraciones personales'
text_edit = 'Editar datos de cuenta'
text_address = 'Editar libreta de direcciones'
text_password = 'Cambiar contraseña'
title_orders = 'Mis pedidos'
text_orders = 'Mostrar mis pedidos anteriores'
title_notification = 'Notificaciones por correo electrónico'
text_goto_cat = 'Categoría:'
title_viewed_products = 'Sus últimos artículos vistos'
text_newsletter = 'Suscribirse o anularse del newsletter.'
text_notifications = 'Mostrar o cambiar mis notificaciones de artículos'
order_nr = 'N°. de pedido: '
order_total = 'Importe del pedido: '
order_status = 'Estado: '
text_login = 'Iniciar sesión o crear una nueva cuenta'
text_delete = 'Borrar Cuenta'
text_oid = 'Número de pedido: '
text_date = 'Fecha del pedido: '
text_shipped = 'Enviar a: '
text_articles = 'Artículo: '
text_total = 'Total del pedido'
text_status = 'Estado de pedido: '
label_tracking = 'Seguimiento de envíos:'
text_express_checkout = 'Editar Mi Compra Rápida'

#categorie listing
[categorie_listing]
heading_more_categories = 'Otras subcategorías:'

#shopping cart
[shopping_cart]
heading_cart = 'Su cesta de compra contiene:'
text_empty = 'Su cesta de compra está vacía.'
text_remove = 'Eliminar'
text_qty = 'Cantidad'
text_article = 'Artículo'
text_total = 'Total'
text_single = 'Precio unitario'
text_total_weight = 'Peso total (incluido el embalaje):'
text_tax = 'IVA'
checkout_express_activate = 'Activar Mi Compra Rápida »'

#login
[login]
heading_login = 'Por favor, seleccione'
title_new = 'Soy un cliente nuevo y deseo registrarme'
text_new = 'Por su registro tiene la posibilidad de hacer pedidos más rápidamente, de saber el estado de su pedidos en todo momento y de tener un resumen de todos sus pedidos realizados hasta el momento.'
title_returning = 'Ya soy cliente'
text_returning = '<strong>¿Ya ha pedido antes con nosotros?</strong><br /><br />Muchas gracias por confiar en nosotros una vez más.'
text_email = 'Dirección de correo electrónico:'
text_password = 'Contraseña:'
text_lost_password = '¿Olvidó su contraseña?'
title_guest = 'Me gustaría hacer un pedido sin registrarme.'
text_guest = 'También puede realizar su pedido sin registro.<br />Por favor, tenga en cuenta que tendrá que volver a introducir sus datos cada vez que realice un pedido.'
text_after_login = '<p>Después de iniciar sesión, se le redirigirá automáticamente, ya sea </p><ul><li>a la página de inicio o</li><li>si tiene artículos en su cesta de la compra, a ella.</li></ul>'
text_after_login1 = '<p>Una vez que haya iniciado sesión, se le redirigirá automáticamente a la página de su pedido.</p>'
text_after_login2 = '<p>Una vez que haya iniciado sesión, se le redirigirá automáticamente a la evaluación del producto.</p>'

[index]
link_index = 'Inicio'
link_login = 'Iniciar sesión'
link_logoff = 'Cerrar sesión'
link_account = 'Mi cuenta'
link_cart = 'cesta de compra'
link_checkout = 'Caja'
text_show = 'Fabricante:'
text_noproduct = 'No se ha encontrado ningún artículo.'
text_shippingtime = 'Tiempo de entrega:'
new_customer = 'registrar'
link_imprint = 'Impressum'
link_contact = 'Contacto'
special_price = 'Precio especial'

#includes/modules/new_products
[new_products]
heading_text = 'Nuevos artículos'
heading_text_top = 'Nuestros mejores artículos'
text_shippingtime = 'Tiempo de entrega:'

#specials
[specials]
heading_text = 'Ofertas'
#BOF DokuMan - 2010-08-13 - show expiry date of special products in specials.php
text_expires_date = 'Oferta especial válida hasta:'
#EOF DokuMan - 2010-08-13 - show expiry date of special products in specials.php

#includes/modules/also_purchased
[also_purchased]
heading_text = 'Los clientes que compraron este artículo también pidieron los siguientes artículos:'

#includes/modules/graduated_price
[graduated_price]
heading_text = 'Precios escalonados'
pieces = 'por cada'
unit = 'unidades'

[product_info]
stock = 'Stock:'
weight = 'Peso:'
status = 'Estado:'
selled = 'Ya vendido:'
model = 'N°. de art.:'
print = 'Imprimir hoja de datos del artículo'
text_shippingtime = 'Tiempo de entrega:'
text_zoom = 'Ampliar imagen'
#BOF web28 - 2010-07-09 - TAB Headers
description = 'Detalles'
cross_selling = 'Recomendación'
also_purchased = 'Consejo del cliente'
products_reviews = 'Reseñas'
products_media = 'Descargas'
more_images = 'Más imágenes'
#EOF web28 - 2010-07-09 - TAB Headers
#BOF DokuMan - 2010-08-13 - show expiry date of special products
text_expires_date = 'Oferta especial válida hasta:'
#EOF DokuMan - 2010-08-13 - show expiry date of special products
#BOF - Tomcraft - 2011-02-24 - Get manufacturer name etc. for the product page
manufacturer = 'Fabricante:'
manufacturer_model = 'Número de artículo del fabricante:'
manufacturer_more_of = 'Más artículos de:'
ean = 'GTIN/EAN:'
#EOF - Tomcraft - 2011-02-24 - Get manufacturer name etc. for the product page
products_category = 'Otros productos de interés'
text_discount = 'Descuento'
fsk18_info = 'Este artículo sólo se puede comprar después de la verificación de edad.'
products_gift_forbidden = 'Los cupones no se pueden comprar como un invitado.'
checkout_express_activate = 'Activar Mi Compra Rápida »'
products_tags = 'Características'
heading_products_tags = 'Características del producto'

#includes/modules/products_media
[products_media]
file = 'Archivo'
size = 'Tamaño de archivo'

[boxes]
heading_add_a_quickie = 'Compra Rápida'
heading_admin = 'Admin Info'
heading_best_sellers = 'bestseller'
heading_cart = 'Cesta de compra'
heading_checkout = 'Caja'
heading_categories = 'Categorías'
heading_content = 'Más sobre..'
heading_currencies = 'Monedas'
heading_guestnewsletter = 'Suscripción al newsletter'
heading_infobox = 'Información'
heading_last_viewed = 'Lo último visto'
heading_languages = 'Idiomas'
heading_login = 'Bienvenida/o de nuevo!'
heading_manufacturers = 'Fabricante'
heading_manufacturers_info = 'Información sobre el fabricante'
heading_order_history = 'Vista general del pedido'
heading_reviews = 'Reseñas'
heading_search = 'Búsqueda'
heading_specials = 'Ofertas '
heading_whatsnew = 'Nuevos artículos'
text_more = '[Más]'
text_advanced_search = 'Búsqueda avanzada »'
text_discount = 'Descuento'
text_total = 'Total'
text_empty_cart = 'Su cesta de compra está vacía.'
text_full_cart = 'Su cesta de compra contiene los siguientes artículos:'
text_products = 'Artículos en la cesta de compra'
heading_customer_group_info = 'Grupo de clientes'
voucher_balance = 'Su saldo:'
box_send_to_friend = 'Enviar cupón'
voucher_redeemed = 'Cupón canjeado'
#cart_coupon = 'Cupón:' #Tomcraft - 2015-07-24 - Seems to be obsolete
#cart_coupon_info = '... más información' #Tomcraft - 2015-07-24 - Seems to be obsolete
text_quickie = 'Por favor, introduzca el número de artículo de nuestro catálogo.'
text_email = 'Dirección de correo electrónico'
text_in = 'registrar'
text_out = 'darse de baja'
text_password_forgotten = '¿Olvidó su contraseña?'
text_my_page = 'Más información en su página privada'
text_in_category = 'En '
text_watch_category = 'Distintivo:<br />'
text_pwd = 'Contraseña'
text_newsletter = 'El newsletter puede ser cancelado en cualquier momento aquí o en su cuenta de cliente.'
show_all = 'más'
special_price = 'Precio especial'
text_article = 'Artículo'

[print_product_info]
price = 'Precio:'
options = 'Opciones:'

[print_order]
invoice = 'Factura'
packingslip = 'nota de entrega'
title = 'Número de pedido:'
payment = 'Modalidad de pago:'
order = 'Número de pedido:'
date = 'Fecha:'
payment_address = 'Dirección de factura:'
shipping_address = 'Dirección de envío:'
heading_products = 'Artículo:'
head_units = 'unidades:'
head_products = 'Artículos:'
head_artnr = 'N°. de artículo:'
head_price = 'Precio total:'
head_single_price = 'Precio unitario:'
csID = 'Número de cliente:'
comments = 'Comentarios:'
#BOF - hendrik - 2011-05-14 - independent billingnumber and date
invoice_number = 'Número de factura:'
invoice_date = 'Fecha de factura:'
#EOF - hendrik - 2011-05-14 - independent billingnumber and date  
text_shippingtime = 'Tiempo de entrega:'
delivery_date = 'Fecha de entrega:'
head_tax = 'IVA'
vat_no = 'RFC:'
vatID = Su RFC:'

[product_navigator]
first = '[<<Primer]'
back = '[<volver]'
overview = '[Vista general]'
next = '[siguiente>]'
last = '[Último>>]'
# BOF - Tomcraft - 2010-05-02 - Show actual product count in product_navigator
#total = 'Artículo en esta categoría'
product = 'Artículo'
total = 'en esta categoría'
of = 'de'
# EOF - Tomcraft - 2010-05-02 - Show actual product count in product_navigator

[gift_cart]
title = 'Su cuenta de saldo'
cart_coupon = '<strong>Bono/Cúpon</strong> exitosamente <strong>canjeadot</strong>,'
cart_coupon_info = '<strong>Detalles</strong> sobre su bono/cupón'
text_info = 'Puede canjear su bono/cupón en la caja.'
voucher_balance = 'Su saldo actual de bono/cupón es:'
voucher_redeemed = 'bono/cupón canjeado'
text_send_to_friend = 'También puede regalar su saldo de bono/cupón en forma de bonos. ¿Cómo es eso? Haga clic aquí:'
box_send_to_friend = '<strong>Enviar cupón</strong>'
text_gift = '¿Ha recibido un cupón de regalo, y le gustaría canjearlo?'
text_code = 'Código de bono/cupón:'
create_account = '<strong>Atención:</strong> Si tiene un bono/cupón, puede canjearlo aquí, después de la creación de su cuenta como cliente.'
text_create_account = '» Crear una cuenta de cliente'


[gv_faq]
heading_gvfaq = 'Cupones - Preguntas y Respuestas'

[gv_redeem]
heading_gvredeem = 'Canjear cupón'
text_imformation = 'Preguntas y respuestas sobre cupones las encuentras bajo'
text_invalid_gv = 'El código del cupón que ha introducido es inválido o el cupón ya ha sido canjeado. Por favor comprueba sus datos. En caso de preguntas, póngase en contacto con nosotros'
text_valid_gva = 'Felicidades, ha canjeado exitosamente su cupón con un valor de'
text_valid_gvb = ' .'

[gv_send]
heading_gvsend = 'Enviar cupón'
heading_text = 'Por favor, introduzca aquí sus datos personales para el cupón. Si tiene alguna pregunta sobre la función de cupón, nuestras preguntas frecuentes le ayudarán:'
entry_name = 'Nombre del destinatario:'
entry_email = 'Dirección de correo electrónico del destinatario:'
entry_message = 'Su mensaje para el destinatario:'
entry_amount = 'Valor del cupón:'
text_success = 'Felicidades, ¡su cupón ha sido enviado!'

[downloads_module]
heading_download = 'Descargas'
table_heading_download_date = 'La descarga vence:'
table_heading_download_count = 'Número de descargas posibles:'
download_not_allowed = 'Sus descargas aún no han sido confirmadas.'
download_save_as = 'Para descargar, haga clic con el botón derecho en el enlace y seleccione "Guardar destino bajo".'
download_not_found = 'La descarga no se ha encontrado.'
text_confirm_download = 'Por favor, introduzca la dirección de correo electrónico con la que realizó el pedido.'
download_exceeded = 'Su descarga ha vencido o el enlace no es válido.'

[xsell]
heading_text = 'Para este producto recomendamos:'

[checkout_navigation]
title_shipping = 'Seleccionar modo de envío'
desc_shipping = 'Seleccione el modo envío deseado.'
title_payment = 'Seleccionar la modalidad de pago.'
desc_payment = 'Seleccione  la modalidad de pago deseada.'
title_confirmation = 'Confirmar'
desc_confirmation = 'Confirme su pedido'
title_success = 'Listo'
desc_success = 'Pedido realizado'

#Dokuman - 2009-08-21 - Added 'delete account' functionality for customers
[account_delete]
heading_delete_account = 'Borrar cuenta'
heading_account_deleted = 'Cuenta borrada'
delete_account = 'Haciendo clic en el botón "Continuar", usted borra su cuenta de cliente con nosotros. Como resultado, todos los datos personales vinculados a esta cuenta serán borrados o bloqueados irremediablemente de acuerdo con los requisitos legales.<br/><br/>¡Por favor, tenga en cuenta que después de borrar su cuenta por lo tanto ya no tendrá ningún acceso a sus pedidos realizados!<br/><br/>Por supuesto, puede crear una nueva cuenta en cualquier momento.<br/><br/>Por favor, introduzca su contraseña de cliente para completar el proceso.'
account_deleted = 'Su cuenta fue borrada exitosamente del sistema.'

#BOF - Tomcraft - 2009-10-03 - Paypal Express Modul
[checkout_paypal]
title_shipping_address = 'Dirección de envío'
title_payment_address = 'Dirección de factura'
title_shipping = 'Modo de envío'
text_shipping = 'Por favor, seleccione el modo de envío deseado para su pedido.'
title_agb = 'Términos y condiciones generales:'
text_accept_agb = 'Acepto sus términos y condiciones generales y de envío'
text_accept_adr = 'He controlado mi dirección de envío y de factura. Los datos son corectos.'
heading_checkout = 'Pago con PayPal'
text_products = 'Productos'
text_product = 'Artículos'
text_model = 'Número de artículo'
text_qty = 'Cantidad'
text_sprice = 'Precio'
text_fprice = 'Total'
text_payment_info = 'Información de pago'
title_comments = 'Agregue aquí sus comentarios a este pedido'
#EOF - Tomcraft - 2009-10-03 - Paypal Express Modul

[duty_info]
text_duty_info = 'En el caso de entregas a países no pertenecientes a la UE, es posible que el cliente tenga que pagar otros derechos de aduana, impuestos o tarifas, pero no al proveedor, sino a las autoridades aduaneras o fiscales responsables en el país. Se aconseja al cliente que solicite los detalles a las autoridades aduaneras o fiscales antes de realizar un pedido.'

[products_category]
heading_text = 'Otros productos interesantes:'

[wishlist]
heading_wishlist = 'lista de deseos'
text_full_wishlist = 'Su lista de deseos contiene los siguientes artículos:'
text_empty_wishlist = 'Todavía no tiene ningún artículo en su lista de deseos.'
text_empty_wishlist_box = 'Su lista de deseos está vacía.'

[account_checkout_express]
heading_checkout_express = 'Mi Compra Rápida'
heading_shipping = 'Modos de envío'
heading_shipping_address = 'Dirección de envío'
heading_payment = 'Modalidad de pago'
heading_payment_address = 'Dirección de factura'
text_choose_shipping_address = 'Por favor, seleccione una dirección de envío de su libreta de direcciones.'
text_choose_payment_address = 'Por favor, seleccione una dirección de factura de su libreta de direcciones.'
text_checkout_express_info = 'Seleccione la configuración de envío y pago preferida para su pedido de compra rápida.'

[filter]
text_filter_options = 'Opciones de filtro:'
text_filter_reset = 'Restablecer filtro'

<?php
/*******************************************************
* file: ups.php
* built by noRiddle 02-2016
* reworked for modified 2.0.X by noRiddle 02-2018
*******************************************************/

defined('_VALID_XTC') or die('Direct Access to this location is not allowed.');

define('MODULE_UPS_TEXT_DESCRIPTION', 'Export - UPS (komma-getrennt)');
define('MODULE_UPS_TEXT_TITLE', 'UPS - CSV');
define('MODULE_UPS_FILE_TITLE', '<hr noshade>Dateiname');
define('MODULE_UPS_FILE_DESC', 'Geben Sie einen Dateinamen ein. Falls die Exportadatei am Server gespeichert werden soll ist das Verzeichnis (Verzeichnis /export/)');
define('MODULE_UPS_STATUS_DESC', 'Modulstatus');
define('MODULE_UPS_STATUS_TITLE', 'Status');
define('MODULE_UPS_CURRENCY_TITLE', 'W&auml;hrung');
define('MODULE_UPS_CURRENCY_DESC', 'Welche W&auml;hrung soll exportiert werden?');
define('EXPORT_YES', 'Nur herunterladen');
define('EXPORT_NO', 'Nur auf dem Server speichern (/export/)');
define('CURRENCY', '<hr noshade><b>W&auml;hrung:</b>');
define('CURRENCY_DESC', 'W&auml;hrung in der Exportdatei');
define('EXPORT', 'Bitte den Sicherungsprozess AUF KEINEN FALL unterbrechen. Dieser kann einige Minuten in Anspruch nehmen.');
define('EXPORT_TYPE', '<hr noshade><b>Speicherart:</b>');
define('EXPORT_STATUS_TYPE', '<hr noshade><b>Kundengruppe:</b>');
define('EXPORT_STATUS', 'Bitte w&auml;hlen Sie die Kundengruppe, die Basis f&uuml;r den Exportierten Preis bildet. (Falls Sie keine Kundengruppenpreise haben, w&auml;hlen Sie <i>Gast</i>):</b>');
define('ORDERS_STATUS', '<hr noshade><b>Bestellstatus:</b>');
define('ORDERS_STATUS_DESC', 'Bitte wählen Sie den Bestellstatus der Bestellungen, die Sie exportieren wollen:');
define('ORDERS_STATUS_NEW_DESC', 'Bitte wählen Sie den Bestellstatus, der nach dem Export für die jeweilige Bestellung gelten soll:<br /><span style="color:#c00;">Bitte bedacht benutzen !</span><br />Wenn "Bestellstatus" oben auf "alle" steht wird kein Status geändert.');
define('DATE_FORMAT_EXPORT', '%d.%m.%Y'); // this is used for strftime()
define('GM_PAKET', '<hr noshade><b>Paketgr&ouml;&szlig;e:</b><br /><input type="text" name="gm_paket" value="NP" size="4" />');
// include needed functions

class ups {
    var $code, $title, $description, $enabled;

    function __construct() {
        global $order;

        $this->code = 'ups';
        $this->title = MODULE_UPS_TEXT_TITLE;
        $this->description = MODULE_UPS_TEXT_DESCRIPTION;
        $this->sort_order = MODULE_UPS_SORT_ORDER;
        $this->enabled = ((MODULE_UPS_STATUS == 'True') ? true : false);
        $this->exp_path = 'export/'; //must be without leading slash (see /admin/ah_export.php)
        $this->seperator = ',';
        $this->textsign = '"';
	}

    function process($file) {
        if($file != '') {
            @xtc_set_time_limit(0);
            $sep = $this->seperator; //set var for seperator
            $txts = $this->textsign;
            if ($_POST['oders_status'] != '') {
                $orders_query_where = " WHERE orders_status = '".$_POST['oders_status']."'";
            }
            $schema = '';
            //$schema = 'Kontaktperson;Unternehmen oder Name;Land;"Adresse, Zeile 1";"Adresse, Zeile 2";"Adresse, Zeile 3";Ort;Bundesstaat/Provinz/Bezirk;Postleitzahl;Telefonnummer;Durchwahl;"Markierung für ""Privatadresse""";E-mail;Verpackungstyp;Zollwert;Gewicht;Länge;Breite;Höhe;Maßeinheit:;Warenbeschreibung;Dokumente ohne Handelswert;Waren sind nicht zum freien Verkehr abgefertigt;Deklarierter Wert des Pakets;Service;Zustellbestätigung;Versenderfreigabe (Zustellung ohne Unterschrift);Rücksendung des Dokuments;Samstagszustellung;UPS kohlenstoffneutral;Großes Paket;Zusätzliche Handhabung;Referenz-Nr. 1;Referenz-Nr. 2;Referenz-Nr. 3;Adresse 1 für E-mail-Benachrichtigung;Versandbenachrichtigung per E-mail;E-Mail-Ausnahmebenachrichtigung;E-Mail-Zustellbenachrichtigung;Adresse 2 für E-mail-Benachrichtigung;Versandbenachrichtigung per E-mail;E-Mail-Ausnahmebenachrichtigung;E-Mail-Zustellbenachrichtigung;Adresse 3 für E-mail-Benachrichtigung;Versandbenachrichtigung per E-mail;E-Mail-Ausnahmebenachrichtigung;E-Mail-Zustellbenachrichtigung;Adresse 4 für E-mail-Benachrichtigung;Versandbenachrichtigung per E-mail;E-Mail-Ausnahmebenachrichtigung;E-Mail-Zustellbenachrichtigung;Adresse 5 für E-mail-Benachrichtigung;Versandbenachrichtigung per E-mail;E-Mail-Ausnahmebenachrichtigung;E-Mail-Zustellbenachrichtigung;E-Mail Nachricht;E-mail-Adresse für fehlgeschlagene Zustellung;UPS Premium Care;Ortsangabe ID;Medienformat;Sprache;Benachrichtigungsadresse;ADL Nachnahmebetrag;ADL Zustellung an Empfänger;ADL Versender Sektorformat;ADL Versender Sprache;ADL Versender Benachrichtigung' ."\n";

            $orders_query = "SELECT orders_id,
                                    customers_id,
                                    customers_telephone,
                                    customers_email_address,
                                    #delivery_name, #not needed
                                    delivery_firstname,
                                    delivery_lastname,
                                    delivery_company,
                                    delivery_street_address,
                                    delivery_suburb,
                                    delivery_city,
                                    delivery_postcode,
                                    delivery_state,
                                    #delivery_country, #not needed
                                    delivery_country_iso_code_2
                                    #payment_method, #not needed
                                    #comments, #not needed
                                    #date_purchased, #not needed
                                    #orders_status, #not needed
                                    #currency, #not needed
                                    #shipping_class #not needed
                               FROM orders "
                               . $orders_query_where;

            $customers_query = xtc_db_query($orders_query);
            while ($customers = xtc_db_fetch_array($customers_query)) {
                $comp_or_name = ($customers['delivery_company'] != '' && !is_numeric($customers['delivery_company'])) ? $customers['delivery_company'].' att. '.$customers['delivery_lastname'] : $customers['delivery_firstname'].' '.$customers['delivery_lastname']; //define var for company or name | !is_numeric since field is used for Packstation-Nr.
                $priv_or_not = ($customers['delivery_company'] != '' && !is_numeric($customers['delivery_company'])) ? '0' : '1'; //define var for private or company | !is_numeric since field is used for Packstation-Nr.
                
                //BOC get weight of order
                $total_prod_weight = 0;
                $prod_query = xtc_db_query("SELECT products_weight FROM ".TABLE_ORDERS_PRODUCTS." WHERE orders_id = ".$customers['orders_id']);
                while($prod_weight = xtc_db_fetch_array($prod_query)) {
                    $total_prod_weight += $prod_weight['products_weight'];
                }
                $tpw = number_format($total_prod_weight, 3, ',', '');
                //EOC get weight of order

                $schema_entry = 
                    $customers['delivery_firstname'].' '.$customers['delivery_lastname'].$sep       //Kontaktperson
                   .$comp_or_name.$sep                                                              //Unternehmen oder Name
                   .$customers['delivery_country_iso_code_2'].$sep                                  //Land
                   .$customers['delivery_street_address'].$sep                                      //Adresse, Zeile 1
                   .$sep                                                                            //Adresse, Zeile 2
                   .$sep                                                                            //Adresse, Zeile 3
                   .$customers['delivery_city'].$sep                                                //Ort
                   .($customers['delivery_state'] != '' ? $customers['delivery_state'] : '').$sep   //Bundesstaat/Provinz/Bezirk
                   .$customers['delivery_postcode'].$sep                                            //Postleitzahl
                   .$customers['customers_telephone'].$sep                                          //Telefonnummer
                   .$sep                                                                            //Durchwahl
                   .$priv_or_not.$sep                                                               //Markierung für "Privatadresse" | 0 geschäftl., 1 privat
                   .$sep                                                                            //E-mail
                   .'2'.$sep                                                                        //Verpackungstyp | 2 = Andere Verpackung | 4 = UPS PAK
                   .$sep                                                                            //Zollwert
                   .$txts.$tpw.$txts.$sep                                                           //Gewicht
                   .$sep                                                                            //Länge
                   .$sep                                                                            //Breite
                   .$sep                                                                            //Höhe
                   .$sep                                                                            //Maßeinheit:
                   .$sep                                                                            //Warenbeschreibung
                   .$sep                                                                            //Dokumente ohne Handelswert
                   .$sep                                                                            //Waren sind nicht zum freien Verkehr abgefertigt
                   .$sep                                                                            //Deklarierter Wert des Pakets
                   .'11'.$sep                                                                       //Service | 11 = UPS Standard
                   .$sep                                                                            //Zustellbestätigung | U = Unterschrift erforderlich
                   .$sep                                                                            //Versenderfreigabe (Zustellung ohne Unterschrift)
                   .$sep                                                                            //Rücksendung des Dokuments
                   .$sep                                                                            //Samstagszustellung
                   .$sep                                                                            //UPS kohlenstoffneutral
                   .$sep                                                                            //Großes Paket
                   .$sep                                                                            //Zusätzliche Handhabung
                   .$sep                                                                            //Referenz-Nr. 1
                   .$sep                                                                            //Referenz-Nr. 2
                   .$sep                                                                            //Referenz-Nr. 3
                   .$customers['customers_email_address'].$sep                                      //Adresse 1 für E-mail-Benachrichtigung
                   .'1'.$sep                                                                        //Versandbenachrichtigung per E-mail
                   .$sep                                                                            //E-Mail-Ausnahmebenachrichtigung
                   .$sep                                                                            //E-Mail-Zustellbenachrichtigung
                   .$sep                                                                            //Adresse 2 für E-mail-Benachrichtigung
                   .$sep                                                                            //Versandbenachrichtigung per E-mail
                   .$sep                                                                            //E-Mail-Ausnahmebenachrichtigung
                   .$sep                                                                            //E-Mail-Zustellbenachrichtigung
                   .$sep                                                                            //Adresse 3 für E-mail-Benachrichtigung
                   .$sep                                                                            //Versandbenachrichtigung per E-mail
                   .$sep                                                                            //E-Mail-Ausnahmebenachrichtigung
                   .$sep                                                                            //E-Mail-Zustellbenachrichtigung
                   .$sep                                                                            //Adresse 4 für E-mail-Benachrichtigung
                   .$sep                                                                            //Versandbenachrichtigung per E-mail
                   .$sep                                                                            //E-Mail-Ausnahmebenachrichtigung
                   .$sep                                                                            //E-Mail-Zustellbenachrichtigung
                   .$sep                                                                            //Adresse 5 für E-mail-Benachrichtigung
                   .$sep                                                                            //Versandbenachrichtigung per E-mail
                   .$sep                                                                            //E-Mail-Ausnahmebenachrichtigung
                   .$sep                                                                            //E-Mail-Zustellbenachrichtigung
                   .$sep                                                                            //E-Mail Nachricht
                   .$sep                                                                            //E-mail-Adresse für fehlgeschlagene Zustellung
                   .$sep                                                                            //UPS Premium Care | nicht in Hilfe aufgeführt (https://www.ups.com/content/de/de/shipping/create/shipping/create/batch_file.html)
                   .$sep                                                                            //Ortsangabe ID
                   .$sep                                                                            //Medienformat
                   .$sep                                                                            //Sprache
                   .$sep                                                                            //Benachrichtigungsadresse
                   .$sep                                                                            //ADL Nachnahmebetrag
                   .$sep                                                                            //ADL Zustellung an Empfänger
                   .$sep                                                                            //ADL Versender Sektorformat
                   .$sep                                                                            //ADL Versender Sprache
                   .$sep                                                                            //ADL Versender Benachrichtigung
                   ."\n";
                   
                    $schema .= $schema_entry;
            }

            if(empty($schema)) {
                $schema = ' ';
            }

            // create File
            $fp = fopen(DIR_FS_DOCUMENT_ROOT . 'export/' . $file, "w+");
            fputs($fp, $schema);
            fclose($fp);

            switch($_POST['export']) {
				case 'yes':
					// send File to Browser
					$extension = substr($file, -3);
					$fp = fopen(DIR_FS_DOCUMENT_ROOT . $this->exp_path . $file, "rb");
					$buffer = fread($fp, filesize(DIR_FS_DOCUMENT_ROOT . $this->exp_path . $file));
					fclose($fp);
					header('Content-type: application/x-octet-stream');
					header('Content-disposition: attachment; filename=' . $file);
					echo $buffer;
                    
                    unlink(DIR_FS_DOCUMENT_ROOT . $this->exp_path . $file);
                    
					if ($_POST['oders_status_new'] != '' AND $_POST['oders_status'] != '') {
						$ord_upd_qu = "UPDATE ".TABLE_ORDERS." SET orders_status = '".(int)$_POST['oders_status_new']."' WHERE orders_status = ".(int)$_POST['oders_status'];
                        $ord_stat_hist_arr = array ('orders_id' => $customers['orders_id'],
                                                    'orders_status_id' => (int)$_POST['oders_status_new'],
                                                    'date_added' => 'now()',
                                                    'customer_notified' => '0',
                                                    'comments' => ''
                                                   );
  
						xtc_db_query($ord_upd_qu);
                        xtc_db_perform(TABLE_ORDERS_STATUS_HISTORY, $ord_stat_hist_arr);
					}
					exit;
                break;
                case 'no':
                    if ($_POST['oders_status_new'] != '' AND $_POST['oders_status'] != '') {
						$ord_upd_qu = "UPDATE ".TABLE_ORDERS." SET orders_status = '".(int)$_POST['oders_status_new']."' WHERE orders_status = ".(int)$_POST['oders_status'];
                        $ord_stat_hist_arr = array ('orders_id' => $customers['orders_id'],
                                                    'orders_status_id' => (int)$_POST['oders_status_new'],
                                                    'date_added' => 'now()',
                                                    'customer_notified' => '0',
                                                    'comments' => ''
                                                   );
  
						xtc_db_query($ord_upd_qu);
                        xtc_db_perform(TABLE_ORDERS_STATUS_HISTORY, $ord_stat_hist_arr);
					}
                break;
			}
		}
	}

	function display() {

		$customers_statuses_array = xtc_get_customers_statuses();

		// build Currency Select

		$orders_status_array = array(array('id' => '', 'text' => ($_SESSION['languages_id'] == '2' ? 'alle' : 'all')));
		$orders_status_query = xtc_db_query("SELECT orders_status_name, orders_status_id FROM ".TABLE_ORDERS_STATUS." WHERE language_id = ".(int)$_SESSION['languages_id']." ORDER BY sort_order"); //changed ORDER BY; noRiddle
		while ($orders_status = xtc_db_fetch_array($orders_status_query)) {
			$orders_status_array[] = array('id' => $orders_status['orders_status_id'],
				                           'text' => $orders_status['orders_status_name'],
			                              );
		}
		$orders_status_new_array = $orders_status_array;
		$orders_status_new_array[0]['text'] = ($_SESSION['languages_id'] == '2' ? 'nicht ändern' : 'don\'t change');
		return array (
			'text' => ORDERS_STATUS . '<br />' .
			ORDERS_STATUS_DESC . '<br />' .
			xtc_draw_pull_down_menu('oders_status',	$orders_status_array) . '<br /><br />' .
            ORDERS_STATUS_NEW_DESC . '<br />' .
            xtc_draw_pull_down_menu('oders_status_new', $orders_status_new_array) . '<br />' .
            EXPORT_TYPE . '<br />' .
            EXPORT . '<br />' .
            xtc_draw_radio_field('export', 'no', false) . EXPORT_NO . '<br />' .
            xtc_draw_radio_field('export', 'yes', true) . EXPORT_YES . '<br />' .
            '<br /><div align="center">' . xtc_button(BUTTON_EXPORT) .
            xtc_button_link(BUTTON_CANCEL, xtc_href_link('ah_module_export.php', 'set=' . $_GET['set'] . '&module=ups')) .'</div>'
        );

	}

	function check() {
		if (!isset ($this->_check)) {
			$check_query = xtc_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_UPS_STATUS'");
			$this->_check = xtc_db_num_rows($check_query);
		}
		return $this->_check;
	}

	function install() {
		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, set_function, date_added) values ('MODULE_UPS_FILE', 'ups.csv',  '6', '1', '', now())");
		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, set_function, date_added) values ('MODULE_UPS_STATUS', 'True',  '6', '1', 'xtc_cfg_select_option(array(\'True\', \'False\'), ', now())");
	}

	function remove() {
		xtc_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
	}

	function keys() {
		return array (
			'MODULE_UPS_STATUS',
			'MODULE_UPS_FILE'
		);
	}

}
?>
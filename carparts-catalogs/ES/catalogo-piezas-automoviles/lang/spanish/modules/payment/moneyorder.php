<?php
/* -----------------------------------------------------------------------------------------
   $Id: moneyorder.php 998 2005-07-07 14:18:20Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(moneyorder.php,v 1.8 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (moneyorder.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_MONEYORDER_TEXT_TITLE', 'Prepago/Transferencia bancaria');
define('MODULE_PAYMENT_MONEYORDER_TEXT_DESCRIPTION', 'Detalles bancarios:<br />' . (defined('MODULE_PAYMENT_MONEYORDER_PAYTO') ? nl2br(MODULE_PAYMENT_MONEYORDER_PAYTO) : '') . '<br /><br />Titular de la cuenta:<br />' . nl2br(STORE_OWNER) . '<br />');
define('MODULE_PAYMENT_MONEYORDER_TEXT_EMAIL_FOOTER', "Detalles bancarios: ". (defined('MODULE_PAYMENT_MONEYORDER_PAYTO') ? MODULE_PAYMENT_MONEYORDER_PAYTO : '') . "\nTitular de la cuenta:\n" . STORE_OWNER . "\n");
  if (MODULE_PAYMENT_MONEYORDER_SUCCESS == 'True') {
define('MODULE_PAYMENT_MONEYORDER_TEXT_INFO', 'Enviamos su pedido después de recibir el pago. Recibirá los datos de la cuenta en el último paso del pedido.');
  } else {
define('MODULE_PAYMENT_MONEYORDER_TEXT_INFO', 'Enviamos su pedido después de recibir el pago. Recibirá los datos de la cuenta por correo electrónico después de la aceptación de su pedido.');
  }
define('MODULE_PAYMENT_MONEYORDER_STATUS_TITLE' , 'Activar módulo de Prepago/Transferencia bancaria');
define('MODULE_PAYMENT_MONEYORDER_STATUS_DESC' , '¿Desea aceptar pagos a través de Prepago/Transferencia bancaria?');
define('MODULE_PAYMENT_MONEYORDER_ALLOWED_TITLE' , 'Zonas permititdas');
define('MODULE_PAYMENT_MONEYORDER_ALLOWED_DESC' , 'Introduzca <b>individualmente</b>  las zonas permitidas para este módulo. (por ejemplo AT,DE (si está vacío, se permiten todas las zonas))');
define('MODULE_PAYMENT_MONEYORDER_PAYTO_TITLE' , 'A pagar a:');
define('MODULE_PAYMENT_MONEYORDER_PAYTO_DESC' , '¿A quién se harán los pagos?');
define('MODULE_PAYMENT_MONEYORDER_SORT_ORDER_TITLE' , 'Orden de visualización');
define('MODULE_PAYMENT_MONEYORDER_SORT_ORDER_DESC' , 'Orden de la visualización. Primero se muestra el dígito más pequeño.');
define('MODULE_PAYMENT_MONEYORDER_ZONE_TITLE' , 'Zona de pago');
define('MODULE_PAYMENT_MONEYORDER_ZONE_DESC' , 'Si se selecciona una zona, el método de pago se aplica sólo a esa zona.');
define('MODULE_PAYMENT_MONEYORDER_ORDER_STATUS_ID_TITLE' , 'Determinar estado del pedido');
define('MODULE_PAYMENT_MONEYORDER_ORDER_STATUS_ID_DESC' , 'Cambiar los pedidos realizados con este módulo a este estado');
define('MODULE_PAYMENT_MONEYORDER_SUCCESS_TITLE' , 'Mostrar datos bancarios');
define('MODULE_PAYMENT_MONEYORDER_SUCCESS_DESC' , '¿Deben mostrarse los datos bancarios en la página de éxito?');
?>

<?php
if(!isset($_SESSION['customer_id'])) xtc_redirect(xtc_href_link(FILENAME_DEFAULT)); //redirect to start page if not logged in, noRiddle

// Include soap request class
include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'requestOem.php');
// Include view class
include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'render'.DIRECTORY_SEPARATOR.'filter'.DIRECTORY_SEPARATOR.'default.php');
include('nr_catalogue_files/extender.php');

class GuayaquilExtender3 extends CommonExtender
{
	function FormatLink($type, $dataItem, $catalog, $renderer)
	{
        //BOC set link to content 1006 unit.php, noRiddle
        //$link = 'unit.php?c=' . $catalog . '&vid=' . $renderer->vehicle_id . '&uid=' . $dataItem['unitid'] .  '&cid=' . $renderer->categoryid . '&ssd=' . $dataItem['ssd'];
        $link = xtc_href_link(FILENAME_CONTENT, 'coID=1006').'?c=' . $catalog . '&vid=' . $renderer->vehicle_id . '&uid=' . $dataItem['unitid'] .  '&cid=' . $renderer->categoryid . '&ssd=' . $dataItem['ssd'];
        //EOC set link to content 1006 unit.php, noRiddle

        return $link;
	}
}

// Create request object
$request = new GuayaquilRequestOEM($_GET['c'], $_GET['ssd'], Config::$catalog_data);
if (Config::$useLoginAuthorizationMethod) {
    $request->setUserAuthorizationMethod(Config::$userLogin, Config::$userKey);
}

// Append commands to request
$request->appendGetFilterByUnit($_GET['f'], $_GET['vid'], $_GET['uid']);
$request->appendGetUnitInfo($_GET['uid']);

// Execute request
$data = $request->query();

// Check errors
if ($request->error != '')
{
    echo $request->error;
}
else
{
    $filter_data = $data[0];
    $unit = $data[1]->row;

    //echo '<h1>'.CommonExtender::FormatLocalizedString('UnitName', (string)$unit['name']).'</h1>';
    echo '<h1>'
         //.'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.Config::$nr_icon_arr[$_GET['c']].'" alt ="" />&nbsp;'
         .'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['brand_icon'].'" alt ="'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].'" />&nbsp;'
         .CommonExtender::FormatLocalizedString('UnitName', (string)$unit['name'])
         .'</h1>';
         
    //BOC extend toolbar, noRiddle
    echo '<div id="main-top cf">';
    echo '<div class="log-and-butt cf">';
    //$html .= '<div class="brand-logo"><img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/'.Config::$nr_icon_arr[$_GET['c']].'" alt ="" /></div>';
    echo '<div class="top-buttons cf">';
    
    /*$html .= '<a href="/katalog_shop_test/"><span class="btn button-catalogue"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_brand_choice').'</span></a>&nbsp;'
            .'<a href="'.xtc_href_link(FILENAME_CATALOGUE_SHOPPING_CART).'"><span class="btn button-catalogue"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_shopp_cart').'</span></a>';*/
    echo '</div>';
    echo '</div>'; //end log-and-butt
    echo '</div>'; //end main-top
    //EOC extend toolbar, noRiddle

    $renderer = new GuayaquilFilter(new GuayaquilExtender3());
    $renderer->vehicle_id = $_GET['vid'];
    $renderer->categoryid = $_GET['cid'];
    $renderer->ssd = $_GET['ssd'];
    echo $renderer->Draw($_GET['c'], $filter_data, $_GET['ssd'], $unit);

}
?>

<script type="text/javascript">
    function ProcessFilters(skip)
    {
        //var url = '<?php echo 'vehicle.php?&c='.$_GET['c'].'&vid='.$_GET['vid'].'&cid='.$_GET['cid'].'&ssd=$'?>';
        var url = '<?php echo 'shop_content.php?coID=1004&c='.$_GET['c'].'&vid='.$_GET['vid'].'&cid='.$_GET['cid'].'&ssd=$'?>';
        //var url = '<?php echo xtc_href_link(FILENAME_CONTENT, 'coID=1004&'.xtc_get_all_get_params(array('coID', 'uid', 'ssd', 'f')).'&ssd=$'); ?>'; //noRiddle
        var ssd = '<?php echo $_GET['ssd']?>';
        var col = jQuery('#guayaquilFilterForm .g_filter');
        var hasErrors = false;
        col.each(function(){
            var name = this.nodeName;
            var ssdmod = null;
            if (name == 'SELECT')
                ssdmod = this.value;
            else if (name == 'INPUT' && jQuery(this).attr('type') == 'text' && this.value.length > 0)
            {
                var s = jQuery(this).attr('ssd');
                if (s != null && s.length > 0)
                {
                    var expr = new RegExp(jQuery(this).attr('regexp'), 'i');
                    if ((expr.test(value)))
                    {
                        ssdmod = s.replace('\$', this.value);
                        jQuery(this).removeClass('g_error');
                    }
                    else
                    {
                        jQuery(this).addClass('g_error');
                        hasErrors = true;
                    }
                }
            }
            else if (name == 'INPUT' && jQuery(this).attr('type') == 'radio' && this.checked)
                var ssdmod = jQuery(this).attr('ssd');

            if (ssdmod != null && ssdmod.length > 0)
                ssd += ssdmod;
        })

        if (!hasErrors)
            window.location = url.replace('\$', ssd);
    }
</script>
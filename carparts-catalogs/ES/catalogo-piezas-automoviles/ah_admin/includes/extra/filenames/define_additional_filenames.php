<?php
/*******************************************************
* file: define_additional_filenames.php
* use: define additional filenames
* (c) noRiddle 01-2017
*******************************************************/
define('FILENAME_MASTER_ADMIN', 'master_admin.php');
define('FILENAME_IMAGESLIDERS', 'imagesliders.php');
define('FILENAME_ORDER_STATUS_COMMENTS', 'order_status_comments.php');
define('FILENAME_GENERATE_SHOPPING_CART_LINK', 'generate_shoppingcart_link.php');
define('FILENAME_ORDER_MAIL_ADVERTISE', 'order_mail_advertise.php');

//BOC PDFBill NEXT
define('FILENAME_PDF_BILL_NR','bill_nr.php');
define('FILENAME_PRINT_ORDER_PDF','print_order_pdf.php');
define('FILENAME_PRINT_PACKINGSLIP_PDF','print_packingslip_pdf.php');
define('FILENAME_PDF_BILL_DEL','pdfbill_del.php');
//EOC PDFBill NEXT
?>
<?php
/* --------------------------------------------------------------
   $Id: specials.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(specials.php,v 1.10 2002/01/31); www.oscommerce.com 
   (c) 2003	 nextcommerce (specials.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('SPECIALS_TITLE', 'Offre spéciale');

define('TEXT_SPECIALS_PRODUCT', 'Aticle:');
define('TEXT_SPECIALS_SPECIAL_PRICE', 'Prix proposé:');
define('TEXT_SPECIALS_SPECIAL_QUANTITY', 'Nombre:');
define('TEXT_SPECIALS_START_DATE', 'Valable à partir de: <small>(AAAA-MM-JJ)</small>');
define('TEXT_SPECIALS_EXPIRES_DATE', 'Valable jusqu\'à: <small>(AAAA-MM-JJ)</small>');

define('TEXT_INFO_DATE_ADDED', 'Ajouté le:');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification:');
define('TEXT_INFO_NEW_PRICE', 'Nouveau prix:');
define('TEXT_INFO_ORIGINAL_PRICE', 'Ancien prix:');
define('TEXT_INFO_PERCENTAGE', 'Pourcentage:');
define('TEXT_INFO_START_DATE', 'Valable à partir de:');
define('TEXT_INFO_EXPIRES_DATE', 'Valable jusqu\'à:');

define('TEXT_INFO_HEADING_DELETE_SPECIALS', 'Supprimer l\'offre spéciale');
define('TEXT_INFO_DELETE_INTRO', 'Vous en êtes sûr de vouloir supprimer l\'offre spéciale?');

define('TEXT_SPECIALS_NO_PID', 'L\'article doit d\'abord être sauvegarder. Sinon l\'offre spéciale ne peut pas être aménagé correctement!');

define('TEXT_CATSPECIALS_START_DATE_TT', 'Indiquez la date à partir de laquelle le le prix proposé est valable.<br>.');
define('TEXT_CATSPECIALS_EXPIRES_DATE_TT', 'Laissez vide le champ <strong>valable jusqu\'à</strong> si le prix proposé doit rester illimité.<br>');
define('TEXT_CATSPECIALS_SPECIAL_QUANTITY_TT', 'Vous pouvez entrer le nombre des pièces valables pour l’offre dans le champ <strong>nombre</strong>.<br> Sous "Configuration" -> " Option gestion de stock" -> "Vérifier les offres spéciales" vous pouvez décider si le stock des offres spéciales doit être vérifié.');
define('TEXT_CATSPECIALS_SPECIAL_PRICE_TT', 'Dans le champs prix de l‘offre vous pouvez également entrer des valeurs en pourcentage, p.ex: <strong>20%</strong><br> Si vous entrez un nouveau prix, les décimales doivent être séparés par un \'.\', p.ex : <strong>49.99</strong>' );
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_coupon.php 2096 2011-08-15 15:42:57Z Tomcraft1980 $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(t_coupon.php,v 1.1.2.2 2003/05/15); www.oscommerce.com
   (c) 2006 XT-Commerce (ot_coupon.php 899 2005-04-29)

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_COUPON_TITLE', 'Coupons de réduction');
define('MODULE_ORDER_TOTAL_COUPON_HEADER', 'Bon d\'achat / Coupons de réduction');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION', 'Coupons de réduction');
define('SHIPPING_NOT_INCLUDED', ' [Livraison pas comprise]');
define('TAX_NOT_INCLUDED', ' [TVA pas comprise]');
define('MODULE_ORDER_TOTAL_COUPON_USER_PROMPT', '');
define('ERROR_NO_INVALID_REDEEM_COUPON', 'Code invalide');
  //BOF - DokuMan - 2010-08-31 - constants already defined in german.php
  //define('ERROR_INVALID_STARTDATE_COUPON', 'Dieser Gutschein ist noch nicht verf&uuml;gbar');
  //define('ERROR_INVALID_FINISDATE_COUPON', 'Dieser Gutschein ist nicht mehr g&uuml;ltig');
  //define('ERROR_INVALID_USES_COUPON', 'Dieser Gutschein kann nur ');
  //define('TIMES', ' mal benutzt werden.');
  //define('ERROR_INVALID_USES_USER_COUPON', 'Die maximale Nutzung dieses Gutscheines wurde erreicht.');
  //define('REDEEMED_COUPON', 'ein Gutschein &uuml;ber ');
  //EOF - DokuMan - 2010-08-31 - constants already defined in german.php
define('REDEEMED_MIN_ORDER', 'pour produits au dessus de ');
define('REDEEMED_RESTRICTIONS', ' [article / limitations catégorie]');
define('TEXT_ENTER_COUPON_CODE', 'Veuillez saisir le code du bon d\'achat   ');
  
define('MODULE_ORDER_TOTAL_COUPON_STATUS_TITLE', 'Afficher valeur');
define('MODULE_ORDER_TOTAL_COUPON_STATUS_DESC', 'Souhaitez-vous afficher la valeur du coupon de réduction ?');
define('MODULE_ORDER_TOTAL_COUPON_SORT_ORDER_TITLE', 'Ordre de triage');
define('MODULE_ORDER_TOTAL_COUPON_SORT_ORDER_DESC', 'Ordre de l\'affichage');
define('MODULE_ORDER_TOTAL_COUPON_INC_SHIPPING_TITLE', 'Frais de port inclus');
define('MODULE_ORDER_TOTAL_COUPON_INC_SHIPPING_DESC', 'Facturer les frais de livraison à la valeur de l\'achat');
define('MODULE_ORDER_TOTAL_COUPON_INC_TAX_TITLE', 'TVA incluse');
define('MODULE_ORDER_TOTAL_COUPON_INC_TAX_DESC', 'Facturer la TVA à la valeur de l\'achat');
define('MODULE_ORDER_TOTAL_COUPON_CALC_TAX_TITLE', 'Recalculer TVA');
define('MODULE_ORDER_TOTAL_COUPON_CALC_TAX_DESC', 'Recalculer TVA');
define('MODULE_ORDER_TOTAL_COUPON_TAX_CLASS_TITLE', 'Taux de TVA');
define('MODULE_ORDER_TOTAL_COUPON_TAX_CLASS_DESC', 'Utiliser le taux de TVA suivant si vous souhaitez utiliser le coupon de réduction comme crédit.');
  //BOF - web28 - 2010-06-20 - no discount for special offers
define('MODULE_ORDER_TOTAL_COUPON_SPECIAL_PRICES_TITLE', 'Réduction sur des offres spéciales');
define('MODULE_ORDER_TOTAL_COUPON_SPECIAL_PRICES_DESC', 'Permettre réductions sur des offres spéciales');
  //EOF - web28 - 2010-06-20 - no discount for special offers
?>

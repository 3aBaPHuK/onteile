<?php
/* *******************************************************************************
* ah_module_export.php, noRiddle 07-2017
**********************************************************************************/

define('HEADING_TITLE_MODULES_AH_EXPORT', 'Exportación de clientes / de pedidos');

define('TABLE_HEADING_AH_MODULES', 'Módulo');
define('TABLE_HEADING_AH_SORT_ORDER', 'Orden');
define('TABLE_HEADING_AH_STATUS', 'Estado');
define('TABLE_HEADING_AH_ACTION', 'Acción');

define('TEXT_MODULE_AH_DIRECTORY', 'Directorio del módulo:');
define('TABLE_HEADING_AH_FILENAME', 'Nombre del módulo (para el uso interno)');
define('ERROR_AH_EXPORT_FOLDER_NOT_WRITEABLE', '¡%s directorio no escribible!');
define('TEXT_AH_MODULE_INFO', '¡Por favor, compruebe los módulos con el proveedor correspondiente para la versión más actual!');

define('TABLE_HEADING_AH_MODULES_INSTALLED', 'Se han instalado los siguientes módulos');
define('TABLE_HEADING_AH_MODULES_NOT_INSTALLED', 'Los siguientes módulos todavía están disponibles');
?>

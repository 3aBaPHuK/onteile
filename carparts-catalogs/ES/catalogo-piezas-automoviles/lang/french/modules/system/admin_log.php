<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_ADMIN_LOG_TEXT_TITLE', 'Administrator Log');
define('MODULE_ADMIN_LOG_TEXT_DESCRIPTION', 'Des modifications seront enregistrés par Administrator Log.
<ul>
  <li>Commandes</li>
  <li>Catégories</li>
  <li>Produits</li>
  <li>Gestionnaire contenu</li>
  <li>Modes d\'expédition</li>
  <li>Modes de paiement</li>
  <li>Résumé</li>
  <li>Modules de système</li>
  <li>Modules d\'export</li>
  <li>Configuration</li>
</ul>
  ');
define('MODULE_ADMIN_LOG_STATUS_TITLE', 'Activer le module?');
define('MODULE_ADMIN_LOG_STATUS_DESC', 'Activer Administrator Log');

define('MODULE_ADMIN_LOG_DISPLAY_TITLE', 'Afficher log?');
define('MODULE_ADMIN_LOG_DISPLAY_DESC', 'Afficher Administrator Log au footer');

define('MODULE_ADMIN_LOG_SHOW_DETAILS_TITLE', 'Afficher les détails?');
define('MODULE_ADMIN_LOG_SHOW_DETAILS_DESC', 'Afficher les détails de changement comme Array');

define('MODULE_ADMIN_LOG_SHOW_DETAILS_FULL_TITLE', 'Afficher les détails complets?');
define('MODULE_ADMIN_LOG_SHOW_DETAILS_FULL_DESC', 'Afficher les détails complets comme Array');

?>

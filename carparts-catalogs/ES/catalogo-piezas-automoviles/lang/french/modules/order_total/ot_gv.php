<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_gv.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ot_gv.php,v 1.1.2.1 2003/05/15); www.oscommerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_GV_TITLE', 'Bons d\'achat');
define('MODULE_ORDER_TOTAL_GV_HEADER', 'Bons d\'achat');
define('MODULE_ORDER_TOTAL_GV_DESCRIPTION', 'Bons d\'achat');
define('SHIPPING_NOT_INCLUDED', ' [Livraison pas incluse]');
define('TAX_NOT_INCLUDED', ' [TVA pas incluse]');
define('MODULE_ORDER_TOTAL_GV_USER_PROMPT', 'Sélectionner si vous souhaitez utiliser votre crédit');
define('TEXT_ENTER_GV_CODE', 'Veuillez saisir votre code de bon d\'achat ici   ');

define('MODULE_ORDER_TOTAL_GV_STATUS_TITLE', 'Afficher valeur');
define('MODULE_ORDER_TOTAL_GV_STATUS_DESC', 'Souhaitez-vous afficher la valeur de votre bon-cadeau ?');
define('MODULE_ORDER_TOTAL_GV_SORT_ORDER_TITLE', 'Ordre du triage');
define('MODULE_ORDER_TOTAL_GV_SORT_ORDER_DESC', 'Ordre de l\'affichage');
define('MODULE_ORDER_TOTAL_GV_QUEUE_TITLE', 'Liste de validation');
define('MODULE_ORDER_TOTAL_GV_QUEUE_DESC', 'Souhaitez-vous mettre les bons-cadeau d\'abord dans la liste de validation ?');
define('MODULE_ORDER_TOTAL_GV_INC_SHIPPING_TITLE', 'Frais de port inclus');
define('MODULE_ORDER_TOTAL_GV_INC_SHIPPING_DESC', 'Facturer frais de port à la valeur de l\'achat');
define('MODULE_ORDER_TOTAL_GV_INC_TAX_TITLE', 'TVA incluse');
define('MODULE_ORDER_TOTAL_GV_INC_TAX_DESC', 'Facturer TVA à la valeur de l\'achat');
define('MODULE_ORDER_TOTAL_GV_CALC_TAX_TITLE', 'Recalculer TVA');
define('MODULE_ORDER_TOTAL_GV_CALC_TAX_DESC', 'Recalculer TVA');
define('MODULE_ORDER_TOTAL_GV_TAX_CLASS_TITLE', 'Taux de TVA');
define('MODULE_ORDER_TOTAL_GV_TAX_CLASS_DESC', 'Utiliser le taux de TVA suivant si vous souhaitez utiliser le bon d\'achat comme crédit.');
define('MODULE_ORDER_TOTAL_GV_CREDIT_TAX_TITLE', 'Crédit contient la TVA');
define('MODULE_ORDER_TOTAL_GV_CREDIT_TAX_DESC', 'Facturer TVA à la valeur du bon d\'achat');
?>

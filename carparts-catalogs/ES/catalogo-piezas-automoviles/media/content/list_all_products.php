<?php
/*****************************************************************************
* read all products_manufacturers_model and products_name into a simple list *
* build a link to each product | (c) noRiddle 11-2015                        *
*****************************************************************************/

//BOC config
$limit_per_page = 90; //how many products on one page
$prod_list_cols = 3; //how many columns to display on one page
$page_links_to_display = MAX_DISPLAY_PAGE_LINKS; //how many page links to display
//EOC config

$lisprd_col_pcs = ceil($limit_per_page/$prod_list_cols); //products in one column

$prod_sql = "SELECT p.products_id,
                    p.products_manufacturers_model,
                    pd.products_name
               FROM ". TABLE_PRODUCTS ." p
          LEFT JOIN ". TABLE_PRODUCTS_DESCRIPTION ." pd
                 ON pd.products_id = p.products_id
                AND pd.language_id = ". (int)$_SESSION['languages_id'] ."
              WHERE p.products_status = '1'
                AND p.gm_price_status != '2'
           ORDER BY p.products_model"; //order by products_manufacturers_model is to slow since we have no index on this column
               
$prod_split = new splitPageResults($prod_sql, (isset($_GET['page']) ? (int)$_GET['page'] : 1), $limit_per_page, 'p.products_manufacturers_model');
$prod_query = xtc_db_query($prod_split->sql_query);

$prod_dip_lks = $prod_split->display_links($page_links_to_display, xtc_get_all_get_params(array ('page')));

if (USE_PAGINATION_LIST == 'false') {
    $list_pag_string = '<div class="smallText" style="clear:both;">
                            <div style="float:left;">'.$prod_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS).'</div> 
                            <div align="right">'.TEXT_RESULT_PAGE.' '.$prod_split->display_links($page_links_to_display, xtc_get_all_get_params(array ('page'))).'</div> 
                        </div>';
} else {   
    $list_pag_string = '<div class="pagination_bar cf">
                            <div class="pagination_text">'.$prod_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS).'</div>
                            <div class="pagination_list">
                                <ul>  
                                    <li class="plain">'.TEXT_RESULT_PAGE.'</li>'
                                    .(isset($prod_dip_lks['previous']) ? '<li>'.$prod_dip_lks['previous'].'</li>' : '')
                                    .(isset($prod_dip_lks['previouspages']) ? '<li>'.$prod_dip_lks['previouspages'].'</li>' : '');
                                    foreach($prod_dip_lks['pages'] as $key => $pages) {
                                        $list_pag_string .= '<li class="'.$key.'">'.$pages.'</li>';
                                    }
                                    $list_pag_string .= (isset($prod_dip_lks['nextpages']) ? '<li>'.$prod_dip_lks['nextpages'].'</li>' : '')
                                                        .(isset($prod_dip_lks['next']) ? '<li>'.$prod_dip_lks['next'].'</li>' : '');
            $list_pag_string .= '</ul>
                            </div>
                        </div>';
}

$list_prod_string = $list_pag_string;

$lisprd_cnt = 0;
//$list_col_cnt = 0;
$col_padd = false;
$list_items_string = '';

while($prods_arr = xtc_db_fetch_array($prod_query)) {
    $lisprd_cnt++; //count items on one page
        
    if($lisprd_cnt % $lisprd_col_pcs == 1) {
        //$list_col_cnt++; //count columns on one page

        /*if($list_col_cnt != 1 && $list_col_cnt != $prod_list_cols) {
            $col_padd = true;
        }*/
        
        //$list_items_string .= '<div class="dib32'.($col_padd === true ? ' pdrl02' : '').'">';
        $list_items_string .= '<div class="lpdib">';
    }
    
    $list_items_string .= '<a href="'.xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $prods_arr['products_id'], $request_type).'">'.$prods_arr['products_manufacturers_model'].' '.$prods_arr['products_name'].'</a><br />';
    
    if($lisprd_cnt % $lisprd_col_pcs == 0 || $lisprd_cnt == $limit_per_page) {
        $list_items_string .= '</div>';
    }
}
$list_prod_string .= '<div class="cf">'.$list_items_string.'</div>';

//BOC debug
//$list_prod_string .= '<p>'.$lisprd_cnt.' | '.$list_col_cnt.' | '.$prod_list_cols.' | '.$lisprd_col_pcs.' | '.$lisprd_cnt % $lisprd_col_pcs.'</p>';
//$list_prod_string .= '<p>'.(21 % 20).'</p>';
//$list_prod_string .= '<p>$list_col_cnt != $prod_list_cols: '.($list_col_cnt != $prod_list_cols ? 'true' : 'false').'</p>';
//$list_prod_string .= '<p>$list_col_cnt == $prod_list_cols: '.($list_col_cnt == $prod_list_cols ? 'true' : 'false').'</p>';
//$nr_var_1 = 3; $nr_var_2 = 3;
//$list_prod_string .= '<p>$nr_var_1 != $nr_var_2: '.($nr_var_1 != $nr_var_2 ? 'true' : 'false').'</p>';
//$list_prod_string .= '<p>$nr_var_1 == $nr_var_2: '.($nr_var_1 == $nr_var_2 ? 'true' : 'false').'</p>';
//EOC debug

$list_prod_string .= $list_pag_string;


echo $list_prod_string;

?>
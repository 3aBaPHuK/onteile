<?php
/* -----------------------------------------------------------------------------------------
   $Id: item.php 5118 2013-07-18 10:58:36Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(item.php,v 1.6 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (item.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_ITEM_TEXT_TITLE', 'Costos de envío por pieza');
define('MODULE_SHIPPING_ITEM_TEXT_DESCRIPTION', 'Costos de envío por pieza');
define('MODULE_SHIPPING_ITEM_TEXT_WAY', 'El mejor camino');
define('MODULE_SHIPPING_ITEM_INVALID_ZONE', 'No es posible realizar un envío a este país.');

define('MODULE_SHIPPING_ITEM_STATUS_TITLE' , 'Activar Costos de envío por pieza');
define('MODULE_SHIPPING_ITEM_STATUS_DESC' , '¿Desea ofrecer Costos de envío por pieza?');
define('MODULE_SHIPPING_ITEM_ALLOWED_TITLE' , 'Zonas de envío permitidas');
define('MODULE_SHIPPING_ITEM_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE (deje este campo vacío si desea permitir todas las zonas))');
define('MODULE_SHIPPING_ITEM_TAX_CLASS_TITLE' , 'Zona de impuestos');
define('MODULE_SHIPPING_ITEM_TAX_CLASS_DESC' , 'Aplicar la siguiente clase de impuestos a los costos de envío.');
define('MODULE_SHIPPING_ITEM_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_ITEM_ZONE_DESC' , 'Si se ha seleccionado una zona, este método de envío se utilizará sólo para esa zona.');
define('MODULE_SHIPPING_ITEM_SORT_ORDER_TITLE' , 'Orden de clasificación');
define('MODULE_SHIPPING_ITEM_SORT_ORDER_DESC' , 'Orden de la visualización');
define('MODULE_SHIPPING_ITEM_NUMBER_ZONES_TITLE' , 'Número de zonas');
define('MODULE_SHIPPING_ITEM_NUMBER_ZONES_DESC' , 'Número de zonas proporcionadas');
define('MODULE_SHIPPING_ITEM_DISPLAY_TITLE' , 'Activar la visualización');
define('MODULE_SHIPPING_ITEM_DISPLAY_DESC' , '¿Desea mostrar si no es posible realizar envíos a ese país o si no se pueden calcular los costos de envío?');

for ($module_shipping_item_i = 1; $module_shipping_item_i <= MODULE_SHIPPING_ITEM_NUMBER_ZONES; $module_shipping_item_i ++) {
define('MODULE_SHIPPING_ITEM_COUNTRIES_'.$module_shipping_item_i.'_TITLE' , '<hr/>Zona '.$module_shipping_item_i.' países');
define('MODULE_SHIPPING_ITEM_COUNTRIES_'.$module_shipping_item_i.'_DESC' , 'Lista separada por comas de los códigos de países ISO de 2 caracteres que forman parte de la zona '.$module_shipping_item_i.' (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_ITEM_COST_'.$module_shipping_item_i.'_TITLE' , 'Zona '.$module_shipping_item_i.' costos de envío');
define('MODULE_SHIPPING_ITEM_COST_'.$module_shipping_item_i.'_DESC' , 'Los costos de envío por zona '.$module_shipping_item_i.' se multiplican por el número de artículos de un pedido si se especifica este método de envío.');
define('MODULE_SHIPPING_ITEM_HANDLING_'.$module_shipping_item_i.'_TITLE' , 'Zona '.$module_shipping_item_i.' Recargo de manejo');
define('MODULE_SHIPPING_ITEM_HANDLING_'.$module_shipping_item_i.'_DESC' , 'Recargo de manejo para esta zona de envío');
}
?>

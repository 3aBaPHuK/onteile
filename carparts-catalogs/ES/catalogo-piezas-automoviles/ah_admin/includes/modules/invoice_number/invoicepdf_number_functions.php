<?php
/********************************************************
* file: invoicepdf_number_functions.php
* use: define functions for PDFBillNext invoice number
* (c) noRiddle 10-2017
********************************************************/
   
if(defined('RUN_MODE_ADMIN')) {
    include(DIR_WS_MODULES.'invoice_number/'.$_SESSION['language'].'/invoice_number.php');
} else {
    include(DIR_ADMIN.'includes/modules/invoice_number/'.$_SESSION['language'].'/invoice_number.php');
}
   
function add_select_ibillnr($sSelect) {
    if (defined('MODULE_PDF_BILL_STATUS') && MODULE_PDF_BILL_STATUS == 'True') {
      $sSelect .= ',o.ibn_billnr';
    }
    return $sSelect;
}

function add_select_ibilldate($sSelect) {
    if (defined('MODULE_PDF_BILL_STATUS') && MODULE_PDF_BILL_STATUS == 'True') {
        $sSelect .= ',o.ibn_billdate';
    }
   return $sSelect;
}

function add_table_infos_ibillnr($order) {
    $html = '';
    if (defined('MODULE_PDF_BILL_STATUS') && MODULE_PDF_BILL_STATUS == 'True') {
        $html .= '<tr>'.PHP_EOL;
        $html .= '  <td class="main"><b>'.ENTRY_INVOICE_NUMBER.'</b></td>'.PHP_EOL;
        $html .= '  <td class="main">'.($order->info['ibn_billnr'] == '' ? '<span class="not_assigned">'.NOT_ASSIGNED.'<span>' : $order->info['ibn_billnr']).'</td>'.PHP_EOL;
        $html .= '</tr>'.PHP_EOL;
        $html .= '<tr>'.PHP_EOL;
        $html .= '  <td class="main"><b>'.ENTRY_INVOICE_DATE.'</b></td>'.PHP_EOL;
        $html .= '  <td class="main">'. ($order->info['ibn_billdate'] == '0000-00-00'? '<span class="not_assigned">'.NOT_ASSIGNED.'<span>' : xtc_date_short($order->info['ibn_billdate'])).'</td>'.PHP_EOL;
        $html .= '</tr>'.PHP_EOL;
    }
    return $html;
}

function add_btn_ibillnr($order, $oID) {
    $html = '';
    if (defined('MODULE_PDF_BILL_STATUS') && MODULE_PDF_BILL_STATUS == 'True') {
      if ($order->info['ibn_billnr'] == '') {
        $html = '              <a class="button ibillnr-btn" href="'.xtc_href_link(FILENAME_ORDERS, 'page='.$_GET['page'].'&oID='.$oID.'&action=edit&action2=set_ibillnr').'">'. BUTTON_BILL .'</a>'.PHP_EOL;
      }
    }
    return $html;
}
   
function action_next_ibillnr($order, $oID) {
    if( (isset($_GET['action2']) && $_GET['action2']=='set_ibillnr') && ($order->info['ibn_billnr'] == '') ) {
      $ibillnr = get_next_ibillnr($oID);
      set_order_ibillnr($oID, $ibillnr);
      set_next_ibillnr($oID);
      xtc_redirect(xtc_href_link(FILENAME_ORDERS, 'page=1&oID='.(int)$oID.'&action=edit'));
    }
}

function action_checkout_next_ibillnr($insert_id) {
    $ibillnr = get_next_ibillnr($insert_id);
    set_order_ibillnr($insert_id, $ibillnr);
    set_next_ibillnr($insert_id);
    // generate and send bill
    xtc_pdf_bill($insert_id, true);
}

function set_next_ibillnr($oID) {
    if(PDF_USE_ORDERID == 'false') {
        $data = (int)MODULE_INVOICEPDF_NUMBER_IBN_BILLNR;
        if ($data == 0) {
          return 0;
        }
    } else {
        $data = (int)$oID;
    }
    $data++;
    
    $sql_data_array = array('configuration_value' => $data);
    
    xtc_db_perform(TABLE_CONFIGURATION, $sql_data_array, 'update', "configuration_key = 'MODULE_INVOICEPDF_NUMBER_IBN_BILLNR'");
}

function get_next_ibillnr($oID) {
    if(PDF_USE_ORDERID == 'false') {
        $n = (int)MODULE_INVOICEPDF_NUMBER_IBN_BILLNR;
        $year = date('Y');
        $month = date('m');
        $day = date('d');

        $d = MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT;
        $d = str_replace('{n}', $n, $d);
        $d = str_replace('{d}', $day, $d);
        $d = str_replace('{m}', $month, $d);
        $d = str_replace('{y}', $year, $d);
    } else {
        
        if(defined('PDF_USE_ORDERID_PREFIX') && PDF_USE_ORDERID_PREFIX != '') {
            $d = PDF_USE_ORDERID_PREFIX.(int)$oID;
        } else {
            $d = (int)$oID;
        }
        if(defined('PDF_USE_ORDERID_SUFFIX') && PDF_USE_ORDERID_SUFFIX != '') {
            $d .= PDF_USE_ORDERID_SUFFIX;
        }
    }
        return $d;
}

function set_order_ibillnr($orders_id, $ibn_billnr) {
    $sql_data_array = array(
        'ibn_billnr' => xtc_db_prepare_input($ibn_billnr), 
        'ibn_billdate' => 'now()'
      );
    xtc_db_perform(TABLE_ORDERS, $sql_data_array, 'update', "orders_id = '" . (int)$orders_id . "'"); 
}
?>
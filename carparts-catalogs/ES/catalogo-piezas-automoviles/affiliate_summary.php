<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_summary.php 40 2013-01-08 16:36:44Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_summary.php, v 1.17 2003/09/17);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

require('includes/application_top.php');

if (!isset($_SESSION['affiliate_id'])) {
    xtc_redirect(xtc_href_link(FILENAME_AFFILIATE, '', 'SSL'));
}

// create smarty elements
$smarty = new Smarty;

// include needed functions
require_once(DIR_FS_INC . 'affiliate_period.inc.php');
require_once(DIR_FS_INC . 'affiliate_level_statistics_query.inc.php');
require_once(DIR_FS_INC . 'xtc_image_button.inc.php');
require_once(DIR_FS_INC . 'xtc_round.inc.php');
require_once(DIR_FS_INC . 'affiliate_build_orders_status_query.inc.php');

$breadcrumb->add(NAVBAR_TITLE, xtc_href_link(FILENAME_AFFILIATE, '', 'SSL'));
$breadcrumb->add(NAVBAR_TITLE_SUMMARY, xtc_href_link(FILENAME_AFFILIATE_SUMMARY));

// get the Bannerstatistic
$affiliate_banner_history_raw = "select sum(affiliate_banners_shown) as count from " . TABLE_AFFILIATE_BANNERS_HISTORY .  " where affiliate_banners_affiliate_id  = '" .  (int)$_SESSION['affiliate_id'] . "'";
$affiliate_banner_history_query = xtc_db_query($affiliate_banner_history_raw);
$affiliate_banner_history = xtc_db_fetch_array($affiliate_banner_history_query);
$affiliate_impressions = $affiliate_banner_history['count'];
if ($affiliate_impressions == 0) $affiliate_impressions = "n/a";
$smarty->assign('affiliate_impressions', $affiliate_impressions);

// get the clickthroughs
$affiliate_clickthroughs_raw = "select count(*) as count from " . TABLE_AFFILIATE_CLICKTHROUGHS . " where affiliate_id = '" . (int)$_SESSION['affiliate_id'] . "'";
$affiliate_clickthroughs_query = xtc_db_query($affiliate_clickthroughs_raw);
$affiliate_clickthroughs = xtc_db_fetch_array($affiliate_clickthroughs_query);
$affiliate_clickthroughs = $affiliate_clickthroughs['count'];
$smarty->assign('affiliate_clickthroughs', $affiliate_clickthroughs);

$affiliate_sales_raw = "select count(*) as count, sum(affiliate_value) as total, sum(affiliate_payment) as payment from " . TABLE_AFFILIATE_SALES . " a
    left join " . TABLE_ORDERS . " o on (a.affiliate_orders_id=o.orders_id)
    where a.affiliate_id = '" . (int)$_SESSION['affiliate_id'] . "' " . affiliate_build_orders_status_query();
$affiliate_sales_query = xtc_db_query($affiliate_sales_raw);
$affiliate_sales = xtc_db_fetch_array($affiliate_sales_query);
$affiliate_transactions=$affiliate_sales['count'];
$smarty->assign('affiliate_transactions', xtc_not_null($affiliate_transactions) ? $affiliate_transactions : 0);

if ($affiliate_clickthroughs > 0) {
	$affiliate_conversions = xtc_round(($affiliate_transactions / $affiliate_clickthroughs) * 100, 2) . "%";
}
else {
    $affiliate_conversions = "n/a";
}
$smarty->assign('affiliate_conversions', $affiliate_conversions);

$affiliate_raw = "select MONTH(a.affiliate_date_account_created) as start_month, "
                   . "YEAR(a.affiliate_date_account_created) as start_year, "
                   . "a.affiliate_commission_percent, a.affiliate_firstname, a.affiliate_id, a.affiliate_lastname "
                   . "from " . TABLE_AFFILIATE . " AS a "
                   . " where a.affiliate_id  = '" . $_SESSION['affiliate_id'] . "'";
$affiliate_query = xtc_db_query( $affiliate_raw );
$affiliate = xtc_db_fetch_array($affiliate_query);
$smarty->assign('affiliate', $affiliate);

$smarty->assign('period_selector', affiliate_period( 'a_period', $affiliate['start_year'], $affiliate['start_month'], true, xtc_db_prepare_input( $_GET['a_period'] ), 'onChange="this.form.submit();"' ));

$affiliate_percent = 0;
$affiliate_percent = $affiliate['affiliate_commission_percent'];
if ($affiliate_percent < AFFILIATE_PERCENT) $affiliate_percent = AFFILIATE_PERCENT;
$smarty->assign('affiliate_percent', xtc_round($affiliate_percent, 2));

$affiliate_percent_tier = explode(";", AFFILIATE_TIER_PERCENTAGE, AFFILIATE_TIER_LEVELS );

if ( (empty($_GET['a_period'])) or ( $_GET['a_period'] == "all" ) ) {
    $affiliate_sales = affiliate_level_statistics_query( $_SESSION['affiliate_id'] );
}
else {
    $affiliate_sales = affiliate_level_statistics_query( $_SESSION['affiliate_id'], xtc_db_prepare_input( $_GET['a_period'] ) );
}
$smarty->assign('affiliate_amount', $xtPrice->xtcFormat($affiliate_sales['total'], true));

if ($affiliate_transactions > 0) {
	$affiliate_average = xtc_round($affiliate_amount / $affiliate_transactions, 2);
	$affiliate_average = $xtPrice->xtcFormat($affiliate_average, true);
}
else {
	$affiliate_average = "n/a";
}
$smarty->assign('affiliate_average', $affiliate_average);

$smarty->assign('affiliate_commission', $xtPrice->xtcFormat($affiliate_sales['payment'], true));

require(DIR_FS_CATALOG .'templates/'.CURRENT_TEMPLATE. '/source/boxes.php');
require(DIR_WS_INCLUDES . 'header.php');

$smarty->assign('FORM_ACTION', xtc_draw_form('period', xtc_href_link(FILENAME_AFFILIATE_SUMMARY ), 'get', 'SSL' ));

$smarty->assign('LINK_IMPRESSION', '<a href="javascript:popupWindow(\'' . xtc_href_link(FILENAME_AFFILIATE_HELP, 'issue=1') . '\')">');
$smarty->assign('LINK_VISIT', '<a href="javascript:popupWindow(\'' . xtc_href_link(FILENAME_AFFILIATE_HELP, 'issue=2') . '\')">');
$smarty->assign('LINK_TRANSACTIONS', '<a href="javascript:popupWindow(\'' . xtc_href_link(FILENAME_AFFILIATE_HELP, 'issue=3') . '\')">');
$smarty->assign('LINK_CONVERSION', '<a href="javascript:popupWindow(\'' . xtc_href_link(FILENAME_AFFILIATE_HELP, 'issue=4') . '\')">');
$smarty->assign('LINK_AMOUNT', '<a href="javascript:popupWindow(\'' . xtc_href_link(FILENAME_AFFILIATE_HELP, 'issue=5') . '\')">');
$smarty->assign('LINK_AVERAGE', '<a href="javascript:popupWindow(\'' . xtc_href_link(FILENAME_AFFILIATE_HELP, 'issue=6') . '\')">');
$smarty->assign('LINK_COMISSION_RATE', '<a href="javascript:popupWindow(\'' . xtc_href_link(FILENAME_AFFILIATE_HELP, 'issue=7') . '\')">');
$smarty->assign('LINK_COMISSION', '<a href="javascript:popupWindow(\'' . xtc_href_link(FILENAME_AFFILIATE_HELP, 'issue=8') . '\')">');

$check_sql = xtc_db_query("SELECT affiliate_tiers_allowed FROM " . TABLE_AFFILIATE . " WHERE affiliate_id = '" . $_SESSION['affiliate_id'] . "'");
$check = xtc_db_fetch_array($check_sql);

$affiliate_tiers_array = array();

if ( AFFILIATE_USE_TIER == 'true' && $check['affiliate_tiers_allowed'] == 1) {
	$smarty->assign('AFFILIATE_USE_TIER', 'true');

    for ($tier_number = 0; $tier_number <= AFFILIATE_TIER_LEVELS; $tier_number++ ) {
    	if (is_null($affiliate_percent_tier[$tier_number - 1])) {
    		$affiliate_percent_tier[$tier_number - 1] = $affiliate_percent;
    	}
    	$affiliate_tiers_array[] = array('level_link' => xtc_href_link(FILENAME_AFFILIATE_SALES, (xtc_not_null($a_period))?'a_level=' . $tier_number . '&a_period=' . $a_period:'a_level=' . $tier_number, 'SSL'),
    									 'level_number' => $tier_number,
    									 'level_percent' => xtc_round($affiliate_percent_tier[$tier_number - 1], 2),
    									 'level_count' => ($affiliate_sales[$tier_number]['count'] > 0 ? $affiliate_sales[$tier_number]['count'] : '0'),
    									 'level_total' => $xtPrice->xtcFormat($affiliate_sales[$tier_number]['total'], true),
    									 'level_payment' => $xtPrice->xtcFormat($affiliate_sales[$tier_number]['payment'],true));
	}
	$smarty->assign('tiers_array', $affiliate_tiers_array);
}
$smarty->assign('LINK_BANNER', '<a href="' . xtc_href_link(FILENAME_AFFILIATE_BANNERS) . '">' . xtc_image_button('button_affiliate_banners.gif', IMAGE_BANNERS) . '</a>');
$smarty->assign('LINK_BANNER_RAW', xtc_href_link(FILENAME_AFFILIATE_BANNERS));
$smarty->assign('LINK_CLICKS', '<a href="' . xtc_href_link(FILENAME_AFFILIATE_CLICKS, '', 'SSL') . '">' . xtc_image_button('button_affiliate_clickthroughs.gif', IMAGE_CLICKTHROUGHS) . '</a>');
$smarty->assign('LINK_CLICKS_RAW', xtc_href_link(FILENAME_AFFILIATE_CLICKS, '', 'SSL'));
$smarty->assign('LINK_SALES', '<a href="' . xtc_href_link(FILENAME_AFFILIATE_SALES, (xtc_not_null($a_period))?'a_period=' . $a_period:'', 'SSL') . '">' . xtc_image_button('button_affiliate_sales.gif', IMAGE_SALES) . '</a>');
$smarty->assign('LINK_SALES_RAW', xtc_href_link(FILENAME_AFFILIATE_SALES, (xtc_not_null($a_period))?'a_period=' . $a_period:'', 'SSL'));
$smarty->assign('language', $_SESSION['language']);
$smarty->caching = 0;
$main_content=$smarty->fetch(CURRENT_TEMPLATE . '/module/affiliate_summary.html');
$smarty->assign('main_content',$main_content);

$smarty->assign('language', $_SESSION['language']);
$smarty->caching = 0;
if (!defined(RM))
	$smarty->load_filter('output', 'note');
	
$smarty->display(CURRENT_TEMPLATE . '/index.html');

include ('includes/application_bottom.php');
?>
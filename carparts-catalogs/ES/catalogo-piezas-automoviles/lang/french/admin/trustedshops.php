<?php
  /* --------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Trusted Shops');
define('HEADING_TECHNOLOGIE', 'Technologie Trustbadge');
define('HEADING_PRODUCTS', 'Produits');

define('TABLE_HEADING_TRUSTEDSHOPS_ID', 'TS-ID');
define('TABLE_HEADING_LANGUAGE', 'Langue');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');

define('HEADING_TRUSTBADGE', 'Trustbadge');
define('HEADING_ADVANCED', 'Avancé');

define('TEXT_DATE_ADDED', 'Ajouté le:');
define('TEXT_LAST_MODIFIED', 'Actualisé le:');
define('TEXT_TRUSTEDSHOPS_STATUS', 'Statut:');
define('TEXT_TRUSTEDSHOPS_ID', 'TS-ID:');
define('TEXT_TRUSTEDSHOPS_LANGUAGES', 'Sélectionnez la langue:');
define('TEXT_TRUSTEDSHOPS_WIDGET', 'Activer le widget d\'avis client');
define('TEXT_TRUSTEDSHOPS_SNIPPETS', 'Activer l\'annonce Rich Snippets Anzeige dans Google');
define('TEXT_TRUSTEDSHOPS_SNIPPETS_INFO', '<a href="https://developers.google.com/structured-data/rich-snippets/products" target="_blank">Goolge Rich Snippets Dokumentation</a>');
define('TEXT_WIDGET_INFO', 'Le widget montre vos étoiles d\'évaluation et l\'évaluation la plus récente en plus de votre Trustbadge dans votre Shop.');
define('TEXT_TRUSTBADGE_INFO', 'Le Trustbadge montre votre label et les évaluations de clients dans la variante par défaut et se trouve en bas à droite dans votre Shop. Vous pouvez choisir, soit d\'utiliser le Trustbadge dans la version recommandée (par défaut), soit d\'afficher seulement les avis clients ou d\'individualiser l\'apparence et la position (des connaissances de programmation sont nécessaire pour ceci).');
define('TEXT_SNIPPETS_PRODUCTS', 'Pages de détails de produits');
define('TEXT_SNIPPETS_CATEGORY', 'Pages de catégories');
define('TEXT_SNIPPETS_INDEX', 'Page d\'acceuil (non recommandé)');

define('TEXT_TRUSTEDSHOPS_BADGE', 'Sélectionnez la version:');
define('TEXT_BADGE_DEFAULT', 'Par défaut');
define('TEXT_BADGE_SMALL', 'Par défaut (petit)');
define('TEXT_BADGE_REVIEWS', 'Evaluations');
define('TEXT_BADGE_CUSTOM', 'Custom');
define('TEXT_BADGE_CUSTOM_REVIEWS', 'Custom (critiques)');
define('TEXT_BADGE_OFFSET', 'Position Y-Achse (max. 250px):');
define('TEXT_BADGE_INSTRUCTION', 'Vous trouverez <a href="%s" target="_blank">un guide étape par étape </a> dans notre centre d\'intégration pour la configuration et l\'intégration individuelle.');
define('TEXT_BADGE_CUSTOM_CODE', 'Insérez votre code Trustbadge ici :');

define('TEXT_PRODUCT_STICKER_API', 'Product Sticker API (Beta):');
define('TEXT_PRODUCT_STICKER_API_INFO', 'Par notre API nous vous offrons l\'interface qui vous permet d\'utiliser vos évaluations de produits dans votre propre système.');
define('TEXT_PRODUCT_STICKER_STATUS', 'Product Sticker Status:');
define('TEXT_PRODUCT_STICKER', 'Editer le Product Sticker Code');
define('TEXT_PRODUCT_STICKER_INFO', 'Le Product Sticker vous montre les évaluations de produits actuelles dans votre magasin.<br/>Avec notre<a target="_blank" href="%s">guide</a> vous configurez votre  Product Sticker.');
define('TEXT_PRODUCT_STICKER_INTRO', 'Evaluations de clients');

define('TEXT_REVIEW_STICKER_STATUS', 'Review Sticker Statut:');
define('TEXT_REVIEW_STICKER', 'Editer Review Sticker Code:');
define('TEXT_REVIEW_STICKER_INFO', 'Le Review Sticker vous montre les évaluations actuelles de votre magasin.<br/>Avec notre<a target="_blank" href="%s">guide</a> vous configurez votre Review Sticker.');
define('TEXT_REVIEW_STICKER_INTRO', 'Evaluations de clients');

define('TEXT_HEADING_DELETE_TRUSTEDSHOPS', 'Supprimer la TS-ID');
define('TEXT_DELETE_INTRO', 'Êtes-vous sûr de vouloir supprimer cette TS-ID?');

define('TEXT_DISABLED', 'désactivé');
define('TEXT_ENABLED', 'activé');

define('TEXT_DISPLAY_NUMBER_OF_TRUSTEDSHOPS', 'Affichés sont <b>%d</b> à <b>%d</b> (d\'un total de <b>%d</b> TS-ID)');

define('TEXT_TS_MAIN_INFO', '
<img src="images/trustedshops/trustmark-widget-de-AT-CH-DE-w300-h245-v1.png" style="width:200px;float:right;margin-top:30px;padding-left:30px;"/>
<h2>Trusted Shops</h2>
<b>La confiance: la clé du succès</b><br/>
<br/>
Avec plus de 15 ans d\'expérience et plus de 20.000 magasins, Trusted Shops est la marque de confiance leader d\'Europe dans le e-commerce. Offrez plus de sécurité à vos clients pour leurs achats en ligne grâce au module Trusted Shops - pour une expérience de shopping positive et un chiffre d\'affaires croissant.
<ul>
  <li><b>Label de qualité avec garantie de remboursement:</b> La sécurité pour plus de ventes</li>
  <li><b>Avis clients et évaluations de produits:</b> La confiance pour plus de traffique</li>
  <li><b>Générateur de textes juridiques et protection d\'avertissements</b> L\'expertise pour plus de sécurité juridique</li>
</ul>');
define('TEXT_TS_BADGE_INFO', '
<img src="images/trustedshops/trustbadge_Review_Trustmark_de-DE.png" style="width:115px;float:right;margin-top:10px;padding-left:30px;"/>
<h2>Technologie Trustbadge</h2>
<b>Simple, flexible, innovateur</b><br/>
<br/>
Le module Trusted Shops Modul intègre la technologie Trustbadge dans votre boutique: il montre vos éléments de confiance visibles sur toutes les pages de la boutique en ligne et fonctionne comme poste de commande qui offre la garantie de remboursement au bon moment à vos clients ou qui leur envoie une demande d\'évaluation après la commande.<br/>
<br/>
En effet, le Trustbadge est parfaitement équipé pour le futur en tant que code dynamique: Les fonctionnalités peuvent être actualisés sans efforts et de nouvelles fonctions peuvent être intégrées.Vous pouvez donc être sûr, grâce à la technologie Trustbadge, de toujours utiliser les solutions de confiance de Trusted Shops au mieux.');
define('TEXT_TS_PRODUCT_INFO', '
<h2>Produits</h2>
<b>Vos avantages avec Trusted Shops</b><br/>
<br/>
<ul>
  <li><b>Label de qualité - plus de confiance, plus de chiffre d\'affaire</b><br/>
       Plus de 60% des clients en ligne font confiance aux labels de qualité. Vous aussi, utilisez cette confiance pour des taux de conversions plus élevés </li>
 <li><b>Les évaluations de clients - cinq étoiles disent plus que mille mots</b><br/>
      Un marketing d\'évaluation professionnel pour plus d\'attention dans Google, plus de SEO et plus de possibilités d\'analyse.</li>
  <li><b>La garantie de remboursement - plus de sécurité, moins dâbandons de commande</b><br/>
      Moins le risque financier est grand pour l\'acheteur, plus le panier est grand - et plus le chemin à la caisse devient facile.</li>
</ul>');
define('TEXT_TS_SPECIAL_INFO', '
<b>Notre offre exeptionelle pour vous:<br/> baisse durable de prix pour les utilisateurs modified!</b><br/>
<a target="_blank" href="http://www.trustedshops.de/shopbetreiber/index.html?shopsw=MODIFIED#offer?utm_source=Xtmodified&utm_medium=modul&utm_content=link1&utm_campaign=modultracking&a_aid=55cb437783a78"><img src="images/trustedshops/btn_330x40_de-DE_v2.png" style="margin-top:10px;"/></a>');
            
?>

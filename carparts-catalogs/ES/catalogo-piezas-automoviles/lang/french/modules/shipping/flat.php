<?php
/* -----------------------------------------------------------------------------------------
   $Id: flat.php 4200 2013-01-10 19:47:11Z Tomcraft1980 $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(flat.php,v 1.6 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (flat.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_FLAT_TEXT_TITLE', 'Frais de port forfaitaires');
define('MODULE_SHIPPING_FLAT_TEXT_DESCRIPTION', 'Frais de port forfaitaires');
define('MODULE_SHIPPING_FLAT_TEXT_WAY', 'Meilleur chemin');

define('MODULE_SHIPPING_FLAT_STATUS_TITLE' , 'Activer les frais de port forfaitaires');
define('MODULE_SHIPPING_FLAT_STATUS_DESC' , 'Voulez-vous proposer les frais de port forfaitaires?');
define('MODULE_SHIPPING_FLAT_ALLOWED_TITLE' , 'Zones d\'envoi permises');
define('MODULE_SHIPPING_FLAT_ALLOWED_DESC' , 'Veuillez indiquer les zones <b>individuellement</b> pour lesquelles l\'envoi est possible. Par exemple AT,DE (Laissez ce champ libre si vous volez permettre toutes les zones)');
define('MODULE_SHIPPING_FLAT_COST_TITLE' , 'Frais de port');
define('MODULE_SHIPPING_FLAT_COST_DESC' , 'Frais de port pour toutes les commandes de ce type d\'envoi');
define('MODULE_SHIPPING_FLAT_TAX_CLASS_TITLE' , 'Catégorie fiscale');
define('MODULE_SHIPPING_FLAT_TAX_CLASS_DESC' , 'Appliquer la catégorie fiscale aux frais de port');
define('MODULE_SHIPPING_FLAT_ZONE_TITLE' , 'Zone d\'envoi');
define('MODULE_SHIPPING_FLAT_ZONE_DESC' , 'Quand une zone est séléctionnée ce type d\'envoi s\'applique seulement sur cette zone.');
define('MODULE_SHIPPING_FLAT_SORT_ORDER_TITLE' , 'Ordre de tri');
define('MODULE_SHIPPING_FLAT_SORT_ORDER_DESC' , 'Ordre d\'affichage');
?>

<?php
/*********************************************************************
* file: generate_shoppingcart_link.php for use in backend of shop
* path: /lang/italian/admin/
* use: language file for module
* 
* (c) copyright 10-2020, noRiddle
*********************************************************************/

define('GSCL_NO_MODEL_NO_TXT', 'You either no entered a model number or the entered product doesn\'t exist in the shop.');
define('GSCL_ONE_MODEL_NO_EXISTS_TXT', 'At least one of the entered model numbers doesn\'t exist in the shop.');
define('GSCL_STATUS_WRONG_TXT', 'The products %s either do not have product_status 1 (=> deactivated) or do not have gm_price_status 0 (=> price question or not available). Please verify in the shop.');
define('GSCL_EXPLANATION_TXT', '<h1>Generate a shopping cart link</h1>
                                <ul>
                                    <li>Enter model number(s), adapt "Pieces" where required and click "Generate the link"
                                        <ul>
                                            <li>If several model numbers are required:<br />generate another input field by clicking the button "more input fields"</li>
                                            <li>If less model numbers are required than input fields are available:<br />remove not needed fields by clicking the button "less input fields"</li>
                                         </ul>
                                    </li>
                                    <li>To verify the model number "live" click somewhere into the white space</li>
                                    <li>Copy link and send it to the customer via e-mail</li>
                                </ul>');
define('GSCL_MODEL_NO_TXT', 'Model no.:');
define('GSCL_PCS_TXT', '- | - Pieces:');
define('GSCL_MORE_INPUT_TXT', '+ more input fields');
define('GSCL_LESS_INPUT_TXT', '- less input fields');
define('GSCL_GENERATE_LINK_TXT', '&raquo; Generate the link');
define('GSCL_DELETE_ALL_ENTRIES_TXT', 'x Delete all entries');
<?php
/* -----------------------------------------------------------------------------------------
   $Id: findologic.php 2011-11-24 modified-shop $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com
   (c) 2003   nextcommerce (invoice.php,v 1.6 2003/08/24); www.nextcommerce.org
   (c) 2005 XT-Commerce - community made shopping http://www.xt-commerce.com ($Id: billiger.php 950 2005-05-14 16:45:21Z mz $)
   (c) 2008 Gambio OHG (billiger.php 2008-11-11 gambio)

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_FINDOLOGIC_TEXT_TITLE', 'FINDOLOGIC recherche de boutique - stop searching - find!');
define('MODULE_FINDOLOGIC_TEXT_DESCRIPTION', 'FINDOLOGIC est la solution ultimative de recherche pour votre boutique en ligne. Vos clients trouvent très vite l\'article souhaité et l\'achètent!<span style="color: #ff8c00;">Des augmentations des ventes  de nos clients se situent dans un pourcentage de deux chiffres.</span><br /><br />Avec le code d\'affiliation <span class="col-red" style="font-weight:bold;">qXgXj</span> vous épargnez 50€ de frais d\'activation! <br /><br /><a href="https://secure.findologic.com/bestellung?partner=qXgXj"target="_blank"><strong><u>Inscrivez-vous maintenant!</u></strong></a>');
define('MODULE_FINDOLOGIC_LANG_TITLE' , '<hr noshade>Langue');
define('MODULE_FINDOLOGIC_LANG_DESC' , 'Langue des articles que vous souhaitez exporter.<br/><br/><b>Important:</b> Cette sélection n\'est que possible lorsque vous n\'avez réservé seulement 1 service.');
define('MODULE_FINDOLOGIC_CUSTOMER_GROUP_TITLE' , '<hr noshade>Groupe de clients:');
define('MODULE_FINDOLOGIC_CUSTOMER_GROUP_DESC' , 'Veuillez choisir le groupe de clients qui forme la base pour le prix exportée. (Si vous n\'avez pas de prix des groupes de clients, choisissez invité):');
define('MODULE_FINDOLOGIC_CURRENCY_TITLE' , '<hr noshade>Monnaie:');
define('MODULE_FINDOLOGIC_CURRENCY_DESC' , 'Monnaie dans le fichier d\'export');
define('MODULE_FINDOLOGIC_STATUS_TITLE' , 'Statut');
define('MODULE_FINDOLOGIC_STATUS_DESC' , 'Activer le module?');
define('MODULE_FINDOLOGIC_AUTOCOMPLETE_TITLE' , '<hr noshade>Recherche Autocomplete');
define('MODULE_FINDOLOGIC_AUTOCOMPLETE_DESC' , 'Activer la fonctionnalité Autocomplete dans la boîte de recherche?');

?>

<?php
/* --------------------------------------------------------------
   $Id$


   modified eCommerce Shopsoftware
   http://www.modified-shop.org


   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   Released under the GNU General Public License
   --------------------------------------------------------------*/


define('HEADING_TITLE', 'Seguimiento de los envíos de los proveedores de servicios de paquetería');


define('TABLE_HEADING_CARRIER_NAME', 'Nombre del proveedor del servicio de paquetería');
define('TABLE_HEADING_TRACKING_LINK', 'URL del seguimiento de los envíos del proveedor de servicios de paquetería');
define('TABLE_HEADING_SORT_ORDER', 'Orden de clasificación');
define('TABLE_HEADING_ACTION', 'Acción');
define('TEXT_CARRIER_LINK_DESCRIPTION', '<b>Aviso:</b> La URL está aprovisionado con símbolos de marcadores de posición <b>$1, $2, $3, $4</b> y <b>$5</b> . Estos marcadores de posición se reemplazarán más adelante de la siguiente manera:<br/><ul><li>$1: Número de seguimiento o ID de seguimiento</li><li>$2: ISO 639-1 Código de idioma</li><li>$3: Día de la fecha de creación</li><li>$4: Mes de la fecha de creación</li><li>$5: Año de la fecha de creación</li></ul>');
define('TEXT_INFO_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios');
define('TEXT_INFO_CARRIER_NAME', 'Nombre del proveedor del servicio de paquetería:');
define('TEXT_INFO_CARRIER_TRACKING_LINK', 'URL del seguimiento de los envíos');
define('TEXT_INFO_CARRIER_SORT_ORDER', 'Orden de clasificación:');
define('TEXT_INFO_DATE_ADDED', 'agregado el:');
define('TEXT_INFO_LAST_MODIFIED', 'Última modificación:');
define('TEXT_INFO_INSERT_INTRO', 'Por favor, introduzca el nuevo proveedor del servicio de paquetería con todos los datos relevantes.');
define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar este proveedor del servicio de paquetería?');
define('TEXT_INFO_HEADING_NEW_CARRIER', 'Nuevo proveedor del servicio de paquetería');
define('TEXT_INFO_HEADING_EDIT_CARRIER', 'Editar proveedor del servicio de paquetería');
define('TEXT_INFO_HEADING_DELETE_CARRIER', 'Borrar proveedor del servicio de paquetería');
define('BUTTON_NEW_CARRIER', 'Nuevo proveedor del servicio de paquetería');
?>

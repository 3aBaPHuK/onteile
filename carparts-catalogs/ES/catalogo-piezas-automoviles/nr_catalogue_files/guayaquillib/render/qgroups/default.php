<?php
/**********************************************
* file reworked and adadapted by noRiddle
*
* (c) noRiddle 03-2018
**********************************************/

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'template.php';

class GuayaquilQuickGroupsList extends GuayaquilTemplate
{
	var $groups = NULL;
	var $vehicleid = NULL;
	var $ssd = NULL;
    var $collapsed_level = 1;
    var $catalog;

    var $drawtoolbar = true;

	function Draw($groups, $catalog, $vehicle_id, $ssd)
	{
        $this->catalog = $catalog;
        $this->vehicleid = $vehicle_id;
        $this->ssd = $ssd;
        //$this->AppendCSS(dirname(__FILE__).'/groups.css'); //include this in /templates/catalogue/css/general.css.php, noRiddle
        //$this->AppendJavaScript(dirname(__FILE__).'/groups.js'); //include this in /templates/catalogue/javascript/general.js.php, noRiddle

        $this->groups = $groups->row;

        GuayaquilToolbar::AddButton($this->GetLocalizedString('VehicleLink'), $this->FormatLink('vehicle', null, $this->catalog));

        $html = $this->drawtoolbar ? GuayaquilToolbar::Draw() : '';

        //$html .= '<div class="col-md-8 cright">'; //noRiddle
        $html .= '<div class="left-2flts nr-highlightbox">';
        //$html .= '<div class="div-padd-5">'; //noRiddle
        $html .= '<div>'; //noRiddle
        
        $html .= '<p>'.$this->GetLocalizedString('explain_group_search').'</p><br />'; // add explanation, noRiddle
        
        $html .= $this->DrawSearchPanel();

        $html .= '<div id="qgTree" onclick="tree_toggle(arguments[0])"><ul class="qgContainer">';

        if (count($this->groups) == 1)
        {
            $index = 1;
            $amount = count($groups->row);
            foreach ($this->groups->row as $subgroup)
                $html .= $this->DrawTreeNode($subgroup, 1, $index++ == $amount);
        }
        else
            foreach ($this->groups as $group)
                $html .= $this->DrawTreeNode($group, 1);

        $html .= '</ul></div>';
        $html .= '</div>'; //end div-padd-5
        $html .= '</div>'; //end col-md-4 cright

		//BOC don't need link to laximo, noRiddle
        //$html .= '<div style="visibility: visible; display: block; height: 20px; text-align: right;"><a href="http://dev.laximo.ru" rel="follow" style="visibility: visible; display: inline; font-size: 10px; font-weight: normal; text-decoration: none;">guayaquil</a></div>';
        $html .= '<div style="display:block; height:20px; text-align:right;"></div>';
        //EOC don't need link to laximo, noRiddle

		return $html;
	}

	function DrawTreeNode($group, $level, $last = false)
	{
        $childrens = count($group->children());
        $html = '<li class="qgNode '.($childrens == 0 ? 'qgExpandLeaf' : 'qgExpandClosed').($last ? ' qgIsLast' : '').'"><div class="qgExpand"></div>';

        //BOC new parameter for children to be able to hide link in case group has got sub-groups (faster Queries), 11-2019, noRiddle
        //$html .= $this->DrawItem($group, $level);
        $no_of_children = $childrens;
        $html .= $this->DrawItem($group, $level, $no_of_children);
        //EOC new parameter for children to be able to hide link in case group has got sub-groups (faster Queries), 11-2019, noRiddle

        $subhtml = '';
        $index = 1;
		foreach ($group->row as $subgroup)
			$subhtml .= $this->DrawTreeNode($subgroup, $level + 1, $index++ == $childrens);

        if ($subhtml)
            $html .= '<ul class="qgContainer">'.$subhtml.'</ul>';

		$html .= '</li>';

		return $html;
	}
	
	//function DrawItem($group, $level)
    function DrawItem($group, $level, $nofchild) //new parameter for children to be able to hide link in case group has got sub-groups (faster Queries), 11-2019, noRiddle
	{
        $has_link = ((string)$group['link']) == 'true';
        //if ($has_link)
        if($has_link && $nofchild == 0) //make use of new parameter for children to be able to hide link in case group has got sub-groups (faster Queries), 11-2019, noRiddle
        {
            $link = $this->FormatLink('quickgroup', $group, $this->catalog);
            //return '<div class="qgContent"><a href="'.$link.'">'.$group['name'].'</a></div>';
            return '<div class="qgContent nr-expand"><a href="'.$link.'">'.$group['name'].'</a></div>';
        }

        //return '<div class="qgContent">'.$group['name'].'</div>';
        return '<div class="qgContent nr-expand">'.$group['name'].'</div>';
	}

	function DrawSearchPanel()
	{
        //BOC use our css button, noRiddle
        /*return '<input type="text" maxlength="20" size="50" id="qgsearchinput" value="'.$this->GetLocalizedString('ENTER_GROUP_NAME').'" title="'.$this->GetLocalizedString('ENTER_GROUP_NAME').'" onfocus=";if(this.value==\''.$this->GetLocalizedString('ENTER_GROUP_NAME').'\')this.value=\'\';" onblur="if(this.value.replace(\' \', \'\')==\'\')this.value=\''.$this->GetLocalizedString('ENTER_GROUP_NAME').'\';" onkeyup="QuickGroups.Search(this.value);">
            <input type="button" value="'.$this->GetLocalizedString('RESET_GROUP_NAME').'" onclick="jQuery(\'#qgsearchinput\').attr(\'value\', \''.$this->GetLocalizedString('ENTER_GROUP_NAME').'\'); QuickGroups.Search(\'\');">

        <div id="qgFilteredGroups"></div>';*/
        
        /*return '<input type="text" maxlength="20" size="50" id="qgsearchinput" value="'.$this->GetLocalizedString('ENTER_GROUP_NAME').'" title="'.$this->GetLocalizedString('ENTER_GROUP_NAME').'" onfocus=";if(this.value==\''.$this->GetLocalizedString('ENTER_GROUP_NAME').'\')this.value=\'\';" onblur="if(this.value.replace(\' \', \'\')==\'\')this.value=\''.$this->GetLocalizedString('ENTER_GROUP_NAME').'\';" onkeyup="QuickGroups.Search(this.value);">
            <button type="button" class="btn button-catalogue" onclick="jQuery(\'#qgsearchinput\').attr(\'value\', \''.$this->GetLocalizedString('ENTER_GROUP_NAME').'\'); QuickGroups.Search(\'\');">'.$this->GetLocalizedString('RESET_GROUP_NAME').'</button>

        <div id="qgFilteredGroups"></div>';*/
        
        $ret_val = '<input type="text" maxlength="20" size="50" id="qgsearchinput" value="'.$this->GetLocalizedString('ENTER_GROUP_NAME').'" title="'.$this->GetLocalizedString('ENTER_GROUP_NAME').'" onfocus=";if(this.value==\''.$this->GetLocalizedString('ENTER_GROUP_NAME').'\')this.value=\'\';" onblur="if(this.value.replace(\' \', \'\')==\'\')this.value=\''.$this->GetLocalizedString('ENTER_GROUP_NAME').'\';" onkeyup="QuickGroups.Search(this.value);" placeholder="'.$this->GetLocalizedString('ENTER_GROUP_NAME').'">';
        $ret_val .= '<p><span class="cssButton cssButtonColor1" onclick="jQuery(\'#qgsearchinput\').attr(\'value\', \''.$this->GetLocalizedString('ENTER_GROUP_NAME').'\'); QuickGroups.Search(\'\');">';
        $ret_val .= '<span class="cssButtonText">'.$this->GetLocalizedString('RESET_GROUP_NAME').'</span>';
        $ret_val .=  '<button type="button" class="cssButtonText" title="'.$this->GetLocalizedString('RESET_GROUP_NAME').'">'.$this->GetLocalizedString('RESET_GROUP_NAME').'</button>';
        $ret_val .=  '</span></p>';
        $ret_val .= '<div id="qgFilteredGroups"></div>';
        
        return $ret_val;
        //EOC use our css button, noRiddle
	}

}

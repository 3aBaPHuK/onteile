<?php
/* -----------------------------------------------------------------------------------------
   $Id: table.php 5118 2013-07-18 10:58:36Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project (earlier name of osCommerce)
   (c) 2002-2003 osCommerce (table.php,v 1.6 2003/02/16); www.oscommerce.com 
   (c) 2003 nextcommerce (table.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 XT-Commerce

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_TABLE03_TEXT_TITLE', 'Frais de port internationales DHL');
define('MODULE_SHIPPING_TABLE03_TEXT_DESCRIPTION', 'Frais de port internationales DHL');
define('MODULE_SHIPPING_TABLE03_TEXT_WAY', 'Meilleur chemin (%01.2f kg)');
define('MODULE_SHIPPING_TABLE03_TEXT_WEIGHT', 'Poids');
define('MODULE_SHIPPING_TABLE03_TEXT_AMOUNT', 'Quantité');
define('MODULE_SHIPPING_TABLE03_UNDEFINED_RATE', 'Les frais de port ne peuvent pas être calculés pour le moment.');
define('MODULE_SHIPPING_TABLE03_INVALID_ZONE', 'Malheureusement il n\'est pas possible d\'envoyer dans ce pays');

define('MODULE_SHIPPING_TABLE03_STATUS_TITLE' , 'Activer les frais de port tabulaires');
define('MODULE_SHIPPING_TABLE03_STATUS_DESC' , 'Voulez-vous proposer les frais de port tabulaires?');
define('MODULE_SHIPPING_TABLE03_ALLOWED_TITLE' , 'Zones d\'envoi permises');
define('MODULE_SHIPPING_TABLE03_ALLOWED_DESC' , 'Veuillez indiquer les zones <b>individuellement</b> pour lesquelles l\'envoi est possible. Par exemple AT,DE');
define('MODULE_SHIPPING_TABLE03_MODE_TITLE' , 'Méthode de frais de port');
define('MODULE_SHIPPING_TABLE03_MODE_DESC' , 'Les frais de port se basent sur le total ou le poids total des articles commandés.');
define('MODULE_SHIPPING_TABLE03_TAX_CLASS_TITLE' , 'Catégorie fiscale');
define('MODULE_SHIPPING_TABLE03_TAX_CLASS_DESC' , 'Appliquer cette catégorie fiscale aux frais de port');
define('MODULE_SHIPPING_TABLE03_ZONE_TITLE' , 'Zone d\'envoi');
define('MODULE_SHIPPING_TABLE03_ZONE_DESC' , 'Si vous choisissez une zone ce type d\'envoi est seulement porposé dans cette zone.');
define('MODULE_SHIPPING_TABLE03_SORT_ORDER_TITLE' , 'Ordre de tri');
define('MODULE_SHIPPING_TABLE03_SORT_ORDER_DESC' , 'Ordre d\'affichage');
define('MODULE_SHIPPING_TABLE03_NUMBER_ZONES_TITLE' , 'Nombre de zones');
define('MODULE_SHIPPING_TABLE03_NUMBER_ZONES_DESC' , 'Nombre de zones à disposition');
define('MODULE_SHIPPING_TABLE03_DISPLAY_TITLE' , 'Activer l\'affichage');
define('MODULE_SHIPPING_TABLE03_DISPLAY_DESC' , 'Voulez-vous afficher si l\'envoi dans le pays n\'est pas possible ou si les frais de port ne peuvent pas être calculés?');

for ($module_shipping_table_i = 1; $module_shipping_table_i <= MODULE_SHIPPING_TABLE03_NUMBER_ZONES; $module_shipping_table_i ++) {
define('MODULE_SHIPPING_TABLE03_COUNTRIES_'.$module_shipping_table_i.'_TITLE' , '<hr/>Zone '.$module_shipping_table_i.' Pays');
define('MODULE_SHIPPING_TABLE03_COUNTRIES_'.$module_shipping_table_i.'_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone '.$module_shipping_table_i.' (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_TABLE03_COST_'.$module_shipping_table_i.'_TITLE' , 'Zone '.$module_shipping_table_i.' Frais de port');
define('MODULE_SHIPPING_TABLE03_COST_'.$module_shipping_table_i.'_DESC' , 'Frais de port vers la zone '.$module_shipping_table_i.' destinations,basées sur un groupe de poids de commandes maximales ou  de valeur du panier, selon les réglages du module. Exemple: 3:8.50,7:10.50,... Poids/prix de moins ou égale à 3 couterait 8.50 pour la zone'.$module_shipping_table_i.' destinations.');
define('MODULE_SHIPPING_TABLE03_HANDLING_'.$module_shipping_table_i.'_TITLE' , 'Zone '.$module_shipping_table_i.' Frais de traitement');
define('MODULE_SHIPPING_TABLE03_HANDLING_'.$module_shipping_table_i.'_DESC' , 'Frais de traitement pour cette zone d\'envoi');
  //BOC new field for shippingtime, noRiddle
define('MODULE_SHIPPING_TABLE03_SHIPPINGTIME_'.$module_shipping_table_i.'_TITLE' , 'Zone '.$module_shipping_table_i.' Temps d\'envoi');
define('MODULE_SHIPPING_TABLE03_SHIPPINGTIME_'.$module_shipping_table_i.'_DESC' , 'Veuillez indiquer le temps d\'envoi pour cette zone. Exemple: 2-3 jours');
  //EOC new field for shippingtime, noRiddle
  //BOC new flat costs depending on order_total amount, noRiddle 
define('MODULE_SHIPPING_TABLE03_FLAT_'.$module_shipping_table_i.'_TITLE' , 'Zone '.$module_shipping_table_i.' Frais forfaitaires');
define('MODULE_SHIPPING_TABLE03_FLAT_'.$module_shipping_table_i.'_DESC' , 'Les frais forfaitaires se basent sur le total des articles commandés <br />Exemple: 25:5.50,50:8.50,etc..<br /> A partir de 25 EUR 5.50 EUR sont compensés etc.');
  //EOC new flat costs depending on order_total amount, noRiddle
}
define('MODULE_SHIPPING_TABLE03_SHIPPING_INFO', 'Temps d\'envoi');
define('MODULE_SHIPPING_TABLE03_SHIPPING_INFO_UNITS', 'Jour ouvré');
define('MODULE_SHIPPING_TABLE03_SHIPPING_INFO_UNITP', 'Jours ouvrés');
?>

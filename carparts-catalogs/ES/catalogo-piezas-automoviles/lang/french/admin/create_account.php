<?php
/* --------------------------------------------------------------
   $Id: create_account.php 985 2005-06-17 22:35:22Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(create_account.php,v 1.13 2003/05/19); www.oscommerce.com 
   (c) 2003	 nextcommerce (create_account.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('NAVBAR_TITLE', 'Créer un compte');

define('HEADING_TITLE', 'Compte client administrateur');

define('TEXT_ORIGIN_LOGIN', '<span class="col-red"><small><b>ATTENTION:</b></small></span> Si vous avez déjà un compte, veuillez vous connecter<a href="%s"><u><b>ici</b></u></a> .');

define('EMAIL_SUBJECT', 'Bienvenue chez ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Monsieur ' . stripslashes($HTTP_POST_VARS['lastname']) . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Madame ' . stripslashes($HTTP_POST_VARS['lastname']) . ',' . "\n\n");
define('EMAIL_GREET_NONE', ' ' . stripslashes($HTTP_POST_VARS['firstname']) . ',' . "\n\n");
define('EMAIL_WELCOME', 'Bienvenue chez <b>' . STORE_NAME . '</b>.' . "\n\n");
define('EMAIL_TEXT', 'Vous pouvez utiliser notre <b>service en ligne</b> maintenant. Le service propose entre autres: ' . "\n\n" . '<li><b>Panier client</b> - Chaque article reste y registré jusqu‘à ce que vous allez à la caisse ou supprimer les articles du panier.' . "\n" . '<li><b>Carnet d‘adresses</b> - Nous pouvons envoyer les articles à l‘adresse choisie par vous. Le chemin parfait et l’envoi d’un cade d’anniversaire.' . "\n" . '<li><b>Commandes précédentes</b> - Vous pouvez vérifier les commandes précédentes à tout moment.' . "\n" . '<li><b>Avis sur cet article</b> - Partagez votre avis sur cet article avec d‘autres clients.' . "\n\n");
define('EMAIL_CONTACT', 'Si vous avez des questions par rapport à notre service client, contactez: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('EMAIL_WARNING', '<b>Attention:</b> Un client nous a donné cette adresse mail. Si vous n\'êtes pas connecté, envoyez un mail à ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n");
define('ENTRY_PAYMENT_UNALLOWED', 'Modules de paiement non autorisés:');
define('ENTRY_SHIPPING_UNALLOWED', 'Modules d\'expédition non autorisés:');
?>

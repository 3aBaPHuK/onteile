<?php
/***************************************************************************
* file: federal_states.php
* use: language file for module
*
* (c) 09-2018 noRiddle
***************************************************************************/

define('MODULE_SHIPPING_FEDERAL_STATES_TEXT_TITLE', 'Shipping');
define('MODULE_SHIPPING_FEDERAL_STATES_TEXT_DESCRIPTION', 'Shipping');
define('MODULE_SHIPPING_FEDERAL_STATES_TEXT_WAY', 'Best Way (%01.2f kg)');
define('MODULE_SHIPPING_FEDERAL_STATES_TEXT_WEIGHT', 'Weight');
define('MODULE_SHIPPING_FEDERAL_STATES_TEXT_AMOUNT', 'Amount');
define('MODULE_SHIPPING_FEDERAL_STATES_INVALID_ZONE', 'No shipping available to the selected region!');
define('MODULE_SHIPPING_FEDERAL_STATES_UNDEFINED_RATE', 'The shipping rate cannot be determined at this time.');

define('MODULE_SHIPPING_FEDERAL_STATES_STATUS_TITLE', 'Enable shipping costs for federal states');
define('MODULE_SHIPPING_FEDERAL_STATES_STATUS_DESC', 'Do you want to offer shipping costs for federal states?');
define('MODULE_SHIPPING_FEDERAL_STATES_ALLOWED_TITLE', 'Federal states shipping costs for the following state');
define('MODULE_SHIPPING_FEDERAL_STATES_ALLOWED_DESC', 'Indicate the ISO-Code of the state (only a single one) for which the shippimg costs for federal states shall be defined in this module (e.g. MX for Mexico)<br /> !! Don\'t leave the field empty !!');
define('MODULE_SHIPPING_FEDERAL_STATES_MODE_TITLE', 'Table Method');
define('MODULE_SHIPPING_FEDERAL_STATES_MODE_DESC', 'The shipping cost is based on the order total or the total weight of the items ordered.');
define('MODULE_SHIPPING_FEDERAL_STATES_TAX_CLASS_TITLE', 'Tax Class');
define('MODULE_SHIPPING_FEDERAL_STATES_TAX_CLASS_DESC', 'Use the following tax class on the shipping fee.');
define('MODULE_SHIPPING_FEDERAL_STATES_ZONE_TITLE', 'Shipping Zone');
define('MODULE_SHIPPING_FEDERAL_STATES_ZONE_DESC', 'If a zone is selected, only enable this shipping method for that zone.');
define('MODULE_SHIPPING_FEDERAL_STATES_SORT_ORDER_TITLE', 'Sort Order');
define('MODULE_SHIPPING_FEDERAL_STATES_SORT_ORDER_DESC', 'Sort order of display.');
define('MODULE_SHIPPING_FEDERAL_STATES_NUMBER_FS_ZONES_TITLE', 'Number of zones');
define('MODULE_SHIPPING_FEDERAL_STATES_NUMBER_FS_ZONES_DESC', 'Number of zones to use');
define('MODULE_SHIPPING_FEDERAL_STATES_DISPLAY_TITLE', 'Enable Display');
define('MODULE_SHIPPING_FEDERAL_STATES_DISPLAY_DESC', 'Do you want to display, if shipping to destination is not possible or if shipping costs cannot be calculated?');
define('MODULE_SHIPPING_FEDERAL_STATES_ERROR_NO_COUNTRY', 'No country has been indicated above with "Federal states shipping costs for the following state" !');

for ($module_shipping_table_i = 1; $module_shipping_table_i <= MODULE_SHIPPING_FEDERAL_STATES_NUMBER_FS_ZONES; $module_shipping_table_i ++) {
  define('MODULE_SHIPPING_FEDERAL_STATES_FS_STATES_'.$module_shipping_table_i.'_TITLE' , '<hr style="background:#b90000; height:3px; margin-bottom:12px;" />Federal states zone '.$module_shipping_table_i);
  define('MODULE_SHIPPING_FEDERAL_STATES_FS_STATES_'.$module_shipping_table_i.'_DESC' , 'Comma separated list of federal states codes that are part of federal states zone '.$module_shipping_table_i.'.<br />When the federal states are loaded a simple click on one of the appearing buttons will insert the federal state code below. <a class="button ld-fs-states" style="vertical-align:unset;" href="">Load federal states</a><div class="fs-code-buttons"></div>');
  define('MODULE_SHIPPING_FEDERAL_STATES_COST_'.$module_shipping_table_i.'_TITLE' , 'Federal states zone '.$module_shipping_table_i.' shipping costs');
  define('MODULE_SHIPPING_FEDERAL_STATES_COST_'.$module_shipping_table_i.'_DESC' , 'Shipping rates for federal states zone '.$module_shipping_table_i.' destinations based on a group of maximum order weights or order total. Example: 3:8.50,7:10.50,... Weights/Total less than or equal to 3 would cost 8.50 for federal states zone '.$module_shipping_table_i.' destinations.');
  define('MODULE_SHIPPING_FEDERAL_STATES_HANDLING_'.$module_shipping_table_i.'_TITLE' , 'Federal states zone '.$module_shipping_table_i.' handling fee');
  define('MODULE_SHIPPING_FEDERAL_STATES_HANDLING_'.$module_shipping_table_i.'_DESC' , 'Handling fee for this federal states zone');
  //BOC new field for shippingtime, noRiddle
  define('MODULE_SHIPPING_FEDERAL_STATES_SHIPPINGTIME_'.$module_shipping_table_i.'_TITLE' , 'Federal states zone '.$module_shipping_table_i.' delivery time');
  define('MODULE_SHIPPING_FEDERAL_STATES_SHIPPINGTIME_'.$module_shipping_table_i.'_DESC' , 'Indicate the delivery time for this federal states zone. Example: 2-3 (working day/s will be added automatically)');
  //EOC new field for shippingtime, noRiddle
  //BOC new flat costs depending on order_total amount, noRiddle 
  define('MODULE_SHIPPING_FEDERAL_STATES_FLAT_'.$module_shipping_table_i.'_TITLE' , 'Federal states zone '.$module_shipping_table_i.' flat costs');
  define('MODULE_SHIPPING_FEDERAL_STATES_FLAT_'.$module_shipping_table_i.'_DESC' , 'The flat costs base on the total amount of the order.<br />Example: 25:5.50,50:8.50,etc..<br />From 25 EUR 5.50 EUR will be charged, etc.');
  //EOC new flat costs depending on order_total amount, noRiddle
}
define('MODULE_SHIPPING_FEDERAL_STATES_SHIPPING_INFO', 'Delivery time'); //new constant for delivery time, noRiddle
define('MODULE_SHIPPING_FEDERAL_STATES_SHIPPING_INFO_UNITS', 'working day'); //new constant for delivery time, noRiddle
define('MODULE_SHIPPING_FEDERAL_STATES_SHIPPING_INFO_UNITP', 'working days'); //new constant for delivery time, noRiddle
?>
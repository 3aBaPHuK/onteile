<?php
/**************************************
* file: include_admin_css.php
* use: styles for edit gm_price_status
*      and multi_order_status
*(c) noRiddle 01-2017
**************************************/

//BOC edit price status in order
if(strpos($PHP_SELF, 'orders.php') !== false) {
?>
<link rel="stylesheet" type="text/css" href="includes/css/edit_gm_price_status.css" />
<?php
}
//EOC edit price status in order
//BOC add border to .infoBoxContent
?>
<style>
.infoBoxContent {border-bottom:1px dotted #aaa;}
.infoBoxContent label:not(.fileUpload) {display:block;}
.multi-sel {background:#d7b9b3;}
select[name="change_status_to"] {max-width:520px;}
</style>
<?php
//EOC add border to .infoBoxContent
?>
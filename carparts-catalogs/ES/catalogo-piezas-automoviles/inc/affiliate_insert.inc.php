<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_insert.inc.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_functions.php, v 1.15 2003/09/17);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

function affiliate_insert ($sql_data_array, $affiliate_parent = 0) {

	// check if affiliate_parent is allowed to have subaffiliates
	$check_sql = xtc_db_query("SELECT affiliate_tiers_allowed FROM " . TABLE_AFFILIATE . " WHERE affiliate_id = '" . $affiliate_parent . "'");
	$check = xtc_db_fetch_array($check_sql);
	if($check['affiliate_tiers_allowed'] == 0) {
		// it is not allowed
		$affiliate_parent = 0;
	}

    // LOCK TABLES
    //@mysql_query("LOCK TABLES " . TABLE_AFFILIATE . " WRITE"); //why lock tables ?, noRiddle
    if ($affiliate_parent > 0) {
    	$affiliate_root_query = xtc_db_query("select affiliate_root, affiliate_rgt, affiliate_lft from  " . TABLE_AFFILIATE . " where affiliate_id = '" . $affiliate_parent . "' ");
    	// Check if we have a parent affiliate
    	if ($affiliate_root_array = xtc_db_fetch_array($affiliate_root_query)) {
    		xtc_db_query("update " . TABLE_AFFILIATE . " SET affiliate_lft = affiliate_lft + 2 WHERE affiliate_root  =  '" . $affiliate_root_array['affiliate_root'] . "' and  affiliate_lft > "  . $affiliate_root_array['affiliate_rgt'] . "  AND affiliate_rgt >= " . $affiliate_root_array['affiliate_rgt'] . " ");
        	xtc_db_query("update " . TABLE_AFFILIATE . " SET affiliate_rgt = affiliate_rgt + 2 WHERE affiliate_root  =  '" . $affiliate_root_array['affiliate_root'] . "' and  affiliate_rgt >= "  . $affiliate_root_array['affiliate_rgt'] . "  ");
            $sql_data_array['affiliate_root'] = $affiliate_root_array['affiliate_root'];
            $sql_data_array['affiliate_lft'] = $affiliate_root_array['affiliate_rgt'];
            $sql_data_array['affiliate_rgt'] = ($affiliate_root_array['affiliate_rgt'] + 1);
            xtc_db_perform(TABLE_AFFILIATE, $sql_data_array);
            $affiliate_id = xtc_db_insert_id();
        }
        // no parent -> new root
    }
	else {
		$sql_data_array['affiliate_lft'] = '1';
		$sql_data_array['affiliate_rgt'] = '2';
		xtc_db_perform(TABLE_AFFILIATE, $sql_data_array);
		$affiliate_id = xtc_db_insert_id();
		xtc_db_query ("update " . TABLE_AFFILIATE . " set affiliate_root = '" . $affiliate_id . "' where affiliate_id = '" . $affiliate_id . "' ");
    }
    // UNLOCK TABLES
    //@mysql_query("UNLOCK TABLES");  //why lock tables ?, noRiddle
    return $affiliate_id;
}
?>

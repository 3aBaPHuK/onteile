<?php
/** 
 * -----------------------------------------------------------------------------------------
 * PDFBill NEXT by Robert Hoppe
 * Copyright 2011 Robert Hoppe - xtcm@katado.com - http://www.katado.com
 *
 * Umbau für Modified 2.0 von Ralph_84 (c) 2016
 *
 * Please visit http://pdfnext.katado.com for newer Versions
 * -----------------------------------------------------------------------------------------
 *  
 * Released under the GNU General Public License 
 * 
 */

 
// Bestellungsnummer holen
$oID = $_GET['oID'];
$last_bill = 0;

$sqlBillNr = "SELECT ibn_billnr FROM " . TABLE_ORDERS . " WHERE orders_id = '" . $oID . "';";
$resBillNr = xtc_db_query($sqlBillNr);



// get data
$rowBillNr = xtc_db_fetch_array($resBillNr);


// set new if needed
if(isset($_POST['new_billnr'])) {
    // get ibn_billnr from POST 
    $new_billnr = $_POST['new_billnr'];

    // save new ibn_billnr 
    $sqlUpOrder = "UPDATE " . TABLE_ORDERS . " SET ibn_billnr = '" . $new_billnr . "' WHERE orders_id = '" . $oID . "'";
    $resUpOrder = xtc_db_query($sqlUpOrder);

    // update last ibn_billnr        
    $sqlUpLast = "UPDATE " . TABLE_CONFIGURATION . " SET configuration_value='" . $new_billnr . "' WHERE configuration_key = 'MODULE_INVOICE_NUMBER_IBN_BILLNR'";
    $resUpLast = xtc_db_query($sqlUpLast);

    // redirec to orders
    xtc_redirect(xtc_href_link(FILENAME_ORDERS, 'page=1&oID='.(int)$oID.'&action=edit'));
}



    // Get last ibn_billnr

    $sqlLastBill = "SELECT configuration_value FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_INVOICE_NUMBER_IBN_BILLNR'";
    $resLastBill = xtc_db_query($sqlLastBill);
    $rowLastBill = xtc_db_fetch_array($resLastBill);
    $last_bill = $rowLastBill['configuration_value'];

    // check given ibn_billnr
    if(!isset($_POST['new_billnr'])) {
        $new_billnr = $last_bill + 1;

    } 

?>
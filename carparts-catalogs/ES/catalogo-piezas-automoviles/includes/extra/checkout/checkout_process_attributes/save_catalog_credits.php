<?php
/****************************************************************************************
* file: save_catalog_credits.php
* use: if catalog_credits for VIN search were ordered update credits in table customers
*
* (c) noRiddle 04-2018
****************************************************************************************/

//BOC set catalog_credit for VIN if products_id = 1 (product must have this ID !!) and options_id = '1' (must be 1 !!!), noRiddle
if($bought_vin_credit === true && (isset($sql_data_array['orders_products_options_id']) && $sql_data_array['orders_products_options_id'] == '1')) {
    if(isset($sql_data_array['orders_products_options_values_id'])) {
        switch($sql_data_array['orders_products_options_values_id']) {
            case '1':
                $bought_how_many_credits = 10;
              break;
            case '2':
                $bought_how_many_credits = 100;
              break;
            case '3':
                $bought_how_many_credits = 1000;
              break;
            default:
                $bought_how_many_credits = 0;
              break;
        }
        
        if((isset($bought_vin_credit) && $bought_vin_credit === true) && $bought_how_many_credits > 0) {
            $bought_how_many_credits_total = $bought_how_many_credits * $bought_qty_credits; //var for qty defined in /includes/etra/checkout/checkout_process_products/save_catalog_credits.php
            $bought_vincred_qu_str = ("UPDATE ".TABLE_CUSTOMERS." SET catalog_credit = catalog_credit + ".(int)$bought_how_many_credits_total." WHERE customers_id = ".(int)$_SESSION['customer_id']);
            if(xtc_db_query($bought_vincred_qu_str)) {
                $so_bought_vin_credits = $bought_how_many_credits_total;
            }
        }
    }
}
?>
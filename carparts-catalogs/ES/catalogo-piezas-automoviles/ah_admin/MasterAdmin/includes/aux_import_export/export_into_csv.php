<?php
/*******************************************************************************************
* routine to reload browser while exporting data into csv
* file: /export_into_csv.php
* (c) noRiddle 12-2015
*******************************************************************************************/

class admin_tools_export {

    //function admin_tools_export() {
    function __construct($exp_file_name) {
        global $shp_sfx;

        if(!isset($_GET['step_exp'])) {
            if($_GET['sel_prod'] == 'sp') {
                $count_entries_sql = xtc_db_query("SELECT count(products_id) as total FROM ".TABLE_PRODUCTS." WHERE products_startpage > 0");
                //$exp_file_name = 'product_startpage_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                //$exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'product_startpage_export'.$shp_sfx.'.csv';
            } else if($_GET['sel_prod'] == 'gpsn') {
                $count_entries_sql = xtc_db_query("SELECT count(products_id) as total FROM ".TABLE_PRODUCTS." WHERE gm_price_status = 0");
                //$exp_file_name = 'product_gm_normal_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                //$exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'product_gm_normal_export'.$shp_sfx.'.csv';
            } else if($_GET['sel_prod'] == 'gpsor') {
                $count_entries_sql = xtc_db_query("SELECT count(products_id) as total FROM ".TABLE_PRODUCTS." WHERE gm_price_status = 1");
                //$exp_file_name = 'product_gm_onrequest_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                //$exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'product_gm_onrequest_export'.$shp_sfx.'.csv';
            } else if($_GET['sel_prod'] == 'gpsnp') {
                $count_entries_sql = xtc_db_query("SELECT count(products_id) as total FROM ".TABLE_PRODUCTS." WHERE gm_price_status = 2");
                //$exp_file_name = 'product_notforsale_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                //$exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'product_notforsale_export'.$shp_sfx.'.csv';
            } else if($_GET['sel_prod'] == 'p') {
                $count_entries_sql = xtc_db_query("SELECT count(products_id) as total FROM ".TABLE_PRODUCTS);
                //$exp_file_name = 'product_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                //$exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'product_export'.$shp_sfx.'.csv';
            } else if($_GET['sel_prod'] == 'sba') { //new set_by_admin //added 30.06.2016
                $count_entries_sql = xtc_db_query("SELECT count(products_id) as total FROM ".TABLE_PRODUCTS." WHERE set_by_admin != ''");
                //$exp_file_name = 'set_by_admin_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                //$exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'set_by_admin_export'.$shp_sfx.'.csv';
            } else if($_GET['sel_prod'] == 'sbk') { //new products_sbk //added 04.05.2020
                $count_entries_sql = xtc_db_query("SELECT count(products_id) as total FROM ".TABLE_PRODUCTS." WHERE products_sbk != ''");
            } else if($_GET['sel_prod'] == 'blckf') { //new for blackforest //added 09-2019
                $count_entries_sql = xtc_db_query("SELECT count(products_id) as total FROM ".TABLE_PRODUCTS." WHERE bf_prod_model != '' AND products_supplier_id = 100");
            }
            $count_entries = xtc_db_fetch_array($count_entries_sql);
            $count_entries = $count_entries['total'];
            if($count_entries > 0) {
                if($count_entries >= IE_EXPORT_FACTOR) {
                    $counts = ceil($count_entries/IE_EXPORT_FACTOR); //IE_EXPORT_FACTOR defined in config in /admin/master_admin.php | correct: defined in table configuration
                    //$counts = 2; //for tests
                } else {
                    $counts = (int)1;
                }
            } else {
                $counts = (int)0;
                $_SESSION[$_SESSION['ma_shopsess'].'expout'] = '<span style="color:#c00;">'.ADMIN_TOOLS_NO_PRODUCTS.'</span>';
            }
            $_GET['counts'] = $counts; //put $counts into get var in order to no have to execute the select count() on each reload
            $_GET['filename'] = $exp_file_name; //store in GET, without filename will be lost for fopen()
            
            unset($_SESSION[$_SESSION['ma_shopsess'].'mom_date']); //unset the session withn file date
            unset($_SESSION[$_SESSION['ma_shopsess'].'exp_file_name']); //unset filename session, we'll have that now in GET
        }
        //echo '<pre style="font-size:28px;">counts = '.$counts.'</pre>';
        //echo '<pre style="font-size:28px;">count_entries = '.$count_entries.'</pre>';
        
        $this->exp_file_name = $_GET['filename']; //$exp_file_name;
        $this->counts = $_GET['counts'];
        $this->fp = $this->open_file();
        $this->langs = $this->get_langs();
        $this->ie_personal_offers_array = $this->ie_personal_offers_array();
    }

    //BOC functions
    function write_csv() {
        global $country_envir; //added 09-2019
        $langs = $this->langs;
        $sizeof_langs = sizeof($langs);
    
        if($this->counts > 0) {
            $this->steps(0); //set first step
            
            for($i = 0; $i < $this->counts; $i++) {
                if(isset($_GET['step_exp']) && $_GET['step_exp'] == $i) {
                    $exp_start_time = microtime(true); //start time
                    if($i == 0) {
                        $_SESSION[$_SESSION['ma_shopsess'].'tot_expprod'] = 0; //set session for total amount of products
                        
                        //BOC get blackforest categories and write them into session, added 09-2019
                        if($_GET['sel_prod'] == 'blckf') {
                            $which_main_cats_array = array();
                            $which_main_cats_array[] = (int)NR_CLIST_MAIN_CAT; //include main cat ???
                            $which_main_cats = xtc_db_query("SELECT categories_id FROM categories WHERE parent_id = ".(int)NR_CLIST_MAIN_CAT);
                            while($which_main_cats_arr = xtc_db_fetch_array($which_main_cats)) {
                                $which_main_cats_array[] = $which_main_cats_arr['categories_id'];
                            }
                            $which_main_cats_str = implode(',', $which_main_cats_array);
                            
                            $which_submain_cats_array = array();
                            $which_submain_cats = xtc_db_query("SELECT categories_id FROM ".TABLE_CATEGORIES." WHERE parent_id IN(".$which_main_cats_str.")");
                            while($which_submain_cats_arr = xtc_db_fetch_array($which_submain_cats)) {
                                $which_submain_cats_array[] = $which_submain_cats_arr['categories_id'];
                            }
                            $which_submain_cats_str = implode(',', $which_submain_cats_array);
                            $which_all_bf_cats_str = $which_main_cats_str.','.$which_submain_cats_str;
                            
                            $_SESSION[$_SESSION['ma_shopsess'].'blckf_cats'] = $which_all_bf_cats_str;
                        }
                        //EOC get blackforest categories and write them into session, added 09-2019
                    }
                    
                    $this->fp; //open file

                    //BOC fields to select from table products
                    //$select_string_prod = 'products_id, products_model, products_manufacturers_model, products_ean, products_quantity, products_sort, products_startpage, products_startpage_sort, products_shippingtime, manufacturers_id, products_tax_class_id, products_status, gm_price_status, gm_min_order, gm_graduated_qty, products_weight, products_discount_allowed, products_price, products_dealer_price, products_image';
                    
                    //BOC define vars
                    $select_str_prod = '';
                    $get_selprod = $_GET['sel_prod'];
                    //EOC define vars
                    $select_str = "SHOW COLUMNS FROM csv_import_aux 
                                               WHERE FIELD != 'import_id'
                                                 AND FIELD NOT LIKE 'free_field\_%'
                                                 AND FIELD NOT LIKE 'personal_offers_by_customers_status\_%'
                                                 AND FIELD NOT LIKE 'group_permission\_%' 
                                                 AND FIELD NOT LIKE 'products_name\_%' 
                                                 AND FIELD NOT LIKE 'products_description\_%' 
                                                 AND FIELD NOT LIKE 'products_short_description\_%' 
                                                 AND FIELD NOT LIKE 'cat\_%'"; //escaping of _, added 09-2019
                    
                    $select_str_query = xtc_db_query($select_str);
                    
                    while($select_str_arr = xtc_db_fetch_array($select_str_query)) {
                        //BOC rename fields which are named differently in table products new vpe fields)
                        if($select_str_arr['Field'] == 'vpe_status') {
                            $needed_field = 'products_vpe_status';
                        } else if($select_str_arr['Field'] == 'vpe') {
                            $needed_field = 'products_vpe';
                        } else if($select_str_arr['Field'] == 'vpe_value') {
                            $needed_field = 'products_vpe_value';
                        } else if($select_str_arr['Field'] == 'bf_prod_model') {  //blackforest filter added 09-2019
                            $needed_field = $country_envir != 'bf' ? "CONCAT('NOT', '', 'BF') AS bf_prod_model" : 'bf_prod_model';
                        } else {
                            $needed_field = $select_str_arr['Field'];
                        }
                        //EOC rename fields which are named differently in table products new vpe fields)
                        
                        //$select_str_prod .= $select_str_arr['Field'].',';
                        $select_str_prod .= $needed_field.',';
                    }
                    $select_str_prod = rtrim($select_str_prod, ',');
                    
                    switch ($get_selprod) {
                        case 'sp': //startpage
                            $prod_export_query = xtc_db_query("SELECT products_id,".$select_str_prod."
                                                                 FROM ".TABLE_PRODUCTS." 
                                                                WHERE products_startpage = 1 
                                                             ORDER BY products_id ASC 
                                                                LIMIT ".($i * IE_EXPORT_FACTOR).", ".IE_EXPORT_FACTOR);
                        break;
                        case 'gpsn': //gm_price_status_normal
                            $prod_export_query = xtc_db_query("SELECT products_id,".$select_str_prod." 
                                                                 FROM ".TABLE_PRODUCTS." 
                                                                WHERE gm_price_status = 0 
                                                             ORDER BY products_id ASC 
                                                                LIMIT ".($i * IE_EXPORT_FACTOR).", ".IE_EXPORT_FACTOR);
                        break;
                        case 'gpsor': //gm_price_status_onrequ
                            $prod_export_query = xtc_db_query("SELECT products_id,".$select_str_prod." 
                                                                 FROM ".TABLE_PRODUCTS." 
                                                                WHERE gm_price_status = 1 
                                                             ORDER BY products_id ASC 
                                                                LIMIT ".($i * IE_EXPORT_FACTOR).", ".IE_EXPORT_FACTOR);
                        break;
                        case 'gpsnp': //gm_price_status_np
                            $prod_export_query = xtc_db_query("SELECT products_id,".$select_str_prod." 
                                                                 FROM ".TABLE_PRODUCTS." 
                                                                WHERE gm_price_status = 2 
                                                             ORDER BY products_id ASC 
                                                                LIMIT ".($i * IE_EXPORT_FACTOR).", ".IE_EXPORT_FACTOR);
                        break;
                        case 'p': //products
                            $prod_export_query = xtc_db_query("SELECT products_id,".$select_str_prod." 
                                                                 FROM ".TABLE_PRODUCTS." 
                                                                 FORCE INDEX FOR ORDER BY (PRIMARY) 
                                                             ORDER BY products_id ASC 
                                                                LIMIT ".($i * IE_EXPORT_FACTOR).", ".IE_EXPORT_FACTOR);
                        break;
                        case 'sba': //new set_by_admin //added 30.06.2016 // !! revoked, extra routine below !! added products_sbk (new field for "Sonderbeschaffungskosten", 11-2019, noRiddle
                            $prod_export_query = xtc_db_query("SELECT products_id,".$select_str_prod." 
                                                                 FROM ".TABLE_PRODUCTS." 
                                                                WHERE set_by_admin != ''
                                                             ORDER BY products_id ASC 
                                                                LIMIT ".($i * IE_EXPORT_FACTOR).", ".IE_EXPORT_FACTOR);
                        break;
                        case 'sbk': //added products_sbk (new field for "Sonderbeschaffungskosten", 05-2020, noRiddle
                            $prod_export_query = xtc_db_query("SELECT products_id,".$select_str_prod." 
                                                                 FROM ".TABLE_PRODUCTS." 
                                                                WHERE products_sbk != ''
                                                             ORDER BY products_id ASC 
                                                                LIMIT ".($i * IE_EXPORT_FACTOR).", ".IE_EXPORT_FACTOR);
                        break;
                        case 'blckf': //new for blackforest parts, added 09-2019
                            $prod_export_query = xtc_db_query("SELECT products_id,".$select_str_prod." 
                                                                 FROM ".TABLE_PRODUCTS." 
                                                                WHERE bf_prod_model != '' 
                                                                  AND products_supplier_id = 100
                                                             ORDER BY products_id ASC 
                                                                LIMIT ".($i * IE_EXPORT_FACTOR).", ".IE_EXPORT_FACTOR);
                        break;
                    }
                    //EOC fields to select from table products
                    
                    if($i == ($this->counts - 1)) $db_num_rows = xtc_db_num_rows($prod_export_query); //store no. of results for last loop in var
                    
                    while($prod_arr = xtc_db_fetch_array($prod_export_query)) {
                        if($prod_arr['bf_prod_model'] == 'NOTBF') {
                            $prod_arr['bf_prod_model'] = '';
                        }
                        
                        //BOC no prices for startpage and set_by_admin, 05-2020, noRiddle
                        if($get_selprod == 'sp' || $get_selprod == 'sba') {
                            $prod_arr['products_price'] = '';
                            $prod_arr['products_dealer_price'] = '';
                            $prod_arr['products_bulk'] = '';
                            $prod_arr['products_deposit'] = '';
                            $prod_arr['products_sbk'] = '';
                        }
                        //EOC no prices for startpage and set_by_admin, 05-2020, noRiddle

                        //BOC Don't fill products_image field in CSV here in export but process it in import depending on there defined conditions, 04-2021, noRiddle
                        $prod_arr['products_image'] = '';
                        //EOC Don't fill products_image field in CSV here in export but process it in import depending on there defined conditions, 04-2021, noRiddle

                        //BOC more images | we don't use more images any more, just for e.g. start page products loaded via backend of shop, 04-2021, noRiddle
                        //BOC we use just the number of pics in field products_image, added or changed 09-2019 | revoked 04-2021, noRiddle
                        /*for($ap = 1; $ap <= IE_NUM_OF_ADDI_PICS; $ap++) {
                            $imgs_query = xtc_db_query("SELECT image_name
                                                         FROM ".TABLE_PRODUCTS_IMAGES."
                                                        WHERE products_id = ".(int)$prod_arr['products_id']."
                                                          AND image_nr = ".$ap);
                            if(xtc_db_num_rows($imgs_query) == 1) {
                                $imgs = xtc_db_fetch_array($imgs_query);
                                $prod_arr = $prod_arr += array('image_name_'.$ap => $imgs['image_name']);
                            }
                        }*/

                        /*
                        $imgs_query = xtc_db_query("SELECT COUNT(*) AS noofaddpics
                                                      FROM ".TABLE_PRODUCTS_IMAGES."
                                                     WHERE products_id = ".(int)$prod_arr['products_id']);
                        $imgs = xtc_db_fetch_array($imgs_query);
                        $overwritten_prodimg = $prod_arr['products_image'] != '' ? (1 + (int)$imgs['noofaddpics']) : '';
                        $prod_arr['products_image'] = ''; //$overwritten_prodimg; //we don't handle images for the time being, 10-2020, noRiddle
                        */
                        //EOC we use just the number of pics in field products_image, added or changed 09-2019 | revoked 04-2021, noRiddle
                        //EOC more images

                        //BOC we have 3 new free fields which will be used later, added or changed 09-2019
                        //$prod_arr['free_field_1'] = ''; // we renamed that to products_sbk fpr "sonderbeschaffungskosten", 10-2019, noRiddle
                        $prod_arr['free_field_2'] = '';
                        $prod_arr['free_field_3'] = '';
                        //EOC we have 3 new free fileds which will be used later, added or changed 09-2019

                        //BOC fields for personal_offers
                        foreach($this->ie_personal_offers_array as $po) {
                            $price_query = xtc_db_query("SELECT CONCAT_WS(':', quantity, personal_offer) AS combined
                                                           FROM ".$po."
                                                          WHERE products_id = ".(int)$prod_arr['products_id']."
                                                            AND personal_offer > 0
                                                          ORDER BY quantity");
                            $groupPrice = '';
                            while ($price_data = xtc_db_fetch_array($price_query)) {
                                $groupPrice .= $price_data['combined'].'::';
                            }
                            $groupPrice = ($get_selprod != 'sp' && $get_selprod != 'sba') ? rtrim($groupPrice, '::') : ''; //no prices for startpage and set_by_admin, 05-2020, noRiddle
                            $prod_arr = $prod_arr += array('personal_offers_by_customers_status_'.$po => $groupPrice);
                        }
                        //EOC fields for personal_offers
                        
                        //BOC group permissons
                        $groups_arr = $this->get_group_perm($prod_arr['products_id']);
                        $prod_arr = $prod_arr += $groups_arr;
                        //EOC group permissons
                        
                        //BOC products description
                        for($l = 1; $l <= $sizeof_langs; $l++) {
                            if(strpos($langs[$l]['code'], '_') !== false) {
                                $langs_code_arr = explode('_', $langs[$l]['code']);
                                $langs_l_code = $langs_code_arr[0];
                            } else {
                                $langs_l_code = $langs[$l]['code'];
                            }

                            if($langs[$l]['id'] != '') {
                                $prod_name_query = xtc_db_query("SELECT products_name
                                                                   FROM ".TABLE_PRODUCTS_DESCRIPTION."
                                                                  WHERE products_id = ".(int)$prod_arr['products_id']."
                                                                    AND language_id = ".(int)$langs[$l]['id']);

                                if(xtc_db_num_rows($prod_name_query) == 1) {
                                    $prod_name = xtc_db_fetch_array($prod_name_query);
                                    $prod_arr = $prod_arr += array('products_name_'.$langs_l_code => str_replace(array("\n", "\r", chr(13)), '', $prod_name['products_name']));
                                } else {
                                    $prod_arr = $prod_arr += array('products_name_'.$langs_l_code => '');
                                }
                            } else {
                                $prod_arr = $prod_arr += array('products_name_'.$langs_l_code => '');
                            }
                        }
                        for($l = 1; $l <= $sizeof_langs; $l++) {
                            if(strpos($langs[$l]['code'], '_') !== false) {
                                $langs_code_arr = explode('_', $langs[$l]['code']);
                                $langs_l_code = $langs_code_arr[0];
                            } else {
                                $langs_l_code = $langs[$l]['code'];
                            }

                            if($langs[$l]['id'] != '') {
                                $prod_desc_query = xtc_db_query("SELECT products_description
                                                                   FROM ".TABLE_PRODUCTS_DESCRIPTION."
                                                                  WHERE products_id = ".(int)$prod_arr['products_id']."
                                                                    AND language_id = ".(int)$langs[$l]['id']);

                                if(xtc_db_num_rows($prod_desc_query) == 1) {
                                    $prod_desc = xtc_db_fetch_array($prod_desc_query);
                                    $prod_arr = $prod_arr += array('products_description_'.$langs_l_code => str_replace(array("\n", "\r", chr(13)), '', $prod_desc['products_description']));
                                } else {
                                    $prod_arr = $prod_arr += array('products_description_'.$langs_l_code => '');
                                }
                            } else {
                                $prod_arr = $prod_arr += array('products_description_'.$langs_l_code => '');
                            }
                        }
                        for($l = 1; $l <= $sizeof_langs; $l++) {
                            if(strpos($langs[$l]['code'], '_') !== false) {
                                $langs_code_arr = explode('_', $langs[$l]['code']);
                                $langs_l_code = $langs_code_arr[0];
                            } else {
                                $langs_l_code = $langs[$l]['code'];
                            }

                            if($langs[$l]['id'] != '') {
                                $prod_shdesc_query = xtc_db_query("SELECT products_short_description
                                                                   FROM ".TABLE_PRODUCTS_DESCRIPTION."
                                                                  WHERE products_id = ".(int)$prod_arr['products_id']."
                                                                    AND language_id = ".(int)$langs[$l]['id']);

                                if(xtc_db_num_rows($prod_shdesc_query) == 1) {
                                    $prod_shdesc = xtc_db_fetch_array($prod_shdesc_query);
                                    $prod_arr = $prod_arr += array('products_short_description_'.$langs_l_code => str_replace(array("\n", "\r", chr(13)), '', $prod_shdesc['products_short_description']));
                                } else {
                                    $prod_arr = $prod_arr += array('products_short_description_'.$langs_l_code => '');
                                }
                            } else {
                                $prod_arr = $prod_arr += array('products_short_description_'.$langs_l_code => '');
                            }
                        }
                        //EOC products description
                        
                        //BOC cats
                        if($get_selprod != 'blckf') { //difference from blackforest export, added or changed 09-2019
                            //$cat_query = xtc_db_query("SELECT categories_id FROM ".TABLE_PRODUCTS_TO_CATEGORIES." WHERE products_id = '".$prod_arr['products_id']."'");
                            $cat_query = xtc_db_query("SELECT MIN(categories_id) AS categories_id FROM ".TABLE_PRODUCTS_TO_CATEGORIES." WHERE products_id = ".(int)$prod_arr['products_id']); //catch only 1 cat
                            $cat = xtc_db_fetch_array($cat_query);
                            $relevant_catid = $cat['categories_id'];
                        } else {
                            //blackforest has got some products in more than one category, added or changed 09-2019
                            $cat_query = xtc_db_query("SELECT categories_id FROM ".TABLE_PRODUCTS_TO_CATEGORIES." WHERE products_id = ".(int)$prod_arr['products_id']." AND categories_id IN(".$_SESSION[$_SESSION['ma_shopsess'].'blckf_cats'].")");
                            if(xtc_db_num_rows($cat_query) > 0) {
                                if(xtc_db_num_rows($cat_query) > 1) { //we do that just in case we will want to have all entries in CSV (products in more than one category)
                                    $more_than_one_blckf_cat = xtc_db_num_rows($cat_query);
                                    $found_cats_arr = array();

                                    while($cats_array = xtc_db_fetch_array($cat_query)) {
                                        $found_cats_arr[] = $cats_array['categories_id'];
                                    }

                                    $relevant_catid = $found_cats_arr[0];
                                } else {
                                    $cat = xtc_db_fetch_array($cat_query);
                                    $relevant_catid = $cat['categories_id'];
                                }
                            } else {
                                $relevant_catid = $which_main_cats_array[0]; //which should be NR_CLIST_MAIN_CAT, what else could we do, no other choice
                            }
                        } //END difference from blackforest export, added or changed 09-2019

                        if($relevant_catid != '0') { //don't do anything here if categories_id = 0 (TOP)
                            $all_cats = explode(',', $this->get_all_cat($relevant_catid));
                            //sort($all_cats); //wrong get the elements in reverse order, don't sort() or natsort() which is nonsense
                            $all_cats = array_reverse($all_cats); //
                            
                            //echo '<pre><b>'.$relevant_catid.'</b></pre>';
                            //echo '<pre>'.print_r($all_cats, true).'</pre>'; exit();
                            //echo '<pre>'.print_r($prod_arr, true).'</pre>'; exit();
                            
                            for($pc = 0, $cpc = count($all_cats); $pc < $cpc; $pc++) {
                                for($lc = 1; $lc <= $sizeof_langs; $lc++) {
                                    if(strpos($langs[$lc]['code'], '_') !== false) {
                                        $langs_code_arr = explode('_', $langs[$lc]['code']);
                                        $langs_lc_code = $langs_code_arr[0];
                                    } else {
                                        $langs_lc_code = $langs[$lc]['code'];
                                    }

                                    if($langs[$lc]['id'] != '') {
                                        if($all_cats[$pc] != 'X') { //see method get_all_cat() below, 11-08-2018, noRiddle
                                            $cat_desc_query = xtc_db_query("SELECT categories_name
                                                                              FROM ".TABLE_CATEGORIES_DESCRIPTION."
                                                                             WHERE categories_id = ".(int)$all_cats[$pc]."
                                                                               AND language_id = ".(int)$langs[$lc]['id']);
                                            if(xtc_db_num_rows($cat_desc_query) == 1) {
                                                $cat_desc = xtc_db_fetch_array($cat_desc_query);
                                                $prod_arr = $prod_arr += array('cat_'.($pc+1).'_'.$langs_lc_code => $cat_desc['categories_name']);
                                                //echo '<pre>'.print_r($cat_desc, true).'</pre>';
                                            } else {
                                                $prod_arr = $prod_arr += array('cat_'.($pc+1).'_'.$langs_lc_code => '');
                                            }
                                        } else {
                                            if($get_selprod != 'blckf') {
                                                $prod_arr = $prod_arr += array('cat_'.($pc+1).'_'.$langs_lc_code => 'CATEGORY MISSING !!!');
                                            } else {
                                                $prod_arr = $prod_arr += array('cat_'.($pc+1).'_'.$langs_lc_code => 'Blackforest Specials');
                                            } //different handling with blackforest, added or changed 09-2019
                                            //echo '<pre>'.print_r($all_cats, true).'</pre>'; exit();
                                        }
                                    } else {
                                        $prod_arr = $prod_arr += array('cat_'.($pc+1).'_'.$langs_lc_code => '');
                                    }
                                }
                            }
                        } else { //write TOP into cat_1 with warning
                            for($lc = 1; $lc <= $sizeof_langs; $lc++) {
                                if(strpos($langs[$lc]['code'], '_') !== false) {
                                    $langs_code_arr = explode('_', $langs[$lc]['code']);
                                    $langs_lc_code = $langs_code_arr[0];
                                } else {
                                    $langs_lc_code = $langs[$lc]['code'];
                                }

                                $prod_arr = $prod_arr += array('cat_1_'.$langs_lc_code => 'TOP, nicht okay !!!');
                            }
                        }
                        //EOC cats
                        
                        //echo '<pre style="margin-top:100px;">'.print_r($prod_arr, true).'</pre>';
                        $this->insertProds($prod_arr);

                        
                    }
                    //BOC csv
                    fclose($this->fp);
                    //EOC csv

                    //BOC time calculation
                    $exp_end_time = microtime(true);
                    $time_for_one_loop = ($exp_end_time - $exp_start_time);

                    $_SESSION[$_SESSION['ma_shopsess'].'expout'] = '<span style="color:#0A7E00;">'.sprintf(MOD_CSV_TIME_ONE_LOOP_TXT, ($i == ($this->counts - 1) ? $db_num_rows : IE_EXPORT_FACTOR), number_format($time_for_one_loop, 4)).' sec.</span>';
                    $_SESSION[$_SESSION['ma_shopsess'].'tot_expprod'] += ($i == ($this->counts - 1) ? $db_num_rows : IE_EXPORT_FACTOR);
                    if($i == ($this->counts - 1)) {
                        unset($_SESSION[$_SESSION['ma_shopsess'].'blckf_cats']); //unset session with blackforest cats string, added 09-2019

                        $_SESSION[$_SESSION['ma_shopsess'].'expout'] .= '<br /><span style="color:#0A7E00; font-weight:bold;">READY !! | '.sprintf(MOD_CSV_TOT_PRODUCTS_EXPORTED, number_format($_SESSION[$_SESSION['ma_shopsess'].'tot_expprod'])).'</span>';
                    }
                    
                    $_SESSION[$_SESSION['ma_shopsess'].'exptot_time'] += number_format(($time_for_one_loop + 3), 4); //added waiting time for reload
                    //EOC time calculation

                    $_SESSION[$_SESSION['ma_shopsess'].'expout'] .= '<br />--|-- TOTAL elapsed time: '.number_format($_SESSION[$_SESSION['ma_shopsess'].'exptot_time'], 4).' sec. --|--';

                    if($_GET['step_exp'] < ($this->counts - 1)) {$this->steps($i + 1);} //set next step

                    if($_GET['step_exp'] == ($this->counts - 1) || $this->counts == 0) {
                        unset($_SESSION[$_SESSION['ma_shopsess'].'exptot_time']);
                        unset($_SESSION[$_SESSION['ma_shopsess'].'tot_expprod']);
                    }
                }
            }
        }
    }

    function insertProds($prod_aux_arr) {
        unset($prod_aux_arr['products_id']);
        $prod_aux_arr['manufacturers_id'] = $this->get_manufacurers_name($prod_aux_arr['manufacturers_id']);
        //echo '<pre>'.print_r($prod_aux_arr, true).'</pre>';
        
        //WRITE DIRECTLY INTO CSV
        //global $fp;
        if(CSV_TEXTSIGN != '') {
            $line = CSV_TEXTSIGN.implode(CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN, $prod_aux_arr).CSV_TEXTSIGN; //e.g. ^entry1^|^entry2^|^entry3^
        } else {
            $line = implode(CSV_SEPERATOR, $prod_aux_arr);
        }
        fwrite($this->fp, $line."\r\n");
    }
    
    function open_file() {
        $fp = fopen(DIR_FS_CATALOG.'export/'.$this->exp_file_name, "a+");
        return $fp;
    }

    function get_all_cat($catid) {
        $parentid_str = $catid;

        $cat_parent_query = xtc_db_query("SELECT parent_id FROM ".TABLE_CATEGORIES." WHERE categories_id = ".(int)$catid);

        if(xtc_db_num_rows($cat_parent_query)) {
            $cat_parent = xtc_db_fetch_array($cat_parent_query);

            if($cat_parent['parent_id'] == '0') {
                $parentid_str .= '';
            } else {
                $parentid_str .= ','.$this->get_all_cat($cat_parent['parent_id']);
            }
        } else {
            $parentid_str .= ',X'; //mark if parent categorie doesn't exists, 11-08-2018, noRiddle
        }

        return $parentid_str;
    }

    function get_manufacurers_name($manf_id) {
        if(empty($manf_id)) {
            return;
        } else {
            $mfn_query = xtc_db_query("SELECT manufacturers_name FROM ".TABLE_MANUFACTURERS." WHERE manufacturers_id = ".(int)$manf_id);
            $mfn = xtc_db_fetch_array($mfn_query);
            $man_name = $mfn['manufacturers_name'];

            return $man_name;
        }
    }
    
    function ie_personal_offers_array() {
        $pers_off_col_query = xtc_db_query("SHOW COLUMNS FROM csv_import_aux LIKE 'personal_offers_by_customers_status_%'");
        while($pers_off_cols = xtc_db_fetch_array($pers_off_col_query)) {
            $ie_personal_offers_array[] = $pers_off_cols['Field'];
        }
        return $ie_personal_offers_array;
    }
    
    function get_group_perm($prodid) {
        $groups_perm_arr = array();
        $groups_arr = array();
        $group_perm_query = xtc_db_query("SHOW COLUMNS FROM csv_import_aux LIKE 'group_permission_%'");
        while($group_perms = xtc_db_fetch_array($group_perm_query)) {
            $groups_perm_arr[] = $group_perms['Field'];
        }
        
        $group_perms_str = implode(',', $groups_perm_arr);
        $group_query = xtc_db_query("SELECT ".$group_perms_str." 
                                       FROM ".TABLE_PRODUCTS." 
                                      WHERE products_id = ".(int)$prodid);
        $groups = xtc_db_fetch_array($group_query);
        for($gp = 0, $cgp = count($groups_perm_arr); $gp < $cgp; $gp++) {
            $groups_arr = array_merge($groups_arr, array('group_permission_'.$gp => $groups['group_permission_'.$gp]));
        }
        return $groups_arr;
    }

    function get_langs() {
        $all_langs_sort = array('1' => 'de', '2' => 'en', '3' => 'es', '4' => 'fr', '5' => 'it'); //5 languages to be able to use same CSV for every shop, no matter how many and which languages it has got
        $languages_array = array();
        $languages_query = xtc_db_query("SELECT languages_id, code, ie_sort_order FROM ".TABLE_LANGUAGES." ORDER BY ie_sort_order");
        while ($languages = xtc_db_fetch_array($languages_query)) {
            $languages_array[$languages['ie_sort_order']] = array('id' => $languages['languages_id'], 'code' => $languages['code']);
        }

        foreach($all_langs_sort as $sort => $l_code) { //catch no existing languages
            if(!array_key_exists($sort, $languages_array)) {
                $languages_array[$sort] = array('id' => '', 'code' => $l_code);
            }
        }
        ksort($languages_array);

        return $languages_array;
    }

    function steps($step) {
        if(!isset($_GET['step_exp']) || (isset($_GET['step_exp']) && $_GET['step_exp'] != $step && $_GET['step_exp'] < $step)) { // 
            //echo '<meta http-equiv="refresh" content="3; URL='.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=export&sel_prod='.$_GET['sel_prod'].'&filename='.$this->exp_file_name.'&counts='.$this->counts.'&step_exp='.$step).'">';
            $this->ie_meta_out = '<meta http-equiv="refresh" content="2; URL='.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=export&sel_prod='.$_GET['sel_prod'].'&filename='.$this->exp_file_name.'&counts='.$this->counts.'&step_exp='.$step).'">';
        } else {
            $this->ie_meta_out = false;
        }
        
        return $this->ie_meta_out;
    }
    //EOC functions
    
} //end class
?>
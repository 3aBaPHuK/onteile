<?php
/**************************************************
* sitemap creator per ajax
* (c) Mario Maeles and noRiddle 07-2015
**************************************************/
include_once 'includes/application_top.php';
include_once 'create_sitemap_function.php';
require_once 'inc/seo_url_href_mask.php';

define('SITEMAP_CATALOG', HTTP_SERVER.DIR_WS_CATALOG); //define path to shop, noRiddle

function iso8601_date($timestamp) { 
    if (PHP_VERSION < 5) { 
        $tzd = date('O',$timestamp); 
        $tzd = substr(chunk_split($tzd, 3, ':'),0,6); 
        return date('Y-m-d\TH:i:s', $timestamp) . $tzd; 
    } else { 
        return date('c', $timestamp); 
    } 
} 

$code_entry = '';
$index_code_entry = '';
$changefreq = 'weekly';

$datei = 'sitemap/sitemap'.str_pad(($_POST['file_counter']), 3, '0', STR_PAD_LEFT).'.xml';
$index_datei = 'sitemap/sitemap_index.xml';
$head = '<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
$index_head = '<?xml version="1.0" encoding="UTF-8"?>'."\n".'<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">'."\n"; //for sitemap_index, noRiddle
$footer = '</urlset>';
$index_footer_entry = '</sitemapindex>'; //for sitemap_index, noRiddle

//BOC measure time, noRiddle
$conts_time_start = 0;
$conts_time_end = 0;
//EOC measure time, noRiddle

if(isset($_POST['file_counter']) && isset($_POST['lines_to_create'])) {
    if($_POST['file_counter'] == 1) {
        if(file_exists($index_datei)) unlink($index_datei);
        file_put_contents($index_datei, $index_head);
    }

    $conts_time_start += microtime(true); //measure time, noRiddle
    $linePerFile = $_POST['lines_to_create'];
    $post_filecount = $_POST['file_counter'];
    $line_count_start = $post_filecount == 1 ? '' : ($post_filecount -1) * $linePerFile;
    $line_count_end = $linePerFile;
    $line_count_total = $line_count_start + $linePerFile;
    $entries = get_entries($line_count_start, $line_count_end);

    $code_entry .= $head."\n";

    foreach ($entries as $key => $value) {
        //if($value['type'] == 'product') {
        if($value['type'] == 'content' && $value['lang'] != '') {
            $lang_param = $value['lang'] != DEFAULT_LANGUAGE ? '?language='.$value['lang'] : '';

            $link = seo_url_href_mask($value['content_name']).'_'.$value['id'].'.html'.$lang_param;
            $code_entry .= '<url>'."\n".'<loc>'.SITEMAP_CATALOG.$link.'</loc>'."\n";
            $code_entry .= '<lastmod>'.iso8601_date(time()).'</lastmod>'."\n"; //products_last_modified is often empty in data base, take momentary time, noRiddle
            $code_entry .= '<changefreq>'.$changefreq.'</changefreq>'."\n".'</url>'."\n";
            //EOC don't need this query, products_name is fetched in seo_url_model_noRiddle.php, noRiddle
        }
    }
    
    $code_entry .= $footer;
    
    $index_code_entry .= '<sitemap>'."\n".'<loc>'.SITEMAP_CATALOG.$datei.'</loc>'."\n".'<lastmod>'.iso8601_date(time()).'</lastmod>'."\n".'</sitemap>'."\n";

    if(file_put_contents($datei, $code_entry) && file_put_contents($index_datei, $index_code_entry, FILE_APPEND)) {
        //echo $_POST['file_counter']; // test noRiddle
        if($_POST['file_counter'] == $_POST['all_files']) {
            echo '<strong>FERTIG ! --|-- </strong><br />';
        }
        $conts_time_end += microtime(true); //measure time, noRiddle
        $prod_time_eff = $conts_time_end - $conts_time_start; //measure time, noRiddle
        $_SESSION['sitemaps_elapsed_time'] += $prod_time_eff; //measure time, noRiddle
        echo 'Content-Sitemap '.$_POST['file_counter'].' erstellt. Contents '.$line_count_start.' bis '.$line_count_total.(isset($prod_time_eff) ? '<br />Verbrauchte Zeit: '.number_format($prod_time_eff, 4, ',', '.').' sec' : '').'<br />Gesamte verbrauchte Zeit: '.number_format($_SESSION['sitemaps_elapsed_time'], 4, ',', '.').' sec';
        
        if($_POST['file_counter'] == $_POST['all_files']) unset($_SESSION['sitemaps_elapsed_time']); //unset session with total elapsed time
    }
    if ($_POST['file_counter'] == $_POST['all_files']) {
        file_put_contents($index_datei, $index_footer_entry, FILE_APPEND);
    }
}
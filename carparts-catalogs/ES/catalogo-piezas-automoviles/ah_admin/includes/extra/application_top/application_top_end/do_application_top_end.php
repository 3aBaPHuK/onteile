<?php
/************************************************
* file: do_application_top_end.php
* use: define needed vars etc.
* (c) noRiddle 06-2017
************************************************/

//BOC add favorites in top menu
$own_favorites = array();
//MasterAdmin
$own_favorites[12] = array('file'  => 'master_admin.php',
                           'par'   => '', 
                           'mode'  => 0,
                           'icon'  => 'icon_master_admin.png',
                           'name'  => NR_MASTER_ADMIN_FAV_ALT_TXT,
                           'class' => ''
                          );
//i18n (internationalization)
$own_favorites[14] = array('file'  => 'langs_edit.php',
                           'par'   => '', 
                           'mode'  => 0,
                           'icon'  => 'icon_i18n.png',
                           'name'  => NR_I18N_ICON_ALT_TXT,
                           'class' => 'right'
                          );
//EOC add favorites in top menu

//array with customers_id who are allowed to delete orders and give administration rights
$admin_array = $remote_url[$cat_env]['admin_array']; //array('1', '13899', '53206', '282796');

//BOC define array for guest-status-Ids (read out from configuration group 17 GUEST_STATUS_IDS)
$guest_status_array = explode(',', GUEST_STATUS_IDS);
//EOC define array for guest-status-Ids (read out from configuration group 17 GUEST_STATUS_IDS)

//inclusion for affiliate program
require(DIR_WS_INCLUDES . 'affiliate_application_top.php');

//BOC ajax response for automativ meta-descriptions for certain contents, noRiddle
if(isset($_POST['coid']) && $_POST['coid'] > 1100 && $_POST['coid'] < 2400 && substr($_POST['coid'], -1, 1) == 8) {
    if(isset($_POST['make_meta_desc']) && isset($_POST['coid']) && isset($_POST['lng_id'])) {
        $meta_desc_qu_str = "SELECT content_text, content_meta_description FROM ".TABLE_CONTENT_MANAGER." WHERE content_group = ".(int)$_POST['coid']." AND languages_id = ".(int)$_POST['lng_id'];
        $meta_desc_qu = xtc_db_query($meta_desc_qu_str);
        if(xtc_db_num_rows($meta_desc_qu)) {
            $meta_desc_arr = xtc_db_fetch_array($meta_desc_qu);

            if($meta_desc_arr['content_text'] != '') {
                //preg_match_all('#<li>(.*)<\/li>#', $meta_desc_arr['content_text'], $cat_vals);
                preg_match_all('#<li>([^<>]+)<\/li>#', $meta_desc_arr['content_text'], $cat_vals);
                //echo print_r($cat_vals, true);
                if(!empty($cat_vals[1])) {
                    $all_cats = implode(', ', $cat_vals[1]);
                    if(strpos($meta_desc_arr['content_meta_description'], ':') !== false) {
                        $meta_desc_parts = explode(':', $meta_desc_arr['content_meta_description']);
                        $meta_desc_cats = trim($meta_desc_parts[1]);

                        if($all_cats != $meta_desc_cats) {
                            echo $meta_desc_parts[0].': '.$all_cats;
                        } else {
                            echo 'META-DESCRIPTION HAS GOT CATEGORIES ALREADY';
                        }
                    } else {
                        echo $meta_desc_arr['content_meta_description'].': '.$all_cats;
                    }
                } else {
                    echo 'NOTHING FOUND';
                }
            }
        } else {
            echo 'ERROR';
        }

        exit();
    }
}
//EOC ajax response for automativ meta-descriptions for certain contents, noRiddle
?>
<?php
/* --------------------------------------------------------------
   $Id: orders_status.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(orders_status.php,v 1.7 2002/01/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (orders_status.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Statut de commande');

define('TABLE_HEADING_ORDERS_STATUS', 'Statut de commande');
define('TABLE_HEADING_SORT', 'Tri');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Veuillez faire tous les changements nécessaires');
define('TEXT_INFO_ORDERS_STATUS_NAME', 'Statut de commande:');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez indiquer le nouveau statut de commande avec toutes les données pertinentes');
define('TEXT_INFO_DELETE_INTRO', 'Êtes-vous sùr de vouloir supprimer le statut de commande?');
define('TEXT_INFO_HEADING_NEW_ORDERS_STATUS', 'Nouveau statut de commande');
define('TEXT_INFO_HEADING_EDIT_ORDERS_STATUS', 'Editer le statut de commande');
define('TEXT_INFO_HEADING_DELETE_ORDERS_STATUS', 'Supprimer le statut de commande');
define('TEXT_INFO_ORDERS_STATUS_SORT_ORDER', 'Tri:');

define('ERROR_REMOVE_DEFAULT_ORDER_STATUS', 'Erreur: Le statut de commande par défaut ne peut pas être supprimé. Veuillez définir un nouveau statut de commande par défaut et rééssayez.');
define('ERROR_STATUS_USED_IN_ORDERS', 'Erreur: Ce statut de commande est encore utilisé pour des commandes.');
define('ERROR_STATUS_USED_IN_HISTORY', 'Erreur: Ce statut de commande est encore utilisé dans le historique de commandes.');
?>

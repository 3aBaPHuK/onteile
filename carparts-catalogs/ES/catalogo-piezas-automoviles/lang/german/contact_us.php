<?php
 //additional fields
 define('EMAIL_COMPANY', 'Firma: ');
 define('EMAIL_STREET', 'Stra&szlig;e: ');
 define('EMAIL_POSTCODE', 'PLZ: ');
 define('EMAIL_CITY', 'Stadt: ');
 define('EMAIL_PHONE', 'Telefon: ');
 define('EMAIL_FAX', 'Fax: ');
 define('EMAIL_SENT_BY', '&Uuml;bermittelt von %s %s am %s um %s Uhr');
 //define('EMAIL_NOTIFY', 'ACHTUNG, diese E-Mail kann NICHT mit -ABSENDER ANTWORTEN- beantwortet werden!'); // Not used yet
 
 //default fields
 define('EMAIL_NAME', 'Name: ');
 define('EMAIL_EMAIL', 'Email: ');
 define('EMAIL_MESSAGE', 'Nachricht: ');
 
 //contact-form error messages
 //BOC nicer error output, noRiddle
 //define('ERROR_EMAIL','<p><b>Ihre E-Mail-Adresse:</b> Keine oder ung&uuml;ltige Eingabe!</p>');
 //define('ERROR_VVCODE','<p><b>Sicherheitscode:</b> Keine &Uuml;bereinstimmung, bitte geben Sie den Sicherheitscode erneut ein!</p>');
 //define('ERROR_MSG_BODY','<p><b>Ihre Nachricht:</b> Keine Eingabe!</p>');	
 define('ERROR_EMAIL','<li><b>Ihre E-Mail-Adresse:</b> Keine oder ung&uuml;ltige Eingabe!</li>');
 define('ERROR_VVCODE','<li><b>Sicherheitscode:</b> Keine &Uuml;bereinstimmung, bitte geben Sie den Sicherheitscode erneut ein!</li>');
 define('ERROR_MSG_BODY','<li><b>Ihre Nachricht:</b> Keine Eingabe!</li>');
 //EOC nicer error output, noRiddle
 
 //BOC new constants for VIN, price question and file upload, noRiddle
 define('AH_MAIL_PRICE_QUESTION_SUBJ', 'Preisanfrage zu Artikel:');
 define('CONTACT_UPLOAD_TEXT', 'Erlaubt sind %s mit einer Grösse von höchstens %s.<br />Es sind keine Leerzeichen im Dateinamen erlaubt.');
 define('CONTACT_ERROR_FILE_SIZE', '<li><b>Bild hochladen:</b> Datei zu gross. Erlaubt sind %s.</li>');
 define('CONTACT_ERROR_FILE_EXT', '<li><b>Bild hochladen:</b> Dateiendung nicht erlaubt. Erlaubt sind %s.</li>');
 define('CONTACT_ERROR_FILE_LENGTH_AND_CHARS', '<li><b>Bild hochladen:</b> Dateiname ist zu lang (höchstens %d Zeichen plus Dateiendung)<br />oder es wurden verbotene Zeichen im Dateinamen gefunden.<br />Erlaubt sind a-z, A-Z, 0-9 Unterstrich und Bindestrich.</li>');
 //EOC new constants for VIN, price question and file upload, noRiddle
?>
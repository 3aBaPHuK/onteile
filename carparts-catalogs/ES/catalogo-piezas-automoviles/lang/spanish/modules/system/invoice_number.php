<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_INVOICE_NUMBER_STATUS_TITLE', 'Estado');
define('MODULE_INVOICE_NUMBER_STATUS_DESC', 'Estado del módulo');
define('MODULE_INVOICE_NUMBER_STATUS_INFO', 'Activo');
define('MODULE_INVOICE_NUMBER_TEXT_TITLE', 'Módulo Nuevo Número de Factura');
define('MODULE_INVOICE_NUMBER_TEXT_BTN', 'Editar');
define('MODULE_INVOICE_NUMBER_TEXT_DESCRIPTION',  'Módulo Nuevo Número de Factura');
define('MODULE_INVOICE_NUMBER_IBN_BILLNR_TITLE', 'Número de factura siguiente');
define('MODULE_INVOICE_NUMBER_IBN_BILLNR_DESC', 'Al asignar un pedido, este número se asignará como siguiente.');
define('MODULE_INVOICE_NUMBER_IBN_BILLNR_FORMAT_TITLE', 'Formato de número de factura');
define('MODULE_INVOICE_NUMBER_IBN_BILLNR_FORMAT_DESC', 'Esquema de estructura de No. de factura: {n}=número secuencial, {d}=Día, {m}Mes, {y}=Año, <br>p.ej. "100{n}-{d}-{m}-{y}" resulta en "10099-28-02-2007"');

?>

<?php
/*************************************************************
* file: define_new_database_tables.php
* define database tables for new tables
* (c) noRiddle 12-2016
*************************************************************/
define('TABLE_ORDER_STATUS_COMMENTS', 'order_status_comments');
define('TABLE_ORDER_STATUS_COMMENTS_TXT', 'order_status_comments_txt');
// BOC constants for Imageslider tables, noRiddle
define('TABLE_IMAGESLIDERS', 'imagesliders');
define('TABLE_IMAGESLIDERS_INFO', 'imagesliders_info');
// EOC constants for Imageslider tables, noRiddle
//BOC db table for orer mail advertising
define('TABLE_ORDER_MAIL_ADVERTISE', 'order_mail_advertise');
//EOC db table for orer mail advertising
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_CLEVERREACH_TEXT_TITLE', 'CleverReach - marketing email professionnel');
define('MODULE_CLEVERREACH_TEXT_DESCRIPTION', 'La passerelle rend possible une connexion directe à la boutique en ligne. Des données de bénéficaire seront importés sans effort manuel dans votre compte.<hr noshade>');
define('MODULE_CLEVERREACH_STATUS_TITLE', 'Activer le module?');
define('MODULE_CLEVERREACH_STATUS_DESC', 'Activer le système de newsletter CleverReach<br/><b>Important:</b> Pour cela <a href="http://www.cleverreach.de/frontend/" target="_blank"><strong><u>une inscription chez CleverReach</u></strong></a> est indispensable.');
define('MODULE_CLEVERREACH_APIKEY_TITLE', 'CleverReach API');
define('MODULE_CLEVERREACH_APIKEY_DESC', 'Entrez la clé API ici.');
define('MODULE_CLEVERREACH_NAME_TITLE', 'Projet CleverReach');
define('MODULE_CLEVERREACH_NAME_DESC', 'Entrez le nom de projet ici.');
define('MODULE_CLEVERREACH_GROUP_TITLE', 'ID de groupe CleverReach');
define('MODULE_CLEVERREACH_GROUP_DESC', 'Entrez l\'ID de groupe pour la newsletter ici.');

?>

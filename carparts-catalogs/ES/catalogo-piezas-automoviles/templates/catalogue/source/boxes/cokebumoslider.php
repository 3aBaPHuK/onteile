<?php
/************************************************************************
* CoKeBuMo imageslider plugin                                           *
*                                                                       *
* Imageslider for modified eCommerce shopsoftware 1.06 rev 4356         *
* plugin derived from imageslider by Hetfield                           *
* based on responsive kenburner slider by codecanyon, licence necessary *
*                                                                       *
* copyright (c) for all changes noRiddle of webdesign-endres.de 2013    *
************************************************************************/

$box_smarty = new smarty;
$imagesliders_string = '';
$tab = "\t";
require_once(DIR_FS_INC.'xtc_get_categories_name.inc.php');
require_once(DIR_FS_INC.'xtc_get_products_name.inc.php');

$box_smarty->assign('language', $_SESSION['language']);

if (!CacheCheck()) {
    $cache=false;
    $box_smarty->caching = 0;
} else {
    $cache=true;
    $box_smarty->caching = 1;
    $box_smarty->cache_lifetime = CACHE_LIFETIME;
    $box_smarty->cache_modified_check = CACHE_CHECK;
    $cache_id = 'cokebumo'.$_SESSION['language'].$_SESSION['customers_status']['customers_status_id'];
}

if (!$box_smarty->is_cached(CURRENT_TEMPLATE.'/boxes/box_cokebumoslider.html', $cache_id) || !$cache) {
    $box_smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');

    $imagesliders_query = "SELECT
                            #ii.imagesliders_title, ***not needed here, noRiddle
                            ii.imagesliders_url,
                            ii.imagesliders_url_target,
                            ii.imagesliders_url_typ,
                            ii.imagesliders_data_kenburn,
                            ii.imagesliders_data_transition,
                            ii.imagesliders_data_startalign,
                            ii.imagesliders_data_endalign,
                            ii.imagesliders_data_zoom,
                            ii.imagesliders_data_zoomfact,
                            ii.imagesliders_data_panduration,
                            ii.imagesliders_image,
                            ii.imagesliders_alt,
                            ii.imagesliders_video_url,
                            ii.imagesliders_video_expl,
                            ii.imagesliders_desc_direct,
                            ii.imagesliders_desc_effect,
                            ii.imagesliders_description
                     FROM ".TABLE_IMAGESLIDERS_INFO." ii
                LEFT JOIN ".TABLE_IMAGESLIDERS." i
                       ON i.imagesliders_id = ii.imagesliders_id
                    WHERE i.status = '0'
                      AND ii.imagesliders_image != ''
                      AND ii.languages_id='".(int) $_SESSION['languages_id']."'
                 ORDER BY i.sorting ASC";

    $imagesliders_query = xtDBquery($imagesliders_query);
    
    if(xtc_db_num_rows($imagesliders_query, true) > 0) {
        $imagesliders_string .= $tab.'<ul>'."\n";

        while ($imagesliders_data = xtc_db_fetch_array($imagesliders_query, true)) {  
            if ($imagesliders_data['imagesliders_url'] == '') {
                $url = '';  // set url to empty when no link provided, noRiddle
            }  else {
                if ($imagesliders_data['imagesliders_url_target'] == '0') {
                    $target = '';
                } elseif ($imagesliders_data['imagesliders_url_target'] == '1') {
                    $target = ' target="_blank"';
                } elseif ($imagesliders_data['imagesliders_url_target'] == '2') {
                    $target = ' target="_top"';
                } elseif ($imagesliders_data['imagesliders_url_target'] == '3') {
                    $target = ' target="_self"';
                } elseif ($imagesliders_data['imagesliders_url_target'] == '4') {
                    $target = ' target="_parent"';
                }
                if ($imagesliders_data['imagesliders_url_typ'] == '0') {  // new type, no link, noRiddle
                    $url = '';  // set url to empty when no link desired, noRiddle
                } elseif ($imagesliders_data['imagesliders_url_typ'] == '1') {
                    $url = $imagesliders_data['imagesliders_url'];
                } elseif ($imagesliders_data['imagesliders_url_typ'] == '2') {
                    $url = xtc_href_link($imagesliders_data['imagesliders_url']);
                } elseif ($imagesliders_data['imagesliders_url_typ'] == '3') {
                    $url = xtc_href_link(FILENAME_PRODUCT_INFO, xtc_product_link((int)$imagesliders_data['imagesliders_url'],xtc_get_products_name((int)$imagesliders_data['imagesliders_url'])));
                } elseif ($imagesliders_data['imagesliders_url_typ'] == '4') {
                    $url = xtc_href_link(FILENAME_DEFAULT, xtc_category_link((int)$imagesliders_data['imagesliders_url'],xtc_get_categories_name((int)$imagesliders_data['imagesliders_url'])));
                } elseif ($imagesliders_data['imagesliders_url_typ'] == '5') {
                    $content_querys = "SELECT
                                        content_title
                                 FROM ".TABLE_CONTENT_MANAGER."
                                WHERE languages_id='".(int) $_SESSION['languages_id']."'
                                  AND content_group = '".(int)$imagesliders_data['imagesliders_url']."'
                                  AND content_status=1
                             ORDER BY sort_order";
                    $content_querys = xtDBquery($content_querys);
                    $content_title = xtc_db_fetch_array($content_querys,true);				
                    $SEF = '';
                    if (SEARCH_ENGINE_FRIENDLY_URLS == 'true'){
                        $SEF = '&content='.xtc_cleanName($content_title['content_title']);
                    }
                    $url = xtc_href_link(FILENAME_CONTENT, 'coID='.(int)$imagesliders_data['imagesliders_url'].$SEF);
                }			
            }
            
            
            $imagesliders_string .= $tab.$tab.'<li'
                                          . ($imagesliders_data['imagesliders_data_kenburn'] != '' ? ' data-kenburn="'.$imagesliders_data['imagesliders_data_kenburn'].'"' : '')
                                          . ($imagesliders_data['imagesliders_data_transition'] != '' ? ' data-transition="'.$imagesliders_data['imagesliders_data_transition'].'"' : '')
                                          . ($imagesliders_data['imagesliders_data_startalign'] != '' ? ' data-startalign="'.$imagesliders_data['imagesliders_data_startalign'].'"' : '')
                                          . ($imagesliders_data['imagesliders_data_endalign'] != '' ? ' data-endalign="'.$imagesliders_data['imagesliders_data_endalign'].'"' : '')
                                          . ($imagesliders_data['imagesliders_data_zoom'] != '' ? ' data-zoom="'.$imagesliders_data['imagesliders_data_zoom'].'"' : '')
                                          . ($imagesliders_data['imagesliders_data_zoomfact'] != '' && $imagesliders_data['imagesliders_data_zoomfact'] != '0' ? ' data-zoomfact="'.$imagesliders_data['imagesliders_data_zoomfact'].'"' : '')
                                          . ($imagesliders_data['imagesliders_data_panduration'] != '' && $imagesliders_data['imagesliders_data_panduration'] != '0' ? ' data-panduration="'.$imagesliders_data['imagesliders_data_panduration'].'"' : '')
                                          . '>'."\n";
            //$imagesliders_string .= $tab.$tab.'<li data-kenburn="on" data-transition="fade" data-startalign="center,center" data-endalign="right,top" data-zoom="random" data-zoomfact="1.5" data-panduration="7">';
                                          
            $imagesliders_string .= $url != '' ? $tab.$tab.$tab.'<a href="'.$url.'"'.$target.'>'."\n".$tab.$tab.$tab.$tab : "\n".$tab.$tab.$tab.'';
            $imagesliders_string .= xtc_image(DIR_WS_IMAGES.$imagesliders_data['imagesliders_image'],$imagesliders_data['imagesliders_alt'],'','','');
            $imagesliders_string .= $url != '' ? "\n".$tab.$tab.$tab.'</a>' : '';
            $imagesliders_string .= $imagesliders_data['imagesliders_video_url'] != '' ? 
                                                                   $tab.$tab.$tab.'<div class="video_kenburner">'
                                                                 . $tab.$tab.$tab.$tab.'<div class="video_kenburn_wrap">'
                                                                 . $tab.$tab.$tab.$tab.$tab.'<div class="video_video">'
                                                                 . $tab.$tab.$tab.$tab.$tab.$tab.'<iframe class="video_clip" src="'.$imagesliders_data['imagesliders_video_url'].'"></iframe>'
                                                                 . $tab.$tab.$tab.$tab.$tab.'</div>'
                                                                 . $tab.$tab.$tab.$tab.$tab.'<div class="video_details">'
                                                                 . $tab.$tab.$tab.$tab.$tab.$tab.$imagesliders_data['imagesliders_video_expl']
                                                                 . $tab.$tab.$tab.$tab.$tab.'</div>'
                                                                 . $tab.$tab.$tab.$tab.$tab.'<div class="close"></div>'
                                                                 . $tab.$tab.$tab.$tab.'</div>'
                                                                 . $tab.$tab.$tab.'</div>'
                                                                 : '';
                                                                 
            $imagesliders_string .= "\n".$tab.$tab.$tab.'<div class="creative_layer">'
                                    . ($imagesliders_data['imagesliders_description'] != '' ? "\n".$tab.$tab.$tab.$tab.'<div class="'.$imagesliders_data['imagesliders_desc_direct'].' '.$imagesliders_data['imagesliders_desc_effect'].'">' : '')
                                    . "\n".$tab.$tab.$tab.$tab.$tab.$imagesliders_data['imagesliders_description']
                                    . ($imagesliders_data['imagesliders_description'] != '' ? "\n".$tab.$tab.$tab.$tab.'</div>' : '')
                                    . "\n".$tab.$tab.$tab.'</div>';
                                    
            $imagesliders_string .= "\n".$tab.$tab.'</li>'."\n";
            

        }
        
        $imagesliders_string .= "\n".$tab.'</ul>';

        $box_smarty->assign('BOX_COKEBUMOSLIDER', $imagesliders_string);
    }
}

if (!$cache) {
	$box_cokebumoslider = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_cokebumoslider.html');
} else {
	$box_cokebumoslider = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_cokebumoslider.html', $cache_id);
}

$smarty->assign('box_COKEBUMOSLIDER', $box_cokebumoslider);
?>
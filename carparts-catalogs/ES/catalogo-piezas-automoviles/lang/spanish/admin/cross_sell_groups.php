<?php
/* --------------------------------------------------------------
   $Id: cross_sell_groups.php 1231 2005-09-21 13:05:36Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(orders_status.php,v 1.7 2002/01/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (orders_status.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Grupos de marketing cruzado');

define('TABLE_HEADING_XSELL_GROUP_NAME', 'Nombre del grupo');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_INFO_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_INFO_XSELL_GROUP_NAME', 'Nombre del grupo:');
define('TEXT_INFO_INSERT_INTRO', 'Por favor, introduzca el nuevo nombre del grupo con todos los datos relevantes.');
define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar este grupo?');
define('TEXT_INFO_HEADING_NEW_XSELL_GROUP', 'Nuevo nombre del grupo');
define('TEXT_INFO_HEADING_EDIT_XSELL_GROUP', 'Editar nombre de grupo');
define('TEXT_INFO_HEADING_DELETE_XSELL_GROUP', 'Borrar nombre de grupo');
define('ERROR_STATUS_USED_IN_ORDERS', 'Error: Este grupo se sigue utilizando actualmente para artículos de marketing cruzado.');

?>

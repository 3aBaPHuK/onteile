$(document).ready(function() {
//$(window).load(function() {
    if ($.fn.cssOriginal != undefined)
        $.fn.css = $.fn.cssOriginal;

    $('.bannercontainer').kenburn(
        {
        vCorrection:0,
        hCorrection:0,
        durSlideFade:0,
        durSlideSlide:700,
        navarrl:'&larr;',
        navarrr:'&rarr;',
        thumbWidth:50,
        thumbHeight:50,
        thumbAmount:5,
        thumbStyle:"bullet",   //thumb, bullet, none, both
        thumbVideoIcon:"off",
        thumbVertical:"bottom",
        thumbHorizontal:"right",					
        thumbXOffset:20,
        thumbYOffset:40,
        bulletXOffset:0,
        bulletYOffset:-16,
        hideThumbs:"on",
        repairChromeBug:"off",
        touchenabled:'on',
        pauseOnRollOverThumbs:'on',
        pauseOnRollOverMain:'on',
        preloadedSlides:2,
        timer:5,
        debug:"off",	
        googleFonts:'',
        googleFontJS:''
        }
    );
    
    $('.cp-left p, .cp-right p, .cp-top p, .cp-bottom p').flowtype({maxFont:13, fontRatio:38}); /*use flowtype for slider, noRiddle*/
});
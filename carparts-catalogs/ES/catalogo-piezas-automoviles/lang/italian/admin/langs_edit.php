<?php
/***********************************************************************************************************
* file: langs_edit.php
* use: language file for /Admin/langs_edit.php
*
* (c) noRiddle 06-2018
***********************************************************************************************************/

define('NR_LANGS_EDIT_GUIDE_HEADING', 'Guide');
define('NR_LANGS_EDIT_GUIDE_TXT', '<ul>
                                    <li><u>Preparation:</u>
                                        <ul>
                                            <li>Choose basis language and save to database.<br />
                                                !! This basis language should not be altered later in case langauges where already edited and saved. The mapping of lines would not be possible otherwise !!</li>
                                            <li>Choose helping languages and save to database.<br />
                                                Each language which shall be edited has to exist as a directory in <i>/lang/helpfiles/</i> and has to contain at least one file. If no sample files exist simply load a file of another existing language into the directory as a pattern.</li>
                                        </ul>
                                    </li>
                                    <li><span class="red"><u>To observe unconditionally !!</u></span>
                                        <ul>
                                            <li>Single quotes have to be preserved unconditionally.<br />
                                            If they are part of the text (e.g. in engl. "don\'t") they have to be escaped, which means that they have to be preceded by a backslash.<br />
                                            This looks like follows: "don\\\'t" or "d\\\'accord" .</li>
                                            <li>HTML and other special characters which belong to PHP also have to be preserved unconditionally.<br />
                                            With a little bit of experience one will recognize which parts are text which has to be translated and which parts belong to program code (HTML, Javascript or PHP).</li>
                                        </ul>
                                    </li>
                                    <li><u>Save:</u>
                                        <ul>
                                            <li>In case languages are opened to be edited they are saved automatically every 10 minutes to avoid expiry of the session and thus a logout.</li>
                                            <li>When saving edited languages they are only stored in the database.<br />
                                            Only when "Generate language files for choosen file" has been clicked the corresponding files will be generated and saved in <i>/lang/translated/</i> in a directory named after the respective language.</li>
                                        </ul>
                                    </li>
                                  </ul>'
);

define('NR_LANGS_EDIT_LOAD_LANGS_HEADING', 'Load langauges into database');
define('NR_LANGS_EDIT_LOAD_LANGS_FILETREE_HEADING', 'File tree');
define('NR_LANGS_EDIT_LOAD_BASIS_LANG', 'Load desired basis language into database:');
define('NR_LANGS_EDIT_WARNING_LOAD_BASIS_JS', 'Really want to load the basis language again ?');
define('NR_LANGS_EDIT_LOAD_HELPER_LANG', 'Load languages to be edited into database:');
define('NR_LANGS_EDIT_WARN_NO_BASIS_LANG', 'First a basis language has to be loaded !');
define('NR_LANGS_EDIT_WARN_HELP_DIR_NOT_EXISTS', 'The directory <i>/lang/helpfiles/</i> doesn\'t exist.<br />If pattern language files exist please load them into <i>/lang/helpfiles/LANGUAGE/</i>.<br />For each language replace LANGUAGE with the english name of the language in lowercase. (Example: /lang/helpfiles/italian/ for Italian)');
define('NR_LANGS_EDIT_LOAD_EXIST_LANG', 'Load already existent languages in database:');
define('NR_LANGS_EDIT_LOADED_TXT', 'Loaded:');
define('NR_LANGS_EDIT_HELPINGLANG_TXT', 'helping language');
define('NR_LANGS_EDIT_EXISTINGLANG_TXT', 'existing already live');

define('NR_LANGS_EDIT_ATTENTION_READ_TXT', 'ATTENTION, read first !! ');
define('NR_LANGS_EDIT_EXPL_LOAD_BASIC_TXT', '!! Don\'t change when other languages where edited already !!<br />
    The choosen language will be imported completely new.<br />
    To assign further languages properly the languages to be edited have to be loaded again as well after having loaded this basic one.<br />
    !! Already saved data will be lost and can not be assigned !!');
define('NR_LANGS_EDIT_EXPL_LOAD_HELPER_TXT', 'In the directory <i>/lang/helpfiles/</i> are directories for further languages which where for example downloaded from modified eCommerce as language packages.<br />
    They may serve as a pattern while editing and translating.<br />
    When loading into the database every value will be programmatically assigned to the corresponding value of the loaded basis language.');
define('NR_LANGS_EDIT_EXPL_LOAD_EXIST_TXT', 'The choosen language is a language whose files already exist in the live directory and will be imported completely new.<br />
    Several languages may be loaded consecutively. They may serve as a translation pattern.<br />
    !! Please obeserve that a language which is loaded more than one time will overwrite the one previously loaded !!');
define('NR_LANGS_EDIT_EXPL_SHOW_LANGS_TXT', 'The above loaded basis language will always automatically be shown as a pattern..<br />
    Already live existent languages, if checked, will be only shown as a guide and are not editable.<br />
    !! If translations where already stored into the database they will be automatically loaded instead of the original helping files values !!');
define('NR_LANGS_EDIT_EXPL_GENERATE_FILES', 'After having saved your translations for a file the files for the edited languages should be generated.');
define('NR_LANGS_EDIT_FILL_EMPTY_WITH_ENGL', 'Fill potentially empty values with english values ?');

define('NR_LANGS_EDIT_LOAD_IN_DB_TXT', 'load in database');
define('NR_LANGS_EDIT_SHOW_MARKED_LANGS_TO_EDIT', 'Show marekd languages to edit them');
define('NR_LANGS_EDIT_SAVE_TRANSL_TO_DB', 'Save changes into database');
define('NR_LANGS_EDIT_GENERATE_FILES', 'Generate language files for chossen file');
    
//messagestack
define('NR_LANGS_EDIT_NOLANG_CHOOSEN_OR_NO_DIR', 'No language choosen or language not existent as directory!');
define('NR_LANGS_EDIT_CONSTS_LOADED', 'Defined constants from %s for %s have been loaded');
define('NR_LANGS_EDIT_CONFS_LOADED', 'Defined Smarty-Conf variables from %s for %s have been loaded');
define('NR_LANGS_EDIT_UNALLOWED_LANGS_MSG', '!! Unallowed langauges can not be displayed !!');
define('NR_LANGS_EDIT_GENERATED_FILES_SUCCESS', '%s in %s has been written into <i>/%s</i>.');
?>
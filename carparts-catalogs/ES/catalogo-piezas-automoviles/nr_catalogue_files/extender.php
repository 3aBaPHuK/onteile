﻿<?php
// Including localization file
global $lax_ui_loc, $lax_cat_dat; //need this for catalogs.php, don't really know why yet, 08-2018, noRiddle
include_once('nr_catalogue_files/config.php');

/*$class = new ReflectionClass('Config');
$arr = $class->getStaticProperties();
echo '<pre>'.print_r($arr, true).'</pre>';*/

include_once('nr_catalogue_files/localization_'.Config::$ui_localization.'.php');
//echo '<pre>$lax_ui_loc: '.$lax_ui_loc.' | $ui_localization: '.Config::$ui_localization.'</pre>';
//echo '<pre>'.(file_exists(DIR_FS_CATALOG.'nr_catalogue_files/localization_'.$_SESSION['language_code'].'.php') ? 'Yupp '.$_SESSION['language_code'] : 'Nope '.$_SESSION['language_code']).'</pre>';

// Including common template
require_once('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'render'.DIRECTORY_SEPARATOR.'template.php');

// Implement common "CMS" functions
abstract class CommonExtender extends LanguageTemplate
{
    public function GetLocalizedString($name, $params = false, $renderer)
    {
        return self::FormatLocalizedString($name, $params);
    }

    public static function FormatLocalizedString($name, $params = false)
    {
        if ($params == false)
            return self::LocalizeString($name);

        if (!is_array($params))
            return sprintf(self::LocalizeString($name), $params);

        array_unshift($params, self::LocalizeString($name));
        return call_user_func_array('sprintf', $params);
    }

    public function AppendJavaScript($filename, $renderer)
    {
    	  echo '<script src="'.$this->Convert2uri($filename).'" type="text/javascript"></script>';
    }

    public function AppendCSS($filename, $renderer)
    {
    	  echo '<link href="'.$this->Convert2uri($filename).'" media="screen" rel="stylesheet" type="text/css"/>';
    }

    public function Convert2uri($filename)
    {
        $filename = str_replace('\\', '/', $filename);
        $current_script = explode('/', dirname($_SERVER['SCRIPT_FILENAME']));
        $included_file = explode('/', dirname($filename));
        $url = implode('/', array_slice($included_file, count($current_script))) . '/' . basename($filename);
        return $url;
    }

    static function LocalizeString($str)
    {
        $str = strtolower($str);
        $data = @LanguageTemplate::$language_data[$str];
        return $data ? $data : $str;
    }

    static function isFeatureSupported($catalogInfo, $featureName)
    {
        $result = false;
        //BOC fix foreach error, noRiddle | Warning: Invalid argument supplied for foreach() in /usr/www/users/onteile/katalog_shop_test/nr_catalogue_files/extender.php on line 58
        /*foreach ($catalogInfo->features->feature as $feature) {
            if ((string)$feature['name'] == $featureName) {
                $result = true;
                break;
            }
        }*/
        if(is_array($catalogInfo->features->feature) || is_object($catalogInfo->features->feature)) {
            foreach ($catalogInfo->features->feature as $feature) {
                if ((string)$feature['name'] == $featureName) {
                    $result = true;
                    break;
                }
            }
        }
        //EOC fix foreach error, noRiddle

        return $result;
    }
}
?>
<?php
/**************************************
* file: csrf_exclusions.php
* use: exclude scripts from csrf token
* (c) noRiddle 04-2017
**************************************/

//define('CSRF_TOKEN_EXCLUSIONS', 'imagesliders');
$module_exclusions[] = 'imagesliders';
$module_exclusions[] = 'langs_edit';
$module_exclusions[] = 'catalogue_statistic';
?>
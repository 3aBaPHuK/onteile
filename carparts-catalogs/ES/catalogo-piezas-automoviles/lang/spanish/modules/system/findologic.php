<?php
/* -----------------------------------------------------------------------------------------
   $Id: findologic.php 2011-11-24 modified-shop $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com
   (c) 2003   nextcommerce (invoice.php,v 1.6 2003/08/24); www.nextcommerce.org
   (c) 2005 XT-Commerce - community made shopping http://www.xt-commerce.com ($Id: billiger.php 950 2005-05-14 16:45:21Z mz $)
   (c) 2008 Gambio OHG (billiger.php 2008-11-11 gambio)

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_FINDOLOGIC_TEXT_TITLE', 'FINDOLOGIC Búsqueda de tienda - dejar de buscar - encontrar!');
define('MODULE_FINDOLOGIC_TEXT_DESCRIPTION', 'FINDOLOGIC es la solución de búsqueda óptima para su tienda online. ¡Sus clientes encontrarán rápidamente el artículo que desean y lo comprarán! <span style="color: #ff8c00;">Los incrementos de ventas de dos dígitos en porcentaje de nuestros clientes están documentados.</span><br /><br />Con el código de asociado <span class="col-red" style="font-weight:bold;">qXgXj</span> ahorra 50€ en tarifa de activación!<br /><br /><a href="https://secure.findologic.com/bestellung?partner=qXgXj" target="_blank"><strong><u>¡Regístrese ahora!</u></strong></a>');
define('MODULE_FINDOLOGIC_LANG_TITLE' , '<hr noshade>Idioma');
define('MODULE_FINDOLOGIC_LANG_DESC' , 'Idioma de los artículos que desea exportar.<br/><br/><b>Importante:</b> Esta selección sólo es necesaria si sólo ha reservado 1 servicio.');
define('MODULE_FINDOLOGIC_CUSTOMER_GROUP_TITLE' , '<hr noshade>Grupo de clientes:');
define('MODULE_FINDOLOGIC_CUSTOMER_GROUP_DESC' , 'Seleccione el grupo de clientes que constituye la base para el precio exportado. (Si no tiene precios de grupo de clientes, seleccione Invitado):');
define('MODULE_FINDOLOGIC_CURRENCY_TITLE' , '<hr noshade>Moneda:');
define('MODULE_FINDOLOGIC_CURRENCY_DESC' , 'Moneda en el archivo de exportación');
define('MODULE_FINDOLOGIC_STATUS_TITLE' , 'Estado');
define('MODULE_FINDOLOGIC_STATUS_DESC' , '¿Activar módulo?');
define('MODULE_FINDOLOGIC_AUTOCOMPLETE_TITLE' , '<hr noshade>Búsqueda de autocompletación');
define('MODULE_FINDOLOGIC_AUTOCOMPLETE_DESC' , '¿Activar la función de autocompletación en el cuadro de búsqueda?');

?>

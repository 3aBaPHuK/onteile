<?php
define('BOX_ORDER_MAIL_ADVERTISE', 'Werbung in Bestellbestätigung');
define('TEXT_DISPLAY_NUMBER_OF_ORDER_MAIL_ADVERTISE', 'Angezeigt werden <b>%d</b> bis <b>%d</b> (von insgesamt <b>%d</b> Werbungs-Texten)');

define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_ID', 'ID');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_TITLE', 'Werbungs-Titel');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_TEXT', 'Werbungs-Text');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Aktion');

define('TEXT_INFO_EDIT_INTRO', 'Bitte f&uuml;hren Sie alle notwendigen &Auml;nderungen durch');
define('TEXT_INFO_INSERT_ORDER_MAIL_ADVERTISE_INTRO', 'Bitte geben Sie einen neuen Werbungs-Text ein');
define('TEXT_INFO_DELETE_ORDER_MAIL_ADVERTISE_INTRO', 'Sind Sie sicher, dass Sie diesen Werbungs-Text l&ouml;schen m&ouml;chten?');
define('TEXT_INFO_HEADING_NEW_ORDER_MAIL_ADVERTISE', 'Neuer Werbungs-Text');
define('TEXT_INFO_HEADING_EDIT_ORDER_MAIL_ADVERTISE', 'Werbungs-Text bearbeiten');
define('TEXT_INFO_HEADING_DELETE_ORDER_MAIL_ADVERTISE', 'Werbungs-Text l&ouml;schen');
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_ADMIN_LOG_TEXT_TITLE', 'Administrator Log');
define('MODULE_ADMIN_LOG_TEXT_DESCRIPTION', 'Con el Administrator Log se registran los cambios.
<ul>
  <li>Pedidos</li>
  <li>Categorías</li>
  <li>Artículos</li>
  <li>Manager de Contenido</li>
  <li>Modos de envío</li>
  <li>Opciones de pago</li>
  <li>Resumen</li>
  <li>Módulos de sistema</li>
  <li>Módulos de Exportación</li>
  <li>Configuracíon</li>
</ul>
  ');
define('MODULE_ADMIN_LOG_STATUS_TITLE', '¿Activar módulo?');
define('MODULE_ADMIN_LOG_STATUS_DESC', 'Activar Administrator Log');

define('MODULE_ADMIN_LOG_DISPLAY_TITLE', '¿Mostrar Log?');
define('MODULE_ADMIN_LOG_DISPLAY_DESC', 'Mostrar Administrator Log en el pie de la página');

define('MODULE_ADMIN_LOG_SHOW_DETAILS_TITLE', '¿Mostrar detalles?');
define('MODULE_ADMIN_LOG_SHOW_DETAILS_DESC', 'Mostrar detalles de cambio como Array');

define('MODULE_ADMIN_LOG_SHOW_DETAILS_FULL_TITLE', '¿Mostrar detalles completos?');
define('MODULE_ADMIN_LOG_SHOW_DETAILS_FULL_DESC', 'Mostrar detalles de cambio completos como Array');

?>

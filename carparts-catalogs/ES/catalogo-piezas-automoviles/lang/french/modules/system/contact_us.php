<?php
/* -----------------------------------------------------------------------------------------
   $Id: contact_us.php 10261 2016-08-22 11:21:32Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_CONTACT_US_STATUS_TITLE', 'Statut');
define('MODULE_CONTACT_US_STATUS_DESC', 'Statut de module');
define('MODULE_CONTACT_US_STATUS_INFO', 'Actif');
define('MODULE_CONTACT_US_TEXT_TITLE', 'Formulaire de contact Log');
define('MODULE_CONTACT_US_TEXT_DESCRIPTION', 'Une fois une demande via le formulaire de contact a été effectuée, un log sera entré dans la banque des données.');
?>

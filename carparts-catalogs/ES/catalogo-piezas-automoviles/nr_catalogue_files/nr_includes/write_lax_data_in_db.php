<?php
/*******************************
* 
*
* (c) noRiddle
*******************************/
$exclude_array_for_doubles = array(); //array('subaru japan', 'subaru usa', 'zaz');
//$nr_ctlg_brand_arr = array();
$db_ins_arr = array();
//echo '<pre>'.print_r($data, true).'</pre>';
foreach($data[0]->row as $row) {
    if(!in_array(strtolower((string)$row->attributes()->name), $exclude_array_for_doubles)) {
        switch(strtolower((string)$row->attributes()->name)) { //we switched ->brand before but with new Kia Commercial, Hyundai Commercial etc. it is not possible any more because brand is same as without Commercial
            case 'abarth':
                $our_brd = 'fiat';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'alfa romeo':
                $our_brd = 'alfaromeo';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'bmw motorrad':
                if($cat_env == 'de' || $cat_env == 'op') {
                    $our_brd = 'bmw-motorrad';
                } else if($cat_env == 'es') {
                    $our_brd = 'bmw-moto';
                } else if($cat_env == 'fr') {
                    $our_brd = 'bmw-moto';
                } else if($cat_env == 'it') {
                    $our_brd = 'bmw-moto';
                } else if($cat_env == 'mx') {
                    $our_brd = 'bmw-moto';
                } else if($cat_env == 'uk') {
                    $our_brd = 'bmw-bike';
                }
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'chrysler':
                $our_brd = 'chrysler-dodge-jeep-ram';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'daf':  //new nutzfahrzeuge
                $our_brd = 'daf';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'dodge':
                $our_brd = 'chrysler-dodge-jeep-ram';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'fiat professional':
                $our_brd = 'fiat';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'ford europe';
                $our_brd = 'ford';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'hyundai commercial': //new nutzfahrzeuge
                $our_brd = 'hyundai';
                $our_icon = 'hyundai-com.png';
                $our_name = (string)$row->attributes()->name;
              break;
            case 'infinity':
                $our_brd = 'nissan';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'isuzu commercial':  //new nutzfahrzeuge
                $our_brd ='isuzu';
                $our_icon = 'isuzu-com.png';
                $our_name = (string)$row->attributes()->name;
              break;
            case 'iveco':  //new nutzfahrzeuge
                $our_brd = 'iveco';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'jeep':
                $our_brd = 'chrysler-dodge-jeep-ram';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'kia commercial':  //new nutzfahrzeuge
                $our_brd = 'kia';
                $our_icon = 'kia-com.png';
                $our_name = (string)$row->attributes()->name;
              break;
            case 'lancia':
                $our_brd = 'alfaromeo';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'land rover': //'land rover europe': //has been changed 01-2020, noRiddle
                $our_brd = 'landrover';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'lexus':
                $our_brd = 'toyota';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'man':  //new nutzfahrzeuge
                $our_brd = 'man';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'mercedes-benz':
                $our_brd = 'mercedes';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'mercedes-benz commercial':  //new nutzfahrzeuge
                $our_brd = 'mercedes';
                $our_icon = 'mercedes-benz-com.png';
                $our_name = (string)$row->attributes()->name;
              break;
            /*case 'opel/vauxhall':
                $our_brd = 'opel';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;*/ //there is both, opel and vauxhall now, noRiddle
            case 'ram':
                $our_brd = 'chrysler-dodge-jeep-ram';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'renault':  //we call it Renault/Dacia because the models sold in Europe are in the Renault rubrique
                $our_brd = strtolower((string)$row->attributes()->brand);
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = 'Renault/Dacia'; //(string)$row->attributes()->name;
              break;
            case 'renault trucks':  //new nutzfahrzeuge
                $our_brd = 'renault-trucks';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            case 'scania':  //new nutzfahrzeuge
                $our_brd = 'scania';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;
            /*case 'subaru europe':
                $our_brd = 'subaru';
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
              break;*/ //name has changed to only subaru, noRiddle
            case 'volvo trucks': //new nutzfahrzeuge
                $our_brd = 'volvo-trucks';
                $our_icon = 'volvo-com.png';
                $our_name = (string)$row->attributes()->name;
              break;
            default:
                $our_brd = strtolower((string)$row->attributes()->brand);
                $our_icon = strtolower((string)$row->attributes()->icon);
                $our_name = (string)$row->attributes()->name;
        }
        
        $row->attributes()->icon = $our_icon; //overwrite Laximos Icons with our one (important for Nutzfahrzeuge where normal Icon has got same name (like e.g. hyundai.png for both, normal and commercial))
        $row->attributes()->name = $our_name; //overwrite  Laximos name if needed (e.g. Vauxhall in UK

        //BOC write in db
        $sec_our_brand = xtc_db_input($our_brd);
        $sec_c_param = xtc_db_input((string)$row->attributes()->code);
        $sec_brand = xtc_db_input((string)$row->attributes()->brand);
        $sec_name = xtc_db_input((string)$row->attributes()->name);
        $sec_icon = xtc_db_input($our_icon); //xtc_db_input(strtolower((string)$row->attributes()->icon));

        $db_helper_ins_arr = array('c_param' => $sec_c_param, //(string)$row->attributes()->code,
                                   'lax_brand' => $sec_brand, //(string)$row->attributes()->brand,
                                   'lax_name' => $sec_name, //xtc_db_input((string)$row->attributes()->name);
                                   'our_brand' => $sec_our_brand,
                                   'brand_icon' => $sec_icon //strtolower((string)$row->attributes()->icon)
                                  );
        //echo '<br /><br /><pre>'.print_r($db_helper_ins_arr, true).'</pre>';
        
        $verif_dat_qu = xtc_db_query("SELECT c_param, brand_icon, coid FROM laximo_data_helper WHERE lax_brand = '".$sec_brand."' AND lax_name = '".$sec_name."'");
        if(!xtc_db_num_rows($verif_dat_qu)) { //just a fallback, normally shouldn't happen
            //unset($_SESSION['nr_ctlg_brands']);
            xtc_db_perform('laximo_data_helper', $db_helper_ins_arr);
            
            //BOC write new brand into $_SESSION['nr_ctlg_brands']
            $_SESSION['nr_ctlg_brands'][$sec_c_param] = array('lax_brand' => $sec_brand, 
                                                              'lax_name' => $sec_name,
                                                              'our_brand' => $sec_our_brand, 
                                                              'brand_icon' => $sec_icon,
                                                              'coid' => '' //not known if new brand, has to be added manually
                                                             );
            //EOC write new brand into $_SESSION['nr_ctlg_brands']
        } else {
            $verif_dat_arr = xtc_db_fetch_array($verif_dat_qu);
            if(($verif_dat_arr['c_param'] != $sec_c_param) || ($verif_dat_arr['brand_icon'] != $sec_icon)) { //(string)$row->attributes()->code) {
                unset($_SESSION['nr_ctlg_brands'][$verif_dat_arr['c_param']]);
                xtc_db_perform('laximo_data_helper', $db_helper_ins_arr, 'update', "lax_brand = '".$sec_brand."' AND lax_name = '".$sec_name."'");
                
                //BOC write brand into $_SESSION['nr_ctlg_brands'] newly
                $_SESSION['nr_ctlg_brands'][$sec_c_param] = array('lax_brand' => $sec_brand, 
                                                                  'lax_name' => $sec_name,
                                                                  'our_brand' => $sec_our_brand, 
                                                                  'brand_icon' => $sec_icon,
                                                                  'coid' => $verif_dat_arr['coid']
                                                                 );
                //EOC write brand into $_SESSION['nr_ctlg_brands'] newly
                
                //BOC look into table catalogue_cart whether we have products of the brand in there
                    //BOC fix table manually
                    //DELETE FROM catalogue_cart WHERE lax_brand = '';
                    //UPDATE catalogue_cart SET lax_brand = 'GM_OP201809' WHERE brand = 'opel';
                    //EOC fix table manually
                
                //example: bmw was BMW201605, BMWMR201605 01-2018
                $cat_cart_verif_qu = xtc_db_query("SELECT catalogue_cart_id FROM catalogue_cart WHERE lax_brand = '".$verif_dat_arr['c_param']."'");

                if(xtc_db_num_rows($cat_cart_verif_qu) > 0) {
                    $db_cart_upd_arr = array('lax_brand' => $sec_c_param);
                    xtc_db_perform('catalogue_cart', $db_cart_upd_arr, 'update', "lax_brand = '".$verif_dat_arr['c_param']."'");
                }
                //EOC look into table catalogue_cart whether we have products of the brand in there
            }
        }
        //EOC write in db
    }
    // echo '<pre>'.$verif_dat_arr['c_param'].' | '.(string)$row->attributes()->code.'</pre>';
    //echo '<pre>'.print_r($_SESSION['nr_ctlg_brands'][$verif_dat_arr['c_param']], true).'</pre>';
}
?>
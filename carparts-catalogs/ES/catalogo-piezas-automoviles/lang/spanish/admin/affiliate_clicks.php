<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_clicks.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_clicks.php, v 1.2 2003/02/12);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Estadística de clics del programa de afiliados');

define('TABLE_HEADING_ONLINE', 'Estadística de clics');
define('TABLE_HEADING_AFFILIATE_USERNAME', 'Afiliados');
define('TABLE_HEADING_IPADDRESS', 'Dirección IP');
define('TABLE_HEADING_ENTRY_TIME', 'Tiempo');
define('TABLE_HEADING_BROWSER', 'Navegador');
define('TABLE_HEADING_ENTRY_DATE', 'Fecha');
define('TABLE_HEADING_CLICKED_PRODUCT', 'Producto cliqueado');
define('TABLE_HEADING_REFERRAL_URL', 'URL de referencia');

define('TEXT_NO_CLICKS', 'Hasta ahora no se han recogido estadísticas de clics.');
define('TEXT_DISPLAY_NUMBER_OF_CLICKS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> clics)');
?>

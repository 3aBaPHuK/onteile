<?php
/* --------------------------------------------------------------
   $Id: geo_TAGs.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(geo_TAGs.php,v 1.11 2003/05/07); www.oscommerce.com 
   (c) 2003	 nextcommerce ( geo_TAGs.php,v 1.4 2003/08/1); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Caractéristiques de l\'article');
define('HEADING_TITLE_DETAIL', 'Caractéristiques de l\'article - valeurs');

define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Détails');
define('TABLE_HEADING_SORT', 'Tri');
define('TABLE_HEADING_FILTER', 'Filtre');

define('TABLE_HEADING_VALUES_IMAGE', 'Image');
define('TABLE_HEADING_VALUES_CONTENT', 'Contenu');
define('TABLE_HEADING_VALUES_NAME', 'Valeur');
define('TABLE_HEADING_VALUES_DESCRIPTION', 'Déscription');

define('TABLE_HEADING_OPTIONS_NAME', 'Nom');
define('TABLE_HEADING_OPTIONS_DESCRIPTION', 'Déscription');
define('TABLE_HEADING_OPTIONS_CONTENT', 'Contenu');

define('TEXT_INFO_DATE_ADDED', 'ajouté le:');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification:');

define('TEXT_INFO_VALUE_NAME', 'Valeur:');
define('TEXT_INFO_VALUE_DESCRIPTION', 'Déscription:');
define('TEXT_INFO_VALUE_IMAGE', 'Image:');
define('TEXT_INFO_VALUE_CONTENT', 'Contenu:');
define('TEXT_INFO_VALUE_SORT', 'Tri:');
define('TEXT_INFO_DELETE_VALUE_IMAGE', 'Supprimer l\'image?');

define('TEXT_INFO_HEADING_NEW_VALUE', 'Nouvelle valeur');
define('TEXT_INFO_NEW_VALUE_INTRO', 'Veuillez entrer la nouvelle valeur avec toutes les données pertinentes');
define('TEXT_INFO_HEADING_EDIT_VALUE', 'Editer la valeur');
define('TEXT_INFO_EDIT_VALUE_INTRO', 'Veuillez faire tous les changements nécessaires.');
define('TEXT_INFO_HEADING_DELETE_VALUE', 'Supprimer la valeur');
define('TEXT_INFO_DELETE_VALUE_INTRO', 'Êtes-vous sûr de vouloir supprimer la valeur?');

define('TEXT_DISPLAY_NUMBER_OF_VALUES', 'Affichés sont les valeurs de <b>%d</b> à <b>%d</b> (d\'un total de <b>%d</b> valeurs)');


define('TEXT_INFO_OPTION_NAME', 'Nom:');
define('TEXT_INFO_OPTION_DESCRIPTION', 'Déscription:');
define('TEXT_INFO_OPTION_CONTENT', 'Contenu:');
define('TEXT_INFO_OPTION_SORT', 'Tri:');

define('TEXT_INFO_NUMBER_OPTION', 'Nombre de valeurs:');
define('TEXT_INFO_HEADING_NEW_OPTION', 'Nouvelle caractéristique');
define('TEXT_INFO_NEW_OPTION_INTRO', 'Veuillez entrer la nouvelle caractéristique avec toutes les données pertinentes');
define('TEXT_INFO_HEADING_EDIT_OPTION', 'Editer la caractéristique');
define('TEXT_INFO_EDIT_OPTION_INTRO', 'Veuillez faire tous les changements nécessaires.');
define('TEXT_INFO_HEADING_DELETE_OPTION', 'Supprimer la caractéristique');
define('TEXT_INFO_DELETE_OPTION_INTRO', 'Êtes-vous sûr de vouloir supprimer cette caractéristique?');

define('TEXT_DISPLAY_NUMBER_OF_OPTIONS', 'Affichés sont les caractéristiques de <b>%d</b> à <b>%d</b> (d\'un total de <b>%d</b> caractéristiques)');

define('TEXT_INFO_HEADING_IMPORT', 'Importer');
define('TEXT_INFO_ATTRIBUTES_OPTION', 'Importer les spécificités d\'article comme caractéristique?');
define('TEXT_INFO_ATTRIBUTES_VALUE', 'Importer les valeurs d\'options comme valeurs de caractéristiques?');
define('TEXT_INFO_ATTRIBUTES', 'Attributs - reprendre les attributions comme caractéristiques de tous les articles?');
define('TEXT_INFO_ATTRIBUTES_DELETE', 'Attributs - enlever les attributions des attributs supprimés?');
define('TEXT_INFO_IMPORT_WAIT', 'Veuillez patienter...');
define('TEXT_INFO_IMPORT_FINISHED', 'Importation finalisée.');

?>

﻿<?php
/**********************************************
* file reworked and adadapted by noRiddle
*
* (c) noRiddle 03-2018
**********************************************/

if(!isset($_SESSION['customer_id'])) xtc_redirect(xtc_href_link(FILENAME_DEFAULT)); //redirect to start page if not logged in, noRiddle

// Include soap request class
include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'requestOem.php');
// Include view class
include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'render'.DIRECTORY_SEPARATOR.'qgroups'.DIRECTORY_SEPARATOR.'default.php');
// Include view class
include('nr_catalogue_files/extender.php');

class QuickGroupsExtender extends CommonExtender
{
    function FormatLink($type, $dataItem, $catalog, $renderer)
    {
        if ($type == 'vehicle')
            //BOC set link to content 1004 which includes vehicle.php, noRiddle
            //$link = 'vehicle.php?c='.$catalog.'&vid='.$renderer->vehicleid. '&ssd=' . $renderer->ssd;
            $link = xtc_href_link(FILENAME_CONTENT, 'coID=1004').'?c='.$catalog.'&vid='.$renderer->vehicleid. '&ssd=' . $renderer->ssd;
            //EOC set link to content 1004 which includes vehicle.php, noRiddle
        else
            //BOC set link to content 1007 which includes qdetails.php, noRiddle
            //$link = 'qdetails.php?c='.$catalog.'&gid='.$dataItem['quickgroupid']. '&vid=' . $renderer->vehicleid. '&ssd=' . $renderer->ssd;
            $link = xtc_href_link(FILENAME_CONTENT, 'coID=1007').'?c='.$catalog.'&gid='.$dataItem['quickgroupid']. '&vid=' . $renderer->vehicleid. '&ssd=' . $renderer->ssd;
            //EOC set link to content 1007 which includes qdetails.php, noRiddle

        return $link;
    }
}

// Create request object
$request = new GuayaquilRequestOEM($_GET['c'], $_GET['ssd'], Config::$catalog_data);
if (Config::$useLoginAuthorizationMethod) {
    $request->setUserAuthorizationMethod(Config::$userLogin, Config::$userKey);
}

// Append commands to request
$request->appendGetVehicleInfo($_GET['vid']);
$request->appendListQuickGroup($_GET['vid']);

// Execute request
$data = $request->query();

// Check errors
if ($request->error != '')
{
    echo $request->error;
}
else
{
		$vehicle = $data[0]->row;
        $groups = $data[1];

		//echo '<h1>'.CommonExtender::FormatLocalizedString('CarName', $vehicle['name']).'</h1>';
        //echo '<div class="col-md-4 col-sm-12 bord-e3">'; //will be close in guayaquillib/renderer/template.php
        echo '<h1>'
         //.'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.Config::$nr_icon_arr[$_GET['c']].'" alt ="" />&nbsp;'
         .'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['brand_icon'].'" alt ="'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].'" />&nbsp;'
         .CommonExtender::FormatLocalizedString('CarName', $vehicle['name'])
         .'</h1>';

         echo '<h2>'.CommonExtender::FormatLocalizedString('GroupDetails', $vehicle['name']).'</h2>'; //display also 'parts by groups', noRiddle
         
         echo '<div class="right-2flts nr-highlightbox">'; //will be close in guayaquillib/renderer/template.php

		//BOC new top menu, see guayaquillib/render/template.php, noRiddle
        //echo '<div id="pagecontent">';
        
        $renderer = new GuayaquilQuickGroupsList(new QuickGroupsExtender());
        echo $renderer->Draw($groups, $_GET['c'], $_GET['vid'], $_GET['ssd']);
        
        //echo '<pre>'.$data[0]->row['brand'].'</pre>';
        //echo '<pre>'.print_r($data, true).'</pre>';

        //echo '</div>'; //end pagecontent
        //EOC new top menu, noRiddle
}
?>
<?php
/************************************************************************
* language file for imageslider                                         *
* plugin derived from imageslider by Hetfield                           *
* based on                                                              *
* - responsive kenburner slider by codecanyon, licence necessary        *
*                                                                       *
* copyright (c) for all changes: noRiddle of webdesign-endres.de 2013   *
************************************************************************/

$slider_measures = array();
$slider_flyin_measures = array();
if((isset($data20) && !empty($data20)) && !isset($dataC20) && file_exists('../includes/local/configure.php')) {
    switch($shop) {
        case 'audi'.$shop_suffix:
            $slider_measures[$shop] = array('1420px', '250px');
            $slider_flyin_measures[$shop] = array('478', '146');
        break;
        default:
            $slider_measures[$shop] = array('1420px', '250px');
            $slider_flyin_measures[$shop] = array('478', '146');
        break;
    }
} else if((isset($dataC20) && !empty($dataC20)) || !file_exists('../includes/local/configure.php')) {
    $slider_measures[$shop] = array('1580px', '250px');
    $slider_flyin_measures[$shop] = array('478', '146');
}

define('HEADING_TITLE_II', 'CoKeBuMo-Imageslider ed. 1.5.2');
define('TABLE_HEADING_IMAGESLIDERS', 'Imageslider');
define('TABLE_HEADING_SPEED', 'Geschwindigkeit');
define('TABLE_HEADING_SORTING', 'Sortierung');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Aktion');
define('TEXT_HEADING_NEW_IMAGESLIDER', 'Neues Bild');
define('TEXT_HEADING_EDIT_IMAGESLIDER', 'Bild bearbeiten');
define('TEXT_HEADING_DELETE_IMAGESLIDER', 'Bild l&ouml;schen');
define('TEXT_IMAGESLIDERS', 'Bild:');
define('TEXT_IMAGE_DIMENSIONS', 'Original-Bildma&szlig;e: ');
define('TEXT_DATE_ADDED', 'hinzugef&uuml;gt am:');
define('TEXT_LAST_MODIFIED', 'letzte &Auml;nderung am:');
define('TEXT_IMAGE_NONEXISTENT', 'BILD NICHT VORHANDEN');
define('TEXT_NEW_INTRO', 'Bitte legen Sie das neue Bild mit allen relevanten Daten an.');
define('TEXT_EDIT_INTRO', 'Bitte f&uuml;hren Sie alle notwendigen &Auml;nderungen durch');
define('TEXT_IMAGESLIDERS_TITLE', 'Title f&uuml;r Bild:');
define('TEXT_IMAGESLIDERS_NAME', 'Name für Bildeintrag:');
define('TEXT_IMAGESLIDERS_IMAGE', 'Bild:');
define('TEXT_IMAGESLIDERS_URL', 'Bild verlinken <span style="color:#B30000; font-size:9px;"> (nur m&ouml;glich wenn kein Video implementiert wurde (s.u.), im Fall einer Video-Implementation "Kein Link" wählen)</span>');
define('TEXT_TARGET', 'Zielfenster:');
define('TEXT_TYP', 'Linkart:');
define('TEXT_URL', 'Linkadresse:');
define('TEXT_IMAGESLIDERS_ALT', 'alt-tag f&uuml;r Bild (Google ?):');
define('TEXT_IMAGESLIDERS_VIDEO_URL', 'Embed-URL eines gewünschten Videos f&uuml;r dieses Bild. "YouTube" und "Vimeo" werden unterstützt.');
define('TEXT_IMAGESLIDERS_VIDEO_DESC', 'Video-Beschreibung:');
define('TEXT_IMAGESLIDERS_CREDITS_DESC', 'Erkl&auml;rungen und Credits:');
define('TEXT_IMAGESLIDERS_CREDITS_EXPL', '<p>Based on kenburner by codecanyon, code widely modified for better text-effects and other changes by <a href="http://www.revilonetz.de" target="_blank">noRiddle</a> ed. 1.5.2 | 03-2013 - '.date('Y').'</p><p>Bei Fragen zu dem Interface oder auch Sonderwünschen zu Erweiterungen wende dich an <a class="imgslider-link" href="http://www.revilonetz.de/kontakt" target="_blank">noRiddle</a></p>');
define('TXT_BUTTON_COPY_SLIDER', 'Slider kopieren von Shop %s');
define('TXT_IDS_RANGE', 'Hier Ids von bis eingeben in der Form X-Y. (Beispiel:10-15)<br />Wenn das Feld leer gelassen wird werden alle Bilder kopiert.');
define('TXT_COPY_SLIDER_EXPL', 'Es werden momentan nur aktivierte Bilder aus dem Slider des Shops %1$s kopiert.<br />ACHTUNG: %1$s hat evtl. andere Maße, bitte erst überprüfen.');
define('TXT_BUTTON_NEWIMAGE', 'Neues Bild einfügen');
define('TXT_BUTTON_COPY_IMAGE', 'Bild mit allen Einstellungen kopieren');
define('TXT_DEFAULT_COPY_IMAGE', 'Bild auswählen');

define('NONE_TARGET', 'Kein Ziel festlegen');
define('TARGET_BLANK', '_blank');
define('TARGET_TOP', '_top');
define('TARGET_SELF', '_self');
define('TARGET_PARENT', '_parent');
define('TYP_NONE', 'Kein Link');
define('TYP_PRODUCT', 'Link zum Produkt (bitte nur die productsID bei Linkadresse eintragen)');
define('TYP_CATEGORIE', 'Link zur Kategorie (bitte nur die catID bei Linkadresse eintragen. <span style="color:#c00">!! Bei Unterkategorien immer nur die letzte ID ohne Unterstrich davor !!</span>)');
define('TYP_CONTENT', 'Link zur Contentseite (bitte nur die coID bei Linkadresse eintragen)');
define('TYP_INTERN', 'Interner Shoplink (z.B. account.php oder newsletter.php)');
define('TYP_EXTERN', 'Externer Link (z.B. http://www.externerlink.com)');
define('TEXT_IMAGESLIDERS_DESCRIPTION', 'Bildbeschreibung:');
define('INFO_IMAGESLIDERS_DESCRIPTION', 'Wenn Bilder eingefügt werden darf die Breite höchstens '.$slider_flyin_measures[$shop][0].', die Höhe höchstens '.$slider_flyin_measures[$shop][1].' betragen.<br />Bitte unbedingt Bildmaße im Filebrowser löschen.<br />Im Dropdown "Stil" können diverse Schriftgrößen ausgewählt werden.<br />Für Schriftgrößen bitte nicht den Dropdown "Größe" verwenden.');
define('TEXT_DELETE_INTRO', 'Sind Sie sicher, dass Sie dieses Bild l&ouml;schen m&ouml;chten?');
define('TEXT_DELETE_IMAGE', 'Bild ebenfalls l&ouml;schen?');
define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Fehler: Das Verzeichnis %s ist schreibgesch&uuml;tzt. Bitte korrigieren Sie die Zugriffsrechte zu diesem Verzeichnis!');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Fehler: Das Verzeichnis %s existiert nicht!');
define('TEXT_DISPLAY_NUMBER_OF_IMAGESLIDERS', 'Angezeigt werden <b>%d</b> bis <b>%d</b> (von insgesamt <b>%d</b> Bildern in der Slideshow)');  //'Anzahl Bilder in Slideshow');
define('IMAGE_ICON_STATUS_GREEN', 'Aktiv');
define('IMAGE_ICON_STATUS_GREEN_LIGHT', 'Aktivieren');
define('IMAGE_ICON_STATUS_RED', 'Nicht aktiv');
define('IMAGE_ICON_STATUS_RED_LIGHT', 'Deaktivieren');
define('ACTIVE', 'aktiv');
define('NOTACTIVE', 'nicht aktiv');

define('INFO_IMAGE_NAME', 'Hier bitte einen Namen f&uuml;r das Bild eingeben um sp&auml;ter im Fall des Editierens das Bild am Namen zu erkennen.');
define('INFO_IMAGE_SORT', 'Hier eine ganze Zahl eingeben womit die Reihenfolge der Bilderanzeige bestimmt wird.');
define('INFO_IMAGE_ACTIVE', 'Hier k&ouml;nnen Sie das Bild aktiv oder inaktiv schalten.<br>Die anderen Bilder die aktiv geschaltet sind, werden davon nicht beieinflusst.');
define('INFO_IMAGE_UPLOAD', 'Der Name des hochzuladenden Bildes darf nicht zu lang sein (nicht mehr als 38 Zeichen ohne Dateiendung).<br>F&uuml;r jede Sprache in der der Slider dargestellt werden soll muß ein Bild hochgeladen werden.<br><span style="color:#c00;">!! Maße f&uuml;r Ihren Shop !!</span><br>Bildbreite: '.$slider_measures[$shop][0].'<br>Bildh&ouml;he: '.$slider_measures[$shop][1]);
define('INFO_IMAGE_TITLE', 'Der Title des Bildes wird zur Referenzierung für den Text der im Bild dargestellt wird benötigt.<br>Bitte kurzen eindeutigen Title angeben.<br>Title darf nur einmal vorkommen, also bei anderen Bildern andere Title benutzen.<br>Am besten Sie benutzen immer den gleichen Title und nummerieren diesen durch.<br>z.B.:<br>cap1, nächstes Bild cap2 usw.<br /><span style="color:#c00">Wenn kein Text (Bildbeschreibung) angegeben wird, hier auch keinen Title vergeben !</span>');
define('INFO_IMAGE_ALT', 'Hier den Text eingeben der im alt-tag des images stehen soll.<br />Das k&ouml;nnte f&uuml;r Suchmaschinenergebnisse (SERP) wichtig sein.');

//***BOC KenBurns Codecanyon***
define('TEXT_IMAGESLIDERS_DATA_ATTRIBUTES', 'Effekte f&uuml;r den Slider <span style="color:#B30000; font-size:9px;">(bei Wahl eines Wertes in einer Sprache werden die anderen Sprachen ebenfalls auf den Wert gesetzt)</span>');
define('INFO_IMAGE_DATA_ATTRIBUTES', 'Von den hier dargestellten Optionen muß jeweils eine ausgew&auml;hlt werden wenn der KenBurns-Effekt aktiviert ist.<br>Die Textfelder m&uuml;ssen ebenfalls ausgef&uuml;llt werden (siehe dort).');
define('INFO_IMAGE_CAPTION_ATTRIBUTES', 'Bitte darauf achten, daß die Effekte zu dem Darstellungsort passen (s.u.)');
define('INFO_IMAGE_VIDEO_URL', 'Im Falle das vorliegende Bild ein Video enthalten soll, hier die Embed-URL eingeben.<br />Embed-URL bedeutet die URL die in dem von "YouTube" oder "Vimeo" zur Verfügung gestellten Einbettungscode enthalten ist. <span style="color:#c00;">! Nur die URL, nicht den ganzen Embed-Code !</span><br />Sie k&ouml;nnen das Anzeigen von "related videos" nach Abspielen des Videos unterdrücken indem Sie folgendes (bei YouTube-Videos) an die URL anhängen: <i>&amp;amp;rel=0</i><br />Es gibt noch weitere Parameter mit Hilfe derer man das Verhalten des Videoplayers beeinflussen kann.<br />Schauen Sie dazu bitte in die YouTube- und/ oder Vimeo-Spezifikationen.');

define('TXT_GENERAL_KENBURNS', 'Schalten sie den KenBurns-Effekt an oder aus');
define('TXT_IF_KENBURNS_ON', 'Wenn Kenburns ausgeschaltet ist, sind die darunter stehenden Einstellung nicht alle nötig (siehe dort)');
define('TXT_GENERAL_TRANSITION', 'Der &Uuml;bergang der Bilder, "fade" oder "slide" (<span style="color:#c00;">! muß immer angegeben werden</span>)');
define('TXT_GENERAL_STARTALIGN', 'Beginn des KenBurns-Effektes, horizontal | vertikal');
define('TXT_GENERAL_ENDALIGN', 'Ende des KenBurns-Effektes, horizontal | vertikal');
define('TXT_GENERAL_ZOOM_AND_PAN', 'Zoom-Einstellungen und Dauer der Bewegung');
define('TXT_GENERAL_CAPTION', 'Stellen Sie ein wo der Text erscheint und mit welchem Effekt');
define('TEXT_IMAGESLIDERS_CAPTION_ATTRIBUTES', 'Textattribute <span style="color:#B30000; font-size:9px;">(bei Wahl eines Wertes in einer Sprache werden die anderen Sprachen ebenfalls auf den Wert gesetzt)</span>');
define('TXT_CAPTION_DIRECTION_AND_EFFECT', 'Die Effekte und Erscheinungsorte die zusammen passen stehen nebeneinander');
define('TXT_CAPTION_LEFT', 'Text links');
define('TXT_CAPTION_FADE_RIGHT', 'slidet von links nach rechts rein');
define('TXT_CAPTION_RIGHT', 'Text rechts');
define('TXT_CAPTION_FADE_LEFT', 'slidet von rechts nach links rein');
define('TXT_CAPTION_TOP', 'Text oben');
define('TXT_CAPTION_FADE_DOWN', 'slidet von oben nach unten rein');
define('TXT_CAPTION_BOTTOM', 'Text unten');
define('TXT_CAPTION_FADE_UP', 'slidet von unten nach oben rein');
define('TXT_CAPTION_FADE', 'wird durch "fade"-Effekt eingeblendet, passt immer');
define('TXT_KENBURN_ON', '<b>An</b>');
define('TXT_KENBURN_OFF', '<b>Aus</b>');                                  
define('TXT_TRANSITION_FADE', '<b>Fade</b>');
define('TXT_TRANSITION_SLIDE', '<b>Slide</b>');
define('TXT_STARTALIGN_LEFTTOP', '<b>links | oben</b>');
define('TXT_STARTALIGN_LEFTCENTER', '<b>links | zentrum</b>');
define('TXT_STARTALIGN_LEFTBOTTOM', '<b>links | unten</b>');
define('TXT_STARTALIGN_CENTERTOP', '<b>zentrum | oben</b>');
define('TXT_STARTALIGN_CENTERCENTER', '<b>zentrum | zentrum</b>');
define('TXT_STARTALIGN_CENTERBOTTOM', '<b>zentrum | unten</b>');
define('TXT_STARTALIGN_RIGHTTOP', '<b>rechts | oben</b>');
define('TXT_STARTALIGN_RIGHTCENTER', '<b>rechts | zentrum</b>');
define('TXT_STARTALIGN_RIGHTBOTTOM', '<b>rechts | unten</b>');
define('TXT_STARTALING_RANDOM', '<b>zuf&auml;llig</b>');
define('TXT_ENDALIGN_LEFTTOP', '<b>links | oben</b>');
define('TXT_ENDALIGN_LEFTCENTER', '<b>links | zentrum</b>');
define('TXT_ENDALIGN_LEFTBOTTOM', '<b>links | unten</b>');
define('TXT_ENDALIGN_CENTERTOP', '<b>zentrum | oben</b>');
define('TXT_ENDALIGN_CENTERCENTER', '<b>zentrum | zentrum</b>');
define('TXT_ENDALIGN_CENTERBOTTOM', '<b>zentrum | unten</b>');
define('TXT_ENDALIGN_RIGHTTOP', '<b>rechts | oben</b>');
define('TXT_ENDALIGN_RIGHTCENTER', '<b>rechts | zentrum</b>');
define('TXT_ENDALIGN_RIGHTBOTTOM', '<b>rechts | unten</b>');
define('TXT_ENDALIGN_RANDOM', '<b>zuf&auml;llig</b>');
define('TXT_ZOOM', '<b>Raus- oder rein-zoomen</b>, einen der Werte "out | in | random" eingeben');
define('TXT_ZOOMFACT', 'Zoomfaktor (bitte eine einstellige Kommazahl (! Komma muß ein Punkt sein) eingeben, am Besten zwischen 1 und 2). Wenn obiger Wert auf "out" steht muß der Zoomfaktor entsprechend hoch sein oder das Bild größer als der HTML-Container in dem es liegt.');
define('TXT_PANDURATION', 'Dauer der Bewegung in Sekunden (<span style="color:#c00;">! muß immer angegeben werden</span>) (wenn nicht angegeben wird der Wert der als timer für Bildwechsel in <i>general.js.php</i> angegeben ist (5) benutzt)');
?>
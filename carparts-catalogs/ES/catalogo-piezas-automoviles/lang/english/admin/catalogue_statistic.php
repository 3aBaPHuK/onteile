<?php
/***************************************************
* file: catalogue_statistic.php
* use: language file
*      
* (c) noRiddle 08-2018
***************************************************/

define('SC_PLEASE_CHOOSE_TXT', 'Please choose');
define('SC_SEARCH_ONLY_VIN_TXT', 'Show only searches by VIN');
define('SC_SEARCH_ONLY_WIZARD_TXT', 'Show only searches by parameter');
define('SC_SEARCH_VIN_FOUND_TXT', 'Show only found VINs');
define('SC_SEARCH_VIN_NOT_FOUND_TXT', 'Show only not found VINs');

define('SC_TABLE_HEADING_CUST_ID', 'customers_id');
define('SC_TABLE_HEADING_CUST_IP', 'customer IP');
define('SC_TABLE_HEADING_DATE_ADDED', 'date_added');
define('SC_TABLE_HEADING_BRAND', 'Brand');
define('SC_TABLE_HEADING_VINORWIZ', 'Search VIN or parameter');
define('SC_TABLE_HEADING_VIN', 'VIN');
define('SC_TABLE_HEADING_VIN_FOUND', 'VIN found ?');

define('SC_TEXT_SORT_ASC', 'sort ascending');
define('SC_TEXT_SORT_DESC', 'sort descending');

define('SC_VARIOUS_FILTERS_TXT', 'Various filters');
define('SC_FILTER_VINORWIZ_TXT', 'Filter search for:');
define('SC_FILTER_BRAND_TXT', 'Filter brand:');
define('SC_FILTER_CUSTMAIL_TXT', 'Filter customer (mail-address):');
define('SC_SHOW_BOUGHT_VINS', 'Show bought credits: ');
define('SC_FILTER_FROM_DATE_TXT', 'from date (incl.):');
define('SC_FILTER_TO_DATE_TXT', 'to date (incl.):');
define('SC_BOUGHT_TEXT', ' | bought: ');

define('SC_TAB_HEADING_STATISTIC', 'Statistic');
define('SC_TAB_HEADING_TABLE', 'Table');
define('SC_CLICK_ON_FOR_STATS_TXT', 'If data has been loaded via the fikters above, click on the tab "Statistic" to load the statistics.<br />If there is no data in the tab "Table" initiate filters above (e.g. via the button "Go")');
define('SC_GRAPH_HEADING_SEARCHBY', 'Searches %s %s to %s');
define('SC_GRAPH_HEADING_BRAND_RELATIONS', '%s Saerches by brand %s to %s');

define('SC_TEXT_DISPLAY_HOW_MANY_OF_HO_MANY', 'Indicated are <b>%d</b> to <b>%d</b> (of total <b>%d</b> searches)');
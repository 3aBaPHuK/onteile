<?php
/* -----------------------------------------------------------------------------------------
   $Id: order_mail_step.php 9865 2016-05-25 11:30:53Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_MAIL_STEP_TEXT_TITLE', 'Confirmation de commande manuelle');
define('MODULE_ORDER_MAIL_STEP_TEXT_DESCRIPTION', 'Confirmation de commande manuelle.<br/><b>Attention :</b>Vous pouvez utiliser uniquement des moyens de paiement qui ne demanderont pas de paiement au client lors de la fin de la transaction.');

define('MODULE_ORDER_MAIL_STEP_STATUS_TITLE', 'Statut');
define('MODULE_ORDER_MAIL_STEP_STATUS_DESC', 'Statut de module');

define('MODULE_ORDER_MAIL_STEP_ORDERS_STATUS_ID_TITLE', 'Statut de commande');
define('MODULE_ORDER_MAIL_STEP_ORDERS_STATUS_ID_DESC', 'Quel statut de commande souhaitez-vous mettre lors de l\'envoi de la confirmation de la commande ?');
?>

<?php
/* --------------------------------------------------------------
   $Id: geo_zones.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(geo_zones.php,v 1.11 2003/05/07); www.oscommerce.com 
   (c) 2003	 nextcommerce ( geo_zones.php,v 1.4 2003/08/1); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Zonas fiscales');

define('TABLE_HEADING_COUNTRY', 'País');
define('TABLE_HEADING_COUNTRY_ZONE', 'Estado federal');
define('TABLE_HEADING_TAX_ZONES', 'Zonas fiscales');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_INFO_HEADING_NEW_ZONE', 'Nueva zona fiscal');
define('TEXT_INFO_NEW_ZONE_INTRO', 'Por favor, introduzca la nueva zona fiscal con todos los datos relevantes.');

define('TEXT_INFO_HEADING_EDIT_ZONE', 'Editar zona fiscal');
define('TEXT_INFO_EDIT_ZONE_INTRO', 'Por favor, haga todos los cambios necesarios.');

define('TEXT_INFO_HEADING_DELETE_ZONE', 'Borrar zona fiscal');
define('TEXT_INFO_DELETE_ZONE_INTRO', '¿Está seguro de que quiere borrar esta zona fiscal?');

define('TEXT_INFO_HEADING_NEW_SUB_ZONE', 'Nueva subzona');
define('TEXT_INFO_NEW_SUB_ZONE_INTRO', 'Por favor, introduzca la nueva subzona con todos los datos relevantes.');

define('TEXT_INFO_HEADING_EDIT_SUB_ZONE', 'Editar subzona');
define('TEXT_INFO_EDIT_SUB_ZONE_INTRO', 'Por favor, haga todos los cambios necesarios.');

define('TEXT_INFO_HEADING_DELETE_SUB_ZONE', 'Borrar subzona');
define('TEXT_INFO_DELETE_SUB_ZONE_INTRO', '¿Está seguro de que quiere borrar esta subzona?');

define('TEXT_INFO_DATE_ADDED', 'agregado el:');
define('TEXT_INFO_LAST_MODIFIED', 'última modificación:');
define('TEXT_INFO_ZONE_NAME', 'Nombre de zona fiscal:');
define('TEXT_INFO_NUMBER_ZONES', 'Número de zonas fiscales:');
define('TEXT_INFO_ZONE_DESCRIPTION', 'Descripción:');
define('TEXT_INFO_COUNTRY', 'País:');
define('TEXT_INFO_COUNTRY_ZONE', 'Estado federal:');
define('TYPE_BELOW', 'Todos los estados federales');
define('PLEASE_SELECT', 'Todos los estados federales');
define('TEXT_ALL_COUNTRIES', 'Todos los países');

define('TEXT_INFO_ZONE_INFO', '¿Debe mostrarse el aviso de aduana para esta zona fiscal?');
define('TEXT_INFO_ZONE_INFO_DEFAULT', 'Para esta zona fiscal, se muestra el aviso de aduana.');
?>

<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_clicks.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_clicks.php, v 1.2 2003/02/12);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Statistique des clics du programme affilié ');

define('TABLE_HEADING_ONLINE', 'Statistique des clics');
define('TABLE_HEADING_AFFILIATE_USERNAME', 'Partenaire');
define('TABLE_HEADING_IPADDRESS', 'adresse IP');
define('TABLE_HEADING_ENTRY_TIME', 'Temps');
define('TABLE_HEADING_BROWSER', 'Navigateur');
define('TABLE_HEADING_ENTRY_DATE', 'Date');
define('TABLE_HEADING_CLICKED_PRODUCT', 'Produit coché');
define('TABLE_HEADING_REFERRAL_URL', 'Référence URL');

define('TEXT_NO_CLICKS', 'Jusqu\'à maintenant pas de statistique de clique a été enregistré.');
define('TEXT_DISPLAY_NUMBER_OF_CLICKS', 'Montrant <b>%d</b> de <b>%d</b> (au total <b>%d</b> clics)');
?>

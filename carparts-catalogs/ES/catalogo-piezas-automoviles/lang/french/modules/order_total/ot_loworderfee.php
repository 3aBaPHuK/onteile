<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_loworderfee.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ot_loworderfee.php,v 1.2 2002/04/17); www.oscommerce.com 
   (c) 2003	 nextcommerce (ot_loworderfee.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_LOWORDERFEE_TITLE', 'Surtaxe de quantité manquante');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESCRIPTION', 'Surtaxe en cas d\'infériorité de commande minimale');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_TITLE', 'Afficher surtaxe de quantité manquante');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_DESC', 'Souhaitez-vous afficher la surtaxe de quantité manquante');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_SORT_ORDER_TITLE', 'Ordre de triage');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_SORT_ORDER_DESC', 'Ordre d\'affichage');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_TITLE', 'Permettre surtaxe de quantité manquante');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_DESC', 'Souhaitez-vous permettre les surtaxes de quantité manquante ?');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_TITLE', 'Surtaxe de quantité manquante pour commandes inférieures à');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_DESC', 'Surtaxe de quantité manquante sera ajoutée pour des commandes inférieures à cette valeur');

define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_INTERNATIONAL_TITLE', 'Surtaxe de quantité manquante pour commandes internationales inférieures à');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_INTERNATIONAL_DESC', 'Surtaxe de quantité manquante sera ajoutée pour des commandes internationales inférieures à cette valeur' );
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_TITLE', 'Surtaxe');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_DESC', 'Surtaxe de quantité manquante');

define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_INTERNATIONAL_TITLE', 'Surtaxe internationale');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_INTERNATIONAL_DESC', 'Surtaxe de quantité manquante internationale');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_TITLE', 'Calculer surtaxe de quantité manquante par zones');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_DESC', 'Surtaxe de quantité manquante pour des commandes qui ont été envoyées à cet endroit');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_TITLE', 'Catégorie fiscale');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_DESC', 'Utiliser la catégorie fiscale suivante pour la surtaxe de quantité manquante');
?>

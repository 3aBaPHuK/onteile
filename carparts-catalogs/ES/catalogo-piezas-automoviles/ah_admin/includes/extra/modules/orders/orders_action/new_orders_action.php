<?php
/*********************************************************************
* file: new_orders_action.php
* use: fill comments field with order_status_comments
* (c) noRiddle 12-2016
*********************************************************************/

//BOC order_status_comments, noRiddle
if(isset($_POST['osc_id'])) {
    if($_POST['osc_id'] != '0') {
        $get_order_status_comments_text_query = xtc_db_query("SELECT order_status_comments_text 
                                                                FROM ".TABLE_ORDER_STATUS_COMMENTS_TXT." 
                                                               WHERE order_status_comments_id = '".(int)$_POST['osc_id']."'
                                                                 AND language_id = '".(int)$_POST['sess_lang']."'
                                                            ");
        $get_order_status_comments_text = xtc_db_fetch_array($get_order_status_comments_text_query);
        
        echo $get_order_status_comments_text['order_status_comments_text'];
    }
    exit();
}
//EOC order_status_comments, noRiddle

//BOC edit gm_price_status
if(isset($_POST['nr_prod_stat']) && $_POST['nr_prod_stat'] != '0') {
    $nr_prod_updated = false;
    $nr_p_upd_arr = array('gm_price_status' => '2');
    
    if($_POST['nr_prod_stat'] == '1') {
        $nr_p_upd_arr = array_merge($nr_p_upd_arr, array('products_replace' => ''));
        
        $nr_pdesc_upd_arr_de = array('products_description' => 'Das Teil ist entfallen');
        $nr_pdesc_upd_arr_en = array('products_description' => 'The part is no more available');

        $nr_prod_updated = true;
    } else if($_POST['nr_prod_stat'] == '2') {
        if(isset($_POST['nr_prod_replace']) && $_POST['nr_prod_replace'] != '') {
            $nr_repl_part_ex = false;
            
            if(!preg_match('#[\s-_\"\']+#', $_POST['nr_prod_replace'])) {
                $nr_p_upd_arr = array_merge($nr_p_upd_arr, array('products_replace' => $_POST['nr_prod_replace']));
            
                $nr_new_prod_qu = xtc_db_query("SELECT products_id FROM ".TABLE_PRODUCTS." WHERE products_model = '".$_POST['nr_prod_replace']."'");
                if(xtc_db_num_rows($nr_new_prod_qu) > 0) {
                    $nr_repl_part_ex = true;
                    
                    $nr_new_prod_arr = xtc_db_fetch_array($nr_new_prod_qu);
                    $nr_pdesc_upd_arr_de = array('products_description' => 'Das Teil wurde ersetzt durch <a href="product_info.php?'.xtc_product_link($nr_new_prod_arr['products_id']).'">'.$_POST['nr_prod_replace'].'</a>');
                    $nr_pdesc_upd_arr_en = array('products_description' => 'The part was replaced with <a href="product_info.php?'.xtc_product_link($nr_new_prod_arr['products_id']).'">'.$_POST['nr_prod_replace'].'</a>');

                    $nr_prod_updated = true;
                }
            } else {
                $nr_prod_updated = false;
            }
        } else {
            $nr_prod_updated = false;
        }
        
    }
    
    if($nr_prod_updated) {
        if(xtc_db_perform(TABLE_PRODUCTS, $nr_p_upd_arr, 'update', "products_id = '".(int)$_POST['nr_prod_id']."'")
            && xtc_db_query("UPDATE ".TABLE_PRODUCTS." SET set_by_admin = CONCAT(set_by_admin, '//', '".$_SESSION['customer_first_name']." ".$_SESSION['customer_last_name']."|".date('d-m-Y H:i:s')."') WHERE products_id = '".(int)$_POST['nr_prod_id']."'")
            && xtc_db_perform(TABLE_PRODUCTS_DESCRIPTION, $nr_pdesc_upd_arr_de, 'update', "products_id = '".(int)$_POST['nr_prod_id']."' and language_id = '2'")
            && xtc_db_perform(TABLE_PRODUCTS_DESCRIPTION, $nr_pdesc_upd_arr_en, 'update', "products_id = '".(int)$_POST['nr_prod_id']."' and language_id = '1'")
        ) {
            $messageStack->add_session(sprintf(NR_EDITPROD_EDIT_SUCCESS, $_POST['nr_prod_model']), 'success');
        }
    } else {
        if($nr_repl_part_ex === false) {
            $messageStack->add_session(NR_EDITPROD_ALERT_REPLPART_NO_EXISTS, 'warning');
        } else {
            $messageStack->add_session(NR_EDITPROD_ALERT_NO_SPECIALCHARS, 'warning');
        }
    }
    
    //delete DB cache if activated
    if(defined('DB_CACHE') && DB_CACHE == 'true') {
        clear_dir(SQL_CACHEDIR);
    }

    xtc_redirect(xtc_href_link(FILENAME_ORDERS, xtc_get_all_get_params(array ('action')).'action=edit'));
}
//EOC edit gm_price_status

//BOC new UVP (RRP) price nr_prodprice_replace
if(isset($_POST['nr_prodprice_replace']) && $_POST['nr_prodprice_replace'] != '' && $_POST['nr_prodprice_replace'] > 0) {
    $post_nr_prod_price_replace = str_replace(',', '.', $_POST['nr_prodprice_replace']);
    
    //get old price
    $before_upd_price_qu = xtc_db_query("SELECT products_price FROM ".TABLE_PRODUCTS." WHERE products_id = ".(int)$_POST['nr_prod_id']);
    $before_upd_price_arr = xtc_db_fetch_array($before_upd_price_qu);
    $before_upd_price = $before_upd_price_arr['products_price'];
    
    //get personal_offers prices
    $pers_off_relations_arr = array();
    //$pers_off_col_query = xtc_db_query("SHOW COLUMNS FROM csv_import_aux LIKE 'personal_offers_by_customers_status_%'");
    $pers_off_col_query = xtc_db_query("SELECT DISTINCT customers_status_id FROM ".TABLE_CUSTOMERS_STATUS);
    while($pers_off_cols = xtc_db_fetch_array($pers_off_col_query)) {
        //$ie_personal_offers_array[] = $pers_off_cols['Field'];
        $ie_personal_offers_array[] = 'personal_offers_by_customers_status_'.$pers_off_cols['customers_status_id'];
    }
    foreach($ie_personal_offers_array as $table) {
        $po_price_qu = xtc_db_query("SELECT personal_offer, quantity FROM ".$table." WHERE products_id = ".(int)$_POST['nr_prod_id']);
        while($po_price_arr = xtc_db_fetch_array($po_price_qu)) {
            $pers_off_relations_arr[$table][$po_price_arr['quantity']] = $po_price_arr['personal_offer'];
        }
    }
    
    //write array with calculation factors
    $po_fact_arr = array();
    foreach($pers_off_relations_arr as $tbl => $qty_arr) {
        foreach($qty_arr as $qty => $poprice) {
            $rel_perc = 100*$poprice/$before_upd_price;
            $rel_fact = $rel_perc/100;
            $po_fact_arr[$tbl][$qty] = $rel_fact;
        }
    }
    
    //update table products with new price
    $new_rrp_price = xtc_round($post_nr_prod_price_replace, PRICE_PRECISION);
    if(xtc_db_query("UPDATE ".TABLE_PRODUCTS." SET products_price = ".$new_rrp_price." WHERE products_id = ".(int)$_POST['nr_prod_id'])) {
        $messageStack->add_session(sprintf(NR_EDITPRODPRICE_EDIT_SUCCESS, $_POST['nr_prod_model']), 'success');
    }
    //update all personal offers with new prices
    foreach($po_fact_arr as $po_tble => $quant_arr) {
        foreach($quant_arr as $quant => $new_price_fact) {
            $new_price_temp = $new_rrp_price*$new_price_fact;
            $new_price = xtc_round($new_price_temp, PRICE_PRECISION);
            xtc_db_query("UPDATE ".$po_tble." SET personal_offer = ".$new_price." WHERE products_id = ".(int)$_POST['nr_prod_id']." AND quantity = '".$quant."'");
        }
    }
    
    //delete DB cache if activated
    if(defined('DB_CACHE') && DB_CACHE == 'true') {
        clear_dir(SQL_CACHEDIR);
    }
    
    xtc_redirect(xtc_href_link(FILENAME_ORDERS, xtc_get_all_get_params(array ('action')).'action=edit'));
}

//EOC new UVP (RRP) price
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_CUSTOMERS_CID_STATUS_TITLE', 'Estado');
define('MODULE_CUSTOMERS_CID_STATUS_DESC', 'Estado del módulo');
define('MODULE_CUSTOMERS_CID_STATUS_INFO', 'Activo');
define('MODULE_CUSTOMERS_CID_TEXT_TITLE', 'Número de cliente automático');
define('MODULE_CUSTOMERS_CID_TEXT_DESCRIPTION', 'Número de cliente automático al registrar una cuenta de cliente.');
define('MODULE_CUSTOMERS_CID_NEXT_TITLE', 'Siguiente número de cliente');
define('MODULE_CUSTOMERS_CID_NEXT_DESC', 'Cuando se registre, se le asignará este número como siguiente.');
define('MODULE_CUSTOMERS_CID_FORMAT_TITLE', 'Formato de número de cliente');
define('MODULE_CUSTOMERS_CID_FORMAT_DESC', 'Esquema de estructura: {n}=número secuencial, {d}=Día, {m}Mes, {y}=Año, <br>p.ej. "KD{n}-{d}-{m}-{y}" resulta en "KD99-28-02-2007"');

?>

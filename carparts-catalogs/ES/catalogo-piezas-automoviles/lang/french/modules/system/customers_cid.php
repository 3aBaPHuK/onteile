<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_CUSTOMERS_CID_STATUS_TITLE', 'Statut');
define('MODULE_CUSTOMERS_CID_STATUS_DESC', 'Statut de module');
define('MODULE_CUSTOMERS_CID_STATUS_INFO', 'Actif');
define('MODULE_CUSTOMERS_CID_TEXT_TITLE', 'Numéro client automatique');
define('MODULE_CUSTOMERS_CID_TEXT_DESCRIPTION', 'Numéro client automatique pour l\'inscription d\'un compte client');
define('MODULE_CUSTOMERS_CID_NEXT_TITLE', 'Prochain numéro client');
define('MODULE_CUSTOMERS_CID_NEXT_DESC', 'Lors d\'une inscription ce numéro sera attribué la prochaine fois.');
define('MODULE_CUSTOMERS_CID_FORMAT_TITLE', 'Format numéro client');
define('MODULE_CUSTOMERS_CID_FORMAT_DESC', 'Schéma de construction: {n}=numéro séquentiel, {d}=jour, {m}=mois, {y}=année, <br>p.ex. "KD{n}-{d}-{m}-{y}" fait "KD99-28-02-2007"');

?>

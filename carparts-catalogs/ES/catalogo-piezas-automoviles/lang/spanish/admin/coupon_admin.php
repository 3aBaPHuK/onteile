<?php
/* -----------------------------------------------------------------------------------------
   $Id: coupon_admin.php 10755 2017-06-02 13:29:46Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(coupon_admin.php,v 1.1.2.5 2003/05/13); www.oscommerce.com
   (c) 2006 XT-Commerce

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c) Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('TOP_BAR_TITLE', 'Estadística');
define('HEADING_TITLE', 'Cupones de descuento');
define('HEADING_TITLE_STATUS', 'Estado : ');
define('TEXT_CUSTOMER', 'Cliente:');
define('TEXT_COUPON', 'Nombre del cupón');
define('TEXT_COUPON_ALL', 'Todos los cupones');
define('TEXT_COUPON_ACTIVE', 'Cupones activos');
define('TEXT_COUPON_INACTIVE', 'Cupones inactivos');
define('TEXT_SUBJECT', 'Asunto:');
define('TEXT_FROM', 'de:');
define('TEXT_FREE_SHIPPING', 'libre de costos de envío');
define('TEXT_MESSAGE', 'Mensaje:');
define('TEXT_SELECT_CUSTOMER', 'Seleccionar cliente');
define('TEXT_ALL_CUSTOMERS', 'Todos los clientes');
define('TEXT_NEWSLETTER_CUSTOMERS', 'Todos los suscriptores del newsletter');
define('TEXT_CONFIRM_DELETE', '¿Realmente debe anularse este cupón?');

define('TEXT_TO_REDEEM', 'Puede canjear el cupón con su pedido. Para aquello introduzca su código de cupón en el campo correspondiente y haga clic en el botón "Canjear".');
define('TEXT_IN_CASE', ' En el improbable caso de problemas de contabilidad.');
define('TEXT_VOUCHER_IS', 'Su código de cupón es: ');
define('TEXT_REMEMBER', 'Conserve su código de cupón para que pueda beneficiarse de esta oferta.');
define('TEXT_VISIT', 'cuando nos visita la próxima vez bajo ' . HTTP_SERVER . DIR_WS_CATALOG. ' .');
define('TEXT_ENTER_CODE', ' e introduce el código ');

define('TABLE_HEADING_ACTION', 'Acción');

define('CUSTOMER_ID', 'Número de cliente');
define('CUSTOMER_NAME', 'Nombre del cliente');
define('REDEEM_DATE', 'canjeado el');
define('IP_ADDRESS', 'Dirección IP');

define('TEXT_REDEMPTIONS', 'Recaudación');
define('TEXT_REDEMPTIONS_TOTAL', 'Total');
define('TEXT_REDEMPTIONS_CUSTOMER', 'Para este cliente');
define('TEXT_NO_FREE_SHIPPING', 'No libre de costos de envío');

define('NOTICE_EMAIL_SENT_TO', 'Nota: Correo electrónico enviado a: %s');
define('ERROR_NO_CUSTOMER_SELECTED', 'Error: Ningún cliente seleccionado');
define('COUPON_NAME', 'Nombre del cupón');
define('COUPON_AMOUNT', 'Valor del cupón');
define('COUPON_CODE', 'Código del cupón');
define('COUPON_STARTDATE', 'válido desde');
define('COUPON_FINISHDATE', 'válido hasta');
define('COUPON_FREE_SHIP', 'libre de costos de envío');
define('COUPON_DESC', 'Descripción del cupón');
define('COUPON_MIN_ORDER', 'Valor mínimo de pedido del cupón');
define('COUPON_USES_COUPON', 'Número/usos por cupón');
define('COUPON_USES_USER', 'Número/usos por cliente');
define('COUPON_PRODUCTS', 'Lista de artículos válidos');
define('COUPON_CATEGORIES', 'Lista de categorías válidas');
define('VOUCHER_NUMBER_USED', 'Número utilizado');
define('DATE_CREATED', 'creado el');
define('DATE_MODIFIED', 'cambiado el');
define('TEXT_HEADING_NEW_COUPON', 'Crear nuevo cupón');
define('TEXT_NEW_INTRO', 'Por favor, introduzca la siguiente información para el nuevo cupón.<br />');

define('COUPON_NAME_HELP', 'Una denominación breve para el cupón');
define('COUPON_AMOUNT_HELP', 'Introduzca aquí el descuento para este cupón. Un importe fijo o un descuento porcentual como por ejemplo 10%');
define('COUPON_CODE_HELP', 'Aquí puede introducir su propio código (máx. 16 caracteres). Si deja este campo en blanco, este código se generará automáticamente.');
define('COUPON_STARTDATE_HELP', 'La fecha a partir de la cual el cupón es válido.<br>');
define('COUPON_FINISHDATE_HELP', 'La fecha a partir de la cual expira el cupón.<br>');
define('COUPON_FREE_SHIP_HELP', 'Cupón para entrega libre de costos de envío');
define('COUPON_DESC_HELP', 'Descripción del cupón para el cliente');
define('COUPON_MIN_ORDER_HELP', 'Valor mínimo de pedido a partir del cual es válido este cupón');
define('COUPON_USES_COUPON_HELP', 'Ingrese aquí cuántas veces este cupón puede ser canjeado. Si deja este campo en blanco, el uso será ilimitado.');
define('COUPON_USES_USER_HELP', 'Ingrese aquí cuántas veces un cliente puede canjear este cupón. Si deja este campo en blanco, el uso será ilimitado.');
define('COUPON_PRODUCTS_HELP', 'Una lista separada por comas de los ID de artículos para los que es válido este cupón. Un campo vacío significa que no hay una restricción.');
define('COUPON_CATEGORIES_HELP', 'Una lista separada por comas de los ID de categorías para los que es válido este cupón. Un campo vacío significa que no hay una restricción.');
define('COUPON_ID', 'cID');
define('BUTTON_DELETE_NO_CONFIRM', 'Borrar sin preguntar');
define('TEXT_NONE', 'Sin restricción');
define('TEXT_COUPON_DELETE', 'Borrar');
define('TEXT_COUPON_STATUS', 'Estado');
define('TEXT_COUPON_DETAILS', 'Datos del cupón');
define('TEXT_COUPON_EMAIL', 'Envío de correo electrónico');
define('TEXT_COUPON_OVERVIEW', 'Resumen');
define('TEXT_COUPON_EMAIL_PREVIEW', 'Confirmación');
define('TEXT_COUPON_MINORDER', 'Valor mínimo de pedido');
define('TEXT_VIEW', 'Vista de lista');
define('TEXT_VIEW_SHORT', 'Visualización');
//BOF - web28 - 2011-04-13 - ADD Coupon message infos
define('COUPON_MINORDER_INFO', "\nValor mínimo de pedido: ");
define('COUPON_RESTRICT_INFO', "\n¡Este cupón sólo es válido para ciertos artículos!");
define('COUPON_INFO', "\nValor del cupón: ");
define('COUPON_FREE_SHIPPING', 'libre de costos de envío');
define('COUPON_LINK_TEXT', '\n\nDetalles');
define('COUPON_CATEGORIES_RESTRICT', '\nVálido para estas categorías');
define('COUPON_PRODUCTS_RESTRICT', '\nVálido para estos artículos');
define('COUPON_NO_RESTRICT', '\nVálido para todos los artículos');
//EOF - web28 - 2011-04-13 - ADD Coupon message infos

//BOF - web28 - 2011-07-05 - ADD error message
define('ERROR_NO_COUPON_NAME', 'ERROR: Sin nombre de cupón ');
define('ERROR_NO_COUPON_AMOUNT', 'ERROR: Sin valor de cupón ');
//EOF - web28 - 2011-07-05 - ADD error message

define('COUPON_DATE_START_TT', 'Comienza a las 00:00:00 horas');
define('COUPON_DATE_END_TT', 'Termina a medianoche (23:59:59)');
define('ERROR_COUPON_DATE', 'ERROR: La fecha de finalización es anterior a la fecha de inicio.');

define('TEXT_OT_COUPON_STATUS_INFO', 'El Módulo de Cupón (ot_coupon) todavía debe ser instalado (Módulo -> <a href="'.xtc_href_link('modules.php','set=ordertotal').'">Resumen</a>)');
?>

<?php
/*------------------------------------------------------------------------------
   v 1.0 
   XTC-DPD Shipping Module - Contribution for XT-Commerce http://xt-commerce.com
   modified by http://www.hwangelshop.de

   Copyrigt (c) 2004 cigamth
  ------------------------------------------------------------------------------
   $Id: dpd.php 2751 2012-04-12 13:28:06Z Tomcraft1980 $

   XTC-GLS Shipping Module - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.hhgag.com

   Copyright (c) 2004 H.H.G.
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 Deutsche Post Module
   Original written by Marcel Bossert-Schwab (webmaster@wernich.de), Version 1.2b
   Addon Released under GLSL V2.0 by Gunter Sammet (Gunter@SammySolutions.com)

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License

   ---------------------------------------------------------------------------*/
define('MODULE_SHIPPING_DPD_TEXT_TITLE', 'Distribución dinámica de paquetes DPD');
define('MODULE_SHIPPING_DPD_TEXT_DESCRIPTION', 'Distribución dinámica de paquetes DPD - Módulo de envío mundial');
define('MODULE_SHIPPING_DPD_TEXT_WAY', ' Envíos a ');
define('MODULE_SHIPPING_DPD_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_DPD_INVALID_ZONE', 'No es posible realizar un envío a este país.');
define('MODULE_SHIPPING_DPD_UNDEFINED_RATE', 'Los costos de envío no se pueden calcular por el momento.');
define('MODULE_SHIPPING_DPD_FREE_SHIPPING', 'Nosotros asumimos los costos de envío.');
define('MODULE_SHIPPING_DPD_SUBSIDIZED_SHIPPING', 'Nosotros asumimos una parte de los costos de envío.');

define('MODULE_SHIPPING_DPD_STATUS_TITLE', 'Distribución dinámica de paquetes DPD');
define('MODULE_SHIPPING_DPD_STATUS_DESC', '¿Desea ofrecer envíos a través de Distribución dinámica de paquetes DPD?');
define('MODULE_SHIPPING_DPD_HANDLING_TITLE', 'Costo de procesamiento');
define('MODULE_SHIPPING_DPD_HANDLING_DESC', 'Costo de procesamiento para este modo de envío en Euro');
define('MODULE_SHIPPING_DPD_ALLOWED_TITLE' , 'Zonas de envío permitidas');
define('MODULE_SHIPPING_DPD_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE (deje este campo vacío si desea permitir todas las zonas))');
define('MODULE_SHIPPING_DPD_SORT_ORDER_TITLE', 'Orden de la visualización');
define('MODULE_SHIPPING_DPD_SORT_ORDER_DESC', 'Menor se muestra en primer lugar.');
define('MODULE_SHIPPING_DPD_TAX_CLASS_TITLE', 'Tasa de impuesto');
define('MODULE_SHIPPING_DPD_TAX_CLASS_DESC', 'Seleccione el tipo de IVA para este modo de envío.');
define('MODULE_SHIPPING_DPD_ZONE_TITLE', 'Zona de envío');
define('MODULE_SHIPPING_DPD_ZONE_DESC', 'Si selecciona una zona, este modo de envío sólo se ofrecerá en esa zona.');

?>

<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_build_orders_status_query.inc.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

function affiliate_build_orders_status_query() {
    $orders_status = explode(';', AFFILIATE_PAYMENT_ORDER_MIN_STATUS);
    
    $where = '';
    
    if(is_array($orders_status)) {
    	$sizeof = sizeof($orders_status);
    	$where .= 'AND (';
    	for($i=0; $i<sizeof($orders_status); $i++) {
    		if(is_numeric($orders_status[$i])) {
    			$where .= 'o.orders_status = ' . $orders_status[$i];
    		} else {
    			if($i > 0) {
    				$where = substr($where, 0, -4);
    			}
    		}
    		$sizeof--;
    		if($sizeof > 0) {
    			$where .= ' OR ';
    		} else {
    			$where .= ')';
    		}
    	}
    }
    
    return $where;
}
?>

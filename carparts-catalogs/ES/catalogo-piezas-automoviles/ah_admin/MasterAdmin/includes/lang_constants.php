<?php
/**************************************************************
* lang constants for import/export script with browser reload
* c) noRiddle 12-2015
**************************************************************/
//BOC language constants
switch ($_SESSION['language']) {
    case 'german':
        define('BUTTON_EXECUTE', 'Ausführen');  //added new, 02-2021, noRiddle

        define('ADMIN_TOOLS_HEADING', 'AH - Master Admin Tools');
        define('ADMIN_TOOLS_NEW_HOME_TITLE', 'Hauptseite &laquo;');
        define('ADMIN_TOOLS_NEW_IMPEXP_SCRIPT_TITLE', 'Import/Export-Skript');
        define('ADMIN_TOOLS_NEW_IMPEXP_SCRIPT_DESC', '- Import von großen oder kleinen CSV-Dateien in die Datenbank ohne Server-Timeout und Memory-Limit-Probleme.<br />- Export aller oder bestimmter Produkte ohne Timeout- und Memory-Probleme.'); //<br /><span style="color:#ff6868;">!! Nach Importen immer den DB-Cache leeren (Erw. Konfiguration => Cache Optionen => "Cache leeren") !!</span>');
        define('ADMIN_TOOLS_COPY_CUSTOMERS_STATUS_PRICES_TITLE', 'Kundengruppenpreise kopieren');
        define('ADMIN_TOOLS_COPY_CUSTOMERS_STATUS_PRICES_DESC', '- Kopieren von Kundengruppenpreisen von einer Tabelle in eine oder mehrere andere.');
        define('ADMIN_TOOLS_VERIFY_KEYS_TITLE', 'Indexes/DB überprüfen');
        define('ADMIN_TOOLS_VERIFY_KEYS_DESC', '- Sind alle Indexes aktiviert ?<br />- Haben alle Indexes eine Cardinality ?<br />- Gibt es tote Einträge ?');
        define('ADMIN_TOOLS_SET_PROD_STATUS_TITLE', 'Für SERGEJ<br />Setze products_status auf 0');
        define('ADMIN_TOOLS_SET_PROD_STATUS_DESC', '- setze products_status auf 0 für bestimmte products_supplier_id');
        //BOC scripts
        /*if(isset($_GET['script']) && in_array($_GET['script'], $script_array)) {
            $script_heading = implode(' ', explode('_', $_GET['script']));
            define('ADMIN_TOOLS_SCRIPT_HEADING', strtoupper($script_heading));
        }*/
        define('ADMIN_TOOLS_LOAD_PER_FTP', '(bei sehr großen Dateien Datei besser per FTP ins Verzeichnis /import/ laden)');
        define('ADMIN_TOOLS_FILE_UPLOAD_TXT', 'Datei hochladen: ');
        define('ADMIN_TOOLS_IMPORT_AUX_FUNCS', '&raquo; Zusatzfunktionen <span class="sml">(öffnen und schließen per Klick)</span>');
        define('ADMIN_TOOLS_IMPORT_SET_FUNCS', '&raquo; Einstellungen <span class="sml">(öffnen und schließen per Klick)</span>');
        define('CSV_TEXTSIGN_TITLE', 'Texterkennungszeichen:');
        define('CSV_TEXTSIGN_DESC', 'z.B.: ^, sollte leer bleiben');
        define('CSV_SEPERATOR_TITLE', 'Feldtrennzeichen:');
        define('CSV_SEPERATOR_DESC', 'z.B.: |, empfohlen: |');
        define('IE_IMPORT_FACTOR_TITLE', 'Import pro Runde:');
        define('IE_IMPORT_FACTOR_DESC', 'wieviele Artikel sollen pro Browser-Relaod importiert werden, empfohlen: 500');
        define('IE_EXPORT_FACTOR_TITLE', 'Export pro Runde:');
        define('IE_EXPORT_FACTOR_DESC', 'wieviele Artikel sollen pro Browser-Relaod exportiert werden, empfohlen: 500');
        define('ADMIN_TOOLS_SELECT_FILE', 'Datei auswählen');
        define('ADMIN_TOOLS_UPLOAD_FILE_SUCCESS', 'Datei erfolgreich hochgeladen.');
        define('ADMIN_TOOLS_SELECT_FILE_TXT', 'Datei in DB importieren: ');
        define('ADMIN_TOOLS_DATA_SUCCESSFULLY_IN_AUX_TABLE', 'Daten wurden in Hilfstabelle geladen');
        define('ADMIN_TOOLS_NO_FILE_CHOOSEN', 'Keine Datei zum importieren ausgewählt');
        define('ADMIN_TOOLS_NO_DATA_IN_AUX_TABLE', 'Keine Daten zum importieren in Hilfstabelle csv_import_aux');
        define('ADMIN_TOOLS_HEADER_CSV_SET', 'Kopfzeile in CSV geschrieben.');
        define('ADMIN_TOOLS_NO_PRODUCTS', 'Keine Produkte für diese Auswahl vorhanden.');
        define('ADMIN_TOOLS_NO_PROD_MODEL', 'Keine Artikelnummer (products_model) für Artikel %s !!<br />Bitte korrigieren und Import neu starten mit einem step kleiner (siehe URL).');
        define('ADMIN_TOOLS_NO_CAT', 'Keine Kategorie für Artikel %s !!<br />Bitte korrigieren und Import neu starten mit einem step kleiner (siehe URL).');
        define('ADMIN_TOOLS_NO_FIRST_CAT', 'Artikel mit Art.-Nr. %s hat keine erste Kategorie !!');
        define('MOD_CHK_ALL', 'Alle anhaken/enthaken');
        define('MOD_PIM', 'Bilder verarbeiten als ARTIKELNUMMER.jpg<br /><span style="color:#c00;">! Das funktioniert nur dann wenn das Feld <i>products_images</i> in der Import-CSV leer ist !</span><br />Ist das Feld nicht leer wird der Wert in dem Feld genommen.'); //new text for image processing as MODEL_NO.jpg, 04-2021, noRiddle
        define('MOD_CSV_DELETE_PRODUCTS', 'Alle im Shop vorhandenen Artikel unwiderruflich vor dem CSV-Import löschen?<br />Gelöscht werden die Tabellen: <i>products, products_attributes, products_description, products_graduated_prices, products_to_categories, personal_offers_by_customers_status_x</i><br /><span style="color:#c00;">Es ist empfehlenswert auch die Sonderangebote zu löschen wenn alle Produkte gelöscht werden.</span>');
        define('MOD_CSV_DELETE_CATEGORIES', 'Alle im Shop vorhandenen Kategorien unwiderruflich vor dem CSV-Import löschen?<br />Gelöscht werden die Tabellen: <i>categories, categories_description</i><br /><span style="color:#c00;">Funktioniert nur wenn auch alle Produkte gelöscht werden.</span>');
        define('MOD_CSV_DELETE_SPECIALS', 'Alle im Shop vorhandenen Sonderangebote unwiderruflich vor dem CSV-Import löschen?<br />Gelöscht werden die Tabellen: <i>specials</i>');
        define('MOD_CSV_DELETE_IMAGES', 'Alle im Shop vorhandenen Artikelbild-Zuordnungen unwiderruflich vor dem CSV-Import löschen?<br />Gelöscht werden die Tabellen: <i>products_images</i><br /><span style="color:#c00;">Hiermit werden lediglich Zusatzbilder gelöscht. Das erste Produktbild steht in der Tabelle <i>products</i> und kann nur mit dem Löschen aller Produkte entfernt werden.</span>');
        define('MOD_CSV_DELETE_MANUFACTURERS', 'Alle im Shop vorhandenen Hersteller unwiderruflich vor dem CSV-Import löschen?<br />Gelöscht werden die Tabellen: <i>manufacturers</i>');
        define('MOD_CSV_DELETE_REVIEWS', 'Alle im Shop vorhandenen Artikelbewertungen unwiderruflich vor dem CSV-Import löschen?<br />Gelöscht werden die Tabellen: <i>reviews, reviews_description</i>');
        define('MOD_CSV_DELETE_ATTRIBUTES', 'Alle im Shop vorhandenen Artikel-Attributzuweisungen unwiderruflich vor dem CSV-Import löschen?<br />Gelöscht werden die Tabellen: <i>products_attributes, products_attributes_download</i>');
        define('MOD_CSV_DELETE_XSELL', 'Alle im Shop vorhandenen Cross-Selling-Artikel und -Gruppen unwiderruflich vor dem CSV-Import löschen?<br />Gelöscht werden die Tabellen: <i>products_xsell, products_xsell_grp_name</i>');
        define('MOD_CSV_GEN_INFOS', 'Steps insg.: %s | momentaner Step: %s <br />');
        define('MOD_CSV_TIME_ONE_LOOP_TXT', 'Verbrauchte Zeit für %s Artikel: %s Sekunden');
        define('MOD_CSV_TOT_PRODUCTS_EXPORTED', 'Insgesamt %s Artikel exportiert');
        define('MOD_CSV_TOT_PRODUCTS_IMP_OR_UPD', 'Insgesamt %s Artikel importiert und %s Artikel upgedatet');
        define('MOD_CSV_PRICE_UPDT_NOT_FOUND', 'Insgesamt %s Artikel nicht im Shop gefunden und %s Artikel upgedatet');
        define('ADMIN_TOOLS_EXPORT_INDEXES_TXT', '(Wenn ein Export abgebrochen wurde 1 x Export von Startseitenartikeln machen um die Indexe in der DB zu fixen)');
        define('ADMIN_TOOLS_SELECT_EXPORT_TXT', 'Produkte exportieren: ');
        define('ADMIN_TOOLS_TEXT_PRODUCTS','Produkte');
        define('ADMIN_TOOLS_TEXT_STARTPAGE', 'Startseite');
        define('ADMIN_TOOLS_TEXT_NORMAL', 'gm_price_status normal');
        define('ADMIN_TOOLS_TEXT_ON_REQUEST', 'gm_price_status Preis auf Anfrage');
        define('ADMIN_TOOLS_TEXT_NON_PURCHASABLE', 'gm_price_status nicht lieferbar');
        define('ADMIN_TOOLS_TEXT_SET_BY_ADMIN', 'set_by_admin geänderte gm_price_status'); //added 30.06.2016
        define('ADMIN_TOOLS_TEXT_BLACKFOREST', 'blackforest parts'); //added 09-2019
        define('ADMIN_TOOLS_TEXT_SBK', 'Sonderbeschaffungskosten'); //added 05-2020, noRiddle
        define('ADMIN_TOOLS_COPY_PERSOFF_WITH_FACTOR', '&raquo personal offers Preise (nur Preise) mit Umrechnungsfaktor kopieren <span class="sml">(öffnen und schließen per Klick)</span>');
        define('ADMIN_TOOLS_COPY_PERSOFF_WITH_FACTOR_GUIDANCE', '<li>1. Feld: von wo Preise kopiert werden sollen</li><li>2. Feld: wohin Preise kopiert werden sollen</li><li>3. Feld: Umrechnungsfaktor (z.B. 1.08)</li><li>Die Preise werden dann von Tabelle Feld 1 in Tabelle Feld 2 mit dem Umrechnungsfaktor übernommen.<br />Beispiel: Preis ist 20,- EUR Umrechnungsfaktor ist 0.5 => Preis wird in Zieltabelle 10,- EUR sein.</li><li>Es kann auch in ein und derselben Tabelle ein Umrechnungfaktor verwirklicht werden. Wenn Ausgangs- und Zieltabelle gleich sind wird dies automatisch erkannt.</li>');
        define('ADMIN_TOOLS_COPY_PERSOFF_FACT_TXT', 'Umrechnungsfaktor:');
        define('ADMIN_TOOLS_PRICES_FROM', 'Preise von');
        //BOC group 16 will not be filled by copying any more, noRiddle
        //define('ADMIN_TOOLS_COPY_PERSOFF_ALL_GUIDANCE', 'Es werden mit dieser Funktion folgende Inhalte der <i>personal_offers_by_customers_status_x</i> Tabellen in die aufgeführten <i>personal_offers_by_customers_status_y</i> kopiert:<br />&bull; _1 in _2, _6, _11, _12<br />&bull; _3 in _5<br />&bull; _7 in _15, _16<br />&bull; _8 in _9, _10, _13, _14');
        define('ADMIN_TOOLS_COPY_PERSOFF_ALL_GUIDANCE', 'Es werden mit dieser Funktion die Inhalte folgender <i>personal_offers_by_customers_status_x</i> Tabellen in die aufgeführten <i>personal_offers_by_customers_status_y</i> kopiert:<br />&bull; _1 in _2, _6, _11, _12<br />&bull; _3 in _5<br />&bull; _7 in _15<br />&bull; _8 in _9, _10, _13, _14');
        //EOC group 16 will not be filled by copying any more, noRiddle
        define('ADMIN_TOOLS_COPY_PERSOFF_SINGLE', '&raquo; einzelne komplette Inhalte von personal offers nach Wahl kopieren <span class="sml">(öffnen und schließen per Klick)</span>');
        define('ADMIN_TOOLS_COPY_PERSOFF_GUIDANCE', 'Die Ziffern für die personal_offers_by_customers_status_x-Tabellen in die kopiert werden soll komma-separiert ins rechte Eingabe-Feld eingeben.');
        define('ADMIN_TOOLS_COPY_PERSOFF_TXT', 'kopieren in &raquo;&raquo;&raquo;');
        define('ADMIN_TOOLS_COPY_PERSOFF_INPUT_PLACEHOLDER', 'z.B.: 5,9,11');
        define('ADMIN_TOOLS_COPY_PERSOFF_SUCCESS_TXT', 'Preise aus <i>personal_offers_by_customers_status_%s</i> wurden in Kundenstatus %s übernommen<br />');
        //added new, 02-2021, noRiddle
        define('ADMIN_TOOLS_CHANGE_PERSOFF_FACTOR', '&raquo; Preise mit Faktor erhöhen oder erniedrigen <span class="sml">(öffnen und schließen per Klick)</span>');
        define('ADMIN_TOOLS_CHANGE_PERSOFF_FACTOR_GUIDANCE', 'Faktor angeben mit welchem der Preis multipliziert werden soll.<br />Beispiel: Preis 10% höher => Faktor-Eingabe 1.1, Preis 10% niedriger => Faktor-Eingabe 0.9<br />Die Berechnungen werden per Ajax (ohne Page-Reload) durchgeführt. <span style="color:#c00; font-weight:bold;">!! Erst wenn "READY !" erscheint ist die Aktion beendet !!</span><br /><span style="color:#c00;">!! Wenn eine Aktion rückgängig gemacht werden soll darauf achten, daß der Faktor nun so berechnet werden muß:<br />1. Aktion: Faktor 1.1 (= 110%), Rückgängigmachen: Faktor 100/110 also 0.9090909090</span>');
        define('ADMIN_TOOLS_CHANGE_PERSOFF_FACT_LAST', 'Letzter Faktor:');
        define('ADMIN_TOOLS_CHANGE_PERSOFF_FACT_LAST_REV', 'Rückgängig mit:');
        define('ADMIN_TOOLS_SYNC_FACTOR_TXT', 'Mit Eingabe eines Faktors alle anderen Faktoren automatisch füllen: ');
        
        define('ADMIN_TOOLS_VERIFY_KEYS_GUIDANCE', '<h3 class="red">Bitte beachten !!</h3><ul class="do-list"><li>Wenn in einem Feld oder mehreren Feldern einer Tabelle in der Spalte "Comment" "disabled" steht: Für die betreffende Tabelle Checkbox in Spalte "Enable keys" anhaken und "Senden" klicken.</li><li>Wenn bei  einem Feld oder mehreren Feldern einer Tabelle kein Wert in der Spalte "Cardinality" steht: Für die betreffende Tabelle Checkbox in der Spalte "Analyze table" anhaken und "senden" klicken.</li><li>"Optimize table" sollte man durchführen wenn einer oder beide der ersten beiden Punkte durchgeführt wurden.<br />Außerdem kann man es ab und an machen, z.B. nach größeren Importen.</li></ul>');
        define('ADMIN_TOOLS_VERIFY_KEYS_ENABLE_SUCCESS_TXT', 'In Tabelle <i>%s</i> wurden die Keys aktiviert<br />');
        define('ADMIN_TOOLS_VERIFY_KEYS_ANALYZE_SUCCESS_TXT', 'Tabelle <i>%s</i> wurde analysiert<br />');
        define('ADMIN_TOOLS_VERIFY_KEYS_OPTIMIZE_SUCCESS_TXT', 'Tabelle <i>%s</i> wurde optimiert<br />');
        define('ADMIN_TOOLS_FIND_PRODDESCIDS_NOTIN_PROD', 'Gibt es <i>product_id</i>s in Tabelle <i>products_decsription</i> die nicht in Tabelle <i>products</i> zu finden sind ?');
        define('ADMIN_TOOLS_FIND_PRODDESCIDS_NOTIN_PROD_RES', 'Es gibt %s <i>product_id</i>s die nicht in der Tabelle <i>products</i> zu finden sind.');
        define('ADMIN_TOOLS_FIND_PRODTOCATIDS_NOTIN_PROD', 'Gibt es <i>product_id</i>s in Tabelle <i>products_to_categories</i> die nicht in Tabelle <i>products</i> zu finden sind ?');
        define('ADMIN_TOOLS_FIND_PRODTOCATIDS_NOTIN_PROD_RES', 'Es gibt %s <i>product_id</i>s die nicht in der Tabelle <i>products</i> zu finden sind.');
        define('ADMIN_TOOLS_FIND_PRODPRODIDS_NOTIN_PRODDESC', 'Gibt es Einträge in Tabelle <i>products_description</i> die nur in einer Sprache zu finden sind ?');
        define('ADMIN_TOOLS_FIND_PRODPRODIDS_NOTIN_PRODDESC_RES', 'Es gibt %s <i>product_id</i>s die in der Tabelle <i>products_description</i> nicht in beiden Sprachen zu finden sind.');
        define('ADMIN_TOOLS_FIND_PRODIDS_MORETHANONE_PRODTOCAT', 'Gibt es <i>product_id</i>s in Tabelle <i>products_to_categories</i> die mehrfach vorkommen ?');
        define('ADMIN_TOOLS_FIND_PRODIDS_MORETHANONE_PRODTOCAT_RES', 'Es gibt %s <i>product_id</i>s die in der Tabelle <i>products_to_categories</i> mehrfach zu finden sind.');
        define('ADMIN_TOOLS_FIND_PRODIDS_IN_PRODTOCAT_TOP', 'Gibt es <i>product_id</i>s die in TOP (= categories_id 0) verlinkt sind ?');
        define('ADMIN_TOOLS_FIND_PRODIDS_IN_PRODTOCAT_TOP_RES', 'Folgende Produkte sind in TOP verlinkt (products_id, products_model):<br /> %s');
        
        define('ADMIN_TOOLS_SET_PROD_STATUS_GUIDANCE', '<h3 class="red">Bitte vorsichtig sein hiermit</h3>
                                                        <ul class="do-list">
                                                            <li>Wenn man eine <i>products_supplier_id</i> in das Textfeld eingibt und "Senden" klickt wird der <i>products_status</i> aller Produkte mit der angegebenen <i>products_supplier_id</i> auf 0 gesetzt</li>
                                                            <li>Der Wert im Textfeld ist lediglich ein Platzhalter, man muß den gewünschten Wert eingeben</li>
                                                            <li>Eine Erfolgsmeldung wird angezeigt werden</li>
                                                            <li>Man kann den <i>products_status</i> auch für einen Bereich von <i>products_supplier_id</i> auf 0 setzen:<span style="color:#c00;"> Der Bereich von Werten die angegeben werden verstehen sich inklusive der angegebenen Werte !!</span></li>
                                                            <li>Wenn man versehentlich eine verkehrte <i>products_supplier_id</i> auf <i>products_status</i> 0 gesetzt hat, kann man sie mithilfe der letzten Möglichkeit unten zurück auf 1 setzen</li>
                                                        </ul>');
        define('ADMIN_TOOLS_SET_PROD_STATUS_0', 'setze products_status auf 0');
        define('ADMIN_TOOLS_SET_PROD_STATUS_B_0', 'setze products_status auf 0 für einen Bereich <span class="sml" style="color:#c00;">(VORSICHT !! Die eingegebenen Werte verstehen sich inklusive !!)');
        define('ADMIN_TOOLS_SET_PROD_STATUS_1', 'setze products_status auf 1');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_0', 'setze <i>products_status</i> auf 0 für die folgende <i>products_supplier_id</i>: ');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_B_01', 'setze <i>products_status</i> auf 0 zwischen (inkl.) <i>products_supplier_id </i>');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_B_01B', ' und (inkl.) &nbsp;');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_1', 'setze <i>products_status</i> auf 1 für die folgende <i>products_supplier_id</i>: ');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUCCESS_TXT_0', 'Der <i>products_status</i> aller Produkte mit <i>products_supplier_id</i> %s wurde auf 0 gesetzt');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUCCESS_TXT_B_0', 'Der <i>products_status</i> aller Produkte mit <i>products_supplier_id</i> zwischen %s (einschließlich) und %s (einschließlich) wurde auf 0 gesetzt');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUCCESS_TXT_1', 'Der <i>products_status</i> aller Produkte mit <i>products_supplier_id</i> %s wurde auf 1 gesetzt');
        define('ADMIN_TOOLS_SET_PROD_STATUS_ERROR_TXT', '<span style="color:#c00;">Es wurde keine <i>products_supplier_id</i> eingegeben oder der angegebene Wert ist nicht erlaubt</span>');
        define('ADMIN_TOOLS_COPY_CONTENTS_TITLE', 'Contents in andere Shops kopieren');
        define('ADMIN_TOOLS_COPY_CONTENTS_DESC', '- Contents mit <i>content_group</i> von X bis Y werden in die gewälten Shops auf <i>'.basename(HTTP_SERVER).'</i> kopiert<br /><span style="color:#ff6868;">Es werden automatisch die Contents für beide Sprachen kopiert.</span>');
        define('ADMIN_TOOLS_COPY_ALLOWED_CONTENTS_GUIDANCE', 'Die erlaubten coIDs ohne Leerzeichen komma-separiert eintragen.');
        define('ADMIN_TOOLS_COPY_CONTENTS_SAVE_ALLOWED', 'Speichern');
        define('ADMIN_TOOLS_COPY_CONTENTS_GUIDANCE', '<h4>Vorsicht !</h4><ul><li>Contents deren <i>content_group</i> im Ziel-Shop nicht existiert werden neu angelegt.</li><li>Contents deren <i>content_group</i> im Ziel-Shop bereits existiert werden überschrieben.</li></ul>');
        define('ADMIN_TOOLS_COPY_CONTENTS_FROM_WHICH_SHOP', 'Von welchem Shop: ');
        define('ADMIN_TOOLS_COPY_CONTENTS_TO_SHOPS', ' in alle anderen Shops auf <i>'.basename(HTTP_SERVER).'</i>');
        define('ADMIN_TOOLS_COPY_CONTENTS_TO_SHOP_SNGL', ' in folgenden Shop auf <i>'.basename(HTTP_SERVER).'</i> ');
        define('ADMIN_TOOLS_COPY_CONTENTS_FROM_CNT_GRP', 'von welcher content_group (einschl.): ');
        define('ADMIN_TOOLS_COPY_CONTENTS_TO_CNT_GRP', '&nbsp; | &nbsp;bis welcher content_group (einschl.): ');
        define('ADMIN_TOOLS_COPY_CONTENTS_SEND', 'Absenden');
        define('ADMIN_TOOLS_COPY_CONTENTS_CHOOSE', 'Bitte wählen');
        //EOC scripts
        define('ADMIN_TOOLS_COPY_CONTENTS_EXCL_LANDPAGE_TXT', 'Landing-Page ausschließen: '); //new for checkbox, 05-2021, noRiddle
    break;
    case 'english':
    default:
        define('BUTTON_EXECUTE', 'Execute');  //added new, 02-2021, noRiddle

        define('ADMIN_TOOLS_HEADING', 'AH - Master Admin Tools');
        define('ADMIN_TOOLS_NEW_HOME_TITLE', 'Main page &laquo;');
        define('ADMIN_TOOLS_NEW_IMPEXP_SCRIPT_TITLE', 'import/export script');
        define('ADMIN_TOOLS_NEW_IMPEXP_SCRIPT_DESC', '- Import big or small CSV-files in the database without server timeouts or memory limit problems.<br />- Export all or certain products without server timeouts or memory limit problems.'); //<br /><span style="color:#ff6868;">!! After imports always delete DB-Cache (Adv. Configuration => Cache Options => "Delete Cache") !!</span>');
        define('ADMIN_TOOLS_COPY_CUSTOMERS_STATUS_PRICES_TITLE', 'Copy customer group prices');
        define('ADMIN_TOOLS_COPY_CUSTOMERS_STATUS_PRICES_DESC', '- Copy customer group prices from one table into one or more other tables.');
        define('ADMIN_TOOLS_VERIFY_KEYS_TITLE', 'Verify indexes/DB');
        define('ADMIN_TOOLS_VERIFY_KEYS_DESC', '- Are all indexes activated ?<br />- Do all indexes have a cardinality ?<br />- Are there dead entries ?');
        define('ADMIN_TOOLS_SET_PROD_STATUS_TITLE', 'For SERGEJ<br />Set products_status to 0');
        define('ADMIN_TOOLS_SET_PROD_STATUS_DESC', '- set products_status to 0 for certain products_supplier_id');
        //BOC scripts
        /*if(isset($_GET['script']) && in_array($_GET['script'], $script_array)) {
            $script_heading = implode(' ', explode('_', $_GET['script']));
            define('ADMIN_TOOLS_SCRIPT_HEADING', strtoupper($script_heading));
        }*/
        define('ADMIN_TOOLS_LOAD_PER_FTP', '(with very big files better load file via FTP into directory /import/)');
        define('ADMIN_TOOLS_FILE_UPLOAD_TXT', 'Upload file: ');
        define('ADMIN_TOOLS_IMPORT_AUX_FUNCS', 'Auxiliary functions <span class="sml">(open and close per click)</span>');
        define('ADMIN_TOOLS_IMPORT_SET_FUNCS', '&raquo; Settings <span class="sml">(open and close per click)</span>');
        define('CSV_TEXTSIGN_TITLE', 'Text sign:');
        define('CSV_TEXTSIGN_DESC', 'e.g.: ^, should be left empty');
        define('CSV_SEPERATOR_TITLE', 'Field separator:');
        define('CSV_SEPERATOR_DESC', 'e.g.: |, recommended: |');
        define('IE_IMPORT_FACTOR_TITLE', 'Import per round:');
        define('IE_IMPORT_FACTOR_DESC', 'how many products shall be imported per browser relaod, recomended: 500');
        define('IE_EXPORT_FACTOR_TITLE', 'Export per round:');
        define('IE_EXPORT_FACTOR_DESC', 'how many products shall be exported per browser relaod, recomended: 500');
        define('ADMIN_TOOLS_SELECT_FILE', 'Select file');
        define('ADMIN_TOOLS_UPLOAD_FILE_SUCCESS', 'File uploaded successfully.');
        define('ADMIN_TOOLS_SELECT_FILE_TXT', 'Import file in database: ');
        define('ADMIN_TOOLS_DATA_SUCCESSFULLY_IN_AUX_TABLE', 'Data was loaded in auxiliary table.');
        define('ADMIN_TOOLS_NO_FILE_CHOOSEN', 'No file choosen to import');
        define('ADMIN_TOOLS_NO_DATA_IN_AUX_TABLE', 'No data to import in auxiliary table csv_import_aux');
        define('ADMIN_TOOLS_HEADER_CSV_SET', 'Wrote head line in csv.');
        define('ADMIN_TOOLS_NO_PRODUCTS', 'No products for this selection.');
        define('ADMIN_TOOLS_NO_PROD_MODEL', 'No products_model for product %s !!<br />Please correct and start import again with one step lower (see URL).');
        define('ADMIN_TOOLS_NO_CAT', 'No ctagory for product %s !!<br />Please correct and start import again with one step lower (see URL).');
        define('ADMIN_TOOLS_NO_FIRST_CAT', 'Product with model no. %s doesn\'t have first categorie !!');
        define('MOD_CHK_ALL', 'Check all/uncheck all');
        define('MOD_PIM', 'Process images as MODEL_NO.jpg<br /><span style="color:#c00;">! This will only work in case the field <i>products_images</i> in the import-CSV is empty !</span><br />If the field is not empty the value in the field will be processed.'); //new text for image processing as MODEL_NO.jpg, 04-2021, noRiddle
        define('MOD_CSV_DELETE_PRODUCTS', 'Delete all articles irrevocably before the CSV import?<br />Deleted will be the tables: <i>products, products_attributes, products_description, products_graduated_prices, products_to_categories, personal_offers_by_customers_status_x</i><br /><span style="color:#c00;">It is recommended to delete also the specials when all products are deleted.</span>');
        define('MOD_CSV_DELETE_CATEGORIES', 'Delete all categories irrevocably before the CSV import?<br />Deleted will be the tables: <i>categories, categories_description</i><br /><span style="color:#c00;">Only works if also all products are deleted.</span>');
        define('MOD_CSV_DELETE_SPECIALS', 'Delete all specials irrevocably before the CSV import?<br />Deleted will be the tables: <i>specials</i>');
        define('MOD_CSV_DELETE_IMAGES', 'Delete all image assignations irrevocably before the CSV import?<br />Deleted will be the tables: <i>products_images</i><br /><span style="color:#c00;">This only deletes the secondary images. The first image of each product is in the table <i>products</i> and can only be deleted when all products are deleted.</span>');
        define('MOD_CSV_DELETE_MANUFACTURERS', 'Delete all manufacturers irrevocably before the CSV import?<br />Deleted will be the tables: <i>manufacturers</i>');
        define('MOD_CSV_DELETE_REVIEWS', 'Delete all reviews irrevocably before the CSV import?<br />Deleted will be the tables: <i>reviews, reviews_description</i>');
        define('MOD_CSV_DELETE_ATTRIBUTES', 'Delete all article-attribute assignations irrevocably before the CSV import?<br />Deleted will be the tables: <i>products_attributes, products_attributes_download</i>');
        define('MOD_CSV_DELETE_XSELL', 'Delete all cross-selling articles and groups irrevocably before the CSV import?<br />Deleted will be the tables: <i>products_xsell, products_xsell_grp_name</i>');
        define('MOD_CSV_GEN_INFOS', 'Steps overall: %s | momentary step: %s <br />');
        define('MOD_CSV_TIME_ONE_LOOP_TXT', 'Elapsed time for %s products: %s seconds');
        define('MOD_CSV_TOT_PRODUCTS_EXPORTED', 'A total of %s products exported');
        define('MOD_CSV_TOT_PRODUCTS_IMP_OR_UPD', 'A total of %s products imported and %s products updated');
        define('MOD_CSV_PRICE_UPDT_NOT_FOUND', 'A total of %s products not found in shop and %s products updated');
        define('ADMIN_TOOLS_EXPORT_INDEXES_TXT', '(If an export was interrupted export 1 x the startpage articles to fix the indexes in the database)');
        define('ADMIN_TOOLS_SELECT_EXPORT_TXT', 'Export products: ');
        define('ADMIN_TOOLS_TEXT_PRODUCTS','Products');
        define('ADMIN_TOOLS_TEXT_STARTPAGE', 'Start page');
        define('ADMIN_TOOLS_TEXT_NORMAL', 'gm_price_status normal');
        define('ADMIN_TOOLS_TEXT_ON_REQUEST', 'gm_price_status price on request');
        define('ADMIN_TOOLS_TEXT_NON_PURCHASABLE', 'gm_price_status not purchasable');
        define('ADMIN_TOOLS_TEXT_SET_BY_ADMIN', 'set_by_admin changed gm_price_status'); //added 30.06.2016
        define('ADMIN_TOOLS_TEXT_BLACKFOREST', 'blackforest parts'); //added 09-2019
        define('ADMIN_TOOLS_TEXT_SBK', 'extra procurement costs'); //added 05-2020, noRiddle
        define('ADMIN_TOOLS_COPY_PERSOFF_WITH_FACTOR', '&raquo; copy personal offers prices (only prices) with conversion factor <span class="sml">(open and close by click)</span>');
        define('ADMIN_TOOLS_COPY_PERSOFF_WITH_FACTOR_GUIDANCE', '<li>1. field: from which table prices shall be copied</li><li>2. field: to which table prices shall be copied</li><li>3. field: conversion factor (e.g. 1.08)</li><li>The prices from table field 1 will then be written in table field 2 with the indicated conversion factor.<br />Example: Price is 20,- EUR conversion factor is 0.5 => proce will be 10,- EUR in aimed table.</li><li>You may also alter the values in the same table. The script will automatically recognize whether initial and targeted table are the same.</li>');
        define('ADMIN_TOOLS_COPY_PERSOFF_FACT_TXT', 'conversion factor:');
        define('ADMIN_TOOLS_PRICES_FROM', 'prices from');
        //BOC group 16 will not be filled by copying any more, noRiddle
        //define('ADMIN_TOOLS_COPY_PERSOFF_ALL_GUIDANCE', 'With this fumction the following <i>personal_offers_by_customers_status_x</i> tables will be copied into the listed <i>personal_offers_by_customers_status_y</i> tables:<br />&bull; _1 in _2, _6, _11, _12<br />&bull; _3 in _5<br />&bull; _7 in _15, _16<br />&bull; _8 in _9, _10, _13, _14');
        define('ADMIN_TOOLS_COPY_PERSOFF_ALL_GUIDANCE', 'With this fumction the following <i>personal_offers_by_customers_status_x</i> tables will be copied into the listed <i>personal_offers_by_customers_status_y</i> tables:<br />&bull; _1 in _2, _6, _11, _12<br />&bull; _3 in _5<br />&bull; _7 in _15<br />&bull; _8 in _9, _10, _13, _14');
        //EOC group 16 will not be filled by copying any more, noRiddle
        define('ADMIN_TOOLS_COPY_PERSOFF_SINGLE', '&raquo; copy complete contents of single personal offers at your own choice <span class="sml">(open and close by click)</span>');
        define('ADMIN_TOOLS_COPY_PERSOFF_GUIDANCE', 'Enter the numbers for the personal_offers_by_customers_status_x tables in which you want to copy comma seperated into the right input field.');
        define('ADMIN_TOOLS_COPY_PERSOFF_TXT', 'copy to &raquo;&raquo;&raquo;');
        define('ADMIN_TOOLS_COPY_PERSOFF_INPUT_PLACEHOLDER', 'e.g.: 5,9,11');
        define('ADMIN_TOOLS_COPY_PERSOFF_SUCCESS_TXT', 'Prices from <i>personal_offers_by_customers_status_%s</i> have been copied to customers status %s<br />');
        //added new, 02-2021, noRiddle
        define('ADMIN_TOOLS_CHANGE_PERSOFF_FACTOR', '&raquo; Increase or decrease prices by factor <span class="sml">(open and close by click)</span>');
        define('ADMIN_TOOLS_CHANGE_PERSOFF_FACTOR_GUIDANCE', 'Indicate the factor with which the price shall be mutiplied.<br />Example: Price 10% higher => factor 1.1, price 10% lower => factor 0.9<br />The caclculations will be executed via Ajax (without page reload). <span style="color:#c00; font-weight:bold;">!! Only whem "READY !" is displayed the action is finished !!</span><br /><span style="color:#c00;">!! If an action shall be revoked be carefull that the factor has to be calculated this way this time:<br />1. Action: factor 1.1 (= 110%), Revoke: factor 100/110 thus 0.9090909090</span>');
        define('ADMIN_TOOLS_SYNC_FACTOR_TXT', 'With entry of one factor fill all other factors automatically: ');
        define('ADMIN_TOOLS_CHANGE_PERSOFF_FACT_LAST', 'Last factor');
        define('ADMIN_TOOLS_CHANGE_PERSOFF_FACT_LAST_REV', 'Revoke with:');
        
        define('ADMIN_TOOLS_VERIFY_KEYS_GUIDANCE', '<h3 class="red">Please observe !!</h3><ul class="do-list"><li>If one field or more fields of a table show "disabled" in the column "Comment": Check checkbox for the respective table in column "Enable keys" and click "Send".</li><li>If one field or more fields of a table show no value in the column "Cardinality": Check checkbox for the respective table in Column "Analyze table" and click "Send".</li><li>"Optimize table" should be perfomed in case one or both of the preceding points have been executed..<br />Also it can be reasonable once in a while, e.g. after bigger imports.</li></ul>');
        define('ADMIN_TOOLS_VERIFY_KEYS_ENABLE_SUCCESS_TXT', 'Keys in table <i>%s</i> have been activated<br />');
        define('ADMIN_TOOLS_VERIFY_KEYS_ANALYZE_SUCCESS_TXT', 'Table <i>%s</i> has been analyzed<br />');
        define('ADMIN_TOOLS_VERIFY_KEYS_OPTIMIZE_SUCCESS_TXT', 'Table <i>%s</i> has been optimized<br />');
        define('ADMIN_TOOLS_FIND_PRODDESCIDS_NOTIN_PROD', 'Are there <i>product_id</i>s in table <i>products_decsription</i> which are not in table <i>products</i> ?');
        define('ADMIN_TOOLS_FIND_PRODDESCIDS_NOTIN_PROD_RES', 'There are %s <i>product_id</i>s which are not in table <i>products</i>.');
        define('ADMIN_TOOLS_FIND_PRODTOCATIDS_NOTIN_PROD', 'Are there <i>product_id</i>s in table <i>products_to_categories</i> which are not in table <i>products</i> ?');
        define('ADMIN_TOOLS_FIND_PRODTOCATIDS_NOTIN_PROD_RES', 'There are %s <i>product_id</i>s  which are not in table <i>products</i>.');
        define('ADMIN_TOOLS_FIND_PRODPRODIDS_NOTIN_PRODDESC', 'Are there <i>product_id</i>s in table <i>products_description</i> which are there only for one language ?');
        define('ADMIN_TOOLS_FIND_PRODPRODIDS_NOTIN_PRODDESC_RES', 'There are %s <i>product_id</i>s  in table <i>products_description</i> which can\'t be found in both languages.');
        define('ADMIN_TOOLS_FIND_PRODIDS_MORETHANONE_PRODTOCAT', 'Are ther <i>product_id</i>s in table <i>products_to_categories</i> which occur more than 1 time ?');
        define('ADMIN_TOOLS_FIND_PRODIDS_MORETHANONE_PRODTOCAT_RES', 'There are %s <i>product_id</i>s which occur more than one time in table <i>products_to_categories</i>.');
        define('ADMIN_TOOLS_FIND_PRODIDS_IN_PRODTOCAT_TOP', 'Are there <i>product_id</i>s which are in TOP (= categories_id 0) ?');
        define('ADMIN_TOOLS_FIND_PRODIDS_IN_PRODTOCAT_TOP_RES', 'The following products are in TOP (products_id, products_model):<br /> %s');
        
        define('ADMIN_TOOLS_SET_PROD_STATUS_GUIDANCE', '<h3 class="red">Please be carefull with this</h3>
                                                        <ul class="do-list">
                                                            <li>If you enter a <i>products_supplier_id</i> in the text field below and click "Send" the <i>products_status</i> of all products with the provided <i>products_supplier_id</i> will be set to 0</li>
                                                            <li>The value in the text field is just a placeholder, you have to enter the desired value yourself</li>
                                                            <li>A success message will be displayed</li>
                                                            <li>You can also set a range of <i>products_supplier_id</i> to 0:<span style="color:#c00;"> the range of values you provide will be understood inclduing the ones you enter !!</span></li>
                                                            <li>In case you errorneously set a wrong <i>products_supplier_id</i> to <i>products_status</i> 0 you can reset it back to 1 with help of the second input field</li>
                                                        </ul>');
        define('ADMIN_TOOLS_SET_PROD_STATUS_0', 'Set products_status to 0');
        define('ADMIN_TOOLS_SET_PROD_STATUS_B_0', 'set products_status to 0 for a range <span class="sml" style="color:#c00;">(ATTENTION !! The indicated values will be understood including !!)');
        define('ADMIN_TOOLS_SET_PROD_STATUS_1', 'Set products_status to 1');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_0', 'set <i>products_status</i> to 0 for the following <i>products_supplier_id</i>: ');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_B_01', 'set <i>products_status</i> to 0 between (incl.) <i>products_supplier_id </i>');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_B_01B', ' and (incl.) &nbsp;');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_1', 'set <i>products_status</i> to 1 for the following <i>products_supplier_id</i>: ');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUCCESS_TXT_0', 'The <i>products_status</i> of all products with <i>products_supplier_id</i> %s has been set to 0');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUCCESS_TXT_B_0', 'The <i>products_status</i> of all products with <i>products_supplier_id</i> between %s (incl.) and %s (incl.) has been set to 0');
        define('ADMIN_TOOLS_SET_PROD_STATUS_SUCCESS_TXT_1', 'The <i>products_status</i> of all products with <i>products_supplier_id</i> %s has been set to 1');
        define('ADMIN_TOOLS_SET_PROD_STATUS_ERROR_TXT', '<span style="color:#c00;">No <i>products_supplier_id</i> has been entered or the value was not allowed</span>');
        define('ADMIN_TOOLS_COPY_CONTENTS_TITLE', 'Copy contents in other shops');
        define('ADMIN_TOOLS_COPY_CONTENTS_DESC', '- Contents with content_group from X to Y will be copied in the choosen shops on <i>'.basename(HTTP_SERVER).'</i><br /><span style="color:#ff6868;">The contents of both languages will be copied automatically.</span>');
        define('ADMIN_TOOLS_COPY_ALLOWED_CONTENTS_GUIDANCE', 'Enter allowed coIDs comma-separated without empty spaces.');
        define('ADMIN_TOOLS_COPY_CONTENTS_SAVE_ALLOWED', 'Save');
        define('ADMIN_TOOLS_COPY_CONTENTS_GUIDANCE', '<h4>Vorsicht !</h4><ul><li>Contents whose <i>content_group</i> doesn\'t exists in the aimed shop will be created.</li><li>Contents whose <i>content_group</i> already exists in the aimed shop will be overwritten.</li></ul>');
        define('ADMIN_TOOLS_COPY_CONTENTS_FROM_WHICH_SHOP', 'From which shop: ');
        define('ADMIN_TOOLS_COPY_CONTENTS_TO_SHOPS', ' into all other shops on <i>'.basename(HTTP_SERVER).'</i>');
        define('ADMIN_TOOLS_COPY_CONTENTS_TO_SHOP_SNGL', ' into the following shop on <i>'.basename(HTTP_SERVER).'</i> ');
        define('ADMIN_TOOLS_COPY_CONTENTS_FROM_CNT_GRP', 'from which content_group (incl.): ');
        define('ADMIN_TOOLS_COPY_CONTENTS_TO_CNT_GRP', '&nbsp; | &nbsp;to which content_group (incl.): ');
        define('ADMIN_TOOLS_COPY_CONTENTS_SEND', 'Send');
        define('ADMIN_TOOLS_COPY_CONTENTS_CHOOSE', 'Please choose');
        //EOC scripts
        define('ADMIN_TOOLS_COPY_CONTENTS_EXCL_LANDPAGE_TXT', 'Exclude landing page: ');  //new for checkbox, 05-2021, noRiddle
    break;
}
//EOC language constants
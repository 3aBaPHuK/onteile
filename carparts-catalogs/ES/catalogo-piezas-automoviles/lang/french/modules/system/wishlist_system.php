<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_WISHLIST_SYSTEM_TEXT_TITLE', 'Liste de souhaits');
define('MODULE_WISHLIST_SYSTEM_TEXT_DESCRIPTION', 'La liste de souhaits vous permet d\'enregistrer des articles.');
define('MODULE_WISHLIST_SYSTEM_STATUS_TITLE', 'Activer module ?');
define('MODULE_WISHLIST_SYSTEM_STATUS_DESC', 'Activer liste de souhaits');
?>

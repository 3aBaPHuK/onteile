<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_shipping.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ot_shipping.php,v 1.4 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (ot_shipping.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_SHIPPING_TITLE', 'Costos de envío');
define('MODULE_ORDER_TOTAL_SHIPPING_DESCRIPTION', 'Costos de envío de un pedido');

define('FREE_SHIPPING_TITLE', 'Libre de costos de envío');
define('FREE_SHIPPING_DESCRIPTION', 'Libre de costos de envío a partir de un valor de pedido %s');
  
define('MODULE_ORDER_TOTAL_SHIPPING_STATUS_TITLE', 'Costos de envío');
define('MODULE_ORDER_TOTAL_SHIPPING_STATUS_DESC', '¿Mostrar de costos de envío?');
  
define('MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER_TITLE', 'Orden de clasificación');
define('MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER_DESC', 'Orden de visualización');
  
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_TITLE', 'Permitir libre de costos de envío');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_DESC', '¿Permitir entrega libre de costos de envío?');
  
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_TITLE', 'Libre de costos de envío para pedidos nacionales a partir de');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_DESC', 'Libre de costos de envío a partir de un valor de pedido');

define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_INTERNATIONAL_TITLE', 'Libre de costos de envío para pedidos internacionales a partir de');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_INTERNATIONAL_DESC', 'Libre de costos de envío a partir de un valor de pedido');
  
define('MODULE_ORDER_TOTAL_SHIPPING_DESTINATION_TITLE', 'Libre de costos de envío por zonas');
define('MODULE_ORDER_TOTAL_SHIPPING_DESTINATION_DESC', 'Calcular libre de costos de envío por zonas.');
  
define('MODULE_ORDER_TOTAL_SHIPPING_TAX_CLASS_TITLE', 'Clase de impuestos');
define('MODULE_ORDER_TOTAL_SHIPPING_TAX_CLASS_DESC', 'Seleccione la siguiente clase de impuestos para los costos de envío (sólo para el procesamiento de pedidos).');
?>

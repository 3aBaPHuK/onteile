<?PHP
/* -----------------------------------------------------------------------------------------
   $Id: selfpickup.php 5137 2013-07-18 14:48:00Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce( freeamount.php,v 1.01 2002/01/24 03:25:00); www.oscommerce.com
   (c) 2003 nextcommerce (freeamount.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   selfpickup         Autor: sebthom

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_SELFPICKUP_TEXT_TITLE', 'Récupération en personne');
define('MODULE_SHIPPING_SELFPICKUP_TEXT_DESCRIPTION', 'Récupération en personne de la marchandise dans notre agence');
define('MODULE_SHIPPING_SELFPICKUP_TEXT_WAY', 'Récupération en personne de la marchandise dans notre agence.');
define('MODULE_SHIPPING_SELFPICKUP_ALLOWED_TITLE' , 'Zones permises');
define('MODULE_SHIPPING_SELFPICKUP_ALLOWED_DESC' , 'Veuillez indiquer les zones <b>individuellement</b> pour lesquelles l\'envoi est possible. Par exemple AT,DE');
define('MODULE_SHIPPING_SELFPICKUP_STATUS_TITLE', 'Activer la récupération en personne');
define('MODULE_SHIPPING_SELFPICKUP_STATUS_DESC', 'Voulez-vous proposer la récupération en personne?');
define('MODULE_SHIPPING_SELFPICKUP_SORT_ORDER_TITLE', 'Ordre de tri');
define('MODULE_SHIPPING_SELFPICKUP_SORT_ORDER_DESC', 'Ordre d\'affichage');
?>

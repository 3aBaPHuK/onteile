<?php
/* --------------------------------------------------------------
   $Id: geo_zones.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(geo_zones.php,v 1.11 2003/05/07); www.oscommerce.com 
   (c) 2003	 nextcommerce ( geo_zones.php,v 1.4 2003/08/1); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Zones d\'impôt');

define('TABLE_HEADING_COUNTRY', 'Pays');
define('TABLE_HEADING_COUNTRY_ZONE', 'Région');
define('TABLE_HEADING_TAX_ZONES', 'Zones d\'impôt');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_HEADING_NEW_ZONE', 'Nouvelle zone d\'impôt');
define('TEXT_INFO_NEW_ZONE_INTRO', 'Veuillez entrer la nouvelle zone d\'impôt avec toutes les données importantes');

define('TEXT_INFO_HEADING_EDIT_ZONE', 'Modifier la zone d\'impôt');
define('TEXT_INFO_EDIT_ZONE_INTRO', 'Veuillez effectuer tous les modifications nécessaires.');

define('TEXT_INFO_HEADING_DELETE_ZONE', 'Supprimer la zone d\'impôt');
define('TEXT_INFO_DELETE_ZONE_INTRO', 'Vous êtes sûr de vouloir supprimer cette zone d\'impôt?');

define('TEXT_INFO_HEADING_NEW_SUB_ZONE', 'Nouvelle sous-zone');
define('TEXT_INFO_NEW_SUB_ZONE_INTRO', 'Veuillez entrer une nouvelle sous-zone avec toutes les données importantes');

define('TEXT_INFO_HEADING_EDIT_SUB_ZONE', 'Éditer la sous-zone');
define('TEXT_INFO_EDIT_SUB_ZONE_INTRO', 'Veuillez effectuer tous les modifications nécessaires.');

define('TEXT_INFO_HEADING_DELETE_SUB_ZONE', 'Supprimer la sous-zone');
define('TEXT_INFO_DELETE_SUB_ZONE_INTRO', 'Vous êtes sûr de vouloir supprimer cette sous-zone?');

define('TEXT_INFO_DATE_ADDED', 'ajouté le:');
define('TEXT_INFO_LAST_MODIFIED', 'dernier changement:');
define('TEXT_INFO_ZONE_NAME', 'Nom de la sous-zone:');
define('TEXT_INFO_NUMBER_ZONES', 'Nombre de zones d\'impôt:');
define('TEXT_INFO_ZONE_DESCRIPTION', 'Description:');
define('TEXT_INFO_COUNTRY', 'Pays:');
define('TEXT_INFO_COUNTRY_ZONE', 'Région:');
define('TYPE_BELOW', 'Toutes les régions');
define('PLEASE_SELECT', 'Toutes les régions');
define('TEXT_ALL_COUNTRIES', 'Tous les pays');

define('TEXT_INFO_ZONE_INFO', 'Afficher la remarque de douane pour cette zone d\'impôt?');
define('TEXT_INFO_ZONE_INFO_DEFAULT', 'Afficher la remarque de douane pour cette zone d\'impôt.');
?>

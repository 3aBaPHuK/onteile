<?php
// BOF - Timo Paul (mail[at]timopaul[dot]biz) - 2014-06-22 - customersNotice
$customersNoticeManagerFile = DIR_WS_EXTERNAL . '/customers_notice/classes/CustomersNoticeManager.class.php';
if (!class_exists('CustomersNoticeManager') && file_exists($customersNoticeManagerFile)) {
  require_once $customersNoticeManagerFile;
  CustomersNoticeManager::run();
}
// EOF - Timo Paul (mail[at]timopaul[dot]biz) - 2014-06-22 - customersNotice
?>
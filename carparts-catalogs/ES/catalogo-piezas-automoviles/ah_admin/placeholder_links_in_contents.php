<?php
/***************************************************************
* file: placeholder_links_in_contents.php
* use: system module to define variables for links in contents
* (c) noRiddle 08-2017
* Examples:
* [[LINK_KONTAKT]] 7
* [[LINK_RABATTE]] //online-teile.com/online-teile/userfiles/files/rabatte/rabatte.htm
* [[TEXT_SHOP_ADRESSE]] STORE_NAME_ADDRESS
* [[TEXT_ANY]] blablabla
***************************************************************/

require_once ('includes/application_top.php');
require_once(DIR_FS_INC . 'xtc_wysiwyg.inc.php');

//BOC helper function
function set_chbx($post, $val) {
    global $alert;
    if(isset($post) && $post == $val && $alert !='') {
        return true;
    }
    
    return false;
}

/*
function get_shop_name($shp_name) {
    $eff_shop = '';
    $raw_eff_shop = explode('-', $shp_name);
    array_pop($raw_eff_shop);
    //$raw_eff_shop = array_map('ucwords', $raw_eff_shop);
    $eff_shop = implode($raw_eff_shop);
    
    return $eff_shop;
}
*/
//EOC helper function

$alert = '';
$alertp = '';

//BOC copy Platzhalter to other shops
$shop_arr = array();
$shop_arr[] = array('id' => '0', 'text' => 'Auswählen');
foreach($dataC20 as $shp => $db_dat) {
    if($shp != $shop) {
        $shop_arr[] = array('id' => $shp, 'text' => $shp);
    }
}
//echo '<pre  style="margin:120px 0 0;">'.print_r($shop_arr, true).'</pre>';

$ph_shops_drop = xtc_draw_pull_down_menu('ph_copy_to_where', $shop_arr, isset($_POST['ph_copy_to_where']) ? $_POST['ph_copy_to_where'] : '0');
//$ph_shops_chck = xtc_draw_checkbox_field('ph_copy_to_all', $value, (isset($_POST['ph_copy_to_all']) ? true : false), $compare, 'id="copy-to-all"');
$ph_shops_chck = xtc_draw_checkbox_field('ph_copy_to_all', $value, $checked, $compare, 'id="copy-to-all"');
$ph_shops_upd = xtc_draw_input_field('ph_copy_owr', '', 'id="ph-copy-owr"');


//verify whether in contsnts used placeholders are missing
if(isset($_POST['verify_phs']) && $_POST['verify_phs'] == 'do_verify') {
    //get all placeholders from DB
    $veri_phs_str_arr = array();;
    $veri_ph_qu = xtc_db_query("SELECT ph_in_contents_placeholder FROM nr_placeholder_in_contents");
    while($veri_ph = xtc_db_fetch_array($veri_ph_qu)) {
        $veri_phs_str_arr[] = $veri_ph['ph_in_contents_placeholder'];
    }
    //echo '<br /><br /><br /><br /><br />';
    //echo '<pre>'.print_r($veri_phs_str_arr, true).'</pre>';
    
    //scan texts for placeholders
    $verif_cont_txts = array();
    $vout_str = '';
    //$verify_qu_str = "SELECT content_group, languages_id, content_text FROM content_manager WHERE content_text REGEXP '\\[\\[([a-z_]+)\\]\\]' ORDER BY content_group, languages_id"; //working in phpMyAdmin but not here, strange
    $verify_qu_str = "SELECT content_group, languages_id, content_text FROM content_manager WHERE content_text LIKE '%[[%' AND content_text LIKE '%]]%' ORDER BY content_group, languages_id";
    $verify_qu = xtc_db_query($verify_qu_str);
    //var_dump($verify_qu);
    while($verify_qu_arr = xtc_db_fetch_array($verify_qu)) {
        $verif_cont_txts[$verify_qu_arr['content_group']][$verify_qu_arr['languages_id']] = $verify_qu_arr['content_text'];
    }
    //echo '<pre>'.print_r($verif_cont_txts, true).'</pre>';
    if(!empty($verif_cont_txts)) {
        foreach($verif_cont_txts as $vgr => $vlangs ) {
            foreach($vlangs as $vlid => $vtxt) {
                if(preg_match_all('#(\[\[[a-z_0-9]+\]\])#i', $vtxt, $matches)) {
                    //echo '<pre>$matches[1] for '.$vgr.' lang: '.$vlid.' :'.print_r($matches[1], true).'</pre>';
                    $already_converted_arr = array(); //define array for with lower case used placeholders in contents
                    $to_repl_arr = array(); //define array with to be replaced values
                    $repl_arr = array(); //define array with replacing values
                    
                    $corr_info = false;
                    
                    for($i = 0, $ci = count($matches[1]); $i < $ci; $i++) {
                        if(!in_array($matches[1][$i], $veri_phs_str_arr)) {
                            if(in_array(strtoupper($matches[1][$i]), $veri_phs_str_arr)) {  //is placeholder defined in uppercase ?
                                //BOC build arrays to convert placeholders in content to uppercase
                                if(!in_array($matches[1][$i], $already_converted_arr)) { //don't convert same value multiple times
                                    $already_converted_arr[] = $matches[1][$i]; //collect converted values so we won't convert them multiple times if they appear multiple times
                                    $to_repl_arr[] = $matches[1][$i]; //augment array with to be replaced values
                                    $repl_arr[] = strtoupper($matches[1][$i]); //augment array with replacing values
                                }
                                //EOC build arrays to convert placeholders in content to uppercase
                            } else {
                                $vout_str .= $matches[1][$i].' in Content '.$vgr.' Sprache '.($vlid == '1' ? '"en"' : '"de"').' ist nicht (auch nicht mit anderer Groß- oder Kleinschreibung) als Platzhalter angelegt<br />';
                            }
                        }
                    }
                    
                    //BOC onvert placeholders in content to uppercase
                    if(!empty($to_repl_arr) && !empty($repl_arr)) {
                        $corr_vtxt = str_replace($to_repl_arr, $repl_arr, $vtxt);
                        $corr_qu_str = "UPDATE content_manager SET content_text = '".$corr_vtxt."' WHERE content_group = ".(int)$vgr." AND languages_id = ".(int)$vlid;

                        if($corr_qu = xtc_db_query($corr_qu_str)) {
                            $corr_info = true;
                        }
                        
                        foreach($to_repl_arr as $to_repl_plhold) {
                            $vout_str .= $to_repl_plhold.' in Content '.$vgr.' Sprache '.($vlid == '1' ? '"en"' : '"de"').' ist als großgeschriebener Platzhalter angelegt'.($corr_info === true ? ' <span class="green">(wurde korrigiert)</span>' : '(!! wurde nicht korrigiert)').'<br />';
                        }
                    }
                    //EOC onvert placeholders in content to uppercase
                }
            }
        }
        if($vout_str == '') $vout_str .= '<span class="green">Alles okay.</span>';
    } else {
        if($vout_str == '') $vout_str .= 'Keine Platzhalter in Contents vorhanden.';
    }
}

//copy placeholders to other shops
if((isset($_POST['ph_copy_to_where']) && $_POST['ph_copy_to_where'] != '0') || (isset($_POST['ph_copy_to_all']) && $_POST['ph_copy_to_where'] == '0')) {
    $ph_dat_arr = array();
    $from_qu = xtc_db_query("SELECT * FROM nr_placeholder_in_contents");
    while($from_wh = xtc_db_fetch_array($from_qu)) {
        $ph_dat_arr[$from_wh['ph_in_contents_placeholder']] = $from_wh;
    }

    if(isset($_POST['ph_copy_owr']) && $_POST['ph_copy_owr'] != '') {
        $ph_owr_arr = explode(',', $_POST['ph_copy_owr']);
    }
}

if(isset($_POST['ph_copy_to_where']) && $_POST['ph_copy_to_where'] != '0') {
    if(!isset($_POST['ph_copy_to_all'])) {
        $post_to_wh = $_POST['ph_copy_to_where'];

        $cop_to_link = mysqli_connect($dataC20[$post_to_wh]['sql_server'], $dataC20[$post_to_wh]['sql_user'], $dataC20[$post_to_wh]['sql_pass'], $dataC20[$post_to_wh]['sql_db']);
        if (!$cop_to_link) $alert .= 'Verbindung zu "'.$post_to_wh.'" schlug fehl: ' . mysqli_error($cop_to_link);
        if (!mysqli_select_db($cop_to_link, $dataC20[$post_to_wh]['sql_db'])) die('Konnte Datenbank fuer "'.$post_to_wh.'" nicht selektieren');
        mysqli_set_charset($cop_to_link, 'utf8');

        foreach($ph_dat_arr as $fph => $fph_arr) {
            $fph = mysqli_real_escape_string($cop_to_link, $fph);
            $to_qu = mysqli_query($cop_to_link, "SELECT ph_in_contents_id FROM nr_placeholder_in_contents WHERE ph_in_contents_placeholder = '".$fph."'");
            if (!$to_qu) $alert .= 'Abfragefehler: ' . mysqli_error($cop_to_link);
            if(!mysqli_num_rows($to_qu)) {
                //BOC add routine to replace shop names in ph_in_contents_value when copying
                /*$eff_shp = get_shop_name($shop);
                if(strpos($fph_arr['ph_in_contents_placeholder'], $eff_shp)) {
                    $copy_eff_shp = get_shop_name($post_to_wh);
                    $fph_arr['ph_in_contents_placeholder'] = str_replace($eff_shp, $copy_eff_shp, $fph_arr['ph_in_contents_placeholder']);
                }*/
                //EOC add routine to replace shop names in ph_in_contents_value when copying
                $ins_qu_str = "INSERT INTO nr_placeholder_in_contents (ph_in_contents_type, ph_in_contents_placeholder, ph_in_contents_value, date_added) VALUES('".$fph_arr['ph_in_contents_type']."', '".$fph_arr['ph_in_contents_placeholder']."', '".$fph_arr['ph_in_contents_value']."', now())";
                //echo '<pre style="margin:120px 0 0;">'.$ins_qu_str.'</pre>';
                $ins_qu = mysqli_query($cop_to_link, $ins_qu_str);
                if(!$ins_qu) {
                    $alert .= 'Platzhalter '.$fph.' konnte nicht in Shop '.$post_to_wh.' kopiert werden<br />';
                } else {
                    $alert .= 'Platzhalter '.$fph.' wurde in Shop '.$post_to_wh.' kopiert<br />';
                }
            } else {
                if(isset($ph_owr_arr) && is_array($ph_owr_arr) && !empty($ph_owr_arr)) {
                    if(in_array($fph_arr['ph_in_contents_id'], $ph_owr_arr)) {
                        $upd_qu_str = "UPDATE nr_placeholder_in_contents SET ph_in_contents_type = '".$fph_arr['ph_in_contents_type']."', ph_in_contents_value = '".$fph_arr['ph_in_contents_value']."', last_modified = now() WHERE ph_in_contents_placeholder = '".$fph."'";
                        $upd_qu = mysqli_query($cop_to_link, $upd_qu_str);
                        if(!$upd_qu) {
                            $alert .= 'Platzhalter '.$fph.' konnte nicht in Shop '.$post_to_wh.' upgedatet werden<br />';
                        } else {
                            $alert .= 'Platzhalter '.$fph.' wurde in Shop '.$post_to_wh.' upgedatet<br />';
                        }
                    }
                } else {
                    $alert .= 'Der Shop '.$post_to_wh.' hat den Platzhalter '.$fph.' aus dem vorliegenden Shop bereits.<br />';
                }
            }
            
            //echo '<pre style="margin:120px 0 0;">PH '.$fph.' existiert in '.$post_to_wh.' ? '.(mysqli_num_rows($to_qu) ? 'yes' : 'no').'</pre>';
        }

        //restore or generate cardinality
        mysqli_query($cop_to_link, "ANALYZE TABLE nr_placeholder_in_contents");

        //echo '<pre>'.print_r($ph_dat_arr, true).'</pre>';

        mysqli_close($cop_to_link);

        //delete DB cache if activated
        if(defined('DB_CACHE') && DB_CACHE == 'true') {
            clear_dir($path.$post_to_wh.'/cache/');
        }
    } else {
        $alert .= 'Wenn Sie in alle Shops kopieren möchten oben im Dropdown bitte nichts auswählen (also "Auswählen").<br />Wenn Sie in lediglich einen bestimmten Shop kopieren wollen, bitte die Checkbox für alle Shops enthaken.';
    }
} else if(isset($_POST['ph_copy_to_all'])) {
    if($_POST['ph_copy_to_where'] == '0') {
        unset($dataC20[$shop]); //don't copy to shop of origin
        //BOC unset catalogues
        if(isset($dataC20['katalog_shop_test'])) unset($dataC20['katalog_shop_test']);
        if(isset($dataC20['oem-kataloge'])) unset($dataC20['oem-kataloge']);
        if(isset($dataC20['oem-catalogs'])) unset($dataC20['oem-catalogs']);
        //EOC unset catalogues
        foreach($dataC20 as $cshp => $cdb_dat) {
            //$fph = mysqli_real_escape_string($cop_to_link, $fph); //?? what is this ??, me idiot, noRiddle
            $cop_to_link = mysqli_connect($dataC20[$cshp]['sql_server'], $dataC20[$cshp]['sql_user'], $dataC20[$cshp]['sql_pass'], $dataC20[$cshp]['sql_db']);
            if (!$cop_to_link) $alert .= 'Verbindung zu "'.$cshp.'" schlug fehl: ' . mysqli_error($cop_to_link).'<br />';
            if (!mysqli_select_db($cop_to_link, $dataC20[$cshp]['sql_db'])) die('Konnte Datenbank fuer "'.$cshp.'" nicht selektieren<br />');
            mysqli_set_charset($cop_to_link, 'utf8');
            
            foreach($ph_dat_arr as $fph => $fph_arr) {
                $fph = mysqli_real_escape_string($cop_to_link, $fph);
                $to_qu = mysqli_query($cop_to_link, "SELECT ph_in_contents_id FROM nr_placeholder_in_contents WHERE ph_in_contents_placeholder = '".$fph."'");
                if (!$to_qu) $alert .= 'Abfragefehler: ' . mysqli_error($cop_to_link).'<br />';
                if(!mysqli_num_rows($to_qu)) {
                    $ins_qu_str = "INSERT INTO nr_placeholder_in_contents (ph_in_contents_type, ph_in_contents_placeholder, ph_in_contents_value, date_added) VALUES('".$fph_arr['ph_in_contents_type']."', '".$fph_arr['ph_in_contents_placeholder']."', '".$fph_arr['ph_in_contents_value']."', now())";
                    $ins_qu = mysqli_query($cop_to_link, $ins_qu_str);
                    if(!$ins_qu) {
                        $alert .= 'Platzhalter '.$fph.' konnte nicht in Shop '.$cshp.' kopiert werden<br />';
                    } else {
                        $alertp .= 'Platzhalter '.$fph.' wurde in Shop '.$cshp.' kopiert<br />';
                    }
                    
                } else {
                    if(isset($ph_owr_arr) && is_array($ph_owr_arr) && !empty($ph_owr_arr)) {
                        if(in_array($fph_arr['ph_in_contents_id'], $ph_owr_arr)) {
                            $upd_qu_str = "UPDATE nr_placeholder_in_contents SET ph_in_contents_type = '".$fph_arr['ph_in_contents_type']."', ph_in_contents_value = '".$fph_arr['ph_in_contents_value']."', last_modified = now() WHERE ph_in_contents_placeholder = '".$fph."'";
                            $upd_qu = mysqli_query($cop_to_link, $upd_qu_str);
                            if(!$upd_qu) {
                                $alert .= 'Platzhalter '.$fph.' konnte nicht in Shop '.$cshp.' upgedatet werden<br />';
                            } else {
                                $alert .= 'Platzhalter '.$fph.' wurde in Shop '.$cshp.' upgedatet<br />';
                            }
                        }
                    } else {
                        $alert .= 'Der Shop '.$cshp.' hat den Platzhalter '.$fph.' bereits.<br />';
                    }
                }
                
                //echo '<pre style="margin:120px 0 0;">PH '.$fph.' existiert in '.$cshp.' ? '.(mysqli_num_rows($to_qu) ? 'yes' : 'no').'</pre>';
            }

            //restore or generate cardinality
            mysqli_query($cop_to_link, "ANALYZE TABLE nr_placeholder_in_contents");

            mysqli_close($cop_to_link);

            //delete DB cache if activated
            if(defined('DB_CACHE') && DB_CACHE == 'true') {
                clear_dir($path.$cshp.'/cache/');
            }
        }
        //echo '<pre>'.print_r($ph_dat_arr, true).'</pre>';
    } else {
        $alert .= 'Wenn Sie in alle Shops kopieren möchten oben im Dropdown bitte nichts auswählen (also "Auswählen").<br />Wenn Sie in lediglich einen bestimmten Shop kopieren wollen, bitte die Checkbox für alle Shops enthaken.';
    }
}
//EOC copy Platzhalter to other shops

//BOC handle saving of data
//BOC show and define Platzhalter
if(isset($_POST['save_action']) && $_POST['save_action'] == 'save') {
    if(($_POST['ph_placeholder']['new'] != '' || $_POST['ph_value']['new'] != '') && !isset($_POST['ph_type']['new'])) {
        $alert .= 'Wenn Sie einen neuen Platzhalter anlegen möchten wählen Sie bitte einen Platzhalter-Typ.<br />Wenn nicht, löschen Sie bitte die Einträge in "NEU:" oder stellen den Platzhalter-Typ auf "Nichts wählen".';
    }
    
    foreach($_POST['ph_type'] as $id => $uval) {
        if($id != 'new') {
            if(!isset($_POST['del'][$id])) {
                if($_POST['ph_placeholder'][$id] == '') { // || $_POST['ph_value'][$id] == '') { // Andreas wants possible empty values, 11.09.2017, noRiddle
                    $alert .= 'Es fehlen Angaben bei Platzhalter '.$id.'<br />';
                } else {
                    $upd_arr = array('ph_in_contents_type' => (int)$uval,
                                     'ph_in_contents_placeholder' => strtoupper($_POST['ph_placeholder'][$id]), //save placeholder in uppercase
                                     'ph_in_contents_value' => $_POST['ph_value'][$id],
                                     'last_modified' => 'now()'
                                    );
                
                    xtc_db_perform('nr_placeholder_in_contents', $upd_arr, 'update', "ph_in_contents_id = '".(int)$id."'");
                }
            } else {
                if(xtc_db_query("DELETE FROM nr_placeholder_in_contents WHERE ph_in_contents_id = ".(int)$id)) {
                    $alertp .= 'Der Platzhalter '.$id.' wurde gelöscht'.($cnt_id > 1 ? '<br />' : '');
                }
            }
        } else {
            if(isset($_POST['ph_type']['new']) && $_POST['ph_type']['new'] != '0') {
                if($_POST['ph_placeholder']['new'] == '') { // || $_POST['ph_value']['new'] == '') { // Andreas wants possible empty values, 11.09.2017, noRiddle
                    $alert .= 'Es fehlen Angaben bei Platzhalter '.$id.'.';
                } else {
                    $verify_dbl_entr_qu = xtc_db_query("SELECT ph_in_contents_id FROM nr_placeholder_in_contents WHERE ph_in_contents_type = '".(int)$uval."' AND ph_in_contents_value = '".xtc_db_input($_POST['ph_value']['new'])."'");
                    if(xtc_db_num_rows($verify_dbl_entr_qu) > 0) {
                        $alert .= 'Es gibt bereits einen Platzhalter-Wert '.$_POST['ph_value']['new'].' mit dem gewählten Platzhalter-Typ.<br />Um doppeltes Anlegen zu vermeiden bitte überprüfen.<br />';
                    } else {
                        $ins_arr = array('ph_in_contents_type' => (int)$uval,
                                         'ph_in_contents_placeholder' => strtoupper($_POST['ph_placeholder']['new']), //save placeholder in uppercase
                                         'ph_in_contents_value' => $_POST['ph_value']['new'],
                                         'date_added' => 'now()'
                                        );
                    
                        if(xtc_db_perform('nr_placeholder_in_contents', $ins_arr)) {
                            $alertp .= 'Der Platzhalter '.$ins_arr['ph_in_contents_placeholder'].' wurde neu angelegt.';
                        }
                    }
                }
            }
        }
    }
    
    //delete DB cache if activated
    if(defined('DB_CACHE') && DB_CACHE == 'true') {
        clear_dir(SQL_CACHEDIR);
    }

    //echo '<pre style="margin:120px 0 0;">'.print_r($_POST, true).'</pre>';
    //echo '<pre style="margin:120px 0 0;">'.$_POST['ph_type']['new'].'</pre>';
}
//BOC show and define Platzhalter
//EOC handle saving of data

$how_many_ph_qu_str = "SELECT * FROM nr_placeholder_in_contents ORDER BY ph_in_contents_placeholder";
$how_many_ph_qu = xtc_db_query($how_many_ph_qu_str);
//$how_many_ph = xtc_db_num_rows($how_many_ph_qu);

$inpt_str_type = '';
$ninpt_str_type = '';
$ph_ids = array();

//helper to see output
//echo '<br /><br /><br /><br /><br /><br />';

//$cnt = 1;
while($how_many_ph_arr = xtc_db_fetch_array($how_many_ph_qu)) {    
    //BOC store ph_in_contents_id in array for wysiwyg
    //$ph_ids[] = $how_many_ph_arr['ph_in_contents_id'];
    $ph_ids[$how_many_ph_arr['ph_in_contents_id']] = $how_many_ph_arr['ph_in_contents_type'];
    //EOC store ph_in_contents_id in array for wysiwyg
    $cnt = $how_many_ph_arr['ph_in_contents_id'];
    
    $conts_arr = array();
    $cont_qu_str = "SELECT content_group, languages_id, content_title FROM content_manager WHERE content_text LIKE '%".$how_many_ph_arr['ph_in_contents_placeholder']."%' ORDER BY content_group ASC, languages_id DESC";
    $cont_qu = xtc_db_query($cont_qu_str);
    while($conts = xtc_db_fetch_array($cont_qu)) {
        if($conts['languages_id'] == '1') {$conts['languages_id'] = 'en';} else if($conts['languages_id'] == '2') {$conts['languages_id'] = 'de';}
        $conts_arr[$conts['content_group']][$conts['languages_id']] = $conts['content_title'];
    }
    //echo '<pre>'.print_r($conts_arr, true).'</pre>';
    //if(!empty($conts_arr)) { //can't do that here because loop will be interrputed if array is empty
        $where_ph_str = 'kommt vor in: '.(empty($conts_arr) ? '<span class="red">nirgends</span>' : '');
        foreach($conts_arr as $gr => $lid) {
            //BOC indicate missing languages
            $add_before_str = '';
            $add_after_str = '';
                
            if(isset($conts_arr[$gr]['en']) && !isset($conts_arr[$gr]['de']) && mb_substr($how_many_ph_arr['ph_in_contents_placeholder'], -5, 3, $_SESSION['language_charset']) != '_EN') {
                $add_before_str .= '<span class="red"><a href="'.xtc_href_link('../shop_content.php', 'coID='.$gr.'&language=de').'" target="_blank" class="tooltip">'
                                  .$gr.':de<span>Content: '.$gr.'<br />Sprache: de<br />Platzhalter: fehlt</span>'
                                  .'</a></span> | ';
            } else if(isset($conts_arr[$gr]['de']) && !isset($conts_arr[$gr]['en']) && mb_substr($how_many_ph_arr['ph_in_contents_placeholder'], -5, 3, $_SESSION['language_charset']) != '_DE') {
                $add_after_str .= '<span class="red"><a href="'.xtc_href_link('../shop_content.php', 'coID='.$gr.'&language=en').'" target="_blank" class="tooltip">'
                                  .$gr.':en<span>Content: '.$gr.'<br />Sprache: en<br />Platzhalter: fehlt</span>'
                                  .'</a></span> | ';
            }
            //EOC indicate missing languages
            
            foreach($lid as $lng => $pht) {
                //if($lng == '1') {$lng = 'en';} else if($lng == '2') {$lng = 'de';}  //do that above when creating array
                $where_ph_str .= $add_before_str.'<a href="'.xtc_href_link('../shop_content.php', 'coID='.$gr.'&language='.$lng).'" target="_blank" class="tooltip">'
                                .$gr.':'.$lng.'<span>Content: '.$gr.'<br />Sprache: '.$lng.'<br />Name: '.$pht.'</span>'
                                .'</a> | '.$add_after_str;
            }
        }
    //}
    //hinzugefügt am: '.date("d.m.Y H:i:s",strtotime($how_many_ph_arr['date_added'])).' -- | -- zuletzt geändert am: '.($how_many_ph_arr['last_modified'] >  0 ? date("d.m.Y H:i:s",strtotime($how_many_ph_arr['last_modified'])) : '').'
    $inpt_str_type .= '<tr><td colspan="4" class="bl"></td></tr>
                      <tr>
                        <td colspan="4" class="head">
                            <div class="sw-type">'
                            .$cnt.'. Platzhalter-Typ: <span class="ud"></span> <span class="sml flr">'.(isset($where_ph_str) && $where_ph_str != '' ? $where_ph_str : '').'</span>
                            </div>
                        </td>
                      </tr>
                      <tr>
                      <td colspan="2" class="w50">
                      <ul class="chck-bxs">
                       <li>'.xtc_draw_checkbox_field('del['.$cnt.']', $value, $checked, $compare, 'id="del-'.$cnt.'"').' <label for="del-'.$cnt.'"><span class="red">Platzhalter löschen</span></label>
                        <li>'.xtc_draw_selection_field('ph_type['.$cnt.']', 'radio', '1', ($how_many_ph_arr['ph_in_contents_type'] == '1' ? true : false), '', 'id="ext-l-'.$cnt.'"').' <label for="ext-l-'.$cnt.'">Externer Link <span class="smlt">(z.B. //online-teile.com)</span></label></li>
                        <li>'.xtc_draw_selection_field('ph_type['.$cnt.']', 'radio', '2', ($how_many_ph_arr['ph_in_contents_type'] == '2' ? true : false), '', 'id="int-l-'.$cnt.'"').' <label for="int-l-'.$cnt.'">Interner Link <span class="smlt">(z.B. account.php oder login.php)</span></label></li>
                        <li>'.xtc_draw_selection_field('ph_type['.$cnt.']', 'radio', '3', ($how_many_ph_arr['ph_in_contents_type'] == '3' ? true : false), '', 'id="prod-l-'.$cnt.'"').' <label for="prod-l-'.$cnt.'">Link zu Produkt <span class="smlt">(nur Artikelnummer als Wert angeben)</span></label></li>
                      </ul>
                      </td>
                      <td colspan="2" class="w50">
                      <ul class="chck-bxs">
                      <li>'.xtc_draw_selection_field('ph_type['.$cnt.']', 'radio', '4', ($how_many_ph_arr['ph_in_contents_type'] == '4' ? true : false), '', 'id="cat-l-'.$cnt.'"').' <label for="cat-l-'.$cnt.'">Link zu Kategorie <span class="smlt">(nur letzte Kategorie-Id in der URL angeben, ohne Unterstrich davor)</span></label></li>
                        <li>'.xtc_draw_selection_field('ph_type['.$cnt.']', 'radio', '5', ($how_many_ph_arr['ph_in_contents_type'] == '5' ? true : false), '', 'id="cont-l-'.$cnt.'"').' <label for="cont-l-'.$cnt.'">Link zu Content <span class="smlt">(nur Gruppen-ID angeben, z.B. 7 für Kontakt)</span></label></li>
                        <li>'.xtc_draw_selection_field('ph_type['.$cnt.']', 'radio', '6', ($how_many_ph_arr['ph_in_contents_type'] == '6' ? true : false), '', 'id="const-'.$cnt.'"').' <label for="const-'.$cnt.'">Konstante <span class="smlt">(Wert aus <i>Konfiguration => Mein Shop (keine sprachabhängigen Konstanten)</i>)</span></label></li>
                        <li>'.xtc_draw_selection_field('ph_type['.$cnt.']', 'radio', '7', ($how_many_ph_arr['ph_in_contents_type'] == '7' ? true : false), '', 'id="arbit-'.$cnt.'"').' <label for="arbit-'.$cnt.'">Beliebiger Wert <span class="smlt">(für beliebigen Text, auch HTML)</span></label></li>
                      </ul>
                      </td>
                      </tr>';
    if($how_many_ph_arr['ph_in_contents_type'] == '7') {
        $inpt_str_type .= '<tr>
                            <td colspan="4">Platzhalter: '.xtc_draw_input_field('ph_placeholder['.$cnt.']', $how_many_ph_arr['ph_in_contents_placeholder']).'</td>
                           </tr>
                           <tr>
                            <td colspan="4" class="cke-tbl">Platzhalter-Wert: '.xtc_draw_textarea_field('ph_value['.$cnt.']', '', '40', '3', $how_many_ph_arr['ph_in_contents_value']).'</td>
                          </tr>';
    } else {
        $inpt_str_type .= '<tr>
                            <td colspan="2">Platzhalter: '.xtc_draw_input_field('ph_placeholder['.$cnt.']', $how_many_ph_arr['ph_in_contents_placeholder']).'</td>
                            <td>Platzhalter-Wert: </td><td>'.xtc_draw_textarea_field('ph_value['.$cnt.']', '', '40', '3', $how_many_ph_arr['ph_in_contents_value']).'</td>
                          </tr>';
    }
}

$ninpt_str_type .= '<tr><td colspan="4" class="bl"></td></tr>
                   <tr>
                     <td colspan="4" class="grhead">
                       <div class="sw-type">NEU: Platzhalter-Typ: <span class="ud"></span></div>
                     </td>
                   </tr>
                   <tr>
                   <td colspan="2" class="w50">
                   <ul class="chck-bxs">
                     <li>'.xtc_draw_selection_field('ph_type[new]', 'radio', '0', set_chbx($_POST['ph_type']['new'], '0'), '', 'id="no-new"').' <label for="no-new">Nichts wählen <span class="smlt">(es soll kein neuer Platzhalter gspeichert werden)</span></label></li>
                     <li>'.xtc_draw_selection_field('ph_type[new]', 'radio', '1', set_chbx($_POST['ph_type']['new'], '1'), '', 'id="ext-l-new"').' <label for="ext-l-new">Externer Link <span class="smlt">(z.B. //online-teile.com)</span></label></li>
                     <li>'.xtc_draw_selection_field('ph_type[new]', 'radio', '2', set_chbx($_POST['ph_type']['new'], '2'), '', 'id="int-l-new"').' <label for="int-l-new">Interner Link <span class="smlt">(z.B. account.php oder login.php)</span></label></li>
                     <li>'.xtc_draw_selection_field('ph_type[new]', 'radio', '3', set_chbx($_POST['ph_type']['new'], '3'), '', 'id="prod-l-new"').' <label for="prod-l-new">Link zu Produkt <span class="smlt">(nur Artikelnummer als Wert angeben)</span></label></li>
                   </ul>
                   </td>
                   <td colspan="2" class="w50">
                   <ul class="chck-bxs">
                     <li>'.xtc_draw_selection_field('ph_type[new]', 'radio', '4', set_chbx($_POST['ph_type']['new'], '4'), '', 'id="cat-l-new"').' <label for="cat-l-new">Link zu Kategorie <span class="smlt">(nur letzte Kategorie-Id in der URL angeben, ohne Unterstrich davor)</span></label></li>
                     <li>'.xtc_draw_selection_field('ph_type[new]', 'radio', '5', set_chbx($_POST['ph_type']['new'], '5'), '', 'id="cont-l-new"').' <label for="cont-l-new">Link zu Content <span class="smlt">(nur Gruppen-ID angeben, z.B. 7 für Kontakt)</span></label></li>
                     <li>'.xtc_draw_selection_field('ph_type[new]', 'radio', '6', set_chbx($_POST['ph_type']['new'], '6'), '', 'id="const-new"').' <label for="const-new">Konstante <span class="smlt">(Wert aus <i>Konfiguration => Mein Shop  (keine sprachabhängigen Konstanten)</i>)</span></label></li>
                     <li>'.xtc_draw_selection_field('ph_type[new]', 'radio', '7', set_chbx($_POST['ph_type']['new'], '7'), '', 'id="arbit-new"').' <label for="arbit-new">Beliebiger Wert <span class="smlt">(für beliebigen Text, auch HTML)</span></label></li>
                   </ul>
                   </td>
                   </tr>';
$ninpt_str_type .= '<tr>
                     <td colspan="4">Platzhalter: '.xtc_draw_input_field('ph_placeholder[new]', (isset($_POST['ph_placeholder']['new']) && $alert !='' ? $_POST['ph_placeholder']['new'] : '')).'</td>
                    </tr>
                    <tr>
                     <td colspan="4">Platzhalter-Wert: '.xtc_draw_textarea_field('ph_value[new]', '', '40', '3', (isset($_POST['ph_value']['new']) && $alert !='' ? $_POST['ph_value']['new'] : '')).'</td>
                   </tr>';

require (DIR_WS_INCLUDES.'head.php');

if (USE_WYSIWYG == 'true') {
    $query = xtc_db_query("SELECT code FROM ".TABLE_LANGUAGES." WHERE languages_id = '".$_SESSION['languages_id']."'");
	$data = xtc_db_fetch_array($query);
    
    foreach($ph_ids as $phid => $phtype) {
        if($phtype == '7' || $phtype == 'new') {
            echo xtc_wysiwyg('ph_value', $data['code'], $phid);
        }
    }
    
    echo xtc_wysiwyg('ph_value', $data['code'], 'new');

}
?>
<script>
$(function() {
    $('input[type="radio"]').on('change', function() {
        var ipt_val = $('input[type="radio"]:checked').val();
        $('#cke_php_value['+ipt_val+']').show();
    });
});
</script>
<?php
?>
<style>
h1 {margin:8px 0 20px; font-size:28px; border-bottom:1px solid #ccc;}
h2 {margin:2px 0 8px; font-size:22px;}
h2 .sml {display:inline-block; font-size:12px; line-height:13px;}
#gen-cont {max-width:1000px; margin:10px 14px 35px; font-size:15px;}
#copy-conts, #verify-ph {margin:0 0 15px; padding:5px; border:1px solid #ccc;}

#alert {margin:10px 0; padding:5px; background:#fff; border:1px solid #c00; color:#c00; font-weight:bold;}

table {border-collapse:collapse; width:100%;}
table td {padding:5px; background:#e8e8e8; border:1px solid #b7b7b7; position:relative; vertical-align:middle;}
/*table td[colspan="2"] {width:50%;}*/
table td.head, table td.grhead {color:#fff;}
table td.head {background:#a8a8a8;}
table td.grhead {background:#88ad86;}
table td.head div, table td.grhead div {/*overflow:hidden;*/}
table td.head div .flr, table td.grhead div .flr {float:right;}
table td.bl {height:0; background:#757575;}
table td.w35 {width:35%;}
table td.w50 {width:50%;}
table td.w65 {width:65%;}
a.tooltip {position:relative;}
a.tooltip span {
position:absolute;
left:-66px; bottom:240%;
width:150px;
padding:2px 3px;
color:#333;
border:1px solid #ccc;
border-radius:5px;
box-shadow:0 0 4px #9c9c9c;
background:#fff;
visibility:hidden;
opacity:0;
transition:all 0.4s ease 0.1s;
}
a.tooltip span:after {
content:'';
position:absolute;
top:100%;
left:70px;
height:0;
width:0;
border:10px solid;
border-color:#fff transparent transparent transparent;
}
a.tooltip:hover span {bottom:145%; visibility:visible; opacity:1;}

label, input[type="radio"] {cursor:pointer;}

.info {margin:0 0 20px;}
.acc-info-more-img {padding:5px; background:#68aa68; color:#fff; font-size:18px; cursor:pointer;}
.acc-info-more-img span.ud:after {content:'\2193'; padding:0 0 0 10px;}
.acc-info-more-img span.ud.close-acc:after {content:'\2191';}
.sw-type {cursor:pointer;}
.sw-type span.ud:after {content:'\2193'; padding:0 10px 0 10px;}
.sw-type span.ud.close-acc:after {content:'\2191';}
span.inf {display:inline-block; width:20px; height:20px; line-height:18px; text-align:center; background:#777; color:#ffff03; font-weight:bold; border-radius:50%;}
.acc-info {display:none; margin:0; padding:5px; border:2px solid #68aa68;}
.acc-info li {margin:0 0 0 18px;}

.green{color:#1bab1b;}
.red {color:#ff3e3e;}

.chck-bxs {list-style-type:none; margin:0; padding:0 0 0 10px; display:none;}
.chck-bxs li {margin:0; padding:0;}
.sml {font-size:13px;}
.sml a {color:#fff4b6;}
.sml .red a {color:#ff3e3e;}
.smlt {font-size:11px;}
.sve {margin:10px 0; text-align:center;}
.sve button {padding:0 9px 1px; font-size:15px; line-height:20px; font-weight:bold; background:#cb8d30; color:#fff; border:1px solid #ae792a; border-radius:5px; cursor:pointer;}
.sve button:hover {box-shadow:0 0 6px #626262 inset;}
.sve .gr-butt {padding:0 9px 3px; background:#a8a8a8; border:1px solid #8f8f8f;} /*{background:#68aa68; border:1px solid #659b65;}*/
.sve .edge-fix {position:fixed; left:-40px; top:50%; transform:rotate(-90deg); transition:left .5s ease;}
.sve .edge-fix:hover {left:-36px;}
input[type="text"] {width:280px; position:relative; transition:width .8s ease;}
input[type="text"]:focus {width:370px; border:1px solid #c00; z-index:1;}

/*[id^=ph_value] {
    display:block !important;
    visibility:visible !important;
}
[id^=cke_ph_value] {
    display:inline-block !important;
}*/
/*.cke_reset {vertical-align:top !important;}*/
/*.cke {display:inline-block !important;}*/ /*will have negative results for the style dropdowns in editor*/
.cke-tbl > div {display:inline-block !important; vertical-align:top !important;}

</style>
</head>
<body>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
   
    <div id="gen-cont">
        <h1>Platzhalter für Contents</h1>
        <div id="inp-wrap">
            <?php if(isset($alert) && $alert !='') { ?>
            <div id="alert">
                <?php echo $alert; ?>
            </div>
            <?php } else if(isset($alertp) && $alertp !='') { ?>
            <div id="alert">
                <?php echo $alertp; ?>
            </div>
            <?php } ?>
            <div id="verify-ph">
                <h2>Prüfen ob Platzhalter in Contents vorkommen die nicht angelegt sind<br /><span class="sml">(Im Falle Platzhalter in Contents versehentlich mit Kleinbuchstaben versehen sind wird dies hiermit autom. korrigiert.)</span></h2>
                <?php echo xtc_draw_form('veri_phs', 'placeholder_links_in_contents.php'); ?>
                    <table>
                        <tr>
                            <td><?php echo xtc_draw_hidden_field('verify_phs', 'do_verify'); ?><div class="sve"><button type="submit">Prüfen</button></div></td>
                        </tr>
                        <?php if(isset($vout_str) && $vout_str != '') { ?>
                        <tr>
                            <td>
                                <div id="alert">
                                    <?php echo $vout_str; ?>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </form>
            </div>
            <div id="copy-conts">
                <h2>Platzhalter in andere Shops kopieren <span class="sml">(Es werden nur Platzhalter kopiert die im jeweiligen Ziel-Shop nicht vorhanden sind.)</span></h2>
                <?php echo xtc_draw_form('copy_phs', 'placeholder_links_in_contents.php'); ?>
                    <table>
                        <tr>
                            <td>
                                Alle gespeicherten Platzhalter in folgenden Shop kopieren: <?php echo $ph_shops_drop; ?><br />
                                <label for="copy-to-all">Alle gespeicherten Platzhalter in alle Shops kopieren <span class="sml">(nur 2.X-Shops, also alle die im Dropdown oben zu finden sind)</span>:</label> <?php echo $ph_shops_chck; ?>
                                <br /><label for="ph-copy-owr">Nrn. der Platzhalter die im Ziel-Shop überschrieben werden sollen <span class="sml">(komma-separiert)</span>:</label> <?php echo $ph_shops_upd; ?>
                            </td>
                            <td rowspan="2">
                                <div class="sve"><button type="submit">Kopieren</button></div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="info">
                <div class="acc-info-more-img"><span class="inf">i</span> Infos zum Bearbeiten und zur Nutzung <span class="ud"></span></div>
                <ul class="acc-info">
                    <li>
                        <strong>Platzhalter definieren:</strong> Alle Platzhalter eindeutig benennen und in [[]] einfassen.
                        <br />Es düfen für Platzhalter lediglich folgende Zeichen benutzt werden: a-z, A-Z, _, 0-9 (also: Buchstaben, Unterstrich und Zahlen, <u>kein Bindestrich</u>!)
                        <br />Beispiel: [[LINK_KONTAKT]]
                        <br /><span class="green">Kleingeschriebene Platzhalter werden automatisch großgeschrieben gespeichert.</span>
                    </li>
                    <li>
                        <strong>Platzhalter Wert definieren:</strong>
                        <ul>
                            <li><span class="red"><strong>Bei Platzhalter-Wert darf nur dann ein Zeilenumbruch benutzt werden wenn als Platzhalter-Typ "Beliebiger Wert" gewählt wurde !</strong></span></li>
                            <li><u>Externer Link:</u><br />"Externer Link" wählen und als Wert die URL ohne Protokoll eingeben (z.B. //online-teile.com).<br />Ausnahme!! wenn der Link in einem Content benutzt wird welcher in E-Mails verwendet wird (z.B. Widerrufsbelehrung). In diesem Fall Link voll ausschreiben, inkl. Protokoll (https://).</li>
                            <li><u>Interner Link:</u><br />Wählen wohin der Link gehen soll und als Wert nur die ID eingeben (z.B. 7 für Kontakt)</li>
                            <li><u>Werte aus der Backend-Konfiguration (z.B. "Mein Shop"):</u><br />"Konfiguration" auswählen und als Wert die Konstante eingeben.
                                <br />Den Namen der Konstanten findet man indem man in das auszulesende Feld mit der rechten Maustaste klickt und "Element untersuchen" wählt. Dort steht dann im Attribute <i>name</i> der Name der Konstanten in Großbuchstaben (z.B. STORE_NAME_ADDRESS). Diesen Wert inkl. der Großbuchstaben übernehmen.<br />
                                <span class="red">Achtung, keine sprachabhängigen Konstanten aus der Konfiguration verwenden.<br />(z.B. nicht CONTACT_US_EMAIL_ADDRESS[DE] oder CONTACT_US_EMAIL_ADDRESS[EN] aus "E-Mail Optionen").</span></li>
                            <li>
                                <u>Beliebiger Wert:</u><br />"Beliebiger Wert" auswählen und als Wert den beliebigen Text einfügen.
                                <br />Hier ist HTML möglich und es sind Zeilenumbrüche erlaubt, im Gegensatz zu allen anderen Platzhalter-Typen.
                            </li>
                            <li><u>Platzhalter löschen:</u><br />Platzhalter können gelöscht aber auch überschrieben werden.<br />Denken Sie daran, daß, wenn sie einen Platzhalter, also nicht den Platzhalter-Wert, überschreiben, dieser auch so in den Contents eingepflegt sein muß.</li>
                        </ul>
                    </li>
                    <li>
                        <strong>Platzhalter verwenden:</strong>
                        <br />Der Platzhalter muß im Content-Manager genauso benutzt werden wie er definiert wurde, also z.B. [[LINK_KONTAKT]].
                        <br /><span class="green">Sollte ein Platzhalter in einem Content versehentlich mit Kleinbuchstaben versehen sein wird dies automatisch mittels der Funktion "Prüfen" oben korrigiert.</span>
                        <br />Bei Links kann auch die Funktion des Link-Editierens des Editors benutzt werden, man muß also nicht in die Quelltext-Ansicht gehen.
                        <br />D.h.: Bei "URL" muß der Platzhalter eingegeben werden, bei "Protokoll" muß "&lt;andere&gt;" ausgewählt werden.
                        <br />Bei "Zielseite" kann nach wie vor ausgewählt werden ob der Link in einem neuen Tab/Fenster oder im selben Fenster aufgehen soll.
                    </li>
                </ul>
            </div>
            <div id="ph-list">
                <?php echo xtc_draw_form('content_placeholders', 'placeholder_links_in_contents.php'); ?>
                <div class="sve">Platzhalter speichern: <button type="submit">Speichern</button> -- | --  alle Platzhalter-Typen öffnen/schließen: <button class="gr-butt" id="op-cl-all" type="button">&#8595; &#8593;</button></div>
                <table>
                    <?php
                    echo $inpt_str_type;
                    ?>
                </table>
                <div id="newentr">
                    <table class="ninp-cont">
                        <?php
                        echo $ninpt_str_type;
                        ?>
                    </table>
                    <?php echo xtc_draw_hidden_field('save_action', 'save'); ?>
                </div>
                <div class="sve"><button type="submit" class="edge-fix">Speichern</button></div>
                <div class="sve">Platzhalter speichern: <button type="submit">Speichern</button></div>
                </form>
            </div>
            
        </div>
    </div>

<script>
$(function(){
    $('.acc-info-more-img').click(function() {
        $(this).next('.acc-info').slideToggle(500);
        $(this).find('span').toggleClass('close-acc');
    });
    
    //BOC copy and remove input fields
    var $buttm = $('#more-inp'),
        $buttl = $('#less-inp'),
        $newentr = $('#newentr');
        
    $buttm.click(function(e) {
        var $inpcont = $('.ninp-cont'),
            $inpclonelast = $inpcont.last().clone();
            e.preventDefault();
            $inpclonelast.find('input').val('');
            $inpclonelast.find('input.sml-inp').val('1');
            $inpclonelast.find('span.msg').empty();
            $inpclonelast.appendTo($newentr);
    });
    
    $('.sw-type').click(function() {
        $(this).parent().parent().next('tr').find('.chck-bxs').slideToggle();
        $(this).find('.ud').toggleClass('close-acc');
    });
    
    $('#op-cl-all').click(function() {
        $('.chck-bxs').slideToggle();
    });
    
    $buttl.click(function(e) {
        var $inpcont = $('.ninp-cont'),
            $inpcont_length = $inpcont.length;
            e.preventDefault();
            if($inpcont_length > 1) {
                $inpcont.last().remove();
            } else {
                alert('Sie koennen die Eingabefelder nicht loeschen.<br />Eine Eingabemaske für "NEU" bleibt bestehen.');
            }
    });
    //EOC copy and remove input fields
});
</script>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
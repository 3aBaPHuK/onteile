<?php

require_once(__DIR__.'/guayaquillib/render/template.php');

abstract class LanguageTemplate implements IGuayaquilExtender {
    public static $language_data = array(
        'catalog title' => 'Katalog Titel',
        'catalog date' => 'Katalog Datum',
        'searchbyvin' => 'Suche Fahrzeug mittels Fahrzeug-Identifizierungsnummer (VIN)', //new parameter, noRiddle
        'vehicsearchbyvin' => 'Suche anderes Fahrzeug mittels Fahrzeug-Identifizierungsnummer (VIN)', //new parameter, noRiddle
        'searchbycustom' => 'Suche Fahrzeug mittels',
        'inputvin' => 'VIN eingeben (17 Zeichen), z.B.: %s',
        'search' => 'Suchen',
        'search_placeholder' => 'VIN eingeben', //new parameter, noRiddle
        'searchbyframe' => 'Suche Fahrzeug mittels Rahmen',
        'inputframe' => 'Code und Nummer des Rahmens eingeben, z.B. %s:',
        'searchdetail' => 'Suche nach Details',
        'searchbyoem' => 'Suche durch Teilenummer:',
        'search by wizard' => 'Identifikation des Fahrzeuges mittels Parametern',
        'search by wizard sub' => '(Es reicht aus wenn Sie die Ihnen bekannten Daten auswählen.)',
        'findfailed' => 'Die Suche mittels VIN %s war leider nicht erfolgreich.<br />Bitte versuchen Sie es <a href="%s">mittels "Identifikation des Fahrzeuges mittels Parametern" (insofern vorhanden)</a>.<br />Bitte auch darauf achten, daß Sie in der richtigen Marke sind (bei Fiat gibt es z.B. auch "Fiat Professional" für Nutzfahrzeuge).<br />Wenn Sie nicht weiterkommen schreiben Sie uns an (Kontakt-Button oben rechts).',
        'cars' => 'Fahrzeuge',
        'columnvehiclename' => 'Name',
        'columnvehiclemodification' => 'Modifikation',
        'columnvehiclegrade' => 'Grade',
        'columnvehicleframe' => 'Rahmen',
        'columnvehicledate' => 'Datum',
        'columnvehicleengine' => 'Maschine',
        'columnvehicleengineno' => 'Motor Nummer',
        'columnvehicletransmission' => 'Trans.',
        'columnvehicledoors' => 'Türen',
        'columnvehiclemanufactured' => 'Gebaut',
        'columnvehicleoptions' => 'Optionen',
        'columnvehiclecreationregion' => 'Region',
        'columnvehicleframes' => 'Rahmen',
        'columnvehicleframecolor' => 'Rahmen-Farbe',
        'columnvehicletrimcolor' => 'Trim color',
        'columnvehiclemodel' => 'Modell',
        'columnvehicledatefrom' => 'Gebaut von',
        'columnvehicledateto' => 'Gebaut bis',
        'columnvehicledestinationregion' => 'Für Region',
        'columnvehiclebrand' => 'Marke',
        'categories' => 'Kategorien',
        //'carname' => 'Fahrzeug: %s',
        'carname' => '%s', //noRiddle
        'selecteddetail' => 'Ausgewähltes Teil %s',
        'unitname' => 'Einheit: %s',
        'columndetailcodeonimage' => 'Nr. in Bild',
        'columndetailchkb' => 'Anhaken', //new element for checkbox, noRiddle
        'columndetailqty' => 'Menge', //new element for qty, noRiddle
        'columndetailname' => 'Name',
        'columndetailoem' => 'Teilenummer',
        'columndetailamount' => 'Summe',
        'columndetailnote' => 'Bemerkung',
        'columndetailprice' => 'Preis',
        'addtocarthint' => 'Teil bestellen',
        'price_question_txt' => 'Preisanfrage stellen', //new element for price question text, noRiddle
        'togglereplacements' => 'Zeige/verstecke Ersatzteile',
        'replacementway' => 'Ersatzteil-Typ',
        'replacementwayfull' => 'Volles Duplikat',
        'replacementwayforward' => 'Ersatz mit gezeigtem Duplikat ist möglich aber umgekehrter Ersatz ist nicht garantiert.',
        'replacementwaybackward' => 'Ersatz ist NICHT möglich',
        'wheretobuy' => 'Wo kaufen',
        'searchbyoemresult' => 'Suche mittels OEM %s',
        'vehiclelink' => 'Wechsel zur Suche nach Bildern',
        'groupdetails' => 'Suche nach Haupt- und Untergruppen',
        'imagedetails' => 'Suche nach Kategorien und Bildern', //new, noRiddle
        'quickgroupslink' => 'Wechsel zu Suche nach Gruppen',
        'list vehicles' => 'Fahrzeuge anzeigen',
        'unit_legend' => 'Legende',
        'unit_legend_mouse_wheel' => 'Mausrad',
        'unit_legend_image_resizing' => 'mit Mausrad Bildgröße anpassen',
        'unit_legend_show_replacement_parts' => 'Zeige Ersatzteil Duplikate', //'показ дубликатов запчастей',
        'unit_legend_mouse_image_drag' => 'Bild ziehen mit Maus', //'Mouse image drag',
        'unit_legend_mouse_scroll_image' => 'mit linker Maustaste Bild bewegen (scrollen)',
        'unit_legend_add_to_cart' => 'Teil bestellen',
        'unit_legend_hover_parts' => 'Halte Maus über das Teil', //'Hover mouse on the part',
        'unit_legend_highlight_parts' => 'Teile auf Bild oder in Tabelle markieren<br />(wird in Bild und Tabelle sichtbar)',
        'unit_legend_show_hind' => 'Zeige detallierte Information über Teil',
        'otherunitparts' => 'Alle Teile dieser Gruppe anzeigen',
        'enter_group_name' => 'Gruppen-Namen- oder Teile-Suchwort eingeben',
        'reset_group_name' => 'Zurücksetzen',
        'applicabilitybrandselectortitle' => 'Wähle Option um Anwendbarkeit zu sehen',
        'applicability' => '(Zeige Anwendbarkeit)',
        //BOC new for translation in /guayaquillib/render/qdetails/default.php, noRiddle
        'nothing_found_see_catalogue' => 'Leider nichts gefunden, machen Sie bitte Gebrauch vom <a href="%s">illustrierten Katalog</a> für eine Detailsuche',
        //EOC new for translation in /guayaquillib/render/qdetails/default.php, noRiddle
        //BOC new lang params, noRiddle
        'nr_brand_choice' => 'Markenauswahl',
        'nr_shopp_cart' => 'Zum Warenkorb',
        'nr_into_shopp_cart' => '',
        'nr_into_shopp_cart_t' => '',
        'nr_not_available' => 'nicht lieferbar',
        'nr_prod_replaced' => 'wurde ersetzt',
        'filter_choose_hint' => 'Bitte wählen Sie aus der Liste das Zutreffende:',
        'filter_choose_please' => 'bitte wählen',
        'filter_ignore_selection' => 'Filteroptionen ignorieren',
        'filter_submit' => 'absenden',
        'nr_searchparts' => 'Teile für das angezeigte Fahrzeug suchen',
        'products_in_cart_after_login' => 'Bitte überprüfen Sie Ihren Warenkorb. Dieser enthält noch Artikel von einem früheren Besuch.',
        'explain_group_search' => 'Suchen Sie mittels Suchwort oder klicken Sie die Gruppen an um Ihre Suche zu spezifizieren.',
        /*'txt_not_looged_in' => 'Liebe Kundin, lieber Kunde, im Rahmen unseres Vertrages mit dem Lieferanten der Ersatzteil-Katalog-Software haben wir uns verpflichtet, diese nur unseren (potentiellen) Kunden und nicht der öffentlichen Nutzung zur Verfügung zu stellen.<ul><li>Wir bitten Sie daher, sich zu registrieren. Wenn Sie bereits ein Konto in einem unserer Ersatzteile-Shops haben, können Sie sich mit diesem auch hier im Katalog anmelden.</li><li>Mit dieser Registrierung können Sie in allen Shops unter www.online-teile.com einkaufen. Selbstverständlich geben wir Ihre Daten nicht weiter, siehe auch unsere <a class="iframe" href="%s">Datenschutzerklärung</a>.</li><li>Falls Sie eine <strong>Werkstatt</strong> betreiben oder Mitglied eines <strong>anerkannten Markenclubs</strong> sind oder innerhalb der EU-Zollunion eine <strong>mehrwertsteuerfreie Rechnung</strong> benötigen, finden Sie <a class="iframe" href="%s"><strong>hier</strong></a> weitere Informationen.</li><li>Falls Sie <strong>ADAC-Mitglied</strong> sind, erhalten Sie die unter <a class="iframe" title="ADAC-Information" href="https://www.online-teile.com/ADAC/" target="_blank" rel="nofollow">https://www.online-teile.com/ADAC/</a> ausgelobten Rabatte. Die Mitgliedsnummer wird benötigt, bevor Sie Ihre Bestellung absenden.</li></ul>',*/
        /*'txt_not_looged_in' => 'Liebe Kundin, lieber Kunde, wir freuen uns dass Sie hier sind und unseren kostenlosen Ersatzteilkatalog für mehr als 40 Marken nutzen wollen ! Wir würden uns freuen, wenn Sie die herausgesuchten Teile dann im angehängten Markenshop kaufen würden. Wir werden Sie schnell und zu guten Preisen beliefern ! Und natürlich vertreiben wir ausschliesslich Originalteile in höchster Qualität.
<ul>
<li>Bitte <a href="//www.online-teile.com/oem-kataloge/login.php" class="link-blue">registrieren</a> Sie sich. Wenn Sie bereits ein <a href="//www.online-teile.com/oem-kataloge/login.php" class="link-blue">Konto</a> in einem unserer Ersatzteile-Shops haben, können Sie sich mit diesem auch hier im Katalog <a href="//www.online-teile.com/oem-kataloge/login.php" class="link-blue">anmelden</a>.</li><li>Mit dieser Registrierung können Sie in allen Shops unter <a href="//www.online-teile.com" target="_blank" class="link-blue">www.online-teile.com</a> einkaufen. Selbstverständlich behandeln wir Ihre Daten vertraulich, siehe auch unsere <a class="iframe link-blue" href="%s">Datenschutzerklärung</a>.</li><li>Falls Sie eine <strong>Werkstatt</strong> betreiben oder Mitglied eines <strong>anerkannten Markenclubs</strong> sind oder innerhalb der EU-Zollunion eine <strong>mehrwertsteuerfreie Rechnung</strong> benötigen, finden Sie <a class="iframe link-blue" href="%s">hier</a> weitere Informationen.</li><li>Falls Sie <strong>ADAC-Mitglied</strong> sind, erhalten Sie die unter <a class="iframe link-blue" title="ADAC-Information" href="https://www.online-teile.com/ADAC/" target="_blank" rel="nofollow">https://www.online-teile.com/ADAC/</a> ausgelobten Rabatte. Die Mitgliedsnummer wird benötigt, bevor Sie Ihre Bestellung absenden.</li></ul>',*/
        'txt_see_repl_part' => 'siehe nächste Position',
        'name_not_known' => 'Bezeichnung nicht bekannt',
        'back_to_group_link' => 'Zurück zu der Gruppenauswahl'
        //EOC new lang params, noRiddle
    );
}

//fill array values where php is used, noRiddle
LanguageTemplate::$language_data['nr_into_shopp_cart'] = '<span class="no-mob">Angehakte Teile '.(isset($_GET['coID']) && $_GET['coID'] == '1007' ? 'dieser Gruppe' : '').' in den Warenkorb legen</span><span class="y-mob"><i class="fa fa-check-square"></i></span>';
LanguageTemplate::$language_data['nr_into_shopp_cart_t'] = 'Angehakte Teile '.(isset($_GET['coID']) && $_GET['coID'] == '1007' ? 'dieser Gruppe' : '').' in den Warenkorb legen';
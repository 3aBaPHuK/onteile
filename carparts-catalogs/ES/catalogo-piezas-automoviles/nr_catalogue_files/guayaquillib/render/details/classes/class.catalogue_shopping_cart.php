<?
/*********************************************************
* file: class.catalogue_shopping_cart.php
* use: shopping cart actions for catalogue
* (c) noRiddle 10-2016
*********************************************************/

require_once (DIR_FS_INC.'xtc_get_tax_description.inc.php');

class catalogue_shopping_cart {

    function __construct() {
        $this->reset();
    }
    
    function reset($reset_database = false) {
        $this->contents = array ();
        $this->total = array();
        $this->tax = array();

        if (isset($_SESSION['customer_id']) && ($reset_database == true)) {
            xtc_db_query("DELETE FROM ".TABLE_CUSTOMERS_BASKET." where customers_id = '".(int)$_SESSION['customer_id']."'");
            xtc_db_query("DELETE FROM ".TABLE_CUSTOMERS_BASKET_ATTRIBUTES." WHERE customers_id = '".(int)$_SESSION['customer_id']."'");
        }
    }

    function restore_contents() {
        if (!isset ($_SESSION['customer_id'])) {
            return false;
        }
        
        //insert current cart contents in database
        if (is_array($this->contents)) { // && !empty($this->contents)
            //reset($this->contents);
            foreach($this->contents as $brand) {
                reset($brand);
                while(list($products_model,) = each($brand)) {
                    $pid = $brand[$products_model]['products_id'];
                    $pname = $brand[$products_model]['products_name'];
                    $pqty = $brand[$products_model]['quantity'];
                    $pprice = $brand[$products_model]['products_price'];
                    $pptxcid = $brand[$products_model]['products_tax_class_id'];
                    
                    $product_query = xtc_db_query("SELECT products_model
                                                     FROM catalogue_cart
                                                    WHERE customers_id = '".(int)$_SESSION['customer_id']."'
                                                      AND products_model = '".xtc_db_input($products_model)."'"
                                                 );

                    if (!xtc_db_num_rows($product_query)) {
                        $sql_data_array = array('customers_id' => $_SESSION['customer_id'],
                                                'brand' => $brand[$products_model]['products_brand'],
                                                'lax_brand' => $brand[$products_model]['lax_brand'],
                                                'products_model' => $products_model,
                                                'products_id' => (int)$pid,
                                                'products_name' => xtc_db_input($pname),
                                                'quantity' => (int)$pqty,
                                                'products_price' => $pprice,
                                                'products_tax_class_id' => (int)$pptxcid
                                               );
                                         
                        xtc_db_perform('catalogue_cart', $sql_data_array);
                    } else {
                        xtc_db_query("UPDATE catalogue_cart
                                        SET quantity = '".xtc_db_input($pqty)."'
                                      WHERE customers_id = '".(int)$_SESSION['customer_id']."'
                                        AND products_model = '".xtc_db_input($products_model)."'");
                    }
                }
            }
        }

        //reset per-session cart contents, but not the database contents
        $this->reset(false);
        $products_query = xtc_db_query("SELECT *
                                          FROM catalogue_cart
                                         WHERE customers_id = '".(int)$_SESSION['customer_id']."'
                                      ORDER BY catalogue_cart_id"
                                      );

        while($products = xtc_db_fetch_array($products_query)) {
            $this->contents[$products['brand']][$products['products_model']] = array('products_brand' => $products['brand'],
                                                                                     'lax_brand' => $products['lax_brand'],
                                                                                     'products_model' => $products['products_model'],
                                                                                     'products_id' => (int)$products['products_id'],
                                                                                     'products_name' => $products['products_name'],
                                                                                     'quantity' => (int)$products['quantity'],
                                                                                     'products_price' => $products['products_price'],
                                                                                     'products_tax_class_id' => (int)$products['products_tax_class_id']
                                                                                     );
          
        }
        $this->cleanup();
    }
    
    function add_cart($products_model, $notify = true) {
        if ($notify == true) {
            //$_SESSION['new_products_id_in_cart_'.$_SESSION['shopsess']] = $products_model['products_model'];
            $_SESSION['new_prid_in_catcart_'.$_SESSION['shopsess']] = array();
            $_SESSION['new_prid_in_catcart_'.$_SESSION['shopsess']][$products_model['products_brand']] = array($products_model['products_id'] => $products_model['products_model']);
        }
        
        if ($this->in_cart($products_model['products_brand'], $products_model['products_model'])) {
            $this->update_quantity($products_model['products_brand'], $products_model['products_model'], (int)$products_model['quantity']);
        } else {
            $this->contents[$products_model['products_brand']][$products_model['products_model']] = array('products_brand' => $products_model['products_brand'],
                                                                                                          'lax_brand' => $products_model['lax_brand'],
                                                                                                          'products_model' => $products_model['products_model'],
                                                                                                          'products_id' => (int)$products_model['products_id'],
                                                                                                          'products_name' => $products_model['products_name'],
                                                                                                          'quantity' => (int)$products_model['quantity'],
                                                                                                          'products_price' => $products_model['products_price'],
                                                                                                          'products_tax_class_id' => (int)$products_model['products_tax_class_id']
                                                                                                         );
            
            // insert into database
            if (isset($_SESSION['customer_id'])){
                $sql_data_array = array('customers_id' => $_SESSION['customer_id'],
                                        'brand' => $products_model['products_brand'],
                                        'lax_brand' => $products_model['lax_brand'],
                                        'products_model' => $products_model['products_model'],
                                        'products_id' => (int)$products_model['products_id'],
                                        'products_name' => xtc_db_input($products_model['products_name']),
                                        'quantity' => (int)$products_model['quantity'],
                                        'products_price' => $products_model['products_price'],
                                        'products_tax_class_id' => (int)$products_model['products_tax_class_id'],
                                        'date_added' => date('Ymd')
                                       );
                
                xtc_db_perform('catalogue_cart', $sql_data_array);
            }
        }
        $this->cleanup();
    }
    
    function update_quantity($brand, $model, $qty) {
        if (empty($qty)){
            return true;
        }
        $this->contents[$brand][$model]['quantity'] = (int)$qty;
        
        // update database
        if (isset($_SESSION['customer_id'])){
            xtc_db_query("UPDATE catalogue_cart
                             SET quantity = '".(int)$qty."'
                           WHERE customers_id = '".(int)$_SESSION['customer_id']."'
                             AND products_model = '".xtc_db_input($model)."'");
        }
    }
    
    function cleanup() {
        //reset($this->contents);
        foreach($this->contents as $brand) {
            reset($brand);
            while(list($key,) = each($brand)) {
                if (isset($brand[$key]['quantity']) && $brand[$key]['quantity'] < 1) {
                    unset ($brand[$key]);
                    // remove from database
                    if (isset($_SESSION['customer_id'])) { // Hetfield - 2009-08-19 - removed deprecated function session_is_registered to be ready for PHP >= 5.3
                        xtc_db_query("DELETE from catalogue_cart WHERE customers_id = '".(int)$_SESSION['customer_id']."' AND products_model = '".xtc_db_input($key)."'");
                    }
                }
            }
        }
    }
    
    function count_all_contents() {
        if (is_array($this->contents) && !empty($this->contents)) {
            $total_items = count($this->contents);
        }
        return $total_items;
    }
    
    function count_contents($brand) {
        $total_items = 0;
        if (is_array($this->contents[$brand])) {
            reset($this->contents[$brand]);
            while(list($products_model,) = each($this->contents[$brand])) {
                $total_items += $this->get_quantity($brand, $products_model);
            }
        }
        return $total_items;
    }
    
    function get_quantity($brand, $products_model) {
        if (isset($this->contents[$brand][$products_model]['quantity'])) {
            return $this->contents[$brand][$products_model]['quantity'];
        } else {
            return 0;
        }
    }

    function in_cart($brand, $products_model) {
        if (isset($this->contents[$brand][$products_model])) {
            return true;
        } else {
            return false;
        }
    }
    
    function remove($brand, $products_model) {
        unset($this->contents[$brand][$products_model]);
        
        if(empty($this->contents[$brand])) {
            unset($this->contents[$brand]);
            unset($this->total[$brand]);
            unset($this->tax[$brand]);
            unset($this->tax_discount[$brand]);
        }

        //remove from database
        if (isset($_SESSION['customer_id'])) { // Hetfield - 2009-08-19 - removed deprecated function session_is_registered to be ready for PHP >= 5.3
            xtc_db_query("DELETE from catalogue_cart WHERE customers_id = '".(int)$_SESSION['customer_id']."' AND products_model = '".xtc_db_input($products_model)."'");
        }
    }
    
    function remove_all() {
        $this->reset();
    }
    
    function get_product_id_list() {
        $product_id_list = '';
        if (is_array($this->contents)) {
            //reset($this->contents);
            foreach($this->contents as $brand) {
                reset($brand);
                while(list($products_model,) = each($brand)) {
                    $products_model_list .= ', '.$products_model;
                }
            }
        }
        return substr($product_id_list, 2);
    }
    
    function calculate() {
        global $nr_foreign_xtcPrice;
        //$this->total = 0;
        $this->total = array();
        $this->tax = array ();
        $this->tax_discount = array (); //Tomcraft - 2011-02-01 - Paypal Express Modul
        if (!is_array($this->contents)) {
            return 0;
        }
        //reset($this->contents);
        foreach($this->contents as $brand => $models) {
            $this->total[$brand] = 0;
            $this->tax[$brand] = array();
            $this->tax_discount[$brand] = array();
            reset($models);
            while(list($products_model) = each($models)) {
                //echo '<br><br><br><pre>'.print_r($models[$products_model], true).'</pre>' . '<pre> $models[$products_model][\'products_price\'] = '.$models[$products_model]['products_price'].'</pre>';
                $qty = $models[$products_model]['quantity'];
                $products_tax_class_id = $models[$products_model]['products_tax_class_id'];
                $products_price = $nr_foreign_xtcPrice->xtcFormat($models[$products_model]['products_price'], false, $products_tax_class_id);

                //echo '<br><br><br><pre>$products_price = '.$products_price.'</pre>';
                //echo '<pre>'.print_r($_SESSION['nr_ctlg_brands'], true).'</pre>';
                //exit();

                //$this->total += $products_price * $qty;
                $this->total[$brand] += $products_price * $qty;

                if ($products_tax_class_id != 0) {
                    if ($_SESSION['customers_status']['customers_status_ot_discount_flag'] == 1) {
                        // Rabatt für die Steuerberechnung
                        // der eigentliche Rabatt wird im order-details_cart abgezogen
                        $products_price_tax = $products_price - ($products_price / 100 * $_SESSION['customers_status']['customers_status_ot_discount']);
                    }

                    $products_tax = $nr_foreign_xtcPrice->TAX[$products_tax_class_id];
                    $products_tax_description = xtc_get_tax_description($products_tax_class_id);
                    
                    //echo '<br><br><br><pre>$products_tax_description = '.$products_tax_description.' | $_SESSION[\'customer_country_id\'] = '.$_SESSION['customer_country_id'].' | $_SESSION[\'customer_zone_id\'] = '.$_SESSION['customer_zone_id'].'</pre>';
                    //exit();

                    // price incl tax
                    if ($_SESSION['customers_status']['customers_status_show_price_tax'] == '1') {
                        //if (!isset($this->tax[$products_tax_class_id])) $this->tax[$products_tax_class_id]['value'] = 0; //DokuMan - 2010-03-26 - set undefined variable
                        if (!isset($this->tax[$brand][$products_tax_class_id])) $this->tax[$brand][$products_tax_class_id]['value'] = 0; //DokuMan - 2010-03-26 - set undefined variable
                        if ($_SESSION['customers_status']['customers_status_ot_discount_flag'] == 1) {
                            //$this->tax[$products_tax_class_id]['value'] += ((($products_price_tax) / (100 + $products_tax)) * $products_tax)*$qty;
                            $this->tax[$brand][$products_tax_class_id]['value'] += ((($products_price_tax) / (100 + $products_tax)) * $products_tax)*$qty;
                        } else {
                            //$this->tax[$products_tax_class_id]['value'] += ((($products_price) / (100 + $products_tax)) * $products_tax)*$qty;
                            $this->tax[$brand][$products_tax_class_id]['value'] += ((($products_price) / (100 + $products_tax)) * $products_tax)*$qty;
                        }
                            //$this->tax[$products_tax_class_id]['desc'] = TAX_ADD_TAX.$products_tax_description;
                            $this->tax[$brand][$products_tax_class_id]['desc'] = TAX_ADD_TAX.$products_tax_description;
                    }
                    // excl tax + tax at checkout
                    if ($_SESSION['customers_status']['customers_status_show_price_tax'] == 0 && $_SESSION['customers_status']['customers_status_add_tax_ot'] == 1) {
                        //if (!isset($this->tax[$products_tax_class_id])) $this->tax[$products_tax_class_id]['value'] = 0; //Web28 - 2012-05-08 - set undefined variable
                        if (!isset($this->tax[$brand][$products_tax_class_id])) $this->tax[$brand][$products_tax_class_id]['value'] = 0; //Web28 - 2012-05-08 - set undefined variable
                            if ($_SESSION['customers_status']['customers_status_ot_discount_flag'] == 1) {
                                //$this->tax[$products_tax_class_id]['value'] += (($products_price_tax) / 100) * ($products_tax)*$qty;
                                $this->tax[$brand][$products_tax_class_id]['value'] += (($products_price_tax) / 100) * ($products_tax)*$qty;
                                //if (!isset($this->tax_discount[$products_tax_class_id])) $this->tax_discount[$products_tax_class_id] = 0;
                                if (!isset($this->tax_discount[$brand][$products_tax_class_id])) $this->tax_discount[$brand][$products_tax_class_id] = 0;
                                //$this->tax_discount[$products_tax_class_id]+=(($products_price_tax) / 100) * ($products_tax)*$qty; // Tomcraft - 2011-02-01 - Paypal Express Modul fuer Einzelrundung
                                $this->tax_discount[$brand][$products_tax_class_id] += (($products_price_tax) / 100) * ($products_tax) * $qty;
                            } else {
                                //$this->tax[$products_tax_class_id]['value'] += (($products_price) / 100) * ($products_tax)*$qty;
                                $this->tax[$brand][$products_tax_class_id]['value'] += (($products_price) / 100) * ($products_tax)*$qty;
                                //$this->total+= (($products_price) / 100) * ($products_tax)*$qty;
                                $this->total[$brand] += (($products_price) / 100) * ($products_tax)*$qty;
                            }
                            //$this->tax[$products_tax_class_id]['desc'] = TAX_NO_TAX.$products_tax_description;
                            $this->tax[$brand][$products_tax_class_id]['desc'] = TAX_NO_TAX.$products_tax_description;
                    }
                }
            }
            // BOF - Tomcraft - 2011-02-01 - Paypal Express Modul // Aufaddieren und Runden der Gesamtsumme je Steuersatz
            foreach ($this->tax_discount[$brand] as $value) {
                //$this->total+=round($value, $nr_foreign_xtcPrice->get_decimal_places(''));
                $this->total[$brand] += round($value, $nr_foreign_xtcPrice->get_decimal_places(''));
            }
            // EOF - Tomcraft - 2011-02-01 - Paypal Express Modul  // Aufaddieren und Runden der Gesamtsumme je Steuersatz
            //echo 'TT'.$this->total;
        }
    }
    
    function get_products() {
        //global $nr_foreign_xtcPrice, $main;
        global $xtPrice;
        if (!is_array($this->contents)){
            return false;
        }

        ksort($this->contents);
        //$products_array = array ();
        //reset($this->contents);
        foreach($this->contents as $brand => $models) {
            $products_array[$brand] = array ();
            reset($models);
            //while(list($products_model,) = each($models)) {
            foreach($models as $products_model => $prod_arr) {
                if($models[$products_model]['quantity'] != 0 || $models[$products_model]['quantity'] != '') {
                    //$products_price = $nr_foreign_xtcPrice->xtcFormat($models[$products_model]['products_price'], true, $models[$products_model]['products_tax_class_id'], true);
                    //$products_price = $models[$products_model]['products_price'];
                    $prod_tax = isset($xtPrice->TAX[$models[$products_model]['products_tax_class_id']]) ? $xtPrice->TAX[$models[$products_model]['products_tax_class_id']] : 0;
                    $products_price = $xtPrice->xtcAddTax($models[$products_model]['products_price'], $prod_tax);

                    $products_array[$brand][] = array('brand' => $models[$products_model]['products_brand'],
                                                      'lax_brand' => $models[$products_model]['lax_brand'],
                                                      'id' => $models[$products_model]['products_id'],
                                                      'name' => $models[$products_model]['products_name'],
                                                      'model' => $models[$products_model]['products_model'],
                                                      'price' => $products_price,
                                                      'quantity' => $models[$products_model]['quantity'],
                                                      //'final_price' => $products_price,
                                                      'final_price' => $products_price * $models[$products_model]['quantity'],
                                                      'tax_class_id' => $models[$products_model]['products_tax_class_id'],
                                                      //'tax' => isset($nr_foreign_xtcPrice->TAX[$models[$products_model]['products_tax_class_id']]) ? $nr_foreign_xtcPrice->TAX[$models[$products_model]['products_tax_class_id']] : 0
                                                      'tax' => $prod_tax
                                                     );

                }
            }
        }
        return $products_array;
    }
    
    function show_total($brand) {
        $this->calculate();
        return $this->total[$brand];
    }
    
    function show_tax($brand, $format = true) {
        global $nr_foreign_xtcPrice;
        $this->calculate();
        $output = "";
        $val=0;
        $gval=0; // Paypal Express Modul
        foreach ($this->tax[$brand] as $key => $value) {
            if ($this->tax[$brand][$key]['value'] > 0 ) {
                $output .= $this->tax[$brand][$key]['desc'].": ".$nr_foreign_xtcPrice->xtcFormat($this->tax[$brand][$key]['value'], true)."<br />";
                $val = $this->tax[$brand][$key]['value'];
                $gval += $this->tax[$brand][$key]['value']; // Paypal Express Modul
            }
        }
        if ($format) {
            return $output;
        } else {
            //return $val; // Paypal Express Modul
            return $gval; // Paypal Express Modul
        }
    }
}
?>
<?php
/************************************
* file: serachleft.php
* use: cutom search box
* (c) noRiddle
************************************/

// include smarty
include(DIR_FS_BOXES_INC . 'smarty_default.php');

$cache_id = md5('new_search'.$_SESSION['language']);

if (!$box_smarty->is_cached(CURRENT_TEMPLATE.'/boxes/box_search_left.html', $cache_id) || !$cache) {
  $filename = FILENAME_NR_ADVANCED_SEARCH_RESULT; //use own search file, noRiddle
  if (defined('MODULE_FINDOLOGIC_STATUS') && MODULE_FINDOLOGIC_STATUS == 'True') {
    $filename = FILENAME_FINDOLOGIC;
  }
  $box_smarty->assign('FORM_ACTION', xtc_draw_form('quick_find', xtc_href_link($filename, '', $request_type, false), 'get') . xtc_hide_session_id());
  $box_smarty->assign('INPUT_SEARCH', xtc_draw_input_field('keywords', AH_IMAGE_BUTTON_SEARCH, 'id="inputString" placeholder="'.AH_IMAGE_BUTTON_SEARCH.'" autocomplete="off" '.((SEARCH_AC_STATUS == 'true') ? 'onkeyup="ac_lookup(this.value);" ' : '').'onfocus="if(this.value==this.defaultValue) this.value=\'\';" onblur="if(this.value==\'\') this.value=this.defaultValue;"'));
  //$box_smarty->assign('BUTTON_SUBMIT', xtc_image_submit('button_quick_find.gif', IMAGE_BUTTON_SEARCH, 'class="search_button"'));
  $box_smarty->assign('BUTTON_SUBMIT', xtc_image_submit('button_quick_find.gif', IMAGE_BUTTON_SEARCH)); //no need for double class attribute, noRiddle
  $box_smarty->assign('FORM_END', '</form>');
  $box_smarty->assign('LINK_ADVANCED', xtc_href_link(FILENAME_ADVANCED_SEARCH));

  //BOC generate link where to find part no. depending on group permission, noRiddle
  if (GROUP_CHECK == 'true') {
    $coid = '';
    $cont_permission_query1 = xtc_db_query("SELECT group_ids FROM ".TABLE_CONTENT_MANAGER." WHERE content_group = 7655 AND languages_id = '".(int)$_SESSION['languages_id']."'");
    $cont_permission_query2 = xtc_db_query("SELECT group_ids FROM ".TABLE_CONTENT_MANAGER." WHERE content_group = 7656 AND languages_id = '".(int)$_SESSION['languages_id']."'");
    $cont_permission1 = xtc_db_fetch_array($cont_permission_query1);
    $cont_permission2 = xtc_db_fetch_array($cont_permission_query2);
    $cont_perm1_arr = explode(',', rtrim($cont_permission1['group_ids'], ','));
    $cont_perm2_arr = explode(',', rtrim($cont_permission2['group_ids'], ','));
    $cont_perm1_arr = array_map('trim', $cont_perm1_arr);
    $cont_perm2_arr = array_map('trim', $cont_perm2_arr);

    $group = 'c_'.$_SESSION['customers_status']['customers_status_id'].'_group';
    if(in_array($group, $cont_perm1_arr)) {
        $coid = '7655';
    } elseif(in_array($group, $cont_perm2_arr)) {
        $coid = '7656';
    } else {
        $coid = '7655';
    }
  } else {
    $coid = '7656';
  }
  $box_content = xtc_href_link(FILENAME_POPUP_CONTENT, 'coID='.$coid, $request_type);
  //EOC generate link where to find part no. depending on group permission, noRiddle

  $box_smarty->assign('BOX_CONTENT', $box_content);
}

//BOC catalogue for BMW group, noRiddle
if(in_array($shop, $bmw_group_shops_ctlg)) { //array $bmw_group_shops_ctlg defined in /includes/extra/application_top/application_top_begin/define_was_prices.php, noRiddle
    $has_bmw_cat = true;
    $box_smarty->assign('HAS_BMW_CAT', $has_bmw_cat);

    include DIR_FS_CATALOG.'mm_mod/lang/'.$_SESSION['language'].'.php';
    
    $mm_car_id = '<input name="car_id" value="'.txt_find_car_no.'" placeholder="'.txt_find_car_no.'" id="car_id" onfocus="if(this.value == this.defaultValue)this.value = \'\';" onblur="if(this.value == \'\')this.value = this.defaultValue;" type="text">';
    $box_smarty->assign('MM_CAR_ID', $mm_car_id);
    $box_smarty->assign('BUTTON_SUBMIT_CI', xtc_image_submit('button_quick_find.gif', IMAGE_BUTTON_SEARCH, ''));

    $mm_select_vehicle = drop_down(mm_select_vehicle, 'baureihe', get_models($db_katalog), (isset($_GET['baureihe']) ? $_GET['baureihe'] : ''), '');
    $box_smarty->assign('MM_SELECT_VEHICLE', $mm_select_vehicle);

    //include DIR_FS_CATALOG.'mm_mod/pulldown_cars_footer.php';
}
//EOC catalogue for BMW group, noRiddle

if (!$cache) {
    $box_searchleft = $box_smarty->fetch(CURRENT_TEMPLATE . '/boxes/box_search_left.html');
} else {
    $box_searchleft = $box_smarty->fetch(CURRENT_TEMPLATE . '/boxes/box_search_left.html', $cache_id);
}

$smarty->assign('box_SEARCHLEFT', $box_searchleft);
?>

<?php
/* --------------------------------------------------------------
   $Id: french.php 10896 2017-08-11 11:31:54Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(french.php,v 1.99 2003/05/28); www.oscommerce.com
   (c) 2003 nextcommerce (french.php,v 1.24 2003/08/24); www.nextcommerce.org
   (c) 2006 XT-Commerce (french.php)

   Released under the GNU General Public License
   --------------------------------------------------------------
   Third Party contributions:
   Customers Status v3.x (c) 2002-2003 Copyright Elari elari@free.fr | www.unlockgsm.com/dload-osc/ | CVS : http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/elari/?sortby=date#dirlist

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

// look in your $PATH_LOCALE/locale directory for available locales..
// on RedHat6.0 I used 'fr_FR'
// on FreeBSD 4.0 I use 'fr_FR.ISO_8859-1'
// this may not work under win32 environments..

@setlocale(LC_TIME, 'fr_FR.UTF-8' ,'fr_FR@euro', 'fr_FR', 'fr-FR', 'fr', 'fr', 'fr_FR.ISO_8859-1', 'French','fr_FR.ISO_8859-15');
define('DATE_FORMAT_SHORT', '%d.%m.%Y');
define('DATE_FORMAT_LONG', '%A, %d. %B %Y');
define('DATE_FORMAT', 'd.m.Y');
define('PHP_DATE_TIME_FORMAT', 'd.m.Y H:i:s');
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');

// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
function xtc_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 0, 2) . substr($date, 3, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 3, 2) . substr($date, 0, 2);
  }
}

require_once(DIR_FS_INC.'auto_include.inc.php');
foreach(auto_include(DIR_FS_LANGUAGES.'french/extra/admin/','php') as $file) require ($file);

// Global entries for the <html> tag
define('HTML_PARAMS', 'dir="ltr" lang="de"');

// page title
define('TITLE', defined('PROJECT_VERSION') ? PROJECT_VERSION : 'undefined');

// header text in includes/header.php
define('HEADER_TITLE_TOP', 'Administration');
define('HEADER_TITLE_SUPPORT_SITE', 'Page du support');
define('HEADER_TITLE_ONLINE_CATALOG', 'Catalogue en ligne');
define('HEADER_TITLE_ADMINISTRATION', 'Administration');

// text for gender
define('MALE', 'Monsieur');
define('FEMALE', 'Madame');

// text for date of birth example
define('DOB_FORMAT_STRING', 'jj.mm.aaaa');

// configuration box text in includes/boxes/configuration.php

define('BOX_HEADING_CONFIGURATION', 'Configuration');
define('BOX_HEADING_MODULES', 'Modules');
define('BOX_HEADING_PARTNER_MODULES', 'Modules d\'affiliation');
define('BOX_HEADING_ZONE', 'Pays/Impôt');
define('BOX_HEADING_CUSTOMERS', 'Clients');
define('BOX_HEADING_PRODUCTS', 'Catalogue');
define('BOX_HEADING_STATISTICS', 'Statistiques');
define('BOX_HEADING_TOOLS', 'Programmes d\'aides');
define('BOX_HEADING_LOCALIZATION', 'Langues/Monnaies');
define('BOX_HEADING_TEMPLATES', 'Modèles');
define('BOX_HEADING_LOCATION_AND_TAXES', 'Pays/Impôt');
define('BOX_HEADING_CATALOG', 'Catalogue');
define('BOX_MODULE_NEWSLETTER', 'Newsletter');

define('BOX_CONTENT', 'Gestionnaire du contenu');
define('TEXT_ALLOWED', 'Autorisation');
define('TEXT_ACCESS', 'Espace d\'accès');
define('BOX_CONFIGURATION', 'Réglages de base');
define('BOX_CONFIGURATION_1', 'Ma boutique');
define('BOX_CONFIGURATION_2', 'Valeurs minimales');
define('BOX_CONFIGURATION_3', 'Valeurs maximales');
define('BOX_CONFIGURATION_4', 'Options d\'image');
define('BOX_CONFIGURATION_5', 'Détails client');
define('BOX_CONFIGURATION_6', 'Options module');
define('BOX_CONFIGURATION_7', 'Option expédition');
define('BOX_CONFIGURATION_8', 'Options des listes d\'articles');
define('BOX_CONFIGURATION_9', 'Options du gestionnaire du stock');
define('BOX_CONFIGURATION_10', 'Options logging');
define('BOX_CONFIGURATION_11', 'Options mémoire cache');
define('BOX_CONFIGURATION_12', 'Options email');
define('BOX_CONFIGURATION_13', 'Options téléchargement');
define('BOX_CONFIGURATION_14', 'Compression');
define('BOX_CONFIGURATION_15', 'Sessions');
define('BOX_CONFIGURATION_16', 'Meta-Tags/Moteurs de recherche');
define('BOX_CONFIGURATION_17', 'Modules supplémentaires');
define('BOX_CONFIGURATION_18', 'ID TVA');
define('BOX_CONFIGURATION_19', 'Partenaire');
define('BOX_CONFIGURATION_22', 'Options de recherche');
define('BOX_CONFIGURATION_24', 'Google, Piwik & Facebook');
define('BOX_CONFIGURATION_25', 'Captcha');
define('BOX_CONFIGURATION_31', 'Skrill');
define('BOX_CONFIGURATION_40', 'Options des fenêtres popup');
define('BOX_CONFIGURATION_1000', 'Options de l\'espace de l\'administrateur');

define('BOX_MODULES', 'Modules de paiement/ expédition/ facturation');
define('BOX_PAYMENT', 'Options de paiement');
define('BOX_SHIPPING', 'Mode d\'expédition');
define('BOX_ORDER_TOTAL', 'Résumé');
define('BOX_CATEGORIES', 'Catégories/Articles');
define('BOX_PRODUCTS_ATTRIBUTES', 'Caractéristiques de l\'article');
define('BOX_MANUFACTURERS', 'Fabricant');
define('BOX_REVIEWS', 'Évaluations du produit');
define('BOX_CAMPAIGNS', 'Campagnes');
define('BOX_XSELL_PRODUCTS', 'Marketing croisé');
define('BOX_SPECIALS', 'Offres spéciales');
define('BOX_PRODUCTS_EXPECTED', 'Articles attendus');
define('BOX_CUSTOMERS', 'Clients');
define('BOX_ACCOUNTING', 'Droits d\'administrateur');
define('BOX_CUSTOMERS_STATUS', 'Groupes de clients');
define('BOX_ORDERS', 'Commandes');
define('BOX_COUNTRIES', 'Pays');
define('BOX_ZONES', 'Régions');
define('BOX_GEO_ZONES', 'Zones d\'impôt');
define('BOX_TAX_CLASSES', 'Catégories fiscales');
define('BOX_TAX_RATES', 'Taux d\'imposition');
define('BOX_HEADING_REPORTS', 'Rapports');
define('BOX_PRODUCTS_VIEWED', 'Articles visités');
define('BOX_STOCK_WARNING', 'Rapport stock');
define('BOX_PRODUCTS_PURCHASED', 'Articles vendus');
define('BOX_STATS_CUSTOMERS', 'Statistique de commande');
define('BOX_BACKUP', 'Gestionnaire banque de données');
define('BOX_BANNER_MANAGER', 'Gestionnaire de bannière');
define('BOX_CACHE', 'Contrôle de la mémoire cache');
define('BOX_DEFINE_LANGUAGE', 'Définir la langue');
define('BOX_FILE_MANAGER', 'Gestionnaire des données');
define('BOX_MAIL', 'Envoyer email');
define('BOX_NEWSLETTERS', 'Gestionnaire des newsletter');
define('BOX_SERVER_INFO', 'Info serveur');
define('BOX_BLZ_UPDATE', 'Actualiser les codes bancaires');
define('BOX_WHOS_ONLINE', 'Qui est en ligne');
define('BOX_TPL_BOXES', 'Ordre boîte');
define('BOX_CURRENCIES', 'Monnaie');
define('BOX_LANGUAGES', 'Langues');
define('BOX_ORDERS_STATUS', 'Statut de commande');
define('BOX_ATTRIBUTES_MANAGER', 'Gestionnaire des attributs');
define('BOX_SHIPPING_STATUS', 'Statut de livraison');
define('BOX_SALES_REPORT', 'Statistique des ventes');
define('BOX_MODULE_EXPORT', 'Modules d\'exports');
define('BOX_MODULE_SYSTEM', 'Modules de système');
define('BOX_HEADING_GV_ADMIN', 'Bons/Coupons');
define('BOX_GV_ADMIN_QUEUE', 'Coupon file d\'attente');
define('BOX_GV_ADMIN_MAIL', 'Coupon email');
define('BOX_GV_ADMIN_SENT', 'Coupons envoyés');
define('BOX_COUPON_ADMIN', 'Coupon admin');
define('BOX_TOOLS_BLACKLIST', 'Liste noire des cartes crédit');
define('BOX_IMPORT', 'Import/Export');
define('BOX_PRODUCTS_VPE', 'Unité de conditionnement');
define('BOX_CAMPAIGNS_REPORT', 'Rapport des campagnes');
define('BOX_ORDERS_XSELL_GROUP', 'Groupes du marketing croisé');
define('BOX_REMOVEOLDPICS', 'Supprimer des anciennes images');
define('BOX_JANOLAW', 'Hosting janolaw AGB');
define('BOX_HAENDLERBUND', 'Händlerbund AGB Service');
define('BOX_SAFETERMS', 'Safeterms - AGB Service');
define('BOX_SHOP', 'Boutique');
define('BOX_LOGOUT', 'Déconnecter');
define('BOX_CREDITS', 'Crédits');
define('BOX_UPDATE', 'Check version');
define('BOX_EASYMARKETING', 'EASYMARKETING AG');
define('BOX_GV_CUSTOMERS', 'Crédit client');
define('BOX_IT_RECHT_KANZLEI', 'IT Recht Kanzlei');
define('BOX_PROTECTEDSHOPS', 'Protected Shops - AGB Service');
define('BOX_CLEVERREACH', 'CleverReach');
define('BOX_SUPERMAILER', 'SuperMailer');
define('BOX_OFFLINE', 'Boutique hors ligne');
define('BOX_LOGS', 'Logfiles');
define('BOX_SHIPCLOUD', 'shipcloud');
define('BOX_SHIPCLOUD_PICKUP', 'shipcloud - récupération');
define('BOX_PRODUCTS_TAGS', 'Caractéristiques de l\'article');
define('BOX_TRUSTEDSHOPS', 'Trusted Shops');

define('TXT_GROUPS', '<b>Groupes</b>:');
define('TXT_SYSTEM', 'Sytème');
define('TXT_CUSTOMERS', 'Clients/Commandes');
define('TXT_PRODUCTS', 'Articles/Catégories');
define('TXT_STATISTICS', 'Outil de statistique');
define('TXT_TOOLS', 'Programmes supplémentaires');
define('TEXT_ACCOUNTING', 'Paramètres d\'accès pour:');

/******* SHOPGATE **********/
if (is_file(DIR_FS_CATALOG.'includes/external/shopgate/base/lang/french/admin/french.php')) {
  include_once (DIR_FS_CATALOG.'includes/external/shopgate/base/lang/french/admin/french.php');
}
/******* SHOPGATE **********/

// javascript messages
define('JS_ERROR', 'Des erreurs sont apparus pendant la saisie!\nVeuillez corriger le suivant:\n\n');

define('JS_OPTIONS_VALUE_PRICE', '*Il faut que vous attribué un prix à cette valeur\n');
define('JS_OPTIONS_VALUE_PRICE_PREFIX', '*Il faut que vous indiquiez un signe devant le prix (+/-)\n');

define('JS_PRODUCTS_NAME', '*Il faut que le nouveau article ait un nom\n');
define('JS_PRODUCTS_DESCRIPTION', '*Il faut que le nouveau articles ait une description\n');
define('JS_PRODUCTS_PRICE', '*Il faut que le nouveau article ait un prix\n');
define('JS_PRODUCTS_WEIGHT', '*Il faut que le nouveau article ait une indication de poids\n');
define('JS_PRODUCTS_QUANTITY', '*Il faut que vous attribué une quantité disponible au nouveau article\n');
define('JS_PRODUCTS_MODEL', '*Il faut que vous attribué un numéro d\'article au nouveau article\n');
define('JS_PRODUCTS_IMAGE', '*Il faut que vous attribué une image au nouveau article\n');

define('JS_SPECIALS_PRODUCTS_PRICE', '*Il faut que vous définissiez un nouveau prix pour cet article\n');

define('JS_GENDER', '*Il faut que vous choisissez un \'titre\'.\n');
define('JS_FIRST_NAME', '* Il faut que le \'prénom\' contienne au moins ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' signes.\n');
define('JS_LAST_NAME', '* Il faut que le \'nom\' contienne au moins ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' signes.\n');
define('JS_DOB', '* Il faut que la \'date de naissance\' soit au format: xx.xx.xxxx (jour/mois/année).\n');
define('JS_EMAIL_ADDRESS', '* Il faut que l\'\'adresse mail\' contienne au moins ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' signes.\n');
define('JS_ADDRESS', '* Il faut que la \'rue\' contienne au moins ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' signes.\n');
define('JS_POST_CODE', '* Il faut que le \'code postal\' contienne au moins ' . ENTRY_POSTCODE_MIN_LENGTH . ' signes.\n');
define('JS_CITY', '* Il faut que la \'ville \' contienne au moins ' . ENTRY_CITY_MIN_LENGTH . ' signes.\n');
define('JS_STATE', '* Il faut choisir la \'région\'.\n');
define('JS_STATE_SELECT', '-- Choisissez en-dessus --');
define('JS_ZONE', '* Il faut que la \'région\' soit sélectionnée de la liste pour ce pays.');
define('JS_COUNTRY', '* Il faut sélectionner le \'pays\'.\n');
define('JS_TELEPHONE', '* Il faut que le \'numéro de téléphone\' contienne au moins ' . ENTRY_TELEPHONE_MIN_LENGTH . ' signes.\n');
define('JS_PASSWORD', '* Le \'mot de passe\' ainsi que la \'confirmation de mot de passe\' doivent être égales et contenir au moins ' . ENTRY_PASSWORD_MIN_LENGTH . ' signes.\n');

define('JS_ORDER_DOES_NOT_EXIST', 'Numéro commande %s n\'existe pas!');

define('CATEGORY_PERSONAL', 'Données personelles');
define('CATEGORY_ADDRESS', 'Adresse');
define('CATEGORY_CONTACT', 'Contact');
define('CATEGORY_COMPANY', 'Entreprise');
define('CATEGORY_OPTIONS', 'Autres options');

define('ENTRY_GENDER', 'Titre:');
define('ENTRY_GENDER_ERROR', ' <span class="errorText">saisie nécessaire</span>');
define('ENTRY_FIRST_NAME', 'Prénom:');
define('ENTRY_FIRST_NAME_ERROR', ' <span class="errorText">au moins' . ENTRY_FIRST_NAME_MIN_LENGTH . ' lettres</span>');
define('ENTRY_LAST_NAME', 'Nom:');
define('ENTRY_LAST_NAME_ERROR', ' <span class="errorText">au moins' . ENTRY_LAST_NAME_MIN_LENGTH . ' lettres</span>');
define('ENTRY_DATE_OF_BIRTH', 'Date de naissance');
define('ENTRY_DATE_OF_BIRTH_ERROR', ' <span class="errorText">(p.ex. 21.05.1970)</span>');
define('ENTRY_EMAIL_ADDRESS', 'Adresse mail');
define('ENTRY_EMAIL_ADDRESS_ERROR', ' <span class="errorText">au moins' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' lettres</span>');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', ' <span class="errorText">adresse mail pas invalide! (Nous n\'acceptons pas de umlaut allemand dans des adresses mail.)</span>');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', ' <span class="errorText">Cette adresse mail existe déjà!</span>');
define('ENTRY_COMPANY', 'Raison sociale');
define('ENTRY_STREET_ADDRESS', 'Rue');
define('ENTRY_STREET_ADDRESS_ERROR', ' <span class="errorText">au moins' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' lettres</span>');
define('ENTRY_SUBURB', 'Adresse');
define('ENTRY_POST_CODE', 'Code postale');
define('ENTRY_POST_CODE_ERROR', ' <span class="errorText">au moins' . ENTRY_POSTCODE_MIN_LENGTH . ' nombres</span>');
define('ENTRY_CITY', 'Ville:');
define('ENTRY_CITY_ERROR', ' <span class="errorText">au moins' . ENTRY_CITY_MIN_LENGTH . ' lettres</span>');
define('ENTRY_STATE', 'Région');
define('ENTRY_STATE_ERROR', ' <span class="errorText">saisie nécessaire</font></small>');
define('ENTRY_COUNTRY', 'Pays:');
define('ENTRY_TELEPHONE_NUMBER', 'Numéro de téléphone:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', ' <span class="errorText">au moins' . ENTRY_TELEPHONE_MIN_LENGTH . ' nombres</span>');
define('ENTRY_FAX_NUMBER', 'Numéro fax:');
define('ENTRY_NEWSLETTER', 'Newsletter:');
define('ENTRY_CUSTOMERS_STATUS', 'Groupe client:');
define('ENTRY_NEWSLETTER_YES', 'abonné');
define('ENTRY_NEWSLETTER_NO', 'pas abonné');
define('ENTRY_MAIL_ERROR', ' <span class="errorText">Veuilez faire un choix</span>');
define('ENTRY_PASSWORD', 'Mot de passe (généré automatiquement)');
define('ENTRY_PASSWORD_ERROR', ' <span class="errorText">Il faut que votre mot de passe contienne au moins ' . ENTRY_PASSWORD_MIN_LENGTH . ' signes.</span>');
define('ENTRY_MAIL_COMMENTS', 'Texte supplémentaire au mail');

define('ENTRY_MAIL', 'Envoyer un mail avec le mot de passe aux clients?');
define('YES', 'oui');
define('NO', 'non');
define('SAVE_ENTRY', 'Sauvegarder les modifications');
define('TEXT_CHOOSE_INFO_TEMPLATE', 'Modèle pour des détails des articles');
define('TEXT_CHOOSE_OPTIONS_TEMPLATE', 'Modèle pour des options des articles');
define('TEXT_SELECT', '-- Veuillez choisir --');

// BOF - Tomcraft - 2009-06-10 - added some missing alternative text on admin icons
// Icons
define('ICON_ARROW_RIGHT', 'marqué');
define('ICON_BIG_WARNING', 'Attention!');
define('ICON_CROSS', 'Faux');
define('ICON_CURRENT_FOLDER', 'Dossier actuel');
define('ICON_DELETE', 'Supprimer');
define('ICON_EDIT', 'Modifier');
define('ICON_ERROR', 'Faute');
define('ICON_FILE', 'Fichier');
define('ICON_FILE_DOWNLOAD', 'Télécharger');
define('ICON_FOLDER', 'Dossier');
define('ICON_LOCKED', 'Bloqué');
define('ICON_POPUP', 'Visualisation bannière');
define('ICON_PREVIOUS_LEVEL', 'Niveau précédent');
define('ICON_PREVIEW', 'Prévisualisation');
define('ICON_STATISTICS', 'Statistique');
define('ICON_SUCCESS', 'Succès');
define('ICON_TICK', 'Vrai');
define('ICON_UNLOCKED', 'Débloqué');
define('ICON_WARNING', 'Avertissement');
// EOF - Tomcraft - 2009-06-10 - added some missing alternative text on admin icons

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Page %s de %s');
define('TEXT_DISPLAY_NUMBER_OF_BANNERS', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> bannières)');
define('TEXT_DISPLAY_NUMBER_OF_COUNTRIES', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> pays)');
define('TEXT_DISPLAY_NUMBER_OF_CUSTOMERS', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> clients)');
define('TEXT_DISPLAY_NUMBER_OF_CURRENCIES', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> monnaies)');
define('TEXT_DISPLAY_NUMBER_OF_LANGUAGES', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> langues)');
define('TEXT_DISPLAY_NUMBER_OF_MANUFACTURERS', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> fabricants)');
define('TEXT_DISPLAY_NUMBER_OF_NEWSLETTERS', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> newsletters)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> commandes)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS_STATUS', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> statuts de commande)');
define('TEXT_DISPLAY_NUMBER_OF_XSELL_GROUP', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> groupes de marketing croisé)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_VPE', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> unités de conditionnément)');
define('TEXT_DISPLAY_NUMBER_OF_SHIPPING_STATUS', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> statuts de livraison)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> articles)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_EXPECTED', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> articles attendus)');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> évaluations)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> offres spéciales)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_CLASSES', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> catégories fiscales)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_ZONES', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> zones d\'impôt)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_RATES', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> taux d\'imposition)');
define('TEXT_DISPLAY_NUMBER_OF_ZONES', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> régions)');
define('TEXT_DISPLAY_NUMBER_OF_WHOS_ONLINE', 'Affiche <b>%d</b> à <b>%d</b> (d\'au total <b>%d</b> clients en ligne)');

define('PREVNEXT_BUTTON_PREV', '«');
define('PREVNEXT_BUTTON_NEXT', '»');

define('TEXT_DEFAULT', 'Standard');
define('TEXT_SET_DEFAULT', 'Définir comme standard');
define('TEXT_FIELD_REQUIRED', ' <span class="fieldRequired">* Nécessaire</span>');

define('ERROR_NO_DEFAULT_CURRENCY_DEFINED', 'Erreur : Pas de devise par défaut sélectionnée. Veuillez la sélectionner sous Administration -> Langues/devises -> devises.');

define('TEXT_CACHE_CATEGORIES', 'Box catégories');
define('TEXT_CACHE_MANUFACTURERS', 'Box fabricants');
define('TEXT_CACHE_ALSO_PURCHASED', 'Module également achetée');

define('TEXT_NONE', '--aucun(e)--');
define('TEXT_AUTO_PROPORTIONAL', '--auto proportional--');
define('TEXT_AUTO_MAX', '--auto max--');
define('TEXT_TOP', 'Top');

define('ERROR_DESTINATION_DOES_NOT_EXIST', 'Erreur : emplacement de stockage n\'existe pas.');
define('ERROR_DESTINATION_NOT_WRITEABLE', 'Erreur : emplacement de stockage n\'est pas enregistrable.');
define('ERROR_FILE_NOT_SAVED', 'Erreur : le fichier n\'a pas pu être enregistré.');
define('ERROR_FILETYPE_NOT_ALLOWED', 'Erreur : type de fichier pas autorisé.');
define('SUCCESS_FILE_SAVED_SUCCESSFULLY', 'Succès : Fichier télécharger a été enregistré.');
define('WARNING_NO_FILE_UPLOADED', 'Attention : Pas de fichier téléchargé.');
define('ERROR_FILE_NOT_REMOVEABLE', 'Erreur : Le fichier n\'a pas pu être supprimé.');

define('DELETE_ENTRY', 'Supprimer saisie ?');
define('TEXT_PAYMENT_ERROR', '<b>ATTENTION :</b> Veuillez activer un <a href="'.xtc_href_link(FILENAME_MODULES, 'set=payment').'">module de paiement</a>!');
define('TEXT_SHIPPING_ERROR', '<b>ATTENTION :</b> Veuillez activer un <a href="'.xtc_href_link(FILENAME_MODULES, 'set=shipping').'">module d\'expédition</a>!');
define('TEXT_PAYPAL_CONFIG', '<b>ATTENTION :</b> Veuillez configurer les paramètres de paiement pour le "mode direct" sous : <a href="%s"><strong>Partner -> PayPal</strong></a>');
define('TEXT_NETTO', 'net : ');
define('TEXT_DUPLUCATE_CONFIG_ERROR', '<b>ATTENTION :</b> Duplicate configuration key: ');

define('ENTRY_CID', 'Numéro de client :');
define('IP', 'IP commande :');
define('CUSTOMERS_MEMO', 'Mémos :');
define('DISPLAY_MEMOS', 'Afficher/Écrire');
define('TITLE_MEMO', 'MÉMO client');
define('ENTRY_LANGUAGE', 'Langue :');
define('CATEGORIE_NOT_FOUND', 'Catégorie n\'existe pas');

// BOF - Tomcraft - 2009-06-10 - added some missing alternative text on admin icons
// Image Icons
define('IMAGE_RELEASE', 'Profiter du bon d\'achat');
define('IMAGE_ICON_STATUS_GREEN_STOCK', 'en stock');
define('IMAGE_ICON_STATUS_GREEN', 'actif');
define('IMAGE_ICON_STATUS_GREEN_LIGHT', 'activer');
define('IMAGE_ICON_STATUS_RED', 'inactif');
define('IMAGE_ICON_STATUS_RED_LIGHT', 'désactiver');
define('IMAGE_ICON_INFO', 'sélectionner');
// EOF - Tomcraft - 2009-06-10 - added some missing alternative text on admin icons

define('_JANUARY', 'Janvier');
define('_FEBRUARY', 'Février');
define('_MARCH', 'Mars');
define('_APRIL', 'Avril');
define('_MAY', 'Mai');
define('_JUNE', 'Juin');
define('_JULY', 'Juillet');
define('_AUGUST', 'Août');
define('_SEPTEMBER', 'Septembre');
define('_OCTOBER', 'Octobre');
define('_NOVEMBER', 'Novembre');
define('_DECEMBER', 'Décembre');

// Beschreibung f&uuml;r Abmeldelink im Newsletter
define('TEXT_NEWSLETTER_REMOVE', 'Pour vous désabonner de notre newsletter, cliquez ici :');

define('TEXT_DISPLAY_NUMBER_OF_GIFT_VOUCHERS', '<b>%d</b> à <b>%d</b> (sur <b>%d</b> bon d\'achats)');
define('TEXT_DISPLAY_NUMBER_OF_COUPONS', '<b>%d</b> à <b>%d</b> (sur <b>%d</b> coupons)');
define('TEXT_VALID_PRODUCTS_LIST', 'Liste de produits');
define('TEXT_VALID_PRODUCTS_ID', 'ID produit');
define('TEXT_VALID_PRODUCTS_NAME', 'Nom du produit');
define('TEXT_VALID_PRODUCTS_MODEL', 'Numéro de produit');

define('TEXT_VALID_CATEGORIES_LIST', 'Liste de catégories');
define('TEXT_VALID_CATEGORIES_ID', 'ID catégorie');
define('TEXT_VALID_CATEGORIES_NAME', 'Nom de la catégorie');

define('SECURITY_CODE_LENGTH_TITLE', 'Longueur du code de bon d\'achat');
define('SECURITY_CODE_LENGTH_DESC', 'Saisissez ici la langueur du bon d\'achat. (16 caractères maximum)');

define('NEW_SIGNUP_GIFT_VOUCHER_AMOUNT_TITLE', 'Valeur bon d\'achat cadeau de bienvenue');
define('NEW_SIGNUP_GIFT_VOUCHER_AMOUNT_DESC', 'Valeur bon d\'achat cadeau de bienvenue : Si vous ne souhaitez pas envoyer de bon d\'achat dans votre mail de bienvenue, tapez 0, sinon saisissez la valeur du bon d\'achat, par exemple 10.00 ou 50.00, sans symbole monétaire.');
define('NEW_SIGNUP_DISCOUNT_COUPON_TITLE', 'Code coupon réduction de bienvenue');
define('NEW_SIGNUP_DISCOUNT_COUPON_DESC', 'Code coupon réduction de bienvenue : Si vous ne souhaitez pas envoyer de coupon dans votre mail de bienvenue, ne tapez rien dans ce champs, sinon indiquez le code coupon que vous souhaitez utiliser.');

define('TXT_ALL', 'Tous');

// UST ID
define('HEADING_TITLE_VAT', 'Numéro d\'ID taxe sur le chiffre d\'affaires :');
define('ENTRY_VAT_ID', 'Numéro d\'ID taxe sur le chiffre d\'affaires :');
define('ENTRY_CUSTOMERS_VAT_ID', 'Numéro d\'ID taxe sur le chiffre d\'affaires :');
define('TEXT_VAT_FALSE', '<span class="messageStackError">Vérifié/Numéro d\'ID taxe sur le chiffre d\'affaires non valide</span>');
define('TEXT_VAT_TRUE', '<span class="messageStackSuccess">Vérifié/Numéro d\'ID taxe sur le chiffre d\'affaires valide</span>');
define('TEXT_VAT_UNKNOWN_COUNTRY', '<span class="messageStackError">Pas vérifié/Pays inconnu!</span>');
define('TEXT_VAT_INVALID_INPUT', '<span class="messageStackError">Pas vérifié/Le code pays est invalide ou le numéro d\'ID taxe sur le chiffre d\'affaires est vide !</span>');
define('TEXT_VAT_SERVICE_UNAVAILABLE', '<span class="messageStackError">Pas vérifié/Le serveur SOAP n\'a pas disponible, veuillez réessayer ultérieurement !</span>');
define('TEXT_VAT_MS_UNAVAILABLE', '<span class="messageStackError">Pas vérifié/Le service du pays membre n\'est pas disponible, veuillez réessayer ultérieurement ou avec un autre pays membre !</span>');
define('TEXT_VAT_TIMEOUT', '<span class="messageStackError">Pas vérifié/Le service du pays membre n\'est pas disponible (timeout), veuillez réessayer ultérieurement ou avec un autre pays membre !</span>');
define('TEXT_VAT_SERVER_BUSY', '<span class="messageStackError">Pas vérifié/Le service ne peut pas traiter votre demande. Veuillez réessayer ultérieurement !</span>');
define('TEXT_VAT_NO_PHP5_SOAP_SUPPORT', '<span class="messageStackError">Pas vérifié/Support pour PHP5 SOAP inexistant !</span>');
define('TEXT_VAT_CONNECTION_NOT_POSSIBLE', '<span class="messageStackError">ERREUR : Connexion au service web pas possible (ERREUR SOAP)!</span>');

define('ERROR_GIF_MERGE', 'Pas de support GDlib Gif, pas de filigrane possible');
define('ERROR_GIF_UPLOAD', 'Pas de support GDlib Gif, upload d\'images GIF impossible');

define('TEXT_REFERER', 'Referer :');

// BOF - Tomcraft - 2009-06-17 Google Sitemap
define('BOX_GOOGLE_SITEMAP', 'Google Sitemap');
// EOF - Tomcraft - 2009-06-17 Google Sitemap

// BOF - Tomcraft - 2009-10-03 - Paypal Express Modul
define('BOX_PAYPAL', 'PayPal');
// EOF - Tomcraft - 2009-10-03 - Paypal Express Modul

// BOF - Dokuman - 2009-10-02 - added moneybookers payment module version 2.4
define('_PAYMENT_MONEYBOOKERS_EMAILID_TITLE', 'Skrill adresse mail');
define('_PAYMENT_MONEYBOOKERS_EMAILID_DESC', 'Adresse mail avec laquelle vous êtes inscrite sur Skrill.com.<br />Si vous ne disposez pas encore de compte, <b>inscrivez-vous</b> maintenant <b>gratuitement</b> sur <a href="https://account.skrill.com/signup/page1" target="_blank"><b>Skrill</b></a>.');
define('_PAYMENT_MONEYBOOKERS_MERCHANTID_TITLE', 'ID vendeur Skrill');
define('_PAYMENT_MONEYBOOKERS_MERCHANTID_DESC', 'Votre ID vendeur Skrill.com');
define('_PAYMENT_MONEYBOOKERS_PWD_TITLE', 'Mot secret Skrill');
define('_PAYMENT_MONEYBOOKERS_PWD_DESC', 'En saisissant votre mot secret, la connexion lors du processus de paiement sera codée pour garantir la plus haute sécurité. Saisissez votre mot secret Skrill (ceci n’est pas votre mot de passe !). Le mot secret ne doit contenir que des lettres minuscules et des chiffres. Vous pouvez définir votre mot secret <b><span class="col-red">après le déverrouillage</b></span> dans votre compte utilisateur Skrill (paramètres vendeur). <br /><br /><span class="col-red">Vous déverrouillez ainsi votre compte Skrill.com pour le processus de paiement !</span><br /><br />Envoyez un mail avec : <br/>- Le nom de votre domaine<br/>- Votre adresse mail Skrill<br /><br />à :<a href="mailto:ecommerce@skrill.com?subject=modified eCommerce Shopsoftware: Aktivierung fuer Skrill Quick Checkout">ecommerce@skrill.com</a>');
define('_PAYMENT_MONEYBOOKERS_TMP_STATUS_ID_TITLE', 'Statut de commande - processus de paiement');
define('_PAYMENT_MONEYBOOKERS_TMP_STATUS_ID_DESC', 'Dès que le client a cliqué sur "envoyer commande", une "commande temporaire" sera créée. Comme ça, les clients qui annulent le processus de paiement sur Moneybookes, le processus de paiement sera enregistré.');
define('_PAYMENT_MONEYBOOKERS_PROCESSED_STATUS_ID_TITLE', 'Statut de commande - Paiement ok');
define('_PAYMENT_MONEYBOOKERS_PROCESSED_STATUS_ID_DESC', 'Sera affiché si le paiement de Skrill sera confirmé.');
define('_PAYMENT_MONEYBOOKERS_PENDING_STATUS_ID_TITLE', 'Statut de commande - Paiement en attente');
define('_PAYMENT_MONEYBOOKERS_PENDING_STATUS_ID_DESC', 'Si le client n\'a pas d\'avoirs sur son compte, le paiement restera en attente jusqu\'à ce que le compte Skrill soit équilibré.');

define('_PAYMENT_MONEYBOOKERS_CANCELED_STATUS_ID_TITLE', 'Statut de commande - Paiement annulé');
define('_PAYMENT_MONEYBOOKERS_CANCELED_STATUS_ID_DESC', 'Sera affiché, si par exemple la carte de crédit a été rejetée');
define('MB_TEXT_MBDATE', 'Dernière actualisation :');
define('MB_TEXT_MBTID', 'ID TR :');
define('MB_TEXT_MBERRTXT', 'Statut :');
define('MB_ERROR_NO_MERCHANT', 'Il n\'existe pas de compte Skrill.com avec cette adresse mail !');
define('MB_MERCHANT_OK', 'Compte Skrill.com correcte, ID vendeur %s réceptionné et enregistré de Skrill.com.');
define('MB_INFO', '<img src="../images/icons/moneybookers/MBbanner.jpg" /><br /><br />Vous pouvez désormais accepter cartes de crédit, débit direct, transfert immédiat, Giropay et tous les autres options de paiement locales avec une simple activation dans la boutique. Avec la solution All-in-One de Skrill il n‘est pas nécessaire de signer des contrats individuels par mode de paiement. Il vous faut uniquement un <a href="https://account.skrill.com/signup/page1" target="_blank"><b>compte Skrill gratuit</b></a> pour accepter tous les modes de paiement dans la boutique. Des modes de paiement supplémentaires n’engendrent pas de frais, le module ne contient <b>pas de frais mensuels ou frais d’installation</b>.<br /><br /><b>Vos bénéfices :</b><br />-L’acceptabilité des modes de paiement les plus importants augmentera votre chiffre d’affaire<br />- Un fournisseur réduira vos efforts et vos frais<br />-Votre client payera directement et sans processus d’inscription<br />-Activation en un clic et intégrationbr />-Des <a href="https://www.skrill.com/de/fees/" target="_blank"><b>conditions</b></a>très attractives <br />-Confirmation de paiement directe et vérification des données clients<br />-Exécution du paiement également à l’étranger sans frais<br />-6 millions de clients font confiance en Skrill dans le monde entier.');
// EOF - Dokuman - 2009-10-02 - added moneybookers payment module version 2.4

// BOF - Tomcraft - 2009-11-02 - set global customers-group-permissions
define('BOX_CUSTOMERS_GROUP', 'Autorisations KG');
// EOF - Tomcraft - 2009-11-02 - set global customers-group-permissions

// BOF - Tomcraft - 2009-11-02 - New admin top menu
define('TEXT_ADMIN_START', 'Accueil');
define('BOX_HEADING_CONFIGURATION2', 'Configuration élargie');
// EOF - Tomcraft - 2009-11-02 - New admin top menu

// BOF - Tomcraft - 2009-11-28 - Included xs:booster
define('BOX_HEADING_XSBOOSTER', 'xs:booster');
define('BOX_XSBOOSTER_LISTAUCTIONS', 'Afficher enchère');
define('BOX_XSBOOSTER_ADDAUCTIONS', 'Créer enchère');
define('BOX_XSBOOSTER_CONFIG', 'Configuration de base');
// EOF - Tomcraft - 2009-11-28 - Included xs:booster

//BOF - web28 - 2010-04-10 - ADMIN SEARCH BAR
define('ASB_QUICK_SEARCH_CUSTOMER', 'Rechercher client...');
define('ASB_QUICK_SEARCH_ORDER_ID', 'Rechercher numéro de commande...');
define('ASB_QUICK_SEARCH_ARTICLE', 'Rechercher produit/catégorie...');
define('ASB_QUICK_SEARCH_EMAIL', 'Rechercher adresse mail...');
define('ASB_QUICK_SEARCH_ARTICLE_ID', 'Rechercher produit/catégorie ID...');
//EOF - web28 - 2010-04-10 - ADMIN SEARCH BAR

//BOF - web28 - 2010.05.30 - accounting - set all checkboxes , countries - set all flags
define('BUTTON_SET', 'Activer tous');
define('BUTTON_UNSET', 'Désactiver tous');
//EOF - web28 - 2010.05.30 - accounting - set all checkboxes 

//BOF - DokuMan - 2010-08-12 - added possibility to reset admin statistics
define('TEXT_ROWS', 'Ligne');
define('TABLE_HEADING_RESET', 'Réinitialiser statistiques');
//EOF - DokuMan - 2010-08-12 - added possibility to reset admin statistics

//BOF - web28 - 2010-11-13 - added BUTTON_CLOSE_WINDOW
define('BUTTON_CLOSE_WINDOW' , 'Fermer fenêtre');
//EOF - web28 - 2010-11-13 - added BUTTON_CLOSE_WINDOW

//BOF - hendrik - 2011-05-14 - independent invoice number and date
define('ENTRY_INVOICE_NUMBER', 'Numéro de facture :');
define('ENTRY_INVOICE_DATE', 'Date de la facture :');
//EOF - hendrik - 2011-05-14 - independent invoice number and date  

//BOF - web28 - 2010-07-06 - added missing error text
define('ENTRY_VAT_ERROR', ' <span class="errorText">Numéro de taxe sur le chiffre d\'affaire invalide</span>');
//EOF - web28 - 2010-07-06 - added missing error text

define('CONFIG_INT_VALUE_ERROR', '"%s" ERREUR: Saisissez des chiffres uniquement ! Saisie %s ignorée !');
define('CONFIG_MAX_VALUE_WARNING', '"%s" ERREUR : Saisie %s ignorée ! [maximum: %s]');
define('CONFIG_MIN_VALUE_WARNING', '"%s" ERREUR : Saisie %s ignorée ! [minimum: %s]');

define('WHOS_ONLINE_TIME_LAST_CLICK_INFO', 'Intervalle d\'affichage en sec. : %s. Les entrées seront supprimées ensuite.');

define('TEXT_GLOBAL_PRODUCTS_MODEL', 'Numéro de produit');

define('TEXT_INFO_MODULE_RESTORE', 'Souhaitez-vous restaurer les paramètres sauvegardés ?<br /><br /><b>ATTENTION </b>:Tous les autres paramètres actuels seront écrasés !');
define('TEXT_INFO_MODULE_REMOVE', 'Souhaitez-vous desinstaller le module ? <br /><br /><b>ATTENTION</b> : Tous les autres paramètre de module seront écrasés !');
define('TEXT_INFO_MODULE_BACKUP', 'Souhaitez-vous sauvegarder les paramètres de module ?');
define('MODULE_BACKUP_CONFIRM', 'Les paramètres de module ont été sauvegardés avec succès !');
define('MODULE_RESTORE_CONFIRM', 'Les paramètres de module ont été restaurés avec succès !');
define('MODULE_UPDATE_CONFIRM', 'Les paramètres de module ont été actualisés avec succès !');

/* magnalister v1.0.0 */
define('BOX_HEADING_MAGNALISTER', 'magnalister');
define('BOX_MAGNALISTER', 'magnalister Admin');
/* END magnalister */

define('CHARS_LEFT', 'Chiffres restants');
define('CHARS_MAX', 'de max.');

define('DISPLAY_PER_PAGE', 'Afficher par page :');

define('SPECIALS_DATE_START_TT', 'Offres démarrent à 00h00h00');
define('SPECIALS_DATE_END_TT', 'Offres expirent à minuit (23h59h59)');

define('BOX_PARCEL_CARRIERS', 'Prestataire de services de colis');
define('TEXT_DISPLAY_NUMBER_OF_CARRIERS', '<b>%d</b> à <b>%d</b> (sur <b>%d</b> Prestataire de services de colis)');

define('RSS_FEED_TITLE', 'Informations actuelles du Blog eCommerce Shopsoftware');
define('RSS_FEED_DESCRIPTION', 'Informations actuelles du forum de support modified eCommerce Shopsoftware');
define('RSS_FEED_LINK', 'http://www.modified-shop.org/blog');
define('RSS_FEED_ALTERNATIVE', 'Les dernières nouvelles ne peuvent malheureusement pas être affichées dans le flux RSS. Veuillez visiter notre blog sous <a href="'.RSS_FEED_LINK.'">www.modified-shop.org/blog</a> pour vous renseigner sur le gestionnaire de la boutique les sujets suivants : <ul><li>Mises à jour et fixes</li><li>Extensions des fonctions</li><li>Jurisprudence</li><li>Dernières nouvelles</li><li>Potins</li></ul>');
define('TEXT_DISPLAY_NUMBER_OF_NEWSFEED', '<b>%d</b> à <b>%d</b> (sur <b>%d</b> dernières nouvelles)');

define('CFG_TXT_YES', 'Oui');
define('CFG_TXT_NO', 'Non');
define('CFG_TXT_OR', 'ou');
define('CFG_TXT_AND', 'et');
define('CFG_TXT_ASC', 'croissant');
define('CFG_TXT_DESC', 'décroissant');
define('CFG_TXT_PRODUCTS_NAME', 'Nom du produit');
define('CFG_TXT_PRODUCTS_MODEL', 'Numéro du produit');
define('CFG_TXT_DATE_EXPECTED', 'Disponible à partir de');
define('CFG_TXT_ACCOUNT', 'Compte client');
define('CFG_TXT_GUEST', 'Compte d\'invité');
define('CFG_TXT_BOTH', 'les deux');
define('CFG_TXT_NONE', 'desactivé');
define('CFG_TXT_ADMIN', 'Admin');
define('CFG_TXT_ALL', 'tous');
define('CFG_TXT_WEIGHT', 'Poids');
define('CFG_TXT_PRICE', 'Prix');
define('CFG_TXT_ITEM', 'Pièce');

define('CSRF_TOKEN_MANIPULATION', 'Manipulation CSRFToken (Pour des raisons de sécurité, il n\'est plus autorisé de travailler dans plusieurs tabs dans l\'espace admin.)');
define('CSRF_TOKEN_NOT_DEFINED', 'CSRFToken pas défini (Pour des raisons de sécurité, il n\'est plus autorisé de travailler dans plusieurs tabs dans l\'espace admin.)');

define('TEXT_ACCOUNTING_INFO', 'Il n\'est pas possible de retirer les droits d\'accès du Admin principal [1] !');

define('JAVASCRIPT_DISABLED_INFO', 'JavaScript est desactivé dans votre navigateur. Veuillez activer JavaScript afin de pouvoir utiliser toutes les fonctions de votre site web et de pouvoir voir tous les contenus.');

define('BOX_MODULE_TYPE', 'Module extensions de classes');

define('MULTIPLE_INSTALLATION', '<span style="color:red">[Installation multiple: %s]</span>');

define('FILEUPLOAD_INPUT_TXT', 'Pas de fichier');
define('FILEUPLOAD_BTN_TXT', 'Rechercher');

define('CHECK_LABEL_PRICE', 'Demander prix');

define('TEXT_PAYPAL_TAB_CONFIG', 'Configuration PayPal');
define('TEXT_PAYPAL_TAB_PROFILE', 'Profile PayPal');
define('TEXT_PAYPAL_TAB_WEBHOOK', 'Webhook PayPal');
define('TEXT_PAYPAL_TAB_MODULE', 'Module PayPal');
define('TEXT_PAYPAL_TAB_TRANSACTIONS', 'Transactions PayPal');

define('TEXT_DEFAULT_SORT_ORDER_TITLE', 'Ordre de triage');
define('TEXT_DEFAULT_SORT_ORDER_DESC', 'Ordre de traitement. Chiffre le plus bas sera exécuté en premier.');
define('TEXT_DEFAULT_STATUS_TITLE', 'Activer module ?');
define('TEXT_DEFAULT_STATUS_DESC', 'Statut module');

define('TEXT_HOUR', 'Heure');
define('TEXT_HOURS', 'Heures');

define('DELETE_LOGS_SUCCESSFUL', 'Logfiles supprimés avec succès.');

define('BOX_BLACKLIST_LOGS', 'Blacklist Logs');

define('CONTINUE_WITHOUT_SAVE', 'Modifications non enregistrées seront perdues.');
?>

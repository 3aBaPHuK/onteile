<?php
/***********************************************************************************************************
* file: langs_edit_ajax.php
* use: test translated files from langs_edit.php
*
* (c) noRiddle 06-2018
***********************************************************************************************************/

if(isset($_POST['test_file']) && !empty($_POST['test_file'])) {

    exec('php -l '.escapeshellarg($_POST['test_file']), $out, $ret); //if third parameter (here $ret) returns 0 everything is okay
    if($ret !== 0) {
        echo print_r($out, true);
    } else {
        echo '('.$_POST['test_file_no'].') is OKAY';
    }
    
    exit();
}
?>
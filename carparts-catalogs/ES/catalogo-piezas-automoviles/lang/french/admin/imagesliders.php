<?php
/************************************************************************
* language file for imageslider                                         *
* plugin derived from imageslider by Hetfield                           *
* based on                                                              *
* - responsive kenburner slider by codecanyon, licence necessary        *
*                                                                       *
* copyright (c) for all changes: noRiddle of webdesign-endres.de 2013   *
************************************************************************/

$slider_measures = array();
$slider_flyin_measures = array();
if((isset($data20) && !empty($data20)) && !isset($dataC20) && file_exists('../includes/local/configure.php')) {
    switch($shop) {
        case 'audi'.$shop_suffix:
            $slider_measures[$shop] = array('1420px', '250px');
            $slider_flyin_measures[$shop] = array('478', '146');
        break;
        default:
            $slider_measures[$shop] = array('1420px', '250px');
            $slider_flyin_measures[$shop] = array('478', '146');
        break;
    }
} else if((isset($dataC20) && !empty($dataC20)) || !file_exists('../includes/local/configure.php')) {
    $slider_measures[$shop] = array('1580px', '250px');
    $slider_flyin_measures[$shop] = array('478', '146');
}

define('HEADING_TITLE_II', 'CoKeBuMo-Imageslider ed. 1.5.2');
define('TABLE_HEADING_IMAGESLIDERS', 'Curseur d\'images');
define('TABLE_HEADING_SPEED', 'Vitesse');
define('TABLE_HEADING_SORTING', 'Tri');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');
define('TEXT_HEADING_NEW_IMAGESLIDER', 'Nouvelle image');
define('TEXT_HEADING_EDIT_IMAGESLIDER', 'Editer l\'image');
define('TEXT_HEADING_DELETE_IMAGESLIDER', 'Supprimer l\'image');
define('TEXT_IMAGESLIDERS', 'Image:');
define('TEXT_IMAGE_DIMENSIONS', 'Dimensions de l\'image originales:');
define('TEXT_DATE_ADDED', 'ajouté le:');
define('TEXT_LAST_MODIFIED', 'Dernière modification le:');
define('TEXT_IMAGE_NONEXISTENT', 'L\'IMAGE N\'EXISTE PAS');
define('TEXT_NEW_INTRO', 'Veuillez créer l\'image avec toutes les données pertinentes.');
define('TEXT_EDIT_INTRO', 'Veuillez faire tous les changements nécessaires');
define('TEXT_IMAGESLIDERS_TITLE', 'Titre pour l\'image:');
define('TEXT_IMAGESLIDERS_NAME', 'Nom pour l\'entrée de l\'image:');
define('TEXT_IMAGESLIDERS_IMAGE', 'Image:');
define('TEXT_IMAGESLIDERS_URL', 'Inclure l\'image <span style="color:#B30000; font-size:9px;"> (seulement possible si aucune vidéo a été incluse (vois plus bas), si une vidéo est incluse séléctionnez "pas de lien" )</span>');
define('TEXT_TARGET', 'Fenêtre de visée:');
define('TEXT_TYP', 'Type du lien:');
define('TEXT_URL', 'Adresse du lien:');
define('TEXT_IMAGESLIDERS_ALT', 'alt-tag pour l\'image (Google?):');
define('TEXT_IMAGESLIDERS_VIDEO_URL', 'Embed-URL d\'une vidéo désirée pour cette image. "YouTube" et "Vimeo" sont soutenus.');
define('TEXT_IMAGESLIDERS_VIDEO_DESC', 'Déscription de la vidéo:');
define('TEXT_IMAGESLIDERS_CREDITS_DESC', 'Explications et Credits:');
define('TEXT_IMAGESLIDERS_CREDITS_EXPL', '<p>Based on kenburner by codecanyon, code widely modified for better text-effects and other changes by <a href="http://www.revilonetz.de"');
define('TXT_BUTTON_COPY_SLIDER', 'Copier le curseur de la boutique %s');
define('TXT_IDS_RANGE', 'Entrez Ids de à en forme X-Y. (Exemple:10-15)<br /> Sie le champ reste libre toutes les images seront copiées.');
define('TXT_COPY_SLIDER_EXPL', 'Seul les images activées sont copiées du curseur de la boutique %1$s pour le moment.<br />ATTENTION: %1$s a éventuellement d\'autres dimensions, veuillez vérifier d\'abord.');
define('TXT_BUTTON_NEWIMAGE', 'Insérer une nouvelle image');
define('TXT_BUTTON_COPY_IMAGE', 'Copier l\'imager avec toutes les configurations');
define('TXT_DEFAULT_COPY_IMAGE', 'Séléctionner l\'image');

define('NONE_TARGET', 'Ne fixer aucune cible');
define('TARGET_BLANK', '_blank');
define('TARGET_TOP', '_top');
define('TARGET_SELF', '_self');
define('TARGET_PARENT', '_parent');
define('TYP_NONE', 'Pas de lien');
define('TYP_PRODUCT', 'Lien au produit (veuillez seulement entrer l\'ID du produit dans l\'adresse du lien)');
define('TYP_CATEGORIE', 'Lien vers la catégorie (Veuillez seulement entrer l\'IDcat dans l\'adresse du lien. <span style="color:#c00">!! Dans les sous-catégories seulement la dernière ID sans le tiret bas !!</span>)');
define('TYP_CONTENT', 'Lien vers la page de contenu (veuillez seulement entrer la IDco dans l\'adresse du lien)');
define('TYP_INTERN', 'Lien de boutique interne (par exemple account.php oder newsletter.php)');
define('TYP_EXTERN', 'Lien externe (par exemple http://www.lienexterne.com)');
define('TEXT_IMAGESLIDERS_DESCRIPTION', 'Déscription de l\'image:');
define('INFO_IMAGESLIDERS_DESCRIPTION', 'Si vous insérez des images la largeur doit être au maximum '.$slider_flyin_measures[$shop][0].', et la hauteur au maximum '.$slider_flyin_measures[$shop][1].'<br />Veuillez impérativement supprimer les dimensions de l\'image dans le navigateur de fichiers.<br /> Dans le "style" Dropdown vous pouvez choisir des tailles de polices diverses.<br />Ne veuillez pas utiliser le Dropdown "Taille" pour les tailles de police.');
define('TEXT_DELETE_INTRO', 'Êtes-vous sûr de voulour supprimer cette image?');
define('TEXT_DELETE_IMAGE', 'Voulez-vous également supprimer l\'image?');
define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le répértoire %s est en lecture seule. Veuillez corriger les privilèges d\'accès de ce répértoire!');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le répértoire %s n\'existe pas!');
define('TEXT_DISPLAY_NUMBER_OF_IMAGESLIDERS', 'Affichées sont les images <b>%d</b> à <b>%d</b> (d\'un total de <b>%d</b> images dans le diaporama)');  //'Nombre d\'images dans le diaporama');
define('IMAGE_ICON_STATUS_GREEN', 'Actif');
define('IMAGE_ICON_STATUS_GREEN_LIGHT', 'Activer');
define('IMAGE_ICON_STATUS_RED', 'Pas actif');
define('IMAGE_ICON_STATUS_RED_LIGHT', 'Désactiver');
define('ACTIVE', 'acif');
define('NOTACTIVE', 'pas actif');

define('INFO_IMAGE_NAME', 'Veuillez entrer le nom de l\'image ici pour reconnaitre l\'image par son nom lors de l\'édition.');
define('INFO_IMAGE_SORT', 'Veuillez entrer un nombre entier pour définir l\'ordre de l\'affichage des images.');
define('INFO_IMAGE_ACTIVE', 'Vous pouvez activer ou désactiver l\'image ici.<br>Les autres images qui sont actives ne sont pas influencées par ce choix.');
define('INFO_IMAGE_UPLOAD', 'Le nom de l\'image à télécharger ne doit pas être trop long (pas plus de 38 signes sans extension du fichier).<br>Pour chaque langue dans laquelle le curseur doit être réglé vous devez télécharger une image.<br><span style="color:#c00;">!! Dimensions pour votre boutique !!</span><br>Bildbreite: '.$slider_measures[$shop][0].'<br>Bildhöhe: '.$slider_measures[$shop][1]);
define('INFO_IMAGE_TITLE', 'Le titre de l\'image est nécessaire pour référencier le text qui est représenté dans l\'image.<br>Veuillez entrer un titre court et clair<br> Vous pouvez seulement utiliser une fois chaque titre, veuillez donc utiliser d\'autres titres pour les autres images.<br>Le mieux est d\'utiliser toujours le même titre et de le numéroter.<br>Par exemple:<br>cap1, prochaine image cap2 etc.<br /><span style="color:#c00">Si vous indiquer pas de déscription, n\'indiquez pas de titre ici !</span>');
define('INFO_IMAGE_ALT', 'Veuillez entrer le texte que vous voulez mettre dans le alt-tag de l\'image.<br />Cela pourrait être important pour les résultats des moteurs de recherche.');

//***BOC KenBurns Codecanyon***
define('TEXT_IMAGESLIDERS_DATA_ATTRIBUTES', 'Effets pour le curseur <span style="color:#B30000; font-size:9px;"></span>');
define('INFO_IMAGE_DATA_ATTRIBUTES', 'Veuillez choisir une option des options représentées ici quand l\'effet KenBurn est activé.<br> Les champs de texte doivent également être remplis (voir là).');
define('INFO_IMAGE_CAPTION_ATTRIBUTES', 'Veuillez assurer que les effets correspondent au lieu de représentation (voir plus bas)');
define('INFO_IMAGE_VIDEO_URL', 'Si l\'image doit contenir une vidéo, veuillez entrer la Embed-URL ici.<br />Embed-URL est l\'URL qui est inclus dans le code embarqué fourni par "YouTube" ou "Vimeo". <span style="color:#c00;">!Seulement l\'URL, pas le Embed-Code complet !</span><br /> Vous pouvez réprimer la lecture des vidéos relatifs en attachant ce qui suit à l\'URL (des vidéos Youtube): <i>&rel=0</i><br /> Il y a d\'autres paramètres qui permettent d\'influencer le comportement du lecteur vidéo.<br />Veuillez voir les spécificités Youtube et Vimeo pour cela.');

define('TXT_GENERAL_KENBURNS', 'Activer ou déscativer l\'effet KenBurn');
define('TXT_IF_KENBURNS_ON', 'Si l\'effet KenBurn est désactivé les réglages en dessous ne sont pas tous nécessaires (vois là).');
define('TXT_GENERAL_TRANSITION', 'La transition des images, "fade" oder "slide" (<span style="color:#c00;">! doit toujours être indiqué</span>)');
define('TXT_GENERAL_STARTALIGN', 'Début de l\'effet KenBurn, horizontal | vertical');
define('TXT_GENERAL_ENDALIGN', 'Fin de l\'effet KenBurn, horizontal | vertical');
define('TXT_GENERAL_ZOOM_AND_PAN', 'Réglages du zoom et durée du mouvement');
define('TXT_GENERAL_CAPTION', 'Régler où le texte apparaît et avec quel effet');
define('TEXT_IMAGESLIDERS_CAPTION_ATTRIBUTES', 'Les attributs de texte<span style="color:#B30000; font-size:9px;"></span>');
define('TXT_CAPTION_DIRECTION_AND_EFFECT', 'Les effets et lieux de parution qui vont ensemble sont côte à côte');
define('TXT_CAPTION_LEFT', 'Texte à gauche');
define('TXT_CAPTION_FADE_RIGHT', 'glisse de gauche à droite');
define('TXT_CAPTION_RIGHT', 'Texte à droite');
define('TXT_CAPTION_FADE_LEFT', 'glisse de droite à gauche');
define('TXT_CAPTION_TOP', 'Texte en haut');
define('TXT_CAPTION_FADE_DOWN', 'glisse du haut au bas');
define('TXT_CAPTION_BOTTOM', 'Texte en bas');
define('TXT_CAPTION_FADE_UP', 'glisse du bas au haut');
define('TXT_CAPTION_FADE', 's\'affiche avec l\'effet "fade", convient toujours');
define('TXT_KENBURN_ON', '<b>Allumé</b>');
define('TXT_KENBURN_OFF', '<b>Eteint</b>');
define('TXT_TRANSITION_FADE', '<b>Fade</b>');
define('TXT_TRANSITION_SLIDE', '<b>Slide</b>');
define('TXT_STARTALIGN_LEFTTOP', '<b>gauche | haut</b>');
define('TXT_STARTALIGN_LEFTCENTER', '<b>gauche | centre</b>');
define('TXT_STARTALIGN_LEFTBOTTOM', '<b>gauche | bas</b>');
define('TXT_STARTALIGN_CENTERTOP', '<b>centre | haut</b>');
define('TXT_STARTALIGN_CENTERCENTER', '<b>centre | centre</b>');
define('TXT_STARTALIGN_CENTERBOTTOM', '<b>centre | bas</b>');
define('TXT_STARTALIGN_RIGHTTOP', '<b>droite | haut</b>');
define('TXT_STARTALIGN_RIGHTCENTER', '<b>droite | centre</b>');
define('TXT_STARTALIGN_RIGHTBOTTOM', '<b>droite | bas/b>');
define('TXT_STARTALING_RANDOM', '<b>aléatoire</b>');
define('TXT_ENDALIGN_LEFTTOP', '<b>gauche | haut</b>');
define('TXT_ENDALIGN_LEFTCENTER', '<b>gauche | centre</b>');
define('TXT_ENDALIGN_LEFTBOTTOM', '<b>gauche | bas</b>');
define('TXT_ENDALIGN_CENTERTOP', '<b>centre | haut</b>');
define('TXT_ENDALIGN_CENTERCENTER', '<b>centre | centre</b>');
define('TXT_ENDALIGN_CENTERBOTTOM', '<b>centre | bas</b>');
define('TXT_ENDALIGN_RIGHTTOP', '<b>droite | haut</b>');
define('TXT_ENDALIGN_RIGHTCENTER', '<b>droite | centre</b>');
define('TXT_ENDALIGN_RIGHTBOTTOM', '<b>droite | bas</b>');
define('TXT_ENDALIGN_RANDOM', '<b>aléatoire</b>');
define('TXT_ZOOM', '<b>Zoom à une vue large ou zoom à une vue restreinte</b>, entrez la valeur "out | in | random" ');
define('TXT_ZOOMFACT', 'Entrez le facteur de zoom (valeur non entière à un chiffre (! virgule doit être un point) entre 1 et 2). Si la valeur ci-dessus est "out" le facteur de zoom doit être proportionnellement élévé ou l\'image doit être plus grande que le container HTML dans lequel elle se situe.');
define('TXT_PANDURATION', 'Durée du mouvement en secondes (<span style="color:#c00;">! doit toujours être indiqué</span>) (si elle n\'est pas indiquée la valeur des réglages à défaut pour "timer" <i>general.js est utilisé.');
?>

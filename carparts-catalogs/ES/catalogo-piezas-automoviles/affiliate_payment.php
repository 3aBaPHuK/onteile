<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_payment.php 40 2013-01-08 16:36:44Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_payment.php, v 1.9 2003/09/22);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

require('includes/application_top.php');

if (!isset($_SESSION['affiliate_id'])) {
    xtc_redirect(xtc_href_link(FILENAME_AFFILIATE, '', 'SSL'));
}

// include needed functions
require_once(DIR_FS_INC . 'xtc_date_short.inc.php');

// create smarty elements
$smarty = new Smarty;

$breadcrumb->add(NAVBAR_TITLE, xtc_href_link(FILENAME_AFFILIATE, '', 'SSL'));
$breadcrumb->add(NAVBAR_TITLE_PAYMENT, xtc_href_link(FILENAME_AFFILIATE_PAYMENT, '', 'SSL'));

if (!isset($_GET['page'])) $_GET['page'] = 1;

$affiliate_payment_raw = "select p.* , s.affiliate_payment_status_name
           from " . TABLE_AFFILIATE_PAYMENT . " p, " . TABLE_AFFILIATE_PAYMENT_STATUS . " s 
           where p.affiliate_payment_status = s.affiliate_payment_status_id 
           and s.affiliate_language_id = '" . $_SESSION['languages_id'] . "'
           and p.affiliate_id =  '" . $_SESSION['affiliate_id'] . "'
           order by p.affiliate_payment_id DESC";

$affiliate_payment_split = new splitPageResults($affiliate_payment_raw, $_GET['page'], MAX_DISPLAY_SEARCH_RESULTS);

require(DIR_FS_CATALOG .'templates/'.CURRENT_TEMPLATE. '/source/boxes.php');
require(DIR_WS_INCLUDES . 'header.php');

$smarty->assign('affiliate_payment_split_number', $affiliate_payment_split->number_of_rows);

$affiliate_payment_table = '';

if ($affiliate_payment_split->number_of_rows > 0) {
	$affiliate_payment_values = xtc_db_query($affiliate_payment_split->sql_query);
	$ap_array = array();
    while ($affiliate_payment = xtc_db_fetch_array($affiliate_payment_values)) {
    	
    	$ap_array[] = array('affiliate_id' => $affiliate_payment['affiliate_payment_id'],
    						'affiliate_payment_date' => xtc_date_short($affiliate_payment['affiliate_payment_date']),
    						'affiliate_payment_total' => $xtPrice->xtcFormat($affiliate_payment['affiliate_payment_total'], true),
    						'affiliate_payment_status_name' => $affiliate_payment['affiliate_payment_status_name']);
	}
	$smarty->assign('ap_array', $ap_array);
}

if ($affiliate_payment_split->number_of_rows > 0) {
	$smarty->assign('affiliate_payment_split_count', $affiliate_payment_split->display_count(TEXT_DISPLAY_NUMBER_OF_PAYMENTS));
	$smarty->assign('affiliate_payment_split_link', $affiliate_payment_split->display_links(MAX_DISPLAY_PAGE_LINKS, xtc_get_all_get_params(array('page', 'info', 'x', 'y'))));
}

$affiliate_payment_values = xtc_db_query("select sum(affiliate_payment_total) as total from " . TABLE_AFFILIATE_PAYMENT . " where affiliate_id = '" . $_SESSION['affiliate_id'] . "'");
$affiliate_payment = xtc_db_fetch_array($affiliate_payment_values);

$smarty->assign('affiliate_payment_total', $xtPrice->xtcFormat($affiliate_payment['total'], true));
$smarty->assign('language', $_SESSION['language']);
$smarty->caching = 0;
$main_content = $smarty->fetch(CURRENT_TEMPLATE . '/module/affiliate_payment.html');
$smarty->assign('main_content',$main_content);

if (!defined(RM))
	$smarty->load_filter('output', 'note');
	
$smarty->display(CURRENT_TEMPLATE . '/index.html');

include ('includes/application_bottom.php');
?>
<?php
/* --------------------------------------------------------------
   $Id: specials.php 4200 2013-01-10 19:47:11Z Tomcraft1980 $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(specials.php,v 1.10 2002/01/31); www.oscommerce.com 
   (c) 2003	 nextcommerce (specials.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Promotions');

define('TABLE_HEADING_PRODUCTS', 'Articles');
define('TABLE_HEADING_PRODUCTS_QUANTITY', 'Quantité d\'articles (stock)');
define('TABLE_HEADING_SPECIALS_QUANTITY', 'Quantité promotions');
define('TABLE_HEADING_START_DATE', 'Valide à partir de');
define('TABLE_HEADING_EXPIRES_DATE', 'Valide jusqu\'au');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Prix de l\'article');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_SPECIALS_PRODUCT', 'Article:');
define('TEXT_SPECIALS_SPECIAL_PRICE', 'Offre promotionnelle:');
define('TEXT_SPECIALS_SPECIAL_QUANTITY', 'Quantité:');
define('TEXT_SPECIALS_START_DATE', 'Valide à partir du: <small>(JJJJ-MM-TT)</small>');
define('TEXT_SPECIALS_EXPIRES_DATE', 'Valide jusqu\'au: <small>(JJJJ-MM-TT)</small>');

define('TEXT_INFO_DATE_ADDED', 'ajouté le:');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification:');
define('TEXT_INFO_NEW_PRICE', 'Nouveau prix:');
define('TEXT_INFO_ORIGINAL_PRICE', 'Prix original:');
define('TEXT_INFO_PERCENTAGE', 'Pourcentage:');
define('TEXT_INFO_START_DATE', 'Valide à partir du:');
define('TEXT_INFO_EXPIRES_DATE', 'Valide jusqu\'au:');
define('TEXT_INFO_STATUS_CHANGE', 'Désactivé le:');

define('TEXT_INFO_HEADING_DELETE_SPECIALS', 'Supprimer la promotion');
define('TEXT_INFO_DELETE_INTRO', 'Êtes-vous sûr de voulour supprimer la promotion?');

define('TEXT_IMAGE_NONEXISTENT', 'Pas d\'image disponible!');

define('TEXT_SPECIALS_PRICE_TIP', 'Vous pouvez aussi indiquer des valeurs en pourcentage dans le champ offre promotionnelle. Par exemple: <strong>20%</strong><br> Quand vous indiquez un nouveau prix, vous devez séparer les décimales avec un \'.\' , par exemple: <strong>49.99</strong>');
define('TEXT_SPECIALS_QUANTITY_TIP', 'Dans le champ <strong>Quantité</strong> vous pouvez indiquer le nombre de pièces sur lequel la promotion s\'applique.<br> Sous "configuration" -> "Options de gestion des stocks" -> "Vérification des promotions" vous pouvez décider si vous voulez vérifier le stock des promotions.');
define('TEXT_SPECIALS_START_DATE_TIP', 'Indiquez la date à partir de laquelle l\'offre promotionnelle s\'applique.<br>');
define('TEXT_SPECIALS_EXPIRES_DATE_TIP', 'Laissez le champ <strong>Valide jusqu\'au</strong> vide, si l\'offre promotionnelle est sans limitation de durée.<br>');
?>

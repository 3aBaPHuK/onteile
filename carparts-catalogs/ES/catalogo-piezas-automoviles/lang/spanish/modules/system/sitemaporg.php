<?php
/* -----------------------------------------------------------------------------------------
   
   $Id: sitemaporg.php 
   XML-Sitemap.org for xt:Commerce SP2.1a
   by Mathis Klooss
   V1.2
   -----------------------------------------------------------------------------------------
      Original Script:
   $Id: gsitemaps.php 
   Google Sitemaps by hendrik.koch@gmx.de
   V1.1 August 2006
   -----------------------------------------------------------------------------------------
   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com 
   (c) 2003	 nextcommerce (invoice.php,v 1.6 2003/08/24); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SITEMAPORG_TEXT_DESCRIPTION', 'La definición estándar la encuentra aquí: <a href="http://www.sitemaps.org/" target="_blank">www.sitemap.org</a>');
define('MODULE_SITEMAPORG_TEXT_TITLE', 'XML Sitemap.org');
define('MODULE_SITEMAPORG_FILE_TITLE' , '<hr />Nombre del archivo');
define('MODULE_SITEMAPORG_FILE_DESC' , 'Introduzca un nombre de archivo si el archivo de exportación debe guardarse en el servidor.<br />(directorio "export/")');
define('MODULE_SITEMAPORG_STATUS_DESC', 'Estado del módulo');
define('MODULE_SITEMAPORG_STATUS_TITLE', 'Estado');
define('MODULE_SITEMAPORG_CHANGEFREQ_TITLE', 'Frecuencia de cambio');
define('MODULE_SITEMAPORG_CHANGEFREQ_DESC', 'La frecuencia con la que probablemente cambiará la página.');
define('MODULE_SITEMAPORG_ROOT_TITLE', '<hr /><b>¿Instalación en root?</b>');
define('MODULE_SITEMAPORG_ROOT_DESC', '¿Debe colocarse inmediatamente el archivo Sitemap en el directorio root?');
define('MODULE_SITEMAPORG_PRIORITY_LIST_TITLE', '<b>Prioridad para la lista</b>');
define('MODULE_SITEMAPORG_PRIORITY_LIST_DESC', '');
define('MODULE_SITEMAPORG_PRIORITY_PRODUCT_TITLE', '<b>Prioridad para los productos</b>');
define('MODULE_SITEMAPORG_PRIORITY_PRODUCT_DESC', '');
define('MODULE_SITEMAPORG_GZIP_TITLE', '<b>¿Utilizar compresión gzip?</b>');
define('MODULE_SITEMAPORG_GZIP_DESC', '¡La extensión ".gz" se coloca automáticamente al final del archivo!');
define('MODULE_SITEMAPORG_EXPORT_TITLE', '<hr /><b>¿Descargar?</b>');
define('MODULE_SITEMAPORG_EXPORT_DESC', '¿Desea descargar el archivo?');
define('MODULE_SITEMAPORG_YAHOO_TITLE', 'ID de Yahoo');
define('MODULE_SITEMAPORG_YAHOO_DESC', '¡Introduzca aquí su ID de Yahoo! Esto es necesario para comunicar el Sitemap a Yahoo.');

?>

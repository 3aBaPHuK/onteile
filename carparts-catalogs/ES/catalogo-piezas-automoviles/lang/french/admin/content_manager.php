<?php
/* --------------------------------------------------------------
   $Id: content_manager.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on:
   (c) 2003	 nextcommerce (content_manager.php,v 1.8 2003/08/25); www.nextcommerce.org
   
   Released under the GNU General Public License 
   --------------------------------------------------------------*/
   
define('HEADING_TITLE', 'Gestionnaire de contenu');
define('HEADING_CONTENT', 'Contenu de page');
define('HEADING_PRODUCTS_CONTENT', 'Contenu d\'article');
define('TABLE_HEADING_CONTENT_ID', 'ID');
define('TABLE_HEADING_CONTENT_TITLE', 'Titre');
define('TABLE_HEADING_CONTENT_FILE', 'Fichier');
define('TABLE_HEADING_CONTENT_STATUS', 'Visible dans la boîte');
define('TABLE_HEADING_CONTENT_BOX', 'Boîte');
define('TABLE_HEADING_PRODUCTS_ID', 'ID');
define('TABLE_HEADING_PRODUCTS', 'Article');
define('TABLE_HEADING_PRODUCTS_CONTENT_ID', 'ID');
define('TABLE_HEADING_LANGUAGE', 'Langue');
define('TABLE_HEADING_CONTENT_NAME', 'Nom/Nom de fichier');
define('TABLE_HEADING_CONTENT_LINK', 'Lien');
define('TABLE_HEADING_CONTENT_HITS', 'Vus');
define('TABLE_HEADING_CONTENT_GROUP', 'coID');
define('TABLE_HEADING_CONTENT_SORT', 'Ordre');
define('TEXT_YES', 'Oui');
define('TEXT_NO', 'Non');
define('TABLE_HEADING_CONTENT_ACTION', 'Action');
define('TEXT_DELETE', 'Supprimer');
define('TEXT_EDIT', 'Modifier');
define('TEXT_PREVIEW', 'Aperçu');
define('CONFIRM_DELETE', 'Supprimer le contenu?');
define('CONTENT_NOTE', 'Contenu marque avec <span class="col-red">*</span> obtient au système et ne peut pas être supprimé!');

 
 // edit
define('TEXT_LANGUAGE', 'Langue:');
define('TEXT_STATUS', 'Visible:');
define('TEXT_STATUS_DESCRIPTION', 'Afficher le lien dans la boîte?');
define('TEXT_TITLE', 'Titre:');
define('TEXT_TITLE_FILE', 'Titre/ Nom du fichier:');
define('TEXT_SELECT', '-Veuillez choisir-');
define('TEXT_HEADING', 'Titre:');
define('TEXT_CONTENT', 'Texte:');
define('TEXT_UPLOAD_FILE', 'Télécharger fichier:');
define('TEXT_UPLOAD_FILE_LOCAL', '(à partir de votre système local)');
define('TEXT_CHOOSE_FILE', 'Sélectionner fichier:');
define('TEXT_CHOOSE_FILE_DESC', 'Vous pouvez également choisir un fichier utilisé de la liste');
define('TEXT_NO_FILE', 'Supprimer la sélection');
define('TEXT_CHOOSE_FILE_SERVER', '(Si vous avez sauvegardé vos fichiers vous-même via FTP sur votre serveur <i>(media/content)</i>, vous pouvez choisir le fichier ici.');
define('TEXT_CURRENT_FILE', 'Fichier actuel:');
define('TEXT_FILE_DESCRIPTION', '<b>Info:</b><br />Vous avez également la possibilité d‘intégrer un fichier <b>.html</b> ou <b>.htm</b> comme contenu.<br /> Si vous choisissez ou téléchargez un fichier, le texte au champ de texte sera ignoré.<br /><br />');
define('ERROR_FILE', 'Mauvais Format de fichier  (seulement .html ou .htm)');
define('ERROR_TITLE', 'Veuillez entrer un titre');
define('ERROR_COMMENT', 'Veuillez entrer une description du fichier!');
define('TEXT_FILE_FLAG', 'Boîte:');
define('TEXT_PARENT', 'Documents majeurs:');
define('TEXT_PARENT_DESCRIPTION', 'Attribuer à ce document comme sous-contenu');
define('TEXT_PRODUCT', 'Article:');
define('TEXT_LINK', 'Lien:');
define('TEXT_SORT_ORDER', 'Triage:');
define('TEXT_GROUP', 'coID:');
define('TEXT_GROUP_DESC', 'Avec ce ID vous connectez des thèmes similaires des langues différentes.');
 
define('TEXT_CONTENT_DESCRIPTION', 'Le gestionnaire de contenu vous permet d‘ajouter chaque type de fichier possible.<br />P.ex. Description d‘articles, Manuels, fiches techniques, extraits auditifs,… <br /> Ces éléments s‘affichent dans la vue détaillée de l’article.<br /><br />'
);
define('TEXT_FILENAME', 'Fichier utilisé');
define('TEXT_FILE_DESC', 'Description:');
define('USED_SPACE', 'Espace de stockage utilisé');
define('TABLE_HEADING_CONTENT_FILESIZE', 'Taille de fichier');
define('TEXT_CONTENT_NOINDEX', 'noindex (Interdire au robot de recherche d\'ajouter la page web à l\'Index.)');
define('TEXT_CONTENT_NOFOLLOW', 'nofollow (Le robot de recherche a le droit d\'ajouter la page, mais il ne peut pas suivre des hyperliens.)');
define('TEXT_CONTENT_NOODP', 'noodp (Le moteur de recherche ne peut pas utiliser les textes de description DMOZ (ODP) sur la page des résultats.)');
define('TEXT_CONTENT_META_ROBOTS', 'Robots meta');
 
define('TABLE_HEADING_STATUS_ACTIVE', 'Statut');
define('TEXT_STATUS_ACTIVE', 'Statut aktif:');
define('TEXT_STATUS_ACTIVE_DESCRIPTION', 'Activer le contenu?');
  
define('TEXT_CONTENT_DOUBLE_GROUP_INDEX', 'Index des groupes de contenu double! Veuillez sauvegardé encore une fois. Le problème sera résolu automatiquement!');
 
?>

<?php
/* --------------------------------------------------------------
   $Id$


   modified eCommerce Shopsoftware
   http://www.modified-shop.org


   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   Released under the GNU General Public License
   --------------------------------------------------------------*/


define('HEADING_TITLE', 'Suivi des colis de prestataires d\'envoi');


define('TABLE_HEADING_CARRIER_NAME', 'Nom du préstataire d\'envoi');
define('TABLE_HEADING_TRACKING_LINK', 'URL du suivi de colis de pretataire d\'envoi');
define('TABLE_HEADING_SORT_ORDER', 'Ordre de tri');
define('TABLE_HEADING_ACTION', 'Action');
define('TEXT_CARRIER_LINK_DESCRIPTION', '<b>Remarque:</b> L\'URL comporte des caractères de remplacement: <b>$1, $2, $3, $4</b> et <b>$5</b>. Ces caractères de remplacement seront remplacés de la facon suivante:<br/><ul><li>$1: Numéro de suivi du colis</li><li>$2: ISO 639-1 Code de langue</li><li>$3:Jour de création</li><li>$4: Mois de création</li><li>$5: Année de création</li></ul>');
define('TEXT_INFO_EDIT_INTRO', 'Veuillez faire tous les changements nécessaires');
define('TEXT_INFO_CARRIER_NAME', 'Nom du prestataire d\'envoi');
define('TEXT_INFO_CARRIER_TRACKING_LINK', 'URL du suivi de colis:');
define('TEXT_INFO_CARRIER_SORT_ORDER', 'Ordre de tri:');
define('TEXT_INFO_DATE_ADDED', 'ajouté le:');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification:');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez indiquer le nouveau prestataire d\'envoi avec toutes les données pertinentes.');
define('TEXT_INFO_DELETE_INTRO', 'Êtes-vous sûr de vouloir supprimer le prestataire d\'envoi?');
define('TEXT_INFO_HEADING_NEW_CARRIER', 'Nouveau prestataire d\'envoi');
define('TEXT_INFO_HEADING_EDIT_CARRIER', 'Editer le prestataire d\'envoi');
define('TEXT_INFO_HEADING_DELETE_CARRIER', 'Supprimer le prestataire d\'envoi');
define('BUTTON_NEW_CARRIER', 'Nouveau prestataire d\'envoi');
?>

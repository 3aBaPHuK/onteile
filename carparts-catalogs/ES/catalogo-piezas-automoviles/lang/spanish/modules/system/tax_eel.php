<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_TAX_EEL_TEXT_TITLE', 'Tasas de impuesto para servicios prestados por vía electrónica');
define('MODULE_TAX_EEL_TEXT_DESCRIPTION', 'Se insertarán todas las tasas de impuesto de los Estados miembros de la UE.');
define('MODULE_TAX_EEL_TEXT_DESCRIPTION_PROCESSED', 'Ya se han insertado todas las tasas de impuesto de los estados miembros de la UE.<br/><br/>');

?>

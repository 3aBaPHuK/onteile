<?php
/* --------------------------------------------------------------
  $Id$

  modified eCommerce Shopsoftware
  http://www.modified-shop.org

  Copyright (c) 2009 - 2013 [www.modified-shop.org]
  --------------------------------------------------------------
  based on:
  (c) 2013 Falk Wolsky
  
  Released under the GNU General Public License
  --------------------------------------------------------------*/

define('MODULE_EASYBILL_CSV_TEXT_DESCRIPTION', 'Export - easyBill.de<br />Créer une facture en ligne facilement');
define('MODULE_EASYBILL_CSV_TEXT_TITLE', 'easybill - CSV Export');
define('MODULE_EASYBILL_CSV_ORDER_STATUS_DESC', 'avec quel statut de commande souhaitez-vous exporter les commandes ? <br/>(sélectionnez plusieurs options en maintenant la touche majuscule enfoncée)');
define('MODULE_EASYBILL_CSV_ORDER_STATUS_TITLE', '<hr /><strong>Statut de commande</strong>');
define('MODULE_EASYBILL_CSV_CUSTOMER_STATUS_DESC', 'avec quel statut de client souhaitez-vous exporter les commandes ? <br/>(sélectionnez plusieurs options en maintenant la touche majuscule enfoncée)');
define('MODULE_EASYBILL_CSV_CUSTOMER_STATUS_TITLE', '<hr /><strong>Statut de client</strong>');
define('MODULE_EASYBILL_CSV_ORDER_DATE_DESC', 'à partir de quelle date de commande souhaitez-vous exporter les commandes ?');
define('MODULE_EASYBILL_CSV_ORDER_DATE_TITLE', '<strong>Date de commande</strong>');
define('MODULE_EASYBILL_CSV_EXPORT_YES', 'Télécharger uniquement');
define('MODULE_EASYBILL_CSV_EXPORT_NO', 'Enregistrer');
define('MODULE_EASYBILL_CSV_EXPORT_CRON', 'Créer URL Cronjob');
define('MODULE_EASYBILL_CSV_CRON_URL_TITLE', '<hr /><strong>URL pour le Cronjob</strong>');
define('MODULE_EASYBILL_CSV_CRON_URL_DESC', 'Cette URL doit être saisie dans easyBill pour que les commandes seront retirées automatiquement.');
define('MODULE_EASYBILL_CSV_EXPORT', 'N\'interrompez EN AUCUN CAS l\'export. Celui-ci peut durer quelques minutes.');
define('MODULE_EASYBILL_CSV_EXPORT_TYPE', '<hr /><strong>Moyen de sauvegarde :</strong>');
define('MODULE_EASYBILL_CSV_ERROR', 'Pas de commande pour les critères sélectionnés trouvée.');
define('MODULE_EASYBILL_CSV_STANDARD_TAX_CLASS', '1');

?>

<?php
/*******************************************************************************************
* file: /copy_content_into_other_shops.html.php
* (c) noRiddle 12-2015
*******************************************************************************************/

//allowed contents
$c_allowed_form = xtc_draw_form('copy_conts_allowed', FILENAME_MASTER_ADMIN, 'script=copy_content_into_other_shops');
$c_allowed_ipt = xtc_draw_input_field('allowed_copy_conts', (isset($_POST['allowed_copy_conts']) ? trim($_POST['allowed_copy_conts'], ',') : $allowed_cc_arr['copy_allowed_contents']));

//into all shops
$c_form = xtc_draw_form('copy_contents', FILENAME_MASTER_ADMIN, 'script=copy_content_into_other_shops');
$c_shops_drop = xtc_draw_pull_down_menu('cont_grps_from_where', $shop_arr, (isset($_POST['cont_grps_from_where'])  && ($nocont_error === true || $not_allowed_grps_error === true)) ? $_POST['cont_grps_from_where'] : '0');
$c_cnts_from = xtc_draw_input_field('from_grp', (isset($_POST['from_grp']) && ($nocont_error === true || $not_allowed_grps_error === true)) ? (int)$_POST['from_grp'] : '');
$c_cnts_to = xtc_draw_input_field('to_grp', (isset($_POST['to_grp']) && ($nocont_error === true || $not_allowed_grps_error === true)) ? (int)$_POST['to_grp'] : '');

//into single shop
$c_form_singl = xtc_draw_form('copy_contents_singl', FILENAME_MASTER_ADMIN, 'script=copy_content_into_other_shops');
$c_shops_drop_singl = xtc_draw_pull_down_menu('cont_grps_from_where_singl', $shop_arr, (isset($_POST['cont_grps_from_where_singl']) && ($nocont_error === true || $not_allowed_grps_error === true)) ? $_POST['cont_grps_from_where_singl'] : '0');
$c_shops_to_drop_singl = xtc_draw_pull_down_menu('cont_grps_to_where_singl', $shop_arr, (isset($_POST['cont_grps_to_where_singl']) && ($nocont_error === true || $not_allowed_grps_error === true)) ? $_POST['cont_grps_to_where_singl'] : '0');
$c_cnts_from_singl = xtc_draw_input_field('from_grp_singl', (isset($_POST['from_grp_singl']) && ($nocont_error === true || $not_allowed_grps_error === true)) ? (int)$_POST['from_grp_singl'] : '');
$c_cnts_to_singl = xtc_draw_input_field('to_grp_singl', (isset($_POST['to_grp_singl']) && ($nocont_error === true || $not_allowed_grps_error === true)) ? (int)$_POST['to_grp_singl'] : '');

?>
<div class="script-code">
    <h2><?php echo ADMIN_TOOLS_COPY_CONTENTS_TITLE.'<br /><span class="sml">'.ADMIN_TOOLS_COPY_CONTENTS_DESC.'</span>'; ?></h2>
    <div class="div-marg"> 
        <div id="error-copy-conts">
            <?php 
            echo isset($out) && $out != '' ? '<p>'.$out.'</p>' : '';
            echo isset($out_singl) && $out_singl != '' ? '<p>'.$out_singl.'</p>' : '';
            echo isset($ellol) ? '<p>'.$ellol.'</p>' : '';
            echo isset($ellol_singl) ? '<p>'.$ellol_singl.'</p>' : '';
            echo isset($out_allowed) && $out_allowed != '' ? '<p>'.$out_allowed.'</p>' : '';
            ?>
        </div>
    </div>
    <div class="div-marg">
        <div class="bord">
            <h3>Für das Kopieren in andere Shops erlaubte Contents (gemeinsame Contents)</h3>
<?php
        echo '<div class="red">'.ADMIN_TOOLS_COPY_ALLOWED_CONTENTS_GUIDANCE.'</div>'
            .$c_allowed_form
            .'<p>'.$c_allowed_ipt.'</p>'
            .'<p><button type="submit">'.ADMIN_TOOLS_COPY_CONTENTS_SAVE_ALLOWED.'</button></p>'
            .'</form>';
?>
        </div>
    </div>
    <div class="div-marg">
        <div class="bord">
            <h3>Contents kopieren in alle 2.X-Shops</h3>
<?php
            echo '<div class="red">'.ADMIN_TOOLS_COPY_CONTENTS_GUIDANCE.'</div>'
                 .$c_form
                 //BOC checkbox to exclude landing page, 05-2021, noRiddle
                 .'<p>'.ADMIN_TOOLS_COPY_CONTENTS_EXCL_LANDPAGE_TXT.xtc_draw_checkbox_field('excl_landingpage', '1', true).'</p>'
                 //EOC checkbox to exclude landing page, 05-2021, noRiddle
                 .'<p>'.ADMIN_TOOLS_COPY_CONTENTS_FROM_WHICH_SHOP.$c_shops_drop.ADMIN_TOOLS_COPY_CONTENTS_TO_SHOPS.'</p>'
                 .'<p>'.ADMIN_TOOLS_COPY_CONTENTS_FROM_CNT_GRP.$c_cnts_from.ADMIN_TOOLS_COPY_CONTENTS_TO_CNT_GRP.$c_cnts_to.'</p>'
                 .'<p><button type="submit">'.ADMIN_TOOLS_COPY_CONTENTS_SEND.'</button></p>'
                 .'</form>';
?>
        </div>
    </div>
    <div class="div-marg">
        <div class="bord">
            <h3>Contents kopieren in einen 2.X-Shop</h3>
<?php
            echo '<div class="red">'.ADMIN_TOOLS_COPY_CONTENTS_GUIDANCE.'</div>'
                 .$c_form
                 .'<p>'.ADMIN_TOOLS_COPY_CONTENTS_FROM_WHICH_SHOP.$c_shops_drop_singl.ADMIN_TOOLS_COPY_CONTENTS_TO_SHOP_SNGL.$c_shops_to_drop_singl.'</p>'
                 .'<p>'.ADMIN_TOOLS_COPY_CONTENTS_FROM_CNT_GRP.$c_cnts_from_singl.ADMIN_TOOLS_COPY_CONTENTS_TO_CNT_GRP.$c_cnts_to_singl.'</p>'
                 .'<p><button type="submit">'.ADMIN_TOOLS_COPY_CONTENTS_SEND.'</button></p>'
                 .'</form>';
?>
        </div>
    </div>
</div>
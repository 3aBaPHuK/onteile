<?php
/* -----------------------------------------------------------------------------------------
   $Id: paypalcart.php 10597 2017-01-23 18:10:51Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/


$lang_array = array(
'MODULE_PAYMENT_PAYPALCART_TEXT_TITLE' => 'Cesta de compra de PayPal',
'MODULE_PAYMENT_PAYPALCART_TEXT_INFO' => '<img src="https://www.paypal.com/de_DE/DE/i/logo/lockbox_150x47.gif" />',
'MODULE_PAYMENT_PAYPALCART_TEXT_DESCRIPTION' => 'Después de la "confirmación", será redirigido a PayPal para pagar su pedido. <br />Luego será llevado de vuelta a la tienda y recibirá la confirmación de su pedido.<br />Ahora pague más rápido con la protección ilimitada de PayPal para el comprador - gratis, por supuesto.',
'MODULE_PAYMENT_PAYPALCART_ALLOWED_TITLE' => 'Zonas permitidas',
'MODULE_PAYMENT_PAYPALCART_ALLOWED_DESC' => 'Introduzca <b>individualmente</b> las zonas que deben permitirse para este módulo. (por ejemplo, AT,DE (si está vacío, se permiten todas las zonas))',
'MODULE_PAYMENT_PAYPALCART_STATUS_TITLE' => 'Activar el módulo PayPal',
'MODULE_PAYMENT_PAYPALCART_STATUS_DESC' => '¿Desea aceptar pagos a través de PayPal?',
'MODULE_PAYMENT_PAYPALCART_SORT_ORDER_TITLE' => 'Orden de visualización',
'MODULE_PAYMENT_PAYPALCART_SORT_ORDER_DESC' => 'Orden de la visualización. El dígito más pequeño se muestra primero',
'MODULE_PAYMENT_PAYPALCART_ZONE_TITLE' => 'Zona de pago',
'MODULE_PAYMENT_PAYPALCART_ZONE_DESC' => 'Si se ha seleccionado una zona, el método de pago se aplica sólo a esa zona.',
'MODULE_PAYMENT_PAYPALCART_LP' => '<br /><br /><a target="_blank" href="http://www.paypal.com/de/webapps/mpp/referral/paypal-business-account2?partner_id=EHALBVD4M2RQS"><strong>Ahora cree una cuenta PayPal aquí.</strong></a>',

'MODULE_PAYMENT_PAYPALCART_TEXT_EXTENDED_DESCRIPTION' => '<strong><font color="red">ATENCIÓN</font></strong> Por favor, modifique las configuraciones en "Módulo Afiliados" -> "PayPal" -> <a href="'.xtc_href_link('paypal_config.php').'"><strong>"Configuración de PayPal"</strong></a> !',

'MODULE_PAYMENT_PAYPALCART_TEXT_ERROR_HEADING' => 'Aviso',
'MODULE_PAYMENT_PAYPALCART_TEXT_ERROR_MESSAGE' => 'Se ha cancelado el pago de PayPal',
  
'TEXT_PAYPAL_CART_ACCOUNT_CREATED' => 'Hemos creado una cuenta para usted con su dirección de correo electrónico de PayPal. Puede solicitar la contraseña de su nueva cuenta de cliente más tarde a través de la función "Contraseña olvidada".',
);


foreach ($lang_array as $key => $val) {
  defined($key) or define($key, $val);
}
?>

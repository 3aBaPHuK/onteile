<?php
  /* --------------------------------------------------------------
   $Id: manufacturers.php 5850 2013-09-30 09:37:43Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(manufacturers.php,v 1.52 2003/03/22); www.oscommerce.com
   (c) 2003	nextcommerce (manufacturers.php,v 1.9 2003/08/18); www.nextcommerce.org
   (c) 2006 XT-Commerce (manufacturers.php 901 2005-04-29)

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Fabricante');

define('TABLE_HEADING_MANUFACTURERS', 'Fabricante');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_HEADING_NEW_MANUFACTURER', 'Nuevo fabricante');
define('TEXT_HEADING_EDIT_MANUFACTURER', 'Editar fabricante');
define('TEXT_HEADING_DELETE_MANUFACTURER', 'Borrar fabricante');

define('TEXT_MANUFACTURERS', 'Fabricante:');
define('TEXT_DATE_ADDED', 'agregado el:');
define('TEXT_LAST_MODIFIED', 'Última modificación el:');
define('TEXT_PRODUCTS', 'Artículo:');
define('TEXT_IMAGE_NONEXISTENT', 'IMAGEN INEXISTENTE');

define('TEXT_NEW_INTRO', 'Por favor, introduzca el nuevo fabricante con todos los datos relevantes.');
define('TEXT_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');

define('TEXT_MANUFACTURERS_NAME', 'Nombre del fabricante:');
define('TEXT_MANUFACTURERS_IMAGE', 'Imagen del fabricante:');
define('TEXT_MANUFACTURERS_URL', 'URL del fabricante:');

define('TEXT_DELETE_INTRO', '¿Está seguro de que quiere eliminar este fabricante?');
define('TEXT_DELETE_IMAGE', '¿Borrar imagen del fabricante?');
define('TEXT_DELETE_PRODUCTS', '¿Borrar todos los artículos de este fabricante? (incluidas reseñas, ofertas y nuevas publicaciones).');
define('TEXT_DELETE_WARNING_PRODUCTS', '<b>ADVERTENCIA:</b> ¡Todavía existen %s artículos que están vinculados a este fabricante!');

define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Error: El directorio %s está protegido contra escritura. ¡Por favor, corrija los derechos de acceso a este directorio!');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Error: ¡El directorio %s no existe!');

define('TEXT_CHARACTERS', 'Carácter');
define('TEXT_META_TITLE', 'Meta Title:');
define('TEXT_META_DESCRIPTION', 'Meta Description:');
define('TEXT_META_KEYWORDS', 'Meta Keywords:');
define('TEXT_MANUFACTURERS_DESCRIPTION', 'Descripción del fabricante:');
define('TEXT_DELETE', 'Borrar');
?>

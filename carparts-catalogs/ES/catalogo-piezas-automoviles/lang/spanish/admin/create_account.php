<?php
/* --------------------------------------------------------------
   $Id: create_account.php 985 2005-06-17 22:35:22Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(create_account.php,v 1.13 2003/05/19); www.oscommerce.com 
   (c) 2003	 nextcommerce (create_account.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('NAVBAR_TITLE', 'Crear cuenta');

define('HEADING_TITLE', 'Cuenta del cliente Admin');

define('TEXT_ORIGIN_LOGIN', '<span class="col-red"><small><b>ATENCIÓN:</b></small></span> Si ya tiene una cuenta, por favor inicie sesión <a href="%s"><u><b>aquí</b></u></a> .');

define('EMAIL_SUBJECT', 'Bienvenidos en ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Estimado Señor ' . stripslashes($HTTP_POST_VARS['lastname']) . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Estimada Señora ' . stripslashes($HTTP_POST_VARS['lastname']) . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Estimados ' . stripslashes($HTTP_POST_VARS['firstname']) . ',' . "\n\n");
define('EMAIL_WELCOME', 'Bienvenidos en <b>' . STORE_NAME . '</b>.' . "\n\n");
define('EMAIL_TEXT', 'Ahora puede utilizar nuestro <b>Servicio en línea</b> . El servicio ofrece, entre otros, :' . "\n\n" . '<li><b>Cesta de compra del cliente</b> - Cada artículo permanece registrado ahí hasta que pague en la caja o elimine los artículos de la cesta de compra. ' . "\n" . '<li><b>Libreta de direcciones</b> - Ahora podemos enviar los artículos a la dirección que haya seleccionado. La forma perfecta de enviar un regalo de cumpleaños.' . "\n" . '<li><b>Pedidos anteriores</b> - Puede revisar sus pedidos anteriores en cualquier momento.' . "\n" . '<li><b>Opiniones sobre artículos</b> - Comparta su opinión sobre nuestros artículos con otros clientes.' . "\n\n");
define('EMAIL_CONTACT', 'Si tiene alguna pregunta sobre nuestro servicio de atención al cliente, póngase en contacto con nosotros: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('EMAIL_WARNING', '<b>Atención:</b> Esta dirección de correo electrónico nos fue dada por un cliente. Si no se ha registrado, por favor envíe un correo electrónico a  ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n");
define('ENTRY_PAYMENT_UNALLOWED', 'Módulos de pago no permitidos:');
define('ENTRY_SHIPPING_UNALLOWED', 'Módulos de envío no permitidos:');
?>

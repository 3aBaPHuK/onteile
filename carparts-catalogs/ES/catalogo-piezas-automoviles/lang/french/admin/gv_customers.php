<?php
/* -----------------------------------------------------------------------------------------
   $Id:$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Crédit du client');

define('TABLE_HEADING_GV_ID', 'ID');
define('TABLE_HEADING_GV_NAME', 'Nom');
define('TABLE_HEADING_GV_AMOUNT', 'Crédit');
define('HEADING_TITLE_TOTAL', 'Crldit total de tous les clients:');
?>

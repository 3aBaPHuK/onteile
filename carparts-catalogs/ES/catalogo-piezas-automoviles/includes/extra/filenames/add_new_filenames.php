<?php
/******************************************
* file: add_new_filenames.php
* use: define constants for new filenames
* (c) noRiddle 04-2017
******************************************/

//BOC define own search result page, not possible with auto_include, noRiddle
define('FILENAME_NR_ADVANCED_SEARCH_RESULT', 'search_results.php');
//EOC define own search result page, not possible with auto_include, noRiddle
//new catalogue shopping cart
define('FILENAME_CATALOGUE_SHOPPING_CART', 'catalogue_shopping_cart.php');
?>
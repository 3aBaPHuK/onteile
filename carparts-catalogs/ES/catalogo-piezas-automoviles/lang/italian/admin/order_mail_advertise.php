<?php
define('BOX_ORDER_MAIL_ADVERTISE', 'Advertise in order mail');
define('TEXT_DISPLAY_NUMBER_OF_ORDER_MAIL_ADVERTISE', 'Displayed are <b>%d</b> to <b>%d</b> (of total <b>%d</b> advertising texts)');

define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_ID', 'ID');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_TITLE', 'Advertising title');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_TEXT', 'Advertising text');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Aktion');

define('TEXT_INFO_EDIT_INTRO', 'Bitte f&uuml;hren Sie alle notwendigen &Auml;nderungen durch');
define('TEXT_INFO_INSERT_ORDER_MAIL_ADVERTISE_INTRO', 'Please insert a new advertising text');
define('TEXT_INFO_DELETE_ORDER_MAIL_ADVERTISE_INTRO', 'Are you sure you want to delete this advertising text?');
define('TEXT_INFO_HEADING_NEW_ORDER_MAIL_ADVERTISE', 'New advertising text');
define('TEXT_INFO_HEADING_EDIT_ORDER_MAIL_ADVERTISE', 'Edit advertising text');
define('TEXT_INFO_HEADING_DELETE_ORDER_MAIL_ADVERTISE', 'Delete advertising text');
?>
<?php
/* --------------------------------------------------------------
   $Id: customers.php 10228 2016-08-10 16:37:45Z GTB $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(customers.php,v 1.13 2002/06/15); www.oscommerce.com 
   (c) 2003 nextcommerce (customers.php,v 1.8 2003/08/15); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Clientes');
define('HEADING_TITLE_SEARCH', 'Búsqueda:');

define('TABLE_HEADING_CUSTOMERSCID', 'Número de cliente');
define('TABLE_HEADING_FIRSTNAME', 'Nombre');
define('TABLE_HEADING_LASTNAME', 'Apellido');
define('TABLE_HEADING_ACCOUNT_CREATED', 'creado el');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_DATE_ACCOUNT_CREATED', 'Acceso creado el');
define('TEXT_DATE_ACCOUNT_LAST_MODIFIED', 'última modificación:');
define('TEXT_INFO_DATE_LAST_LOGON', 'último inicio de sesión:');
define('TEXT_INFO_NUMBER_OF_LOGONS', 'Número de inicios de sesión:');
define('TEXT_INFO_COUNTRY', 'País:');
define('TEXT_INFO_NUMBER_OF_REVIEWS', 'Número de reseñas de productos:');
define('TEXT_DELETE_INTRO', '¿Realmente quiere borrar a este cliente?');
define('TEXT_DELETE_REVIEWS', 'Borrar %s revisión(es)');
define('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'Borrar cliente');
define('TYPE_BELOW', 'Por favor, introduzca abajo');
define('PLEASE_SELECT', 'Seleccionar');
define('HEADING_TITLE_STATUS', 'Grupo de clientes');
define('TEXT_ALL_CUSTOMERS', 'Todos los grupos');
define('TEXT_INFO_HEADING_STATUS_CUSTOMER', 'Grupo de clientes');
define('TABLE_HEADING_NEW_VALUE', 'Nuevo estado');
define('TABLE_HEADING_DATE_ADDED', 'Fecha');
define('TEXT_NO_CUSTOMER_HISTORY', '--Sin modificación hasta ahora--');
define('TABLE_HEADING_GROUPIMAGE', 'Icon');
define('ENTRY_MEMO', 'Comentario');
define('TEXT_DATE', 'Fecha');
define('TEXT_TITLE', 'Título');
define('TEXT_POSTER', 'Autor');
define('ENTRY_PASSWORD_CUSTOMER', 'Contraseña:');
define('TABLE_HEADING_ACCOUNT_TYPE', 'Cuenta');
define('TEXT_ACCOUNT', 'Sí');
define('TEXT_GUEST', 'No');
define('NEW_ORDER', '¿Nuevo pedido?');
define('ENTRY_PAYMENT_UNALLOWED', 'Módulos de pago no permitidos:');
define('ENTRY_SHIPPING_UNALLOWED', 'Módulos de envío no permitidos:');
define('ENTRY_NEW_PASSWORD', 'Nueva contraseña:');

// NEU HINZUGEFUEGT 04.12.2008 - UMSATZANZEIGE BEI KUNDEN 03.12.2008
define('TABLE_HEADING_UMSATZ', 'Ventas');

// BOF - web28 - 2010-05-28 - added  customers_email_address
define('TABLE_HEADING_EMAIL', 'Correo electrónico');
// EOF - web28 - 2010-05-28 - added  customers_email_address

define('TEXT_INFO_HEADING_ADRESS_BOOK', 'Libreta de direcciones');
define('TEXT_INFO_DELETE', '<b>¿Borrar esta entrada de la libreta de direcciones?</b>');
define('TEXT_INFO_DELETE_DEFAULT', '<b>¡No se puede borrar esta entrada de la libreta de direcciones!</b>');

define('TABLE_HEADING_AMOUNT', 'Saldo');
define('WARNING_CUSTOMER_ALREADY_EXISTS', 'El grupo de clientes no se puede modificar. Esta dirección de correo electrónico ya se utiliza para una cuenta de cliente.');

define('TEXT_SORT_ASC', 'ascendente');
define('TEXT_SORT_DESC', 'descendente');

define('TEXT_INFO_HEADING_STATUS_NEW_ORDER', 'Nuevo pedido');
define('TEXT_INFO_PAYMENT', 'Modalidad de pago:');
define('TEXT_INFO_SHIPPING', 'Modo de envío:');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_EASYMARKETING_TEXT_DESCRIPTION', 'Módulo - Easymarketing.de');
define('MODULE_EASYMARKETING_TEXT_TITLE', 'Easymarketing.de');
define('MODULE_EASYMARKETING_STATUS_TITLE', 'Estado');
define('MODULE_EASYMARKETING_STATUS_DESC', 'Estado del módulo') ;
define('MODULE_EASYMARKETING_SHOP_TOKEN_TITLE', 'Shop Token') ;
define('MODULE_EASYMARKETING_SHOP_TOKEN_DESC', 'Este debe ser depositado con Easymarketing.<br/><br/><b>Aviso:</b> El token debe tener 32 caracteres. Si no es así, se generará automáticamente.');
define('MODULE_EASYMARKETING_ACCESS_TOKEN_TITLE', 'Access Token') ;
define('MODULE_EASYMARKETING_ACCESS_TOKEN_DESC', 'Este token lo obtiene de Easymarketing.');
define('MODULE_EASYMARKETING_CONDITION_DEFAULT_TITLE', 'Condición del artículo') ;
define('MODULE_EASYMARKETING_CONDITION_DEFAULT_DESC', 'Por favor, indique la condición de los artículos.');
define('MODULE_EASYMARKETING_AVAILIBILLITY_STOCK_0_TITLE', 'Disponibilidad - Stock < 1') ;
define('MODULE_EASYMARKETING_AVAILIBILLITY_STOCK_0_DESC', 'Por favor, indique la disponibilidad de los artículos que tienen un stock negativo en la tienda o un stock de 0.');
define('MODULE_EASYMARKETING_AVAILIBILLITY_STOCK_1_TITLE', 'Disponibilidad - Stock > 0') ;
define('MODULE_EASYMARKETING_AVAILIBILLITY_STOCK_1_DESC', 'Por favor, indique la disponibilidad de los artículos que tienen un stock positivo en la tienda.');

?>

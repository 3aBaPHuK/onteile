<?php
/* -----------------------------------------------------------------------------------------
   
   $Id: sitemaporg.php 
   XML-Sitemap.org for xt:Commerce SP2.1a
   by Mathis Klooss
   V1.2
   -----------------------------------------------------------------------------------------
      Original Script:
   $Id: gsitemaps.php 
   Google Sitemaps by hendrik.koch@gmx.de
   V1.1 August 2006
   -----------------------------------------------------------------------------------------
   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com 
   (c) 2003	 nextcommerce (invoice.php,v 1.6 2003/08/24); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SITEMAPORG_TEXT_DESCRIPTION', 'Vous trouverez la définition standard ici : <a href="http://www.sitemaps.org/" target="_blank">www.sitemap.org</a>');
define('MODULE_SITEMAPORG_TEXT_TITLE', 'XML Sitemap.org');
define('MODULE_SITEMAPORG_FILE_TITLE' , '<hr />Nom du fichier');
define('MODULE_SITEMAPORG_FILE_DESC' , 'Entrez un nom du fichier si vous souhaitez enregistrer le fichier d\'export sur le serveur.<br />(registre "export/")');
define('MODULE_SITEMAPORG_STATUS_DESC', 'Statut du module');
define('MODULE_SITEMAPORG_STATUS_TITLE', 'Statut');
define('MODULE_SITEMAPORG_CHANGEFREQ_TITLE', 'Fréquence de changement');
define('MODULE_SITEMAPORG_CHANGEFREQ_DESC', 'La fréquence dans laquelle le site changera probablement.');
define('MODULE_SITEMAPORG_ROOT_TITLE', '<hr /><b>Installation en racine ?</b>');
define('MODULE_SITEMAPORG_ROOT_DESC', 'Souhaitez-vous enregistrer le fichier du plan du site dans le registre racine ?');
define('MODULE_SITEMAPORG_PRIORITY_LIST_TITLE', '<b>Priorité pour la liste</b>');
define('MODULE_SITEMAPORG_PRIORITY_LIST_DESC', '');
define('MODULE_SITEMAPORG_PRIORITY_PRODUCT_TITLE', '<b>Priorité pour les produits</b>');
define('MODULE_SITEMAPORG_PRIORITY_PRODUCT_DESC', '');
define('MODULE_SITEMAPORG_GZIP_TITLE', '<b>Utiliser compression gzip ?</b>');
define('MODULE_SITEMAPORG_GZIP_DESC', 'Le suffixe ".gz" sera mis automatiquement à la fin du fichier');
define('MODULE_SITEMAPORG_EXPORT_TITLE', '<hr /><b>Télécharger ?</b>');
define('MODULE_SITEMAPORG_EXPORT_DESC', 'Souhaitez-vous télécharger le fichier ?');
define('MODULE_SITEMAPORG_YAHOO_TITLE', 'ID Yahoo');
define('MODULE_SITEMAPORG_YAHOO_DESC', 'Saisissez votre ID Yahoo ! Celui-ci est nécessaire pour transmettre le plan du site à Yahoo.');

?>

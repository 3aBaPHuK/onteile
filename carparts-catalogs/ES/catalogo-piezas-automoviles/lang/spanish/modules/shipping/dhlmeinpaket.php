<?php
/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id: dhl_meinpaket.php -1   $
 *
 * (c) 2010 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the GNU General Public License v2 or later
 * -----------------------------------------------------------------------------
 */

define('MODULE_SHIPPING_DHLMEINPAKET_TEXT_TITLE', 'Envío con DHL (MeinPaket)');
define('MODULE_SHIPPING_DHLMEINPAKET_TEXT_DESCRIPTION', 'Envío con DHL establecido por MeinPaket');
define('MODULE_SHIPPING_DHLMEINPAKET_SORT_ORDER', 'Clasificación');
define('MODULE_SHIPPING_DHLMEINPAKET_TEXT_WAY', MODULE_SHIPPING_DHLMEINPAKET_TEXT_DESCRIPTION);
define('MODULE_SHIPPING_DHLMEINPAKET_ALLOWED_TITLE' , 'Zonas permititdas');
define('MODULE_SHIPPING_DHLMEINPAKET_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío.');
define('MODULE_SHIPPING_DHLMEINPAKET_STATUS_TITLE', 'Determinación de envío mediante MeinPaket');
define('MODULE_SHIPPING_DHLMEINPAKET_STATUS_DESC', '¿Desea ofrecer la determinación de envío a través de DHL MeinPaket?');
define('MODULE_SHIPPING_DHLMEINPAKET_SORT_ORDER_TITLE', 'Orden de clasificación');
define('MODULE_SHIPPING_DHLMEINPAKET_SORT_ORDER_DESC', 'Orden de la visualización');

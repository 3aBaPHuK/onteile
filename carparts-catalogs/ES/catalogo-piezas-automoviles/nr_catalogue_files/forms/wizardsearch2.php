<?php
echo '<h2>'.CommonExtender::LocalizeString('Search by wizard').'<br /><span class="h-sml">'.CommonExtender::LocalizeString('Search by wizard sub').'</span></h2>';

include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'render'.DIRECTORY_SEPARATOR.'wizard2'.DIRECTORY_SEPARATOR.'wizard.php');

class WizardExtender extends CommonExtender {
    function FormatLink($type, $dataItem, $catalog, $renderer) {
        if ($type == 'vehicles') {
            //BOC set link to content 1001 which includes vehicles.php, noRiddle
            //return 'vehicles.php?ft=findByWizard2&c='.$catalog.'&ssd='.$renderer->wizard->row['ssd'];
            return xtc_href_link(FILENAME_CONTENT, 'coID=1001').'?ft=findByWizard2&c='.$catalog.'&ssd='.$renderer->wizard->row['ssd'];
            //return xtc_href_link(FILENAME_CONTENT, 'coID=1001&ft=findByWizard2&c='.$catalog.'&ssd='.$renderer->wizard->row['ssd']);
            //EOC set link to content 1001 which includes vehicles.php, noRiddle
        } else {
            //BOC set link to content 1002 which includes wizard2.php, noRiddle
            //return 'wizard2.php?c='.$catalog.'&ssd=$ssd$';
            return xtc_href_link(FILENAME_CONTENT, 'coID=1002').'?c='.$catalog.'&ssd=$ssd$';
            //return xtc_href_link(FILENAME_CONTENT, 'coID=1002&c='.$catalog.'&ssd=$ssd$');
            //EOC set link to content 1002 which includes wizard2.php, noRiddle
        }
    }
}

class GuayaquilWizard2 extends GuayaquilWizard {
	function DrawConditionDescription($catalog, $condition) {
		return '';
	}

	function DrawVehiclesListLink($catalog, $wizard) {
		return '';
	}
}

$renderer = new GuayaquilWizard2(new WizardExtender());
echo $renderer->Draw($_GET['c'], $wizard);

//echo '<br><br>'; //commented out, noRiddle

?>


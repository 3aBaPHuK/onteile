<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_show_banner.php 40 2013-01-08 16:36:44Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_show_banner.php, v 1.15 2003/08/18);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('TABLE_AFFILIATE_BANNERS_HISTORY', 'affiliate_banners_history');
define('TABLE_AFFILIATE_BANNERS', 'affiliate_banners');
define('TABLE_PRODUCTS', 'products');

require('includes/configure.php');

// include used functions
require_once(DIR_FS_INC . 'xtc_db_connect.inc.php');
require_once(DIR_FS_INC . 'xtc_db_close.inc.php');
require_once(DIR_FS_INC . 'xtc_db_query.inc.php');
require_once(DIR_FS_INC . 'xtc_db_fetch_array.inc.php');

// make a connection to the database... now
xtc_db_connect() or die('Unable to connect to database server!');

function affiliate_show_banner($pic) {
	$pic_data = getimagesize($pic);
	$img_type = $pic_data[2];
	
	//Read Pic and send it to browser
    $fp = fopen($pic, "rb");
    if (!$fp) exit();
    
    // Get Imagename
    $pos = strrpos($pic, "/");
    if ($pos) {
    	$img_name = substr($pic, strrpos($pic, "/" ) + 1);
    }
	else {
		$img_name=$pic;
    }
    
    header("Content-type: image/$img_type");
    header("Content-Length: " . filesize($pic));
    header("Content-Disposition: inline; filename=$img_name");
    fpassthru($fp);
    exit();
}

$banner = '';
$products_id = '';
$banner_id ='';
$prod_banner_id = '';
// Register needed Post / Get Variables
if (isset($_GET['ref'])) $affiliate_id = (int)$_GET['ref'];
if (isset($_POST['ref'])) $affiliate_id = (int)$_POST['ref'];

if (isset($_GET['affiliate_banner_id'])) $banner_id = (int)$_GET['affiliate_banner_id'];
if (isset($_POST['affiliate_banner_id'])) $banner_id = (int)$_POST['affiliate_banner_id'];
if (isset($_GET['affiliate_pbanner_id'])) $prod_banner_id = (int)$_GET['affiliate_pbanner_id'];
if (isset($_POST['affiliate_pbanner_id'])) $prod_banner_id = (int)$_POST['affiliate_pbanner_id'];

if (!empty($banner_id)) {
	$is_banner = 'true';
    $sql = "select affiliate_banners_image, affiliate_products_id from " . TABLE_AFFILIATE_BANNERS . " where affiliate_banners_id = " . $banner_id  . " and affiliate_status = 1";
    $banner_values = xtc_db_query($sql);
    if ($banner_array = xtc_db_fetch_array($banner_values)) {
    	$banner = $banner_array['affiliate_banners_image'];
    	$products_id = $banner_array['affiliate_products_id'];
    }
}

if (!empty($prod_banner_id)) {
	$is_banner = 'false';
    $banner_id = 1; // Banner ID for these Banners is one
    $sql = "select products_image from " . TABLE_PRODUCTS . " where products_id = '" . $prod_banner_id  . "' and products_status = 1";
    $banner_values = xtc_db_query($sql);
    if ($banner_array = xtc_db_fetch_array($banner_values)) {
    	$banner = $banner_array['products_image'];
    	$products_id = $prod_banner_id;
    }
}

if ($banner) {
	if($is_banner == 'true') {
		$pic = DIR_FS_CATALOG . DIR_WS_IMAGES . $banner;
	}
	else {
		$pic = DIR_FS_CATALOG . DIR_WS_THUMBNAIL_IMAGES . $banner;
	}

    // Show Banner only if it exists:
    if(!file_exists($pic)) {
		$pic = DIR_FS_CATALOG . DIR_WS_IMAGES . 'pixel_trans.gif';
	} else {
		$today = date('Y-m-d');
		// Update stats:
		
		if (isset($affiliate_id)) {
			$banner_stats_query = xtc_db_query("select * from " . TABLE_AFFILIATE_BANNERS_HISTORY . " where affiliate_banners_id = '" . $banner_id  . "' and affiliate_banners_products_id = '" . $products_id ."' and affiliate_banners_affiliate_id = '" . $affiliate_id. "' and affiliate_banners_history_date = '" . $today . "'");
			// Banner has been shown today
			if ($banner_stats_array = xtc_db_fetch_array($banner_stats_query)) {
				xtc_db_query("update " . TABLE_AFFILIATE_BANNERS_HISTORY . " set affiliate_banners_shown = affiliate_banners_shown + 1 where affiliate_banners_id = '" . $banner_id  . "' and affiliate_banners_affiliate_id = '" . $affiliate_id. "' and affiliate_banners_products_id = '" . $products_id ."' and affiliate_banners_history_date = '" . $today . "'");
			}
			else { // First view of Banner today
	      		xtc_db_query("insert into " . TABLE_AFFILIATE_BANNERS_HISTORY . " (affiliate_banners_id, affiliate_banners_products_id, affiliate_banners_affiliate_id, affiliate_banners_shown, affiliate_banners_history_date) VALUES ('" . $banner_id  . "', '" .  $products_id ."', '" . $affiliate_id. "', '1', '" . $today . "')");
	      	}
	    }
	}
    
    // Show Banner
    affiliate_show_banner($pic);
}

echo "<br>"; // Output something to prevent endless loading

exit();
?>

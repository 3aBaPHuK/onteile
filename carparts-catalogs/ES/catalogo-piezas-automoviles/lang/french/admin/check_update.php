<?php
/* --------------------------------------------------------------
   $Id: check_update.php 10383 2016-11-07 08:48:16Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(customers.php,v 1.13 2002/06/15); www.oscommerce.com
   (c) 2003 nextcommerce (customers.php,v 1.8 2003/08/15); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Actualisation du logiciel');
define('HEADING_SUBTITLE', 'Contrôle mise à jour');

define('TEXT_DB_VERSION', 'Version de la banque des données:');
define('TEXT_INFO_UPDATE_RECOMENDED', '<div class="error_message">Une nouvelle version est valable. Vous pouvez la télécharger ici : <a href="http://www.modified-shop.org/download" target="_blank">http://www.modified-shop.org/download</a></div>');
define('TEXT_INFO_UPDATE_NOT_POSSIBLE', '<div class="error_message">Malheureusement pas de vérification était possible. Veuillez visiter notre <a target="_blank" href="http://www.modified-shop.org"><b>site web</b></a>.</div>');
define('TEXT_INFO_UPDATE', '<div class="success_message">Votre version est actuelle.</div>');

define('TEXT_HEADING_DEVELOPERS', 'Développeur de la modified eCommerce Shopsoftware:');
define('TEXT_HEADING_SUPPORT', 'Soutenez le développement:');
define('TEXT_HEADING_DONATIONS', 'Donner:');
define('TEXT_HEADING_BASED_ON', 'Le logiciel de la boutique est basé sur:');

define('TEXT_INFO_THANKS', 'Nous remercions tous les programmeurs et développeurs qui ont contribué à ce projet. Si nous avons oublié qulequ‘un dans le listage ci-dessous, veuillez nous informer via le <a style="font-size: 12px; text-decoration: underline;" href="http://www.modified-shop.org/forum/" target="_blank">forum</a> ou un des développeurs mentionnés ci-dessus.');
define('TEXT_INFO_DISCLAIMER', 'Ce programme a été publié dans l‘espoir d‘être aidant. Toutefois nous ne garantissons aucune mise en œuvre parfaite.');
define('TEXT_INFO_DONATIONS', 'La modified eCommerce Shopsoftware est un projet OpenSource – Nous investissons beaucoup de travail et de temps libre dans ce projet. Quel que soit son montant, chaque don est hautement apprécié.');
define('TEXT_INFO_DONATIONS_IMG_ALT', 'Soutenez ce projet avec votre don');
define('BUTTON_DONATE', '<a href="http://www.modified-shop.org/spenden"><img src="https://www.paypal.com/de_DE/DE/i/btn/btn_donateCC_LG.gif" alt="' . TEXT_INFO_DONATIONS_IMG_ALT . '" border="0"></a>');
?>

<?php
/***********************************************************************************************************
* file: langs_edit.php
* use: language file for /Admin/langs_edit.php
*
* (c) noRiddle 06-2018
***********************************************************************************************************/

define('NR_LANGS_EDIT_GUIDE_HEADING', 'Guide');
define('NR_LANGS_EDIT_GUIDE_TXT', '<ul>
                                  <li><u>Préparation:</u>
                                        <ul>
                          <li>Choisir la langue de base et la télécharger dans la base de données.<br />
                                            !! Cette langue de base ne doit plus être changée si on a déjà édité et enregistré des langues. Sinon l\'attribution des lignes n\'est pas possible !!</li>
                                            <li>Choisir les langues d\'aide et les télécharger dans la base de données.<br />
                                                Chaque langue que vous voulez éditer doit exister comme répértoire dans <i>/lang/helpfiles/</i> avec au moins un fichier. S\'il n\'y a pas de modèle, veuillez simplement charger un ficher d\'une autre langue dans ce répértoire.</li>
                                        </ul>
                                    </li>
                                    <li><span class="red"><u>A respecter impérativement!!</u></span>
                                        <ul>
                                            <li>Les guillements simples doivent être conservés impéraivement.<br />
                                           S\'ils font parti du texte (par exemple en anglais "don\'t") ils doivent être escape-t, ce qui veut dire, qu\'ils doivent être précédés par une barre oblique inversée.<br />
                                            Cela se montre de la facon suivante: "don\'t" oder "d\'accord" .</li>
                                            <li> Le langage HTML et d\'autres caractères spéciaux qui font parti de PHP doivent également être impérativement conservés.<br />
                                           Avec un peu d\'expérience on peut voir ce qui fait parti du texte (doit donc être traduit) et ce qui fait parti du code de programmation. (HTML, Javascript ou PHP).</li>
                                       </ul>
                                    </li>
                                   <li><u>Sauvegarder:</u>
                                        <ul>
                                            <li>Quand les langues sont ouvertes pour l\'édition elles sont sauvegardés automatiquement toutes les 10 minutes pour éviter l\'expiration de la session ou la déconnexion.</li>
                                            <li> Lors de l\'enregistrement des langues édités celles-ci sont enregistrés dans la base de données avant tout.<br />
                                            Les données appartenantes sont générées seulement lorsque vous cliquez sur "Générer les données de langue pour le fichier séléctionné". Elles sont ensuite enregistrées dans <i>/lang/translated/</i> un dossier qui porte le nom de la langue.</li>
                                        </ul>
                                    </li>
                                  </ul>'
);

define('NR_LANGS_EDIT_LOAD_LANGS_HEADING', 'Télécharger les langues dans la base de données');
define('NR_LANGS_EDIT_LOAD_LANGS_FILETREE_HEADING', 'Arbre de données');
define('NR_LANGS_EDIT_LOAD_BASIS_LANG', 'Charger la langue de base désirée dans la base de données:');
define('NR_LANGS_EDIT_WARNING_LOAD_BASIS_JS', 'Voulez-vous vraiment recharger la langue de base?');
define('NR_LANGS_EDIT_LOAD_HELPER_LANG', 'Charger les langues à éditer dans la base de données:');
define('NR_LANGS_EDIT_WARN_NO_BASIS_LANG', 'Vous devez d\'abord charger une langue de base!');
define('NR_LANGS_EDIT_WARN_HELP_DIR_NOT_EXISTS', 'Le répértoire <i>/lang/helpfiles/</i> n\'existe pas.<br /> Si des modèles de fichiers de langue existent veuillez les charger dans <i>/lang/helpfiles/LANGUE/</i>.<br /> Pour chaque langue remplacez LANGUE par l\'appellation anglaise en minuscules. (Par exemple: /lang/helpfiles/italian/  pour l\'italien)');
define('NR_LANGS_EDIT_LOAD_EXIST_LANG', 'Charger une langue déjà existante dans la base de données:');
define('NR_LANGS_EDIT_LOADED_TXT', 'Chargé:');
define('NR_LANGS_EDIT_HELPINGLANG_TXT', 'Langue d\'aide');
define('NR_LANGS_EDIT_EXISTINGLANG_TXT', 'existe déjà en direct');

define('NR_LANGS_EDIT_ATTENTION_READ_TXT', 'ATTENTION. Veuillez lire attentivement !!');
define('NR_LANGS_EDIT_EXPL_LOAD_BASIC_TXT', '!! Ne changez pas si d\'autres langues ont déjà été éditées !!<br />
    La langue séléctionée sera complètement reprogrammée.<br />
    Les langues à éditer doivent être rechargées après avoir rechargé la langue de base pour associer d\'autres langues correctement.<br />
    !! Les données déjà enregistrées seront perdues et ne pourront pas être associées !!');
define('NR_LANGS_EDIT_EXPL_LOAD_HELPER_TXT', 'Des répértoires pour d\'autres langues qui ont été téléchargées comme modules de langues par exemple par modified se situent dans le répértoire <i>/lang/helpfiles/</i>..<br />
    Ceux-la peuvent servir comme modèle pour la traduction.<br />
           Chaque valeur est associée programmatiquement à la valeur correspondante de la langue de base chargée à gauche lors du chargement dans la base de données.');
define('NR_LANGS_EDIT_EXPL_LOAD_EXIST_TXT', 'La langue séléctionnée est une langue dont les données existent déjà dans le répértoire en direct et est complètement reprogrammée.<br />
    Plusieures langues d\'affilée peuvent être reprogrammées. Elles peuvent servir comme modèle de traduction.<br />
    !! Veuillez noter qu\'une langue qui est chargée plusieures fois écrase celle qui a été chargée précédemment !!');
define('NR_LANGS_EDIT_EXPL_SHOW_LANGS_TXT', 'La langue chargée comme langue de base en haut est toujours affichée automatiquement en tant que modèle.<br />
    Les langues qui existent déjà en direct sont chargées en tant que modèle et ne peuvent pas être éditées.<br />
    !! Si des traductions personnelles on été enregistrés elles sont automatiquement chargées !!');
define('NR_LANGS_EDIT_EXPL_GENERATE_FILES', 'Vous devez générer les données de la langue éditée pour le fichier séléctionné après la sauvegarde des traductions.');
define('NR_LANGS_EDIT_FILL_EMPTY_WITH_ENGL', 'Remplir les valeurs vides avec les valeurs anglaises?');

define('NR_LANGS_EDIT_LOAD_IN_DB_TXT', 'charger dans la base de données');
define('NR_LANGS_EDIT_SHOW_MARKED_LANGS_TO_EDIT', 'Afficher les langues marquées pour le fichier séléctionné');
define('NR_LANGS_EDIT_SAVE_TRANSL_TO_DB', 'Sauvegarder les changements dans la base de données');
define('NR_LANGS_EDIT_GENERATE_FILES', 'Générer les données de langue pour le fichier séléctionné');

//messagestack
define('NR_LANGS_EDIT_NOLANG_CHOOSEN_OR_NO_DIR', 'Vous n\'avez pas séléctionné de langue ou la langue n\'existe pas comme répértoire!');
define('NR_LANGS_EDIT_CONSTS_LOADED', 'Constantes définies de %s pour %s ont été reprogrammées');
define('NR_LANGS_EDIT_CONFS_LOADED', 'Variables Smarty-Conf définies de %s pour %s ont été reprogrammées');
define('NR_LANGS_EDIT_UNALLOWED_LANGS_MSG', '!! Langue(s) non autorisée(s) ne peut pas être affichée(s) !!');
define('NR_LANGS_EDIT_GENERATED_FILES_SUCCESS', '%s dans %s a été écrit dans <i>/%s</i>.');
?>

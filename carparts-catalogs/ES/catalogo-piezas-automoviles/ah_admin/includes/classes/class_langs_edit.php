<?php
/*********************************************************************
* file: class_langs_edit.php
* use: helper class for /admin/langs_edit.php
*      store language definitions in database and make them editable
* (c) 06-2018 noRiddle
*********************************************************************/

class class_langs_edit { 
    function __construct($messageStack) {
        $this->messageStack = $messageStack;
        $this->mstck = array();
        //$this->basis_path = NR_LT_BASIC_LANG_PATH;
        //$this->translated_path = NR_LT_TRANSL_LANG_PATH;
        $this->has_basic_data = $this->has_basic_data();
        $this->has_langs_exist = $this->has_langs_exist();
        $this->has_langs_help = $this->has_langs_help();
        $this->lang_dir_excl_arr = array('button_templates', 'helpfiles', 'translated');
        $this->file_ext_arr = array('php', 'conf', 'custom');
        $this->dir_excl_arr = array('images', 'sections');
        $this->file_excl_arr = array('masterpayment_callback.php',
                                     'ot_billpay_fee.php',
                                     'ot_billpay_paylater.php',
                                     'ot_billpaybusiness_fee.php',
                                     'ot_billpaydebit_fee.php',
                                     'ot_billpaypaylater_fee.php',
                                     'ot_billpaytc_surcharge.php',
                                     'ot_billsafe.php',
                                     'ot_klarna_fee.php',
                                     'ot_z_bpytc_fee.php',
                                     'ot_z_bpytc_total.php',
                                     'ot_z_paylater_fee.php',
                                     'ot_z_paylater_total.php',
                                     'amazon.php',
                                     'amoneybookers.php',
                                     'billpay.php',
                                     'billpaydebit.php',
                                     'billpaypaylater.php',
                                     'billpaytransactioncredit.php',
                                     'billsafe_2.php',
                                     'chronopost.php',
                                     'ebay.php',
                                     'hitmeister.php',
                                     'klarna_invoice.php',
                                     'klarna_partPayment.php',
                                     'klarna_SpecCamp.php',
                                     'marketplace.php',
                                     'masterpayment_anzahlungskauf.php',
                                     'masterpayment_config.php',
                                     'masterpayment_credit_card.php',
                                     'masterpayment_debit_card.php',
                                     'masterpayment_elv.php',
                                     'masterpayment_finanzierung.php',
                                     'masterpayment_phone.php',
                                     'masterpayment_ratenzahlung.php',
                                     'masterpayment_rechnungskauf.php',
                                     'masterpayment_sofortbanking.php',
                                     'mcp_creditcard.php',
                                     'mcp_debit.php',
                                     'mcp_ebank2pay.php',
                                     'mcp_prepay.php',
                                     'mcp_service.php',
                                     'meinpaket.php',
                                     'moneybookers_cc.php',
                                     'moneybookers_cgb.php',
                                     'moneybookers_csi.php',
                                     'moneybookers_elv.php',
                                     'moneybookers_giropay.php',
                                     'moneybookers_ideal.php',
                                     'moneybookers_mae.php',
                                     'moneybookers_netpay.php',
                                     'moneybookers_psp.php',
                                     'moneybookers_pwy.php',
                                     'moneybookers_sft.php',
                                     'moneybookers_wlt.php',
                                     'payone.php',
                                     'payone_cc.php',
                                     'payone_cod.php',
                                     'payone_elv.php',
                                     'payone_installment.php',
                                     'payone_invoice.php',
                                     'payone_otrans.php',
                                     'payone_paydirekt.php',
                                     'payone_prepay.php',
                                     'payone_wlt.php',
                                     'shopgate.php',
                                     'sofort_ideal.php',
                                     'sofort_payment.php',
                                     'sofort_sofortueberweisung_classic.php',
                                     'sofort_sofortueberweisung_gateway.php',
                                     'worldpay_junior.php',
                                     'yatego.php'
                                    );
    }

    public function prepare_db_basis_table($lang) {
        xtc_db_query("TRUNCATE TABLE nr_lang_transl_paths");
        xtc_db_query("TRUNCATE TABLE nr_lang_transl_files");
        xtc_db_query("TRUNCATE TABLE nr_lang_transl_basis");
        xtc_db_query("TRUNCATE TABLE nr_lang_transl_help");
        xtc_db_query("TRUNCATE TABLE nr_lang_which_loaded");

        xtc_db_query("INSERT INTO nr_lang_which_loaded (nl_lt_base_lang) VALUES('".xtc_db_input($lang)."')");

        //look whether field for language exists amd add it if not
        $lang_cols_arr = array();
        $col_verify_qu = xtc_db_query("SHOW COLUMNS FROM nr_lang_transl_basis LIKE 'nr_lt_value_%'");
        while($col_verify_arr = xtc_db_fetch_array($col_verify_qu)) {
            $lang_cols_arr[] = $col_verify_arr['Field'];
        }

        if(!in_array('nr_lt_value_'.$lang, $lang_cols_arr)) {
            $lang_cols_arr[] = 'nr_lt_value_'.$lang;
            sort($lang_cols_arr);
            foreach($lang_cols_arr as $lk => $col) {
                if($col == 'nr_lt_value_'.$lang) {
                    $aft_col = ($lk == 0 ? 'nr_lt_var' : $lang_cols_arr[$lk-1]);
                }
            }
            xtc_db_query("ALTER TABLE nr_lang_transl_basis ADD nr_lt_value_".$lang." TEXT NOT NULL AFTER ".$aft_col);
        }
    }

    public function prepare_db_help_table($lang, $exists = false) {
        if($exists === true) {
            $has_langs = $this->has_langs_exist;
            if(empty($has_langs) || (!empty($has_langs) && !in_array($lang, $has_langs))) {
                xtc_db_query("UPDATE nr_lang_which_loaded SET nl_lt_ready_langs = CONCAT(nl_lt_ready_langs, '".xtc_db_input($lang).",')");
            }
        } else if($exists === false) {
            $has_langs = $this->has_langs_help;
            if(empty($has_langs) || (!empty($has_langs) && !in_array($lang, $has_langs))) {
                xtc_db_query("UPDATE nr_lang_which_loaded SET nl_lt_help_langs = CONCAT(nl_lt_help_langs, '".xtc_db_input($lang).",')");
            }
        }

        //look whether field for language exists amd add it if not
        $lang_cols_arr = array();
        $col_verify_qu = xtc_db_query("SHOW COLUMNS FROM nr_lang_transl_help LIKE 'nr_lt_value_%'");
        while($col_verify_arr = xtc_db_fetch_array($col_verify_qu)) {
            $lang_cols_arr[] = $col_verify_arr['Field'];
        }

        if(!in_array('nr_lt_value_'.$lang, $lang_cols_arr)) {
            $lang_cols_arr[] = 'nr_lt_value_'.$lang;
            sort($lang_cols_arr);
            foreach($lang_cols_arr as $lk => $col) {
                if($col == 'nr_lt_value_'.$lang) {
                    $aft_col = ($lk == 0 ? 'nr_lt_var' : $lang_cols_arr[$lk-1]);
                }
            }
            xtc_db_query("ALTER TABLE nr_lang_transl_help ADD nr_lt_value_".$lang." TEXT AFTER ".$aft_col);
        }
    }

    public function build_basic_lang_arr($excl = false) {
        if(is_dir(NR_LT_BASIC_LANG_PATH)) {
            $nrl_basic_langs = array();
            $nrl_help_langs = new DirectoryIterator(NR_LT_BASIC_LANG_PATH);
            foreach($nrl_help_langs as $nrl_lang) {
                if($nrl_lang->isDot() === false && !in_array($nrl_lang->getFilename(), $this->lang_dir_excl_arr) && $nrl_lang->isDir()) {
                    if(file_exists(NR_LT_BASIC_LANG_PATH.$nrl_lang->getFilename().'/'.$nrl_lang->getFilename().'.php')) { //only show if LANGUAGE.php exists
                        if($excl === true) {
                            if($this->has_basic_data !== false && $this->has_basic_data != $nrl_lang->getFilename()) {
                                $nrl_basic_langs[$nrl_lang->getFilename()] = $nrl_lang->getFilename();
                            }
                        } else {
                            $nrl_basic_langs[$nrl_lang->getFilename()] = $nrl_lang->getFilename();
                        }
                    }
                }
            }
            asort($nrl_basic_langs);
        }
        return $nrl_basic_langs;
    }

    public function build_helper_lang_arr() {
        if(is_dir(NR_LT_HELP_LANG_PATH)) {
            $nrl_helper_langs = array();
            $nrl_help_langs = new DirectoryIterator(NR_LT_HELP_LANG_PATH);
            foreach($nrl_help_langs as $nrl_lang) {
                if($nrl_lang->isDot() === false && $nrl_lang->isDir()) {
                    $nrl_helper_langs[$nrl_lang->getFilename()] = $nrl_lang->getFilename();
                }
            }
            asort($nrl_helper_langs);
        }
        return $nrl_helper_langs;
    }

    public function dir_lang_iterate($path) {
        $dir_lit = new DirectoryIterator($path);
        $dirs = array();
        $dirs[] = $path.'/';
        foreach ($dir_lit as $dir) {
            if($dir->isDot() === false && $dir->isDir() && !in_array($dir->getFilename(), $this->dir_excl_arr)) {
                $dirs = array_merge($dirs, $this->dir_lang_iterate($dir->getPathname()));
            }
        }
        sort($dirs);

        return $dirs;
    }

    public function basic_load_path($dirp, $bas_lang) {
        $dirp_clean = str_replace($bas_lang, 'LANGUAGE', $dirp);
        $transl_path_sql = "INSERT INTO nr_lang_transl_paths (nr_lt_path) VALUES('".xtc_db_input($dirp_clean)."')";
        if(xtc_db_query($transl_path_sql)) {
            //$this->mstck['succ'][] = 'Der Pfad '.$dirp.' wurde eingelesen.';
            $nr_p_id = xtc_db_insert_id(); // 'pid_test'; //
        }

        return $nr_p_id;
    }

    public function basic_load_file($fk, $dirp, $nr_p_id, $bas_lang) {
        if($nr_p_id != 0) {
            $fk_clean = str_replace($bas_lang, 'LANGUAGE', $fk);
            $transl_file_sql = "INSERT INTO nr_lang_transl_files (nr_lt_path_id, nr_lt_file, nr_lt_date_loaded) VALUES(".$nr_p_id.", '".xtc_db_input($fk_clean)."', now())";
            if(xtc_db_query($transl_file_sql)) {
                //$this->mstck['succ'][] = 'Das File '.$fk.' mit dem Pfad '.$dirp.' wurde eingelesen.';
                $nr_f_id = xtc_db_insert_id(); // 'fid_test'; //
            }
        }

        return $nr_f_id;
    }

    public function basic_load_var($nr_p_id, $nr_f_id, $lk, $val_pairs) {
        if($nr_p_id != 0 && $nr_f_id != 0) {
            $transl_line_var_sql = ($lk > 0 ? ", \n" : "\n")."(".($lk+1).", '".xtc_db_input($val_pairs[0])."', '".xtc_db_input($val_pairs[1])."', '".xtc_db_input($val_pairs[2])."', ".$nr_p_id.", ".$nr_f_id.", now())";
        } else {
            $transl_line_var_sql = '';
        }

        return $transl_line_var_sql;
    }

    public function helper_load_var($dirp, $fk, $lk, $val_pairs, $help_lang, $type) {
        $transl_line_var_sql = '';

        if($val_pairs[0] != 'lne') {
            $helper_line_var_sql = "SELECT nltb.nr_lt_id, nltb.nr_lt_line_no, nltp.nr_lt_path, nltf.nr_lt_file
                                      FROM nr_lang_transl_basis nltb 
                                 LEFT JOIN nr_lang_transl_paths nltp
                                        ON nltp.nr_lt_path_id = nltb.nr_lt_path_id
                                 LEFT JOIN nr_lang_transl_files nltf
                                        ON nltf.nr_lt_file_id = nltb.nr_lt_file_id
                                     WHERE nltb.nr_lt_const_or_conf = '".xtc_db_input($val_pairs[0])."'
                                       AND nltb.nr_lt_var = '".xtc_db_input($val_pairs[1])."'
                                  ORDER BY nltb.nr_lt_id";

            $helper_line_var_qu = xtc_db_query($helper_line_var_sql);
            
            if(xtc_db_num_rows($helper_line_var_qu) > 0) {
                $dirp_clean = strpos($dirp, 'helpfiles/') !== false ? str_replace('helpfiles/', '', $dirp) : $dirp;
                $dirp_clean = strpos($dirp, $help_lang) !== false ? str_replace($help_lang, 'LANGUAGE', $dirp_clean) : $dirp_clean;
                $fk_clean = strpos($fk, $help_lang) !== false ? str_replace($help_lang, 'LANGUAGE', $fk) : $fk;

                while($helper_line_var_arr = xtc_db_fetch_array($helper_line_var_qu)) {
                    if($helper_line_var_arr['nr_lt_path'] == $dirp_clean && $helper_line_var_arr['nr_lt_file'] == $fk_clean) {
                        if($val_pairs[0] == 'ccns') {
                            if($helper_line_var_arr['nr_lt_line_no'] == ($lk+1)) {
                                $transl_line_var_sql .= "\n"."(".$helper_line_var_arr['nr_lt_id'].", '".xtc_db_input($val_pairs[0])."', '".xtc_db_input($val_pairs[1])."', '".xtc_db_input($val_pairs[2])."'),";
                            } else {
                                if($type == 'help') {
                                    $transl_line_var_sql .= "\n"."(".$helper_line_var_arr['nr_lt_id'].", '".xtc_db_input($val_pairs[0])."', '".xtc_db_input($val_pairs[1])."', '!! PRUEFEN ZEILENVERSCHIEBUNG !!'),";
                                }
                            }
                        } else {
                            $transl_line_var_sql .= "\n"."(".$helper_line_var_arr['nr_lt_id'].", '".xtc_db_input($val_pairs[0])."', '".xtc_db_input($val_pairs[1])."', '".xtc_db_input($val_pairs[2])."'),";
                            
                        }
                    }
                }
            }
        }

        return $transl_line_var_sql;
        //echo '<pre>'.$helper_line_var_sql.'</pre>';
        //echo '<pre>'.$transl_line_var_sql.'</pre>';
        //echo '<pre>'.print_r($helper_line_var_arr, true).'</pre>';
    }

    public function has_basic_data() {
        $hbd_qu = xtc_db_query("SELECT nl_lt_base_lang FROM nr_lang_which_loaded");

        if(xtc_db_num_rows($hbd_qu) == 1) {
            $hbd_arr = xtc_db_fetch_array($hbd_qu);
            $base_lang = $hbd_arr['nl_lt_base_lang'];
        } else {
            $base_lang = false;
        }
        
        return $base_lang;
    }

    public function has_langs_help() {
        $exists_qu = xtc_db_query("SELECT nl_lt_help_langs FROM nr_lang_which_loaded");
        $exists_arr = xtc_db_fetch_array($exists_qu);
        if($exists_arr['nl_lt_help_langs'] != '') {
            $have_langs = explode(',', $exists_arr['nl_lt_help_langs']);
            array_pop($have_langs); //last element is '' because we have a comma after last language in DB
        } else {
            $have_langs = array();
        }
        
        return $have_langs;
    }

    public function has_langs_exist() {
        $exists_qu = xtc_db_query("SELECT nl_lt_ready_langs FROM nr_lang_which_loaded");
        $exists_arr = xtc_db_fetch_array($exists_qu);
        if($exists_arr['nl_lt_ready_langs'] != '') {
            $have_langs = explode(',', $exists_arr['nl_lt_ready_langs']);
            array_pop($have_langs); //last element is '' because we have a comma after last language in DB
        } else {
            $have_langs = array();
        }
        
        return $have_langs;
    }

    public function file_dropdown() {
        $file_drop_arr = array();
        $fd_qu = xtc_db_query("SELECT nr_lt_file_id, nr_lt_file FROM nr_lang_transl_files ORDER BY nr_lt_file_id");
        while($fd_arr = xtc_db_fetch_array($fd_qu)) {
            if(!in_array($fd_arr['nr_lt_file'], $this->file_excl_arr)) {
                $file_drop_arr[$fd_arr['nr_lt_file_id']] = $fd_arr['nr_lt_file'];
            }
        }
        
        return $file_drop_arr;
    }

    //display languages to edit
    public function display_langs_to_edit($load_file, $load_foredit) {
        $bas_lng = $this->has_basic_data;

        if($bas_lng !== false) {
            if(isset($load_file) && $load_file != '?') {
                $join_str = '';
                $qu_str = '';
                $res_arr = array();
                
                if(isset($load_foredit) && !empty($load_foredit)) {
                    $join_str .= " LEFT JOIN nr_lang_transl_transl tt ON tt.nr_lt_id = tb.nr_lt_id";
                    $join_str .= " LEFT JOIN nr_lang_transl_help th ON th.nr_lt_id = tb.nr_lt_id";
                }
                
                if(isset($load_foredit) && !empty($load_foredit)) {
                    foreach($load_foredit as $typ => $fored_arr) {
                        foreach($fored_arr as $ln) {
                            if(in_array($ln, $this->has_langs_help) || in_array($ln, $this->has_langs_exist)) {
                                $qu_str .= ", "."\n"."IFNULL(tt.nr_lt_value_".$ln.", th.nr_lt_value_".$ln.") AS ".$typ."_".$ln;
                            } else {
                                $this->mstck['succ'][] = NR_LANGS_EDIT_UNALLOWED_LANGS_MSG;
                            }
                        }
                    }
                }

                $edit_sql = "SELECT tb.nr_lt_id,
                                    tb.nr_lt_line_no,
                                    tb.nr_lt_file_id,
                                    tb.nr_lt_var,
                                    tb.nr_lt_value_".$bas_lng." AS base_".$bas_lng.$qu_str."
                               FROM nr_lang_transl_basis tb
                               ".$join_str."
                              WHERE tb.nr_lt_file_id = ".(int)$load_file."
                                AND tb.nr_lt_const_or_conf != 'lne'
                           ORDER BY tb.nr_lt_id";

                $edit_qu = xtc_db_query($edit_sql);
                
                while($edit_arr = xtc_db_fetch_array($edit_qu)) {
                    $res_array[$edit_arr['nr_lt_id']] = $edit_arr;
                }
            }
        }

        //return $edit_sql;
        return $res_array;
    }

    public function write_transl_vals($post_trans_lang) {
        $cnt_l = 0;
        $write_ins_sql = "INSERT INTO nr_lang_transl_transl (nr_lt_id";
        $temp_write_ins_sql = '';
        $write_vals_sql = "VALUES";
        $has_langs = array();

        foreach($post_trans_lang as $line => $lang_arr) {
            $write_vals_sql .= "(".(int)$line;
            $temp_on_dupli_sql = '';

            foreach($lang_arr as $lang => $val) {
                if($cnt_l === 0) {
                    if($lang != 'nr_lt_line_no' && $lang != 'nr_lt_file_id') {
                        $temp_write_ins_sql .= ", nr_lt_value_".$lang;
                    } else {
                        $temp_write_ins_sql .= ", ".$lang;
                    }
                }
                $write_vals_sql .= ", '".xtc_db_input($val)."'";
                if($lang != 'nr_lt_line_no' && $lang != 'nr_lt_file_id') {
                    $temp_on_dupli_sql .= "nr_lt_value_".$lang." = VALUES(nr_lt_value_".$lang."), ";
                } else {
                    $temp_on_dupli_sql .= $lang." = VALUES(".$lang."), ";
                }
            }
            $write_vals_sql .= "), ";
            
            $cnt_l++;
        }
        $write_ins_sql .= $temp_write_ins_sql.") ";
        
        $write_vals_sql = rtrim($write_vals_sql, ', ');
        $temp_on_dupli_sql = rtrim($temp_on_dupli_sql, ', ');
        
        $on_dupli_sql = " ON DUPLICATE KEY UPDATE ".$temp_on_dupli_sql;
        
        $complete_sql = $write_ins_sql.$write_vals_sql.$on_dupli_sql;
        if(xtc_db_query($complete_sql)) return true;

        //return $complete_sql;
    }

    public function generate_transl_files($file_id, $langs_arr) {
        $file_qu = xtc_db_query("SELECT f.nr_lt_file, p.nr_lt_path
                                   FROM nr_lang_transl_files f
                              LEFT JOIN nr_lang_transl_paths p 
                                     ON p.nr_lt_path_id = f.nr_lt_path_id 
                                  WHERE f.nr_lt_file_id = ".(int)$file_id);
        $file_arr = xtc_db_fetch_array($file_qu);
        
        foreach($langs_arr as $typ => $larr) {
            if($typ != 'exists') {
                foreach($larr as $lng) {
                    $eff_filename = str_replace('LANGUAGE', $lng, $file_arr['nr_lt_file']);
                    $eff_path = str_replace('LANGUAGE/', NR_LT_TRANSL_LANG_PATH.$lng.'/', $file_arr['nr_lt_path']);
                    $full_filepath = $eff_path.$eff_filename;

                    $hve_data_qu = xtc_db_query("SELECT nr_lt_id FROM nr_lang_transl_transl WHERE nr_lt_file_id = ".(int)$file_id);
                    if(xtc_db_num_rows($hve_data_qu)) {
                        $dirs = explode('/', $eff_path);
                        $dir = '';
                        foreach ($dirs as $part) {
                            $dir .= $part.'/';
                            if (!is_dir($dir) && strlen($dir) > 0) {
                                mkdir($dir, 0775);
                            }
                        }

                        $generate_lines_qu = xtc_db_query("SELECT tb.nr_lt_line_no, tb.nr_lt_const_or_conf, tb.nr_lt_var, tb.nr_lt_value_".$this->has_basic_data." AS base_val, tt.nr_lt_value_".$lng."
                                                             FROM nr_lang_transl_basis tb
                                                        LEFT JOIN nr_lang_transl_transl tt
                                                               ON tt.nr_lt_id = tb.nr_lt_id
                                                            WHERE tb.nr_lt_file_id = ".(int)$file_id."
                                                         ORDER BY tb.nr_lt_line_no");

                        if(xtc_db_num_rows($generate_lines_qu) > 0) {
                            $fp = fopen($full_filepath, "w+");
                            $num_res = xtc_db_num_rows($generate_lines_qu);
                            $cnt_lines = 0;

                            while($generate_lines_arr = xtc_db_fetch_array($generate_lines_qu)) {
                                $cnt_lines++;
                                
                                switch($generate_lines_arr['nr_lt_const_or_conf']) {
                                    case 'lne':
                                        $write_val = $generate_lines_arr['base_val'];
                                      break;
                                    case 'sl_cns':
                                        $write_val = 'define('.$generate_lines_arr['nr_lt_var'].', '.$generate_lines_arr['nr_lt_value_'.$lng].');';
                                      break;
                                    case 'ml_cns':
                                        $write_val = 'define('.$generate_lines_arr['nr_lt_var'].', '.$generate_lines_arr['nr_lt_value_'.$lng];
                                      break;
                                    case 'ccns':
                                        $write_val = $generate_lines_arr['nr_lt_value_'.$lng];
                                      break;
                                    case 'ccns_l':
                                        $write_val = $generate_lines_arr['nr_lt_value_'.$lng].');';
                                      break;
                                    case 'arrk':
                                        $write_val = $generate_lines_arr['nr_lt_var'].' = '.$generate_lines_arr['nr_lt_value_'.$lng];
                                      break;
                                    case 'arr':
                                        $write_val = $generate_lines_arr['nr_lt_var'].' => '.$generate_lines_arr['nr_lt_value_'.$lng];
                                      break;
                                    case 'cnf':
                                        $write_val = $generate_lines_arr['nr_lt_var'].' = '.$generate_lines_arr['nr_lt_value_'.$lng];
                                      break;
                                }
                                
                                fwrite($fp, $write_val."\r\n");
                            }

                            if($cnt_lines == $num_res) {
                                fclose($fp);
                                $this->mstck['succ'][] = sprintf(NR_LANGS_EDIT_GENERATED_FILES_SUCCESS, $eff_filename, $lng, str_replace(DIR_FS_DOCUMENT_ROOT, '', $eff_path));
                            }
                        }
                    } else {
                        $this->mstck['err'][] = sprintf(NR_LANGS_EDIT_GENERATED_FILES_ERROR, $eff_filename, $lng, str_replace(DIR_FS_DOCUMENT_ROOT, '', $eff_path));
                    }
                }
            }
        }
        //return $eff_path.$eff_filename;
    }

    public function give_path_of_edited_file($file_id) {
        $path_qu = xtc_db_query("SELECT tf.nr_lt_file, tp.nr_lt_path
                                   FROM nr_lang_transl_files tf
                              LEFT JOIN nr_lang_transl_paths tp
                                     ON tp.nr_lt_path_id = tf.nr_lt_path_id
                                  WHERE tf.nr_lt_file_id = ".(int)$file_id);

        $path_arr = xtc_db_fetch_array($path_qu);
        $path_raw = $path_arr['nr_lt_path'].$path_arr['nr_lt_file'];
        $path = str_replace(DIR_FS_DOCUMENT_ROOT, '', $path_raw);
        
        return '/'.$path;
    }

    /*private function listFolderFiles($dir) {       
        
    }*/
    
    public function build_file_tree() {
        $tobe_transl_langs = $this->has_langs_help();
        $bas_lang = $this->has_basic_data();

        if($bas_lang !== false) {
            //$sub_html_structure = $this->build_files_html($this->dir_iterate(DIR_FS_DOCUMENT_ROOT.'lang/'.$bas_lang)); //mmh, must do it inloop to have $lang
            
            $html_structure = '<div class="nr-tree-h">
                                <table>
                                    <tr>
                                        <td class="root-td f-td">
                                            <span class="dir-sp">lang</span>
                                        </td>
                                        <td>
                                            <table>';

            foreach($tobe_transl_langs as $lang) {
                $html_structure .= '<tr>
                                        <td class="f-td"><span class="dir-sp">'.$lang.'</span></td>
                                            <td>
                                                <table>';

                //$html_structure .= $sub_html_structure; //mmh, must do it inloop to have $lang (see above)
                $html_structure .= $this->build_files_html($this->dir_iterate(DIR_FS_DOCUMENT_ROOT.'lang/'.$bas_lang), $lang);

                $html_structure .= '</table></td></tr>';
            }

            $html_structure .= '</table></td></tr></table></div>';

            return $html_structure;
            
            //$files = $this->dir_iterate(DIR_FS_DOCUMENT_ROOT.'lang/'.$bas_lang);
            //return '<pre>'.print_r($files ,true).'</pre>';
        }
    }

    public function dir_iterate($path) {
        $dir_it = new RecursiveDirectoryIterator($path);
        $filesD = array();
        $filesF = array();
        foreach ($dir_it as $file) {
            if ('.' != $file->getFilename() && '..' != $file->getFilename()) {
                if ($file->isDir() && !in_array($file->getFilename(), $this->dir_excl_arr)) {
                    $filesD[$file->getFilename()] = $this->dir_iterate($file->getPathname());
                } else {
                    if(in_array($file->getExtension(), $this->file_ext_arr)) {
                        $filesF[] = $file->getPathname().'|'.$file->getFilename();
                    }
                }
            }
        }
        ksort($filesD);
        sort($filesF);
        $files = array_merge($filesD, $filesF);
        
        //**************************************************************************************
        //BOC from database (more complicated, can't get the desired pattern)
        //**************************************************************************************
        /*$files = array();

        $all_files_qu = xtc_db_query("SELECT tp.nr_lt_path, tf.nr_lt_file_id, tf.nr_lt_file
                                        FROM nr_lang_transl_paths tp
                                   LEFT JOIN nr_lang_transl_files tf
                                          ON tf.nr_lt_path_id = tp.nr_lt_path_id
                                    ORDER BY tp.nr_lt_path, tf.nr_lt_file");

        while($all_files_arr = xtc_db_fetch_array($all_files_qu)) {
            $files[$all_files_arr['nr_lt_path']][] = $all_files_arr['nr_lt_file'].'|'.$all_files_arr['nr_lt_file_id'].'|'.$all_files_arr['nr_lt_path'];
        }*/
        //**************************************************************************************
        //EOC from database
        //**************************************************************************************

        return $files;
    }

    private function build_files_html($files_arr, $lang) {
        $base_lng = $this->has_basic_data();

        foreach($files_arr as $key => $val) {
            $test_file_html = '';
            //is_numeric = file | !is_numeric = directory
            if(is_numeric($key)) {
                $val_arr = explode('|', $val);
                $val_arr[0] = str_replace($val_arr[1], '', $val_arr[0]);
                //find path id since we have files with the same name in different sub directories (e.g. LANGUAGE.php, zones.php)
                $tmp_val0 = str_replace($base_lng, 'LANGUAGE', $val_arr[0]);
                $tmp_path_qu = xtc_db_query("SELECT nr_lt_path_id FROM nr_lang_transl_paths WHERE nr_lt_path = '".xtc_db_input($tmp_val0)."'");
                $tmp_path_arr = xtc_db_fetch_array($tmp_path_qu);
                
                $val_arr[1] = str_replace($base_lng, $lang, $val_arr[1]);
                $transl_path = str_replace($base_lng, NR_LT_TRANSL_LANG_PATH.$lang, $val_arr[0]);

                if(!in_array($val_arr[1], $this->file_excl_arr)) {
                    if(file_exists($transl_path.$val_arr[1])) {
                        $file_class = ' class="green-sp"';
                        if(substr($val_arr[1], -4) == '.php') {
                            $test_file_html .= '<span class="test-file-butt" data-testpath="'.$transl_path.$val_arr[1].'">test</span>';
                        }
                    } else {
                        $file_class = ' class="red-sp"';
                    }
                } else {
                    $file_class = '';
                }
                
                $valarr1 = strpos($val_arr[1], $lang) !== false ? str_replace($lang, 'LANGUAGE', $val_arr[1]) : $val_arr[1];
                $file_id_qu = xtc_db_query("SELECT nr_lt_file_id FROM nr_lang_transl_files WHERE nr_lt_file = '".xtc_db_input($valarr1)."' AND nr_lt_path_id = ".(int)$tmp_path_arr['nr_lt_path_id']);
                $file_id_arr = xtc_db_fetch_array($file_id_qu);
            }

            $dirorfile_aclass = !is_numeric($key) ? ' class="dir-sp"' : $file_class;
            $dirorfile_tdclass = !is_numeric($key) ? ' class="f-td"' : '';

            $dir_or_file = !is_numeric($key) ? $key : $val_arr[1].' ('.$file_id_arr['nr_lt_file_id'].')';
            $data_filid = is_numeric($key) ? ' data-file="'.$file_id_arr['nr_lt_file_id'].'"' : '';

            $html_structure .= '<tr><td'.$dirorfile_tdclass.'><span'.$data_filid.$dirorfile_aclass.'>'.$dir_or_file.'</span>'.($test_file_html != '' ? $test_file_html : '').'</td>';

            if(!is_numeric($key)) {
                $html_structure .= '<td><table>';
                $html_structure .= $this->build_files_html($val, $lang);
                $html_structure .= '</table></td>';
            } else {
                $html_structure .= '</tr>';
            }
            
            $cnt++;
        }

        return $html_structure;
        //return '<pre>'.print_r($files_arr, true).'</pre>';
    }

    public function build_messageStack($mstck) {
        if(!empty($mstck)) {
            foreach($mstck as $ms_key => $ms_vals) {
                if($ms_key == 'succ') {
                    $ms_typ = 'success';
                } else if($ms_key == 'err') {
                    $ms_typ = 'warning';
                }
                foreach($ms_vals as $ms_v) {
                    $this->messageStack->add($ms_v, $ms_typ);
                }
            }
        }
        return $this->messageStack;
    }
}
?>
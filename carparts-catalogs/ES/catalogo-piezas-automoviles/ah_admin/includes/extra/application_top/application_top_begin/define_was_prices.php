<?php
/*******************************************
* file: define_was_prices.php
* use: 
* (c) noRiddle 01-2017
*******************************************/

//BOC new was-is-prices
$groups_show_other_price_arr_1 = array('3','4','5','16'); //should get and show normal price from customer status 1
$groups_show_other_price_arr_8 = array('7','15'); //should get and show normal price from customer status 8
$groups_have_other_price_arr_1 = array('1', '2', '6', '11', '12', '17'); //should get and NOT show normal price from customer status 1
$groups_have_other_price_arr_8 = array('8', '9', '10', '13', '14'); //should get and NOT show normal price from customer status 8
//EOC new was-is-prices

define('MODULE_PRICE_WEIGHT_PREFIX_STATUS', true); // use = for attribute as well, constant used in /admin/includes/modules/new_attributes_include.php, noRiddle
?>
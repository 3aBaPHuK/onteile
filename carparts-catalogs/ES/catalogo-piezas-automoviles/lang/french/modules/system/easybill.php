<?php
/* -----------------------------------------------------------------------------------------
   $Id: easybill.php 4242 2013-01-11 13:56:09Z gtb-modified $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
  
define('MODULE_EASYBILL_TEXT_DESCRIPTION', 'Écrire votre facture en ligne');
define('MODULE_EASYBILL_TEXT_TITLE', 'easyBill - Écrire des factures en ligne');
define('MODULE_EASYBILL_STATUS_DESC', 'Statut de module');
define('MODULE_EASYBILL_STATUS_TITLE', 'Statut');
define('MODULE_EASYBILL_API_TITLE', 'Key API');
define('MODULE_EASYBILL_API_DESC', 'Il faut que le Key soit crée dans le menu du fournisseur.');
define('MODULE_EASYBILL_BILLINGID_TITLE', 'Gérer le numéro de facture:');
define('MODULE_EASYBILL_BILLINGID_DESC', 'Il faut que les réglages pour easyBill soient effecté dans le menu du fournisseur.<br/><br/>Si vous souhaitez gérer les numéro de factures il faut que le numéro séquentiel précis soit sauvegardé.');
define('MODULE_EASYBILL_BILLCREATE_TITLE', 'Créer des factures');
define('MODULE_EASYBILL_BILLCREATE_DESC', 'Créer des factures automatiquement dans le checkout ou manuellement?');
define('MODULE_EASYBILL_BILLSAVE_TITLE', 'Sauvegarder les factures');
define('MODULE_EASYBILL_BILLSAVE_DESC', 'Archiver les factures chez easyBill ou local?');
define('MODULE_EASYBILL_PAYMENT_TITLE', 'Modes de paiement');
define('MODULE_EASYBILL_PAYMENT_DESC', 'Quels modes de paiement ne génèrent pas de facture automatique?<br/><br/>Liste séparée par point-virgule (;)');
define('MODULE_EASYBILL_DO_STATUS_CHANGE_TITLE', 'Modifier le statut de la commande');
define('MODULE_EASYBILL_DO_STATUS_CHANGE_DESC', 'Modifier le statut de la commande après que la création de la commande soit effectué?');
define('MODULE_EASYBILL_DO_AUTO_PAYMENT_TITLE', 'Marquer la facture comme payée');
define('MODULE_EASYBILL_DO_AUTO_PAYMENT_DESC', 'Marquer la facture comme payée quand elle est crée?');
define('MODULE_EASYBILL_NO_AUTO_PAYMENT_TITLE', 'Marquer la facture comme payée - exceptions');
define('MODULE_EASYBILL_NO_AUTO_PAYMENT_DESC', 'Quels modes de paiement est-ce qu\'il ne faut pas mettre automatiquement sur payé? <br/><br/>Liste séparée par point-virgule (;)');
define('MODULE_EASYBILL_STATUS_CHANGE_TITLE', 'Statut de la commande');
define('MODULE_EASYBILL_STATUS_CHANGE_DESC', 'Après création de facture réussie mettre sur ce statut.');
define('MODULE_EASYBILL_STANDARD_TAX_CLASS_TITLE', '<hr /><b>Taux d\'imposition standard:</b>');
define('MODULE_EASYBILL_STANDARD_TAX_CLASS_DESC', 'Veuillez indiquer un taux standard au cas ou la catégorie fiscale soit caluclée.');
  
?>

<?php
/*******************************************************
* file: ribbon_module.php
* use: activate/deactivate percent ribbons on specials product images
* (c) noRiddle 08-2017
* Released under the GNU General Public License
*******************************************************/

defined( '_VALID_XTC' ) or die( 'Direct Access to this location is not allowed.' );

class ribbon_module
{
    var $code, $title, $description, $enabled;

    function __construct() 
    {
        $this->code = 'ribbon_module';
        $this->title = MODULE_RIBBON_MODULE_TEXT_TITLE;
        $this->description = MODULE_RIBBON_MODULE_TEXT_DESCRIPTION;
        $this->sort_order = MODULE_RIBBON_MODULE_SORT_ORDER;
        if(defined('MODULE_RIBBON_MODULE_STATUS')) {
            $this->enabled = ((MODULE_RIBBON_MODULE_STATUS == 'true') ? true : false);
        } else {
            $this->enabled = false;
        }
    }

    function process($file) 
    {
        //do nothing
    }

    function display() 
    {
        return array('text' => '<br>' . xtc_button(BUTTON_SAVE) . '&nbsp;' .
                               xtc_button_link(BUTTON_CANCEL, xtc_href_link(FILENAME_MODULE_EXPORT, 'set=' . $_GET['set'] . '&module='.$this->code))
                     );
    }

    function check() 
    {
        if(!isset($this->_check)) {
          $check_query = xtc_db_query("SELECT configuration_value FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_RIBBON_MODULE_STATUS'");
          $this->_check = xtc_db_num_rows($check_query);
        }
        return $this->_check;
    }

    function install() 
    {
        xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) VALUES ('MODULE_RIBBON_MODULE_STATUS', 'false',  '6', '1', 'xtc_cfg_select_option(array(\'true\', \'false\'), ', now())");
    }

    function remove()
    {
        xtc_db_query("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key LIKE 'MODULE_RIBBON_MODULE_%'");
    }

    function keys() 
    {
        return array(
          'MODULE_RIBBON_MODULE_STATUS',
        );
    }    
}
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id: moneyorder.php 998 2005-07-07 14:18:20Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(moneyorder.php,v 1.8 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (moneyorder.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_MONEYORDER_TEXT_TITLE', 'Paiement d\'avance/virement bancaire');
define('MODULE_PAYMENT_MONEYORDER_TEXT_DESCRIPTION', 'Coordonnées bancaires:<br />' . (defined('MODULE_PAYMENT_MONEYORDER_PAYTO') ? nl2br(MODULE_PAYMENT_MONEYORDER_PAYTO) : '') . '<br /><br />Propriétaire du compte:<br />' . nl2br(STORE_OWNER) . '<br />');
define('MODULE_PAYMENT_MONEYORDER_TEXT_EMAIL_FOOTER', "Bankverbindung: ". (defined('MODULE_PAYMENT_MONEYORDER_PAYTO') ? MODULE_PAYMENT_MONEYORDER_PAYTO : '') . "\nPropriétaire du compte:\n" . STORE_OWNER . "\n");
  if (MODULE_PAYMENT_MONEYORDER_SUCCESS == 'True') {
define('MODULE_PAYMENT_MONEYORDER_TEXT_INFO', 'Nous encoyons votre commande après la réception de paiement. Vous recevez les coordonnées bancaires après l\'acceptation de commande par mail.');
  } else {
define('MODULE_PAYMENT_MONEYORDER_TEXT_INFO', 'Nous encoyons votre commande après la réception de paiement. Vous recevez les coordonnées bancaires après l\'acceptation de commande par mail.');
  }
define('MODULE_PAYMENT_MONEYORDER_STATUS_TITLE' , 'Activer module Check/Money Order');
define('MODULE_PAYMENT_MONEYORDER_STATUS_DESC' , 'Accepter des paiements par Check/Money Order?');
define('MODULE_PAYMENT_MONEYORDER_ALLOWED_TITLE' , 'Zones autorisées');
define('MODULE_PAYMENT_MONEYORDER_ALLOWED_DESC' , 'Entrez <b>séparément</b> les zones qui sont autorisées pour ce module. (p.ex. AT,DE (si vide, toutes les zones sont autorisées))');
define('MODULE_PAYMENT_MONEYORDER_PAYTO_TITLE' , 'Payable à:');
define('MODULE_PAYMENT_MONEYORDER_PAYTO_DESC' , 'À qui est-ce qu\'il faut effectuer des paiements?');
define('MODULE_PAYMENT_MONEYORDER_SORT_ORDER_TITLE' , 'Ordre d\'affichage');
define('MODULE_PAYMENT_MONEYORDER_SORT_ORDER_DESC' , 'Ordre d\'affichage. Le chiffre le plus petit est affiché en premier.');
define('MODULE_PAYMENT_MONEYORDER_ZONE_TITLE' , 'Zone de paiement');
define('MODULE_PAYMENT_MONEYORDER_ZONE_DESC' , 'Si une zone est choisie, la méthode de paiement n\'est que valable pour cette zone.');
define('MODULE_PAYMENT_MONEYORDER_ORDER_STATUS_ID_TITLE' , 'Établir le statut de commande');
define('MODULE_PAYMENT_MONEYORDER_ORDER_STATUS_ID_DESC' , 'Moduler les commandes effectués avec ce module vers ce statut');
define('MODULE_PAYMENT_MONEYORDER_SUCCESS_TITLE' , 'Afficher les coordonnées bancaires');
define('MODULE_PAYMENT_MONEYORDER_SUCCESS_DESC' , 'Afficher les coordonnées bancaires sur la page de succès?');
?>

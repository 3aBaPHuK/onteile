<?php
/* -----------------------------------------------------------------------------------------
   $Id: gv_sent.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(gv_sent.php,v 1.2 2003/02/18 00:15:52); www.oscommerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Cupones enviados');

define('TABLE_HEADING_SENDERS_NAME', 'remitente');
define('TABLE_HEADING_VOUCHER_VALUE', 'Valor del cupón');
define('TABLE_HEADING_VOUCHER_CODE', 'Código del cupón');
define('TABLE_HEADING_DATE_SENT', 'Fecha del envío');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_INFO_SENDERS_ID', 'Número del remitente:');
define('TEXT_INFO_AMOUNT_SENT', 'Importe enviado:');
define('TEXT_INFO_DATE_SENT', 'Fecha:');
define('TEXT_INFO_VOUCHER_CODE', 'Código del cupón:');
define('TEXT_INFO_EMAIL_ADDRESS', 'Dirección de correo electrónico:');
define('TEXT_INFO_DATE_REDEEMED', 'Fecha de reembolso:');
define('TEXT_INFO_IP_ADDRESS', 'Dirección IP');
define('TEXT_INFO_CUSTOMERS_ID', 'Múmero del cliente:');
define('TEXT_INFO_NOT_REDEEMED', 'No canjeado');
?>

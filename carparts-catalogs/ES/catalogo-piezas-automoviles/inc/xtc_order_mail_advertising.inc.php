<?php
/*************************************************************
* file: xtc_order_mail_advertising.inc.php
* use: get advertisng texts from table order_mail_advertise
* (c) noRiddle 08-2016
*************************************************************/

function xtc_order_mail_advertising() {
    $adv_txt_qu = xtc_db_query("SELECT order_mail_advertise_text FROM ".TABLE_ORDER_MAIL_ADVERTISE." WHERE order_mail_advertise_status = '1' AND language_id = '".(int)$_SESSION['languages_id']."'");
    if(xtc_db_num_rows($adv_txt_qu)) {
        $adv_txt = xtc_db_fetch_array($adv_txt_qu);
        return nl2br($adv_txt['order_mail_advertise_text']);
    } else {
        return false;
    }
}
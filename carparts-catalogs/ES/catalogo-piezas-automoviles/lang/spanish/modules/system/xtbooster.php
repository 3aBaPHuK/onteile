<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_XTBOOSTER_TEXT_DESCRIPTION', 'xs:booster eBay-Connector');
define('MODULE_XTBOOSTER_TEXT_TITLE', 'xs:booster eBay-Connector');
define('MODULE_XTBOOSTER_STATUS_DESC', 'Estado de módulo');
define('MODULE_XTBOOSTER_STATUS_TITLE', 'Estado');
define('MODULE_XTBOOSTER_SHOPKEY_TITLE', 'ShopKey');
define('MODULE_XTBOOSTER_SHOPKEY_DESC', '');
define('MODULE_XTBOOSTER_STDSITE_TITLE', 'sitio estándar');
define('MODULE_XTBOOSTER_STDSITE_DESC', '');
define('MODULE_XTBOOSTER_STDCURRENCY_TITLE', 'Moneda estándar');
define('MODULE_XTBOOSTER_STDCURRENCY_DESC', '');
define('MODULE_XTBOOSTER_STDPLZ_TITLE', 'Ubicación estándar del artículo (código postal)');
define('MODULE_XTBOOSTER_STDPLZ_DESC', '');
define('MODULE_XTBOOSTER_STDSTANDORT_TITLE', 'Ubicación estándar del artículo');
define('MODULE_XTBOOSTER_STDSTANDORT_DESC', '');

?>

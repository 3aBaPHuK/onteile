<?php
/* --------------------------------------------------------------
   $Id: countries.php 4200 2013-01-10 19:47:11Z Tomcraft1980 $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(countries.php,v 1.8 2002/01/19); www.oscommerce.com 
   (c) 2003	 nextcommerce (countries.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Países');

define('TABLE_HEADING_COUNTRY_NAME', 'País');
define('TABLE_HEADING_COUNTRY_CODES', 'Códigos ISO');
define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_STATUS', 'Estado');

define('TEXT_INFO_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_INFO_COUNTRY_NAME', 'Nombre:');
define('TEXT_INFO_COUNTRY_CODE_2', 'Código ISO (2):');
define('TEXT_INFO_COUNTRY_CODE_3', 'Código ISO (3):');
define('TEXT_INFO_ADDRESS_FORMAT', 'Formato de dirección:');
define('TEXT_INFO_INSERT_INTRO', 'Por favor, introduzca el nuevo país con todos los datos relevantes.');
define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar el país?');
define('TEXT_INFO_HEADING_NEW_COUNTRY', 'Nuevo país');
define('TEXT_INFO_HEADING_EDIT_COUNTRY', 'Editar país');
define('TEXT_INFO_HEADING_DELETE_COUNTRY', 'Borrar país');

define('TABLE_HEADING_REQUIRED_ZONES', 'Mostrar estados federales');
?>

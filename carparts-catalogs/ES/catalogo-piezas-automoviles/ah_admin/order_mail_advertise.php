<?php
/* ------------------------------------------------
*  orders_mail_advertise.php
*  use: define advertising slogans for order mail
* (c) noRiddle 07-2016
*  ----------------------------------------------*/

require('includes/application_top.php');

//BOC vars
$languages = xtc_get_languages();
$action = (isset($_GET['action']) ? $_GET['action'] : '');
//EOC vars

//BOC functions
function xtc_get_order_mail_advertise_title($order_mail_advertise_id, $language_id = '') {
    if (!$language_id)
        $language_id = $_SESSION['languages_id'];
        $order_mail_advertise_title_query = xtc_db_query("SELECT order_mail_advertise_title FROM ".TABLE_ORDER_MAIL_ADVERTISE." WHERE order_mail_advertise_id = '".$order_mail_advertise_id."' AND language_id = '".(int)$language_id."'");
        $order_mail_advertise_title = xtc_db_fetch_array($order_mail_advertise_title_query);
        return $order_mail_advertise_title['order_mail_advertise_title'];
}
  
function xtc_get_order_mail_advertise_text($order_mail_advertise_id, $language_id = '') {
    if (!$language_id)
        $language_id = $_SESSION['languages_id'];
        $order_mail_advertise_text_query = xtc_db_query("SELECT order_mail_advertise_text FROM ".TABLE_ORDER_MAIL_ADVERTISE." WHERE order_mail_advertise_id = '".$order_mail_advertise_id."' AND language_id = '".(int)$language_id."'");
        $order_mail_advertise_text = xtc_db_fetch_array($order_mail_advertise_text_query);
        return (isset($_GET['action']) && $_GET['action'] == 'edit') ? ($order_mail_advertise_text['order_mail_advertise_text']) : (substr($order_mail_advertise_text['order_mail_advertise_text'], 0, 40) . ' ...');
}
  
function nr_set_mail_advertise_flag($adv_id, $stat) {
    if($stat == '1') {
        return xtc_db_query("UPDATE ".TABLE_ORDER_MAIL_ADVERTISE." SET order_mail_advertise_status = 0")
            .xtc_db_query("UPDATE ".TABLE_ORDER_MAIL_ADVERTISE." SET order_mail_advertise_status = 1 WHERE order_mail_advertise_id = '".(int)$adv_id."'");
    } else if($stat == '0') {
        return xtc_db_query("UPDATE ".TABLE_ORDER_MAIL_ADVERTISE." SET order_mail_advertise_status = 0 WHERE order_mail_advertise_id = '".(int)$adv_id."'");
    } else {
        return false;
    }
}
//EOC functions

//BOC switch action
if (xtc_not_null($action)) {
    switch ($action) {
        case 'set_st_flag':
            if (($_GET['flag'] == '0') || ($_GET['flag'] == '1')) {
                nr_set_mail_advertise_flag($_GET['mail_advertise_id'], $_GET['flag']);
            }
        break;
        case 'insert':
        case 'save':
            if (isset($_GET['mail_advertise_id'])) $order_mail_advertise_id = xtc_db_prepare_input($_GET['mail_advertise_id']);

            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                $order_mail_advertise_text_array = $_POST['order_mail_advertise_text'];
                $order_mail_advertise_title_array = $_POST['order_mail_advertise_title'];
                $language_id = $languages[$i]['id'];

                $sql_data_array = array(
                    'order_mail_advertise_text' => xtc_db_prepare_input($order_mail_advertise_text_array[$language_id]),
                    'order_mail_advertise_title' => xtc_db_prepare_input($order_mail_advertise_title_array[$language_id])
                );

                if ($action == 'insert') {
                    if (!xtc_not_null($order_mail_advertise_id)) {
                        $next_id_query = xtc_db_query("SELECT max(order_mail_advertise_id)
                                                           AS order_mail_advertise_id 
                                                         FROM ".TABLE_ORDER_MAIL_ADVERTISE
                                                     );
                                     
                        $next_id = xtc_db_fetch_array($next_id_query);
                  
                        $order_mail_advertise_id = $next_id['order_mail_advertise_id'] + 1;
                    }

                    $insert_sql_data = array('order_mail_advertise_id' => $order_mail_advertise_id,
                                             'language_id' => $language_id);
                                             
                    $sql_data_array = xtc_array_merge($sql_data_array, $insert_sql_data);
                    xtc_db_perform(TABLE_ORDER_MAIL_ADVERTISE, $sql_data_array);
                } elseif ($action == 'save') {
                    $order_mail_advertise_query = xtc_db_query("SELECT * 
                                                                    FROM ".TABLE_ORDER_MAIL_ADVERTISE." 
                                                                   WHERE language_id = '".(int)$language_id."' 
                                                                     AND order_mail_advertise_id = " .(int)$order_mail_advertise_id
                                                                );
        				
                    if (xtc_db_num_rows($order_mail_advertise_query) == 0) 
                        xtc_db_perform(TABLE_ORDER_MAIL_ADVERTISE, array ('order_mail_advertise_id' => (int)$order_mail_advertise_id, 'language_id' => (int)$language_id));
        
                    xtc_db_perform(TABLE_ORDER_MAIL_ADVERTISE, $sql_data_array, 'update', "order_mail_advertise_id = '" . (int)$order_mail_advertise_id . "' and language_id = '" . (int)$language_id . "'");
                }
            }
            xtc_redirect(xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&mail_advertise_id=' . $order_mail_advertise_id));
        break;

        case 'deleteconfirm':
            $mail_advertise_id = xtc_db_prepare_input($_GET['mail_advertise_id']);
            xtc_db_query("delete from " . TABLE_ORDER_MAIL_ADVERTISE . " where order_mail_advertise_id = '" . xtc_db_input($mail_advertise_id) . "'");

            xtc_redirect(xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page']));
        break;

        case 'delete':
            $mail_advertise_id = xtc_db_prepare_input($_GET['mail_advertise_id']);
     
            $remove_status = true;
     
        break;
    }
}
//EOC switch action

//BOC HTML
require (DIR_WS_INCLUDES.'head.php');
?>
</head>
<body>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
        <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top">
            <table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
                <tr>
                    <td>
                        <?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
                    </td>
                </tr>
            </table>
        </td>
        <td class="boxCenter" width="100%" valign="top">
            <table border="0" width="100%" cellspacing="0" cellpadding="2">
                <tr>
                    <td>
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="80" rowspan="2"><?php echo xtc_image(DIR_WS_ICONS.'heading/icon_configuration.png'); ?></td>
                                <td class="pageHeading"><?php echo BOX_ORDER_MAIL_ADVERTISE; ?></td>
                            </tr>
                            <tr>
                                <td class="main" valign="top">XT Configuration</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top">
                                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                        <tr class="dataTableHeadingRow">
                                            <td class="dataTableHeadingContentb" width="6%"><?php echo TABLE_HEADING_ORDER_MAIL_ADVERTISE_ID.xtc_sorting(FILENAME_ORDER_MAIL_ADVERTISE,'oma_id'); ?></td>
                                            <td class="dataTableHeadingContentb" width="22%"><?php echo TABLE_HEADING_ORDER_MAIL_ADVERTISE_TITLE; ?></td>
                                            <td class="dataTableHeadingContentb" width="60%"><?php echo TABLE_HEADING_ORDER_MAIL_ADVERTISE_TEXT; ?></td>
                                            <td class="dataTableHeadingContentb" width="6%"><?php echo TABLE_HEADING_ORDER_MAIL_ADVERTISE_STATUS.xtc_sorting(FILENAME_ORDER_MAIL_ADVERTISE,'oma_status'); ?></td>
                                            <td class="dataTableHeadingContentb" width="60px"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td> 
                                        </tr>
<?php
//BOC sorting
$sorting = (isset($_GET['sorting']) ? $_GET['sorting'] : '');
    if (xtc_not_null($sorting)) {
        switch ($sorting) {
            case 'oma_id':
                $sort = 'order_mail_advertise_id ASC';
              break;
            case 'oma_id-desc':
                $sort = 'order_mail_advertise_id DESC';
              break;
            case 'oma_status':
                $sort = 'order_mail_advertise_status ASC';
              break;
            case 'oma_status-desc':
                $sort = 'order_mail_advertise_status DESC';
              break;
            default:
                $sort = 'order_mail_advertise_id DESC';
             break;
    }
} else {
    $sort    = 'order_mail_advertise_id DESC';
}
//EOC sorting

$order_mail_advertise_query_raw = "SELECT order_mail_advertise_id, order_mail_advertise_status, order_mail_advertise_title, order_mail_advertise_text
                                       FROM " . TABLE_ORDER_MAIL_ADVERTISE . " 
                                      WHERE language_id = '" . $_SESSION['languages_id'] . "'
                                   ORDER BY ".$sort;  			     
  				
$order_mail_advertise_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $order_mail_advertise_query_raw, $order_mail_advertise_query_numrows);
$order_mail_advertise_query = xtc_db_query($order_mail_advertise_query_raw);
  
while ($order_mail_advertise = xtc_db_fetch_array($order_mail_advertise_query)) {
    if ((!isset($_GET['mail_advertise_id']) || (isset($_GET['mail_advertise_id']) && ($_GET['mail_advertise_id'] == $order_mail_advertise['order_mail_advertise_id']))) && !isset($oInfo) && (substr($action, 0, 3) != 'new')) {
        $oInfo = new objectInfo($order_mail_advertise);
    }

    if (isset($oInfo) && is_object($oInfo) && ($order_mail_advertise['order_mail_advertise_id'] == $oInfo->order_mail_advertise_id) ) {
        echo '<tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'pointer\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&mail_advertise_id=' . $oInfo->order_mail_advertise_id . '&action=edit') . '\'">' . "\n";
    } else {
        echo '<tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'pointer\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&mail_advertise_id=' . $order_mail_advertise['order_mail_advertise_id']) . '\'">' . "\n";
    }
   
    echo '<td class="dataTableContent" width="6%">' . $order_mail_advertise['order_mail_advertise_id'] . '</td>' . "\n";
    echo '<td class="dataTableContent">' . $order_mail_advertise['order_mail_advertise_title'] . '</td>' . "\n";
    echo '<td class="dataTableContent">' . substr($order_mail_advertise['order_mail_advertise_text'], 0, 80) . ' ...' . '</td>' . "\n";
    echo '<td class="dataTableContent">';
    if ($order_mail_advertise['order_mail_advertise_status'] == '1') {
        echo xtc_image(DIR_WS_IMAGES.'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10).'<a href="' . xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, xtc_get_all_get_params(array('action', 'mail_advertise_id')) . 'action=set_st_flag&flag=0&mail_advertise_id=' . $order_mail_advertise['order_mail_advertise_id']).'">&nbsp;&nbsp;'.xtc_image(DIR_WS_IMAGES.'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10).'</a>';
    } else {
        echo '<a href="'.xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, xtc_get_all_get_params(array('action', 'mail_advertise_id')).'action=set_st_flag&flag=1&mail_advertise_id='.$order_mail_advertise['order_mail_advertise_id']).'">'.xtc_image(DIR_WS_IMAGES.'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10).'</a>&nbsp;&nbsp;'.xtc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
    }
    echo '</td>' . "\n";
?>
                                            <td class="dataTableContent" width="60px" align="right">
<?php
        if (isset($oInfo) && is_object($oInfo) && ($order_mail_advertise['order_mail_advertise_id'] == $oInfo->order_mail_advertise_id) ) {
            echo xtc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ICON_ARROW_RIGHT);
        } else {
            echo '<a href="' . xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&mail_advertise_id=' . $order_mail_advertise['order_mail_advertise_id']) . '">' . xtc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>';
        }
?>
                                            </td>
                                        </tr>
<?php
  }
?>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                        <tr>
                                            <td class="smallText" valign="top">
                                                <?php echo $order_mail_advertise_split->display_count($order_mail_advertise_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ORDER_MAIL_ADVERTISE); ?>
                                            </td>
                                            <td class="smallText" align="right">
                                                <?php echo $order_mail_advertise_split->display_links($order_mail_advertise_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
                                            </td>
                                        </tr>
<?php
if (empty($action)) {
?>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <?php echo '<a class="button" onclick="this.blur();" href="' . xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&action=new') . '">' . BUTTON_INSERT . '</a>'; ?>
                                            </td>
                                        </tr>
<?php
}
?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
<?php
$heading = array();
$contents = array();
switch ($action) {
    case 'new': 
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_NEW_ORDER_MAIL_ADVERTISE . '</b>');
        $contents = array('form' => xtc_draw_form('status', FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&action=insert'));
        $contents[] = array('text' => TEXT_INFO_INSERT_ORDER_MAIL_ADVERTISE_INTRO);

        $order_mail_advertise_inputs_string = '';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $order_mail_advertise_inputs_string .= '<br /><br />' . TABLE_HEADING_ORDER_MAIL_ADVERTISE_TITLE . '<br />' . xtc_image(DIR_WS_LANGUAGES.$languages[$i]['directory'].'/admin/images/'.$languages[$i]['image']) . '&nbsp;' . xtc_draw_input_field('order_mail_advertise_title[' . $languages[$i]['id'] . ']');
            $order_mail_advertise_inputs_string .= '<br />' . TABLE_HEADING_ORDER_MAIL_ADVERTISE_TEXT . '<br />' . xtc_image(DIR_WS_LANGUAGES.$languages[$i]['directory'].'/admin/images/'.$languages[$i]['image'], '', '', '', 'class="ord-stat-img"') . '&nbsp;' .         xtc_draw_textarea_field('order_mail_advertise_text[' . $languages[$i]['id'] . ']', 'soft', '', '', '' , 'class="ord-stat-comm"', false);
        }

        $contents[] = array('text' => $order_mail_advertise_inputs_string);
        $contents[] = array('align' => 'center', 'text' => '<br /><input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_INSERT . '"/> <a class="button" onclick="this.blur();" href="' . xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page']) . '">' . BUTTON_CANCEL . '</a><br style="clear:both;" />');
    break;

    case 'edit':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT_ORDER_MAIL_ADVERTISE . '</b>');
        $contents = array('form' => xtc_draw_form('status', FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&mail_advertise_id=' . $oInfo->order_mail_advertise_id  . '&action=save'));
        $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);

        $order_mail_advertise_inputs_string = '';
        $languages = xtc_get_languages();
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $order_mail_advertise_inputs_string .= '<br /><br />' . TABLE_HEADING_ORDER_MAIL_ADVERTISE_TITLE . '<br />' . xtc_image(DIR_WS_LANGUAGES.$languages[$i]['directory'].'/admin/images/'.$languages[$i]['image']) . '&nbsp;' . xtc_draw_input_field('order_mail_advertise_title[' . $languages[$i]['id'] . ']', xtc_get_order_mail_advertise_title($oInfo->order_mail_advertise_id, $languages[$i]['id']), '', '', false);
            $order_mail_advertise_inputs_string .= '<br />' . TABLE_HEADING_ORDER_MAIL_ADVERTISE_TEXT . '<br />' . xtc_image(DIR_WS_LANGUAGES.$languages[$i]['directory'].'/admin/images/'.$languages[$i]['image'], '', '', '', 'class="ord-stat-img"') . '&nbsp;' .         xtc_draw_textarea_field('order_mail_advertise_text[' . $languages[$i]['id'] . ']', 'soft', '', '', xtc_get_order_mail_advertise_text($oInfo->order_mail_advertise_id, $languages[$i]['id']), 'class="ord-stat-comm"', false);
        }

        $contents[] = array('text' => $order_mail_advertise_inputs_string);
        $contents[] = array('align' => 'center', 'text' => '<br /><input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_UPDATE . '"/> <a class="button" onclick="this.blur();" href="' . xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&mail_advertise_id=' . $oInfo->order_mail_advertise_id) . '">' . BUTTON_CANCEL . '</a><br style="clear:both;" />');	
    break;

    case 'delete':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_ORDER_MAIL_ADVERTISE . '</b>');
        $contents = array('form' => xtc_draw_form('status', FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&mail_advertise_id=' . $oInfo->order_mail_advertise_id  . '&action=deleteconfirm'));
        $contents[] = array('text' => TEXT_INFO_DELETE_ORDER_MAIL_ADVERTISE_INTRO);
      	if ((isset($oInfo->order_mail_advertise_title)) && (!empty($oInfo->order_mail_advertise_title))) {
      		$contents[] = array('text' => '<br /><b>' . $oInfo->order_mail_advertise_title . '</b>');
      	} else {
            $contents[] = array('text' => '<br /><b>' . $oInfo->order_mail_advertise_text . '</b>');
        }
        if ($remove_status) $contents[] = array('align' => 'center', 'text' => '<br /><input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_DELETE . '"/> <a class="button" onclick="this.blur();" href="' . xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&mail_advertise_id=' . $oInfo->order_mail_advertise_id) . '">' . BUTTON_CANCEL . '</a><br style="clear:both;" />');
    break;

    default:  
        if (isset($oInfo) && is_object($oInfo)) {
            $heading[] = array('text' => '<b>' . $oInfo->order_mail_advertise_title . '</b>'); 
            $contents[] = array('align' => 'center', 'text' => '<a class="button" onclick="this.blur();" href="' . xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&mail_advertise_id=' . $oInfo->order_mail_advertise_id . '&action=edit') . '">' . BUTTON_EDIT . '</a> <a class="button" onclick="this.blur();" href="' . xtc_href_link(FILENAME_ORDER_MAIL_ADVERTISE, 'page=' . $_GET['page'] . '&mail_advertise_id=' . $oInfo->order_mail_advertise_id . '&action=delete') . '">' . BUTTON_DELETE . '</a><br style="clear:both;" />'); 
	
            $order_mail_advertise_inputs_string = '';
            $languages = xtc_get_languages();
            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                $order_mail_advertise_inputs_string .= '<br />' . xtc_image(DIR_WS_LANGUAGES.$languages[$i]['directory'].'/admin/images/'.$languages[$i]['image']) . '&nbsp;' . xtc_get_order_mail_advertise_text($oInfo->order_mail_advertise_id, $languages[$i]['id']);
            }

            $contents[] = array('text' => $order_mail_advertise_inputs_string);
        }
      
    break;
}

if ( (xtc_not_null($heading)) && (xtc_not_null($contents)) ) {
    echo '<td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '</td>' . "\n";
}
?>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php
//BOC footer
require(DIR_WS_INCLUDES.'footer.php');
?>
<br />
</body>
</html>
<?php
require(DIR_WS_INCLUDES.'application_bottom.php');
//EOC footer
?>
<?php
  /* --------------------------------------------------------------
  $Id: module_newsletter.php 2515 2011-12-12 16:15:44Z dokuman $

  modified eCommerce Shopsoftware
  http://www.modified-shop.org

  Copyright (c) 2009 - 2013 [www.modified-shop.org]
  --------------------------------------------------------------
  based on:
  (c) 2006 xt:Commerce

  Released under the GNU General Public License
  --------------------------------------------------------------*/

define('HEADING_TITLE', 'Newsletter');
define('TITLE_CUSTOMERS', 'Groupe de clients');
define('TITLE_STK', 'Abonné');
define('TEXT_TITLE', 'Sujet:');
define('TEXT_TO', 'A: ');
define('TEXT_CC', 'Copie:');
define('TEXT_BODY', 'Contenu:');
define('TITLE_NOT_SEND', 'Titre:');
define('TITLE_ACTION', 'Action');
define('TEXT_EDIT', 'Editer');
define('TEXT_DELETE', 'Supprimer');
define('TEXT_SEND', 'Envoyer');
define('CONFIRM_DELETE', 'Êtes-vous sûr?');
define('TITLE_SEND', 'Envoyé');
define('TEXT_NEWSLETTER_ONLY', 'Egalement aux membres du groupe qui ne sont pas abonnés à la Newsletter?');
define('TEXT_USERS', 'Abonnés de');
define('TEXT_CUSTOMERS', 'Clients )</i>');
define('TITLE_DATE', 'Date');
define('TEXT_SEND_TO', 'Destinataire:');
define('TEXT_PREVIEW', '<b>Aperçu</b>');
define('TEXT_REMOVE_LINK', 'Décommander la Newsletter');

// BOF - DokuMan - 2011-12-12 - Texts for Newsletter E-Mail send status
define('INFO_NEWSLETTER_SEND', '%d Newsletter envoyée');
define('INFO_NEWSLETTER_LEFT', '%d Newsletter restant');
// EOF - DokuMan - 2011-12-12 - Texts for Newsletter E-Mail send status

define('TEXT_NEWSLETTER_INFO', '<strong>ATTENTION:</strong> L\'utilisation d\'un programme externe est recommandée pour l\'envoi d\'une Newsletter!<br /><br /> Si vous utilisez le module Newsletter Shop, il est recommandé de demander à son fournisseur combien de courriels peuvent être envoyés au cours d\'une certaine période.<br /> De nombreux fournisseurs ont des limitations ou permettent l\'envoi seulement par des serveurs courriels spécifiques.<br /><br /> La signature est directement attachée à la Newsletter. Si vous voulez attacher une signature formatée différemment par l\'editeur, veuillez insérer le code [NOSIGNATUR] (avec les crochets) à la fin de la Newsletter.');

?>

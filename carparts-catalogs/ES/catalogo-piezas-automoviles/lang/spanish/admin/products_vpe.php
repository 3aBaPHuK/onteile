<?php
/* --------------------------------------------------------------
   $Id: products_vpe.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(orders_status.php,v 1.7 2002/01/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (orders_status.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Unidad de embalaje');

define('TABLE_HEADING_PRODUCTS_VPE', 'Unidad de embalaje');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_INFO_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_INFO_PRODUCTS_VPE_NAME', 'Unidad de embalaje:');
define('TEXT_INFO_INSERT_INTRO', 'Por favor, introduzca la nueva unidad de embalaje con todos los datos relevantes.');
define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar esta unidad de embalaje?');
define('TEXT_INFO_HEADING_NEW_PRODUCTS_VPE', 'Nuevo estado de pedido');
define('TEXT_INFO_HEADING_EDIT_PRODUCTS_VPE', 'Editar unidad de embalaje');
define('TEXT_INFO_HEADING_DELETE_PRODUCTS_VPE', 'Borrar unidad de embalaje');

define('ERROR_REMOVE_DEFAULT_PRODUCTS_VPE', 'Error: No se puede borrar la unidad de embalaje estándar. Por favor, defina una nueva unidad de embalaje estándar y repita el proceso.');
?>

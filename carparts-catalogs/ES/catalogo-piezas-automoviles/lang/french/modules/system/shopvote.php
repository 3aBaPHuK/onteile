<?php
/* -----------------------------------------------------------------------------------------
   $Id: shopvote.php 10898 2017-08-11 16:02:44Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHOPVOTE_STATUS_TITLE', 'Statut');
define('MODULE_SHOPVOTE_STATUS_DESC', 'Statut de module');
define('MODULE_SHOPVOTE_STATUS_INFO', 'Actif');
define('MODULE_SHOPVOTE_TEXT_TITLE', 'Shopvote');
define('MODULE_SHOPVOTE_TEXT_DESCRIPTION', 'Grâce à ce plugin vous pouvez intégrer les meilleurs fonctions du portail d’évaluation SHOPVOTE tout facilement dans votre boutique. La fonction EasyReviews intégrée envoie automatiquement une demande d’évaluation à vos clients. Vos clients pourront facilement évaluer votre entreprise. La fonction RatingStars garantit d’une part la présentation optimale des évaluations sur votre site web et d’autre part la création automatique des étoile d’évaluation dans les résultats de recherche sur Google.');
define('MODULE_SHIPPING_EASYREVIEWS_TITLE', 'Code EasyReviews');
define('MODULE_SHIPPING_EASYREVIEWS_DESC', 'Copiez maintenant le JavaScript/Code HTLM de l’espace vendeur et insérez-le ici. Les variables "CUSTOMERMAIL" et "ORDNERNUMBER" seront automatiquement remplacés avec les données correspondantes par le module.');
define('MODULE_SHIPPING_RATINGSTARS_TITLE', 'Code RatingStars');
define('MODULE_SHIPPING_RATINGSTARS_DESC', 'L’intégration se fait tout simplement via l’utilisation de la figure ShopVote "AllVotes I + II" und "VoteBadge I – III". Vous trouverez les figures dans l’espace vendeur de SHOPVOTE sous „figures & sceaux“. Après avoir sélectionné une figure, faites-vous afficher le code et insérez-le ici.');

?>

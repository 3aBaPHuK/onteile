<?php
/* -----------------------------------------------------------------------------------------
   $Id: fedexeu.php 5123 2013-07-18 11:49:11Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce( fedexeu.php,v 1.01 2003/02/18 03:25:00); www.oscommerce.com
   (c) 2003	 nextcommerce (fedexeu.php,v 1.5 2003/08/1); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   fedex_europe_1.02        	Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/



define('MODULE_SHIPPING_FEDEXEU_TEXT_TITLE', 'FedEx Express Europa');
define('MODULE_SHIPPING_FEDEXEU_TEXT_DESCRIPTION', 'FedEx Express Europa');
define('MODULE_SHIPPING_FEDEXEU_TEXT_WAY', 'Envoi vers');
define('MODULE_SHIPPING_FEDEXEU_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_FEDEXEU_INVALID_ZONE', 'Malheureusement il n\'est pas possible d\'envoyer dans ce pays');
define('MODULE_SHIPPING_FEDEXEU_UNDEFINED_RATE', 'Les frais de port ne peuvent pas être calculés pour le moment');

define('MODULE_SHIPPING_FEDEXEU_STATUS_TITLE' , 'FedEx Express Europe');
define('MODULE_SHIPPING_FEDEXEU_STATUS_DESC' , 'Voulez-vous proposer un envoi avec FedEx Express Europe?');
define('MODULE_SHIPPING_FEDEXEU_HANDLING_TITLE' , 'Frais de traitement');
define('MODULE_SHIPPING_FEDEXEU_HANDLING_DESC' , 'Frais de traitement pour ce type d\'envoi en Euro');
define('MODULE_SHIPPING_FEDEXEU_TAX_CLASS_TITLE' , 'Taux d\'imposition');
define('MODULE_SHIPPING_FEDEXEU_TAX_CLASS_DESC' , 'Séléctionnez le taux de TVA pour ce type d\'envoi');
define('MODULE_SHIPPING_FEDEXEU_ZONE_TITLE' , 'Zone d\'envoi');
define('MODULE_SHIPPING_FEDEXEU_ZONE_DESC' , 'Si vous séléctionnez une zone, ce type d\'envoi sera seulement proposé dans cette zone.');
define('MODULE_SHIPPING_FEDEXEU_SORT_ORDER_TITLE' , 'Ordre d\'affichage');
define('MODULE_SHIPPING_FEDEXEU_SORT_ORDER_DESC' , 'Les plus bas sont affichés d\'abord.');
define('MODULE_SHIPPING_FEDEXEU_ALLOWED_TITLE' , 'Zones d\'envoi individuelles');
define('MODULE_SHIPPING_FEDEXEU_ALLOWED_DESC' , 'Les plus bas sont affichés d\'abord.');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_1_TITLE' , 'Zone Europe 1 Pays');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_1_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 1 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_1_TITLE' , 'Tableau de tarifs pour la zone 1 jusqu\'à 2.50kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_1_DESC' , 'Tableau des tarifs pour la zone 1, basé sur <b>\'PAK\'<b> jusqu\'à un poids d\'envoi de 2.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_1_TITLE' , 'Tableau de tarifs pour la zone 1 jusqu\'à 0.50kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_1_DESC' , 'Tableau des tarifs pour la zone 1, basé sur <b>\'ENV\'<b> jusqu\'à 60 pages en format A4 et un poids de de 0.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_1_TITLE' , 'Tableau de tarifs pour la zone 1 jusqu\'à 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_1_DESC' , 'Tableau des tarifs pour la zone 1, basé sur <b>\'BOX\'<b> jusqu\'à un poids d\'envoi de 10kg.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_1_TITLE' , 'Supplément d\'augmentation jusqu\'à 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_1_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_1_TITLE' , 'Supplément d\'augmentation jusqu\'à 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_1_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_1_TITLE' , 'Supplément d\'augmentation jusqu\'à 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_1_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_2_TITLE' , 'Zone Europe 2 Pays');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_2_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 2 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_2_TITLE' , 'Tableau de tarifs pour la zone 2 jusqu\'à 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_2_DESC' , 'Tableau des tarifs pour la zone 2, basé sur <b>\'PAK\'<b> jusqu\'à un poids d\'envoi de 2.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_2_TITLE' , 'Tableau de tarifs pour la zone 2 jusqu\'à 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_2_DESC' , 'Tableau des tarifs pour la zone 2, basé sur <b>\'ENV\'<b> jusqu\'à 60 pages en format A4 et un poids d\'envoi de 0.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_2_TITLE' , 'Tableau de tarifs pour la zone 2 jusqu\'à 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_2_DESC' , 'Tableau des tarifs pour la zone 2, basé sur <b>\'BOX\'<b> jusqu\'à un poids d\'envoi de 10kg.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_2_TITLE' , 'Supplément d\'augmentation jusqu\'à 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_2_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_2_TITLE' , 'Supplément d\'augmentation jusqu\'à 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_2_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_2_TITLE' , 'Supplément d\'augmentation jusqu\'à 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_2_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_3_TITLE' , 'Zone Europe 3 Pays');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_3_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 3 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_3_TITLE' , 'Tableau de tarifs pour la zone 3 jusqu\'à 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_3_DESC' , 'Tableau des tarifs pour la zone 3, basé sur <b>\'PAK\'<b> jusqu\'à un poids d\'envoi de 2.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_3_TITLE' , 'Tableau de tarifs pour la zone 3 jusqu\'à 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_3_DESC' , 'Tableau des tarifs pour la zone 3, basé sur <b>\'ENV\'<b> jusqu\'à 60 pages en format A4 et un poids d\'envoi de 0.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_3_TITLE' , 'Tableau de tarifs pour la zone 3 jusqu\'à 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_3_DESC' , 'Tableau des tarifs pour la zone 3, basé sur <b>\'BOX\'<b> jusqu\'à un poids d\'envoi de 10kg.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_3_TITLE' , 'Supplément d\'augmentation jusqu\'à 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_3_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_3_TITLE' , 'Supplément d\'augmentation jusqu\'à 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_3_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_3_TITLE' , 'Supplément d\'augmentation jusqu\'à 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_3_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_4_TITLE' , 'Zone monde A Pays' );
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_4_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone A (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_4_TITLE' , 'Tableau de tarifs pour la zone A jusqu\'à 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_4_DESC' , 'Tableau des tarifs pour la zone A, basé sur <b>\'PAK\'<b> jusqu\'à un poids d\'envoi de 2.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_4_TITLE' , 'Tableau de tarifs pour la zone A jusqu\'à 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_4_DESC' , 'Tableau des tarifs pour la zone A, basé sur <b>\'ENV\'<b> jusqu\'à 60 pages en format A4 et un poids d\'envoi de 0.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_4_TITLE' , 'Tableau de tarifs pour la zone A jusqu\'à 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_4_DESC' , 'Tableau des tarifs pour la zone A, basé sur <b>\'BOX\'<b> jusqu\'à un poids d\'envoi de 10kg.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_4_TITLE' , 'Supplément d\'augmentation jusqu\'à 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_4_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_4_TITLE' , 'Supplément d\'augmentation jusqu\'à 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_4_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_4_TITLE' , 'Supplément d\'augmentation jusqu\'à 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_4_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_5_TITLE' , 'Zone monde B Pays');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_5_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone B (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_5_TITLE' , 'Tableau de tarifs pour la zone B jusqu\'à 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_5_DESC' , 'Tableau des tarifs pour la zone B, basé sur <b>\'PAK\'<b> jusqu\'à un poids d\'envoi de 2.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_5_TITLE' , 'Tableau de tarifs pour la zone B jusqu\'à 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_5_DESC' , 'Tableau des tarifs pour la zone B, basé sur <b>\'ENV\'<b> jusqu\'à 60 pages en format A4 et un poids d\'envoi de 0.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_5_TITLE' , 'Tableau de tarifs pour la zone B jusqu\'à 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_5_DESC' , 'Tableau des tarifs pour la zone B, basé sur <b>\'BOX\'<b> jusqu\'à un poids d\'envoi de 10kg.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_5_TITLE' , 'Supplément d\'augmentation jusqu\'à 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_5_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_5_TITLE' , 'Supplément d\'augmentation jusqu\'à 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_5_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_5_TITLE' , 'Supplément d\'augmentation jusqu\'à 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_5_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_6_TITLE' , 'Zone monde C Pays');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_6_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone C (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_6_TITLE' , 'Tableau de tarifs pour la zone C jusqu\'à 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_6_DESC' , 'Tableau des tarifs pour la zone C, basé sur <b>\'PAK\'<b> jusqu\'à un poids d\'envoi de 2.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_6_TITLE' , 'Tableau de tarifs pour la zone C jusqu\'à 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_6_DESC' , 'Tableau des tarifs pour la zone C, basé sur <b>\'ENV\'<b> jusqu\'à 60 pages en format A4 et un poids d\'envoi de 0.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_6_TITLE' , 'Tableau de tarifs pour la zone C jusqu\'à 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_6_DESC' , 'Tableau des tarifs pour la zone C, basé sur <b>\'BOX\'<b> jusqu\'à un poids d\'envoi de 10kg.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_6_TITLE' , 'Supplément d\'augmentation jusqu\'à 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_6_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_6_TITLE' , 'Supplément d\'augmentation jusqu\'à 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_6_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_6_TITLE' , 'Supplément d\'augmentation jusqu\'à 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_6_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_7_TITLE' , 'Zone monde D Pays');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_7_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone D (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_7_TITLE' , 'Tableau de tarifs pour la zone D jusqu\'à 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_7_DESC' , 'Tableau des tarifs pour la zone D, basé sur <b>\'PAK\'<b> jusqu\'à un poids d\'envoi de 2.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_7_TITLE' , 'Tableau de tarifs pour la zone D jusqu\'à 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_7_DESC' , 'Tableau des tarifs pour la zone D, basé sur <b>\'ENV\'<b> jusqu\'à 60 pages en format A4 et un poids d\'envoi de 0.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_7_TITLE' , 'Tableau de tarifs pour la zone D jusqu\'à 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_7_DESC' , 'Tableau des tarifs pour la zone D, basé sur <b>\'BOX\'<b> jusqu\'à un poids d\'envoi de 10kg.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_7_TITLE' , 'Supplément d\'augmentation jusqu\'à 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_7_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_7_TITLE' , 'Supplément d\'augmentation jusqu\'à 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_7_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_7_TITLE' , 'Supplément d\'augmentation jusqu\'à 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_7_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_8_TITLE' , 'Zone monde E Pays');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_8_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone E (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_8_TITLE' , 'Tableau de tarifs pour la zone E jusqu\'à 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_8_DESC' , 'Tableau des tarifs pour la zone E, basé sur <b>\'PAK\'<b> jusqu\'à un poids d\'envoi de 2.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_8_TITLE' , 'Tableau de tarifs pour la zone E jusqu\'à 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_8_DESC' , 'Tableau des tarifs pour la zone E, basé sur <b>\'ENV\'<b> jusqu\'à 60 pages en format A4 et un poids d\'envoi de 0.50kg.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_8_TITLE' , 'Tableau de tarifs pour la zone E jusqu\'à 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_8_DESC' , 'Tableau des tarifs pour la zone E, basé sur <b>\'BOX\'<b> jusqu\'à un poids d\'envoi de 10kg.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_8_TITLE' , 'Supplément d\'augmentation jusqu\'à 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_8_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_8_TITLE' , 'Supplément d\'augmentation jusqu\'à 30 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_8_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_8_TITLE' , 'Supplément d\'augmentation jusqu\'à 50 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_8_DESC' , 'Supplément d\'augmentation par 0,50kg dépassant en EUR');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id: spanish.php 10896 2017-08-11 11:31:54Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(spanish.php,v 1.119 2003/05/19); www.oscommerce.com
   (c) 2003 nextcommerce (spanish.php,v 1.25 2003/08/25); www.nextcommerce.org
   (c) 2006 XT-Commerce
   
   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

/*
 * 
 *  DATE / TIME
 * 
 */
 
define('HTML_PARAMS', 'dir="ltr" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml"');
@setlocale(LC_TIME, 'es_ES.UTF-8' ,'es_ES@euro', 'es_ES', 'es-ES', 'es', 'sp', 'es_ES.ISO_8859-1', 'Spanish','es_ES.ISO_8859-15');

define('DATE_FORMAT_SHORT', '%d.%m.%Y');
define('DATE_FORMAT_LONG', '%A, %d. %B %Y');
define('DATE_FORMAT', 'd.m.Y');
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');
define('DOB_FORMAT_STRING', 'dd.mm.aaaa');

function xtc_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 0, 2) . substr($date, 3, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 3, 2) . substr($date, 0, 2);
  }
}

require_once(DIR_FS_INC.'auto_include.inc.php');
foreach(auto_include(DIR_WS_LANGUAGES.'spanish/extra/','php') as $file) require ($file);

define('TITLE', STORE_NAME);
//define('HEADER_TITLE_TOP', 'Startseite');    
define('HEADER_TITLE_TOP', 'Selección de marcas');
//define('HEADER_TITLE_CATALOG', 'Katalog');
define('HEADER_TITLE_CATALOG', ucwords(implode(' ', explode('-', $shop))));

// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency when changing language, 
// instead of staying with the applications default currency
define('LANGUAGE_CURRENCY', 'EUR');

define('MALE', 'Sr.');
define('FEMALE', 'Sra.');

/*
 * 
 *  BOXES
 * 
 */

// text for gift voucher redeeming
define('IMAGE_REDEEM_GIFT', '¡Canjear cupón!');

define('BOX_TITLE_STATISTICS', 'Estadística:');
define('BOX_ENTRY_CUSTOMERS', 'Clientes:');
define('BOX_ENTRY_PRODUCTS', 'Artículos:');
define('BOX_ENTRY_REVIEWS', 'Reseñas:');
define('TEXT_VALIDATING', 'No confirmado');

// manufacturer box text
define('BOX_MANUFACTURER_INFO_HOMEPAGE', '%s página web');
define('BOX_MANUFACTURER_INFO_OTHER_PRODUCTS', 'Más artículos');

define('BOX_HEADING_ADD_PRODUCT_ID', 'Poner en la cesta');
  
define('BOX_LOGINBOX_STATUS', 'Grupo de clientes:');
define('BOX_LOGINBOX_DISCOUNT', 'Descuento del artículo');
define('BOX_LOGINBOX_DISCOUNT_TEXT', 'Descuento');
define('BOX_LOGINBOX_DISCOUNT_OT', '');

// reviews box text in includes/boxes/reviews.php
define('BOX_REVIEWS_WRITE_REVIEW', '¡Escriba una reseña de este artículo!');
define('BOX_REVIEWS_NO_WRITE_REVIEW', 'No es posible una reseña.');
define('BOX_REVIEWS_TEXT_OF_5_STARS', '¡% de 5 estrellas! ');

// pull down default text
define('PULL_DOWN_DEFAULT', 'Por favor elija');

// javascript messages
define('JS_ERROR', 'Faltan algunos datos necesarios. Por favor rellenar completamente.\n\n');

define('JS_REVIEW_TEXT', '* El texto debe contener como mínimo ' . REVIEW_TEXT_MIN_LENGTH . ' letras.\n\n');
define('JS_REVIEW_RATING', '* Por favor envíe su evaluación.\n\n');
define('JS_ERROR_NO_PAYMENT_MODULE_SELECTED', '* Por favor elija una modalidad de pago para su pedido.\n');
define('JS_ERROR_SUBMITTED', 'Esta página ya ha sido comprobada. Por favor haga clic en OK y espera hasta que el proceso haya terminado.');
define('ERROR_NO_PAYMENT_MODULE_SELECTED', '* Por favor elija una modalidad de pago para su pedido.');
define('JS_ERROR_NO_SHIPPING_MODULE_SELECTED', '* Por favor elija un modo de envío para su pedido.\n');
define('JS_ERROR_CONDITIONS_NOT_ACCEPTED', '* ¡Si no acepta nuestros términos y condiciones generales,\n¡lamentablemente no podemos recibir su pedido!\n\n');
define('JS_ERROR_REVOCATION_NOT_ACCEPTED', '* ¡Si no acepta la expiración del derecho de revocación para los artículos virtuales,\nlamentablemente no podemos recibir su pedido!\n\n');
define('JS_REVIEW_AUTHOR', '* Por favor introduzca su nombre.\n\n');

/*
 * 
 * ACCOUNT FORMS
 * 
 */

define('ENTRY_COMPANY_ERROR', '');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_GENDER_ERROR', 'Por favor elija una forma de dirigirse ');
define('ENTRY_GENDER_TEXT', '*');
define('ENTRY_FIRST_NAME_ERROR', 'Su nombre debe contener como mínimo ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caracteres.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME_ERROR', 'Su apellido debe contener como mínimo ' . ENTRY_LAST_NAME_MIN_LENGTH . ' caracteres.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Su fecha de nacimiento debe introducirse del formato dd.mm.aaaa (por ejemplo 21.05.1970).');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (p.e. 21.05.1970)');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Su dirección de correo electrónico debe contener como mínimo ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' caracteres.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Su dirección de correo electrónico introducida es incorrecta o ya registrada.');
//BOC new error for create_account in case mail already exists, noRiddle
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR_EXISTS_NR', 'Una cuenta con esta direccion de correo electrónico ya existe.<br />Si es su cuenta, pida que le envíen una nueva contraseña. Luego puede cambiarla bajo "Mi cuenta" como desee.');
//EOC new error for create_account in case mail already exists, noRiddle
define('ENTRY_EMAIL_ERROR_NOT_MATCHING', 'Sus direcciones de correo electrónico introducidas no coinciden.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Su dirección de correo electrónico ya existe - por favor compruébala.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
//BOC new for house no., noRiddle
//define('ENTRY_STREET_ADDRESS_ERROR', 'Strasse/Nr. muss aus mindestens ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_STREET_ADDRESS_ERROR', ' Calle/N° debe contener como mínimo ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caracteres y tener un número de casa. Si no tiene un número de casa, por favor, introduzca 0.');
//EOC new for house no., noRiddle
define('ENTRY_STREET_ADDRESS_ERROR', ' Calle/N° debe contener como mínimo ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caracteres.');
define('ENTRY_STREET_ADDRESS_TEXT', '*');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE_ERROR', 'El código postal debe contener como mínimo ' . ENTRY_POSTCODE_MIN_LENGTH . ' caracteres.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY_ERROR', 'La ciudad debe contener como mínimo ' . ENTRY_CITY_MIN_LENGTH . ' caracteres.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE_ERROR', 'La provincia debe contener como mínimo ' . ENTRY_STATE_MIN_LENGTH . ' caracteres.');
define('ENTRY_STATE_ERROR_SELECT', 'Por favor elija su provincia.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY_ERROR', 'Por favor elija su país.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Su número de teléfono debe contener como mínimo ' . ENTRY_TELEPHONE_MIN_LENGTH . ' caracteres.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_PASSWORD_ERROR', 'Su contraseña debe contener como mínimo ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_ERROR_MIN_LOWER', 'Su contraseña debe contener como mínimo %s letras minúsculas.');
define('ENTRY_PASSWORD_ERROR_MIN_UPPER', 'Su contraseña debe contener como mínimo %s letras mayúsculas.');
define('ENTRY_PASSWORD_ERROR_MIN_NUM', 'Su contraseña debe contener como mínimo %s cifras.');
define('ENTRY_PASSWORD_ERROR_MIN_CHAR', 'Su contraseña debe contener como mínimo %s caracteres especiales.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'Sus contraseñas no coinciden.');
define('ENTRY_PASSWORD_TEXT', '*');
define('ENTRY_PASSWORD_CONFIRMATION_TEXT', '*');
define('ENTRY_PASSWORD_CURRENT_TEXT', '*');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Su contraseña debe contener como mínimo ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_NEW_TEXT', '*');
define('ENTRY_PASSWORD_NEW_ERROR', 'Su nueva contraseña debe contener como mínimo ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Sus contraseñas no coinciden.');

/*
 * 
 *  RESULT PAGES
 * 
 */
 
define('TEXT_RESULT_PAGE', 'Páginas:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Muestra <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> artículos)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Muestra <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> pedidos)');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'Muestra <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> reseñas)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW', 'Muestra<b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> nuevos artículos)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Muestra<b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> ofertas)');

/*
 * 
 * SITE NAVIGATION
 * 
 */

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'página anterior');
define('PREVNEXT_TITLE_NEXT_PAGE', 'siguiente página ');
define('PREVNEXT_TITLE_PAGE_NO', 'Página %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', '%d páginas anteriormente visitadas');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Próximas %d páginas');

/*
 * 
 * PRODUCT NAVIGATION
 * 
 */

define('PREVNEXT_BUTTON_PREV', '«');
define('PREVNEXT_BUTTON_NEXT', '»');

/*
 * 
 * IMAGE BUTTONS
 * 
 */

define('IMAGE_BUTTON_ADD_ADDRESS', 'Nueva dirección');
define('IMAGE_BUTTON_BACK', 'Atrás');
define('IMAGE_BUTTON_CHANGE_ADDRESS', 'Cambiar dirección');
define('IMAGE_BUTTON_CHECKOUT', 'Caja');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Comprar');
define('IMAGE_BUTTON_CONTINUE', 'Continuar ');
define('IMAGE_BUTTON_DELETE', 'Borrar');
define('IMAGE_BUTTON_LOGIN', 'Iniciar sesión');
define('IMAGE_BUTTON_IN_CART', 'Añadir a la cesta');
define('IMAGE_BUTTON_SEARCH', 'Buscar');
define('IMAGE_BUTTON_UPDATE', 'Actualizar');
define('IMAGE_BUTTON_UPDATE_CART', 'Actualizar la cesta de compra');
define('IMAGE_BUTTON_WRITE_REVIEW', '¡Su opinión!');
define('IMAGE_BUTTON_ADMIN', 'Admin');
define('IMAGE_BUTTON_PRODUCT_EDIT', 'Editar producto');
define('IMAGE_BUTTON_SEND', 'Enviar');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Seguir comprando');
define('IMAGE_BUTTON_CHECKOUT_STEP2', 'Ir al paso 2');
define('IMAGE_BUTTON_CHECKOUT_STEP3', 'Ir al paso 3');

define('SMALL_IMAGE_BUTTON_DELETE', 'Borrar');
define('SMALL_IMAGE_BUTTON_EDIT', 'Cambiar');
define('SMALL_IMAGE_BUTTON_VIEW', 'Mostrar');

define('ICON_ARROW_RIGHT', 'Mostrar más');
define('ICON_CART', 'Añadir a la cesta');
define('ICON_SUCCESS', 'Éxito');
define('ICON_WARNING', 'Aviso');
define('ICON_ERROR', 'Error');

define('TEXT_PRINT', 'Imprimir');

/*
 * 
 *  GREETINGS
 * 
 */

define('TEXT_GREETING_PERSONAL', 'Es bueno tenerle de vuelta, <span class="greetUser">%s!</span> ¿Quiere ver nuestros <a style="text-decoration:underline;" href="%s">artículos nuevos</a>?');
define('TEXT_GREETING_PERSONAL_RELOGON', '<small>Si no es  %s, por favor inicie sesión <a href="%s">aquí</a> con sus propios datos.</small>');
define('TEXT_GREETING_GUEST', 'Bienvenido <span class="greetUser">Visitante!</span> ¿Quiere <a href="%s">iniciar sesión</a>? ¿O quiere abrir una <a href="%s">cuenta de cliente</a> ?');

define('TEXT_SORT_PRODUCTS', 'El orden de los artículos es');
define('TEXT_DESCENDINGLY', 'descendente');
define('TEXT_ASCENDINGLY', 'ascendente');
define('TEXT_BY', ' hacia ');

define('TEXT_OF_5_STARS', '¡%s de 5 estrellas!');
define('TEXT_REVIEW_BY', 'de %s');
define('TEXT_REVIEW_WORD_COUNT', '%s palabras');
define('TEXT_REVIEW_RATING', 'Reseña: %s [%s]');
define('TEXT_REVIEW_DATE_ADDED', 'Añadido el: %s');
define('TEXT_NO_REVIEWS', 'No existen reseñas hasta el momento.');
define('TEXT_NO_NEW_PRODUCTS', 'No se han publicado nuevos artículos en los últimos '.MAX_DISPLAY_NEW_PRODUCTS_DAYS.' días. En vez de eso, aquí están los artículos publicados más recientes.');
define('TEXT_UNKNOWN_TAX_RATE', 'Tasa de impuesto desconocida');

/*
 * 
 * WARNINGS
 * 
 */

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Advertencia: El archivo de instalación se encuentra en: %s. ¡Por favor, borre este archivo por motivos de seguridad!');
define('WARNING_CONFIG_FILE_WRITEABLE', 'Advertencia: El software modified eCommerce de la tienda puede escribir en el fichero de configuración:  %s. Esto representa un posible riesgo de seguridad - por favor, ¡corrija los permisos de usuario para este archivo!');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Advertencia: El archivo para estas Sessions no existe:' . xtc_session_save_path() . '. ¡Las Sessions no funcionarán hasta que se haya creado el archivo!');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Advertencia: El software modified eCommerce de la tienda no puede escribir en el archivo Sessions: ' . xtc_session_save_path() . '. ¡Las Sessions no funcionarán hasta que se hayan establecidos los permisos correctos de usuario!');
define('WARNING_SESSION_AUTO_START', 'Advertencia: session.auto_start está activado (enabled) - Por favor, ¡desactive (disabled) este PHP Feature en php.ini y reinicie el servidor de WEB de nuevo!');
define('WARNING_DOWNLOAD_DIRECTORY_NON_EXISTENT', 'Advertencia: El archivo para el download del artículo no existe: ' . DIR_FS_DOWNLOAD . '. ¡Esta función no funcionará hasta que se haya creado un nuevo archivo!');

define('SUCCESS_ACCOUNT_UPDATED', 'Su cuenta ha sido actualizada con éxito.');
define('SUCCESS_PASSWORD_UPDATED', 'Su contraseña ha sido modificada con éxito!');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING', 'La contraseña introducida no coincide con la contraseña de su cuenta. Por favor vuelve a intentarlo.');
define('TEXT_MAXIMUM_ENTRIES', '<strong>Aviso:</strong> ¡Tiene a su disponibilidad %s registros de libreta de direcciones!');
define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED', 'El registro seleccionado ha sido borrado con éxito.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED', 'Su libreta de direcciones ha sido actualizada con éxito!');
define('WARNING_PRIMARY_ADDRESS_DELETION', 'La dirección estándar no se puede borrar. Por favor, primero elija otra dirección estándar. Después podrá borrar el registro.');
define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY', 'Este registro de la libreta de direcciones no existe.');
define('ERROR_ADDRESS_BOOK_FULL', 'Su libreta de direcciones no puede recibir más direcciones. Por favor, borre alguna dirección que ya no necesita. Después podrá guardar un nuevo registro.');
define('ERROR_CHECKOUT_SHIPPING_NO_METHOD', 'No se ha seleccionado un modo de envío.');
define('ERROR_CHECKOUT_SHIPPING_NO_MODULE', 'No existe un modo de envío.');

//  conditions check

define('ERROR_CONDITIONS_NOT_ACCEPTED', '* ¡Si no acepta nuestros términos y condiciones generales,lamentablemente no podemos recibir su pedido!');
define('ERROR_REVOCATION_NOT_ACCEPTED', '* ¡Si no acepta la expiración del derecho de revocación para los artículos virtuales, lamentablemente no podemos recibir su pedido!');

define('SUB_TITLE_OT_DISCOUNT', 'Descuento:');

define('TAX_ADD_TAX', 'incl. ');
define('TAX_NO_TAX', 'más ');

define('NOT_ALLOWED_TO_SEE_PRICES', 'Como Visitante (o mejor dich con su estado actual) no puede ver los precios.');
define('NOT_ALLOWED_TO_SEE_PRICES_TEXT', 'No tiene el permiso para ver los precios. Por favor, cree una cuenta de cliente.');

define('TEXT_DOWNLOAD', 'Download');
define('TEXT_VIEW', 'Ver');

define('TEXT_BUY', '1 x \'');
define('TEXT_NOW', '\' pedir');
define('TEXT_GUEST', 'Visitante');
define('TEXT_SEARCH_ENGINE_AGENT', 'Motor de búsqueda');

/*
 * 
 * ADVANCED SEARCH
 * 
 */

define('TEXT_ALL_CATEGORIES', 'Todas las categorías');
define('TEXT_ALL_MANUFACTURERS', 'Todos los fabricantes');
define('JS_AT_LEAST_ONE_INPUT', '* Es obligatorio rellenar uno de los siguientes campos:\nPalabras claves\nPrecio desde\nPrecio hasta\n');
define('AT_LEAST_ONE_INPUT', 'Es obligatorio rellenar uno de los siguientes campos:<br />Palabras claves con un mínimo de tres caracteres<br />Precio desde<br />Precio hasta<br />');
define('TEXT_SEARCH_TERM', 'Su búsqueda por:');
define('JS_INVALID_FROM_DATE', '* Fecha inválida (desde)\n ');
define('JS_INVALID_TO_DATE', '* Fecha inválida (hasta)\n ');
define('JS_TO_DATE_LESS_THAN_FROM_DATE', '* La fecha (desde) debe ser mayor o igual que la fecha (hasta)\n');
define('JS_PRICE_FROM_MUST_BE_NUM', '* \"Precio desde\" debe de ser una cifra \n\n');
define('JS_PRICE_TO_MUST_BE_NUM', '* \"Precio hasta\" debe de ser una cifra\n\n');
define('JS_PRICE_TO_LESS_THAN_PRICE_FROM', '* \"Precio hasta\" debe ser mayor o igual que precio desde.\n');
define('JS_INVALID_KEYWORDS', '* Término de búsqueda inválido\n');
define('TEXT_LOGIN_ERROR', '<b>ERROR:</b> La introducida \'dirección de correo electrónico\' y/o la \'contraseña\' no coinciden.');
//define('TEXT_NO_EMAIL_ADDRESS_FOUND', '<span class="color_error_message"><b>ACHTUNG:</b></span> Die eingegebene E-Mail-Adresse ist nicht registriert. Bitte versuchen Sie es noch einmal.'); // Not used anymore as we do not give a hint that an e-mail address is or is not in the database!
define('TEXT_PASSWORD_SENT', 'Su nueva contraseña ha sido enviada por correo electrónico.');
define('TEXT_PRODUCT_NOT_FOUND', '¡Artículo no encontrado!');
define('TEXT_MORE_INFORMATION', 'Para más información, por favor, visite la <a href="%s" onclick="window.open(this.href); return false;">página web</a> de este artículo.');
define('TEXT_DATE_ADDED', 'Este artículo ha sido incorporado el %s a nuestro catálogo.');
define('TEXT_DATE_AVAILABLE', '<span class="color_error_message">Este artículo probablemente estará nuevamente disponible a partir del %s.</span>');
define('SUB_TITLE_SUB_TOTAL', 'Subtotal:');

define('OUT_OF_STOCK_CANT_CHECKOUT', 'Los artículos marcados con ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' no se encuentran en stock en la cantidad deseada.<br />Por favor reduzca la cantidad de pedido para los artículos marcados. Muchas gracias.');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Los artículos marcados con ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' no se encuentran en stock en la cantidad deseada.<br />La cantidad pedida será entregada por nosotros a corto plazo. Si lo desea, también podemos hacer una entrega parcial.');

define('MINIMUM_ORDER_VALUE_NOT_REACHED_1', 'Desafortunadamente aún no ha alcanzado el valor mínimo de pedido de: ');
define('MINIMUM_ORDER_VALUE_NOT_REACHED_2', ' <br />Por favor, aumente su pedido por los menos por: ');
define('MAXIMUM_ORDER_VALUE_REACHED_1', 'Ha sobrepasado el valor maximo de pedido de: ');
define('MAXIMUM_ORDER_VALUE_REACHED_2', '<br /> Por favor reduzca su pedido por lo menos por: ');

define('ERROR_INVALID_PRODUCT', '¡El artículo seleccionado por usted no se ha encontrado!');

/*
 * 
 * NAVBAR TITLE
 * 
 */

define('NAVBAR_TITLE_ACCOUNT', 'Su cuenta');
define('NAVBAR_TITLE_1_ACCOUNT_EDIT', 'Su cuenta');
define('NAVBAR_TITLE_2_ACCOUNT_EDIT', 'Cambiar sus datos personales');
define('NAVBAR_TITLE_1_ACCOUNT_HISTORY', 'Su cuenta');
define('NAVBAR_TITLE_2_ACCOUNT_HISTORY', 'Sus pedidos realizados ');
define('NAVBAR_TITLE_1_ACCOUNT_HISTORY_INFO', 'Su cuenta');
define('NAVBAR_TITLE_2_ACCOUNT_HISTORY_INFO', 'Pedido realizado');
define('NAVBAR_TITLE_3_ACCOUNT_HISTORY_INFO', 'Número de pedido %s');
define('NAVBAR_TITLE_1_ACCOUNT_PASSWORD', 'Su cuenta');
define('NAVBAR_TITLE_2_ACCOUNT_PASSWORD', 'Cambiar contraseña');
define('NAVBAR_TITLE_1_ADDRESS_BOOK', 'Su cuenta');
define('NAVBAR_TITLE_2_ADDRESS_BOOK', 'Libreta de direcciones');
define('NAVBAR_TITLE_1_ADDRESS_BOOK_PROCESS', 'Su cuenta');
define('NAVBAR_TITLE_2_ADDRESS_BOOK_PROCESS', 'Libreta de direcciones');
define('NAVBAR_TITLE_ADD_ENTRY_ADDRESS_BOOK_PROCESS', 'Nuevo registro');
define('NAVBAR_TITLE_MODIFY_ENTRY_ADDRESS_BOOK_PROCESS', 'Cambiar registro');
define('NAVBAR_TITLE_DELETE_ENTRY_ADDRESS_BOOK_PROCESS', 'Borrar registro');
define('NAVBAR_TITLE_ADVANCED_SEARCH', 'Búsqueda avanzada');
define('NAVBAR_TITLE1_ADVANCED_SEARCH', 'Búsqueda avanzada');
define('NAVBAR_TITLE2_ADVANCED_SEARCH', 'Resultados de búsqueda');
define('NAVBAR_TITLE_1_CHECKOUT_CONFIRMATION', 'Caja');
define('NAVBAR_TITLE_2_CHECKOUT_CONFIRMATION', 'Confirmación');
define('NAVBAR_TITLE_1_CHECKOUT_PAYMENT', 'Caja');
define('NAVBAR_TITLE_2_CHECKOUT_PAYMENT', 'Modalidad de pago');
define('NAVBAR_TITLE_1_PAYMENT_ADDRESS', 'Caja');
define('NAVBAR_TITLE_2_PAYMENT_ADDRESS', 'Cambiar dirección de factura');
define('NAVBAR_TITLE_1_CHECKOUT_SHIPPING', 'Caja');
define('NAVBAR_TITLE_2_CHECKOUT_SHIPPING', 'Información sobre el envío');
define('NAVBAR_TITLE_1_CHECKOUT_SHIPPING_ADDRESS', 'Caja');
define('NAVBAR_TITLE_2_CHECKOUT_SHIPPING_ADDRESS', 'Cambiar dirección de envío');
define('NAVBAR_TITLE_1_CHECKOUT_SUCCESS', 'Caja');
define('NAVBAR_TITLE_2_CHECKOUT_SUCCESS', 'Èxito');
define('NAVBAR_TITLE_CREATE_ACCOUNT', 'Crear cuenta');
if (isset($navigation) && $navigation->snapshot['page'] == FILENAME_CHECKOUT_SHIPPING) {
define('NAVBAR_TITLE_LOGIN', 'Pedir');
} else {
define('NAVBAR_TITLE_LOGIN', 'Iniciar sesión');
}
define('NAVBAR_TITLE_LOGOFF', '¡Hasta la próxima!');
define('NAVBAR_TITLE_PRODUCTS_NEW', 'Artículos nuevos');
define('NAVBAR_TITLE_SHOPPING_CART', 'Cesta de la compra');
define('NAVBAR_TITLE_SPECIALS', 'Ofertas');
define('NAVBAR_TITLE_COOKIE_USAGE', 'Uso de cookies');
define('NAVBAR_TITLE_PRODUCT_REVIEWS', 'Reseñas');
define('NAVBAR_TITLE_REVIEWS_WRITE', 'Reseñas');
define('NAVBAR_TITLE_REVIEWS', 'Reseñas');
define('NAVBAR_TITLE_SSL_CHECK', 'Aviso de seguridad');
define('NAVBAR_TITLE_CREATE_GUEST_ACCOUNT', 'Su cuenta de cliente');
define('NAVBAR_TITLE_PASSWORD_DOUBLE_OPT', '¿Contraseña olvidada?');
define('NAVBAR_TITLE_NEWSLETTER', 'Newsletter');
define('NAVBAR_GV_REDEEM', 'Canjear cupón');
define('NAVBAR_GV_SEND', 'Enviar un cupón');
define('NAVBAR_TITLE_DOWNLOAD', 'Downloads');

/*
 * 
 *  MISC
 * 
 */

define('TEXT_NEWSLETTER', '¿Quiere estar siempre al día?<br />No hay problema, inscríbase en nuestro newsletter y le mantendremos siempre informada/o.');
define('TEXT_EMAIL_INPUT', 'Su dirección de correo electrónico ha sido introducida en nuestro sistema.<br />Al mismo tiempo el sistema le ha enviado un correo electrónico con el enlace de activación. Por favor, después de haber recibido el correo electrónico, haga clic en el enlace para confirmar su registro. ¡De lo contrario no recibirá nuestro newsletter!');

define('TEXT_WRONG_CODE', 'El código de seguridad introducido no coincide con el código mostrado. Por favor, inténtalo de nuevo.');
define('TEXT_EMAIL_EXIST_NO_NEWSLETTER', '¡Esta dirección de correo electrónico existe ya en nuestra base de datos, pero aún no está activada para el recibo del newsletter!');
define('TEXT_EMAIL_EXIST_NEWSLETTER', '¡Esta dirección de correo electrónico existe ya en nuestra base de datos y ya está activada para el recibo del newsletter!');
define('TEXT_EMAIL_NOT_EXIST', '¡Esta dirección de correo electrónico no existe en nuestra base de datos!');
define('TEXT_EMAIL_DEL', 'Su dirección de correo electrónico se ha borrado de nuestra base de datos.');
define('TEXT_EMAIL_DEL_ERROR', 'Se ha producido un error, su dirección de correo electrónico no se ha borrado.');
define('TEXT_EMAIL_ACTIVE', '¡Su dirección de correo electrónico ha sido activada con éxito para el recibo del newsletter!');
define('TEXT_EMAIL_ACTIVE_ERROR', '¡Se ha producido un error, su dirección de correo electrónico no ha sido activada.');
define('TEXT_EMAIL_SUBJECT', 'Su inscripción al newsletter');

define('TEXT_CUSTOMER_GUEST', 'Visitante');

define('TEXT_LINK_MAIL_SENDED', 'Su solicitud de una nueva contraseña primero debe ser confirmada por usted.<br />Por eso el sistema le ha enviado un correo electrónico con un enlace de confirmación. Por favor, después de haber recibido el correo electrónico, haga clic en el enlace enviado. ¡De lo contrario no podrá asignar una nueva contraseña!<br/><br/>El enlace de confirmación es válido por %s minutos.');
define('TEXT_PASSWORD_MAIL_SENDED', 'Un correo electrónico con una contraseña de inicio de sesión le ha sido enviada apenas.<br / Por favor, cambie en su próximo inicio de sesión la contraseña como deseada.');
define('TEXT_CODE_ERROR', 'Por favor introduzca nuevamente su dirección de correo electrónico y el código de seguridad. <br />¡Tenga cuidado con los errores tipográficos!');
define('TEXT_EMAIL_ERROR', 'Por favor introduzca nuevamente su dirección de correo electrónico. <br />¡Tenga cuidado con los errores tipográficos!');
define('TEXT_NO_ACCOUNT', 'Sentimos comunicarle que su solicitud para una nueva contraseña de inicio de sesión o ha sido inválida o se ha pasado de fecha.<br />Por favor inténtelo de nuevo.');
define('HEADING_PASSWORD_FORGOTTEN', '¿Contraseña olvidada?');
define('TEXT_PASSWORD_FORGOTTEN', 'Cambie su contraseña en tres simples pasos.');
define('TEXT_EMAIL_PASSWORD_FORGOTTEN', 'Correo de confirmación del cambio de contraseña');
define('TEXT_EMAIL_PASSWORD_NEW_PASSWORD', 'Su nueva contraseña');
define('ERROR_MAIL', 'Por favor compruebe sus datos introducidos en el formulario');

define('CATEGORIE_NOT_FOUND', 'Categoría no ha sido encontrada');

define('GV_FAQ', 'preguntas frecuentes de cupones ');
define('ERROR_NO_REDEEM_CODE', 'Lamentablemente no ha introducido ningún código.');
define('ERROR_NO_INVALID_REDEEM_GV', 'Código del cupón inválido');
define('TABLE_HEADING_CREDIT', 'Saldo');
define('EMAIL_GV_TEXT_SUBJECT', 'Un regalo de %s');
define('MAIN_MESSAGE', 'Ha decidido enviar un cupón por un valor de %s a %s cuya dirección de correo electrónico es %s .<br /><br />El siguiente texto aparecerá en su correo electrónico:<br /><br />Hola %s,<br /><br />Se les ha enviado un cupón por un valor de %s por %s .');
define('REDEEMED_AMOUNT', 'Su cupón ha sido abonado con éxito en su cuenta. Valor del cupón: %s');
define('REDEEMED_COUPON', 'Su cupón ha sido anotado con éxito y será canjeado automáticamente en su próximo pedido.');

define('ERROR_INVALID_USES_USER_COUPON', 'Usted puede canjear este cupón sólo ');
define('ERROR_INVALID_USES_COUPON', 'Este cupón clientes sólo pueden canjear ');
define('TIMES', ' veces.');
define('ERROR_INVALID_STARTDATE_COUPON', 'Su cupón aún no está disponible.');
define('ERROR_INVALID_FINISDATE_COUPON', 'Su cupón ha caducado.');
define('ERROR_INVALID_MINIMUM_ORDER_COUPON', '¡Este cupón sólo puede ser canjeado a partir de un valor mínimo de pedido de %s!');
define('ERROR_INVALID_MINIMUM_ORDER_COUPON_ADD', '<br/>¡Tiene que introducir el código del cupón nuevamente al llegar al valor mínimo de pedido!');
define('PERSONAL_MESSAGE', '%s escribe: ');

/*
 * 
 *  COUPON POPUP
 * 
 */
 
define('TEXT_CLOSE_WINDOW', 'Cerrar ventana [x]');
define('TEXT_COUPON_HELP_HEADER', 'Su cupón ha sido abonado con éxito.');
define('TEXT_COUPON_HELP_NAME', '<br /><br />Título del cupón: %s');
define('TEXT_COUPON_HELP_FIXED', '<br /><br />El valor del cupón es %s ');
define('TEXT_COUPON_HELP_MINORDER', '<br /><br />El valor minimo de pedido es %s ');
define('TEXT_COUPON_HELP_FREESHIP', '<br /><br />Cupon para envio gratuito');
define('TEXT_COUPON_HELP_DESC', '<br /><br />Descripción del cupón: %s');
define('TEXT_COUPON_HELP_DATE', '<br /><br />Este cupón es valido de %s a %s');
define('TEXT_COUPON_HELP_RESTRICT', '<br /><br />artículos / Restricciones de categoría');
define('TEXT_COUPON_HELP_CATEGORIES', 'Categoria');
define('TEXT_COUPON_HELP_PRODUCTS', 'Artículo');
define('ERROR_ENTRY_AMOUNT_CHECK', 'Valor de cupón inválido');
define('ERROR_ENTRY_EMAIL_ADDRESS_CHECK', 'Dirección de correo electrónico inválida');

// VAT Reg No
define('ENTRY_VAT_TEXT', ''); //'¡Solamente para Alemania y países de la Unión Europea!');
//BOC new error message, noRiddle
//define('ENTRY_VAT_ERROR', 'Die eingegebene USt-IdNr. ist ung&uuml;ltig oder kann derzeit nicht &uuml;berpr&uuml;ft werden! Bitte geben Sie eine g&uuml;ltige ID ein oder lassen Sie das Feld zun&auml;chst leer.');
define('ENTRY_VAT_ERROR', 'El número del IVA introducido no es válido o no puede ser comprobado en este momento! Por favor, introduzca un <a href="https://en.wikipedia.org/wiki/VAT_identification_number" target="_blank">número de IVA válido</a> (los primeros dos caracteres determinan el país) o deja este campo vacío.');
//EOC new error message, noRiddle
define('MSRP', 'precio de cliente');
define('YOUR_PRICE', 'Su precio ');
define('UNIT_PRICE', 'Precio por unidad');
define('ONLY', ' Ahora sólo ');
define('FROM', 'Desde ');
define('YOU_SAVE', 'Usted ahorra ');
define('INSTEAD', 'Nuestro precio previo ');
define('TXT_PER', ' por ');
define('TAX_INFO_INCL', 'incl. %s I.V.A.');
define('TAX_INFO_EXCL', 'excl. %s I.V.A.');
define('TAX_INFO_ADD', 'más %s I.V.A.');
define('SHIPPING_EXCL', 'más');
define('SHIPPING_INCL', 'incl.');
define('SHIPPING_COSTS', 'Costo de envío');

define('SHIPPING_TIME', 'Tiempo de entrega:');
define('MORE_INFO', '[Más]');

define('ENTRY_PRIVACY_ERROR', '¡Por favor acepte nuestra política de privacidad!');
define('TEXT_PAYMENT_FEE', 'Tarifa de pago');

define('_MODULE_INVALID_SHIPPING_ZONE', 'Desafortunadamente el envío a este país no es posible');
define('_MODULE_UNDEFINED_SHIPPING_RATE', 'Los costos de envío no pueden ser calculados en este momento');

define('NAVBAR_TITLE_1_ACCOUNT_DELETE', 'Su cuenta');
define('NAVBAR_TITLE_2_ACCOUNT_DELETE', 'Borrar cuenta');
	
//contact-form error messages
//BOC commented out double defines, others in contact_us.php, noRiddle
//define('ERROR_EMAIL','<p><b>Ihre E-Mail-Adresse:</b> Keine oder ung&uuml;ltige Eingabe!</p>');
//define('ERROR_VVCODE','<p><b>Sicherheitscode:</b> Keine &Uuml;bereinstimmung, bitte geben Sie den Sicherheitscode erneut ein!</p>');
//define('ERROR_MSG_BODY','<p><b>Ihre Nachricht:</b> Keine Eingabe!</p>');
//EOC commented out double defines, others in contact_us.php, noRiddle

//Table Header checkout_confirmation.php
define('HEADER_QTY', 'Cantidad');
define('HEADER_ARTICLE', 'Artículo');
define('HEADER_SINGLE', 'Precio unitario');
define('HEADER_TOTAL', 'Suma');
define('HEADER_MODEL', 'Número de artículo');

### PayPal API Modul
define('NAVBAR_TITLE_PAYPAL_CHECKOUT', 'Cerrar sesión de PayPal');
define('PAYPAL_ERROR', 'Cancelar PayPal');
define('PAYPAL_NOT_AVIABLE', 'PayPal Express no está disponible actualmente.<br />Por favor, seleccione otra modalidad de pago<br />o inténtelo nuevamente más tarde.<br />Gracias por su compresión.<br />');
define('PAYPAL_FEHLER', 'PayPal ha notificado de un error durante el procesamiento.<br />Su pedido está guardado, pero no se ejecutará.<br />Por favor, introduzca un nuevo pedido.<br />Gracias por su compresión.<br />');
define('PAYPAL_WARTEN', 'PayPal ha notificado de un error durante el procesamiento.<br />Tiene que volver a PayPal para pagar el pedido.<br />Abajo puede ver el pedido guardado.<br />Gracias por su compresión.<br />Por favor, haga clic nuevamente en el botón PayPal Express.<br />');
define('PAYPAL_NEUBUTTON', 'Por favor, haga clic nuevamente para pagar el pedido.<br />Todas las demás teclas lleva a la cancelación del pedido.');
define('ERROR_ADDRESS_NOT_ACCEPTED', '* Mientras no acepta su dirección de factura y de envío,\n¡lamentablemente no podemos recibir su pedido!\n\n');
define('PAYPAL_GS', 'Cupón');
define('PAYPAL_TAX', 'IVA');
define('PAYPAL_EXP_WARN', '¡Atención! Costos de envío que posiblemente podrán surgir se calculan recién en la tienda definitivamente.');
define('PAYPAL_EXP_VORL', 'Costos de envío provisionales');
define('PAYPAL_EXP_VERS', '6.90');
define('PAYPAL_ADRESSE', 'El país de su dirección de envío de PayPal no está registrado en nuestra tienda.<br />Por favor, póngase en contacto con nosotros.<br />Gracias por su compresión.<br />País recibido de PayPal: ');
define('PAYPAL_AMMOUNT_NULL', 'El total del encargo revisto (sin envío) es igual a 0.<br />Por eso PayPal Express no se encuentra a disponibilidad.<br />Por favor, elija otra modalidad de pago.<br />Gracias por su compresión.<br />');
### PayPal API Modul

define('BASICPRICE_VPE_TEXT', 'con esta cantidad solamente ');
define('GRADUATED_PRICE_MAX_VALUE', 'desde');
define('_SHIPPING_TO', 'Envío a ');

define('ERROR_SQL_DB_QUERY', 'Lo sentimos, pero se ha producido un error en la base de datos.');
define('ERROR_SQL_DB_QUERY_REDIRECT', '¡Será dirigido a nuestra página web en %s secundos!');

define('TEXT_AGB_CHECKOUT', 'Por favor, tenga en cuenta nuestros términos y condiciones generales y la información al cliente %s como también nuestra política de cancelación %s.');
define('DOWNLOAD_NOT_ALLOWED', '<h1>Prohibido</h1>Este servidor no ha podido verificar que está autorizado a acceder el archivo solicitado. O bien ha proporcionado las credenciales equivocadas (por ejemplo una contraseña incorrecta) o su navegador no sabe como proporcionar las credenciales solicitadas.');

define('TEXT_INFO_DETAILS', ' Detalles');
define('TEXT_SAVED_BASKET', 'Por favor, revise su cesta de compra. Este todavía contiene artículos de una visita anterior.');
//define('TEXT_PRODUCTS_QTY_REDUCED', 'Die maximal erlaubte St&uuml;ckzahl f&uuml;r den zuletzt hinzugef&uuml;gten bzw. ge&auml;nderten Artikel wurde &uuml;berschritten. Die St&uuml;ckzahl wurde automatisch auf die maximal erlaubte St&uuml;ckzahl reduziert.'); // Now we use MAX_PROD_QTY_EXCEEDED

define('ERROR_REVIEW_TEXT', 'El texto de reseña debe contener mínimo ' . REVIEW_TEXT_MIN_LENGTH . ' caracteres.');
define('ERROR_REVIEW_RATING', 'Por favor, haga una evaluación.');
define('ERROR_REVIEW_AUTHOR', 'Por favor, introduzca su nombre.');

define('GV_NO_PAYMENT_INFO', '<div class="infomessage">Puede pagar el pedido completamente con su saldo. ¡Si no quiere canjear su saldo, desactive la selección de saldo y elija una modalidad de pago!</div>');
define('GV_ADD_PAYMENT_INFO', '<div class="errormessage">Su saldo no alcanza o no puede ser aplicado a todas los puestos para pagar el pedido completamente. ¡Por favor elija además otra modalidad de pago!</div>');

define('_SHIPPING_FREE', 'Libre de costo de envío');

define('TEXT_CONTENT_NOT_FOUND', '¡No se ha encontrado esta página!');
define('TEXT_SITE_NOT_FOUND', '¡No se ha encontrado esta página!');

// error message for exceeded product quantity, noRiddle
define('MAX_PROD_QTY_EXCEEDED', 'El número máximo de piezas permitido ' .MAX_PRODUCTS_QTY. ' para <span style="font-style:italic;">"%s"</span> fue sobrepasado.<br />El número de piezas se ha reducido automáticamente al número de piezas permitido.');

define('IMAGE_BUTTON_CONTENT_EDIT', 'Editar Content');
define('PRINTVIEW_INFO', 'Imprimir hoja de datos del artículo');
define('PRODUCTS_REVIEW_LINK', 'Escribir reseña');

define('TAX_INFO_SMALL_BUSINESS', 'Precio final según § 19 UStG.');
define('TAX_INFO_SMALL_BUSINESS_FOOTER', 'Debido al estatus de pequeña empresa según § 19 UStG no cobramos ningún impuesto sobre las ventas y por lo tanto tampoco lo revelamos.');

define('NEED_CHANGE_PWD', 'Por favor cambie su contraseña.');
define('TEXT_REQUEST_NOT_VALID', 'El enlace ha caducado. Por favor, solicite una nueva contraseña.');

define('NAVBAR_TITLE_WISHLIST', 'Lista de favoritos');
define('TEXT_TO_WISHLIST', 'Añadir a lista de favoritos');
define('IMAGE_BUTTON_TO_WISHLIST', 'Añadir a lista de favoritos');

define('GUEST_REDEEM_NOT_ALLOWED', 'Visitantes no pueden canjear cupones.');
define('GUEST_VOUCHER_NOT_ALLOWED', 'Cupones no se pueden comprar como visitante.');

define('TEXT_FILTER_SETTING_DEFAULT', 'Artículos por página');
define('TEXT_FILTER_SETTING', '%s artículos por página');
define('TEXT_FILTER_SETTING_ALL', 'Mostrar todos los artículos');
define('TEXT_SHOW_ALL', ' (mostrar todos)');
define('TEXT_FILTER_SORTING_DEFAULT', 'Clasificar por ...');
define('TEXT_FILTER_SORTING_ABC_ASC', 'A hasta Z');
define('TEXT_FILTER_SORTING_ABC_DESC', 'Z hasta A');
define('TEXT_FILTER_SORTING_PRICE_ASC', 'Precio ascendente');
define('TEXT_FILTER_SORTING_PRICE_DESC', 'Precio descendente');
define('TEXT_FILTER_SORTING_DATE_DESC', 'Productos más nuevos primero');
define('TEXT_FILTER_SORTING_DATE_ASC', 'Productos más antiguos primero');
define('TEXT_FILTER_SORTING_ORDER_DESC', 'Lo más vendido');

define('NAVBAR_TITLE_ACCOUNT_CHECKOUT_EXPRESS_EDIT', 'Configuración para Mi compra rápida');
define('SUCCESS_CHECKOUT_EXPRESS_UPDATED', 'La configuración para Mi compra rápida han sido guardados.');
define('TEXT_ERROR_CHECKOUT_EXPRESS_SHIPPING_ADDRESS', 'Por favor elija una dirección de envío.');
define('TEXT_ERROR_CHECKOUT_EXPRESS_SHIPPING_MODULE', 'Por favor elija un modo de envío.');
define('TEXT_ERROR_CHECKOUT_EXPRESS_PAYMENT_ADDRESS', 'Por favor elija una dirección de factura.');
define('TEXT_ERROR_CHECKOUT_EXPRESS_PAYMENT_MODULE', 'Por favor elija un modalidad de pago.');
define('TEXT_CHECKOUT_EXPRESS_INFO_LINK', 'Mi compra rápida');
define('TEXT_CHECKOUT_EXPRESS_INFO_LINK_MORE', 'Más información acerca Mi compra rápida »');
define('TEXT_CHECKOUT_EXPRESS_CHECK_CHEAPEST', 'Elijir siempre el modo de envió más económico');

define('AC_SHOW_PAGE', 'página ');
define('AC_SHOW_PAGE_OF', ' desde ');

define('FREE_SHIPPING_INFO', 'desde el valor de pedido %s  mandamos su pedido libre de costos de envío');

define('MANUFACTURER_NOT_FOUND', 'Fabricante no encontrado');
define('ENTRY_TOKEN_ERROR', 'Por favor, compruebe sus entradas.');

define('IMAGE_BUTTON_CONFIRM', 'Confirmar');

// ***************************************************
//  Kontodaten-Pr
// ***************************************************
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_0', 'Los detalles bancarios están bien.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1', '¡El número de cuenta y/o el código bancario son inválidos o no coinciden!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2', 'El número de cuenta no es automáticamente verificable.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_3', 'El número de cuenta no es verificable.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_4', '¡Número de cuenta no verificable! Por favor, compruebe sus datos nuevamente.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_5', 'Este código bancario no existe, por favor corrija su entrada.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_8', '¡Error con el códiogo bancario o no código bancario indicado!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_9', '¡Ningún número de cuenta indicado!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_10', 'No ha indicado un titular de cuenta.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_128', 'Error interno con la verificación de los detalles bancarios.');

// Fehlermeldungen alle IBAN-Nummern 
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1000', 'Código del país contenido en el IBAN (1a y 2a posición) desconocido.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1010', 'Longitud de IBAN incorrecta: Demasiados dígitos introducidos.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1020', 'Longitud de IBAN incorrecta: Demasiado pocos dígitos introducidos.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1030', 'El IBAN no corresponde al formato definido para el país.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1040', 'Los dígitos de control del IBAN (dígitos 3 y 4) no son correctos -> error de escritura en el IBAN.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1050', 'El código bancario tiene un formato inválido.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1060', 'Longitud del código bancario incorrecta: Demasiados caracteres introducidos. 8 u 11 caracteres son requeridos.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1070', 'Longitud del código bancario incorrecta: Demasiados pocos caracteres introducidos. 8 u 11 caracteres son requeridos.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1080', 'Longitud del código bancario inválido: 8 u 11 caracteres son requeridos.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1200', 'Lamentablemente, no podemos aceptar IBAN del país indicado (1a y 2a posición del IBAN).');

// Fehlermeldungen f
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2001', 'El número de cuenta (dígitos 13 a 22) y/o el código bancario (dígitos 5 a 12) contenidos en el IBAN son inválidos o no coinciden.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2002', 'El número de cuenta (dígitos 13 a 22) contenido en el IBAN no es automáticamente verificable.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2003', 'Para el número de cuenta (dígitos 13 a 22) contenido en el IBAN no se ha definido un procedimiento de dígito de control.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2004', 'El número de cuenta (dígitos 13 a 22) contenido en el IBAN no es verificable.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2005', 'El código bancario (dígitos 5 a 12 en el IBAN) no existe.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2008', '¡Error con el código bancario (dígitos 5 a 12 en el IBAN) o ningún código bancario indicado!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2009', '¡Ningún número de cuenta (dígitos 13 a 22 en el IBAN) indicado!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2010', 'Ningún titular de cuenta indicado.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2020', 'Código bancario inválido: No existe un banco con este código bancario.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2128', 'Error interno con la verificación de los detalles bancarios.');

define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_UNKNOWN', 'Error desconodido con la verificación de los detalles bancarios.');

define('PRODUCT_REVIEWS_SUCCESS', 'Muchas gracias por su reseña.');
define('PRODUCT_REVIEWS_SUCCESS_WAITING', 'Muchas gracias por su reseña. Esta se comprueba ahora antes de que se libere.');
?>

<?php
/***************************************
* file: do_application_top_end.php
* use: do what you need here ;-)
* (c) noRiddle 05-2017
***************************************/

//BOC set new session params for products_model and products_name to use it everywhere in template
if(isset($_GET['model']) && $_GET['model'] == 1) {
    //echo '<br /><br /><br /><pre>'.print_r($product->data, true).'</pre>';
    $_SESSION['our_prod_model'] = $product->data['products_model'];
    $_SESSION['our_prod_name'] = $product->data['products_name'];
} else {
    if(isset($_SESSION['our_prod_name'])) {
        unset($_SESSION['our_prod_name']);
    }
    if(isset($_SESSION['our_prod_model'])) {
        unset($_SESSION['our_prod_model']);
    }
}
//EOC set new session params for products_model and products_name to use it everywhere in template

//BOC set club shop arrays
$club_shop_array = array('audi'.$shop_suffix, 'bmw'.$shop_suffix, 'bmw-motorrad'.$shop_suffix, 'landrover'.$shop_suffix, 'nissan'.$shop_suffix, 'volvo'.$shop_suffix);
$club_name_array = array('Audi TT Club', 'Syndikat', 'BMW Bike Club', 'Landrover Club', 'SXCE', 'Volvo Club');
//EOC set club shop arrays

//BOC define array for guest-status-Ids (read out from configuration group 17 GUEST_STATUS_IDS)
$guest_status_array = explode(',', GUEST_STATUS_IDS);
//EOC define array for guest-status-Ids (read out from configuration group 17 GUEST_STATUS_IDS)

//BOC define mapping array for customers status with verified UstIDs
$cust_vatid_status_array = array('2' => '2', '9' => '8', '11' => '6', '12' => '6', '13' => '8');
//EOC define mapping array for customers status with verified UstIDs

//BOC build array of customers statuses which we will show a logged in client, noRiddle
$which_cust_stat_show = array('3', '4', '5', '7', '15', '16', '17');
//EOC build array of customers statuses which we will show a logged in client, noRiddle

//BOC catalogue for BMW group
if(in_array($shop, $bmw_group_shops_ctlg)) { //$bmw_group_shops_ctlg defined in application_top_begin
    include DIR_FS_CATALOG.'mm_mod/pulldown_cars_header.php';
}
//EOC catalogue for BMW group

//BOC build cart link for order in other shop
if(defined('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS')) {
    if(MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS == 'true') {
        if(strpos($PHP_SELF, 'shopping_cart.php') !== false) {
            $redir_shop = MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP;
            $foreign_shop = $redir_shop.$shop_suffix;
            //echo '<br><br><br><pre>'.$foreign_shop.'</pre>';
            //$redirect_model_str = '';
            $redirect_model_qty_arr = array();
            
            $ext_shp_db_link = mysqli_connect($data20[$foreign_shop]['sql_server'], $data20[$foreign_shop]['sql_user'], $data20[$foreign_shop]['sql_pass'], $data20[$foreign_shop]['sql_db']);
            if (!$ext_shp_db_link) echo '<br><br><br>Connection to "'.$foreign_shop.'" couldn\'t be established: ' . mysqli_error($ext_shp_db_link);
            if (!mysqli_select_db($ext_shp_db_link, $data20[$foreign_shop]['sql_db'])) echo '<br><br><br>Couldn\'t select database for "'.$foreign_shop.' '.mysqli_error($ext_shp_db_link);
            mysqli_set_charset($ext_shp_db_link, 'utf8');
        }
    }
}
//EOC build cart link for order in other shop

// inclusion for affiliate program
require(DIR_WS_INCLUDES . 'affiliate_application_top.php');

//BOC get laximo data from db, noRiddle
if(!isset($_SESSION['nr_ctlg_brands'])) {
    $lax_data_array = array();
    $lax_qu = xtc_db_query("SELECT * FROM laximo_data_helper");
    while($lax_data = xtc_db_fetch_array($lax_qu)) {
        $lax_data_array[$lax_data['c_param']] = array('lax_brand' => $lax_data['lax_brand'],
                                                      'lax_name' => $lax_data['lax_name'],        
                                                      'our_brand' => $lax_data['our_brand'], 
                                                      'brand_icon' => $lax_data['brand_icon'],
                                                      'coid' => $lax_data['coid'] //added content group to link icons to content when not logged in, 08-217, noRiddle
                                                     );
    }
    $_SESSION['nr_ctlg_brands'] = $lax_data_array;
    //echo '<br /><br /><pre>'.print_r($_SESSION['nr_ctlg_brands'], true).'</pre>';
}
//EOC get laximo data from db, noRiddle

//BOC redirect to start page in case someone uses old c GET parameter in URL
if(isset($_GET['c']) && !array_key_exists($_GET['c'], $_SESSION['nr_ctlg_brands'])) {
    xtc_redirect(xtc_href_link(FILENAME_DEFAULT));
}
//EOC redirect to start page in case someone uses old c GET parameter in URL

//BOC instantiate new price class for prices from shops
if(isset($_GET['c']) && array_key_exists($_GET['c'], $_SESSION['nr_ctlg_brands'])) {
    include_once(DIR_FS_CATALOG.'nr_catalogue_files/guayaquillib/render/details/shop_dbs/conn_to_shop_db.php');
}
include_once(DIR_WS_CLASSES.'nr_foreign_xtcPrice.php');
$nr_foreign_xtcPrice = new nr_foreign_xtcPrice($_SESSION['currency'], $_SESSION['customers_status']['customers_status_id']); //instantiate price class for respective shop
//EOC instantiate new price class for prices from shops

//BOC get an array of all activated countries in shops, noRiddle
require_once(DIR_FS_INC . 'nr_get_activated_countries.php');
$nr_activated_states = nr_get_activated_countries($remote_url[$cat_env]['country_shop'].$remote_url[$cat_env]['shop_suffix']);
//EOC get an array of all activated countries, noRiddle
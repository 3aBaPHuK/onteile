<?php
/**
 * 888888ba                 dP  .88888.                    dP                
 * 88    `8b                88 d8'   `88                   88                
 * 88aaaa8P' .d8888b. .d888b88 88        .d8888b. .d8888b. 88  .dP  .d8888b. 
 * 88   `8b. 88ooood8 88'  `88 88   YP88 88ooood8 88'  `"" 88888"   88'  `88 
 * 88     88 88.  ... 88.  .88 Y8.   .88 88.  ... 88.  ... 88  `8b. 88.  .88 
 * dP     dP `88888P' `88888P8  `88888'  `88888P' `88888P' dP   `YP `88888P' 
 *
 *                          m a g n a l i s t e r
 *                                      boost your Online-Shop
 *
 * -----------------------------------------------------------------------------
 * $Id: dhl_meinpaket.php -1   $
 *
 * (c) 2010 RedGecko GmbH -- http://www.redgecko.de
 *     Released under the GNU General Public License v2 or later
 * -----------------------------------------------------------------------------
 */

define('MODULE_SHIPPING_DHLMEINPAKET_TEXT_TITLE', 'Envoi avec DHL (MeinPaket)');
define('MODULE_SHIPPING_DHLMEINPAKET_TEXT_DESCRIPTION', 'Envoi avec DHL fixé par MeinPaket');
define('MODULE_SHIPPING_DHLMEINPAKET_SORT_ORDER', 'Tri');
define('MODULE_SHIPPING_DHLMEINPAKET_TEXT_WAY', MODULE_SHIPPING_DHLMEINPAKET_TEXT_DESCRIPTION);
define('MODULE_SHIPPING_DHLMEINPAKET_ALLOWED_TITLE' , 'Zones permises');
define('MODULE_SHIPPING_DHLMEINPAKET_ALLOWED_DESC' , 'Indiquez les zones <b>eindividuellement</b> dans lesquelles vous voulez permettre l\'envoi');
define('MODULE_SHIPPING_DHLMEINPAKET_STATUS_TITLE', 'Définition de l\'envoi par MeinPaket');
define('MODULE_SHIPPING_DHLMEINPAKET_STATUS_DESC', 'Voulez-vous proposer une définition d\'envoi par DHL MeinPaket?');
define('MODULE_SHIPPING_DHLMEINPAKET_SORT_ORDER_TITLE', 'Ordre de tri');
define('MODULE_SHIPPING_DHLMEINPAKET_SORT_ORDER_DESC', 'Ordre de l\'affichage');

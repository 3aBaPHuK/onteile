<?php
/*******************************************************
* file: affiliate_module.php
* use: language file for system module
* (c) noRiddle 07-2017
* Released under the GNU General Public License
*******************************************************/

define('MODULE_AFFILIATE_MODULE_TEXT_TITLE', 'Module d\'affiliation');
define('MODULE_AFFILIATE_MODULE_TEXT_DESCRIPTION', 'Met à disposition l\'élargissement d\'affiliation.');
define('MODULE_AFFILIATE_MODULE_STATUS_TITLE', 'Activer le module?');
define('MODULE_AFFILIATE_MODULE_STATUS_DESC', 'Activer le module d\'affiliation');
?>

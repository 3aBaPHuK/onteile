<?php
/*******************************************
* file: use_model_in_products_url.php
* use: 
* (c) mm and noRiddle 2016 / 01-2017
*******************************************/

if(isset($_GET['model']) && $_GET['model'] == 1) {
    $sql = "SELECT products_id from products WHERE TRIM(products_model) = '". xtc_db_input($_GET['products_id'])."'";  //set TRIM since in some shops products_model has got white space, noRiddle
    $q_result = xtc_db_query($sql);
    $products_id_array = xtc_db_fetch_array($q_result);
    //print_r ($products_id_array);
    $products_id = $products_id_array['products_id'];

    $_GET['products_id'] = $products_id;
}

//BOC define available laximo languages (used in /includes/modules/set_language_sessions.php)
$lax_avail_langs = array('en', //english
                         'cn', //chinese
                         'tr', //turkish
                         'fr', //french
                         'de', //german
                         'hi', //hindi
                         'ru', //russian
                         'es', //spanish
                         'jp', //japanese
                         'nl', //dutch
                         'us', //english
                         'gr', //greek
                         'it', //italian
                         'kr', //korean
                         'pl', //polish
                         'pt', //portugese
                         'se', //swedish
                         'th', //thai
                         'tw', //traditional chinese (taiwanese ?)
                         'cz', //czech
                         'dk', //denmark
                         'fi', //finish
                         'hu', //hungarian
                         'ro', //romanian
                         'hr', //croation
                         'ee', //estonian
                         'lv', //latvian
                         'lt', //lithuanian
                         'bg', //bulgarian
                         'sk' //slovakian
                        );
//EOC define available laximo languages (used in /includes/modules/set_language_sessions.php)
?>
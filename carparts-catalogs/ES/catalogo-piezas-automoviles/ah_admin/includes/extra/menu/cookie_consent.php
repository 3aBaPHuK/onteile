<?php
if (defined('MODULE_COOKIE_CONSENT_STATUS')) {
  $add_contents[BOX_HEADING_CONFIGURATION][] = array( 
    'admin_access_name' => 'cookie_consent',
    'filename' => FILENAME_COOKIE_CONSENT,
    'boxname' => 'Cookie Consent', 
    'parameters' => '',
    'ssl' => ''
  );
}

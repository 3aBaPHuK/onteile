<?php
  /* --------------------------------------------------------------
   $Id: manufacturers.php 5850 2013-09-30 09:37:43Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(manufacturers.php,v 1.52 2003/03/22); www.oscommerce.com
   (c) 2003	nextcommerce (manufacturers.php,v 1.9 2003/08/18); www.nextcommerce.org
   (c) 2006 XT-Commerce (manufacturers.php 901 2005-04-29)

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Fabricant');

define('TABLE_HEADING_MANUFACTURERS', 'Fabricant');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_HEADING_NEW_MANUFACTURER', 'Nouveau fabricant');
define('TEXT_HEADING_EDIT_MANUFACTURER', 'Editer le fabricant');
define('TEXT_HEADING_DELETE_MANUFACTURER', 'Supprimer le facricant');

define('TEXT_MANUFACTURERS', 'Fabricant:');
define('TEXT_DATE_ADDED', 'ajouté le:');
define('TEXT_LAST_MODIFIED', 'Dernière modification le:');
define('TEXT_PRODUCTS', 'Articles:');
define('TEXT_IMAGE_NONEXISTENT', 'IMAGE N\'EXISTE PAS');

define('TEXT_NEW_INTRO', 'Veuillez entrer un fabricant avec toutes les données pertinentes.');
define('TEXT_EDIT_INTRO', 'Veuillez faire tous les changements nécessaires');

define('TEXT_MANUFACTURERS_NAME', 'Nom du fabricant:');
define('TEXT_MANUFACTURERS_IMAGE', 'Image du fabricant:');
define('TEXT_MANUFACTURERS_URL', 'URL du fabricant:');

define('TEXT_DELETE_INTRO', 'Êtes-vous sûr de vouloir supprimer le fabricant?');
define('TEXT_DELETE_IMAGE', 'Supprimer l\'image du fabricant?');
define('TEXT_DELETE_PRODUCTS', 'Supprimer tous les articles de ce fabricant? (Critiques, offres, nouveautés incluses)');
define('TEXT_DELETE_WARNING_PRODUCTS', '<b>AVERTISSEMENT:</b> Il y a encore %s articles qui sont en relation avec ce fabricant!');

define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le répértoire est en lecture seule. Veuillez corriger les privilèges d\'accès de ce répértoire!');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le répértoire %s n\'existe pas!');

define('TEXT_CHARACTERS', 'Signes');
define('TEXT_META_TITLE', 'Titre Meta:');
define('TEXT_META_DESCRIPTION', 'Déscription Meta:');
define('TEXT_META_KEYWORDS', 'Mots-clefs Meta:');
define('TEXT_MANUFACTURERS_DESCRIPTION', 'Déscription du fabricant:');
define('TEXT_DELETE', 'Supprimer');
?>

<?php
/************************************************************
* file: nr_order_in_other_shop.php
* use: language file for system file
* (c) noRiddle 04-2018
************************************************************/

define('MODULE_NR_ORDER_IN_OTHER_SHOP_TITLE', 'Pedido en otra tienda');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_DESCRIPTION', 'El módulo sirve para pedir mercancías en la cesta de la compra en otra tienda (por ejemplo, en caso de un cuello de botella en la logística).');

define('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS_TITLE', 'Estado');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS_DESC', '¿Activar módulo?');

define('MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP_TITLE', '¿En qué tienda debe ejecutarse el pedido?');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP_DESC', 'Por favor, introduzca aquí en minúsculas sólo el nombre de la tienda sin "-ersatzteile" o "-spare-parts" .<br />Ejemplos: skoda, bmw, bmw-motorrad, chrysler-dodge-jeep-ram');

$nr_oios_lngs = xtc_get_languages();
for($oios = 0, $coios = count($nr_oios_lngs); $oios < $coios; $oios++) {
define('MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_'.strtoupper($nr_oios_lngs[$oios]['code']).'_TITLE', xtc_image(DIR_WS_LANGUAGES . $nr_oios_lngs[$oios]['directory'] .'/admin/images/'. $nr_oios_lngs[$oios]['image'], $nr_oios_lngs[$oios]['name']).' Nota de texto sobre el botón "A la caja" '.strtoupper($nr_oios_lngs[$oios]['code']));
define('MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_'.strtoupper($nr_oios_lngs[$oios]['code']).'_DESC', 'Introduzca aquí el texto (justificación) por la que el pedido debe completarse en otra tienda.<br />Por favor, utilice  <i>%s</i> como marcador de posición para la tienda introducida anteriormente.<br /><span style="display:inline-block; font-size:11px; line-height:12px;">(Con la función de backup se puede hacer un respaldo de cualquier texto.<br />Al desinstalar, luego instalar y no restaurar el respaldo, el texto por defecto aparecerá aquí de nuevo.)</span>');
}
?>

<?php
/* --------------------------------------------------------------
   $Id: customers.php 10228 2016-08-10 16:37:45Z GTB $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(customers.php,v 1.13 2002/06/15); www.oscommerce.com 
   (c) 2003 nextcommerce (customers.php,v 1.8 2003/08/15); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Clients');
define('HEADING_TITLE_SEARCH', 'Recherche:');

define('TABLE_HEADING_CUSTOMERSCID', 'Numéro client');
define('TABLE_HEADING_FIRSTNAME', 'Prénom');
define('TABLE_HEADING_LASTNAME', 'Nom');
define('TABLE_HEADING_ACCOUNT_CREATED', 'crée le');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_DATE_ACCOUNT_CREATED', 'Accès crée le:');
define('TEXT_DATE_ACCOUNT_LAST_MODIFIED', 'Dernière modification:');
define('TEXT_INFO_DATE_LAST_LOGON', 'dernière connexion:');
define('TEXT_INFO_NUMBER_OF_LOGONS', 'Nombre des connexions:');
define('TEXT_INFO_COUNTRY', 'Pays:');
define('TEXT_INFO_NUMBER_OF_REVIEWS', 'Nombre des évaluations du produit:');
define('TEXT_DELETE_INTRO', 'Voulez-vous vraiment supprimer ce client?');
define('TEXT_DELETE_REVIEWS', 'Supprimer les évaluations de %s');
define('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'Supprimer le client');
define('TYPE_BELOW', 'Veuillez saisir en bas');
define('PLEASE_SELECT', 'Sélectionner');
define('HEADING_TITLE_STATUS', 'Groupe de client');
define('TEXT_ALL_CUSTOMERS', 'Tous les groupes');
define('TEXT_INFO_HEADING_STATUS_CUSTOMER', 'Groupe de client');
define('TABLE_HEADING_NEW_VALUE', 'Nouveau statut');
define('TABLE_HEADING_DATE_ADDED', 'Date');
define('TEXT_NO_CUSTOMER_HISTORY', '--Pas de modifications jusqu\'ici--');
define('TABLE_HEADING_GROUPIMAGE', 'Icon');
define('ENTRY_MEMO', 'Memo');
define('TEXT_DATE', 'Date');
define('TEXT_TITLE', 'Titre');
define('TEXT_POSTER', 'Auteur');
define('ENTRY_PASSWORD_CUSTOMER', 'Mot de passe:');
define('TABLE_HEADING_ACCOUNT_TYPE', 'Compte');
define('TEXT_ACCOUNT', 'Oui');
define('TEXT_GUEST', 'Non');
define('NEW_ORDER', 'Nouvelle commande?');
define('ENTRY_PAYMENT_UNALLOWED', 'Modules de paiement pas autorisés');
define('ENTRY_SHIPPING_UNALLOWED', 'Modules d\'expéditions pas autorisés:');
define('ENTRY_NEW_PASSWORD', 'Nouveau mot de passe:');

// NEU HINZUGEFUEGT 04.12.2008 - UMSATZANZEIGE BEI KUNDEN 03.12.2008
define('TABLE_HEADING_UMSATZ', 'Chiffre d\'affaires');

// BOF - web28 - 2010-05-28 - added  customers_email_address
define('TABLE_HEADING_EMAIL', 'Email');
// EOF - web28 - 2010-05-28 - added  customers_email_address

define('TEXT_INFO_HEADING_ADRESS_BOOK', 'Carnet d\'adresses');
define('TEXT_INFO_DELETE', '<b>Supprimer cette entrée du carnet d\'adresses?</b>');
define('TEXT_INFO_DELETE_DEFAULT', '<b>Cette entrée du carnet d\'adresses ne peut pas être supprimée!</b>');

define('TABLE_HEADING_AMOUNT', 'Crédit');
define('WARNING_CUSTOMER_ALREADY_EXISTS', 'Groupe de clients ne peut pas être modifié. Cette adresse mail est déjà utilisée pour un compte client.');

define('TEXT_SORT_ASC', 'croissant');
define('TEXT_SORT_DESC', 'décroissant');

define('TEXT_INFO_HEADING_STATUS_NEW_ORDER', 'Nouvelle commande');
define('TEXT_INFO_PAYMENT', 'Mode de paiement:');
define('TEXT_INFO_SHIPPING', 'Mode d\'expédition:');
?>

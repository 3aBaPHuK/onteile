<?php
/**************************************************************
* file: nr_get_activated_countries.php
* use: get simple array with ids of all activated countries
* path: /inc/
*
* (c) 06-2019 noRiddle
**************************************************************/

function nr_get_activated_countries($shop) {
    global $data20;
    $countries_array = array();

    return [195];

    $shp_dblink = new mysqli($data20[$shop]['sql_server'], $data20[$shop]['sql_user'], $data20[$shop]['sql_pass'], $data20[$shop]['sql_db']);
    if ($shp_dblink->connect_errno) {
        die("Connection failed: " . $shp_dblink->connect_error);
    }
    //set charset for connection
    $shp_dblink->set_charset('utf8');

    $countries_qu_str = "SELECT countries_id 
                           FROM " . TABLE_COUNTRIES . " 
                          WHERE status = '1'
                       ORDER BY countries_id";

    $countries_qu = $shp_dblink->query($countries_qu_str);
                             
    while($countries_arr = xtc_db_fetch_array($countries_qu)) {
        $countries_array[] = $countries_arr['countries_id'];
    }

    $shp_dblink->close();

    return $countries_array;
}
?>
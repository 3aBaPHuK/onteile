<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_summary.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_summary.php, v 1.4 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'aperçu du programme');

define('TEXT_SUMMARY_TITLE', 'Résumé et aperçu du programme d\'affilié');
define('TEXT_IMPRESSIONS', 'Impressions de la bannière:');
define('TEXT_VISITS', 'Visites par cette page affilié:');
define('TEXT_TRANSACTIONS', 'Ventes du partenaire:');
define('TEXT_AFFILIATES', 'Total des partenaires');
define('TEXT_CONVERSION', 'Ventes/ clics');
define('TEXT_AMOUNT', 'Total valeur de commande:');
define('TEXT_AVERAGE', 'Moyenne:');
define('TEXT_COMMISSION_RATE', 'Commission:');
define('TEXT_PAYPERSALE_RATE', 'Commission par vente:');
define('TEXT_CLICKTHROUGH_RATE', 'Pay par clic:');
define('TEXT_COMMISSION', 'Montant de commission:');
define('TEXT_SUMMARY_HELP', '[?]');
define('TEXT_SUMMARY', 'Cliquez sur [?] afin de pouvoir lire une description de tous les catégories.');
define('HEADING_SUMMARY_HELP', 'Aide');
define('TEXT_IMPRESSIONS_HELP', '<b>Impressions de bannière</b> indique combien de fois une bannière a été affiché pendant une période déterminée.');
define('TEXT_VISITS_HELP', '<b>Visiteurs</b> indique, combien de  clics ont été fait par des visiteurs qui ont visité votre site web. ' );
define('TEXT_TRANSACTIONS_HELP', '<b>Ventes du partenaire</b> indique, combien de ventes ont été réalisé à partir des sites affiliés.');
define('TEXT_CONVERSION_HELP', '<b>Ventes/Clics</b> indique en pourcentage combien de visiteurs de votre site web ont fait un achat chez nous.');
define('TEXT_AMOUNT_HELP', '<b>Total valeur de commande</b> montre le montant de la commande de tous les ventes expédiés.');
define('TEXT_AVERAGE_HELP', '<b>Moyenne</b> représente la valeur de vente moyenne qui a été réalisé à partir de votre compte.');
define('TEXT_COMMISSION_RATE_HELP', '<b>Commission</b> désigne la commission en pourcentage avec laquelle nous facturons les ventes réalisé chez vous.');
define('TEXT_CLICKTHROUGH_RATE_HELP', '<b>Pay par clic</b> désigne la commission que vous recevrez pour une base de commission "Pay par clic".');
define('TEXT_PAY_PER_SALE_RATE_HELP', '<b>Pay per Sale</b> désigne la commission que vous recevrez pour une base de commission "Pay par sale".');
define('TEXT_COMMISSION_HELP', '<b>Montant de commission</b> indique les recettes de commission pour tous vos ventes expédiées.');
define('TEXT_CLOSE_WINDOW', 'Fermer la session [x]');

define('IMAGE_BANNERS', 'Bannière du partenaire');
define('IMAGE_CLICKTHROUGHS', 'Rapport sur des clics');
define('IMAGE_SALES', 'Rapport sur des ventes');
?>

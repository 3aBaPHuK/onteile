<?php
/* --------------------------------------------------------------
   $Id: geo_TAGs.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(geo_TAGs.php,v 1.11 2003/05/07); www.oscommerce.com 
   (c) 2003	 nextcommerce ( geo_TAGs.php,v 1.4 2003/08/1); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Características del artículo');
define('HEADING_TITLE_DETAIL', 'Características del artículo - valores');

define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_STATUS', 'Detalles');
define('TABLE_HEADING_SORT', 'Clasificación');
define('TABLE_HEADING_FILTER', 'Filtro');

define('TABLE_HEADING_VALUES_IMAGE', 'Imagen');
define('TABLE_HEADING_VALUES_CONTENT', 'Contenido');
define('TABLE_HEADING_VALUES_NAME', 'Valor');
define('TABLE_HEADING_VALUES_DESCRIPTION', 'Descripción');

define('TABLE_HEADING_OPTIONS_NAME', 'Nombre');
define('TABLE_HEADING_OPTIONS_DESCRIPTION', 'Descripción');
define('TABLE_HEADING_OPTIONS_CONTENT', 'Contenido');

define('TEXT_INFO_DATE_ADDED', 'agregado el:');
define('TEXT_INFO_LAST_MODIFIED', 'última modificación');

define('TEXT_INFO_VALUE_NAME', 'Valor:');
define('TEXT_INFO_VALUE_DESCRIPTION', 'Descripción:');
define('TEXT_INFO_VALUE_IMAGE', 'Imagen:');
define('TEXT_INFO_VALUE_CONTENT', 'Contenido:');
define('TEXT_INFO_VALUE_SORT', 'Clasificación:');
define('TEXT_INFO_DELETE_VALUE_IMAGE', '¿Borrar imagen?');

define('TEXT_INFO_HEADING_NEW_VALUE', 'Nuevo valor');
define('TEXT_INFO_NEW_VALUE_INTRO', 'Por favor, introduzca el nuevo valor con todos los datos relevantes.');
define('TEXT_INFO_HEADING_EDIT_VALUE', 'Editar valor');
define('TEXT_INFO_EDIT_VALUE_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_INFO_HEADING_DELETE_VALUE', 'Borrar valor');
define('TEXT_INFO_DELETE_VALUE_INTRO', '¿Está seguro de que quiere borrar este valor?');

define('TEXT_DISPLAY_NUMBER_OF_VALUES', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> valores)');


define('TEXT_INFO_OPTION_NAME', 'Nombre:');
define('TEXT_INFO_OPTION_DESCRIPTION', 'Descripción:');
define('TEXT_INFO_OPTION_CONTENT', 'Contenido:');
define('TEXT_INFO_OPTION_SORT', 'Clasificación:');

define('TEXT_INFO_NUMBER_OPTION', 'Número de valores:');
define('TEXT_INFO_HEADING_NEW_OPTION', 'Propiedad nueva');
define('TEXT_INFO_NEW_OPTION_INTRO', 'Por favor, introduzca la nueva propiedad con todos los datos relevantes.');
define('TEXT_INFO_HEADING_EDIT_OPTION', 'Editar propiedad');
define('TEXT_INFO_EDIT_OPTION_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_INFO_HEADING_DELETE_OPTION', 'Borrar propiedad');
define('TEXT_INFO_DELETE_OPTION_INTRO', '¿Está seguro de que quiere borrar esta propiedad?');

define('TEXT_DISPLAY_NUMBER_OF_OPTIONS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de unt total de  <b>%d</b> propiedades)');

define('TEXT_INFO_HEADING_IMPORT', 'Importar');
define('TEXT_INFO_ATTRIBUTES_OPTION', '¿Importar las características del artículo como propiedades?');
define('TEXT_INFO_ATTRIBUTES_VALUE', '¿Importar valores de opción como valores de propiedad?');
define('TEXT_INFO_ATTRIBUTES', 'Atributos - ¿Adoptar asignaciones como propiedades para artículos?');
define('TEXT_INFO_ATTRIBUTES_DELETE', 'Atributos - ¿Eliminar asignaciones de atributos borrados?');
define('TEXT_INFO_IMPORT_WAIT', 'Por favor, espera...');
define('TEXT_INFO_IMPORT_FINISHED', 'Importación completada.');

?>

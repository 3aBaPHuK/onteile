<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_payment.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_payment.php, v 1.5 2003/02/17);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Pagos de comisión del Programa de Afiliados');
define('HEADING_TITLE_SEARCH', 'Buscar:');
define('HEADING_TITLE_STATUS', 'Estado:');

define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_STATUS', 'Estado');
define('TABLE_HEADING_AFILIATE_NAME', 'Afiliado');
define('TABLE_HEADING_PAYMENT', 'Comisión (incl.)');
define('TABLE_HEADING_NET_PAYMENT', 'Comisión (excl.)');
define('TABLE_HEADING_DATE_BILLED', 'Fecha facturado');
define('TABLE_HEADING_NEW_VALUE', 'estado nuevo');
define('TABLE_HEADING_OLD_VALUE', 'estado antiguo');
define('TABLE_HEADING_AFFILIATE_NOTIFIED', 'Notificar afiliado');
define('TABLE_HEADING_DATE_ADDED', 'agregado el:');

define('TEXT_DATE_PAYMENT_BILLED', 'Facturado:');
define('TEXT_DATE_ORDER_LAST_MODIFIED', 'Última modificación:');
define('TEXT_AFFILIATE_PAYMENT', 'Comisión');
define('TEXT_AFFILIATE_BILLED', 'Fecha de facturación');
define('TEXT_AFFILIATE', 'Afiliado');

define('TEXT_INFO_HEADING_DELETE_PAYMENT', 'Borrar facturación');
define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar el pago de comisión?');

define('TEXT_DISPLAY_NUMBER_OF_PAYMENTS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de  <b>%d</b> pagos de comisión)');

define('TEXT_AFFILIATE_PAYING_POSSIBILITIES', 'Opciones de desembolso:');
define('TEXT_AFFILIATE_PAYMENT_CHECK', 'por cheque:');
define('TEXT_AFFILIATE_PAYMENT_CHECK_PAYEE', 'Destinatario del cheque:');
define('TEXT_AFFILIATE_PAYMENT_PAYPAL', 'por PayPal:');
define('TEXT_AFFILIATE_PAYMENT_PAYPAL_EMAIL', 'Correo electrónico de la cuenta PayPal:');
define('TEXT_AFFILIATE_PAYMENT_BANK_TRANSFER', 'por transfernecia bancaria:');
define('TEXT_AFFILIATE_PAYMENT_BANK_NAME', 'Instituto de crédito:');
define('TEXT_AFFILIATE_PAYMENT_BANK_ACCOUNT_NAME', 'Titular de la cuenta:');
define('TEXT_AFFILIATE_PAYMENT_BANK_ACCOUNT_NUMBER', 'Número de cuenta:');
define('TEXT_AFFILIATE_PAYMENT_BANK_BRANCH_NUMBER', 'Código bancario:');
define('TEXT_AFFILIATE_PAYMENT_BANK_SWIFT_CODE', 'Código SWIFT:');

define('IMAGE_AFFILIATE_BILLING', 'Iniciar el motor de facturación');

define('PAYMENT_STATUS', 'Estado de facturación');
define('PAYMENT_NOTIFY_AFFILIATE', 'Notificar afiliado');

define('TEXT_ALL_PAYMENTS', 'Todos los pagos');
define('TEXT_NO_PAYMENT_HISTORY', '¡No hay historial de pagos disponible!');

define('EMAIL_TEXT_SUBJECT', 'Cambio de estado de su facturación de comisión');

define('SUCCESS_BILLING', 'Aviso: ¡Sus comisiones han sido facturados con éxito!');
define('SUCCESS_PAYMENT_UPDATED', 'Aviso: El estado de esta facturación de comisión ha sido actualizado con éxito.');
define('ERROR_PAYMENT_DOES_NOT_EXIST', 'Error: ¡La facturación de comisión no existe!');
?>

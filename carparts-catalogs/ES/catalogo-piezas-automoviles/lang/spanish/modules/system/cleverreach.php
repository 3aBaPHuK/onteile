<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_CLEVERREACH_TEXT_TITLE', 'CleverReach - marketing por correo electrónico profesional');
define('MODULE_CLEVERREACH_TEXT_DESCRIPTION', 'La interfaz permite una conexión directa a su tienda online. Los datos del destinatario se importan automáticamente a su cuenta sin esfuerzo manual.');
define('MODULE_CLEVERREACH_STATUS_TITLE', '¿Activar módulo?');
define('MODULE_CLEVERREACH_STATUS_DESC', 'Activar el sistema de newsletters CleverReach<br/><b>Importante:</b> Para aquello se requiere un <a href="http://www.cleverreach.de/frontend/" target="_blank"><strong><u>Registro con CleverReach</u></strong></a> .');
define('MODULE_CLEVERREACH_APIKEY_TITLE', 'CleverReach API');
define('MODULE_CLEVERREACH_APIKEY_DESC', 'Introduzca aquí la clave de la API.');
define('MODULE_CLEVERREACH_NAME_TITLE', 'Proyecto CleverReach');
define('MODULE_CLEVERREACH_NAME_DESC', 'Introduzca aquí el nombre del proyecto.');
define('MODULE_CLEVERREACH_GROUP_TITLE', 'ID de grupo de CleverReach');
define('MODULE_CLEVERREACH_GROUP_DESC', 'Introduzca aquí el ID del grupo para el newsletter.');

?>

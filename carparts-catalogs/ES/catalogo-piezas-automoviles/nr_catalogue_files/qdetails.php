<?php
if(!isset($_SESSION['customer_id'])) xtc_redirect(xtc_href_link(FILENAME_DEFAULT)); //redirect to start page if not logged in, noRiddle

// Include soap request class
include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'requestOem.php');
// Include view class
include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'render'.DIRECTORY_SEPARATOR.'qdetails'.DIRECTORY_SEPARATOR.'default.php');
// Include view class
include('nr_catalogue_files/extender.php');

class QuickDetailsExtender extends CommonExtender
{
    function FormatLink($type, $dataItem, $catalog, $renderer)
    {
        if ($type == 'vehicle')
            //BOC set link to content 1004 which includes vehicle.php, noRiddle
            //$link = 'vehicle.php?c='.$catalog.'&vid='.$renderer->vehicleid. '&ssd=' . $renderer->ssd;
            $link = xtc_href_link(FILENAME_CONTENT, 'coID=1004').'?c='.$catalog.'&vid='.$renderer->vehicleid. '&ssd=' . $renderer->ssd;
            //EOC set link to content 1004 which includes vehicle.php, noRiddle
        //BOC add link back to groups (qgroups), noRiddle
        elseif ($type == 'groups')
            $link = xtc_href_link(FILENAME_CONTENT, 'coID=1003').'?c='.$catalog.'&vid='.$renderer->vehicleid. '&ssd=' . $renderer->ssd;
        //EOC add link back to groups (qgroups), noRiddle
        elseif ($type == 'category')
            //BOC set link to content 1004 which includes vehicle.php, noRiddle
            //$link = 'vehicle.php?c=' . $catalog . '&vid=' . $renderer->vehicleid . '&cid=' . $dataItem['categoryid'] . '&ssd=' . $dataItem['ssd'];
            $link = xtc_href_link(FILENAME_CONTENT, 'coID=1004').'?c=' . $catalog . '&vid=' . $renderer->vehicleid . '&cid=' . $dataItem['categoryid'] . '&ssd=' . $dataItem['ssd'];
            //EOC set link to content 1004 which includes vehicle.php, noRiddle
        elseif ($type == 'unit')
        {
            $coi = array();
            foreach ($dataItem->Detail as $detail)
            {
                if ((string)$detail['match']) {
                    $i = (string)$detail['codeonimage'];
                    $coi[$i] = $i;
                }
            }
            //BOC set link to content 1006 which includes unit.php, noRiddle
            //$link = 'unit.php?c=' . $catalog . '&vid=' . $renderer->vehicleid . '&uid=' . $dataItem['unitid'] .  '&cid=' . $renderer->currentunit['categoryid'] . '&ssd=' . $dataItem['ssd'] . '&coi=' . implode(',', $coi);
            $link = xtc_href_link(FILENAME_CONTENT, 'coID=1006').'?c=' . $catalog . '&vid=' . $renderer->vehicleid . '&uid=' . $dataItem['unitid'] .  '&cid=' . $renderer->currentunit['categoryid'] . '&ssd=' . $dataItem['ssd'] . '&coi=' . implode(',', $coi);
            //EOC set link to content 1006 which includes unit.php, noRiddle
        }
        elseif ($type == 'detail') {
            $link = Config::$redirectUrl;
            $link = str_replace('$oem$', urlencode($dataItem['oem']), $link);
        }

        return $link;
    }
}

// Create request object
$request = new GuayaquilRequestOEM($_GET['c'], $_GET['ssd'], Config::$catalog_data);
if (Config::$useLoginAuthorizationMethod) {
    $request->setUserAuthorizationMethod(Config::$userLogin, Config::$userKey);
}

// Append commands to request
$request->appendGetVehicleInfo($_GET['vid']);
$request->appendListCategories($_GET['vid'], isset($_GET['cid']) ? $_GET['cid'] : -1);
$request->appendListQuickDetail($_GET['vid'], $_GET['gid'], 1);

// Execute request
$data = $request->query();

// Check errors
if ($request->error != '')
{
    echo $request->error;
}
else
{
    $vehicle = $data[0]->row;
    $categories = $data[1];
    $details= $data[2];
    
    //BOC display also car, noRiddle
    //echo '<h1>'.CommonExtender::FormatLocalizedString('CarName', $vehicle['name']).'</h1>';
    echo '<h1>'
         //.'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.Config::$nr_icon_arr[$_GET['c']].'" alt ="" />&nbsp;'
         .'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['brand_icon'].'" alt ="'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].'" />&nbsp;'
         .CommonExtender::FormatLocalizedString('CarName', $vehicle['name'])
         .'</h1>';
    
    echo '<h2>'.CommonExtender::FormatLocalizedString('GroupDetails', $vehicle['name']).'</h2>';
    //echo '<pre>'.print_r($data, true).'</pre>';
    //EOC display also car, noRiddle
    
    echo '<div id="pagecontent">';

    $renderer = new GuayaquilQuickDetailsList(new QuickDetailsExtender());
    $renderer->detaillistrenderer = new GuayaquilDetailsList($renderer->extender);
    $renderer->detaillistrenderer->group_by_filter = 1;
    
    echo $renderer->Draw($details, $_GET['c'], $_GET['vid'], $_GET['ssd']);

    echo '</div>';
}
?>
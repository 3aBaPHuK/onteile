<?php
 //additional fields
define('TEXT_COLORBOX_CURRENT', 'Image {current} de {total}');
define('TEXT_COLORBOX_PREVIOUS', 'En arrière');
define('TEXT_COLORBOX_NEXT', 'Suivant');
define('TEXT_COLORBOX_CLOSE', 'Fermer');
define('TEXT_COLORBOX_XHRERROR', 'Le chargement du contenu a échoué.');
define('TEXT_COLORBOX_IMGERROR', 'Le chargement de l\'image a échoué.');
define('TEXT_COLORBOX_SLIDESHOWSTART', 'Démarrer le diaporama');
define('TEXT_COLORBOX_SLIDESHOWSTOP', 'Arrêter le diaporama');
?>

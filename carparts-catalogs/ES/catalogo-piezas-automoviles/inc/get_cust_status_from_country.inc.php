<?php
/*******************************************************************************************************************************************
* get_cust_status_from_country.inc.php
* functions to read out prefered browser language and set corresponding customers_status | no, read just entries in countries table, 07-2017
* e.g. for HTTP_ACCEPT_LANGUAGE: de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4,fr-FR;q=0.2
* (c) noRiddle kontakt@revilonetz.de | 05-2015
*******************************************************************************************************************************************/

function detect_browser_languages() {
    if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && $_SERVER['HTTP_ACCEPT_LANGUAGE'] != '') {
        $eff_country = '';
        $acc_langs_array = array();

        $acc_langs_str = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        $acc_langs_arr = preg_split('#,\s*#', $acc_langs_str);

        foreach($acc_langs_arr as $det_lang) {
            $sing_lang = explode(';', $det_lang);
            
            $q = isset($sing_lang[1]) ? (float)$sing_lang[1] : 1.0;
            $split_lang = explode('-', $sing_lang[0]);
            $eff_lang = $split_lang[0]; //isset($split_lang[1]) ? $split_lang[1] : $split_lang[0]; //we need only language not probable country
            $eff_lang = preg_replace('#[^A-Z]#i', '', $eff_lang);

            $acc_langs_array['composed_lng'][] = strtolower($sing_lang[0]);
            $acc_langs_array['single_lng'][] = strtolower($eff_lang);
        }

        return $acc_langs_array;
    } else {
        return false;
    }    
}

function detect_browser_lang() {
    if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && $_SERVER['HTTP_ACCEPT_LANGUAGE'] != '') {
        $eff_country = '';
        $acc_langs_array = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        $acc_langs_arr = preg_split('#,\s*#', $acc_langs_array);
        $first_lang = preg_split('#;\s*#', $acc_langs_arr[0]);
        $split_it = explode('-', $first_lang[0]);
        if(isset($split_it[1])) {
            $eff_country = strtoupper($split_it[1]);
        } else {
            $eff_country = strtoupper($split_it[0]);
        }
        //BOC secure data for DB query
        if($eff_country == 'EN') $eff_country = 'GB'; //HTTP_ACCEPT_LANGUAGE doesn't exactly use countries_iso_code_2 (of which the latter is not ISO 639-1 ?)
        $eff_country = preg_replace('#[^A-Z]#i', '', $eff_country);
        //EOC secure data for DB query
        return $eff_country;
    } else {
        return false;
    }    
}

//following function used in creat_*_account.php and in /includes/write_customers_status.php, noRiddle
function get_cust_status_from_country($country, $status_id = '') {
    if($country == '') $browser_country = detect_browser_lang(); //$country == '' used in /includes/write_customers_status.php, 07-2017 noRiddle
    //if($country !== false) {  //wrong, not needed, 07-2017 noRiddle
    if($country != '') {
        //$cust_stat_query = xtc_db_query("SELECT ".$status_id." FROM ".TABLE_COUNTRIES." WHERE countries_iso_code_2 = '".(int)$country."'");
        $cust_stat_query = xtc_db_query("SELECT ".$status_id." FROM ".TABLE_COUNTRIES." WHERE countries_id = '".(int)$country."'");
        $cust_stat = xtc_db_fetch_array($cust_stat_query);
        if($cust_stat[$status_id] > 0) {
            return $cust_stat[$status_id];
        } else {
            return false;
        }
    } else if((isset($browser_country) && $browser_country !== false)) {
        $cust_stat_query = xtc_db_query("SELECT ".$status_id." FROM ".TABLE_COUNTRIES." WHERE countries_iso_code_2 = '".$browser_country."'"); //$browser_country is secured in detect_browser_lang() above
        $cust_stat = xtc_db_fetch_array($cust_stat_query);
        if($cust_stat[$status_id] > 0) {
            return $cust_stat[$status_id];
        } else {
            return false;
        }
    } else {
        return false;
    }
}
?>
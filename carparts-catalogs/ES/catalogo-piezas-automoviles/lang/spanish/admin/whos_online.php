<?php
/* --------------------------------------------------------------
   $Id: whos_online.php 10502 2016-12-14 16:25:46Z GTB $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(whos_online.php,v 1.7 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (whos_online.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', '¿Quién está en línea?');

define('TABLE_HEADING_ONLINE', 'en línea');
define('TABLE_HEADING_CUSTOMER_ID', 'ID');
define('TABLE_HEADING_FULL_NAME', 'Nombre');
define('TABLE_HEADING_IP_ADDRESS', 'Dirección IP');
define('TABLE_HEADING_COUNTRY', 'País');
define('TABLE_HEADING_ENTRY_TIME', 'Hora de inicio');
define('TABLE_HEADING_LAST_CLICK', 'Último clic');
define('TABLE_HEADING_LAST_PAGE_URL', 'Última URL');
define('TABLE_HEADING_HTTP_REFERER', 'HTTP Referer');
define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_SHOPPING_CART', 'Cesta de compra');
define('TEXT_SHOPPING_CART_SUBTOTAL', 'Total');
define('TEXT_NUMBER_OF_CUSTOMERS', 'Actualmente hay %s clientes en línea.');
define('TEXT_EMPTY_CART', 'La cesta de compra del cliente está vacía.');
define('TEXT_SESSION_IS_ENCRYPTED', '<hr><b>AVISO</b>:<br />El contenido de la cesta de compra no se puede mostrar.<br />La sesión está cifrada con Suhosin<br />(suhosin.session.encrypt = On)<br />Para desactivar el cifrado, póngase en contacto con su proveedor.');
define('TEXT_ACTIVATE_WHOS_ONLINE', 'Activar ¿Quién está en línea?:');
?>

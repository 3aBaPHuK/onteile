<?php
/* -----------------------------------------------------------------------------------------
   $Id: dp.php 5122 2013-07-18 11:47:17Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(dp.php,v 1.4 2003/02/18 04:28:00); www.oscommerce.com
   (c) 2003 nextcommerce (dp.php,v 1.5 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   German Post (Deutsche Post WorldNet)         	Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   enhanced on 2010-12-08 18:17:30Z franky_n
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_DP_TEXT_TITLE', 'Correos alemán');
define('MODULE_SHIPPING_DP_TEXT_DESCRIPTION', 'Correos alemán - Módulo de envío mundial');
define('MODULE_SHIPPING_DP_TEXT_WAY', ' Envíos a ');
define('MODULE_SHIPPING_DP_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_DP_INVALID_ZONE', 'No es posible realizar un envío a este país.');
define('MODULE_SHIPPING_DP_UNDEFINED_RATE', 'Los costos de envío no se pueden calcular por el momento.');

define('MODULE_SHIPPING_DP_STATUS_TITLE' , 'Correos alemán WorldNet');
define('MODULE_SHIPPING_DP_STATUS_DESC' , '¿Desea ofrecer envíos a través de Correos alemán?');
define('MODULE_SHIPPING_DP_HANDLING_TITLE' , 'Tarifa de manejo');
define('MODULE_SHIPPING_DP_HANDLING_DESC' , 'Costo de procesamiento para este modo de envío en Euro');
define('MODULE_SHIPPING_DP_TAX_CLASS_TITLE' , 'Clase de impuestos');
define('MODULE_SHIPPING_DP_TAX_CLASS_DESC' , 'Aplicar la siguiente clase de impuestos a los costos de envío.');
define('MODULE_SHIPPING_DP_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_DP_ZONE_DESC' , 'Si selecciona una zona, este modo de envío sólo se ofrecerá en esa zona.');
define('MODULE_SHIPPING_DP_SORT_ORDER_TITLE' , 'Orden de la visualización');
define('MODULE_SHIPPING_DP_SORT_ORDER_DESC' , 'Menor se muestra en primer lugar.');
define('MODULE_SHIPPING_DP_ALLOWED_TITLE' , 'Zonas de envío individuales');
define('MODULE_SHIPPING_DP_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE');
define('MODULE_SHIPPING_DP_NUMBER_ZONES_TITLE' , 'Número de zonas');
define('MODULE_SHIPPING_DP_NUMBER_ZONES_DESC' , 'Número de zonas proporcionadas');
define('MODULE_SHIPPING_DP_DISPLAY_TITLE' , 'Activar la visualización');
define('MODULE_SHIPPING_DP_DISPLAY_DESC' , '¿Desea mostrar si no es posible realizar envíos a ese país o si no se pueden calcular los costos de envío?');

for ($module_shipping_dp_i = 1; $module_shipping_dp_i <= MODULE_SHIPPING_DP_NUMBER_ZONES; $module_shipping_dp_i ++) {
define('MODULE_SHIPPING_DP_COUNTRIES_'.$module_shipping_dp_i.'_TITLE' , '<hr/>DP Zone '.$module_shipping_dp_i.' países');
define('MODULE_SHIPPING_DP_COUNTRIES_'.$module_shipping_dp_i.'_DESC' , 'Lista separada por comas de los códigos de países ISO de 2 caracteres que forman parte de la zona '.$module_shipping_dp_i.' (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_DP_COST_'.$module_shipping_dp_i.'_TITLE' , 'DP Zone '.$module_shipping_dp_i.' Tabla de envíos');
define('MODULE_SHIPPING_DP_COST_'.$module_shipping_dp_i.'_DESC' , 'Costos de envío de la zona '.$module_shipping_dp_i.' basado en el peso del pedido. Ejemplo: 3:8.50,7:10.50,99999:12.00... Pesos mayores de 0 y menores de 3 cuestan 8,50, menos de 7 cuestan 10,50 para la zona '.$module_shipping_dp_i.'.');
}
?>

<?php
/* --------------------------------------------------------------
   $Id: blz_update.php 10838 2017-07-10 14:02:46Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Actualizar los códigos bancarios del Bundesbank alemán.');
define('BLZ_INFO_TEXT', '<p>Este formulario actualiza la tabla de códigos bancarios del modified eCommerce Shopsoftware.  La tabla de códigos bancarios se utiliza para comprobar los datos bancarios al realizar el pedido.<br/>El Bundesbank proporciona archivos actualizados cada 3 meses.</p><p><strong>Aviso de actualización:</strong></p><p>Para actualizar por favor abra la página de descarga del cóidgo bancario <a href="https://www.bundesbank.de/Redaktion/DE/Standardartikel/Aufgaben/Unbarer_Zahlungsverkehr/bankleitzahlen_download.html" target="_blank"><strong>del Bundesbank</strong></a>  en otra pestaña del navegador. En el tercio inferior de la página web del Bundesbank, bajo la rúbrica "Bankleitzahlendateien ungepackt" (Archivos de código de clasificación bancaria desembalados), se encuentra también un enlace para descargar el archivo BLZ actual en formato de texto (TXT). Copie este enlace (haga clic con el botón derecho del ratón en el enlace y, a continuación, copie la dirección del enlace) y, a continuación, introdúzcalo aquí en el campo de edición.</p><p>El botón "Actualizar" inicia el proceso de actualización.<br/>La actualización tarda unos segundos.</p><p><i>Enlace de ejemplo para el período del 05.06.2017 al 03.09.2017:</i></p>');
define('BLZ_LINK_NOT_GIVEN_TEXT', '<span class="messageStackError">¡No se especificó ningún enlace web al archivo del código bancario del Bundesbank alemán!</span><br /><br />');
define('BLZ_LINK_INVALID_TEXT', '<span class="messageStackError">Enlace web no válido a archivo BLZ.<br/><br/>¡Sólo se permiten los archivos TXT de la página principal del Bundesbank (www.bundesbank.de)!</span><br /><br />');
define('BLZ_DOWNLOADED_COUNT_TEXT', 'Número <u>unambiguo</u> códigos bancarios reconocidos (¡sin duplicados!)');
define('BLZ_PHP_FILE_ERROR_TEXT', '<p><strong><span class="messageStackError">El parámetro PHP "allow_url_fopen" está desactivado ("off"). Esto es necesario para la función PHP <i>file( )</i> . Para realizar la actualización automáticamente, ajuste el parámetro a "on".</span></strong></p>');
define('BLZ_UPDATE_SUCCESS_TEXT', ' ¡Registros almacenados con éxito en la base de datos!');
define('BLZ_UPDATE_ERROR_TEXT', '¡Ha ocurrido un error!');
define('BLZ_LINK_ERROR_TEXT', '<span class="messageStackError">¡El enlace que ha especificado no existe! Por favor, compruebe la entrada en el campo de edición de la página anterior.</span>');
define('BLZ_LINES_PROCESSED_TEXT', 'Registros de datos de código bancario importados.');
define('BLZ_SOURCE_TEXT', 'Fuente: ');
?>

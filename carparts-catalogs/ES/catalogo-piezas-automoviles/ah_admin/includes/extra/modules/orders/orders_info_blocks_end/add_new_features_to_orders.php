<?php
/*********************************************************************
* file: add_new_features_to_orders.php
* use: create button for intraship label printing
*      have dropdown with predefined texts for order status comments
* (c) noRiddle 12-2016
*********************************************************************/

//BOC get order_status_comments titles
$order_status_comments = array();
$order_status_comments[] = array('id' => 0, 'text' => '-- '.TXT_ORDER_STATUS_COMMENTS_CHOOSE.' --');
$get_comments_title_query = xtc_db_query("SELECT osc.order_status_comments_id, 
                                                 osct.order_status_comments_title
                                            FROM ".TABLE_ORDER_STATUS_COMMENTS." osc
                                       LEFT JOIN ".TABLE_ORDER_STATUS_COMMENTS_TXT." osct
                                              ON osct.order_status_comments_id = osc.order_status_comments_id
                                             AND language_id = '".(int)$lang."'
                                           WHERE osc.order_status_comments_status = '1'
                                        ORDER BY order_status_comments_sort_order"
                                        );

while($get_comments_title = xtc_db_fetch_array($get_comments_title_query)){
    $order_status_comments[] = array('id' => $get_comments_title['order_status_comments_id'], 'text' => $get_comments_title['order_status_comments_title']);
}

$osc_dropdown = '<div id="osc" style="margin:4px 0;">'
                //.xtc_draw_form('osc_txts', FILENAME_ORDERS, xtc_get_all_get_params(array('action')))
                .xtc_draw_form('osc_txts', FILENAME_ORDERS, xtc_get_all_get_params(array('action')) . 'action=custom')
                .'<b>'.TEXT_ORDER_STATUS_COMMENTS_DROPDOWN.'</b> '
                .xtc_draw_pull_down_menu('order_status_comments', $order_status_comments, '', 'id="order_status_comments"')
                //.xtc_draw_pull_down_menu('order_status_comments', $order_status_comments, '', 'id="order_status_comments" onChange="this.form.submit();"')
                .'</form>'
                .'</div>';
echo $osc_dropdown;

$post_url = xtc_href_link(FILENAME_ORDERS, xtc_get_all_get_params(array('action')).'action=custom');
//EOC get order_status_comments titles

//BOC edit gm_price_status
$nr_prod_stat_arr = array();
$nr_prod_stat_arr[] = array('id' => 0, 'text' => NR_EDITPROD_PLEASE_CHOOSE);
$nr_prod_stat_arr[] = array('id' => 1, 'text' => NR_EDITPROD_PROD_NOTAVAIL);
$nr_prod_stat_arr[] = array('id' => 2, 'text' => NR_EDITPROD_PROD_WILL_REPLACED);

for ($op = 0, $n = sizeof($order->products); $op < $n; $op ++) {
    $prod_data_cont = '<h3>'.sprintf(NR_EDITPROD_HEADING, $order->products[$op]['model']).'<span class="nr-head-close" title="close">X</span><br /><span class="sml-red">'.NR_EDITPROD_HEADING_COMMENT.'</span></h3>';
    $prod_data_cont .= xtc_draw_form('nr_upd_prod', FILENAME_ORDERS, xtc_get_all_get_params(array('action')) . 'action=custom', 'post', 'class="nr_upd_prod"');
    $prod_data_cont .= '<div>'.xtc_draw_pull_down_menu('nr_prod_stat', $nr_prod_stat_arr, '', 'class="nr-prod-stat"')
                            .xtc_draw_hidden_field('nr_prod_model', trim($order->products[$op]['model']))
                            .xtc_draw_hidden_field('nr_prod_id', $order->products[$op]['id'])
                            .'</div>';
    $prod_data_cont .= '<p class="nr-inpt-repl">'.NR_EDITPROD_REPL_MODEL_TXT.'<br />'.xtc_draw_input_field('nr_prod_replace').'</p>';
    $prod_data_cont .= '<hr /><h3 class="rrp-price">'.sprintf(NR_EDITPRODPRICE_HEADING, $order->products[$op]['model']).'<br /><span class="sml-red">'.NR_EDITPRODPRICE_HEADING_COMMENT.'</span></h3>';
    $prod_data_cont .= '<p class="nr-inpt-uvp">'.NR_EDITPRODPRICE_TXT.'<br />'.xtc_draw_input_field('nr_prodprice_replace').'</p>';
    //$prod_data_cont .= '<p><button type="submit">'.NR_EDITPROD_BUTTON.'</button></p>';
    $prod_data_cont .= '<p><input class="button nr-gmst-subm" type="submit" value="'.NR_EDITPROD_BUTTON.'" /></p>';
    $prod_data_cont .= '</form>';

    echo '<span class="prod-ed-cont" data-artno="'.trim($order->products[$op]['model']).'">';
    echo ' <span class="nr-prod-edit">'.xtc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO).'</span>';
    echo '<span class="nr-prod-data">'.$prod_data_cont.'</span>';
    echo '</span>';
}
//EOC edit gm_price_status

//BOC get total shipping weight and UstID
//shipping weight
$ah_ord_tot_weight = 0;
$ah_shipping_num_boxes = 1;
for ($i = 0, $n = sizeof($order->products); $i < $n; $i ++) {
    if($order->products[$i]['weight'] > 0) {
        $ah_ord_tot_weight += $order->products[$i]['weight'];
    }
}
if($ah_ord_tot_weight > 0) {
    if (SHIPPING_BOX_WEIGHT >= $ah_ord_tot_weight * SHIPPING_BOX_PADDING/100) {
        $ah_ord_tot_weight = ($ah_ord_tot_weight + SHIPPING_BOX_WEIGHT);
    } else {
        $ah_ord_tot_weight = ($ah_ord_tot_weight + ($ah_ord_tot_weight*SHIPPING_BOX_PADDING/100));
    }

    if (SHIPPING_MAX_WEIGHT != '' && $ah_ord_tot_weight > SHIPPING_MAX_WEIGHT) { // Split into many boxes
        $ah_shipping_num_boxes = ceil($ah_ord_tot_weight/SHIPPING_MAX_WEIGHT);
        $ah_ord_tot_weight = ($ah_ord_tot_weight/$shipping_num_boxes);
    }
    $ah_displ_weight = $ah_shipping_num_boxes . ' x ' . round($ah_ord_tot_weight, 2) . ' kg';
    $ah_displ_weight_str = '<span id="displ-shipp-weight"> | '.AH_ORDER_TOT_WEIGHT.' '.$ah_displ_weight.'</span>';
    echo $ah_displ_weight_str;
}
//UstID

//EOC get total shipping weight and UstID

//BOC PDFBillNext
if (MODULE_PDF_BILL_STATUS == 'True') {
    $order_bill = $order->info['ibn_billnr'];
?>
    <table id="pdf-bill-butt" class="table" style="margin-bottom:10px;border: none !important;"><tr><td><div id="bill-butts" style="float:right;">
<?php
    if($order_bill != '') {
?>
        <span style="display:inline-block; margin:4px 4px 0 0; padding:5px; vertical-align:middle; border:1px solid #aaaaaa; background-color:#ffffff;"><?php echo BUTTON_BILL_NR.' '.$order_bill; ?></span>
        <a class="button" href="Javascript:void()" onClick="window.open('<?php echo xtc_href_link(FILENAME_PRINT_ORDER_PDF,'oID='.$_GET['oID']); ?>', 'popup', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no, width=800, height=750')"><?php echo BUTTON_INVOICE_PDF; ?></a>
<?php
    }
?>
    <a class="button" href="Javascript:void()" onClick="window.open('<?php echo xtc_href_link(FILENAME_PRINT_PACKINGSLIP_PDF,'oID='.$_GET['oID']); ?>', 'popup', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no, width=800, height=750')"><?php echo BUTTON_PACKINGSLIP_PDF; ?></a>
    </div></td></tr></table>
<?php
}
//EOC PDFBillNext
?>


<script>
$(function() {
    $('#osc').insertBefore('form[name="status"]'); //position order_status_comments
    //BOC position edit gm_price_status
    var $pedc = $('.prod-ed-cont'),
        $dtr = $('.dataTableRow'),
        $cdtc = $dtr.eq(-1).children('.dataTableContent').length, //take last row, with first $cdtc returns 0 when we have 7 columns, strange..., noRiddle
        $dtc = $('.dataTableContent');

    //var $cdtc = $dtr.eq(0).children('.dataTableContent').length;
    
//alert('$cdtc: ' + $cdtc);
    var j = 2;
    $pedc.each(function() {
        $(this).appendTo($dtc.eq(j));
        //j += 7;
        j += $cdtc;
    });
    //EOC position edit gm_price_status
    
    //BOC load order_status_comments into text area via ajax
    $('#order_status_comments').change(function() {
        var str = $(this).val(),
            slang = "<?php echo $lang; ?>", //echo $_SESSION['languages_id']
            csfrn = $('form[name="osc_txts"] input[type="hidden"]').attr('name'),
            csfrt = $('form[name="osc_txts"] input[type="hidden"]').val(),
            input_txt = $('#comments');
            
            //console.log(csfrn);
            //console.log(csfrt);
        // execute ajax
        var params = {osc_id:""+str+"", sess_lang:""+slang+""};
        params[csfrn] = ""+csfrt+"";
        $.post("<?php echo $post_url; ?>",  //'ajax/order_status_comments_ajax.php', //'includes/extra/modules/orders/orders_info_blocks_end/ajax/order_status_comments_ajax.php', //window.location,
            params,
            function(data){
                input_txt.empty().val(data);
            }
            );
    });
    //EOC load order_status_comments into text area via ajax 
    
    //BOC toggle edit form for edit gm_price_status and RRP (UVP) price updates
    $('.nr-prod-edit').click(function() {
        $('.nr-prod-data').not($(this).next()).hide();
        $(this).next('.nr-prod-data').toggle();
    });
    
    $('.nr-head-close').click(function() {
        //$(this).parent().parent('.nr-prod-data').hide();
        $(this).closest('.nr-prod-data').hide();
    });
    
    $('.nr-prod-stat').change(function() {
        var new_inpt = $(this).closest('.nr_upd_prod').find('.nr-inpt-repl'); //adapt to sumoSelect, noRiddle
        if ($(this).val() == '2') {
            //$(this).parent().next('.nr-inpt-repl').show();
            new_inpt.show(); //adapt to sumoSelect, noRiddle
        } else {
            //$(this).parent().next('.nr-inpt-repl').hide();
            new_inpt.hide(); //adapt to sumoSelect, noRiddle
        }
    });
    
    $('.rrp-price').click(function() {
        $(this).toggleClass('rrp-open');
        $(this).next('.nr-inpt-uvp').slideToggle();
    });
    //EOC toggle edit form for edit gm_price_status
    
    //BOC uncheck notify and notify_comments, noRiddle
    $('input[name=notify]').prop('checked', false);
    $('input[name=notify_comments]').prop('checked', false);
    //EOC uncheck notify and notify_comments, noRiddle
    
    //BOC append shipping weight string
    <?php if($ah_ord_tot_weight > 0) { ?>
    $('#displ-shipp-weight').appendTo('.ah-shipp');
    <?php } ?>
    //EOC append shipping weight string
    
    <?php
    //BOC position PDFBillNext buttons
    if (MODULE_PDF_BILL_STATUS == 'True') {
    ?>
    $(function() {
        var $main_div_box = $('.div_box.mrg5'),
            $btts = $('.table .flt-r a.button'),
            $bill_butts = ('#bill-butts');
    <?php if($order_bill != '') { ?>
        $btts.eq(2).hide();
        $btts.eq(3).hide();
    <?php } else { ?>
        //$btts.eq(3).hide();
        $btts.eq(4).hide();
        $btts.eq(2).prependTo($bill_butts);
    <?php } ?>
        $('#pdf-bill-butt').appendTo($main_div_box);
    });
    <?php 
    }
    //EOC position PDFBillNext buttons
    ?>
});
</script>
<?php
/* --------------------------------------------------------------
   $Id: tax_rates.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(tax_rates.php,v 1.9 2003/03/13); www.oscommerce.com 
   (c) 2003	 nextcommerce (tax_rates.php,v 1.4 2003/08/1); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Taux d\'imposition');

define('TABLE_HEADING_TAX_RATE_PRIORITY', 'Priorité');
define('TABLE_HEADING_TAX_CLASS_TITLE', 'Catégorie fiscale');
define('TABLE_HEADING_COUNTRIES_NAME', 'Pays');
define('TABLE_HEADING_ZONE', 'Zone d\'imposition');
define('TABLE_HEADING_TAX_RATE', 'Taux d\'imposition');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Veuillez faire tous les changements nécessaires');
define('TEXT_INFO_DATE_ADDED', 'Ajouté le:');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification');
define('TEXT_INFO_CLASS_TITLE', 'Nom de la catégorie fiscale:');
define('TEXT_INFO_COUNTRY_NAME', 'Pays');
define('TEXT_INFO_ZONE_NAME', 'Zone d\'imposition:');
define('TEXT_INFO_TAX_RATE', 'Taux d\'imposition (%)');
define('TEXT_INFO_TAX_RATE_PRIORITY', 'Les taux d\'imposition de la même priorité sont additionés, les autres sont mélangés.<br/><br/><b>IMPORTANT:</b> Pour des services électroniques ceux-ci doivent être fixé à 99.<br /><br />Priorité:');
define('TEXT_INFO_RATE_DESCRIPTION', 'Description');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez saisir le nouveau taux d\'imposition avec toutes les données pertinentes');
define('TEXT_INFO_DELETE_INTRO', 'Êtes-vous sùr de voulour supprimer ce taux d\'impositon?');
define('TEXT_INFO_HEADING_NEW_TAX_RATE', 'Nouveau taux d\'imposition');
define('TEXT_INFO_HEADING_EDIT_TAX_RATE', 'Editer le taux d\'imposition');
define('TEXT_INFO_HEADING_DELETE_TAX_RATE', 'Supprimer le taux d\'imposition');
?>

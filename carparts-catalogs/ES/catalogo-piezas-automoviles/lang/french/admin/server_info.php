<?php
/* --------------------------------------------------------------
   $Id: server_info.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(server_info.php,v 1.4 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (server_info.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Information de serveur');

define('TITLE_SERVER_HOST', 'Hébergeur de serveur:');
define('TITLE_SERVER_OS', 'OS de serveur:');
define('TITLE_SERVER_DATE', 'Date de serveur:');
define('TITLE_SERVER_UP_TIME', 'Temps serveur:');
define('TITLE_HTTP_SERVER', 'Serveur HTTP:');
define('TITLE_PHP_VERSION', 'Version PHP:');
define('TITLE_ZEND_VERSION', 'Zend:');
define('TITLE_DATABASE_HOST', 'Hébergeur de base de données:');
define('TITLE_DATABASE', 'Base de données:');
define('TITLE_DATABASE_DATE', 'Date de base de données:');
define('TITLE_SSL_VERSION', 'Version SSL:');
?>

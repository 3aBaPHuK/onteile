﻿<?php
if(!isset($_SESSION['customer_id'])) xtc_redirect(xtc_href_link(FILENAME_DEFAULT)); //redirect to start page if not logged in, noRiddle

//echo '<br /><br /><pre>'.print_r($_GET, true).'</pre>';
//echo '<pre>'.xtc_get_all_get_params(array('language', 'currency')).'</pre>';
// Include soap request class
include('nr_catalogue_files/guayaquillib/data/requestOem.php');
include('nr_catalogue_files/extender.php');

// Create request object
$request = new GuayaquilRequestOEM($_GET['c'], $_GET['ssd'], Config::$catalog_data);
if (Config::$useLoginAuthorizationMethod) {
    $request->setUserAuthorizationMethod(Config::$userLogin, Config::$userKey);
}

// Append commands to request
$request->appendGetCatalogInfo();
if (@$_GET['spi2'] == 't')
    $request->appendGetWizard2();

// Execute request
$data = $request->query();

// Check errors
if ($request->error != '') {
    echo $request->error;
} else {
    $cataloginfo = $data[0]->row;
    
    //BOC new html, noRiddle
    echo '<div class="main-2flts-cont cf">';
    //echo '<div class="log-and-butt col-lg-1 col-md-1 col-sm-12 col-xs-12">';
    //echo '<div class="brand-logo"><img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/75x75/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['brand_icon'].'" alt ="" /></div>';
    /*echo '<div class="top-buttons">'
         .'<a href="/katalog_shop_test/"><span class="btn button-catalogue"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_brand_choice').'</span></a>&nbsp;'
         .'<a href="'.xtc_href_link(FILENAME_CATALOGUE_SHOPPING_CART).'"><span class="btn button-catalogue"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_shopp_cart').'</span></a>'
         .'</div>';*/
    //echo '</div>';
    //EOC new html, noRiddle

    //BOC new function to prove whether wizardsearch2 is available
    function has_wizard() {
        global $cataloginfo;
        $c = 0;
        foreach ($cataloginfo->features->feature as $feature) {
            if((string)$feature['name'] == 'wizardsearch2') {
                $c += 1;
            }
        }
        return $c;
    }
    //echo '<pre>'.(has_wizard() > 0 ? 'true' : 'false').'</pre>';
    //EOC new function to prove whether wizardsearch2 is available
    
    $add_class = has_wizard() ? 'right-2flts ' : 'left-2flts'; //class depending on having wizard, noRiddle
    
    foreach ($cataloginfo->features->feature as $feature) {
        //echo '<pre>'.print_r($feature, true).'</pre>';
        switch ((string)$feature['name']) {
            case 'vinsearch':
                //BOC temporarily no VIN search for..., 17-07-2020, noRiddle, revoked 03-08-2020, handle this in array in cebtral config, 10-2020, noRiddle
                if(!in_array($_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'], $remote_url[$cat_env]['disallow_vin_search'])) {
                    //echo '<div class="col-md-5">';
                    echo '<div class="'.$add_class.' nr-highlightbox">'; //add class if wizardsearch2 available
                    echo '<div class="vin-search">'; //noRiddle
                    include('nr_catalogue_files/forms/vinsearch.php');
                    echo '</div>'; //noRiddle
                    echo '</div>'; //noRiddle
                }
                //EOC temporarily no VIN search for..., 17-07-2020, noRiddle
                break;
            /*case 'framesearch':
                $formframe = $formframeno = '';
                echo '<div class="vin-search">'; //noRiddle
                include('nr_catalogue_files/forms/framesearch.php');
                echo '</div>'; //noRiddle
                break;*/ //we don't need this, noRiddle
            case 'wizardsearch2':
                $wizard = $data[1];
                echo '<div class="left-2flts nr-highlightbox">'; //noRiddle
                echo '<div class="vin-search">'; //noRiddle
                include('forms/wizardsearch2.php');
                echo '</div>'; //noRiddle
                echo '</div>'; //noRiddle
                break;
        }
    }

    /*if ($cataloginfo->extensions->operations) { //don't need that, noRiddle
        foreach ($cataloginfo->extensions->operations->operation as $operation) {
            if ($operation['kind'] == 'search_vehicle') {
                echo '<div class="vin-search">'; //noRiddle
                include('nr_catalogue_files/forms/operation.php');
                echo '</div>'; //noRiddle
            }
        }
    }*/
    
    //BOC new html, noRiddle
    echo '</div>';
    //EOC new html, noRiddle
}
?>
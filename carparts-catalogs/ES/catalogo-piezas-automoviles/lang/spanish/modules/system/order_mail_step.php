<?php
/* -----------------------------------------------------------------------------------------
   $Id: order_mail_step.php 9865 2016-05-25 11:30:53Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_MAIL_STEP_TEXT_TITLE', 'Confirmación manual del encargo');
define('MODULE_ORDER_MAIL_STEP_TEXT_DESCRIPTION', 'Confirmación manual del encargo.<br/><b>Importante:</b> Para ello, sólo se deben utilizar las modalidades de pago para las que no se pide al cliente que pague en la caja.');

define('MODULE_ORDER_MAIL_STEP_STATUS_TITLE', 'Estado');
define('MODULE_ORDER_MAIL_STEP_STATUS_DESC', 'Estado del módulo');

define('MODULE_ORDER_MAIL_STEP_ORDERS_STATUS_ID_TITLE', 'Estado de pedido');
define('MODULE_ORDER_MAIL_STEP_ORDERS_STATUS_ID_DESC', '¿Qué estado de pedido debe establecerse al enviar la confirmación del encargo?');
?>

<?php
/**************************************************
* file: add_to_order_details_attributes.php
* use: get more information for attributes in cart
* (c) noRiddle 07-2017
**************************************************/

$module_content[$i]['ATTRIBUTES'][$subindex]['VALUES_PRICE'] = $xtPrice->xtcFormat($attributes['options_values_price'], true, $products[$i]['tax_class_id']);
$module_content[$i]['ATTRIBUTES'][$subindex]['OPTIONS_ID'] = $attributes['options_id'];
?>
<?php
/* --------------------------------------------------------------
   $Id: reviews.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(reviews.php,v 1.6 2002/02/06); www.oscommerce.com 
   (c) 2003	 nextcommerce (reviews.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Reseñas de productos');

define('TABLE_HEADING_PRODUCTS', 'Artículo');
define('TABLE_HEADING_CUSTOMER', 'Cliente');
define('TABLE_HEADING_RATING', 'Reseña');
define('TABLE_HEADING_DATE_ADDED', 'agregado el');
define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_STATUS', 'Estado');

define('ENTRY_PRODUCT', 'Artículo:');
define('ENTRY_FROM', 'de:');
define('ENTRY_DATE', 'Fecha:');
define('ENTRY_REVIEW', 'Reseña:');
define('ENTRY_REVIEW_TEXT', '<span class="smallText colorRed"><b>AVISO:</b></span> <span class="smallText">¡HTML no se convierte! </span>');
define('ENTRY_RATING', 'Calificación:');

define('TEXT_INFO_DELETE_REVIEW_INTRO', '¿Está seguro de que quiere borrar esta reseña?');

define('TEXT_INFO_DATE_ADDED', 'agregado el:');
define('TEXT_INFO_LAST_MODIFIED', 'última modificación:');
define('TEXT_IMAGE_NONEXISTENT', 'LA IMAGEN NO EXISTE.');
define('TEXT_INFO_REVIEW_AUTHOR', 'escrito por:');
define('TEXT_INFO_REVIEW_RATING', 'Calificación:');
define('TEXT_INFO_REVIEW_READ', 'leído :');
define('TEXT_INFO_REVIEW_SIZE', 'Tamaño:');
define('TEXT_INFO_PRODUCTS_AVERAGE_RATING', 'Calificación promedia:');

define('TEXT_OF_5_STARS', '¡%s de 5 estrellas!');
define('TEXT_GOOD', '<span class="smallText colorRed"><b>BIEN</b></span>');
define('TEXT_BAD', '<span class="smallText colorRed"><b>MAL</b></span>');
define('TEXT_INFO_HEADING_DELETE_REVIEW', 'Borrar reseña');
?>

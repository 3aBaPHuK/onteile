<?php
/* --------------------------------------------------------------
   $Id: campaigns.php 10597 2017-01-23 18:10:51Z Tomcraft $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(manufacturers.php,v 1.14 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (manufacturers.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Poursuite des campagnes');

define('TABLE_HEADING_CAMPAIGNS', 'Campagnes');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_HEADING_NEW_CAMPAIGN', 'Nouveau Campagne');
define('TEXT_HEADING_EDIT_CAMPAIGN', 'Modifier la campagne');
define('TEXT_HEADING_DELETE_CAMPAIGN', 'Supprimer la campagne');

define('TEXT_CAMPAIGNS', 'Campagnes:');
define('TEXT_DATE_ADDED', 'Ajouté le:');
define('TEXT_LAST_MODIFIED', 'Dernière modification le:');
define('TEXT_LEADS', 'Pilotage:');
define('TEXT_SALES', 'Sales:');
define('TEXT_LATE_CONVERSIONS', 'Late Conversions:');
define('TEXT_NEW_INTRO', 'Veuillez entrer une nouvelle campagne.');
define('TEXT_EDIT_INTRO', 'Veuilez exécuter les modifications nécessaires');

define('TEXT_CAMPAIGNS_NAME', 'Nom de la campagne:');
define('TEXT_CAMPAIGNS_REFID', 'refID de la campagne:');
define('TEXT_DISPLAY_NUMBER_OF_CAMPAIGNS', 'Campagnes suivies:');

define('TEXT_DELETE_INTRO', 'Vous en êtes sûr de vouloir supprimer cette campagne?');

define('TEXT_CAMPAIGNS_ERROR_REFID', 'Veuillez indiquer un refID valable');
define('TEXT_CAMPAIGNS_ERROR_REFID_EXISTS', 'Le refID indiqué existe déjà');
?>

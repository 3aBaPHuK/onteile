<?php
/**********************************
* do_order_details_cart_total.php
*
* (c) 04-201, noRiddle
**********************************/

//BOC build cart link for order in other shop
//refering to /includes/extra/modules/order_details_cart_content/add_to_order_details_cart.php
//Link pattern: https://www.online-teile.com/xxx-ersatzteile?action=custom_cart&p_id=360258-1~359853-1
if(defined('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS')) {
    if(MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS == 'true') {
        $foreign_shop_cart_link_part = '';
        
        ksort($redirect_model_qty_arr);
        //echo '<br><br><br><pre>'.print_r($redirect_model_qty_arr, true).'</pre>';
        foreach($redirect_model_qty_arr as $red_mdl => $red_qty) {
            $redirect_model_str .= "'".$red_mdl."'".',';
        }
        $redirect_model_str = rtrim($redirect_model_str, ',');
        //echo '<br><br><br><pre>'.$redirect_model_str.'</pre>';
        
        $redirect_prid_qu_str = "SELECT products_id, products_model FROM products WHERE products_model IN(".$redirect_model_str.") ORDER BY products_model DESC";
        $redirect_prid_qu = mysqli_query($ext_shp_db_link, $redirect_prid_qu_str);
        
        //while($redirect_prid_arr = mysqli_fetch_array($redirect_prid_qu, MYSQLI_ASSOC)) {
        while($redirect_prid_arr = mysqli_fetch_assoc($redirect_prid_qu)) {
            $foreign_shop_cart_link_part .= $redirect_prid_arr['products_id'].'-'.$redirect_model_qty_arr[$redirect_prid_arr['products_model']].'~';
        }
        
        $foreign_shop_cart_link_part = rtrim($foreign_shop_cart_link_part, '~');
        //$foreign_shop_cart_link = HTTP_SERVER.'/'.$redir_shop.$shop_suffix.'?action=custom_cart&p_id='.$foreign_shop_cart_link_part;
        $foreign_shop_cart_link = HTTP_SERVER.'/'.$foreign_shop.'?action=custom_cart&p_id='.$foreign_shop_cart_link_part; //use $foreign_shop, have that already defined in /includes/extra/application_top/application_top_end/do_application_top_end.php

        $smarty->assign('FOREIGN_SHOP_CART_LINK', $foreign_shop_cart_link);
        //$smarty->assign('WHICH_FOREIGN_SHOP', sprintf(NR_EXPLAIN_ORDER_IN_OTHER_SHOP, ucwords($redir_shop)));
        $fs_our_txt = nl2br(constant('MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_'.strtoupper($_SESSION['language_code'])));
        $smarty->assign('WHICH_FOREIGN_SHOP', sprintf($fs_our_txt, strtoupper($redir_shop)));
    }
}
//EOC build cart link for order in other shop
?>
<?php
/*********************************************************************
* file: shopping_cart_link_ajax.php
* path: /lang/french/admin/
* use: language file for module
* 
* (c) copyright 10-2020, noRiddle
*********************************************************************/

define('GSCL_PROD_EXISTS_TXT', '<br /><span style="color:green;">OK, model no. exists | Product name: %s | List price incl. VAT.: %s | Product ID: %s</span>');
define('GSCL_NOT_PROD_STAT1_TXT', '<br /><span style="color:red;">! Product doesn\'t have products_status 1 (= product is deactivated) ! Please verify in the shop.</span>');
define('GSCL_STAT1_TXT', '(= Price question)');
define('GSCL_STAT2_TXT', '(= no more available)');
define('GSCL_NOT_GMSTAT0_TXT', '<br /><span style="color:red;">! Product has got gm_price_status %s %s ! Please verify in the shop.</span>');
define('GSCL_MODEL_EMPTY_OR_NOTINSHOP_TXT', '<br /><span style="color:red;">! Model number is empty or not existent in the shop !</span>');
<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_loworderfee.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ot_loworderfee.php,v 1.2 2002/04/17); www.oscommerce.com 
   (c) 2003	 nextcommerce (ot_loworderfee.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_LOWORDERFEE_TITLE', 'Recargo por pequeñas cantidades');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESCRIPTION', 'Recargo en caso de no alcanzar el valor de un pedido mínimo');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_TITLE', 'Mostrar recargo por pequeñas cantidades');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_DESC', '¿Quiere ver el recargo por cantidades pequeñas?');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_SORT_ORDER_TITLE', 'Orden de clasificación');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_SORT_ORDER_DESC', 'Orden de visualización');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_TITLE', 'Permitir recargo por pequeñas cantidades');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_DESC', '¿Quiere permitir el recargo por cantidades pequeñas?');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_TITLE', 'Recargo por pequeñas cantidades para pedidos bajo');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_DESC', 'El recargo por pequeñas cantidades se añadirá a pedidos inferiores de este valor.');

define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_INTERNATIONAL_TITLE', 'Recargo por pequeñas cantidades para pedidos internacionales bajo');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_INTERNATIONAL_DESC', 'El recargo por pequeñas cantidades se añadirá a pedidos internacionales inferiores de este valor.');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_TITLE', 'Recargo');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_DESC', 'Recargo por pequeñas cantidades.');

define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_INTERNATIONAL_TITLE', 'Recargo internacional');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_INTERNATIONAL_DESC', 'Recargo por pequeñas cantidades internacional.');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_TITLE', 'Calcular recargo por pequeñas cantidades por zonas');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_DESC', 'Recargo por pequeñas cantidades para pedidos que se enviarán a este lugar.');
  
define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_TITLE', 'Clase de impuestos');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_DESC', 'Utilizar la siguiente clase de impuestos para el recargo por pequeñas cantidades.');
?>

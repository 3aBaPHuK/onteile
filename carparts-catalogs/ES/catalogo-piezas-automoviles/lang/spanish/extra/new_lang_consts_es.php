<?php
/**************************************
* file: new_lang_consts_de.php
* use: new language constants
* (c) noRiddle 01-2017
**************************************/

define('AH_IMAGE_BUTTON_SEARCH', 'INTRODUZCA AQUÍ EL NÚMERO DE PIEZA');
//new error for invalid part no coming from search_results.php
define('AH_JS_AT_LEAST_ONE_INPUT', 'Desafortunadamente, no hemos encontrado una pieza de repuesto bajo el número de pieza que introdujo.\n Le ofrecemos la siguiente asistencia:');
//errors for new field VIN in checkout_payment, noRiddle
define('ERROR_NO_VIN_INDICATED', 'Por favor, introduzca el número de identificación de su vehículo (VIN 17 dígitos). <br />Con esta información nos aseguramos de que las piezas pedidas encajen en su coche.<br />En caso de que el FIN no sea relevante para su pedido, introduzca "no" en el campo FIN.');
define('ERROR_VIN_NOT_PLAUSIBLE', 'Por favor, introduzca un número de identificación de vehículo válido (VIN 17 dígitos).<br />Con esta información nos aseguramos de que las piezas pedidas encajen en su coche.');
//new email fields, noRiddle
define('EMAIL_PRODUCTS_PRICE_QUESTION', 'Solicitud de producto para: ');
define('EMAIL_VIN', 'Número de identificación del vehículo (VIN): ');
define('AH_ERROR_VIN_EMPTY', '<li><b>Número de identificación del vehículo (VIN):</b><br />Por favor, introduzca el número de identificación de su vehículo (VIN 17 dígitos).<br />Con esta información nos aseguramos de que la pieza solicitada encaje en su coche.</li>');
define('AH_ERROR_VIN_NOPLAUSIBLE', '<li><b>Número de identificación del vehículo (VIN):</b><br />Por favor, introduzca un número de identificación de vehículo válido (VIN 17 dígitos).<br />Con esta información nos aseguramos de que la pieza solicitada encaje en su coche.</li>');

//BOC SEO meta title texts, noRiddle
define('SEO_META_TITLE_1', 'comprar barato en línea');
define('SEO_META_TITLE_2', 'Pieza de repuesto en línea');
define('SEO_META_TITLE_3', 'Pedir pieza de repuesto en línea');
define('SEO_META_TITLE_4', 'para la búsqueda de VIN en el catálogo de coches en línea');
//EOC SEO meta title texts, noRiddle

define('NR_GOOG_DEFAULT_TXT', 'Idiomas adicionales');

//BOC new for ADAC field, noRiddle
define('ENTRY_ADAC_NO_FALSE', 'Su número de socio de ADAC introducido no es correcto.<br />Por favor, compruébelo.<br />Puede crear una cuenta sin número de socio de ADAC en cualquier momento.<br />En este caso, deje el campo correspondiente en blanco.');
define('ENTRY_ACCOUNT_ADAC_NO_FALSE', 'Su número de socio de ADAC introducido no es correcto.<br />Por favor, compruébelo.');
define('TXT_SUCCESS_ADAC_MEMBER', 'Su estado de cliente ha sido configurado como miembro de ADAC, ahora puede comprar a precios reducidos.');
define('TXT_ERROR_ADAC_MEMBER', 'Su estado de cliente no pudo ser cambiado a miembro de ADAC, le pedimos disculpas.<br />Por favor <a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=7').'" title="Kontakt">contáctenos</a> por este asunto.');
//EOC new for ADAC field, noRiddle

//BOC inclusion for affiliate program, noRiddle
require(DIR_WS_LANGUAGES . 'spanish/'.'affiliate_spanish.php');
//EOC inclusion for affiliate program, noRiddle

//BOC additional text for login.php, noRiddle
define('ADD_TEXT_GUEST_LOGIN', 'También puede obtener beneficios de cliente como <a class="iframe" href="'.xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=964').'" title="Rabatte" target="_blank">Descuentos de ADAC, club y taller</a>, sólo como cliente registrado.');
define('ADD_TEXT_NEW_LOGIN', 'Damos gran importancia a la <a class="iframe" href="'.xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=2').'" title="Datenschutz" target="_blank">protección de sus datos</a>.');
define('ADD_INFO_TEXT_NEW_LOGIN', '<ul><li>Si usted es un <strong>cliente comercial</strong>, por ejemplo si tiene un <strong>taller</strong>, puede comprar con <strong>condiciones especiales</strong>.<br />Por favor, crea una cuenta como empresa y indique su RFC. Luego contactenos por nuestro <a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=7').'" title="Contacto">formulario de contacto</a> y indique los detalles de su empresa (Nombre comercial, ubicación, tipo de negocio).<br />Nosotros entonces le daremos acceso a productos descontados.</li>'.($cat_env == 'de' ? '<li>Tambien ofrecemos <strong>descuentos</strong> para miembros de <strong>clubs</strong> y <strong>ADAC</strong>.<br />To get the Für die ADAC-conditions just indicate your ADAC member number in "Your account" to which you will find the link in the top menu.</li>' : '<li>Tambien ofrecemos  <strong>descuentos</strong> para miembros de <strong>clubs</strong>. Por favor transmitanos su tarjeta de club por nuestro <a href="xx_7.html" title="transmitir fichero por formulario de contacto">formulario de contacto</a>!')
.'</ul>');
//EOC additional text for login.php, noRiddle

//BOC needed for order mail if deposit, noRiddle
define('AH_INCLUSIVE', 'incl.');
//EOC needed for order mail if deposit, noRiddle

//BOC new texts for catalogue, noRiddle
define('S_TXT_PARTNO', 'Número de pieza');
define('S_TXT_NAME', 'Nombre');
define('S_TXT_QTY', 'Cantidad');
define('S_TXT_CHECK', 'Marcar');
define('S_TXT_INTOCART', 'Añadir la pieza marcada a la cesta de compra');
define('S_SPECIALS_HEADING_TXT', 'Una selección de nuestras atractivas ofertas de accesorios por %s');
define('S_PART_WAS_REPLACED_WITH', 'Esta pieza ha sido reemplazada por');
define('IMAGE_BUTTON_CATALOGUE_CHECKOUT', 'Completar pedido en la tienda de la marca');
define('IMAGE_BUTTON_UPDATE_CAT_CART', 'Actualizar %s cesta de compra');
define('NR_UPDATE_CART', 'Su %s cesta de compra contiene:');
define('NR_NOW_CREDIT_FOR_VINSEARCH', '¿Por qué no veo el campo de búsqueda VIN(VIN) ?<br />Al crear su cuenta ha recibido 10 puntos gratuitos para utilizar la búsqueda por VIN.<br />Puede obtener más consultas gratuitas, o comprando piezas de repuesto en una de las tiendas, o bien tendría que <a href="'.xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=1').'" title="Comprar crédito para búsqueda VIN">» comprar aquí</a>más consultas.<br />La identificación de su vehículo por parámetros (si está disponible) es gratuita para usted en cualquier caso.');
define('TXT_SEND_ORDER_VI_CREDIT', 'Ha añadido %d puntos a su cuenta de crédito de búsqueda VIN.');

define('TXT_MAY_WE_HELP', '¿ Necesita ayuda ?');
define('TXT_DIRECTLY_TO_PARTNER_BUTTON1', 'Directamente a nuestro');
define('TXT_DIRECTLY_TO_PARTNER_BUTTON2', ' socio');
define('TXT_CONTACT_US_IF_NEED_HELP', '¿ Algunas preguntas sobre esta marca o una refacción ?');
define('TXT_IMPORT_INFO_VIN', 'No todos NIVs se encuentran en nuestra base de datos. Si el VIN introducido no se encuentra por favor, si es disponible, haga uso de "Identificación del vehículo por parámetros" aqui a la izquierda.<br />');
define('TXT_IMPORT_INFO_VIN_VAG', 'Por razones técnicas solamente tenemos registrados aproximadamente 50% de todos los VIN (número de serie) para la presente marca. Si el VIN ingresado no existe en nuestro catálogo, por favor, intente usar la "Busqueda por Parámetros<br />');
//EOC new texts for catalogue, noRiddle

//BOC new texts for cookieconsent plugin, noRiddle
define('COOKIES_TXT_NEW', '<i class="fa fa-cog"></i> Al continuar utilizando este sitio web, usted acepta el uso de cookies que permitirán un acceso fluido a nuestro sitio web.');
define('COOKIES_LINKTXT_TITLE', 'Más información');
define('COOKIES_LINKTXT', '<i class="fa fa-info-circle"></i>');
define('COOKIES_CLOSE_TITLE', 'cerrar');
define('COOKIES_CLOSE', '<i class="fa fa-close"></i>');
//EOC new texts for cookieconsent plugin, noRiddle

//BOC DSGVO, noRiddle
define('NR_DAT_THPARTY_SGPCOMP_TITLE', 'Consentimiento para el envío de la dirección de correo y el número de teléfono a nuestro proveedor de servicios de envío');
define('NR_DAT_THPARTY_SGPCOMP_TEXT', 'Acepto que mi dirección de correo electrónico y mi número de teléfono puedan ser transmitidos al proveedor de servicios de envío para que pueda ponerse en contacto conmigo si es necesario para coordinar la entrega o para informarme de los cambios de estado relacionados con la entrega.<br />Por razones logísticas y para garantizarles la entrega más rápida posible, se requiere su consentimiento. En caso contrario, debe seleccionar "Recogida por cuenta propia".');
define('NR_ERROR_DTTSC', 'Si no elige la opción de "Recogida por cuenta propia", deberá dar abajo su "consentimiento para la transmisión de la dirección de correo y el número de teléfono a nuestro proveedor de servicios de envío" .');
//EOC DSGVO, noRiddle 

//BOC käufersiegel, noRiddle
define('KS_THIS_SHOP_WAS', 'La tienda fue calificada en kaeufersiegel.de con ');
define('KS_OF', ' de ');
define('KS_STARES_RATED', 'estrellas - basado en ');
define('KS_RATINGS', ' calificaciones');
define('NR_DAT_THPARTY_KFSGL_TITLE', 'Estoy de acuerdo después de recibir mi producto pedido con recibir un correo con la petición para una evaluación. (¡No publicidad!)');
define('NR_DAT_THPARTY_KFSGL_YES', 'Si: ');
define('NR_DAT_THPARTY_KFSGL_NO', 'No: ');
define('NR_DAT_THPARTY_KFSGL_ERR', 'Por favor, marque si podemos enviarle un correo electrónico con una solicitud de evaluación (encima del botón "Comprar").');
//EOC käufersiegel, noRiddle

//BOC new button for login box, noRiddle
define('IMAGE_BUTTON_NEW_CUST', 'Cliente nuevo ?');
//EOC new button for login box, noRiddle

//BOC button "all brands at a glance" in shops box, 10-2020, noRiddle
define('AH_ALL_BRANDS_MAIN_TXT', 'Portal principal');
define('AH_ALL_BRANDS_VIEW_TXT', '&raquo; Todas marcas de una mirada');
//EOC button "all brands at a glance" in shops box, 10-2020, noRiddle

//01-2021, noRiddle
define('NO_GENDER', 'No saludo');
?>
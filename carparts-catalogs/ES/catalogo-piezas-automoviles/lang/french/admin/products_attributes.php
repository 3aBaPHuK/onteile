<?php
/* --------------------------------------------------------------
   $Id: products_attributes.php 1101 2005-07-24 14:51:13Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(products_attributes.php,v 1.9 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (products_attributes.php,v 1.4 2003/08/1); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE_OPT', 'Spécificités de l\'article');
define('HEADING_TITLE_VAL', 'Valeur d\'option');
define('HEADING_TITLE_ATRIB', 'Spécificités de l\'article');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_PRODUCT', 'Nom de l\'article');
define('TABLE_HEADING_OPT_NAME', 'Nom de l\'option');
define('TABLE_HEADING_OPT_VALUE', 'Valeur de l\'option');
define('TABLE_HEADING_OPT_PRICE', 'Prix');
define('TABLE_HEADING_OPT_PRICE_PREFIX', 'Signe algébrique (+/-)');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_DOWNLOAD', 'Articles téléchargables:');
define('TABLE_TEXT_FILENAME', 'Nom du fichier:');
define('TABLE_TEXT_MAX_DAYS', 'Période:');
define('TABLE_TEXT_MAX_COUNT', 'Quantité maximale du téléchargement:');

define('MAX_ROW_LISTS_OPTIONS', 10);

define('TEXT_WARNING_OF_DELETE', 'Des articles et des spécificités d\'option sont reliés à cette option - il est déconseillé de la supprimer.');
define('TEXT_OK_TO_DELETE', 'Aucun d\'article et aucune de spécificité d\'option est relié à cette option - elle peut être supprimée.');
define('TEXT_SEARCH', 'Rechercher:');
define('TEXT_OPTION_ID', 'ID de l\'option');
define('TEXT_OPTION_NAME', 'Nom de l\'option');

// BOF - Tomcraft - 2009-11-07 - Added sortorder to products_options
define('TABLE_HEADING_SORTORDER', 'Tri');
define('TEXT_SORTORDER', 'Tri');
// EOF - Tomcraft - 2009-11-07 - Added sortorder to products_options
define('TEXT_OPTION_ID_FILTER', 'Filtre:');
?>

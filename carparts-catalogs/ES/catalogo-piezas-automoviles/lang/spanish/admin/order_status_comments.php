<?php
/*************************************************************
* file: order_status_comments.php
* use: language constants for order_status_comments
* (c) noRiddle 12-2016
*************************************************************/

define('HEADING_TITLE', 'Textos de comentario del estado del pedido');

define('TEXT_SORT_ASC', 'ascendente');
define('TEXT_SORT_DESC', 'descendente');

define('TABLE_HEADING_ORDER_STATUS_COMMENTS_ID', 'ID');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_TITLE', 'Título del comentario');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_TEXT', 'Texto del comentario');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_SORT_ORDER', 'Clasificación');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_STATUS', 'Estado');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_ACTION', 'Acción');

define('TEXT_ORDER_STATUS_COMMENT', 'Plantilla de comentarios %d');
define('TEXT_INFO_DELETE_COMMENT_INTRO', '¿Está seguro de que quiere borrar esta plantilla de comentarios?');
define('TEXT_DELETE_COMMENT', '¿Borrar plantilla de comentarios?');
define('TEXT_INFO_HEADING_DELETE_ORDER_STATUS_COMMENT', 'Borrar plantilla de comentarios');
define('TEXT_DATE_ADDED_ORDER_STATUS_COMMENT', 'agregado el:');
define('TEXT_LAST_MODIFIED_ORDER_STATUS_COMMENT', 'Última modificación el:');

define('TEXT_ORDER_STATUS_COMMENTS_STATUS', 'Estado');
define('TEXT_ORDER_STATUS_COMMENTS_SORT_ORDER', 'Clasificación');
define('TEXT_ORDER_STATUS_COMMENTS_TITLE', 'Título del texto del comentario:');
define('TEXT_ORDER_STATUS_COMMENTS_TEXT', 'Texto del comentario:');

define('TEXT_DISPLAY_NUMBER_OF_ORDER_STYTUS_COMMENTS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> textos de comentarios)');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id: french.php 10896 2017-08-11 11:31:54Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(french.php,v 1.119 2003/05/19); www.oscommerce.com
   (c) 2003 nextcommerce (french.php,v 1.25 2003/08/25); www.nextcommerce.org
   (c) 2006 XT-Commerce
   
   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

/*
 * 
 *  DATE / TIME
 * 
 */
 
define('HTML_PARAMS', 'dir="ltr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml"');
@setlocale(LC_TIME, 'fr_FR.UTF-8' ,'fr_FR@euro', 'fr_FR', 'fr-FR', 'fr', 'fr', 'fr_FR.ISO_8859-1', 'French','fr_FR.ISO_8859-15');

define('DATE_FORMAT_SHORT', '%d.%m.%Y');
define('DATE_FORMAT_LONG', '%A, %d. %B %Y');
define('DATE_FORMAT', 'd.m.Y');
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');
define('DOB_FORMAT_STRING', 'jj.mm.aaaa');

function xtc_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 0, 2) . substr($date, 3, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 3, 2) . substr($date, 0, 2);
  }
}

require_once(DIR_FS_INC.'auto_include.inc.php');
foreach(auto_include(DIR_WS_LANGUAGES.'french/extra/','php') as $file) require ($file);

define('TITLE', STORE_NAME);
//define('HEADER_TITLE_TOP', 'Startseite');    
define('HEADER_TITLE_TOP', 'Choix de marque');
//define('HEADER_TITLE_CATALOG', 'Katalog');
define('HEADER_TITLE_CATALOG', str_replace('pièces de rechange', 'spare parts', implode(' ', explode('-', $shop))));

// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency when changing language, 
// instead of staying with the applications default currency
define('LANGUAGE_CURRENCY', 'EUR');

define('MALE', 'Monsieur');
define('FEMALE', 'Madame');

/*
 * 
 *  BOXES
 * 
 */

// text for gift voucher redeeming
define('IMAGE_REDEEM_GIFT', 'Utiliser votre coupon !');

define('BOX_TITLE_STATISTICS', 'Statistiques:');
define('BOX_ENTRY_CUSTOMERS', 'Clients');
define('BOX_ENTRY_PRODUCTS', 'Article:');
define('BOX_ENTRY_REVIEWS', 'Critiques:');
define('TEXT_VALIDATING', 'Non confirmé');

// manufacturer box text
define('BOX_MANUFACTURER_INFO_HOMEPAGE', 'Page d\'accueil de %s');
define('BOX_MANUFACTURER_INFO_OTHER_PRODUCTS', 'Autres articles');

define('BOX_HEADING_ADD_PRODUCT_ID', 'Mettre dans le panier');
  
define('BOX_LOGINBOX_STATUS', 'Groupe de clients: ');
define('BOX_LOGINBOX_DISCOUNT', 'Rabais sur l\'article');
define('BOX_LOGINBOX_DISCOUNT_TEXT', 'Rabais');
define('BOX_LOGINBOX_DISCOUNT_OT', '');

// reviews box text in includes/boxes/reviews.php
define('BOX_REVIEWS_WRITE_REVIEW', 'Évaluer cet article!');
define('BOX_REVIEWS_NO_WRITE_REVIEW', 'Évaluation ne pas possible.');
define('BOX_REVIEWS_TEXT_OF_5_STARS', '%s de 5 étoiles');

// pull down default text
define('PULL_DOWN_DEFAULT', 'Veuillez choisir');

// javascript messages
define('JS_ERROR', 'Il manque des informations nécessaires ! Veuillez les remplir correctement.\n\n');

define('JS_REVIEW_TEXT', '* Le texte doit se composer d\'au moins ' . REVIEW_TEXT_MIN_LENGTH . ' caractères.\n\n');
define('JS_REVIEW_RATING', '* Veuillez entrer votre évaluation.\n\n');
define('JS_ERROR_NO_PAYMENT_MODULE_SELECTED', '* Veuillez choisir un mode de paiement pour votre commande.\n');
define('JS_ERROR_SUBMITTED', 'Cette page a déjà été confirmé. Cliquez sur OK et patientez jusqu\'à ce que le processus soit terminé.');
define('ERROR_NO_PAYMENT_MODULE_SELECTED', '* Veuillez choisir un mode de paiement pour votre commande.');
define('JS_ERROR_NO_SHIPPING_MODULE_SELECTED', '* Veuillez choisir un mode de livraison pour votre commande.\n');
define('JS_ERROR_CONDITIONS_NOT_ACCEPTED', '* Si vous n\'acceptez pas les Conditions générales,\nnous ne pouvons malheureusement pas traiter votre commande!\n\n');
define('JS_ERROR_REVOCATION_NOT_ACCEPTED', '* Si vous n\'acceptez pas l\'expiration du droit de rétractation pour des articles virtuelles, nous ne pouvons malheureusement pas traiter votre commande!\n\n');
define('JS_REVIEW_AUTHOR', '* Veuillez saisir votre nom.\n\n');

/*
 * 
 * ACCOUNT FORMS
 * 
 */

define('ENTRY_COMPANY_ERROR', '');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_GENDER_ERROR', 'Veuillez choisir votre titre.');
define('ENTRY_GENDER_TEXT', '*');
define('ENTRY_FIRST_NAME_ERROR', 'Votre prénom doit se composer d\'au moins ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caractères.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME_ERROR', 'Votre nom doit se composer d\'au moins ' . ENTRY_LAST_NAME_MIN_LENGTH . ' caractères.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Votre date de naissance doit être au format jj.mm.aaaa (ex: 21.05.1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (p. ex. 21.05.1970)');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Votre adresse email doit se composer d\'au moins ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' caractères.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Votre adresse email saisie est incorrecte ou a déjà été registrée. Veuillez la vérifier.');
//BOC new error for create_account in case mail already exists, noRiddle
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR_EXISTS_NR', 'Ein Konto mit dieser Mail-Adresse existiert bereits.<br />Wenn es sich um Ihr Konto handelt lassen Sie sich ein neues Passwort senden. Dieses können Sie anschließend unter "Mein Konto" nach Wunsch ändern.');
//EOC new error for create_account in case mail already exists, noRiddle
define('ENTRY_EMAIL_ERROR_NOT_MATCHING', 'Vos adresses email ne sont pas identiques.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Votre adresse email  saisie existe déjà. Veuillez la vérifier.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
//BOC new for house no., noRiddle
//define('ENTRY_STREET_ADDRESS_ERROR', 'Strasse/Nr. muss aus mindestens ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' Zeichen bestehen.');
define('ENTRY_STREET_ADDRESS_ERROR', 'Rue/No doit se composer d\'au moins' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caratères.');
//EOC new for house no., noRiddle
define('ENTRY_STREET_ADDRESS_ERROR', 'Rue/No doit se composer d\'au moins' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caratères.');
define('ENTRY_STREET_ADDRESS_TEXT', '*');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE_ERROR', 'Votre code postal doit se composer d\'au moins ' . ENTRY_POSTCODE_MIN_LENGTH . ' caractères.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY_ERROR', 'La ville doit se composer d\'au moins ' . ENTRY_CITY_MIN_LENGTH . ' caractères.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE_ERROR', 'Votre région doit se composer d\'au moins ' . ENTRY_STATE_MIN_LENGTH . ' caractères.');
define('ENTRY_STATE_ERROR_SELECT', 'Veuillez choisir une région de la liste proposée.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY_ERROR', 'Veuillez choisir un pays de la liste proposée.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Votre numéro de téléphone se composer d\'au moins ' . ENTRY_TELEPHONE_MIN_LENGTH . ' caractères.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_PASSWORD_ERROR', 'Votre mot de passe doit se composer d\'au moins ' . ENTRY_PASSWORD_MIN_LENGTH . ' caractères.');
define('ENTRY_PASSWORD_ERROR_MIN_LOWER', 'Votre mot de passe se composer d\'au moins %s lettres miniscules.');
define('ENTRY_PASSWORD_ERROR_MIN_UPPER', 'Votre mot de passe se composer d\'au moins %s lettres majuscules.');
define('ENTRY_PASSWORD_ERROR_MIN_NUM', 'Votre mot de passe doit se composer d\'au moins %s chiffres.');
define('ENTRY_PASSWORD_ERROR_MIN_CHAR', 'Votre mot de passe doit se composer d\'au moins %s caractères spéciaux.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'Vos mots de passe ne sont pas identiques.');
define('ENTRY_PASSWORD_TEXT', '*');
define('ENTRY_PASSWORD_CONFIRMATION_TEXT', '*');
define('ENTRY_PASSWORD_CURRENT_TEXT', '*');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Votre mot de passe se composer d\'au moins ' . ENTRY_PASSWORD_MIN_LENGTH . ' caractères.');
define('ENTRY_PASSWORD_NEW_TEXT', '*');
define('ENTRY_PASSWORD_NEW_ERROR', 'Votre nouveau mot de passe se composer d\'au moins ' . ENTRY_PASSWORD_MIN_LENGTH . ' caractères.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Vos mots de passe ne sont pas identiques.');

/*
 * 
 *  RESULT PAGES
 * 
 */
 
define('TEXT_RESULT_PAGE', 'Pages:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Afficher de <b>%d</b> à <b>%d</b> (sur un total de <b>%d</b> articles)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Afficher de <b>%d</b> à <b>%d</b> (sur un total de <b>%d</b> commandes)');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'Afficher de <b>%d</b> à <b>%d</b> (sur un total de <b>%d</b> évaluations)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW', 'Afficher de <b>%d</b> à <b>%d</b> (sur un total de <b>%d</b> nouveaux produits)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Afficher de <b>%d</b> à <b>%d</b> (sur un total de <b>%d</b> promotions)');

/*
 * 
 * SITE NAVIGATION
 * 
 */

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'Page précédente');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Page suivante');
define('PREVNEXT_TITLE_PAGE_NO', 'Page %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', '%d pages precédentes');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', '%d pages suivantes');

/*
 * 
 * PRODUCT NAVIGATION
 * 
 */

define('PREVNEXT_BUTTON_PREV', '[<< Précédent]');
define('PREVNEXT_BUTTON_NEXT', '[Suivant>>]');

/*
 * 
 * IMAGE BUTTONS
 * 
 */

define('IMAGE_BUTTON_ADD_ADDRESS', 'Nouvelle adresse');
define('IMAGE_BUTTON_BACK', 'Retour');
define('IMAGE_BUTTON_CHANGE_ADDRESS', 'Changement d\'adresse');
define('IMAGE_BUTTON_CHECKOUT', 'Paiement');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Acheter');
define('IMAGE_BUTTON_CONTINUE', 'Suivant');
define('IMAGE_BUTTON_DELETE', 'Supprimer');
define('IMAGE_BUTTON_LOGIN', 'Se connecter');
define('IMAGE_BUTTON_IN_CART', 'Ajouter au panier');
define('IMAGE_BUTTON_SEARCH', 'Chercher');
define('IMAGE_BUTTON_UPDATE', 'Actualiser');
define('IMAGE_BUTTON_UPDATE_CART', 'Actualiser le panier');
define('IMAGE_BUTTON_WRITE_REVIEW', 'Votre opinion');
define('IMAGE_BUTTON_ADMIN', 'Admin');
define('IMAGE_BUTTON_PRODUCT_EDIT', 'Modifier le produit');
define('IMAGE_BUTTON_SEND', 'Envoyer');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Poursuivre l\'achat');
define('IMAGE_BUTTON_CHECKOUT_STEP2', 'Passer à l\'étape 2');
define('IMAGE_BUTTON_CHECKOUT_STEP3', 'Passer à l\'étape 3');

define('SMALL_IMAGE_BUTTON_DELETE', 'Supprimer');
define('SMALL_IMAGE_BUTTON_EDIT', 'Changer');
define('SMALL_IMAGE_BUTTON_VIEW', 'Afficher');

define('ICON_ARROW_RIGHT', 'Montrer plus');
define('ICON_CART', 'Dans le panier');
define('ICON_SUCCESS', 'Succès');
define('ICON_WARNING', 'Attention');
define('ICON_ERROR', 'Erreur');

define('TEXT_PRINT', 'Imprimer');

/*
 * 
 *  GREETINGS
 * 
 */

define('TEXT_GREETING_PERSONAL', 'Heureux de vous revoir, <span class="greetUser">%s!</span> Souhaitez vous voir nos <a style="text-decoration:underline;" href="%s">nouveaux articles</a>?');
define('TEXT_GREETING_PERSONAL_RELOGON', '<small>Si vous n\'êtes pas %s, veuillez vous identifier avec votre identifiant <a href="%s">ici</a>.</small>');
define('TEXT_GREETING_GUEST', 'Bienvenue <span class="greetUser">visiteur !</span>. Souhaitez-vous <a href="%s">vous connecter</a>? ou bien souhaitez vous ouvrir un <a href="%s">compte client</a>?');

define('TEXT_SORT_PRODUCTS', 'Le tri des articles est ');
define('TEXT_DESCENDINGLY', 'décroissant');
define('TEXT_ASCENDINGLY', 'croissant');
define('TEXT_BY', ' vers ');

define('TEXT_OF_5_STARS', '%s de 5 étoiles!');
define('TEXT_REVIEW_BY', 'de %s');
define('TEXT_REVIEW_WORD_COUNT', '%s mots');
define('TEXT_REVIEW_RATING', 'Évaluation: %s [%s]');
define('TEXT_REVIEW_DATE_ADDED', 'Ajouté le: %s');
define('TEXT_NO_REVIEWS', 'Il n\'existe pas encore d\' évaluations.');
define('TEXT_NO_NEW_PRODUCTS', 'Ces '.MAX_DISPLAY_NEW_PRODUCTS_DAYS.' derniers jours, aucun nouveau article est paru. Au lieu de cela, vous pouvez voir l\'article paru récemment');
define('TEXT_UNKNOWN_TAX_RATE', 'Taux d\'impôsition inconnu');

/*
 * 
 * WARNINGS
 * 
 */

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Attention: Le repértoire d\'installation existe toujours sur: %s. Veuillez supprimer le répertoire pour des raisons de sécurité!');
define('WARNING_CONFIG_FILE_WRITEABLE', 'Attention: Le Shopsoftware modifié eCommerce peut écrire dans le fichier de configuration: %s. Ceci est un risque potentiel - Veuillez mettre les bonnes permissions sur ce fichier!');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Attention: Le répertoire pour les sessions n\'existe pas: ' . xtc_session_save_path() . '. Les sessions ne vont pas fonctionner jusqu\'à ce que le répertoire a été généré!');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Attention: Le Shopsoftware modifié eCommerce ne peut pas écrire dans le répertoire: ' . xtc_session_save_path() . '. Les sessions ne vont pas fonctionner jusqu\'à ce que les bons permissions sont activés!');
define('WARNING_SESSION_AUTO_START', 'Attention: session.auto_start est activé (enabled) - Veuillez désactiver (disabled) cette fonctionnalité PHP sur php.ini et redémarrer le serveur web!');
define('WARNING_DOWNLOAD_DIRECTORY_NON_EXISTENT', 'Attention: Le répertoire pour l\'article téléchargement n\'existe pas: ' . DIR_FS_DOWNLOAD . '. Cette fonction ne vas pas fonctionner jusqu\'à ce que le répertoire a été généré!');

define('SUCCESS_ACCOUNT_UPDATED', 'Votre compte a été actualisé avec succès.');
define('SUCCESS_PASSWORD_UPDATED', 'Votre mot de passe a été modifié avec succès!');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING', 'Le mot de passe que vous venez de saisir ne correspond pas à votre mot de passe enregistré. Veuillez réessayer.');
define('TEXT_MAXIMUM_ENTRIES', '<strong>Remarque:</strong> Vous disposez de %s entrées dans votre carnet d\'adresse!');
define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED', 'L\'entrée sélectionnée a été annulé avec succès.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED', 'Votre carnet d\'adresse a été actualisé avec succès!');
define('WARNING_PRIMARY_ADDRESS_DELETION', 'L\'adresse standard ne peut pas être annulée. Veuillez définir une autre adresse standard au préalable. Ensuite l\'ancienne pourra être annulée.');
define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY', 'Cette entrée dans le carnet d\'adresses n\'est pas disponible.');
define('ERROR_ADDRESS_BOOK_FULL', 'Votre carnet d\'adresses ne peut plus enregistrer de coordonnées. Veuillez annuler une adresse que vous n\'utilisez plus. Ensuite vous pourrez enregistrer une nouvelle adresse.');
define('ERROR_CHECKOUT_SHIPPING_NO_METHOD', 'Mode de livraison pas sélectionnée.');
define('ERROR_CHECKOUT_SHIPPING_NO_MODULE', 'Mode de livraison pas disponible.');

//  conditions check

define('ERROR_CONDITIONS_NOT_ACCEPTED', '* Si vous n\'acceptez pas nos conditions générales, nous ne pourrons pas traiter votre commande!');
define('ERROR_REVOCATION_NOT_ACCEPTED', '* Si vous n\'acceptez pas l\'expiration du droit de rétractation, nous ne pouvons malheureusement pas traiter votre commande!' );

define('SUB_TITLE_OT_DISCOUNT', 'Rabais:');

define('TAX_ADD_TAX', 'compris');
define('TAX_NO_TAX', 'en plus ');

define('NOT_ALLOWED_TO_SEE_PRICES', 'Vous ne pouvez pas consulter nos prix en tant que visiteur.');
define('NOT_ALLOWED_TO_SEE_PRICES_TEXT', 'Vous n\'avez pas la possiblité de consulter nos prix. Veuillez ouvrir un compte client.');

define('TEXT_DOWNLOAD', 'Télécharger');
define('TEXT_VIEW', 'Afficher');

define('TEXT_BUY', '1 x \'');
define('TEXT_NOW', '\' commander');
define('TEXT_GUEST', 'Invité');
define('TEXT_SEARCH_ENGINE_AGENT', 'moteur de recherche');

/*
 * 
 * ADVANCED SEARCH
 * 
 */

define('TEXT_ALL_CATEGORIES', 'Toutes catégories');
define('TEXT_ALL_MANUFACTURERS', 'Tous les producteurs');
define('JS_AT_LEAST_ONE_INPUT', '* Veuillez remplir au moins un champ:\nmots clefs\nprix à partir de\nprix jusqu\'à\n' );
define('AT_LEAST_ONE_INPUT', 'Veuillez remplir au moins un champ: <br />mots clefs avec au moins trois caractères<br  />prix à partir de<br  />prix jusqu à<br />');
define('TEXT_SEARCH_TERM', 'Votre recherche de: ');
define('JS_INVALID_FROM_DATE', '* date invalide (de)\n');
define('JS_INVALID_TO_DATE', '* date invalide (à)\n');
define('JS_TO_DATE_LESS_THAN_FROM_DATE', '* La date (de) doit être supérieur ou égale à la date (jusqu\'à)\n');
define('JS_PRICE_FROM_MUST_BE_NUM', '* \"Prix à partir de\" doit être un chiffre\n\n');
define('JS_PRICE_TO_MUST_BE_NUM', '* \"Prix jusqu à\" doit être un chiffre\n\n');
define('JS_PRICE_TO_LESS_THAN_PRICE_FROM', '* Le prix jusqu\'à doit être supérieur  ou égal au prix à partir de.\n');
define('JS_INVALID_KEYWORDS', '* Mot clef inadmissible\n');
define('TEXT_LOGIN_ERROR', '<b>Erreur:</b> Votre\'adresse mail\' et/ou le \'mot de passe\'ne correspondent pas.' );
//define('TEXT_NO_EMAIL_ADDRESS_FOUND', '<span class="color_error_message"><b>ACHTUNG:</b></span> Die eingegebene E-Mail-Adresse ist nicht registriert. Bitte versuchen Sie es noch einmal.'); // Not used anymore as we do not give a hint that an e-mail address is or is not in the database!
define('TEXT_PASSWORD_SENT', 'Un nouveau mot de passe vous a été envoyé par mail.');
define('TEXT_PRODUCT_NOT_FOUND', 'Article pas trouvé!');
define('TEXT_MORE_INFORMATION', 'Veuillez visiter,  <a  href="%s" onclick="window.open(this.href); return false;">la page d\'accueil</a> de cet article.');
define('TEXT_DATE_ADDED', 'Nous avons ajouté cet article dans notre catalogue le %s.');
define('TEXT_DATE_AVAILABLE', '<span class="color_error_message">Cet article sera de nouveau disponible le %s.</font>');
define('SUB_TITLE_SUB_TOTAL', 'Total intermédiaire:');

define('OUT_OF_STOCK_CANT_CHECKOUT', 'Les articles marqués de ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' ne sont malheureusement pas disponibles dans la quantité souhaitée.<br />Veuillez réduire votre quantité commandée pour les articles désignés. Merci');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Les articles marqués de ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' ne sont malheureusement pas disponibles dans la quantité souhaitée, .<br />La quantité commandée vous sera livré prochainement. Si vous le souhaitez, nous pouvons effectuer une livraison partielle.');

define('MINIMUM_ORDER_VALUE_NOT_REACHED_1', 'Vous n\'avez pas encore atteint la valeur minumum de commande, qui est de: ');
define('MINIMUM_ORDER_VALUE_NOT_REACHED_2', ' Veuillez commander au moins plus de: ');
define('MAXIMUM_ORDER_VALUE_REACHED_1', 'Vous avez dépassé la valeur maximum de commande, qui est de: ');
define('MAXIMUM_ORDER_VALUE_REACHED_2', '<br /> Veuillez réduire votre commande au au moins de:  ');

define('ERROR_INVALID_PRODUCT', 'L\'article sélectionné n\'a pas été trouvé!');

/*
 * 
 * NAVBAR TITLE
 * 
 */

define('NAVBAR_TITLE_ACCOUNT', 'Votre compte');
define('NAVBAR_TITLE_1_ACCOUNT_EDIT', 'Votre comte');
define('NAVBAR_TITLE_2_ACCOUNT_EDIT', 'Modifiez vos données personnelles');
define('NAVBAR_TITLE_1_ACCOUNT_HISTORY', 'Votre compte');
define('NAVBAR_TITLE_2_ACCOUNT_HISTORY', 'Vos commandes accomplies');
define('NAVBAR_TITLE_1_ACCOUNT_HISTORY_INFO', 'Votre compte');
define('NAVBAR_TITLE_2_ACCOUNT_HISTORY_INFO', 'Commande accomplie');
define('NAVBAR_TITLE_3_ACCOUNT_HISTORY_INFO', 'Numéro de commande %s');
define('NAVBAR_TITLE_1_ACCOUNT_PASSWORD', 'Votre compte');
define('NAVBAR_TITLE_2_ACCOUNT_PASSWORD', 'Modifiez votre mot de passe');
define('NAVBAR_TITLE_1_ADDRESS_BOOK', 'Votre compte');
define('NAVBAR_TITLE_2_ADDRESS_BOOK', 'Carnet d\'adresses');
define('NAVBAR_TITLE_1_ADDRESS_BOOK_PROCESS', 'Votre compte');
define('NAVBAR_TITLE_2_ADDRESS_BOOK_PROCESS', 'Carnet d\'adresses');
define('NAVBAR_TITLE_ADD_ENTRY_ADDRESS_BOOK_PROCESS', 'Nouvelle entrée');
define('NAVBAR_TITLE_MODIFY_ENTRY_ADDRESS_BOOK_PROCESS', 'Modifier entrée ');
define('NAVBAR_TITLE_DELETE_ENTRY_ADDRESS_BOOK_PROCESS', 'Supprimer entrée');
define('NAVBAR_TITLE_ADVANCED_SEARCH', 'Recherche avancée');
define('NAVBAR_TITLE1_ADVANCED_SEARCH', 'Recherche avancée');
define('NAVBAR_TITLE2_ADVANCED_SEARCH', 'Résultats de la recherche');
define('NAVBAR_TITLE_1_CHECKOUT_CONFIRMATION', 'Paiement');
define('NAVBAR_TITLE_2_CHECKOUT_CONFIRMATION', 'Confirmation');
define('NAVBAR_TITLE_1_CHECKOUT_PAYMENT', 'Paiement');
define('NAVBAR_TITLE_2_CHECKOUT_PAYMENT', 'Mode de paiement');
define('NAVBAR_TITLE_1_PAYMENT_ADDRESS', 'Paiement');
define('NAVBAR_TITLE_2_PAYMENT_ADDRESS', 'Modifiez votre adresse de facturation');
define('NAVBAR_TITLE_1_CHECKOUT_SHIPPING', 'Paiement');
define('NAVBAR_TITLE_2_CHECKOUT_SHIPPING', 'Informations sur l\'expédition');
define('NAVBAR_TITLE_1_CHECKOUT_SHIPPING_ADDRESS', 'Paiement');
define('NAVBAR_TITLE_2_CHECKOUT_SHIPPING_ADDRESS', 'Modifiez votre adresse de livraison');
define('NAVBAR_TITLE_1_CHECKOUT_SUCCESS', 'Paiement');
define('NAVBAR_TITLE_2_CHECKOUT_SUCCESS', 'Succès');
define('NAVBAR_TITLE_CREATE_ACCOUNT', 'Création du compte');
if (isset($navigation) && $navigation->snapshot['page'] == FILENAME_CHECKOUT_SHIPPING) {
define('NAVBAR_TITLE_LOGIN', 'Se connecter');
} else {
define('NAVBAR_TITLE_LOGIN', 'Se connecter');
}
define('NAVBAR_TITLE_LOGOFF', 'Au revoir');
define('NAVBAR_TITLE_PRODUCTS_NEW', 'Nouveau article');
define('NAVBAR_TITLE_SHOPPING_CART', 'Panier');
define('NAVBAR_TITLE_SPECIALS', 'Offres');
define('NAVBAR_TITLE_COOKIE_USAGE', 'Utilisation des cookies');
define('NAVBAR_TITLE_PRODUCT_REVIEWS', 'Évaluations');
define('NAVBAR_TITLE_REVIEWS_WRITE', 'Évaluations');
define('NAVBAR_TITLE_REVIEWS', 'Évaluations');
define('NAVBAR_TITLE_SSL_CHECK', 'Conseil de sécurité');
define('NAVBAR_TITLE_CREATE_GUEST_ACCOUNT', 'Création du compte');
define('NAVBAR_TITLE_PASSWORD_DOUBLE_OPT', 'Mot de passe oublié?');
define('NAVBAR_TITLE_NEWSLETTER', 'Newsletter');
define('NAVBAR_GV_REDEEM', 'Utiliser votre coupon');
define('NAVBAR_GV_SEND', 'Envoyer votre coupon');
define('NAVBAR_TITLE_DOWNLOAD', 'Téléchargements');

/*
 * 
 *  MISC
 * 
 */

define('TEXT_NEWSLETTER', 'Vous souhaitez toujours rester au courant? <br />Pas de problème. Inscrivez-vous à notre Newsletter pour rester informé sur nos nouveautés.');
define('TEXT_EMAIL_INPUT', 'Votre adresse email a été saisie dans notre système.<br />En même temps, un email vous a été envoyé avec un lien d\'activation. Veuillez cliquez là-dessus afin de confirmer votre adhésion. Sinon, vous ne recevrez pas de Newsletter.');

define('TEXT_WRONG_CODE', 'Votre code secret ne correspond pas au code affiché. Veuillez réessayer.');
define('TEXT_EMAIL_EXIST_NO_NEWSLETTER', 'Cette adresse email existe déjà dans notre base de données, mais elle n\'est pas encore activée!');
define('TEXT_EMAIL_EXIST_NEWSLETTER', 'Cette adresse email existe déjà dans notre base de données et elle est activée!');
define('TEXT_EMAIL_NOT_EXIST', 'Cette adresse email n\'existe pas dans notre base de données!');
define('TEXT_EMAIL_DEL', 'Votre adresse email a été supprimée de notre base de données regroupant les abonnés à la Newsletter');
define('TEXT_EMAIL_DEL_ERROR', 'Une erreur s\'est produite, votre adresse email n\' a pas été supprimée.');
define('TEXT_EMAIL_ACTIVE', 'Votre adresse email a été activé avec succès pour recevoir notre Newsletter!');
define('TEXT_EMAIL_ACTIVE_ERROR', 'Une erreur s\'est produite. Votre adresse email n\' a pas été activée!');
define('TEXT_EMAIL_SUBJECT', 'L\'inscription à la Newsletter');

define('TEXT_CUSTOMER_GUEST', 'Invité');

define('TEXT_LINK_MAIL_SENDED', 'Vous devez d\'abord confirmer votre demande d\'un nouveau mot de passe.<br />C\'est pour cette raison que vous avez reçu un mail avec un lien de confirmation. Veuillez cliquer sur ce lien afin d\'obtenir votre nouveau mot de passe. Sinon le nouveau mot de passe ne vous sera pas attribué.');
define('TEXT_PASSWORD_MAIL_SENDED', 'Un email avec votre noueau mot de passe vous a été envoyé.<br />Veuillez modifier votre mot de passe lors de votre prochaine connexion.');
define('TEXT_CODE_ERROR', 'Veuillez  saisir de nouveau votre adresse email et votre code secret. <br />Veuillez faire attention aux fautes de frappe!');
define('TEXT_EMAIL_ERROR', 'Veuillez  saisir de nouveau votre adresse email et votre code secret. <br />Veuillez faire attention aux fautes de frappe!');
define('TEXT_NO_ACCOUNT', 'Nous devons malheureusement vous signaler que votre demande d\'un nouveau mot de passe n\'a pas été valable ou est expiré. <br />Veuillez réessayer.');
define('HEADING_PASSWORD_FORGOTTEN', 'Mot de passe oublié?');
define('TEXT_PASSWORD_FORGOTTEN', 'Modifiez votre mot de passe en trois étapes simples.');
define('TEXT_EMAIL_PASSWORD_FORGOTTEN', 'Email de confirmation pour votre changement de mot de passe');
define('TEXT_EMAIL_PASSWORD_NEW_PASSWORD', 'Votre nouveau mot de passe');
define('ERROR_MAIL', 'Veuillez vérifier que vous avez saisi correctement toutes les informations.');

define('CATEGORIE_NOT_FOUND', 'La catégorie n\'a pas été trouvée');

define('GV_FAQ', 'coupon FAQ');
define('ERROR_NO_REDEEM_CODE', 'Vous n\'avez pas saisi de code.');
define('ERROR_NO_INVALID_REDEEM_GV', 'Mauvais code pour le coupon');
define('TABLE_HEADING_CREDIT', 'Crédit');
define('EMAIL_GV_TEXT_SUBJECT', 'Un cadeau de %s ');
define('MAIN_MESSAGE', 'Vous vous êtes décidé d\'envoyer un coupon d\'une valeur de %s à %s dont l\'adresse email est %s.<br /><br />Le texte suivant apparait dans votre email:<br /><br />Bonjour %s,<br /><br />%s vous a envoyé un coupon d\'une valeur de.');
define('REDEEMED_AMOUNT', 'Votre crédit a été comptabilisé avec succè sur votre compte. Valeur du crédit: %s');
define('REDEEMED_COUPON', 'Votre coupon a été comptabilisé avec succès et vous sera automatiquement déduit lors de votre prochain achat.');

define('ERROR_INVALID_USES_USER_COUPON', 'Vous pouvez utiliser votre coupon seulement');
define('ERROR_INVALID_USES_COUPON', 'Les clients peuvent utiliser le coupon seulement');
define('TIMES', ' fois.');
define('ERROR_INVALID_STARTDATE_COUPON', 'Votre coupon n\'est pas encore disponible.');
define('ERROR_INVALID_FINISDATE_COUPON', 'Votre coupon est déjà expiré.');
define('ERROR_INVALID_MINIMUM_ORDER_COUPON', 'Cette coupon ne peut que être utilisé à partir d\'une valeur de commande minimale de %s!');
define('ERROR_INVALID_MINIMUM_ORDER_COUPON_ADD', '<br/>Une fois que la valeur de commande minimale est atteinte, vous devez entrer de nouveau le code du coupon!');
define('PERSONAL_MESSAGE', '%s écrit:');

/*
 * 
 *  COUPON POPUP
 * 
 */
 
define('TEXT_CLOSE_WINDOW', 'Fermer la fenêtre [x]');
define('TEXT_COUPON_HELP_HEADER', 'Votre crédit a été comptabilisé avec succès.');
define('TEXT_COUPON_HELP_NAME', '<br /><br />Désignation de votre coupon: %s');
define('TEXT_COUPON_HELP_FIXED', '<br /><br />Votre coupon est d\'une valeur de: %s ');
define('TEXT_COUPON_HELP_MINORDER', '<br /><br />La valeur de commande minimale est de %s ');
define('TEXT_COUPON_HELP_FREESHIP', '<br /><br /> Coupon pour une livraison gratuite');
define('TEXT_COUPON_HELP_DESC', '<br /><br />Désignation du coupon: %s');
define('TEXT_COUPON_HELP_DATE', '<br /><br />Ce coupon est valable du %s au %s');
define('TEXT_COUPON_HELP_RESTRICT', '<br /><br />article /catégorie: limites');
define('TEXT_COUPON_HELP_CATEGORIES', 'Catégorie');
define('TEXT_COUPON_HELP_PRODUCTS', 'Article');
define('ERROR_ENTRY_AMOUNT_CHECK', 'Valeur de coupon invalide');
define('ERROR_ENTRY_EMAIL_ADDRESS_CHECK', 'Adresse email invalide');

// VAT Reg No
define('ENTRY_VAT_TEXT', 'Uniquemenent pour l\'Allemagne et l\'UE!');
//BOC new error message, noRiddle
//define('ENTRY_VAT_ERROR', 'Die eingegebene USt-IdNr. ist ung&uuml;ltig oder kann derzeit nicht &uuml;berpr&uuml;ft werden! Bitte geben Sie eine g&uuml;ltige ID ein oder lassen Sie das Feld zun&auml;chst leer.');
define('ENTRY_VAT_ERROR', 'Votre numéro d\'identification n\'est pas valable ou ne peut pas être vérifié pour le moment. <br /> Veuillez saisir <a href="https://www.service-public.fr/professionnels-entreprises/vosdroits/F23570" target="_blank"> un numéro d\'identification valable</a> ou laisser le champ libre pour l\'instant.');
//EOC new error message, noRiddle
define('MSRP', 'Prix escompté par le client');
define('YOUR_PRICE', 'Votre prix ');
define('UNIT_PRICE', 'prix à l\'unité');
define('ONLY', 'Maintenant seulement ');
define('FROM', ' à partir de ');
define('YOU_SAVE', 'Vous économisez ');
define('INSTEAD', 'Prix conseillé');
define('TXT_PER', ' par ');
define('TAX_INFO_INCL', 'TVA %s inclus');
define('TAX_INFO_EXCL', 'TVA %s non inclus');
define('TAX_INFO_ADD', 'TVA %s en plus');
define('SHIPPING_EXCL', 'en plus');
define('SHIPPING_INCL', 'incl.');
define('SHIPPING_COSTS', 'Frais de transport');

define('SHIPPING_TIME', 'temps de livraison: ');
define('MORE_INFO', '[Plus]');

define('ENTRY_PRIVACY_ERROR', 'Veuillez accepter notre politique de confidentialité!');
define('TEXT_PAYMENT_FEE', 'Coûts de paiement');

define('_MODULE_INVALID_SHIPPING_ZONE', 'L\'expédition dans cet pays n\'est pas possible');
define('_MODULE_UNDEFINED_SHIPPING_RATE', 'Les coûts d\'expédition ne peuvent pas être calculé pour l\'instant');

define('NAVBAR_TITLE_1_ACCOUNT_DELETE', 'Votre compte');
define('NAVBAR_TITLE_2_ACCOUNT_DELETE', 'Supprimer le compte');
	
//contact-form error messages
//BOC commented out double defines, others in contact_us.php, noRiddle
//define('ERROR_EMAIL','<p><b>Ihre E-Mail-Adresse:</b> Keine oder ung&uuml;ltige Eingabe!</p>');
//define('ERROR_VVCODE','<p><b>Sicherheitscode:</b> Keine &Uuml;bereinstimmung, bitte geben Sie den Sicherheitscode erneut ein!</p>');
//define('ERROR_MSG_BODY','<p><b>Ihre Nachricht:</b> Keine Eingabe!</p>');
//EOC commented out double defines, others in contact_us.php, noRiddle

//Table Header checkout_confirmation.php
define('HEADER_QTY', 'Quantité');
define('HEADER_ARTICLE', 'Article');
define('HEADER_SINGLE', 'Prix par unité');
define('HEADER_TOTAL', 'Total');
define('HEADER_MODEL', 'Numéro d\'article');

### PayPal API Modul
define('NAVBAR_TITLE_PAYPAL_CHECKOUT', 'Quitter PayPal');
define('PAYPAL_ERROR', 'Annuler PayPal');
define('PAYPAL_NOT_AVIABLE', 'PayPal n\'est malheureusement pas disponible pour l\'instant.<br />Veuillez choisir un autre mode de paiement<br /> ou réessayer plus tard.<br />' );
define('PAYPAL_FEHLER', 'PayPal a signalé une erreur lors du processus de paiement. <br /> Votre commande est enregistrée, mais ne sera pas traitée. <br /> Merci pour votre compréhension. <br />');
define('PAYPAL_WARTEN', 'PayPal a signalé une erreur lors du processus de paiement. <br /> Vous devez retourner sur la page PayPal pour payer. <br /> Merci pour votre compréhension.<br /> Veuillez cliquer de nouveau sur le bouton PayPal. <br />');
define('PAYPAL_NEUBUTTON', 'Veuillez cliquer de nouveau pour payer la commande. <br /> Toute autre touche va annuler la commande.');
define('ERROR_ADDRESS_NOT_ACCEPTED', '* Pendant que vous n\'acceptiez pas votre adresse de facturation et d\'expédition, \nnous ne pouvons malheureusement pas traiter votre commande!\n\n');
define('PAYPAL_GS', 'Coupon');
define('PAYPAL_TAX', 'TVA');
define('PAYPAL_EXP_WARN', 'Attention! Des frais éventuels d\'expédition ne seront que facturés plus tard.');
define('PAYPAL_EXP_VORL', 'Coûts d\'expédition préliminaires');
define('PAYPAL_EXP_VERS', '6.90');
define('PAYPAL_ADRESSE', 'Le pays indiqué dans votre adresse d\'expédition n\'est pas enregistré dans notre boutique. <br /> Veuillez nous contacter. <br />Merci pour votre compréhension.<br />Pays reçu par PayPal: ');
define('PAYPAL_AMMOUNT_NULL', 'La montant total de la commande (sans expédition) est égale à 0. <br />C\'est pourquoi Paypal n\'est pas disponible.<br />Veuillez choisir un autre mode de paiement.<br /> Merci pour votre compréhension.<br />');
### PayPal API Modul

define('BASICPRICE_VPE_TEXT', 'Pour cette quantitié seulement ');
define('GRADUATED_PRICE_MAX_VALUE', 'à partir de');
define('_SHIPPING_TO', 'Expédition à ');

define('ERROR_SQL_DB_QUERY', 'Nous sommes désolés. Une erreur dans la base de données s\'est produite.');
define('ERROR_SQL_DB_QUERY_REDIRECT', 'Vous serez transféré sur notre page d\'accueil dans %s secondes!');

define('TEXT_AGB_CHECKOUT', 'Veuillez noter nos conditions générales et informations de client %s, ainsi que nos informations sur le droit de rétraction %s.');
define('DOWNLOAD_NOT_ALLOWED', '<h1>Interdit</h1>Le serveur ne pouvait pas vérifier votre autorisation d\'accès au document. Soit vous n\'avez pas saisi les bon données de connexion, soit votre navigateur n\'a pas saisit correctement les données.');

define('TEXT_INFO_DETAILS', ' Détails');
define('TEXT_SAVED_BASKET', 'Veuillez vérifier votre panier. Il contient des article d\'une visite précédente.');
//define('TEXT_PRODUCTS_QTY_REDUCED', 'Die maximal erlaubte St&uuml;ckzahl f&uuml;r den zuletzt hinzugef&uuml;gten bzw. ge&auml;nderten Artikel wurde &uuml;berschritten. Die St&uuml;ckzahl wurde automatisch auf die maximal erlaubte St&uuml;ckzahl reduziert.'); // Now we use MAX_PROD_QTY_EXCEEDED

define('ERROR_REVIEW_TEXT', 'Le texte d\'évaluation doit se composer d\'au moins ' . REVIEW_TEXT_MIN_LENGTH . ' caractères.');
define('ERROR_REVIEW_RATING', 'Faites-nous part de votre évaluation.');
define('ERROR_REVIEW_AUTHOR', 'Veuillez indiquer votre nom.');

define('GV_NO_PAYMENT_INFO', '<div class="infomessage">Vous pouvez payer la commande totale avec votre coupon. Si vous ne voulez pas payer avec votre crédit, désactivez la sélection coupon et choisissez un autre mode de paiement!</div>');
define('GV_ADD_PAYMENT_INFO', '<div class="errormessage">Votre crédit ne suffit pas ou ne peut pas être utilisé complètement afin de payer toute la commande. Veuillez choisir un autre mode de paiement!</div>');

define('_SHIPPING_FREE', 'Livraison gratuite');

define('TEXT_CONTENT_NOT_FOUND', 'La page n\'a pas été trouvée!');
define('TEXT_SITE_NOT_FOUND', 'La page n\'a pas été trouvée!');

// error message for exceeded product quantity, noRiddle
define('MAX_PROD_QTY_EXCEEDED', 'La quantité maximale de ' .MAX_PRODUCTS_QTY. ' pour <span style="font-style:italic;">"%s"</span> est dépassée.<br /> La quantité a été réduite automatiquement à la quantité autorisée.');

define('IMAGE_BUTTON_CONTENT_EDIT', 'Modifer le contenu');
define('PRINTVIEW_INFO', 'Imprimer la fiche d\'article');
define('PRODUCTS_REVIEW_LINK', 'Écrire évaluation');

define('TAX_INFO_SMALL_BUSINESS', 'Valeur final en vertu du § 19 UStG.');
define('TAX_INFO_SMALL_BUSINESS_FOOTER', 'Nous ne prélevons pas de taxe sur le chiffre d\'affaires à cause du statut de petite entreprise en vertu du § 19 UStg.');

define('NEED_CHANGE_PWD', 'Veuillez modifier votre mot de passe.');
define('TEXT_REQUEST_NOT_VALID', 'Le lien est expiré. Veuillez demander un nouveau mot de passe.');

define('NAVBAR_TITLE_WISHLIST', 'Liste d\'envies');
define('TEXT_TO_WISHLIST', 'Sur la liste d\'envies ');
define('IMAGE_BUTTON_TO_WISHLIST', 'Sur la liste d\'envies ');

define('GUEST_REDEEM_NOT_ALLOWED', 'Des invitées ne peuvent pas utiliser des coupons.');
define('GUEST_VOUCHER_NOT_ALLOWED', 'Des invités ne peuvent pas être achté en tant qu\'invité.');

define('TEXT_FILTER_SETTING_DEFAULT', 'Article par site');
define('TEXT_FILTER_SETTING', '%s articles par site');
define('TEXT_FILTER_SETTING_ALL', 'Afficher toutes les articles');
define('TEXT_SHOW_ALL', ' (afficher tout)');
define('TEXT_FILTER_SORTING_DEFAULT', 'Trier par...');
define('TEXT_FILTER_SORTING_ABC_ASC', 'A à Z');
define('TEXT_FILTER_SORTING_ABC_DESC', 'Z à A');
define('TEXT_FILTER_SORTING_PRICE_ASC', 'Prix croissant');
define('TEXT_FILTER_SORTING_PRICE_DESC', 'Prix décroissant');
define('TEXT_FILTER_SORTING_DATE_DESC', 'D\'abord nouveaux produits');
define('TEXT_FILTER_SORTING_DATE_ASC', 'D\'abord anciens produits');
define('TEXT_FILTER_SORTING_ORDER_DESC', 'Le plus vendu');

define('NAVBAR_TITLE_ACCOUNT_CHECKOUT_EXPRESS_EDIT', 'Paramètres d\'achat rapide');
define('SUCCESS_CHECKOUT_EXPRESS_UPDATED', 'Les paramètres d\'achat rapide sont enregistrés.');
define('TEXT_ERROR_CHECKOUT_EXPRESS_SHIPPING_ADDRESS', 'Veuillez choisir une adresse d\'expédition');
define('TEXT_ERROR_CHECKOUT_EXPRESS_SHIPPING_MODULE', 'Veuillez choisir un mode d\'expédition');
define('TEXT_ERROR_CHECKOUT_EXPRESS_PAYMENT_ADDRESS', 'Veuillez choisir une adresse de facturaiton');
define('TEXT_ERROR_CHECKOUT_EXPRESS_PAYMENT_MODULE', 'Veuillez choisir un mode de paiement');
define('TEXT_CHECKOUT_EXPRESS_INFO_LINK', 'Mon achat rapide');
define('TEXT_CHECKOUT_EXPRESS_INFO_LINK_MORE', 'Plus d\'information sur mon achat rapide »');
define('TEXT_CHECKOUT_EXPRESS_CHECK_CHEAPEST', 'Choisir toujours l\'expédition au meilleur prix');

define('AC_SHOW_PAGE', 'Page ');
define('AC_SHOW_PAGE_OF', ' de ');

define('FREE_SHIPPING_INFO', 'L\'expédition sera gratuite à partir d\'une valeur minimum de commande de %s');

define('MANUFACTURER_NOT_FOUND', 'Fabricant ne pas trouvé');
define('ENTRY_TOKEN_ERROR', 'Veuillez vérifier votre saisie.');

define('IMAGE_BUTTON_CONFIRM', 'Confirmer');

// ***************************************************
//  Kontodaten-PrŸfung
// ***************************************************
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_0', 'Coordonnées bancaires fonctionnent.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1', 'Numéro de compte et/ ou code bancaire sont invalides ou ne concordent pas!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2', 'Le numéro de compte n\'est pas vérifiable automatiquement.' );
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_3', 'Le numéro de compte n\'est pas vérifiable.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_4', 'Le numéro de compte n\'est pas vérifiable! Veuillez vérifier vos coordonnées encore une fois.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_5', 'Ce code bancaire n\'existe pas. Veuillez corriger votre saisie.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_8', 'Faute de code bancaire ou pas de code bancaire saisi!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_9', 'Pas de numéro de compte saisi!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_10', 'Vous n\'avez pas saisi de propriétaire du compte.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_128', 'Il est survenu un erreur d\'internet en vérifiant les coordonnées bancaires.');

// Fehlermeldungen alle IBAN-Nummern 
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1000', 'Code pays (1ère et 2ème chiffre de l\'IBAN) inconnu.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1010', 'Longueur du numéro IBAN incorrecte: Trop de signes saisis.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1020', 'Longueur du numéro IBAN incorrecte: Pas assez de signes saisis.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1030', 'Le numéro IBAN ne correspond pas auy format du pays.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1040', 'Chiffres de contrôle du numéro IBAN (position 3 et 4) ne sont pas corrects -> Faute de frappe.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1050', 'Le numéro BIC est invalide.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1060', 'Longueur du numéro BIC est incorrecte: Trop de signes saisis. 8 ou 11 signes sont nécessaires.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1070', 'Longueur du numéro BIC est incorrecte: Pas assez de signes saisis. 8 ou 11 signes sont nécessaires.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1080', 'Longueur du numéro BIC est invalide: 8 ou 11 signes sont nécessaires.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_1200', 'Nous ne pouvons malheureusement pas accepter le numéro IBAN du pays indiqué (position 1 et 2).');

// Fehlermeldungen fŸr deutsche Kontonummern 
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2001', 'Le numéro de compte (positions 13 à 22) et/ou le code bancaire  (positions 5 á 12)inclus dans le numéro IBAN sont invalides ou ne correspondent pas.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2002', 'Le numéro de compte inclus dans le numéro IBAN (positions 13 à 22) ne\'est pas vérifiable automatiquement.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2003', 'Le numéro de compte inclus dans le numéro IBAN (positions 13 à 22) n\'as pas de chiffres de contrôle.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2004', 'Le numéro de compte inclus dans le numéro IBAN (posiitons 13 à 22) n\'est pas vérifiable!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2005', 'Le code bancaire (positions 5 à 12 du numéro IBAN) n\'existe pas!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2008', 'Erreur de code bancaire (positions 5 à 12 du numéro IBAN) ou pas de code bancaire saisi!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2009', 'Pas de numéro de compte (positions 13 à 22 du numéro IBAN) saisi!');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2010', 'Pas de propriétaire du compte indiqué.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2020', 'BIC invalide: Pas de banque connu avec ce numéro BIC.');
define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_2128', 'Erreur d\'internet dans la vérification des coordonnées bancaires.');

define('BANKACCOUNT_CHECK_TEXT_BANK_ERROR_UNKNOWN', 'Erreur inconnu dans la vérification des coordonnées bancaires.');

define('PRODUCT_REVIEWS_SUCCESS', 'Merci beaucoup pour votre évaluation.');
define('PRODUCT_REVIEWS_SUCCESS_WAITING', 'Merci beacoup pour votre évaluation. Elle sera vérifé avant d\'être publiée.');
?>

<?php
/**************************************************************
* file: secure_admin_popup.php
* path: /includes/extra/application_top/application_top_end/
* use: see comment below
*
* � copyright, 04-2021, noRiddle
**************************************************************/

//BOC secure admin popup system module, 04-2021, noRiddle
if(defined('MODULE_NR_STAFF_POPUP_INFOS_STATUS') && MODULE_NR_STAFF_POPUP_INFOS_STATUS == 'true' && MODULE_NR_STAFF_POPUP_INFOS_COID != '') {
    if(basename($PHP_SELF) == 'shop_content.php' && (isset($_GET['coID']) && (int)$_GET['coID'] == (int)MODULE_NR_STAFF_POPUP_INFOS_COID)) {
        if($_SESSION['customers_status']['customers_status_id'] != '0') {
            $_GET['coID'] = 'X';
        }
    }
}
//EOC secure admin popup system module, 04-2021, noRiddle
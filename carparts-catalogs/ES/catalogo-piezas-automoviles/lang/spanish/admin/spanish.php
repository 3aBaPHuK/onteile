<?php
/* --------------------------------------------------------------
   $Id: spanish.php 10896 2017-08-11 11:31:54Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(spanish.php,v 1.99 2003/05/28); www.oscommerce.com
   (c) 2003 nextcommerce (spanish.php,v 1.24 2003/08/24); www.nextcommerce.org
   (c) 2006 XT-Commerce (spanish.php)

   Released under the GNU General Public License
   --------------------------------------------------------------
   Third Party contributions:
   Customers Status v3.x (c) 2002-2003 Copyright Elari elari@free.fr | www.unlockgsm.com/dload-osc/ | CVS : http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/elari/?sortby=date#dirlist

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

// look in your $PATH_LOCALE/locale directory for available locales..
// on RedHat6.0 I used 'es_ES'
// on FreeBSD 4.0 I use 'es_ES.ISO_8859-1'
// this may not work under win32 environments..

@setlocale(LC_TIME, 'es_ES.UTF-8' ,'es_ES@euro', 'es_ES', 'es-ES', 'es', 'sp', 'es_ES.ISO_8859-1', 'Spanish','es_ES.ISO_8859-15');
define('DATE_FORMAT_SHORT', '%d.%m.%Y');
define('DATE_FORMAT_LONG', '%A, %d. %B %Y');
define('DATE_FORMAT', 'd.m.Y');
define('PHP_DATE_TIME_FORMAT', 'd.m.Y H:i:s');
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');

// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
function xtc_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 0, 2) . substr($date, 3, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 3, 2) . substr($date, 0, 2);
  }
}

require_once(DIR_FS_INC.'auto_include.inc.php');
foreach(auto_include(DIR_FS_LANGUAGES.'spanish/extra/admin/','php') as $file) require ($file);

// Global entries for the <html> tag
define('HTML_PARAMS', 'dir="ltr" lang="es"');

// page title
define('TITLE', defined('PROJECT_VERSION') ? PROJECT_VERSION : 'undefined');

// header text in includes/header.php
define('HEADER_TITLE_TOP', 'Administración');
define('HEADER_TITLE_SUPPORT_SITE', 'Página de apoyo');
define('HEADER_TITLE_ONLINE_CATALOG', 'Catálogo en línea');
define('HEADER_TITLE_ADMINISTRATION', 'Administración');

// text for gender
define('MALE', 'Señor');
define('FEMALE', 'Señora');

// text for date of birth example
define('DOB_FORMAT_STRING', 'dd.mm.aaaa');

// configuration box text in includes/boxes/configuration.php

define('BOX_HEADING_CONFIGURATION', 'Configuración');
define('BOX_HEADING_MODULES', 'Módulo');
define('BOX_HEADING_PARTNER_MODULES', 'Módulo de afiliado');
define('BOX_HEADING_ZONE', 'País / Impuesto');
define('BOX_HEADING_CUSTOMERS', 'Clientes');
define('BOX_HEADING_PRODUCTS', 'Catálogo');
define('BOX_HEADING_STATISTICS', 'Estadísticas');
define('BOX_HEADING_TOOLS', 'Programas de ayuda');
define('BOX_HEADING_LOCALIZATION', 'Idiomas / Monedas');
define('BOX_HEADING_TEMPLATES', 'Templates');
define('BOX_HEADING_LOCATION_AND_TAXES', 'País / Impuesto');
define('BOX_HEADING_CATALOG', 'Catálogo');
define('BOX_MODULE_NEWSLETTER', 'Newsletter');

define('BOX_CONTENT', 'Manager de contenido');
define('TEXT_ALLOWED', 'Permiso');
define('TEXT_ACCESS', 'Área de acceso');
define('BOX_CONFIGURATION', 'Configuraciones básicas');
define('BOX_CONFIGURATION_1', 'Mi tienda');
define('BOX_CONFIGURATION_2', 'Valores Mínimos');
define('BOX_CONFIGURATION_3', 'Valores Máximos');
define('BOX_CONFIGURATION_4', 'Opciones de imagen');
define('BOX_CONFIGURATION_5', 'Detalles del cliente');
define('BOX_CONFIGURATION_6', 'Opciones del módulo');
define('BOX_CONFIGURATION_7', 'Opciones de envío');
define('BOX_CONFIGURATION_8', 'Opciones de la lista de artículos');
define('BOX_CONFIGURATION_9', 'Opciones de gestión de almacenes');
define('BOX_CONFIGURATION_10', 'Opciones de Logging');
define('BOX_CONFIGURATION_11', 'Opciones de caché');
define('BOX_CONFIGURATION_12', 'Opciones de correo electrónico');
define('BOX_CONFIGURATION_13', 'Opciones de descarga');
define('BOX_CONFIGURATION_14', 'Compresión');
define('BOX_CONFIGURATION_15', 'Sesiones');
define('BOX_CONFIGURATION_16', 'Meta Tags/Motores de búsqueda');
define('BOX_CONFIGURATION_17', 'Módulos adicionales');
define('BOX_CONFIGURATION_18', 'ID del IVA');
define('BOX_CONFIGURATION_19', 'Afiliado');
define('BOX_CONFIGURATION_22', 'Opciones de búsqueda');
define('BOX_CONFIGURATION_24', 'Google, Piwik & Facebook');
define('BOX_CONFIGURATION_25', 'Captcha');
define('BOX_CONFIGURATION_31', 'Skrill');
define('BOX_CONFIGURATION_40', 'Opciones de la ventana popup');
define('BOX_CONFIGURATION_1000', 'Opciones del área de admin');

define('BOX_MODULES', 'Módulos de pago, envío y liquidación');
define('BOX_PAYMENT', 'Opciones de pago');
define('BOX_SHIPPING', 'Modo de envío');
define('BOX_ORDER_TOTAL', 'Resumen');
define('BOX_CATEGORIES', 'Categorías / Artículos');
define('BOX_PRODUCTS_ATTRIBUTES', 'Características del artículo');
define('BOX_MANUFACTURERS', 'Fabricante');
define('BOX_REVIEWS', 'Reseñas de productos');
define('BOX_CAMPAIGNS', 'Campañas');
define('BOX_XSELL_PRODUCTS', 'Cross Marketing');
define('BOX_SPECIALS', 'Ofertas Especiales');
define('BOX_PRODUCTS_EXPECTED', 'Artículos esperados');
define('BOX_CUSTOMERS', 'Clientes');
define('BOX_ACCOUNTING', 'Gestión de derechos de Admin');
define('BOX_CUSTOMERS_STATUS', 'Grupos de clientes');
define('BOX_ORDERS', 'Pedidos');
define('BOX_COUNTRIES', 'País');
define('BOX_ZONES', 'Estados federales');
define('BOX_GEO_ZONES', 'Zonas fiscales');
define('BOX_TAX_CLASSES', 'Clases de impuestos');
define('BOX_TAX_RATES', 'Tasas fiscales');
define('BOX_HEADING_REPORTS', 'Reportes');
define('BOX_PRODUCTS_VIEWED', 'Artículos visitados');
define('BOX_STOCK_WARNING', 'Informe de existencias');
define('BOX_PRODUCTS_PURCHASED', 'Artículos vendidos');
define('BOX_STATS_CUSTOMERS', 'Estadísticas de pedidos de clientes');
define('BOX_BACKUP', 'Manager de la base de datos');
define('BOX_BANNER_MANAGER', 'Banner Manager');
define('BOX_CACHE', 'Control de caché');
define('BOX_DEFINE_LANGUAGE', 'Definir idiomas');
define('BOX_FILE_MANAGER', 'Manager de archivos');
define('BOX_MAIL', 'Enviar correo electrónico');
define('BOX_NEWSLETTERS', 'Manager del Newsletter');
define('BOX_SERVER_INFO', 'Información del servidor');
define('BOX_BLZ_UPDATE', 'Actualizar códigos bancarios');
define('BOX_WHOS_ONLINE', 'Quién está en línea');
define('BOX_TPL_BOXES', 'Orden de casillas');
define('BOX_CURRENCIES', 'Moendas');
define('BOX_LANGUAGES', 'Idiomas');
define('BOX_ORDERS_STATUS', 'Estado de pedido');
define('BOX_ATTRIBUTES_MANAGER', 'Gestión de atributos');
define('BOX_SHIPPING_STATUS', 'Estado de entrega');
define('BOX_SALES_REPORT', 'Estadísticas de ventas');
define('BOX_MODULE_EXPORT', 'Módulo de Exportación');
define('BOX_MODULE_SYSTEM', 'Módulos del sistema');
define('BOX_HEADING_GV_ADMIN', 'Bonos/Cupones');
define('BOX_GV_ADMIN_QUEUE', 'Cupón cola de espera');
define('BOX_GV_ADMIN_MAIL', 'Cupón correo electrónico');
define('BOX_GV_ADMIN_SENT', 'Cupones enviados');
define('BOX_COUPON_ADMIN', 'Cupón Admin');
define('BOX_TOOLS_BLACKLIST', 'lista negra de tarjetas de crédito');
define('BOX_IMPORT', 'Importación/exportación');
define('BOX_PRODUCTS_VPE', 'Unidad de embalaje');
define('BOX_CAMPAIGNS_REPORT', 'Informe de Campañ');
define('BOX_ORDERS_XSELL_GROUP', 'Grupos de marketing cruzado');
define('BOX_REMOVEOLDPICS', 'Borrar imágenes antiguas');
define('BOX_JANOLAW', 'janolaw AGB Hosting');
define('BOX_HAENDLERBUND', 'Servicio de Condiciones Generales de la Asociación Alemana de Comerciantes');
define('BOX_SAFETERMS', 'Servicio de Condiciones Generales - Safeterms');
define('BOX_SHOP', 'Tienda');
define('BOX_LOGOUT', 'Cerrar sesión');
define('BOX_CREDITS', 'Créditos');
define('BOX_UPDATE', 'Verificación de versión');
define('BOX_EASYMARKETING', 'EASYMARKETING AG');
define('BOX_GV_CUSTOMERS', 'Saldo del cliente');
define('BOX_IT_RECHT_KANZLEI', 'Despacho de Abogados IT');
define('BOX_PROTECTEDSHOPS', 'Servicio de Condiciones Generales - Protected Shops');
define('BOX_CLEVERREACH', 'CleverReach');
define('BOX_SUPERMAILER', 'SuperMailer');
define('BOX_OFFLINE', 'Tienda offline');
define('BOX_LOGS', 'Logfiles');
define('BOX_SHIPCLOUD', 'shipcloud');
define('BOX_SHIPCLOUD_PICKUP', 'shipcloud - recogida');
define('BOX_PRODUCTS_TAGS', 'Características del artículo');
define('BOX_TRUSTEDSHOPS', 'Trusted Shops');

define('TXT_GROUPS', '<b>Grupos</b>:');
define('TXT_SYSTEM', 'Sistema');
define('TXT_CUSTOMERS', 'Clientes/Pedidos');
define('TXT_PRODUCTS', 'Artículos/Categorías');
define('TXT_STATISTICS', 'Herramientas estadísticas');
define('TXT_TOOLS', 'Programas adicionales');
define('TEXT_ACCOUNTING', 'Configuraciones de acceso para:');

/******* SHOPGATE **********/
if (is_file(DIR_FS_CATALOG.'includes/external/shopgate/base/lang/spanish/admin/spanish.php')) {
  include_once (DIR_FS_CATALOG.'includes/external/shopgate/base/lang/spanish/admin/spanish.php');
}
/******* SHOPGATE **********/

// javascript messages
define('JS_ERROR', '¡Se han producido errores durante la entrada!\nPor favor, corrija lo siguiente:\n\n');

define('JS_OPTIONS_VALUE_PRICE', '* Debe asignar un precio a este valor.\n');
define('JS_OPTIONS_VALUE_PRICE_PREFIX', '* Debe introducir un signo para el precio (+/-)\n');

define('JS_PRODUCTS_NAME', '* El nuevo artículo debe tener un nombre\n');
define('JS_PRODUCTS_DESCRIPTION', '* El nuevo artículo debe tener una descripción\n');
define('JS_PRODUCTS_PRICE', '* El nuevo artículo debe tener un precio\n');
define('JS_PRODUCTS_WEIGHT', '* El nuevo artículo debe tener una especificación de peso\n');
define('JS_PRODUCTS_QUANTITY', '* Debe asignar una cantidad disponibleal nuevo artículo\n');
define('JS_PRODUCTS_MODEL', '* Debe asignar un número de artículo al nuevo artículo.\n');
define('JS_PRODUCTS_IMAGE', '* Debe asignar una imagen al artículo.\n');

define('JS_SPECIALS_PRODUCTS_PRICE', '* Se debe fijar un nuevo precio para este artículo.\n');

define('JS_GENDER', '* El \'saludo\' debe ser seleccionado.\n');
define('JS_FIRST_NAME', '* El \'nombre\' debe contener al menos  ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caracteres.\n');
define('JS_LAST_NAME', '* El \'apellido\' debe contener al menos ' . ENTRY_LAST_NAME_MIN_LENGTH . ' caracteres.\n');
define('JS_DOB', '* La \'fecha de nacimiento\' debe tener el siguiente formato: xx.xx.xxxx (Día/Mes/Año).\n');
define('JS_EMAIL_ADDRESS', '* La \'dirección de correo electrónico\' debe contener al menos ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' caracteres.\n');
define('JS_ADDRESS', '* La \'calle\' debe contener al menos ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caracteres.\n');
define('JS_POST_CODE', '* El \'código postal\' debe contener al menos ' . ENTRY_POSTCODE_MIN_LENGTH . ' caracteres.\n');
define('JS_CITY', '* La \'ciudad\' debe contener al menos ' . ENTRY_CITY_MIN_LENGTH . ' caracteres.\n');
define('JS_STATE', '* El \'estado federal\' debe ser seleccionado.\n');
define('JS_STATE_SELECT', '-- Seleccione arriba --');
define('JS_ZONE', '* EL \'estado federal\' debe ser seleccionado de la lista para este país.');
define('JS_COUNTRY', '* El \'país\' debe ser seleccionado.\n');
define('JS_TELEPHONE', '* El \'Número de teléfono\' debe contener al menos ' . ENTRY_TELEPHONE_MIN_LENGTH . ' caracteres.\n');
define('JS_PASSWORD', '* La \'contraseña\' como también la \'confirmación de contraseña\' deben cooincidir y contener al menos ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.\n');

define('JS_ORDER_DOES_NOT_EXIST', '¡Número de orden %s no existe!');

define('CATEGORY_PERSONAL', 'Datos personales');
define('CATEGORY_ADDRESS', 'Dirección');
define('CATEGORY_CONTACT', 'Contacto');
define('CATEGORY_COMPANY', 'Empresa');
define('CATEGORY_OPTIONS', 'opciones adicionales');

define('ENTRY_GENDER', 'Saludo:');
define('ENTRY_GENDER_ERROR', ' <span class="errorText">entrada obligatoria</span>');
define('ENTRY_FIRST_NAME', 'Nombre:');
define('ENTRY_FIRST_NAME_ERROR', ' <span class="errorText">al menos ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' letras</span>');
define('ENTRY_LAST_NAME', 'Apellido:');
define('ENTRY_LAST_NAME_ERROR', ' <span class="errorText">al menos ' . ENTRY_LAST_NAME_MIN_LENGTH . ' letras</span>');
define('ENTRY_DATE_OF_BIRTH', 'Fecha de nacimiento');
define('ENTRY_DATE_OF_BIRTH_ERROR', ' <span class="errorText">(p.ej. 21.05.1970)</span>');
define('ENTRY_EMAIL_ADDRESS', 'Dirección de correo electrónico:');
define('ENTRY_EMAIL_ADDRESS_ERROR', ' <span class="errorText">al menos ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' letras</span>');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', ' <span class="errorText">¡Dirección de correo electrónico inválida!</span>');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', ' <span class="errorText">¡ Esta dirección de correo electrónico ya existe!</span>');
define('ENTRY_COMPANY', 'Nombre de empresa:');
define('ENTRY_STREET_ADDRESS', 'Calle:');
define('ENTRY_STREET_ADDRESS_ERROR', ' <span class="errorText">al menos ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' letras</span>');
define('ENTRY_SUBURB', 'Dirección adicional:');
define('ENTRY_POST_CODE', 'Código postal:');
define('ENTRY_POST_CODE_ERROR', ' <span class="errorText">al menos ' . ENTRY_POSTCODE_MIN_LENGTH . ' cifras</span>');
define('ENTRY_CITY', 'Ciudad:');
define('ENTRY_CITY_ERROR', ' <span class="errorText">al menos ' . ENTRY_CITY_MIN_LENGTH . ' letras</span>');
define('ENTRY_STATE', 'Estado federal:');
define('ENTRY_STATE_ERROR', ' <span class="errorText">entrada obligatoria</font></small>');
define('ENTRY_COUNTRY', 'País:');
define('ENTRY_TELEPHONE_NUMBER', 'Número de teléfono:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', ' <span class="errorText">al menos' . ENTRY_TELEPHONE_MIN_LENGTH . ' cifras</span>');
define('ENTRY_FAX_NUMBER', 'Número de fax:');
define('ENTRY_NEWSLETTER', 'Newsletter:');
define('ENTRY_CUSTOMERS_STATUS', 'Grupo de clientes:');
define('ENTRY_NEWSLETTER_YES', 'suscrito');
define('ENTRY_NEWSLETTER_NO', 'no suscrito');
define('ENTRY_MAIL_ERROR', ' <span class="errorText">Por favor, haga su selección</span>');
define('ENTRY_PASSWORD', 'Contraseña (creada automáticamente)');
define('ENTRY_PASSWORD_ERROR', ' <span class="errorText">Su contraseña debe contener al menos ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.</span>');
define('ENTRY_MAIL_COMMENTS', 'Texto adicional del correo electrónico:');

define('ENTRY_MAIL', '¿Enviar correo electrónico con contraseña a los clientes?');
define('YES', 'sí');
define('NO', 'no');
define('SAVE_ENTRY', '¿Guardar modificaciones?');
define('TEXT_CHOOSE_INFO_TEMPLATE', 'Plantilla para los detalles de artículo');
define('TEXT_CHOOSE_OPTIONS_TEMPLATE', 'Plantilla para opciones de artículo');
define('TEXT_SELECT', '-- Por favor selecciones --');

// BOF - Tomcraft - 2009-06-10 - added some missing alternative text on admin icons
// Icons
define('ICON_ARROW_RIGHT', 'marcado');
define('ICON_BIG_WARNING', '¡Atención!');
define('ICON_CROSS', 'Incorrecto');
define('ICON_CURRENT_FOLDER', 'Carpeta actual');
define('ICON_DELETE', 'Borrar');
define('ICON_EDIT', 'Editar');
define('ICON_ERROR', 'Error');
define('ICON_FILE', 'Archivo');
define('ICON_FILE_DOWNLOAD', 'Descargar');
define('ICON_FOLDER', 'Carpeta');
define('ICON_LOCKED', 'Bloqueado');
define('ICON_POPUP', 'Prevista Banner');
define('ICON_PREVIOUS_LEVEL', 'Nivel anterior');
define('ICON_PREVIEW', 'Prevista');
define('ICON_STATISTICS', 'Estadística');
define('ICON_SUCCESS', 'Éxito');
define('ICON_TICK', 'verdad');
define('ICON_UNLOCKED', 'desbloqueado');
define('ICON_WARNING', 'Advertencia');
// EOF - Tomcraft - 2009-06-10 - added some missing alternative text on admin icons

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Página %s de %d');
define('TEXT_DISPLAY_NUMBER_OF_BANNERS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> Banners)');
define('TEXT_DISPLAY_NUMBER_OF_COUNTRIES', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de  <b>%d</b> países)');
define('TEXT_DISPLAY_NUMBER_OF_CUSTOMERS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> clientes)');
define('TEXT_DISPLAY_NUMBER_OF_CURRENCIES', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> monedas)');
define('TEXT_DISPLAY_NUMBER_OF_LANGUAGES', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> idiomas)');
define('TEXT_DISPLAY_NUMBER_OF_MANUFACTURERS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> fabricantes)');
define('TEXT_DISPLAY_NUMBER_OF_NEWSLETTERS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> newsletters)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> pedidos)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS_STATUS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de  <b>%d</b> estados de pedido)');
define('TEXT_DISPLAY_NUMBER_OF_XSELL_GROUP', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de  <b>%d</b> grupos de marketing cruzado)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_VPE', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> unidades de embalaje)');
define('TEXT_DISPLAY_NUMBER_OF_SHIPPING_STATUS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> estados de entrega)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de  <b>%d</b> artículos)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_EXPECTED', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> artículos esperados)');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> reseñas)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de  <b>%d</b> ofertas especiales)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_CLASSES', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de  <b>%d</b> clases de impuesto)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_ZONES', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de  <b>%d</b> zonas fiscales)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_RATES', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> tasas fiscales)');
define('TEXT_DISPLAY_NUMBER_OF_ZONES', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de  <b>%d</b> estados federales)');
define('TEXT_DISPLAY_NUMBER_OF_WHOS_ONLINE', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> clientes que están en línea)');

define('PREVNEXT_BUTTON_PREV', '«');
define('PREVNEXT_BUTTON_NEXT', '»');

define('TEXT_DEFAULT', 'por defecto');
define('TEXT_SET_DEFAULT', 'definir como por defecto');
define('TEXT_FIELD_REQUIRED', ' <span class="fieldRequired">* requerido</span>');

define('ERROR_NO_DEFAULT_CURRENCY_DEFINED', 'Error: No se ha definido ninguna moneda por defecto. Por favor, defina una moneda por defecto bajo Adminstration -> Idiomas/Monedas -> Monedas.');

define('TEXT_CACHE_CATEGORIES', 'Casilla categorías');
define('TEXT_CACHE_MANUFACTURERS', 'Casilla fabricante');
define('TEXT_CACHE_ALSO_PURCHASED', 'Módulo también comprado');

define('TEXT_NONE', '--ninguno--');
define('TEXT_AUTO_PROPORTIONAL', '--auto proporcional--');
define('TEXT_AUTO_MAX', '--auto max--');
define('TEXT_TOP', 'Top');

define('ERROR_DESTINATION_DOES_NOT_EXIST', 'Error: Lugar de almacenamiento no existe.');
define('ERROR_DESTINATION_NOT_WRITEABLE', 'Error: Lugar de almacenamiento no es descriptible.');
define('ERROR_FILE_NOT_SAVED', 'Error: El archivo no se guardó.');
define('ERROR_FILETYPE_NOT_ALLOWED', 'Error: Tipo de archivo no está permitido.');
define('SUCCESS_FILE_SAVED_SUCCESSFULLY', 'Éxito: El archivo subido se ha guardado exitosamente.');
define('WARNING_NO_FILE_UPLOADED', 'Advertencia: No se ha subido ningún archivo.');
define('ERROR_FILE_NOT_REMOVEABLE', 'Error: El archivo no pudo ser removido.');

define('DELETE_ENTRY', '¿Borrar entrada?');
define('TEXT_PAYMENT_ERROR', '<b>ADVERTENCIA:</b> Por favor active un <a href="'.xtc_href_link(FILENAME_MODULES, 'set=payment').'">módulo de pago</a>!');
define('TEXT_SHIPPING_ERROR', '<b>ADVERTENCIA:</b> Por favor active un <a href="'.xtc_href_link(FILENAME_MODULES, 'set=shipping').'">módulo de envío</a>!');
define('TEXT_PAYPAL_CONFIG', '<b>ADVERTENCIA</b> Por favor, ajuste la configuración de pago de PayPal para el "modo en vivo" bajo: <a href="%s"><strong>afiliado -> PayPal</strong></a>');
define('TEXT_NETTO', 'Neto: ');
define('TEXT_DUPLUCATE_CONFIG_ERROR', '<b>ADVERTENCIA:</b> Duplicate configuration key: ');

define('ENTRY_CID', 'Número de cliente:');
define('IP', 'IP de pedido:');
define('CUSTOMERS_MEMO', 'Memos:');
define('DISPLAY_MEMOS', 'Mostrar/Escribir');
define('TITLE_MEMO', 'MEMO cliente');
define('ENTRY_LANGUAGE', 'Idioma:');
define('CATEGORIE_NOT_FOUND', 'La categoría no existe');

// BOF - Tomcraft - 2009-06-10 - added some missing alternative text on admin icons
// Image Icons
define('IMAGE_RELEASE', 'canjear cupón');
define('IMAGE_ICON_STATUS_GREEN_STOCK', 'en stock');
define('IMAGE_ICON_STATUS_GREEN', 'activo');
define('IMAGE_ICON_STATUS_GREEN_LIGHT', 'aktivar');
define('IMAGE_ICON_STATUS_RED', 'inactivo');
define('IMAGE_ICON_STATUS_RED_LIGHT', 'desactivar');
define('IMAGE_ICON_INFO', 'elegir');
// EOF - Tomcraft - 2009-06-10 - added some missing alternative text on admin icons

define('_JANUARY', 'Enero');
define('_FEBRUARY', 'Febrero');
define('_MARCH', 'Marzo');
define('_APRIL', 'Abril');
define('_MAY', 'Mayo');
define('_JUNE', 'Junio');
define('_JULY', 'Julio');
define('_AUGUST', 'Agosto');
define('_SEPTEMBER', 'Septiembre');
define('_OCTOBER', 'Octubre');
define('_NOVEMBER', 'Noviembre');
define('_DECEMBER', 'Diciembre');

// Beschreibung f&uuml;r Abmeldelink im Newsletter
define('TEXT_NEWSLETTER_REMOVE', 'Para ya no recibir nuestro newsletter, haga clic aquí:');

define('TEXT_DISPLAY_NUMBER_OF_GIFT_VOUCHERS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> cupones)');
define('TEXT_DISPLAY_NUMBER_OF_COUPONS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> cupones)');
define('TEXT_VALID_PRODUCTS_LIST', 'Lista de artículos');
define('TEXT_VALID_PRODUCTS_ID', 'ID del artículo');
define('TEXT_VALID_PRODUCTS_NAME', 'Nombre del artículo');
define('TEXT_VALID_PRODUCTS_MODEL', 'Número del artículo');

define('TEXT_VALID_CATEGORIES_LIST', 'Lista de categorías');
define('TEXT_VALID_CATEGORIES_ID', 'ID de categoría');
define('TEXT_VALID_CATEGORIES_NAME', 'Nombre de la categoría');

define('SECURITY_CODE_LENGTH_TITLE', 'Longitud del código del cupón');
define('SECURITY_CODE_LENGTH_DESC', 'Introduzca aquí la longitud del código del cupón. (máx. 16 caracteres)');

define('NEW_SIGNUP_GIFT_VOUCHER_AMOUNT_TITLE', 'Regalo de bienvenida valor del cupón');
define('NEW_SIGNUP_GIFT_VOUCHER_AMOUNT_DESC', 'Regalo de bienvenida valor del cupón: Si no desea enviar un cupón en su correo electrónico de bienvenida, introduzca 0 aquí; de lo contrario, introduzca el valor del cupón, por ejemplo, 10.00 o 50.00, pero sin símbolos de moneda.');
define('NEW_SIGNUP_DISCOUNT_COUPON_TITLE', 'Descuento de bienvenida código del cupón');
define('NEW_SIGNUP_DISCOUNT_COUPON_DESC', 'Descuento de bienvenida código del cupón:  Si no desea enviar un cupón en su correo electrónico de bienvenida, deje este campo en blanco; de lo contrario, introduzca el código del cupón que desea utilizar.');

define('TXT_ALL', 'Todos');

// UST ID
define('HEADING_TITLE_VAT', 'Número de IVA');
define('ENTRY_VAT_ID', 'Número de IVA');
define('ENTRY_CUSTOMERS_VAT_ID', 'Número de IVA');
define('TEXT_VAT_FALSE', '<span class="messageStackError">Revisado/¡Número de IVA es inválido!</span>');
define('TEXT_VAT_TRUE', '<span class="messageStackSuccess">Revisado/Número de IVA es válido/span>');
define('TEXT_VAT_UNKNOWN_COUNTRY', '<span class="messageStackError">No revisado/¡País desconocido!</span>');
define('TEXT_VAT_INVALID_INPUT', '<span class="messageStackError">No revisado/¡El código de páis entregado no es válido o el número del IVA está vacío!</span>');
define('TEXT_VAT_SERVICE_UNAVAILABLE', '<span class="messageStackError">No revisado/¡El servicio SOAP no está disponible, inténtelo de nuevo más tarde!</span>');
define('TEXT_VAT_MS_UNAVAILABLE', '<span class="messageStackError">No revisado/¡El servicio del Estado miembro no está disponible, inténtelo de nuevo más tarde o con otro Estado miembro!</span>');
define('TEXT_VAT_TIMEOUT', '<span class="messageStackError">No revisado/¡El servicio del Estado miembro no está disponible (timeout), inténtelo de nuevo más tarde o con otro Estado miembro!</span>');
define('TEXT_VAT_SERVER_BUSY', '<span class="messageStackError">No revisado/El servicio no puede procesar su solicitud. ¡Inténtalo de nuevo más tarde!</span>');
define('TEXT_VAT_NO_PHP5_SOAP_SUPPORT', '<span class="messageStackError">No revisado/¡El soporte para PHP5 SOAP no está disponible!</span>');
define('TEXT_VAT_CONNECTION_NOT_POSSIBLE', '<span class="messageStackError">ERROR: ¡No se puede conectar al servicio web (SOAP ERROR)!</span>');

define('ERROR_GIF_MERGE', 'Falta el soporte GDlib gif, no es posible la marca de aqua (merge)');
define('ERROR_GIF_UPLOAD', 'Falta el soporte GDlib Gif, no es posible subir imágenes GIF');

define('TEXT_REFERER', 'Referer: ');

// BOF - Tomcraft - 2009-06-17 Google Sitemap
define('BOX_GOOGLE_SITEMAP', 'Google Sitemap');
// EOF - Tomcraft - 2009-06-17 Google Sitemap

// BOF - Tomcraft - 2009-10-03 - Paypal Express Modul
define('BOX_PAYPAL', 'PayPal');
// EOF - Tomcraft - 2009-10-03 - Paypal Express Modul

// BOF - Dokuman - 2009-10-02 - added moneybookers payment module version 2.4
define('_PAYMENT_MONEYBOOKERS_EMAILID_TITLE', 'Dirección de correo electrónico Skrill');
define('_PAYMENT_MONEYBOOKERS_EMAILID_DESC', 'Dirección de correo electrónico con la que está registrado en Skrill.com.<br />Si aún no tiene una cuenta, <b>regístrese</b> ahora con <a href="https://account.skrill.com/signup/page1" target="_blank"><b>Skrill</b></a> <b>gratuitamente</b> .');
define('_PAYMENT_MONEYBOOKERS_MERCHANTID_TITLE', 'ID de comerciante Skrill');
define('_PAYMENT_MONEYBOOKERS_MERCHANTID_DESC', 'Su ID de comerciante Skrill');
define('_PAYMENT_MONEYBOOKERS_PWD_TITLE', 'Palabra secreta Skrill');
define('_PAYMENT_MONEYBOOKERS_PWD_DESC', 'Con la introducción de la palabra secreta la conexión se encripta durante el proceso de pago. Esto garantiza la máxima seguridad. Introduzca su palabra secreta Skrill (¡ésta no es su contraseña!). La palabra secreta sólo puede consistir en letras minúsculas y números. Puede definir su palabra secreta <b><span class="col-red">después de la activación</b></span> en su cuenta de usuario Skrill (configuración del distribuidor).<br /><br /><span class="col-red">¡Así se activa su cuenta de Skrill.com para el procesamiento de pagos!</span><br /><br />Envíe un correo electrónico con:<br/>- su dominio de tienda <br/>- su dirección de correo electrónico Skrill <br /><br />A: <a href="mailto:ecommerce@skrill.com?subject=modified eCommerce Shopsoftware: Activación para Skrill Quick Checkout">ecommerce@skrill.com</a>');
define('_PAYMENT_MONEYBOOKERS_TMP_STATUS_ID_TITLE', 'Estado de pedido - Proceso de pago');
define('_PAYMENT_MONEYBOOKERS_TMP_STATUS_ID_DESC', ' En cuanto el cliente pulsa "Enviar pedido" en la tienda, se crea un "pedido temporal". Esto tiene la ventaja de que se ha registrado un pedido para los clientes que cancelan el proceso de pago en Moneybookes.');
define('_PAYMENT_MONEYBOOKERS_PROCESSED_STATUS_ID_TITLE', 'Estado de pedido - pago OK');
define('_PAYMENT_MONEYBOOKERS_PROCESSED_STATUS_ID_DESC', 'Aparece cuando el pago ha sido confirmado por Skrill.');
define('_PAYMENT_MONEYBOOKERS_PENDING_STATUS_ID_TITLE', 'Estado del pedido - pago en espera');
define('_PAYMENT_MONEYBOOKERS_PENDING_STATUS_ID_DESC', 'Si el cliente no tiene crédito en su cuenta, el pago permanecerá pendiente hasta que la cuenta Skrill esté saldada.');

define('_PAYMENT_MONEYBOOKERS_CANCELED_STATUS_ID_TITLE', 'Estado del pedido - Pago cancelado');
define('_PAYMENT_MONEYBOOKERS_CANCELED_STATUS_ID_DESC', 'Aparecerá si, por ejemplo, se ha rechazado una tarjeta de crédito');
define('MB_TEXT_MBDATE', 'Última actualización:');
define('MB_TEXT_MBTID', 'TR ID:');
define('MB_TEXT_MBERRTXT', 'Estado:');
define('MB_ERROR_NO_MERCHANT', '¡No existe ninguna cuenta de Skrill.com con esta dirección de correo electrónico!');
define('MB_MERCHANT_OK', 'Cuenta de Skrill.com correcta, el ID de comerciante %s de Skrill.com recibido y guardado.');
define('MB_INFO', '<img src="../images/icons/moneybookers/MBbanner.jpg" /><br /><br />Ahora puede aceptar directamente tarjetas de crédito, débito directo, transfernecia bancaria inmediata, Giropay y todas las demás opciones de pago locales importantes, con una simple activación en la tienda. Con Skrill como solución todo en uno, no necesita firmar contratos individuales para cada modalidad de pago. Sólo necesita una <a href="https://account.skrill.com/signup/page1" target="_blank"><b>cuenta de Skrill gratuita</b></a>, para aceptar todas las opciones de pago importantes en su tienda. Las modalidades de pago adicionales son sin costes adicionales, el módulo es <b>sin costos fijos mensuales ni costos de instalación</b>.<br /><br /><b>Sus ventajas:</b><br />-La aceptación de las opciones de pago más importantes aumenta sus ventas<br />-Un solo proveedor reduce sus gastos y sus costos<br />-Su cliente paga directamente y sin proceso de registro<br />-Activación e integración con un solo clicn<br />-Muy atractivas <a href="https://www.skrill.com/de/fees/" target="_blank"><b>condiciones</b></a> <br />-confirmación inmediata del pago y verificación de los datos del cliente<br />-procesamiento de pagos también en el extranjero y sin costos adicionales<br />-6 millones de clientes en todo el mundo confían en Skrill.');
// EOF - Dokuman - 2009-10-02 - added moneybookers payment module version 2.4

// BOF - Tomcraft - 2009-11-02 - set global customers-group-permissions
define('BOX_CUSTOMERS_GROUP', 'Autorizaciones de GC');
// EOF - Tomcraft - 2009-11-02 - set global customers-group-permissions

// BOF - Tomcraft - 2009-11-02 - New admin top menu
define('TEXT_ADMIN_START', 'Inicio');
define('BOX_HEADING_CONFIGURATION2', 'Configuración avanzada');
// EOF - Tomcraft - 2009-11-02 - New admin top menu

// BOF - Tomcraft - 2009-11-28 - Included xs:booster
define('BOX_HEADING_XSBOOSTER', 'xs:booster');
define('BOX_XSBOOSTER_LISTAUCTIONS', 'Mostrar subastas');
define('BOX_XSBOOSTER_ADDAUCTIONS', 'Crear subastas');
define('BOX_XSBOOSTER_CONFIG', 'Configuración básica');
// EOF - Tomcraft - 2009-11-28 - Included xs:booster

//BOF - web28 - 2010-04-10 - ADMIN SEARCH BAR
define('ASB_QUICK_SEARCH_CUSTOMER', 'Buscar cliente...');
define('ASB_QUICK_SEARCH_ORDER_ID', 'Buscar número de pedido...');
define('ASB_QUICK_SEARCH_ARTICLE', 'Buscar artículo/categoría...');
define('ASB_QUICK_SEARCH_EMAIL', 'Buscar dirección de correo electrónico...');
define('ASB_QUICK_SEARCH_ARTICLE_ID', 'Buscar ID de artículo/categoría...');
//EOF - web28 - 2010-04-10 - ADMIN SEARCH BAR

//BOF - web28 - 2010.05.30 - accounting - set all checkboxes , countries - set all flags
define('BUTTON_SET', 'Activar todos');
define('BUTTON_UNSET', 'Desactivar todos');
//EOF - web28 - 2010.05.30 - accounting - set all checkboxes 

//BOF - DokuMan - 2010-08-12 - added possibility to reset admin statistics
define('TEXT_ROWS', 'Línea');
define('TABLE_HEADING_RESET', 'Reiniciar Estadísticas');
//EOF - DokuMan - 2010-08-12 - added possibility to reset admin statistics

//BOF - web28 - 2010-11-13 - added BUTTON_CLOSE_WINDOW
define('BUTTON_CLOSE_WINDOW' , 'Cerrar ventana');
//EOF - web28 - 2010-11-13 - added BUTTON_CLOSE_WINDOW

//BOF - hendrik - 2011-05-14 - independent invoice number and date
define('ENTRY_INVOICE_NUMBER', 'Número de factura:');
define('ENTRY_INVOICE_DATE', 'Fecha de factura:');
//EOF - hendrik - 2011-05-14 - independent invoice number and date  

//BOF - web28 - 2010-07-06 - added missing error text
define('ENTRY_VAT_ERROR', ' <span class="errorText">Número de IVA inválido</span>');
//EOF - web28 - 2010-07-06 - added missing error text

define('CONFIG_INT_VALUE_ERROR', '"%s" ERROR: ¡Por favor, introduzca sólo números! ¡Introducción %s fue ignorada!');
define('CONFIG_MAX_VALUE_WARNING', '"%s" ADVERTENCIA: ¡La entrada %s fue ignorada! [Máximo: %s]');
define('CONFIG_MIN_VALUE_WARNING', '"%s" ADVERTENCIA: ¡La entrada %s fue ignorada! [Mínimo: %s]');

define('WHOS_ONLINE_TIME_LAST_CLICK_INFO', 'Período de visualización en segundos: %s. Después de este tiempo, las entradas se borran.');

define('TEXT_GLOBAL_PRODUCTS_MODEL', 'Número del artículo');

define('TEXT_INFO_MODULE_RESTORE', '¿Desea restaurar los ajustes guardados? <br /><br /><b>ATENCIÓN</b>: ¡Todos los ajustes actuales serán sobrescritos!');
define('TEXT_INFO_MODULE_REMOVE', '¿Desea desinstalar el módulo?<br /><br /><b>ATENCIÓN</b>: ¡También se borrarán todos los ajustes del módulo!');
define('TEXT_INFO_MODULE_BACKUP', '¿Desea guardar los ajustes del módulo?');
define('MODULE_BACKUP_CONFIRM', '¡Los ajustes del módulo se han guardados exitosamente!');
define('MODULE_RESTORE_CONFIRM', '¡Los ajustes del módulo se han restaurados exitosamente!');
define('MODULE_UPDATE_CONFIRM', '¡Los ajustes del módulo se han actualizados exitosamente!');

/* magnalister v1.0.0 */
define('BOX_HEADING_MAGNALISTER', 'magnalister');
define('BOX_MAGNALISTER', 'magnalister Admin');
/* END magnalister */

define('CHARS_LEFT', 'caracteres restantes');
define('CHARS_MAX', 'de máx.');

define('DISPLAY_PER_PAGE', 'Visualización por página: ');

define('SPECIALS_DATE_START_TT', 'Las ofertas empiezan a las 00:00:00 horas');
define('SPECIALS_DATE_END_TT', 'Las ofertas terminan a medianoche (23:59:59 horas)');

define('BOX_PARCEL_CARRIERS', 'Proveedor de servicios de paquetería');
define('TEXT_DISPLAY_NUMBER_OF_CARRIERS', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> proveedores de servicios de paquetería)');

define('RSS_FEED_TITLE', 'Información más reciente del blog de modified eCommerce Shopsoftware');
define('RSS_FEED_DESCRIPTION', 'Información más reciente del foro del soporte de modified eCommerce Shopsoftware');
define('RSS_FEED_LINK', 'http://www.modified-shop.org/blog');
define('RSS_FEED_ALTERNATIVE', 'Lamentablemente, las últimas noticias no se pueden mostrar en el canal RSS. Visite nuestro blog en <a href="'.RSS_FEED_LINK.'">www.modified-shop.org/blog</a> para obtener información importante para los operadores de tiendas sobre estos temas: <ul><li>Actualizaciones importantes y Fixes</li><li>mejoras funcionales</li><li>jurisdicciones</li><li>novedades</li><li>chismes</li></ul>');
define('TEXT_DISPLAY_NUMBER_OF_NEWSFEED', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> novedades)');

define('CFG_TXT_YES', 'Sí');
define('CFG_TXT_NO', 'No');
define('CFG_TXT_OR', 'o');
define('CFG_TXT_AND', 'y');
define('CFG_TXT_ASC', 'ascendente');
define('CFG_TXT_DESC', 'descendente');
define('CFG_TXT_PRODUCTS_NAME', 'Nombre del artículo');
define('CFG_TXT_PRODUCTS_MODEL', 'Numero del artículo');
define('CFG_TXT_DATE_EXPECTED', 'disponible desde');
define('CFG_TXT_ACCOUNT', 'Cuenta de cliente');
define('CFG_TXT_GUEST', 'Cuenta de visitante');
define('CFG_TXT_BOTH', 'ambos');
define('CFG_TXT_NONE', 'desactivado');
define('CFG_TXT_ADMIN', 'Admin');
define('CFG_TXT_ALL', 'todos');
define('CFG_TXT_WEIGHT', 'peso');
define('CFG_TXT_PRICE', 'precio');
define('CFG_TXT_ITEM', 'cantidad');

define('CSRF_TOKEN_MANIPULATION', 'Manipulación de CSRFToken (Por razones de seguridad ya no está permitido trabajar en diferentes pestañas en el área de administración.)');
define('CSRF_TOKEN_NOT_DEFINED', 'CSRToken no está definido (por razones de seguridad ya no está permitido trabajar en diferentes pestañas en el área de administración.)');

define('TEXT_ACCOUNTING_INFO', '¡Los derechos de acceso no pueden ser revocados del Admin principal[1]!');

define('JAVASCRIPT_DISABLED_INFO', 'JavaScript está desactivado en su navegador. Active JavaScript para utilizar todas las funciones de este sitio web y poder ver todos los contenidos.');

define('BOX_MODULE_TYPE', 'Módulos de extensiones de clase');

define('MULTIPLE_INSTALLATION', '<span style="color:red">[instalación múltiple: %s]</span>');

define('FILEUPLOAD_INPUT_TXT', 'Ningún archivo');
define('FILEUPLOAD_BTN_TXT', 'Buscar');

define('CHECK_LABEL_PRICE', 'Solicitar precio');

define('TEXT_PAYPAL_TAB_CONFIG', 'Configuración PayPal');
define('TEXT_PAYPAL_TAB_PROFILE', 'Perfil de PayPal');
define('TEXT_PAYPAL_TAB_WEBHOOK', 'Webhook de PayPal');
define('TEXT_PAYPAL_TAB_MODULE', 'Módulo PayPal');
define('TEXT_PAYPAL_TAB_TRANSACTIONS', 'Transacciones de PayPal');

define('TEXT_DEFAULT_SORT_ORDER_TITLE', 'Orden de clasificación');
define('TEXT_DEFAULT_SORT_ORDER_DESC', 'Orden de procesamiento. El dígito más pequeño se ejecuta primero.');
define('TEXT_DEFAULT_STATUS_TITLE', '¿Activar módulo?');
define('TEXT_DEFAULT_STATUS_DESC', 'Estado del módulo');

define('TEXT_HOUR', 'hora');
define('TEXT_HOURS', 'horas');

define('DELETE_LOGS_SUCCESSFUL', 'Logfiles borrados con éxito.');

define('BOX_BLACKLIST_LOGS', 'Logs de la lista negra');

define('CONTINUE_WITHOUT_SAVE', 'Los cambios no guardados se pierden.');
?>

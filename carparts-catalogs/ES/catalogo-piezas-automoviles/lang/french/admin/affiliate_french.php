<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_german.php 16 2010-10-28 15:21:32Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_german.php, v 1.3 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   
   corrected E_NOTICE constnt already defined, 01-2018 noRiddle
   ---------------------------------------------------------------------------*/

// reports box text in includes/boxes/affiliate.php
define('BOX_CONFIGURATION_900', 'Affilier configuration');
define('BOX_HEADING_AFFILIATE', 'Programme d\'affiliation');
define('BOX_AFFILIATE_CONFIGURATION', 'Affilier configuration');
define('BOX_AFFILIATE_SUMMARY', 'Résumé');
define('BOX_AFFILIATE', 'Partenaires');
define('BOX_AFFILIATE_PAYMENT', 'Paiement de commission');
define('BOX_AFFILIATE_BANNERS', 'Bannière');
define('BOX_AFFILIATE_CONTACT', 'Contact');
define('BOX_AFFILIATE_SALES', 'Vente partenaires');
define('BOX_AFFILIATE_CLICKS', 'Clics');

define('BUTTON_ADD_BANNER', 'Saisir bannière');
define('BUTTON_BILLING', 'Commencer décompte');
define('BUTTON_BANNERS', 'Administration bannière');
define('BUTTON_CLICKTHROUGHS', 'Statistique des clics');
define('BUTTON_SALES', 'Vente partenaires');
?>

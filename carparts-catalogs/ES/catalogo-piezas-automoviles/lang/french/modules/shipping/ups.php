<?php
/* -----------------------------------------------------------------------------------------
   $Id: ups.php 5121 2013-07-18 11:38:19Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(UPS.php,v 1.4 2003/02/18 04:28:00); www.oscommerce.com 
   (c) 2003	 nextcommerce (UPS.php,v 1.5 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   German Post (Deutsche Post WorldNet)
   Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl
   Changes for personal use: Copyright (C) 2004 Comm4All, Bernd Blazynski | http://www.comm4all.com & http://www.cheapshirt.de

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/


define('MODULE_SHIPPING_UPS_TEXT_TITLE', 'United Parcel Service Standard');
define('MODULE_SHIPPING_UPS_TEXT_DESCRIPTION', 'United Parcel Service Standard - Modules d\'envoi');
define('MODULE_SHIPPING_UPS_TEXT_WAY', 'Envoi vers');
define('MODULE_SHIPPING_UPS_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_UPS_TEXT_FREE', 'A partir de EUR ' . MODULE_SHIPPING_UPS_FREEAMOUNT . ' de commande nous envoyons votre commande sans frais de port!');
define('MODULE_SHIPPING_UPS_TEXT_LOW', 'A partir de EUR ' . MODULE_SHIPPING_UPS_FREEAMOUNT . ' de commande nous envoyons votre commande avec des frais de port réduits!');
define('MODULE_SHIPPING_UPS_INVALID_ZONE', 'Malheureusement il n\'est pas possible d\'envoyer dans ce pays');
define('MODULE_SHIPPING_UPS_UNDEFINED_RATE', 'Les frais de port ne peuvent pas être calculés pour le moment.');

define('MODULE_SHIPPING_UPS_STATUS_TITLE' , 'UPS Standard');
define('MODULE_SHIPPING_UPS_STATUS_DESC' , 'Voulez-vous proposer l\'envoi par UPS Standard?');
define('MODULE_SHIPPING_UPS_HANDLING_TITLE' , 'Supplément');
define('MODULE_SHIPPING_UPS_HANDLING_DESC' , 'Supplément de traitement pour ce type d\'envoi en Euro');
define('MODULE_SHIPPING_UPS_TAX_CLASS_TITLE' , 'Taux d\'imposition');
define('MODULE_SHIPPING_UPS_TAX_CLASS_DESC' , 'Veuillez séléctionner le taux de TVA pour ce type d\'envoi');
define('MODULE_SHIPPING_UPS_ZONE_TITLE' , 'Zone d\'envoi');
define('MODULE_SHIPPING_UPS_ZONE_DESC' , 'Si vous choisissez une zone ce type d\'envoi est seulement porposé dans cette zone.');
define('MODULE_SHIPPING_UPS_SORT_ORDER_TITLE' , 'Ordre d\'affichage');
define('MODULE_SHIPPING_UPS_SORT_ORDER_DESC' , 'Les plus bas sont affichés d\'abord.');
define('MODULE_SHIPPING_UPS_ALLOWED_TITLE' , 'Zones d\'envoi individuelles');
define('MODULE_SHIPPING_UPS_ALLOWED_DESC' , 'Veuillez indiquer les zones <b>individuellement</b> pour lesquelles l\'envoi est possible. Par exemple AT,DE');
define('MODULE_SHIPPING_UPS_FREEAMOUNT_TITLE' , 'Sans frais de port internes');
define('MODULE_SHIPPING_UPS_FREEAMOUNT_DESC' , 'Commande minimum pour l\'envoi sans frais de port interne et des frais de ports réduits à l\'étranger.');

define('MODULE_SHIPPING_UPS_COUNTRIES_1_TITLE' , 'Etats pour Zone 1 UPS Standard');
define('MODULE_SHIPPING_UPS_COUNTRIES_1_DESC' , 'Liste de codes ISO de états séparés par des virgules qui font partis de la zone 1 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_UPS_COST_1_TITLE' , 'Tarifs pour la zone 1 UPS Standard');
define('MODULE_SHIPPING_UPS_COST_1_DESC' , 'Frais de ports basés sur le poids dans la zone 1. Exemple: Colis entre 0 et 4kg coute 5,15 = 4:5.15,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_2_TITLE' , 'Tarifs pour la zone 3 UPS Standard');
define('MODULE_SHIPPING_UPS_COUNTRIES_2_DESC' , 'Liste de codes ISO de états séparés par des virgules qui font partis de la zone 3 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_UPS_COST_2_TITLE' , 'Tarifs pour la zone 3 UPS Standard');
define('MODULE_SHIPPING_UPS_COST_2_DESC' , 'Frais de ports basés sur le poids dans la zone 3. Exemple: Colis entre 0 et 4kg coute EUR 13,75 = 4:13.75,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_3_TITLE' , 'Etats pour Zone 31 UPS Standard');
define('MODULE_SHIPPING_UPS_COUNTRIES_3_DESC' , 'Liste de codes ISO de états séparés par des virgules qui font partis de la zone 31 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_UPS_COST_3_TITLE' , 'Tarifs pour la zone 31 UPS Standard');
define('MODULE_SHIPPING_UPS_COST_3_DESC' , 'Frais de ports basés sur le poids dans la zone 3. Exemple: Colis entre 0 et 4kg coute EUR 23,50 = 4:23.50,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_4_TITLE' , 'Etats pour zone 4 UPS Standard');
define('MODULE_SHIPPING_UPS_COUNTRIES_4_DESC' , 'Liste de codes ISO de états séparés par des virgules qui font partis de la zone 4 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_UPS_COST_4_TITLE' , 'Tarifs pour la zone 4 UPS Standard');
define('MODULE_SHIPPING_UPS_COST_4_DESC' , 'Frais de ports basés sur le poids dans la zone 4. Exemple: Colis entre 0 et 4kg coute EUR 25,40 = 4:25.40,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_5_TITLE' , 'Etats pour zone 41 UPS Standard');
define('MODULE_SHIPPING_UPS_COUNTRIES_5_DESC' , 'Liste de codes ISO de états séparés par des virgules qui font partis de la zone 41 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_UPS_COST_5_TITLE' , 'Tarifs pour la zone 41 UPS Standard');
define('MODULE_SHIPPING_UPS_COST_5_DESC' , 'Frais de ports basés sur le poids dans la zone 4. Exemple: Colis entre 0 et 4kg coute EUR 30,00 = 4:30.00,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_6_TITLE' , 'Etats pour zone 5 UPS Standard');
define('MODULE_SHIPPING_UPS_COUNTRIES_6_DESC' , 'Liste de codes ISO de états séparés par des virgules qui font partis de la zone 41 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_UPS_COST_6_TITLE' , 'Tarifs pour la zone 5 UPS Standard');
define('MODULE_SHIPPING_UPS_COST_6_DESC' , 'Frais de ports basés sur le poids dans la zone 4. Exemple: Colis entre 0 et 4kg coute EUR 34,35 = 4:34.35,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_7_TITLE' , 'Etats pour zone 6 UPS Standard');
define('MODULE_SHIPPING_UPS_COUNTRIES_7_DESC' , 'Liste de codes ISO de états séparés par des virgules qui font partis de la zone 6 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_UPS_COST_7_TITLE' , 'Tarifs pour la zone 6 UPS Standard');
define('MODULE_SHIPPING_UPS_COST_7_DESC' , 'Frais de ports basés sur le poids dans la zone 4. Exemple: Colis entre 0 et 4kg coute EUR 37,10 = 4:37.10,...');
?>

<?php
/*******************************************************************************************
* file: /set_prod_status.html.php
* (c) noRiddle 02-2016
*******************************************************************************************/
?>
<div class="script-code">
    <h2><?php echo ADMIN_TOOLS_SET_PROD_STATUS_TITLE.'<br /><span class="sml">'.ADMIN_TOOLS_SET_PROD_STATUS_DESC.'</span>'; //echo ADMIN_TOOLS_SCRIPT_HEADING; ?></h2>
    <?php echo ADMIN_TOOLS_SET_PROD_STATUS_GUIDANCE; ?>
    <div class="div-marg">
        <?php echo '<h4>'.ADMIN_TOOLS_SET_PROD_STATUS_0.'</h4>'; ?>
        <div class="bord">
            <?php
            echo xtc_draw_form('set_prod_status_0', FILENAME_MASTER_ADMIN, 'script=set_prod_status&action=set_prod_status_0')
            .ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_0.' <input class="" name="supplier_id_0" value="" placeholder="997" type="text" />'
            .'<button type="submit" onclick="this.blur();">'.BUTTON_SEND.'</button>'
            .'<span id="set_prod_out" style="color:#0A7E00;">'.$set_prod_out_0.'</span>';
            ?>
            </form>
        </div>
    </div>
    <div class="div-marg">
        <?php echo '<h4>'.ADMIN_TOOLS_SET_PROD_STATUS_B_0.'</h4>'; ?>
        <div class="bord">
            <?php
            echo xtc_draw_form('set_prod_status_b_0', FILENAME_MASTER_ADMIN, 'script=set_prod_status&action=set_prod_status_b_0')
            .ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_B_01.' <input class="" name="supplier_id_b_01" value="" placeholder="997" type="text" />'
            .ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_B_01B.' <input class="" name="supplier_id_b_02" value="" placeholder="997" type="text" />'
            .'<button type="submit" onclick="this.blur();">'.BUTTON_SEND.'</button>'
            .'<span id="set_prod_out" style="color:#0A7E00;">'.$set_prod_out_b_0.'</span>';
            ?>
            </form>
        </div>
    </div>
    <div class="div-marg">
        <?php echo '<h4>'.ADMIN_TOOLS_SET_PROD_STATUS_1.'</h4>'; ?>
        <div class="bord">
            <?php
            echo xtc_draw_form('set_prod_status_1', FILENAME_MASTER_ADMIN, 'script=set_prod_status&action=set_prod_status_1')
            .ADMIN_TOOLS_SET_PROD_STATUS_SUPPL_ID_1.' <input class="" name="supplier_id_1" value="" placeholder="000" type="text" />'
            .'<button type="submit" onclick="this.blur();">'.BUTTON_SEND.'</button>'
            .'<span id="set_prod_out" style="color:#0A7E00;">'.$set_prod_out_1.'</span>';
            ?>
            </form>
        </div>
    </div>
</div>
<?php
/*****************************************************
* file: add_content_to_startpage.php
* use: have another content to display on startpage
* (c) noRiddle 05-2017
*****************************************************/

$shop_content_data2 = $main->getContentData(10, '', '', false);

$default_smarty->assign('title2', $shop_content_data2['content_heading']);
$default_smarty->assign('text2', $shop_content_data2['content_text']);

//add text AND file to startpage (see getContentData() in main class
$default_smarty->assign('nr_start_file', $shop_content_data['content_file']);

//BOC content for not logged in text 04-2018
if(!isset($_SESSION['customer_id'])) {
    $shop_content_data3 = $main->getContentData(15, '', '', false);

    $default_smarty->assign('title3', $shop_content_data3['content_heading']);
    $default_smarty->assign('text3', $shop_content_data3['content_text']);
}
//EOC content for not logged in text 04-2018
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id: shipcloud.php 2011-11-24 modified-shop $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('HEADING_SHIPCLOUD_PICKUP', 'shipcloud - recogida');

define('TABLE_HEADING_EDIT', 'Todos');
define('TABLE_HEADING_ORDERS_ID', 'Número de pedido');
define('TABLE_HEADING_TRACKING_ID', 'Número de envío');
define('TABLE_HEADING_DATE_ADDED', 'Fecha');

define('TEXT_SC_EARLIEST', 'lo más temprano');
define('TEXT_SC_LATEST', 'a más tardar');

define('BUTTON_PICKUP', 'reservar la recogida');
?>

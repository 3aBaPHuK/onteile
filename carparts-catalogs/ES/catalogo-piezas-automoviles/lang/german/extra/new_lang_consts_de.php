<?php
/**************************************
* file: new_lang_consts_de.php
* use: new language constants
* (c) noRiddle 01-2017
**************************************/

define('AH_IMAGE_BUTTON_SEARCH', 'TEILENUMMER HIER EINGEBEN');
//new error for invalid part no coming from search_results.php
define('AH_JS_AT_LEAST_ONE_INPUT', 'Leider haben wir unter der von Ihnen eingegebenen Teilenummer kein Ersatzteil gefunden.\n Wir bieten Ihnen folgende Hilfen an:');
//errors for new field VIN in checkout_payment, noRiddle
define('ERROR_NO_VIN_INDICATED', 'Bitte geben Sie Ihre Fahrzeug-Identifizierungsnummer (FIN 17-stellig) an. <br />Mit dieser Information stellen wir sicher, dass Ihre bestellten Teile zu Ihrem Auto passen.<br />Im Falle für Ihre Bestellung die FIN nicht relevant ist geben Sie bitte "no" in das FIN-Feld ein.');
define('ERROR_VIN_NOT_PLAUSIBLE', 'Bitte geben Sie eine gültige Fahrzeug-Identifizierungsnummer (FIN 17-stellig) an. <br />Mit dieser Information stellen wir sicher, dass Ihre bestellten Teile zu Ihrem Auto passen.');
//new email fields, noRiddle
define('EMAIL_PRODUCTS_PRICE_QUESTION', 'Produktanfrage für: ');
define('EMAIL_VIN', 'Fahrzeug-Identifizierungsnummer (FIN): ');
define('AH_ERROR_VIN_EMPTY', '<li><b>Fahrzeug-Identifizierungsnummer (FIN):</b><br />Bitte geben Sie Ihre Fahrzeug-Identifizierungsnummer (FIN 17-stellig) an.<br />Mit dieser Information stellen wir sicher, dass das angefragte Teil zu Ihrem Auto passt.</li>');
define('AH_ERROR_VIN_NOPLAUSIBLE', '<li><b>Fahrzeug-Identifizierungsnummer (FIN):</b><br />Bitte geben Sie eine gültige Fahrzeug-Identifizierungsnummer (FIN 17-stellig) an.<br />Mit dieser Information stellen wir sicher, dass das angefragte Teil zu Ihrem Auto passt.</li>');

//BOC SEO meta title texts, noRiddle
define('SEO_META_TITLE_1', 'online günstig kaufen');
define('SEO_META_TITLE_2', 'Ersatzteil online');
define('SEO_META_TITLE_3', 'Ersatzteil online bestellen');
define('SEO_META_TITLE_4', 'für online Auto-Katalog Fin-Suche');
//EOC SEO meta title texts, noRiddle

define('NR_GOOG_DEFAULT_TXT', 'Weitere Sprachen'); //google translator

//BOC new for ADAC field, noRiddle
define('ENTRY_ADAC_NO_FALSE', 'Ihre eingegebene ADAC-Mitgliedsnummer ist nicht korrekt.<br />Bitte überprüfen Sie diese.<br />Sie können jederzeit ein Konto ohne ADAC-Mitgliedsnummer erstellen.<br />In diesem Fall lassen sie das entsprechende Feld einfach leer.');
define('ENTRY_ACCOUNT_ADAC_NO_FALSE', 'Ihre eingegebene ADAC-Mitgliedsnummer ist nicht korrekt.<br />Bitte überprüfen Sie diese.');
define('TXT_SUCCESS_ADAC_MEMBER', 'Ihr Kundenstatus wurde auf ADAC-Mitglied gesetzt, Sie können nun für reduzierte Preise einkaufen');
define('TXT_ERROR_ADAC_MEMBER', 'Ihr Kundenstatus konnte leider nicht auf ADAC-Mitglied geändert werden, wir bitten um Entschuldigung.<br />Bitte <a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=7').'" title="Kontakt">kontaktieren</a> Sie uns dahingehend.');
//EOC new for ADAC field, noRiddle

//BOC inclusion for affiliate program, noRiddle
require(DIR_WS_LANGUAGES . 'german/'.'affiliate_german.php');
//EOC inclusion for affiliate program, noRiddle

//BOC additional text for login.php, noRiddle
define('ADD_TEXT_GUEST_LOGIN', 'Außerdem können Sie Kundenvorteile wie <a class="iframe" href="'.xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=964').'" title="Rabatte" target="_blank">ADAC-, Club- und Werkstattrabatte</a>, nur als registrierter Kunde wahrnehmen.');
define('ADD_TEXT_NEW_LOGIN', 'Wir legen größten Wert auf den <a class="iframe" href="'.xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=2').'" title="Datenschutz" target="_blank">Schutz Ihrer Daten</a>.');
define('ADD_INFO_TEXT_NEW_LOGIN', '<ul><li>Wenn Sie gewerblicher Kunde sind, z.B. eine <strong>Werkstatt</strong>, können Sie zu <strong>Sonderkonditionen</strong> einkaufen.<br />Dafür benötigen wir Ihren Gewerbenachweis, den Sie uns über unsere <a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=7').'" title="Kontakt">Kontakt-Formular</a> zukommen lassen können.</li>'.($cat_env == 'de' ? '<li>Außerdem gibt es <strong>Rabatte</strong> für <strong>Club-</strong> und <strong>ADAC-Mitglieder</strong>.<br />Für die ADAC-Konditionen einfach oben im Top-Menu unter "Mein Konto" Ihre ADAC-Mitgliedsnummer angeben und sie werden in eine günstigere Preisgruppe eingeordnet.</li>' : '<li>Außerdem gibt es <strong>Rabatte</strong> für <strong>Club-</strong>Mitglieder. Bitte senden Sie uns Ihren Clubausweis über das <a href="xx_7.html" title="Datei senden über Kontakt-Formular">Kontaktformular</a>!')
.'</ul>');
//EOC additional text for login.php, noRiddle

//BOC needed for order mail if deposit, noRiddle
define('AH_INCLUSIVE', 'inkl.');
//EOC needed for order mail if deposit, noRiddle

//BOC new texts for catalogue, noRiddle
define('S_TXT_PARTNO', 'Teilenummer');
define('S_TXT_NAME', 'Name');
define('S_TXT_QTY', 'Menge');
define('S_TXT_CHECK', 'Anhaken');
define('S_TXT_INTOCART', 'Angehaktes Teil in den Warenkorb legen');
define('S_SPECIALS_HEADING_TXT', 'Eine Auswahl unserer attraktiven Zubehör-Angebote für %s');
define('S_PART_WAS_REPLACED_WITH', 'Dieses Teil wurde ersetzt mit ');
define('IMAGE_BUTTON_CATALOGUE_CHECKOUT', 'Bestellung im Marken-Shop abschliessen');
define('IMAGE_BUTTON_UPDATE_CAT_CART', '%s Warenkorb aktualisieren'); //display also brand, noRiddle
define('NR_UPDATE_CART', 'Ihr %s Warenkorb enthält:'); //new heading in cart, noRiddle
define('NR_NOW_CREDIT_FOR_VINSEARCH', 'Warum sehe ich das FIN(VIN)-Suchfeld nicht ?<br />Mit Erstellung Ihres  Kontos haben Sie 10 Gratispunkte erhalten um die Suche nach FIN benutzen zu können.<br />Sie können weitere Gratisabfragen entweder durch Kauf von Ersatzteilen in einem der Shops erhalten oder müßten ansonsten Abfragen <a href="'.xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=1').'" title="Guthaben für FIN-Suche kaufen">&raquo; hier kaufen</a>.<br />Die Identifikation Ihres Fahrzeuges mittels Parametern (insofern vorhanden) ist in jedem Falle kostenlos für Sie.');
define('TXT_SEND_ORDER_VI_CREDIT', 'Sie haben %d Punkte zu Ihrem FIN-Suche-Guthaben-Konto hinzubekommen.');

define('TXT_MAY_WE_HELP', 'Können wir Ihnen helfen ?');
define('TXT_DIRECTLY_TO_PARTNER_BUTTON1', 'Direkt zu unserem');
define('TXT_DIRECTLY_TO_PARTNER_BUTTON2', '-Partner');
define('TXT_CONTACT_US_IF_NEED_HELP', 'Sie haben Fragen zu dieser Marke oder Ersatzteilen ?');
define('TXT_IMPORT_INFO_VIN', 'Es sind nicht alle Fahrzeug-Identifizierungsnummer (VIN) erfasst. Wenn Ihre VIN nicht gefunden wird benutzen Sie bitte, wenn sie vorhanden ist, die "Identifikation des Fahrzeuges mittels Parametern" links.<br />');
define('TXT_IMPORT_INFO_VIN_VAG', 'Es sind für dieses Fabrikat im Moment aus technischen Gründen nur ca 50% aller Fahrzeug-Identifizierungsnummer (VIN) erfasst. Wenn Ihre VIN nicht gefunden wird benutzen Sie bitte links die "Identifikation des Fahrzeuges mittels Parametern".<br />');
//EOC new texts for catalogue, noRiddle

//BOC new texts for cookieconsent plugin, noRiddle
define('COOKIES_TXT_NEW', '<i class="fa fa-cog"></i> Durch die Weiterverwendung dieser Webseite akzeptieren Sie den Gebrauch von Cookies, die einen reibungslosen Zugang zu unserer Webseite garantieren.');
define('COOKIES_LINKTXT_TITLE', 'Weitere Informationen');
define('COOKIES_LINKTXT', '<i class="fa fa-info-circle"></i>');
define('COOKIES_CLOSE_TITLE', 'schließen');
define('COOKIES_CLOSE', '<i class="fa fa-close"></i>');
//EOC new texts for cookieconsent plugin, noRiddle

//BOC käufersiegel, noRiddle
define('KS_THIS_SHOP_WAS', 'Der Shop wurde bei kaeufersiegel.de mit ');
define('KS_OF', ' von ');
define('KS_STARES_RATED', 'Sternen bewertet - basierend auf ');
define('KS_RATINGS', ' Bewertungen');
//EOC käufersiegel, noRiddle

//BOC ADAC save button, noRiddle
define('IMAGE_BUTTON_SAVE_ADAC_NO', 'ADAC Mitgliedsnummer speichern');
//EOC ADAC save button, noRiddle

//BOC new for account information, noRiddle
define('SHOW_CUST_STATUS_NAME_GARAGE', 'Sie sind als angemeldet Werkstatt');
define('SHOW_CUST_STATUS_NAME_CLUB', 'Sie sind angemeldet als Marken-Club-Mitglied');
define('SHOW_CUST_STATUS_NAME_ADAC', 'Sie sind angemeldet als ADAC-Mitglied');
//EOC new for account information, noRiddle

//BOC new button for login box, noRiddle
define('IMAGE_BUTTON_NEW_CUST', 'Neuer Kunde ?');
//EOC new button for login box, noRiddle

//BOC button "all brands at a glance" in shops box, 10-2020, noRiddle
define('AH_ALL_BRANDS_MAIN_TXT', 'Hauptportal');
define('AH_ALL_BRANDS_VIEW_TXT', '&raquo; Alle Marken auf einen Blick');
//EOC button "all brands at a glance" in shops box, 10-2020, noRiddle

//01-2021, noRiddle
define('NO_GENDER', 'Keine Anrede');
?>
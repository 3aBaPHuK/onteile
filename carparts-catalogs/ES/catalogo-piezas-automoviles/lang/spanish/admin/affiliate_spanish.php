<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_german.php 16 2010-10-28 15:21:32Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_german.php, v 1.3 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   
   corrected E_NOTICE constnt already defined, 01-2018 noRiddle
   ---------------------------------------------------------------------------*/

// reports box text in includes/boxes/affiliate.php
define('BOX_CONFIGURATION_900', 'Configuración de afiliado');
define('BOX_HEADING_AFFILIATE', 'Programa de afiliados');
define('BOX_AFFILIATE_CONFIGURATION', 'Configuración de afiliado');
define('BOX_AFFILIATE_SUMMARY', 'Resumen');
define('BOX_AFFILIATE', 'afiliado');
define('BOX_AFFILIATE_PAYMENT', 'Pago de comisión');
define('BOX_AFFILIATE_BANNERS', 'Banner de afiliado');
define('BOX_AFFILIATE_CONTACT', 'Contacto');
define('BOX_AFFILIATE_SALES', 'Ventas de afiliado');
define('BOX_AFFILIATE_CLICKS', 'Clics');

define('BUTTON_ADD_BANNER', 'Registrar Banner');
define('BUTTON_BILLING', 'Iniciar facturación');
define('BUTTON_BANNERS', 'Gestión de Banner');
define('BUTTON_CLICKTHROUGHS', 'Estadística de clics');
define('BUTTON_SALES', 'Ventas de afiliado');
?>

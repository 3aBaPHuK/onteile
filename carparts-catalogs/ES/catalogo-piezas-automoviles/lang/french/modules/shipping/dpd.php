<?php
/*------------------------------------------------------------------------------
   v 1.0 
   XTC-DPD Shipping Module - Contribution for XT-Commerce http://xt-commerce.com
   modified by http://www.hwangelshop.de

   Copyrigt (c) 2004 cigamth
  ------------------------------------------------------------------------------
   $Id: dpd.php 2751 2012-04-12 13:28:06Z Tomcraft1980 $

   XTC-GLS Shipping Module - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.hhgag.com

   Copyright (c) 2004 H.H.G.
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 Deutsche Post Module
   Original written by Marcel Bossert-Schwab (webmaster@wernich.de), Version 1.2b
   Addon Released under GLSL V2.0 by Gunter Sammet (Gunter@SammySolutions.com)

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License

   ---------------------------------------------------------------------------*/
define('MODULE_SHIPPING_DPD_TEXT_TITLE', 'DPD Dynamic Parcel Distribution');
define('MODULE_SHIPPING_DPD_TEXT_DESCRIPTION', 'DPD Dynamic Parcel Distribution - Envoi mondial');
define('MODULE_SHIPPING_DPD_TEXT_WAY', 'Envoi vers');
define('MODULE_SHIPPING_DPD_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_DPD_INVALID_ZONE', 'Malheureusement il n\'est pas possible d\'envoyer dans ce pays');
define('MODULE_SHIPPING_DPD_UNDEFINED_RATE', 'Les frais de port ne peuvent pas être calculés pour le moment');
define('MODULE_SHIPPING_DPD_FREE_SHIPPING', 'Nous prenons en charge les frais de port');
define('MODULE_SHIPPING_DPD_SUBSIDIZED_SHIPPING', 'Nous prenons en charge une partie des frais de port.');

define('MODULE_SHIPPING_DPD_STATUS_TITLE', 'DPD Dynamic Parcel Distribution');
define('MODULE_SHIPPING_DPD_STATUS_DESC', 'Voulez-vous proposer un envoi par DPD Dynamic Parcel Distribution?');
define('MODULE_SHIPPING_DPD_HANDLING_TITLE', 'Frais de traitement');
define('MODULE_SHIPPING_DPD_HANDLING_DESC', 'Frais de traitement pour ce type d\'envoi en Euro');
define('MODULE_SHIPPING_DPD_ALLOWED_TITLE' , 'Zones d\'envoi permises');
define('MODULE_SHIPPING_DPD_ALLOWED_DESC' , 'Indiquez les zones <b>individuellement</b> dans lesquelles l\'envoi est possible. Par exemple AT,DE (Laissez ce champ vide si vous voulez permettre toutes les zones)');
define('MODULE_SHIPPING_DPD_SORT_ORDER_TITLE', 'Ordre d\'affichage');
define('MODULE_SHIPPING_DPD_SORT_ORDER_DESC', 'Indiquez les zones <b>individuellement</b> dans lesquelles l\'envoi est possible. Par exemple AT,DE');
define('MODULE_SHIPPING_DPD_TAX_CLASS_TITLE', 'Taux d\'imposition');
define('MODULE_SHIPPING_DPD_TAX_CLASS_DESC', 'Séléctionnez le taux de TVA pour ce type d\'envoi');
define('MODULE_SHIPPING_DPD_ZONE_TITLE', 'Zone d\'envoi');
define('MODULE_SHIPPING_DPD_ZONE_DESC', 'Si vous séléctionnez une zone, ce type d\'envoi sera seulement proposé dans cette zone.');

?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id: gls.php 5121 2013-07-18 11:38:19Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(dp.php,v 1.4 2003/02/18 04:28:00); www.oscommerce.com 
   (c) 2003	nextcommerce (dp.php,v 1.5 2003/08/13); www.nextcommerce.org
   (c) 2009	shd-media (gls.php 899 27.05.2009);

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   GLS (German Logistic Service) based on DP (Deutsche Post)        
   (c) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl&auml;nkers | http://www.themedia.at & http://www.oscommerce.at
    
   GLS contribution made by shd-media (c) 2009 shd-media - www.shd-media.de
   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_GLS_TEXT_TITLE', 'GLS');
define('MODULE_SHIPPING_GLS_TEXT_DESCRIPTION', 'GLS - Envoi paneuropéen');
define('MODULE_SHIPPING_GLS_TEXT_WAY', 'Envoi vers');
define('MODULE_SHIPPING_GLS_POSTCODE_INFO_TEXT', 'Suppléments insulaires incluses');
define('MODULE_SHIPPING_GLS_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_GLS_INVALID_ZONE', 'Malheureusement il n\'est pas possible d\'envoyer dans ce pays');
define('MODULE_SHIPPING_GLS_UNDEFINED_RATE', 'Les frais de port ne peuvent pas être caluclés pour le moment');

define('MODULE_SHIPPING_GLS_STATUS_TITLE' , 'GLS');
define('MODULE_SHIPPING_GLS_STATUS_DESC' , 'Voulez-vous proposer l\'envoi par GLS?');
define('MODULE_SHIPPING_GLS_HANDLING_TITLE' , 'Frais de traitement');
define('MODULE_SHIPPING_GLS_HANDLING_DESC' , 'Frais de traitement pour ce type d\'envoi en Euro');
define('MODULE_SHIPPING_GLS_TAX_CLASS_TITLE' , 'Taux d\'imposition');
define('MODULE_SHIPPING_GLS_TAX_CLASS_DESC' , 'Veuillez séléctionner le taux de TVA pour ce type d\'envoi');
define('MODULE_SHIPPING_GLS_ZONE_TITLE' , 'Zone d\'envoi');
define('MODULE_SHIPPING_GLS_ZONE_DESC' , 'Si vous choisissez une zone ce type d\'envoi est seulement porposé dans cette zone.');
define('MODULE_SHIPPING_GLS_SORT_ORDER_TITLE' , 'Ordre d\'affichage');
define('MODULE_SHIPPING_GLS_SORT_ORDER_DESC' , 'Les plus bas sont affichés d\'abord.');
define('MODULE_SHIPPING_GLS_ALLOWED_TITLE' , 'Zones d\'envoi individuelles');
define('MODULE_SHIPPING_GLS_ALLOWED_DESC' , 'Veuillez indiquer les zones <b>individuellement</b> pour lesquelles l\'envoi est possible. Par exemple AT,DE');

define('MODULE_SHIPPING_GLS_COUNTRIES_1_TITLE' , 'GLS Zone 1 Pays');
define('MODULE_SHIPPING_GLS_COUNTRIES_1_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 1 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_GLS_COST_1_TITLE' , 'GLS Zone 1 Tableau de frais de port');
define('MODULE_SHIPPING_GLS_COST_1_DESC' , 'Frais de port pour les pays en zone 1 basées sur l\'indication du poids (de-à) de la commande. Exemple: 0-3:8.50,3-7:10.50 etc. Les poids plus grand que 0 et moins ou égal à 3 coûteraient 8.50 pour les pays de la  zone 1.');

define('MODULE_SHIPPING_GLS_COUNTRIES_2_TITLE' , 'GLS Zone 2 Pays');
define('MODULE_SHIPPING_GLS_COUNTRIES_2_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 2 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_GLS_COST_2_TITLE' , 'GLS Zone 2 Tableau de frais de port');
define('MODULE_SHIPPING_GLS_COST_2_DESC' , 'Frais de port pour les pays en zone 1 basées sur l\'indication du poids (de-à) de la commande. Exemple: 0-3:8.50,3-7:10.50 etc. Les poids plus grand que 0 et moins ou égal à 3 coûteraient 8.50 pour les pays de la  zone 1.');

define('MODULE_SHIPPING_GLS_COUNTRIES_3_TITLE' , 'GLS Zone 3 Pays');
define('MODULE_SHIPPING_GLS_COUNTRIES_3_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 3 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_GLS_COST_3_TITLE' , 'GLS Zone 3 Tableau de frais de port');
define('MODULE_SHIPPING_GLS_COST_3_DESC' , 'Frais de port pour les pays en zone 1 basées sur l\'indication du poids (de-à) de la commande. Exemple: 0-3:8.50,3-7:10.50 etc. Les poids plus grand que 0 et moins ou égal à 3 coûteraient 8.50 pour les pays de la  zone 1.');

define('MODULE_SHIPPING_GLS_COUNTRIES_4_TITLE' , 'GLS Zone 4 Pays');
define('MODULE_SHIPPING_GLS_COUNTRIES_4_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 4 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_GLS_COST_4_TITLE' , 'GLS Zone 4 Tableau de frais de port');
define('MODULE_SHIPPING_GLS_COST_4_DESC' , 'Frais de port pour les pays en zone 1 basées sur l\'indication du poids (de-à) de la commande. Exemple: 0-3:8.50,3-7:10.50 etc. Les poids plus grand que 0 et moins ou égal à 3 coûteraient 8.50 pour les pays de la  zone 1.');

define('MODULE_SHIPPING_GLS_COUNTRIES_5_TITLE' , 'GLS Zone 5 Pays');
define('MODULE_SHIPPING_GLS_COUNTRIES_5_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 5 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_GLS_COST_5_TITLE' , 'GLS Zone 5 Tableau de frais de port');
define('MODULE_SHIPPING_GLS_COST_5_DESC' , 'Frais de port pour les pays en zone 1 basées sur l\'indication du poids (de-à) de la commande. Exemple: 0-3:8.50,3-7:10.50 etc. Les poids plus grand que 0 et moins ou égal à 3 coûteraient 8.50 pour les pays de la  zone 1.');

define('MODULE_SHIPPING_GLS_COUNTRIES_6_TITLE' , 'GLS Zone 6 Pays');
define('MODULE_SHIPPING_GLS_COUNTRIES_6_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone 6 (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_GLS_COST_6_TITLE' , 'GLS Zone 6 Tableau de frais de port');
define('MODULE_SHIPPING_GLS_COST_6_DESC' , 'Frais de port pour les pays en zone 1 basées sur l\'indication du poids (de-à) de la commande. Exemple: 0-3:8.50,3-7:10.50 etc. Les poids plus grand que 0 et moins ou égal à 3 coûteraient 8.50 pour les pays de la  zone 1.');

define('MODULE_SHIPPING_GLS_POSTCODE_TITLE' , 'GLS suppléments insulaires - codes postaux');
define('MODULE_SHIPPING_GLS_POSTCODE_DESC' , 'Zones de codes postaux');
define('MODULE_SHIPPING_GLS_POSTCODE_EXTRA_COST_TITLE' , 'GLS suppléments insulaires - frais');
define('MODULE_SHIPPING_GLS_POSTCODE_EXTRA_COST_DESC' , 'Suppléments insulaires : Veuillez indiquer de combien les frais de port augmentent si l\'adresse de livraison se trouve sur une île allemande.');
?>

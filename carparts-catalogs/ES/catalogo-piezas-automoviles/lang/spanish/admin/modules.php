<?php
/* --------------------------------------------------------------
   $Id: modules.php 2957 2012-05-31 11:55:56Z Tomcraft1980 $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(modules.php,v 1.8 2002/04/09); www.oscommerce.com
   (c) 2003 nextcommerce (modules.php,v 1.5 2003/08/14); www.nextcommerce.org
   (c) 2006 XT-Commerce (modules.php 899 2005-04-29)

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE_MODULES_PAYMENT', 'Modalidades de pago');
define('HEADING_TITLE_MODULES_SHIPPING', 'Modos de envío');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Order Total Modul');

define('TABLE_HEADING_MODULES', 'Módulos');
define('TABLE_HEADING_SORT_ORDER', 'Orden de clasificación');
define('TABLE_HEADING_STATUS', 'Estado');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_MODULE_DIRECTORY', 'Directorio del módulo:');
define('TEXT_MODULE_FILE_MISSING', '<b>¡Falta el archivo de idioma "%s", no se muestra el módulo "%s"!</b>');
define('TABLE_HEADING_FILENAME', 'Nombre del módulo (para uso interno)');

// BOF - Tomcraft - 2009-10-03 - Paypal Express Modul
define('TEXT_INFO_DELETE_PAYPAL', '¡Si desinstala este módulo ahora, los datos de transacción de PayPal se eliminarán! <br />Si desea recibir estos datos, pulse Cancelar y desactive el módulo solamente. (Activar módulo = False)');
// EOF - Tomcraft - 2009-10-03 - Paypal Express Modul
define('TABLE_HEADING_MODULES_INSTALLED', 'Se han instalado los siguientes módulos');
define('TABLE_HEADING_MODULES_PREFERRED', 'Módulos populares');
define('TABLE_HEADING_MODULES_NOT_INSTALLED', 'Los siguientes módulos todavía están disponibles');
define('TEXT_MODULE_UPDATE_NEEDED', 'Los siguientes módulos han sido actualizados y requieren una actualización de la base de datos. Para aquello, por favor, guarde las configuraciones y reinstale estos módulos.');
?>

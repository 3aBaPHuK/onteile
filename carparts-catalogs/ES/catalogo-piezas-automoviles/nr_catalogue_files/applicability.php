﻿<?php
if(!isset($_SESSION['customer_id'])) xtc_redirect(xtc_href_link(FILENAME_DEFAULT)); //redirect to start page if not logged in, noRiddle

// Include soap request class
include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'requestOem.php');
// Include catalog list view
include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'render'.DIRECTORY_SEPARATOR.'applicability'.DIRECTORY_SEPARATOR.'applicability.php');

include('nr_catalogue_files/extender.php');

class CatalogExtender extends CommonExtender
{
    function FormatLink($type, $dataItem, $catalog, $renderer)
    {
        if ($type == 'unit')
            //BOC set link to content 1006 unit.php, noRiddle
            //return 'unit.php?&uid='.$dataItem['unitid'].'&c='.$catalog.'&ssd='.$dataItem['ssd'].'&oem='.$renderer->oem;
            return xtc_href_link(FILENAME_CONTENT, 'coID=1006').'?&uid='.$dataItem['unitid'].'&c='.$catalog.'&ssd='.$dataItem['ssd'].'&oem='.$renderer->oem;
            //BOC set link to content 1006 unit.php, noRiddle

        //BOC set link to content 1008 applicability.php, noRiddle
        //return 'applicability.php?&oem='.$dataItem['oem'].'&brand='.$dataItem['brand'];
        return xtc_href_link(FILENAME_CONTENT, 'coID=1008').'?&oem='.$dataItem['oem'].'&brand='.$dataItem['brand'];
        //EOC set link to content 1008 applicability.php, noRiddle
    }
}

$brand = $_GET['brand'];
$oem = $_GET['oem'];

// Create request object
$request = new GuayaquilRequestOEM('', '', Config::$catalog_data);
if (Config::$useLoginAuthorizationMethod) {
    $request->setUserAuthorizationMethod(Config::$userLogin, Config::$userKey);
}

// Append commands to request
$request->appendFindDetailApplicability($oem, $brand);

// Execute request
$data = $request->query();

//echo '<pre>'; print_r($data); echo '</pre>';
// Check errors
if ($request->error != '')
{
    echo $request->error;
}
else
{
    // Create GuayaquilCatalogsList object. This class implements default catalogs list view
    $renderer = new GuayaquilApplicability(new CatalogExtender(), $oem);
    $renderer->columns = array('name', 'date', 'datefrom', 'dateto', 'model', 'framecolor', 'trimcolor', 'modification', 'grade', 'frame', 'engine', 'engineno', 'transmission', 'doors', 'manufactured', 'options', 'creationregion', 'destinationregion', 'description', 'remarks');

    // Draw catalogs list
    echo $renderer->Draw($data[0]);
}
?>
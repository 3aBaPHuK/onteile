<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ptebanktransfer.php,v 1.4.1 2003/09/25 19:57:14); www.oscommerce.com
   (c) 2003 xtCommerce www.xt-commerce.com

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_TEXT_TITLE', 'Transferencia bancaria estándar de la UE');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_TEXT_DESCRIPTION', 
          '<br />El método de pago más barato y sencillo dentro de la UE es el IBAN y el BIC.'.
					'<br />Por favor, utilice los siguientes datos para transferir la cantidad total:<br />' .
          '<br />Nombre del banco: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKNAM') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKNAM : '') .
          '<br />Destinatario: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_BRANCH') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_BRANCH : '') .
          '<br />Código bancario: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNAM') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNAM : '') .
          '<br />Número de cuenta: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNUM') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNUM : '') .
          '<br />IBAN: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCIBAN') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCIBAN : '') .
          '<br />BIC/SWIFT: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKBIC') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKBIC : '') .
          '<br /><br />La mercancía recién se entregará cuando se haya abonado el importe en nuestra cuenta.<br />');

  if (MODULE_PAYMENT_EUSTANDARDTRANSFER_SUCCESS == 'True') {
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_TEXT_INFO', 'Por favor, transfiera el importe de factura debido a nuestra cuenta. Recibirá los datos de la cuenta en el último paso del pedido.');
  } else {
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_TEXT_INFO', 'Por favor, transfiera el importe de factura debido a nuestra cuenta. Recibirá los datos de la cuenta por correo electrónico después de la aceptación de su pedido.');
  }
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_STATUS_TITLE', 'Activar el módulo de transferencia bancaria estándar de la UE');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_STATUS_DESC', '¿Desea aceptar transferencias bancarias?');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BRANCH_TITLE', 'Destinatario');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BRANCH_DESC', 'El destinatario de la transferencia bancaria.');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKNAM_TITLE', 'Nombre del banco');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKNAM_DESC', 'El nombre completo del banco');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNAM_TITLE', 'Código bancario');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNAM_DESC', 'El código bancario de la cuenta especificada.');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNUM_TITLE', 'Número de cuenta');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNUM_DESC', 'Su número de cuenta');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCIBAN_TITLE', 'Cuenta bancaria IBAN');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCIBAN_DESC', 'ID Internacional del banco.<br />(Pregunte a su banco si no está seguro.)');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKBIC_TITLE', 'BIC del banco');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKBIC_DESC', 'ID Internacional del banco.<br />(Pregunte a su banco si no está seguro.)');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_SORT_ORDER_TITLE', 'Orden de visualización');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_SORT_ORDER_DESC', 'Orden de la visualización. Primero se muestra el dígito más pequeño.');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ALLOWED_TITLE' , 'Zonas permititdas');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ALLOWED_DESC' , 'Introduzca <b>individualmente</b>  las zonas permitidas para este módulo. (por ejemplo AT,DE (si está vacío, se permiten todas las zonas))');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ZONE_TITLE' , 'Zona de pago');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ZONE_DESC' , 'Si se selecciona una zona, el método de pago se aplica sólo a esa zona.');
  
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ORDER_STATUS_ID_TITLE' , 'Determinar estado del pedido');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ORDER_STATUS_ID_DESC' , 'Cambiar los pedidos realizados con este módulo a este estado');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_SUCCESS_TITLE' , 'Mostrar datos bancarios');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_SUCCESS_DESC' , '¿Deben mostrarse los datos bancarios en la página de éxito?');
?>

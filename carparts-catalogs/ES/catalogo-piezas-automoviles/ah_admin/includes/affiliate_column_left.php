<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_column_left.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   
   adapted to modified 2.0.2.2, 07-2017, noRiddle
   ---------------------------------------------------------------------------*/
   
echo ('<li>');  
  echo ('<div class="dataNavHeadingContent"><a href="#"><strong>'.BOX_HEADING_AFFILIATE.'</strong></a></div>');
echo ('<ul>');
if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['configuration'] == '1')) echo '<li><a id="BOX_AFFILIATE_CONFIGURATION" href="' . xtc_href_link(FILENAME_CONFIGURATION, 'gID=900', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_AFFILIATE_CONFIGURATION . '</a></li>';
if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['affiliate_affiliates'] == '1')) echo '<li><a id="BOX_AFFILIATE" href="' . xtc_href_link(FILENAME_AFFILIATE, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_AFFILIATE . '</a></li>';
if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['affiliate_banners'] == '1')) echo '<li><a id="BOX_AFFILIATE_BANNERS" href="' . xtc_href_link(FILENAME_AFFILIATE_BANNERS, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_AFFILIATE_BANNERS . '</a></li>';
if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['affiliate_textlinks'] == '1')) echo '<li><a id="BOX_AFFILIATE_TEXTLINKS" href="' . xtc_href_link(FILENAME_AFFILIATE_TEXTLINKS, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_AFFILIATE_TEXTLINKS . '</a></li>';
if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['affiliate_clicks'] == '1')) echo '<li><a id="BOX_AFFILIATE_CLICKS" href="' . xtc_href_link(FILENAME_AFFILIATE_CLICKS, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_AFFILIATE_CLICKS . '</a></li>';
if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['affiliate_contact'] == '1')) echo '<li><a id="BOX_AFFILIATE_CONTACT" href="' . xtc_href_link(FILENAME_AFFILIATE_CONTACT, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_AFFILIATE_CONTACT . '</a></li>';
if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['affiliate_payment'] == '1')) echo '<li><a id="BOX_AFFILIATE_PAYMENT" href="' . xtc_href_link(FILENAME_AFFILIATE_PAYMENT, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_AFFILIATE_PAYMENT . '</a></li>';
if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['affiliate_sales'] == '1')) echo '<li><a id="BOX_AFFILIATE_SALES" href="' . xtc_href_link(FILENAME_AFFILIATE_SALES, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_AFFILIATE_SALES . '</a></li>';
if (($_SESSION['customers_status']['customers_status_id'] == '0') && ($admin_access['affiliate_summary'] == '1')) echo '<li><a id="BOX_AFFILIATE_SUMMARY" href="' . xtc_href_link(FILENAME_AFFILIATE_SUMMARY, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_AFFILIATE_SUMMARY . '</a></li>';
echo ('</ul>');
echo ('</li>');
?>
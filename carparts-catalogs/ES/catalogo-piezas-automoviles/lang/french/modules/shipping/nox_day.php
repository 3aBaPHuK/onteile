<?php
/*******************************************************************
* shipping module: nox_day
*
*
*
* (c) noRiddle 01-2018
*******************************************************************/

define('MODULE_SHIPPING_NOX_DAY_TEXT_TITLE', 'TAGEXPRESS tax (without possible bulk goods costs <a href="'.xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=12006').'" title="Info about TAGEXPRESS" target="_blank" class="iframe"><i class="fa fa-info-circle or-ic"></i></a>)');
define('MODULE_SHIPPING_NOX_DAY_TEXT_DESCRIPTION', 'TAGEXPRESS tax');
define('MODULE_SHIPPING_NOX_DAY_TEXT_WAY', 'Best Way (%01.2f kg)');
define('MODULE_SHIPPING_NOX_DAY_TEXT_WEIGHT', 'Weight');
define('MODULE_SHIPPING_NOX_DAY_TEXT_AMOUNT', 'Amount');
define('MODULE_SHIPPING_NOX_DAY_INVALID_ZONE', 'No shipping available to the selected country!');
define('MODULE_SHIPPING_NOX_DAY_UNDEFINED_RATE', 'The shipping rate cannot be determined at this time.');

define('MODULE_SHIPPING_NOX_DAY_STATUS_TITLE' , 'Enable TAGEXPRESS tax shipping method');
define('MODULE_SHIPPING_NOX_DAY_STATUS_DESC' , 'Do you want to offer TAGEXPRESS tax rate shipping?');
define('MODULE_SHIPPING_NOX_DAY_ALLOWED_TITLE' , 'Allowed Zones');
define('MODULE_SHIPPING_NOX_DAY_ALLOWED_DESC' , 'Please enter the zones <b>separately</b> which should be allowed to use this modul (e. g. AT,DE (leave empty if you want to allow all zones))');
define('MODULE_SHIPPING_NOX_DAY_CUST_STATS_TITLE', 'Allowed customer groups');
define('MODULE_SHIPPING_NOX_DAY_CUST_STATS_DESC', 'Please enter Ids of customer groups comma-separated.');
define('MODULE_SHIPPING_NOX_DAY_MODE_TITLE' , 'Shipping method');
define('MODULE_SHIPPING_NOX_DAY_MODE_DESC' , 'The shipping cost is based on the order total or the total weight of the items ordered.');
define('MODULE_SHIPPING_NOX_DAY_TAX_CLASS_TITLE' , 'Tax Class');
define('MODULE_SHIPPING_NOX_DAY_TAX_CLASS_DESC' , 'Use the following tax class on the shipping fee.');
define('MODULE_SHIPPING_NOX_DAY_ZONE_TITLE' , 'Shipping Zone');
define('MODULE_SHIPPING_NOX_DAY_ZONE_DESC' , 'If a zone is selected, only enable this shipping method for that zone.');
define('MODULE_SHIPPING_NOX_DAY_SORT_ORDER_TITLE' , 'Sort Order');
define('MODULE_SHIPPING_NOX_DAY_SORT_ORDER_DESC' , 'Sort order of display.');
define('MODULE_SHIPPING_NOX_DAY_NUMBER_ZONES_TITLE' , 'Number of zones');
define('MODULE_SHIPPING_NOX_DAY_NUMBER_ZONES_DESC' , 'Number of zones to use');
define('MODULE_SHIPPING_NOX_DAY_DISPLAY_TITLE' , 'Enable Display');
define('MODULE_SHIPPING_NOX_DAY_DISPLAY_DESC' , 'Do you want to display, if shipping to destination is not possible or if shipping costs cannot be calculated?');

for ($module_shipping_nox_day_i = 1; $module_shipping_nox_day_i <= MODULE_SHIPPING_NOX_DAY_NUMBER_ZONES; $module_shipping_nox_day_i ++) {
  define('MODULE_SHIPPING_NOX_DAY_COUNTRIES_'.$module_shipping_nox_day_i.'_TITLE' , '<hr/>Zone '.$module_shipping_nox_day_i.' Countries');
  define('MODULE_SHIPPING_NOX_DAY_COUNTRIES_'.$module_shipping_nox_day_i.'_DESC' , 'Comma separated list of two character ISO country codes that are part of Zone '.$module_shipping_nox_day_i.' (Enter WORLD for the rest of the world.).');
  define('MODULE_SHIPPING_NOX_DAY_COST_'.$module_shipping_nox_day_i.'_TITLE' , 'Zone '.$module_shipping_nox_day_i.' Shipping Table');
  define('MODULE_SHIPPING_NOX_DAY_COST_'.$module_shipping_nox_day_i.'_DESC' , 'Shipping rates to Zone '.$module_shipping_nox_day_i.' destinations based on a group of maximum order weights or order total. Example: 3:8.50,7:10.50,... Weights/Total less than or equal to 3 would cost 8.50 for Zone '.$module_shipping_nox_day_i.' destinations.');
  define('MODULE_SHIPPING_NOX_DAY_HANDLING_'.$module_shipping_nox_day_i.'_TITLE' , 'Zone '.$module_shipping_nox_day_i.' Handling Fee');
  define('MODULE_SHIPPING_NOX_DAY_HANDLING_'.$module_shipping_nox_day_i.'_DESC' , 'Handling Fee for this shipping zone');
  //BOC new field for shippingtime, noRiddle
  define('MODULE_SHIPPING_NOX_DAY_SHIPPINGTIME_'.$module_shipping_nox_day_i.'_TITLE' , 'Zone '.$module_shipping_nox_day_i.' Delivery time');
  define('MODULE_SHIPPING_NOX_DAY_SHIPPINGTIME_'.$module_shipping_nox_day_i.'_DESC' , 'Indicate the delivery time for this zone. Beispiel: 2-3 Tage');
  //EOC new field for shippingtime, noRiddle
  //BOC new flat costs depending on order_total amount, noRiddle 
  define('MODULE_SHIPPING_NOX_DAY_FLAT_'.$module_shipping_nox_day_i.'_TITLE' , 'Zone '.$module_shipping_nox_day_i.' flat costs');
  define('MODULE_SHIPPING_NOX_DAY_FLAT_'.$module_shipping_nox_day_i.'_DESC' , 'The flat costs base on the total amount of the order.<br />Example: 25:5.50,50:8.50,etc..<br />From 25 EUR 5.50 EUR will be charged, etc.');
  //EOC new flat costs depending on order_total amount, noRiddle
}
define('MODULE_SHIPPING_NOX_DAY_SHIPPING_INFO', 'Delivery time'); //new constant for delivery time, noRiddle
define('MODULE_SHIPPING_NOX_DAY_SHIPPING_INFO_UNITS', 'working day'); //new constant for delivery time, noRiddle
define('MODULE_SHIPPING_NOX_DAY_SHIPPING_INFO_UNITP', 'working days'); //new constant for delivery time, noRiddle
?>

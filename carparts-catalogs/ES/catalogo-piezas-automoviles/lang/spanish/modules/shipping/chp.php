<?php
/* -----------------------------------------------------------------------------------------
   $Id: chp.php 5123 2013-07-18 11:49:11Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(chp.php,v 1.01 2003/02/18 03:30:00); www.oscommerce.com 
   (c) 2003	 nextcommerce (chp.php,v 1.4 2003/08/1); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   swiss_post_1.02       	Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/


define('MODULE_SHIPPING_CHP_TEXT_TITLE', 'Correos suizos');
define('MODULE_SHIPPING_CHP_TEXT_DESCRIPTION', 'Los Correos suizos');
define('MODULE_SHIPPING_CHP_TEXT_WAY', ' Envíos a ');
define('MODULE_SHIPPING_CHP_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_CHP_INVALID_ZONE', 'No es posible realizar un envío a este país.');
define('MODULE_SHIPPING_CHP_UNDEFINED_RATE', 'Los costos de envío no se pueden calcular por el momento.');

define('MODULE_SHIPPING_CHP_STATUS_TITLE' , 'Correos suizos');
define('MODULE_SHIPPING_CHP_STATUS_DESC' , '¿Desea ofrecer envíos a través de Correos suizos?');
define('MODULE_SHIPPING_CHP_HANDLING_TITLE' , 'Tarifa de manejo');
define('MODULE_SHIPPING_CHP_HANDLING_DESC' , 'Costo de procesamiento para este modo de envío en CHF');
define('MODULE_SHIPPING_CHP_TAX_CLASS_TITLE' , 'Tasa de impuesto');
define('MODULE_SHIPPING_CHP_TAX_CLASS_DESC' , 'Seleccione el tipo de IVA para este modo de envío.');
define('MODULE_SHIPPING_CHP_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_CHP_ZONE_DESC' , 'Si selecciona una zona, este modo de envío sólo se ofrecerá en esa zona.');
define('MODULE_SHIPPING_CHP_SORT_ORDER_TITLE' , 'Orden de la visualización');
define('MODULE_SHIPPING_CHP_SORT_ORDER_DESC' , 'Menor se muestra en primer lugar.');
define('MODULE_SHIPPING_CHP_ALLOWED_TITLE' , 'Zonas de envío individuales');
define('MODULE_SHIPPING_CHP_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE');

define('MODULE_SHIPPING_CHP_COUNTRIES_1_TITLE' , 'Países de la zona tarifaria 0');
define('MODULE_SHIPPING_CHP_COUNTRIES_1_DESC' , 'Zona doméstica');
define('MODULE_SHIPPING_CHP_COST_ECO_1_TITLE' , 'Tabla de tarifas para zona 0 hasta 30 kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_1_DESC' , 'Tabla de tarifas para la zona doméstica, basada en  <b>\'ECO\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_PRI_1_TITLE' , 'Tabla de tarifas para zona 0 hasta 30 kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_1_DESC' , 'Tabla de tarifas para la zona doméstica, basada en  <b>\'PRI\'</b>  hasta 30 kg de peso de envío.');

define('MODULE_SHIPPING_CHP_COUNTRIES_2_TITLE' , 'Países de la zona tarifaria 1');
define('MODULE_SHIPPING_CHP_COUNTRIES_2_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona 1 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_CHP_COST_ECO_2_TITLE' , 'Tabla de tarifas para zona 1 hasta 30 kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_2_DESC' , 'Tabla de tarifas para la zona 1, basada en  <b>\'ECO\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_PRI_2_TITLE' , 'Tabla de tarifas para zona 1 hasta 30 kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_2_DESC' , 'Tabla de tarifas para la zona 1, basada en  <b>\'PRI\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_URG_2_TITLE' , 'Tabla de tarifas para zona 1 hasta 30 kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_2_DESC' , 'Tabla de tarifas para la zona 1, basada en  <b>\'URG\'</b>  hasta 30 kg de peso de envío.');

define('MODULE_SHIPPING_CHP_COUNTRIES_3_TITLE' , 'Países de la zona tarifaria 2');
define('MODULE_SHIPPING_CHP_COUNTRIES_3_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona 2 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_CHP_COST_ECO_3_TITLE' , 'Tabla de tarifas para zona 2 hasta 30 kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_3_DESC' , 'Tabla de tarifas para la zona 2, basada en  <b>\'ECO\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_PRI_3_TITLE' , 'Tabla de tarifas para zona 2 hasta 30 kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_3_DESC' , 'Tabla de tarifas para la zona 2, basada en  <b>\'PRI\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_URG_3_TITLE' , 'Tabla de tarifas para zona 2 hasta 30 kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_3_DESC' , 'Tabla de tarifas para la zona 2, basada en  <b>\'URG\'</b>  hasta 30 kg de peso de envío.');

define('MODULE_SHIPPING_CHP_COUNTRIES_4_TITLE' , 'Países de la zona tarifaria 3');
define('MODULE_SHIPPING_CHP_COUNTRIES_4_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona 3 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_CHP_COST_ECO_4_TITLE' , 'Tabla de tarifas para zona 3 hasta 30 kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_4_DESC' , 'Tabla de tarifas para la zona 3, basada en  <b>\'ECO\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_PRI_4_TITLE' , 'Tabla de tarifas para zona 3 hasta 30 kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_4_DESC' , 'Tabla de tarifas para la zona 3, basada en  <b>\'PRI\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_URG_4_TITLE' , 'Tabla de tarifas para zona 3 hasta 30 kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_4_DESC' , 'Tabla de tarifas para la zona 3, basada en  <b>\'URG\'</b>  hasta 30 kg de peso de envío.');

define('MODULE_SHIPPING_CHP_COUNTRIES_5_TITLE' , 'Países de la zona tarifaria 4');
define('MODULE_SHIPPING_CHP_COUNTRIES_5_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona 4 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_CHP_COST_ECO_5_TITLE' , 'Tabla de tarifas para zona 4 hasta 30 kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_5_DESC' , 'Tabla de tarifas para la zona 4, basada en  <b>\'ECO\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_PRI_5_TITLE' , 'Tabla de tarifas para zona 4 hasta 30 kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_5_DESC' , 'Tabla de tarifas para la zona 4, basada en  <b>\'PRI\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_URG_5_TITLE' , 'Tabla de tarifas para zona 4 hasta 30 kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_5_DESC' , 'Tabla de tarifas para la zona 4, basada en  <b>\'URG\'</b>  hasta 30 kg de peso de envío.');

define('MODULE_SHIPPING_CHP_COUNTRIES_6_TITLE' , 'Países de la zona tarifaria 4');
define('MODULE_SHIPPING_CHP_COUNTRIES_6_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona 4 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_CHP_COST_ECO_6_TITLE' , 'Tabla de tarifas para zona 4 hasta 30 kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_6_DESC' , 'Tabla de tarifas para la zona 4, basada en  <b>\'ECO\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_PRI_6_TITLE' , 'Tabla de tarifas para zona 4 hasta 30 kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_6_DESC' , 'Tabla de tarifas para la zona 4, basada en  <b>\'PRI\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_URG_6_TITLE' , 'Tabla de tarifas para zona 4 hasta 30 kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_6_DESC' , 'Tabla de tarifas para la zona 4, basada en  <b>\'URG\'</b>  hasta 30 kg de peso de envío.');

define('MODULE_SHIPPING_CHP_COUNTRIES_7_TITLE' , 'Países de la zona tarifaria 5');
define('MODULE_SHIPPING_CHP_COUNTRIES_7_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona 5 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_CHP_COST_ECO_7_TITLE' , 'Tabla de tarifas para zona 5 hasta 30 kg ECO');
define('MODULE_SHIPPING_CHP_COST_ECO_7_DESC' , 'Tabla de tarifas para la zona 5, basada en  <b>\'ECO\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_PRI_7_TITLE' , 'Tabla de tarifas para zona 5 hasta 30 kg PRI');
define('MODULE_SHIPPING_CHP_COST_PRI_7_DESC' , 'Tabla de tarifas para la zona 5, basada en  <b>\'PRI\'</b>  hasta 30 kg de peso de envío.');
define('MODULE_SHIPPING_CHP_COST_URG_7_TITLE' , 'Tabla de tarifas para zona 5 hasta 30 kg URG');
define('MODULE_SHIPPING_CHP_COST_URG_7_DESC' , 'Tabla de tarifas para la zona 5, basada en  <b>\'URG\'</b>  hasta 30 kg de peso de envío.');
?>

<?php
/* --------------------------------------------------------------
   $Id: buttons.php 10580 2017-01-19 13:16:33Z GTB $


   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(categories.php,v 1.22 2002/08/17); www.oscommerce.com 
   (c) 2003	 nextcommerce (categories.php,v 1.10 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

// buttons
define('BUTTON_REVERSE_SELECTION', 'Inverser la sélection');
define('BUTTON_SWITCH_PRODUCTS', 'Seulement des produits');
define('BUTTON_SWITCH_CATEGORIES', 'Seulement des catégories');
define('BUTTON_NEW_CATEGORIES', 'Nouvelle catégorie');
define('BUTTON_NEW_PRODUCTS', 'Nouveau prodit');
define('BUTTON_COPY', 'Copier');
define('BUTTON_BACK', 'En arrière');
define('BUTTON_CANCEL', 'Annuler');
define('BUTTON_EDIT', 'Modifier');
define('BUTTON_DELETE', 'Supprimer');
define('BUTTON_MOVE', 'Déplacer');
define('BUTTON_SAVE', 'Sauvegarder');
define('BUTTON_STATUS_ON', 'Statut actif');
define('BUTTON_STATUS_OFF', 'Statut inactif');
define('BUTTON_EDIT_ATTRIBUTES', 'Éditer les attributs');
define('BUTTON_INSERT', 'Insérer');
define('BUTTON_UPDATE', 'Actualiser');
define('BUTTON_EXPORT', 'Exporter');
define('BUTTON_CURRENCY_UPDATE', 'Actualiser le cours monétaire');
define('BUTTON_REVIEW_APPROVE', 'OK');
define('BUTTON_SEND_EMAIL', 'Envoyer un mail');
define('BUTTON_SEND_COUPON', 'Envoyer un bon');
define('BUTTON_INVOICE', 'Facture');
define('BUTTON_PACKINGSLIP', 'Bon de livraison');
define('BUTTON_AFTERBUY_SEND', 'Afterbuy - envoyer');
define('BUTTON_NEW_NEWSLETTER', 'Nouvelle newsletter');
define('BUTTON_RESET', 'Remise à zero');
define('BUTTON_STATUS', 'Groupe de clients');
define('BUTTON_ACCOUNTING', 'Droits d\'administrateur');
define('BUTTON_ORDERS', 'Commandes');
define('BUTTON_EMAIL', 'Email');
define('BUTTON_IPLOG', 'Log IP');
define('BUTTON_NEW_ORDER', 'Nouvelle commande');
define('BUTTON_CREATE_ACCOUNT', 'Nouveau client');
define('BUTTON_SEARCH', 'Rechercher');
define('BUTTON_PRODUCT_OPTIONS', 'Options de produit');
define('BUTTON_PREVIEW', 'Aperçu');
define('BUTTON_MODULE_REMOVE', 'Désinstaller');
define('BUTTON_MODULE_INSTALL', 'Installer');
define('BUTTON_START', 'Départ');
define('BUTTON_NEW_CONTENT', 'Nouveau contenu');
define('BUTTON_BACKUP', 'Sauvegarde');
define('BUTTON_RESTORE', 'Restaurer');
define('BUTTON_NEW_BANNER', 'Nouvelle bannière');
define('BUTTON_UPLOAD', 'Télécharger');
define('BUTTON_IMPORT', 'Importer');
define('BUTTON_CONFIRM', 'Confirmer');
define('BUTTON_REPORT', 'Report');
define('BUTTON_RELEASE', 'Utiliser');
define('BUTTON_NEW_LANGUAGE', 'Nouvelle langue');
define('BUTTON_NEW_COUNTRY', 'Nouveau pays');
define('BUTTON_NEW_CURRENCY', 'Nouvelle monnaie');
define('BUTTON_NEW_ZONE', 'Nouvelle région');
define('BUTTON_DETAILS', 'Détails');
define('BUTTON_NEW_TAX_CLASS', 'Nouvelle trance d\'imposition');
define('BUTTON_NEW_TAX_RATE', 'Nouveau taux d\'imposition');
define('BUTTON_SEND', 'Envoyer');
define('BUTTON_REVERSE', 'Annuler');

// BOF - Tomcraft - 2009-11-28 - Included xs:booster
define('BUTTON_EDIT_XTBOOSTER', 'Article eBay');
define('BUTTON_XTBOOSTER_MULTI', 'Article multiple eBay');
// EOF - Tomcraft - 2009-11-28 - Included xs:booster

// BOF - Dokuman - 2010-02-04 - delete cache files in admin section
define('BUTTON_DELETE_CACHE', 'Vider le cache');
define('BUTTON_DELETE_TEMP_CACHE', 'Vider cache du modèle');
define('BUTTON_DELETE_LOGS', 'Supprimer les fichiers journals');
// EOF - Dokuman - 2010-02-04 - delete cache files in admin section

// BOF - web28 - 2011-07-13 - New Button
define('BUTTON_VIEW_PRODUCT', 'Regarder l\'article');
//EOF - web28 - 2011-07-13 - New Button

define('BUTTON_VALUES', 'Valeurs');
define('BUTTON_DELETE_BANKTRANSFER', 'Supprimer les données bancaires');
define('BUTTON_BLACKLIST', 'Liste noire');
?>

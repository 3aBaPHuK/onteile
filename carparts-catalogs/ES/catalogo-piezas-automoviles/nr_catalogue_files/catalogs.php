﻿<?php
// Include soap request class
include('nr_catalogue_files/guayaquillib/data/requestOem.php');
include('nr_catalogue_files/extender.php');
// Include catalog list view
//include('guayaquillib/render/catalogs/2coltable.php');
include('nr_catalogue_files/guayaquillib/render/catalogs/nr_cols.php');

class CatalogExtender extends CommonExtender
{
    function FormatLink($type, $dataItem, $catalog, $renderer)
    {
        //BOC build array to link to shop internal catalogue for bmw, mini and rolls-royce
        global $shop_suffix, $int_cat_arr, $remote_url, $cat_env; //added $remote_url and $cat_env, 07-2020, noRiddle
        //BOC build array to link to shop internal catalogue for bmw, mini and rolls-royce
        
        //BOC only logged in customers may see this, noRiddle
        //BOC set link to content 1000 which includes catalog.php, noRiddle
        //$link = 'catalog.php?&c=' . $dataItem['code'] . '&ssd=' . $dataItem['ssd'];
        
        //echo '<pre>'.print_r($_SESSION['nr_ctlg_brands'], true).'</pre>';
        
        //BOC link to content 20 if shop is not yet online, noRiddle
        //$link = xtc_href_link(FILENAME_CONTENT, 'coID=1000') . '?c=' . $dataItem['code'] . '&ssd=' . $dataItem['ssd'];
        if(isset($_SESSION['customer_id'])) {
            //if(Config::$nr_brand_arr[(string)$dataItem['code']] == '') {
                //$link = xtc_href_link(FILENAME_CONTENT, 'coID=20');
            //} else {
                //BOC respect link to shop internal catalogue for bmw, mini and rolls-royce
                if(in_array($_SESSION['nr_ctlg_brands'][(string)$dataItem['code']]['our_brand'], $int_cat_arr)) {
                    //$link = HTTP_SERVER.'/'.$_SESSION['nr_ctlg_brands'][(string)$dataItem['code']]['our_brand'].'-ersatzteile/shop_content.php?coID=14000';
                    $link = (!empty($remote_url) ? $remote_url[$cat_env]['domain'] : '').'/'.$_SESSION['nr_ctlg_brands'][(string)$dataItem['code']]['our_brand'].$shop_suffix.'/shop_content.php?coID=14000'; //added domain for remote shops, 07-2020, noRiddle
                } else {
                    $link = xtc_href_link(FILENAME_CONTENT, 'coID=1000') . '?c=' . $dataItem['code'] . '&ssd=' . $dataItem['ssd'];
                }
                //EOC respect link to shop internal catalogue for bmw, mini and rolls-royce
            //}
        } else {
            //BOC link to content with information about the brand, 08-2017, noRiddle
            //$link = xtc_href_link(FILENAME_LOGIN);
            $link = xtc_href_link(FILENAME_CONTENT, 'coID='.$_SESSION['nr_ctlg_brands'][(string)$dataItem['code']]['coid']);
            //EOC link to content with information about the brand, 08-2017, noRiddle
        }
        //EOC link to content 20 if shop is not yet online, noRiddle
        //EOC set link to content 1000 which includes catalog.php, noRiddle
        //EOC only logged in customers may see this, noRiddle

        //if (CommonExtender::isFeatureSupported($dataItem, 'wizardsearch2'))
        //if (CommonExtender::isFeatureSupported($dataItem, 'wizardsearch2') && Config::$nr_brand_arr[(string)$dataItem['code']] != '' && isset($_SESSION['customer_id'])) //look for not open shops and whether logged in, noRiddle
        if (CommonExtender::isFeatureSupported($dataItem, 'wizardsearch2') && isset($_SESSION['customer_id'])) {
            //respect link to shop internal catalogue for bmw, mini and rolls-royce
            if(!in_array($_SESSION['nr_ctlg_brands'][(string)$dataItem['code']]['our_brand'], $int_cat_arr)) {
                $link .= '&spi2=t';
                //$_SESSION['nr_ctlg_brands'][(string)$dataItem['code']]['have_parame_search'] = 'yes'; //store possibility of search by car parameter in our session defimed in application_top (need that for redirects to vehicles.php if no credits), noRiddle
            }
        }

        return $link;
    }
}

// Create request object
$request = new GuayaquilRequestOEM('', '', Config::$catalog_data);
if (Config::$useLoginAuthorizationMethod) {
    $request->setUserAuthorizationMethod(Config::$userLogin, Config::$userKey);
}

// Append commands to request
$request->appendListCatalogs();

// Execute request
$data = $request->query();

//BOC show array, noRiddle
//echo '<br /><br /><pre>'.print_r($data, true).'</pre>';
//EOC show array, noRiddle

// Check errors
if ($request->error != '') {
    echo $request->error;
} else {
    //BOC let not logged in customer see an advice instead of form, noRiddle
    if(isset($_SESSION['customer_id'])) {
        //echo '<pre>'.$_SESSION['REFERER'].'</pre>';
        if($_SESSION['catalogue_cart'.$_SESSION['shopsess']]->count_all_contents() > 0 && isset($_SESSION['REFERER']) && $_SESSION['REFERER'] == 'login.php') {
            //echo '<pre>'.$_SESSION['REFERER'].'</pre>';
            echo '<div id="main-top cf">';
            echo '<div class="mess-cart">'.CommonExtender::FormatLocalizedString('products_in_cart_after_login').'</div>';
            echo '</div>';
            unset($_SESSION['REFERER']);
        }
        /*echo '<div id="vin-search">';
        $formvin = '';
        $cataloginfo = false;
        include('nr_catalogue_files/forms/vinsearch.php');
        echo '</div>';*/
    } else {
        //echo '<div class="h2cent">'.CommonExtender::FormatLocalizedString('txt_not_looged_in', array(xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=2'), xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=964'))).'</div>';
    }
    //EOC let not logged in customer see an advice instead of form, noRiddle

    // Create GuayaquilCatalogsList object. This class implements default catalogs list view
    $renderer = new GuayaquilCatalogsList(new CatalogExtender());

    //BOC write important data into db, get it and write it in session in /includes/application_top.php, noRiddle
    include('nr_catalogue_files/nr_includes/write_lax_data_in_db.php');
    //EOC write important data into db, get it and write it in session in /includes/application_top.php, noRiddle

    // Configure columns
    $renderer->columns = array('icon', 'name', 'version');

    // Draw catalogs list
    echo $renderer->Draw($data[0]);
}
?>
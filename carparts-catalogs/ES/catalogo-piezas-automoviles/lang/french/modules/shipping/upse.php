<?php
/* -----------------------------------------------------------------------------------------
   $Id: upse.php 4200 2013-01-10 19:47:11Z Tomcraft1980 $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce( fedexeu.php,v 1.01 2003/02/18 03:25:00); www.oscommerce.com 
   (c) 2003	 nextcommerce (fedexeu.php,v 1.5 2003/08/1); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   fedex_europe_1.02        	Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/



define('MODULE_SHIPPING_UPSE_TEXT_TITLE', 'United Parcel Service Express');
define('MODULE_SHIPPING_UPSE_TEXT_DESCRIPTION', 'United Parcel Service Express - Module d\'expédition');
define('MODULE_SHIPPING_UPSE_TEXT_WAY', 'Expédition à');
define('MODULE_SHIPPING_UPSE_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_UPSE_INVALID_ZONE', 'Malheureusement pas d\'expédition possible à ce pays.');
define('MODULE_SHIPPING_UPSE_UNDEFINED_RATE', 'Les frais d\'expédition ne peuvent pas être calculés pour l\'instant.');

define('MODULE_SHIPPING_UPSE_STATUS_TITLE' , 'UPS Express');
define('MODULE_SHIPPING_UPSE_STATUS_DESC' , 'Voulez vous proposer l\'expédition par UPS Express?');
define('MODULE_SHIPPING_UPSE_HANDLING_TITLE' , 'Surtaxe');
define('MODULE_SHIPPING_UPSE_HANDLING_DESC' , 'Frais de traitement en euro pour ce mode de livraison');
define('MODULE_SHIPPING_UPSE_TAX_CLASS_TITLE' , 'Taux d\'impôt');
define('MODULE_SHIPPING_UPSE_TAX_CLASS_DESC' , 'Choisissez le taux de TVA pour ce mode de livraison.');
define('MODULE_SHIPPING_UPSE_ZONE_TITLE' , 'Zone d\'expédition');
define('MODULE_SHIPPING_UPSE_ZONE_DESC' , 'Si vous choisissez une zone, cette mode de paiement ne sera  que proposé dans cette zone.');
define('MODULE_SHIPPING_UPSE_SORT_ORDER_TITLE' , 'Ordre d\'affichage');
define('MODULE_SHIPPING_UPSE_SORT_ORDER_DESC' , 'Afficher le plus bas en premier.');
define('MODULE_SHIPPING_UPSE_ALLOWED_TITLE' , 'Zones individuelle d\'expéditions');
define('MODULE_SHIPPING_UPSE_ALLOWED_DESC' , 'Entrez <b>séparément</b> les zones dans lesquelles une expédition sera possible, p.ex. : AT,DE.');




/* UPS Express

*/

define('MODULE_SHIPPING_UPSE_COUNTRIES_1_TITLE' , 'Pays pour UPS Express zone 1');
define('MODULE_SHIPPING_UPSE_COUNTRIES_1_DESC' , 'Sigles ISO des pays pour zone 1 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_1_TITLE' , 'Tarifs pour UPS Express Zone 1');
define('MODULE_SHIPPING_UPSE_COST_1_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 1. Exemple: Expédition entre 0 et 0,5kg coûte EUR 22,70 = 0,5:22.7...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_2_TITLE' , 'Pays pour UPS Express zone 2');
define('MODULE_SHIPPING_UPSE_COUNTRIES_2_DESC' , 'Sigles ISO des pays pour zone 2 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_2_TITLE' , 'Tarifs pour UPS Express zone 2');
define('MODULE_SHIPPING_UPSE_COST_2_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 2. Exemple: Expédition entre 0 et 0,5kg coûte EUR 51,55 = 0,5:51.55...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_3_TITLE' , 'Pays pour UPS Express zone 3');
define('MODULE_SHIPPING_UPSE_COUNTRIES_3_DESC' , 'Sigles ISO des pays pour zone 3 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_3_TITLE' , 'Tarifs pour UPS Express zone 3');
define('MODULE_SHIPPING_UPSE_COST_3_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 3. Exemple: Expédition entre 0 et 0,5kg coûte EUR 60,70 = 0,5:60.70...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_4_TITLE' , 'Pays pour UPS Express zone 4');
define('MODULE_SHIPPING_UPSE_COUNTRIES_4_DESC' , 'Sigles ISO des pays pour zone 4 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_4_TITLE' , 'Tarifs pour UPS Express zone 4');
define('MODULE_SHIPPING_UPSE_COST_4_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 4. Exemple: Expédition entre 0 et 0,5kg coûte EUR 66,90 = 0,5:66.90...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_5_TITLE' , 'Pays pour UPS Express zone 41');
define('MODULE_SHIPPING_UPSE_COUNTRIES_5_DESC' , 'Sigles ISO des pays pour zone 41 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_5_TITLE' , 'Tarifs pour UPS Express zone 41');
define('MODULE_SHIPPING_UPSE_COST_5_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 41. Exemple: Expédition entre 0 et 0,5kg coûte EUR 82,10 = 0,5:82.10...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_6_TITLE' , 'Pays pour UPS Express zone 42');
define('MODULE_SHIPPING_UPSE_COUNTRIES_6_DESC' , 'Sigles ISO des pays pour zone 42 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_6_TITLE' , 'Tarifs pour UPS Express zone 42');
define('MODULE_SHIPPING_UPSE_COST_6_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 42. Exemple: Expédition entre 0 et 0,5kg coûte EUR 82,90 = 0,5:82.90...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_7_TITLE' , 'Pays pour UPS Express zone 5');
define('MODULE_SHIPPING_UPSE_COUNTRIES_7_DESC' , 'Sigles ISO des pays pour zone 5 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_7_TITLE' , 'Tarifs pour UPS Express zone 5');
define('MODULE_SHIPPING_UPSE_COST_7_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 5. Exemple: Expédition entre 0 et 0,5kg coûte EUR 59,00 = 0,5:59.00...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_8_TITLE' , 'Pays pour UPS Express zone 6');
define('MODULE_SHIPPING_UPSE_COUNTRIES_8_DESC' , 'Sigles ISO des pays pour zone 6 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_8_TITLE' , 'Tarifs pour UPS Express zone 6');
define('MODULE_SHIPPING_UPSE_COST_8_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 6. Exemple: Expédition entre 0 et 0,5kg coûte EUR 84,50 = 0,5:84.50...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_9_TITLE' , 'Pays pour UPS Express zone 7');
define('MODULE_SHIPPING_UPSE_COUNTRIES_9_DESC' , 'Sigles ISO des pays pour zone 7 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_9_TITLE' , 'Tarifs pour UPS Express zone 7');
define('MODULE_SHIPPING_UPSE_COST_9_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 7. Exemple: Expédition entre 0 et 0,5kg coûte EUR 71,85 = 0,5:71.85...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_10_TITLE' , 'Pays pour UPS Express zone 8');
define('MODULE_SHIPPING_UPSE_COUNTRIES_10_DESC' , 'Sigles ISO des pays pour zone 8 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_10_TITLE' , 'Tarifs pour UPS Express zone 8');
define('MODULE_SHIPPING_UPSE_COST_10_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 8. Exemple: Expédition entre 0 et 0,5kg coûte EUR 80,05 = 0,5:80.05...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_11_TITLE' , 'Pays pour UPS Express zone 9');
define('MODULE_SHIPPING_UPSE_COUNTRIES_11_DESC' , 'Sigles ISO des pays pour zone 9 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_11_TITLE' , 'Tarifs pour UPS Express zone 9');
define('MODULE_SHIPPING_UPSE_COST_11_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 9. Exemple: Expédition entre 0 et 0,5kg coûte EUR 85,20 = 0,5:85.20...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_12_TITLE' , 'Pays pour UPS Express zone 10');
define('MODULE_SHIPPING_UPSE_COUNTRIES_12_DESC' , 'Sigles ISO des pays pour zone 10 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_12_TITLE' , 'Tarifs pour UPS Express zone 10');
define('MODULE_SHIPPING_UPSE_COST_12_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 10. Exemple: Expédition entre 0 et 0,5kg coûte EUR 93,10 = 0,5:93.10...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_13_TITLE' , 'Pays pour UPS Express zone 11');
define('MODULE_SHIPPING_UPSE_COUNTRIES_13_DESC' , 'Sigles ISO des pays pour zone 11 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_13_TITLE' , 'Tarifs pour UPS Express zone 11');
define('MODULE_SHIPPING_UPSE_COST_13_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 11. Exemple: Expédition entre 0 et 0,5kg coûte EUR 103.50 = 0,5:103,50...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_14_TITLE' , 'Pays pour UPS Express zone 12');
define('MODULE_SHIPPING_UPSE_COUNTRIES_14_DESC' , 'Sigles ISO des pays pour zone 12 séparés par virgule (Entrez WORLD pour le reste du monde.):');
define('MODULE_SHIPPING_UPSE_COST_14_TITLE' , 'Tarifs pour UPS Express zone 12');
define('MODULE_SHIPPING_UPSE_COST_14_DESC' , 'Frais d\'expédition basés sur le poids dans la zone 12. Exemple: Expédition entre 0 et 0,5kg coûte EUR 105,20 = 0,5:105.20...');
?>

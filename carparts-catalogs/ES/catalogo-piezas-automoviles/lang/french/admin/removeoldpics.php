<?php
/* --------------------------------------------------------------
   $Id: removeoldpics.php 3503 2012-08-23 11:24:07Z dokuman $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(manufacturers.php,v 1.14 2003/02/16); www.oscommerce.com
   (c) 2003 nextcommerce (manufacturers.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Supprimer les vieilles images de l\'article');
define('LINK_INFO_TEXT', '<p>Avec cette fontion vous pouvez supprimer des images inutiles du serveur dans les répértoires suivants:</p>
- /images/product_images/info_images<br/>
- /images/product_images/original_images<br/>
- /images/product_images/popup_images<br/>
- /images/product_images/thumbnail_images<br/>
<p>si elles n\'ont plus de relation à la base de données.<br/> Si une image n\'est plus utilisée par un produit, elle peut être supprimé du serveur sans risque.</p><br/>');
define('LINK_ORIGINAL', 'Supprimer toutes les images originales.');
define('LINK_INFO', 'Supprimer des vieilles images d\'information');
define('LINK_THUMBNAIL', 'Supprimer des vieilles images thumbnail');
define('LINK_POPUP', 'Supprimer des vieilles images popup');
define('LINK_MESSAGE', 'Les images d\'articles inutiles ont été supprimé du répértoire "/images/product_images/%s_images".');
define('LINK_MESSAGE_NO_DELETE', 'Aucune image d\'article inutile a été trouvée dans le répértoire "/images/product_images/%s_images".');
?>

<?php
/*******************************************************
* file: affiliate_module.php
* use: language file for system module
* (c) noRiddle 07-2017
* Released under the GNU General Public License
*******************************************************/

define('MODULE_AFFILIATE_MODULE_TEXT_TITLE', 'Affiliate module');
define('MODULE_AFFILIATE_MODULE_TEXT_DESCRIPTION', 'Makes affilaite module available');
define('MODULE_AFFILIATE_MODULE_STATUS_TITLE', 'Activate Module ?');
define('MODULE_AFFILIATE_MODULE_STATUS_DESC', 'Activate Affiliate module');
?>
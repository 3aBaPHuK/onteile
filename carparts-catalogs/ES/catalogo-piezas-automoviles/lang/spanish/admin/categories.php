<?php
/* --------------------------------------------------------------
   $Id: categories.php 10211 2016-08-07 08:15:31Z GTB $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(categories.php,v 1.22 2002/08/17); www.oscommerce.com 
   (c) 2003	 nextcommerce (categories.php,v 1.10 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/
   
// BOF - Tomcraft - 2009-11-02 - Admin language tabs
define('TEXT_EDIT_STATUS', 'Estado activo');
// BOF - Tomcraft - 2009-11-02 - Admin language tabs
define('HEADING_TITLE', 'Categoría');
define('HEADING_TITLE_SEARCH', 'Búsqueda: ');
define('HEADING_TITLE_GOTO', 'Ir a:');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_CATEGORIES_PRODUCTS', 'Categorías / Artículos');
define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_STATUS', 'Estado');
define('TABLE_HEADING_STARTPAGE', 'Página de inicio');
define('TABLE_HEADING_STOCK', 'Inventario');
define('TABLE_HEADING_SORT', 'Clasif.');
define('TABLE_HEADING_EDIT', 'Editar');
// BOF - Tomcraft - 2010-04-07 - Added definition for products model
define('TABLE_HEADING_PRODUCTS_MODEL', 'Número del Articulo');
// EOF - Tomcraft - 2010-04-07 - Added definition for products model

// BOF - Hendrik - 2010-08-11 - Thumbnails in admin products list
define('TABLE_HEADING_IMAGE', 'Imagen');
// EOF - Hendrik - 2010-08-11 - Thumbnails in admin products list

define('TEXT_ACTIVE_ELEMENT', 'Elemento activo');
define('TEXT_MARKED_ELEMENTS', 'Elementos marcados');
define('TEXT_INFORMATIONS', 'Información');
define('TEXT_INSERT_ELEMENT', 'Elemento nuevo');

define('TEXT_WARN', 'Alerta de inventario:');
define('TEXT_WARN_MAIN', 'Artículo Principal');
define('TEXT_NEW_PRODUCT', 'Nuevo artículo en "%s"');
define('TEXT_EDIT_PRODUCT', 'Editar  artículo en "%s"');
define('TEXT_CATEGORIES', 'Categorías:');
define('TEXT_PRODUCTS', 'Productos:');
define('TEXT_PRODUCTS_PRICE_INFO', 'Precio:');
define('TEXT_PRODUCTS_TAX_CLASS', 'Clase de impuesto:');
define('TEXT_PRODUCTS_AVERAGE_RATING', 'Evaluación promedio:');
define('TEXT_PRODUCTS_QUANTITY_INFO', 'Inventario:');
define('TEXT_PRODUCTS_DISCOUNT_ALLOWED_INFO', 'Descuento máximo permitido:');
define('TEXT_DATE_ADDED', 'Agregado el:');
define('TEXT_DATE_AVAILABLE', 'Fecha de publicación:');
define('TEXT_LAST_MODIFIED', 'Última modificación:');
define('TEXT_IMAGE_NONEXISTENT', 'La imagen no existe');
define('TEXT_NO_CHILD_CATEGORIES_OR_PRODUCTS', 'Por favor, inserte una nueva categoría o artículo en  <strong>%s</strong> .');
define('TEXT_PRODUCT_MORE_INFORMATION', 'Para más información, visite la <a href="http://%s" target="_blank"><u>Página web</u></a> del fabricante.');
define('TEXT_PRODUCT_DATE_ADDED', 'Este artículo ha sido incorporado a nuestro catálogo el %s.');
define('TEXT_PRODUCT_DATE_AVAILABLE', 'Este artículo está disponible a partir del %s.');

define('TEXT_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_EDIT_CATEGORIES_ID', 'ID de Categoría:');
define('TEXT_EDIT_CATEGORIES_NAME', 'Nombre de la categoría:');
define('TEXT_EDIT_CATEGORIES_HEADING_TITLE', 'Título de la categoría:');
define('TEXT_EDIT_CATEGORIES_DESCRIPTION', 'Descripción de la categoría:');
define('TEXT_EDIT_CATEGORIES_IMAGE', 'Imagen de la categoría:');

define('TEXT_EDIT_SORT_ORDER', 'Orden de clasificación:');

define('TEXT_INFO_COPY_TO_INTRO', 'Por favor, seleccione una nueva categoría en la que desea copiar el artículo:');
define('TEXT_INFO_CURRENT_CATEGORIES', 'Las categorías actuales:');

define('TEXT_INFO_HEADING_NEW_CATEGORY', 'Nueva categoría en "%s"');
define('TEXT_INFO_HEADING_EDIT_CATEGORY', 'Editar categoría "%s"');
define('TEXT_INFO_HEADING_DELETE_CATEGORY', 'Eliminar categoría');
define('TEXT_INFO_HEADING_MOVE_CATEGORY', 'Mover categoría');
define('TEXT_INFO_HEADING_DELETE_PRODUCT', 'Eliminar artículo');
define('TEXT_INFO_HEADING_MOVE_PRODUCT', 'Mover artículo');
define('TEXT_INFO_HEADING_COPY_TO', 'Copiar a');
define('TEXT_INFO_HEADING_MOVE_ELEMENTS', 'Mover elementos');
define('TEXT_INFO_HEADING_DELETE_ELEMENTS', 'Eliminar elementos');

define('TEXT_DELETE_CATEGORY_INTRO', '¿Está seguro que quiere borrar esta categoría?');
define('TEXT_DELETE_PRODUCT_INTRO', '¿Está seguro que quiere borrar este artíulo?');

define('TEXT_DELETE_WARNING_CHILDS', '<b>ADVERTENCIA:</b> ¡Existen todavía %s (sub-)categorías, que están vinculados a esta categoría!');
define('TEXT_DELETE_WARNING_PRODUCTS', '<b>ADVERTENCIA:</b> ¡Existen todavía %s productos, que están vinculados a esta categoría!');

define('TEXT_MOVE_WARNING_CHILDS', '<b>Información:</b> ¡Existen todavía %s (sub-)categorías, que están vinculados a esta categoría!');
define('TEXT_MOVE_WARNING_PRODUCTS', '<b>Información:</b> ¡Existen todavía %s productos, que están vinculados a esta categoría!');

define('TEXT_MOVE_PRODUCTS_INTRO', 'Por favor, seleccione una categoría superior en la que desea mover <b>%s</b> ');
define('TEXT_MOVE_CATEGORIES_INTRO', 'Por favor, seleccione una categoría superior en la que desea mover <b>%s</b> ');
define('TEXT_MOVE', 'Mover <b>%s</b> a:');
define('TEXT_MOVE_ALL', 'Mover todo a:');

define('TEXT_NEW_CATEGORY_INTRO', 'Por favor, introduzca la nueva categoría con todos los datos relevantes.');
define('TEXT_CATEGORIES_NAME', 'Nombre de la categoría:');
define('TEXT_CATEGORIES_IMAGE', 'Imagen de la categoría:');

define('TEXT_META_TITLE', 'Meta Title:');
define('TEXT_META_DESCRIPTION', 'Meta Description:');
define('TEXT_META_KEYWORDS', 'Meta Keywords:');

define('TEXT_SORT_ORDER', 'Orden de clasificación:');

define('TEXT_PRODUCTS_STATUS', 'Estado del arctículo:');
define('TEXT_PRODUCTS_STARTPAGE', 'Mostrar en la página de inicio:');
define('TEXT_PRODUCTS_STARTPAGE_YES', 'Sí');
define('TEXT_PRODUCTS_STARTPAGE_NO', 'No');
define('TEXT_PRODUCTS_STARTPAGE_SORT', 'Orden de clasificación (pagina de inicio):');
define('TEXT_PRODUCTS_DATE_AVAILABLE', 'Fecha de publicación:');
// BOF - Hetfield - 2010-01-28 - Changing product available in correctly names for status
define('TEXT_PRODUCT_AVAILABLE', 'Activado');
define('TEXT_PRODUCT_NOT_AVAILABLE', 'Deactivado');
// EOF - Hetfield - 2010-01-28 - Changing product available in correctly names for status
define('TEXT_PRODUCTS_MANUFACTURER', 'Fabricante del artículo:');
define('TEXT_PRODUCTS_MANUFACTURER_MODEL', 'Número de pieza de fabricante (HAN/MPN):');
define('TEXT_PRODUCTS_NAME', 'Nombre del artíulo:');
define('TEXT_PRODUCTS_DESCRIPTION', 'Descripción del artíulo:');
define('TEXT_PRODUCTS_QUANTITY', 'Inventario:');
define('TEXT_PRODUCTS_MODEL', 'Número del articulo:');
define('TEXT_PRODUCTS_IMAGE', 'Imagen del articulo:');
define('TEXT_PRODUCTS_URL', 'Enlace del fabricante');
define('TEXT_PRODUCTS_URL_WITHOUT_HTTP', '<small>(sin llevando http://)</small>');
define('TEXT_PRODUCTS_PRICE', 'Precio del articulo:');
define('TEXT_PRODUCTS_WEIGHT', 'Peso del articulo:');
define('TEXT_PRODUCTS_EAN', 'GTIN/EAN');
define('TEXT_PRODUCT_LINKED_TO', 'Enlazado en:');
define('TEXT_DELETE', 'Borrar');
define('EMPTY_CATEGORY', 'Categoría vacía');

define('TEXT_HOW_TO_COPY', 'Método de copia para artículos:');
define('TEXT_COPY_AS_LINK', 'Enlazar');
define('TEXT_COPY_AS_DUPLICATE', 'Duplicar');

define('ERROR_CANNOT_LINK_TO_SAME_CATEGORY', 'Error: Los artículos no pueden estar enlazados en la misma categoría.');
define('ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Error: El directorio \'images\' en el directorio del catálogo está protegido contra escritura: ' . DIR_FS_CATALOG_IMAGES);
define('ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Error: El directorio \'images\' en el directorio del catálogo es inexistente: ' . DIR_FS_CATALOG_IMAGES);

define('TEXT_PRODUCTS_DISCOUNT_ALLOWED', 'Descuento máximo permitido:');
define('HEADING_PRICES_OPTIONS', '<b>Opciones de precio</b>');
define('HEADING_PRODUCT_IMAGES', '<b>Imágenes del artículo</b>');
define('TEXT_PRODUCTS_WEIGHT_INFO', '<small>(kg)</small>');
define('TEXT_PRODUCTS_SHORT_DESCRIPTION', 'Breve descripción:');
define('TEXT_PRODUCTS_KEYWORDS', 'Términos adicionales para la búsqueda:');
define('TXT_STK', 'Piezas: ');
define('TXT_PRICE', 'a :');
define('TXT_NETTO', 'Precio neto: ');
define('TXT_STAFFELPREIS', 'Precios escalonados');

define('HEADING_PRODUCTS_MEDIA', '<b>Medio de artículo</b>');
define('TABLE_HEADING_PRICE', 'Precio');

define('TEXT_FSK18', 'FSK 18:');
define('TEXT_CHOOSE_INFO_TEMPLATE_CATEGORIE', 'Plantilla para el vista general de categorías');
define('TEXT_CHOOSE_INFO_TEMPLATE_LISTING', 'Plantilla para el vista general de artículos');
// BOF - Tomcraft - 2009-11-02 - Admin language tabs
//define('TEXT_PRODUCTS_SORT','Reihung:');
define('TEXT_PRODUCTS_SORT', 'Orden de clasificación:');
// EOF - Tomcraft - 2009-11-02 - Admin language tabs
define('TEXT_EDIT_PRODUCT_SORT_ORDER', 'Clasificación de artículos');
define('TXT_PRICES', 'Precio');
define('TXT_NAME', 'Nombre del arctículo');
define('TXT_ORDERED', 'Artículos pedidos');
// BOF - Tomcraft - 2009-11-02 - Admin language tabs
define('TXT_SORT', 'Orden de clasificación:');
// EOF - Tomcraft - 2009-11-02 - Admin language tabs
define('TXT_WEIGHT', 'Peso');
define('TXT_QTY', 'En stock');
// BOF - Tomcraft - 2009-09-12 - add option to sort by date and products model
define('TXT_DATE', 'Fecha de lanzamiento');
define('TXT_MODEL', 'Número del artículo');
// EOF - Tomcraft - 2009-09-12 - add option to sort by date and products model

define('TEXT_MULTICOPY', 'Múltiples');
define('TEXT_MULTICOPY_DESC', 'Copiar elementos en las siguientes categorías:<br />(Si se selecciona, se ignoran los ajustes de "Simple".)');
define('TEXT_SINGLECOPY', 'Simple');
define('TEXT_SINGLECOPY_DESC', 'Copiar elementos en las siguientes categorías:<br />(Para aquello no debe ser activado ninguna categoría bajo "Múltiple".)');
define('TEXT_SINGLECOPY_CATEGORY', 'Categoría:');

define('TEXT_PRODUCTS_VPE', 'Unidad de embalaje');
define('TEXT_PRODUCTS_VPE_VISIBLE', 'Mostrar unidad de embalaje');
define('TEXT_PRODUCTS_VPE_VALUE', ' Valor:');

define('CROSS_SELLING_1', 'Cross Selling');
define('CROSS_SELLING_2', 'para artículos');
define('CROSS_SELLING_SEARCH', 'Búsqueda de producto:<br/><small><i>Introducir número del artículo</i></small>');
define('BUTTON_EDIT_CROSS_SELLING', 'Cross Selling');
define('HEADING_DEL', 'Borrar');
define('HEADING_ADD', '¿Añadir?');
define('HEADING_GROUP', 'Grupo');
define('HEADING_SORTING', 'Orden');
define('HEADING_MODEL', 'Número Artículo');
define('HEADING_NAME', 'Artículo');
define('HEADING_CATEGORY', 'Categoría');
define('HEADING_IMAGE', 'Imagen');

// BOF - Tomcraft - 2009-11-02 - Admin language tabs
define('TEXT_SORT_ASC', 'ascendente');
define('TEXT_SORT_DESC', 'descendente');
// EOF - Tomcraft - 2009-11-02 - Admin language tabs

// BOF - Tomcraft - 2009-11-06 - Use variable TEXT_PRODUCTS_DATE_FORMAT
define('TEXT_PRODUCTS_DATE_FORMAT', 'AAAA-MM-DD');
// EOF - Tomcraft - 2009-11-06 - Use variable TEXT_PRODUCTS_DATE_FORMAT

// BOF - web28 - 2010-08-03 - add metatags max charakters info
define('TEXT_CHARACTERS', 'caracteres');
// EOF - web28 - 2010-08-03 - add metatags max charakters info

define('TEXT_ATTRIBUTE_COPY', 'Copiar adjunto atributos de artículo');
define('TEXT_ATTRIBUTE_COPY_INFO', 'Copiar adjunto atributos de artículo<br/ >Sólo se recomienda para una sola copia (1 artículo)');

define('TEXT_PRODUCTS_ORDER_DESCRIPTION', 'Descripción del pedido - visualización al final del pedido, en el correo de pedido, impresión del pedido');

define('TEXT_HOW_TO_LINK', '<b>Vista de página después de copiar/enlazar</b>');
define('TEXT_HOW_TO_LINK_INFO', 'Pantalla de entrada de artículo<br/ >(Si hay varios artículos ir al último en la lista)');

define('TEXT_SET_GROUP_PERMISSIONS', '¿Adoptar permisos de los grupos de clientes para todos los subcarpetas y artículos?');

define('HEADING_TITLE_ONLY_INACTIVE_PRODUCTS', 'Mostrar sólo artículos inactivos');

// BOF - Timo Paul (mail[at]timopaul[dot]biz) - 2014-01-17 - duplicate products content and links
define('TEXT_CONTENT_COPY', 'Copiar adjunto contenido del artículo');
define('TEXT_CONTENT_COPY_INFO', 'Copiar adjunto contenido del artículo<br/ >Sólo se recomienda para una sola copia (1 artículo)');
define('TEXT_LINKS_COPY', 'Copiar adjunto enlaces a artículos');
define('TEXT_LINKS_COPY_INFO', 'Copiar adjunto enlaces a artículos<br/ >Sólo se recomienda para una sola copia (1 artículo)');
// EOF - Timo Paul (mail[at]timopaul[dot]biz) - 2014-01-17 - duplicate products content and links

define('TEXT_GRADUATED_PRICES_INFO', 'El número de campos de entrada para los precios escalonados se puede ajustar en "<b>Configuración - Opciones del área de admin - Número de precios escalonados</b>" .');
define('TEXT_CATEGORY_SETTINGS', 'Configuración de la categoría:');

define('ERROR_QTY_SAVE_CHANGED', 'Durante la edición del artículo, el inventario se modificó y no se guardó.');

define('TEXT_NO_MOVE_POSSIBLE', 'El artículo no puede ser movido.');

define('TEXT_IN', 'en:');

define('TEXT_PRODUCTS_ATTRIBUTES_RECALCULATE', 'Recalcular atributos al modificar la tasa de impuesto');

define('HEADING_TITLE_CAT_BREADCRUMB', ' en "%s"');

define('TEXT_PRODUCTS_TAGS', 'Características del artículo');

define('TEXT_GRADUATED_PRICES_GROUP_INFO', 'Actualmente, el grupo de clientes no tiene permiso para ver precios escalonados. Esto se puede cambiar en la configuración del grupo de clientes en cualquier momento.');

define('TEXT_NO_FILE', '¡No hay fichero de plantilla disponible!');

define('ERROR_COPY_METHOD_NOT_SPECIFIED', 'Método de copia no especificado.');
define('ERROR_COPY_METHOD_NOT_ALLOWED', 'El método de copia "Enlazar" no está permitido para las categorías.');

define('TEXT_TAGS_COPY', 'Copiar adjunto características del artículo');
define('TEXT_TAGS_COPY_INFO', 'Copiar adjunto características del artículo<br/ >Sólo se recomienda para una sola copia (1 artículo)');

define('TEXT_PRODUCTS_LAST_MODIFIED', 'Última actualización:');
?>

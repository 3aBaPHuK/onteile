<?php
/**********************************************
* file: catalogue_shopping_cart.php
* use: new catalogue shopping_cart box
* (c) noRiddle 10-2016
**********************************************/

include_once 'nr_catalogue_files/config.php';

  $box_smarty = new smarty;

  $box_smarty->assign('tpl_path', DIR_WS_BASE.'templates/'.CURRENT_TEMPLATE.'/');

  // define defaults
  $products_in_cart = array ();
  $cc_total_tot = 0;
  $cc_qty_tot = 0;

  // include needed files
  require_once (DIR_FS_INC.'xtc_recalculate_price.inc.php');
  
  //echo '<br><br><br><pre>'.print_r($_SESSION['catalogue_cart'.$_SESSION['shopsess']], true).'</pre>';

if ($_SESSION['catalogue_cart'.$_SESSION['shopsess']]->count_all_contents() > 0) {
    // build array with cart content and count quantity  
    if (strpos($PHP_SELF, FILENAME_LOGOFF) === false) {
        $cc_products = $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->get_products();
        $cc_discount = array();
        $cc_qty = array();
        //$cc_total = array();
        
        foreach ($cc_products as $cc_brand => $cc_models) {
            $cc_discount[$cc_brand] = 0;
            $cc_qty[$cc_brand] = 0;
            //$cc_total[$cc_brand] = $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->show_total($cc_brand);

            $sizeof_models = sizeof($cc_models);
            for ($i = 0, $n = $sizeof_models; $i < $n; $i++) {
                /*$del_button = '<a href="' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('action', 'c', 'vid', 'uid', 'gid', 'coi', 'spi2', 'ssd', 'brd', 'mod')).'action=remove_product&brd='.$cc_brand.'&mod='.$cc_models[$i]['model'], 'NONSSL') . '">' . xtc_image_button('cart_del.gif', IMAGE_BUTTON_DELETE) . '</a>';
                $del_link = '<a href="' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('action', 'c', 'vid', 'uid', 'gid', 'coi', 'spi2', 'ssd', 'brd', 'mod')).'action=remove_product&brd='.$cc_brand.'&mod='.$cc_models[$i]['model'], 'NONSSL') . '">' . IMAGE_BUTTON_DELETE . '</a>';*/
                
                $del_button = '<a href="' . xtc_href_link(basename($PHP_SELF), 'action=remove_product&brd='.$cc_brand.'&mod='.$cc_models[$i]['model'], 'NONSSL') . '">' . xtc_image_button('cart_del.gif', IMAGE_BUTTON_DELETE) . '</a>';
                $del_link = '<a href="' . xtc_href_link(basename($PHP_SELF), 'action=remove_product&brd='.$cc_brand.'&mod='.$cc_models[$i]['model'], 'NONSSL') . '">' . IMAGE_BUTTON_DELETE . '</a>';
          
                $cc_qty[$cc_brand] += $cc_models[$i]['quantity'];
                $cc_qty_tot += $cc_models[$i]['quantity'];
                
                $products_in_cart[$cc_brand][] = array('QTY' => $cc_models[$i]['quantity'],
                                                       'MODEL' => $cc_models[$i]['model'],
                                                       'NAME' => $cc_models[$i]['name'],
                                                       'BUTTON_DELETE' => $del_button,
                                                       'LINK_DELETE' => $del_link
                                                      );
            
                if($i == 0) {
                    //$cc_brand_pic[$cc_brand] = 'nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/35x35/'.Config::$nr_icon_arr[$cc_models[$i]['lax_brand']];
                    $cc_brand_pic[$cc_brand] = 'nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/35x35/'.$_SESSION['nr_ctlg_brands'][$cc_models[$i]['lax_brand']]['brand_icon'];
                }
            }
    
            /*
            //sales discount
            if ($_SESSION['customers_status']['customers_status_ot_discount_flag'] == '1' && $_SESSION['customers_status']['customers_status_ot_discount'] != '0.00') {
                if ($_SESSION['customers_status']['customers_status_show_price_tax'] == 0 && $_SESSION['customers_status']['customers_status_add_tax_ot'] == 1) {
                    $price = $cc_total[$cc_brand] - $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->show_tax($cc_brand, false);
                } else {
                    $price = $cc_total[$cc_brand];
                }
                $cc_discount[$cc_brand] = $nr_foreign_xtcPrice->xtcGetDC($price, $_SESSION['customers_status']['customers_status_ot_discount']);
                $cc_discount_all[$cc_brand] =  $nr_foreign_xtcPrice->xtcFormat(($cc_discount[$cc_brand] * (-1)), $price_special = 1, $calculate_currencies = false);
            }
            
            // generate total price
            if ($_SESSION['customers_status']['customers_status_show_price'] == '1') {
                if($cc_discount) {
                    if($_SESSION['customers_status']['customers_status_show_price_tax'] == 0 && $_SESSION['customers_status']['customers_status_add_tax_ot'] == 0)
                        $cc_total[$cc_brand] -= $cc_discount[$cc_brand];
                    if($_SESSION['customers_status']['customers_status_show_price_tax'] == 0 && $_SESSION['customers_status']['customers_status_add_tax_ot'] == 1)
                        $cc_total[$cc_brand] -= $cc_discount[$cc_brand];
                    if($_SESSION['customers_status']['customers_status_show_price_tax'] == 1)
                        $cc_total[$cc_brand] -= $cc_discount[$cc_brand];
                }
                //$cc_total[$cc_brand] = $nr_foreign_xtcPrice->xtcFormat($cc_total[$cc_brand], true);
                $ust_all = $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->show_tax($cc_brand);
            }
            */
        }
        /*
        if ($_SESSION['customers_status']['customers_status_show_price'] == '1') {
            foreach($cc_total as $val) {
                $cc_total_tot += $val;
            }
            $cc_total_tot = $nr_foreign_xtcPrice->xtcFormat($cc_total_tot, true);
        }
        */
        //echo '<br /><br /><pre>'.print_r($cc_total, true).'</pre>';
        //echo '<pre>'.$_SESSION['catalogue_cart'.$_SESSION['shopsess']]->count_all_contents().'</pre>';
    }
}

if ($_SESSION['customers_status']['customers_status_ot_discount_flag'] == '1' && $_SESSION['customers_status']['customers_status_ot_discount'] != '0.00') {
    $box_smarty->assign('DISCOUNT', $cc_discount_all);
    $box_smarty->assign('UST', $ust_all);
}
/*
if ($_SESSION['customers_status']['customers_status_show_price'] == '1') {
    $box_smarty->assign('TOTAL', $cc_total_tot);
}
*/

$box_smarty->assign('CURR_SYMB', $nr_foreign_xtcPrice->currencies[$nr_foreign_xtcPrice->actualCurr]['symbol_right']);

$box_smarty->assign('BRAND_PIC', $cc_brand_pic);

$box_smarty->assign('deny_cart', strpos($PHP_SELF, 'checkout') !== false ? 'true' : 'false'); // no cart at the checkout
$box_smarty->assign('products', $products_in_cart);
$box_smarty->assign('PRODUCTS', $cc_qty_tot);
$box_smarty->assign('empty', $cc_qty_tot > 0 ? 'false' : 'true');

// GV Code
/*if (isset($_SESSION['customer_id'])) {
    $gv_query = xtc_db_query("-- /templates/".CURRENT_TEMPLATE."/source/boxes/catalogue_shopping_cart.php
                              SELECT amount
                                FROM ".TABLE_COUPON_GV_CUSTOMER."
                               WHERE customer_id = '".(int)$_SESSION['customer_id']."'");
    $gv_result = xtc_db_fetch_array($gv_query);
    if ($gv_result['amount'] > 0) {
        $box_smarty->assign('GV_AMOUNT', $nr_foreign_xtcPrice->xtcFormat($gv_result['amount'], true, 0, true));
        $box_smarty->assign('GV_SEND_TO_FRIEND_LINK', '<a href="'.xtc_href_link(FILENAME_GV_SEND).'">');
    }
    if (isset($_SESSION['gv_id'])) {
        $gv_query = xtc_db_query("-- /templates/".CURRENT_TEMPLATE."/source/boxes/catalogue_shopping_cart.php
                                SELECT coupon_amount
                                  FROM ".TABLE_COUPONS."
                                 WHERE coupon_id = '".(int)$_SESSION['gv_id']."'");
                                 
        $coupon = xtc_db_fetch_array($gv_query);
        $box_smarty->assign('COUPON_AMOUNT2', $nr_foreign_xtcPrice->xtcFormat($coupon['coupon_amount'], true, 0, true));
    }
    if (isset($_SESSION['cc_id'])) {
        $box_smarty->assign('COUPON_HELP_LINK', '<a target="_blank" class="thickbox" title="Information" href="'.xtc_href_link(FILENAME_POPUP_COUPON_HELP, 'cID='.$_SESSION['cc_id']. '&KeepThis=true&TB_iframe=true&height=400&width=600', $request_type).'">Information</a>');
    }
}*/ //no need for this

$box_smarty->assign('LINK_CART', xtc_href_link('catalogue_shopping_cart.php', '', 'NONSSL'));
$box_smarty->caching = 0;
$box_smarty->assign('language', $_SESSION['language']);
$box_catalogue_shopping_cart = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_catalogue_cart.html');
$smarty->assign('box_CATALOGUE_CART', $box_catalogue_shopping_cart);
?>
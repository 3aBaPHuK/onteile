<?php
/* -----------------------------------------------------------------------------------------
   $Id: contact_us.php 10261 2016-08-22 11:21:32Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_CONTACT_US_STATUS_TITLE', 'Estado');
define('MODULE_CONTACT_US_STATUS_DESC', 'Estado del módulo');
define('MODULE_CONTACT_US_STATUS_INFO', 'Activo');
define('MODULE_CONTACT_US_TEXT_TITLE', 'Registro del Formulario de contacto');
define('MODULE_CONTACT_US_TEXT_DESCRIPTION', 'Se escribe un registro en la base de datos tan pronto como se realiza una solicitud a través del formulario de contacto.');
?>

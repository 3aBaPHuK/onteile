<?php
/*------------------------------------------------------------------------------
   $Id: affiliate.php 40 2013-01-08 16:36:44Z Hubi $

   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate.php, v 1.6 2003/02/22);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   
   adapted to modified 2.0.2.2, 07-2017, noRiddle
   ---------------------------------------------------------------------------*/

// include smarty
include(DIR_FS_BOXES_INC . 'smarty_default.php');
   
//$box_smarty = new smarty;

$box_content = '<ul class="contentlist">';
//$box_smarty->assign('tpl_path',DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/');

if (isset($_SESSION['affiliate_id'])) {
    $box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE_SUMMARY, '', 'SSL') . '">' . BOX_AFFILIATE_SUMMARY . '</a></li>';
    $box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE_DETAILS, '', 'SSL'). '">' . BOX_AFFILIATE_ACCOUNT . '</a></li>';
    $box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE_PAYMENT, '', 'SSL'). '">' . BOX_AFFILIATE_PAYMENT . '</a></li>';
    $box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE_CLICKS, '', 'SSL'). '">' . BOX_AFFILIATE_CLICKRATE . '</a></li>';
    $box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE_SALES, '', 'SSL'). '">' . BOX_AFFILIATE_SALES . '</a></li>';
    $box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE_BANNERS, 'type=b'). '">' . BOX_AFFILIATE_BANNERS . '</a></li>';
    $box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE_BANNERS, 'type=t'). '">' . BOX_AFFILIATE_TEXTLINKS . '</a></li>';
    if($product->isProduct()) {
    	$box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE_BANNERS, 'individual_banner_id=' . $product->pID). '">' . BOX_AFFILIATE_PRODUCTLINK . '</a></li>';
    }
    $box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE_CONTACT). '">' . BOX_AFFILIATE_CONTACT . '</a></li>';
    $box_content .= '<li><a href="' . xtc_href_link(FILENAME_CONTENT, 'coID=902'). '">' . BOX_AFFILIATE_FAQ . '</a></li>';
    $box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE_LOGOUT). '">' . BOX_AFFILIATE_LOGOUT . '</a></li>';
}
else {
	$box_content .= '<li><a href="' . xtc_href_link(FILENAME_CONTENT,'coID=901'). '">' . BOX_AFFILIATE_INFO . '</a></li>';
	$box_content .= '<li><a href="' . xtc_href_link(FILENAME_AFFILIATE, '', 'SSL') . '">' . BOX_AFFILIATE_LOGIN . '</a></li>';
	$box_content .= '<li><a href="' . xtc_href_link(FILENAME_CONTENT,'coID=900'). '">' . BOX_AFFILIATE_AGB . '</a></li>';
}

$box_content .= '</ul>';

//$box_smarty->assign('BOX_TITLE', BOX_HEADING_ADD_PRODUCT_ID); //what's that for ?, noRiddle
$box_smarty->assign('BOX_CONTENT', $box_content);
//$box_smarty->assign('language', $_SESSION['language']);

// set cache ID
$box_smarty->caching = 0;
$box_affiliate = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_affiliate.html');

$smarty->assign('box_AFFILIATE', $box_affiliate);
?>

<?php
/* --------------------------------------------------------------
   $Id: specials.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(specials.php,v 1.10 2002/01/31); www.oscommerce.com 
   (c) 2003	 nextcommerce (specials.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('SPECIALS_TITLE', 'Oferta especial');

define('TEXT_SPECIALS_PRODUCT', 'Artículo:');
define('TEXT_SPECIALS_SPECIAL_PRICE', 'Precio de la oferta:');
define('TEXT_SPECIALS_SPECIAL_QUANTITY', 'Cantidad:');
define('TEXT_SPECIALS_START_DATE', 'Válido desde: <small>(AAAA-MM-DD)</small>');
define('TEXT_SPECIALS_EXPIRES_DATE', 'Válido hasta: <small>(AAAA-MM-DD)</small>');

define('TEXT_INFO_DATE_ADDED', 'agregado el:');
define('TEXT_INFO_LAST_MODIFIED', 'última modificación:');
define('TEXT_INFO_NEW_PRICE', 'Nuevo precio:');
define('TEXT_INFO_ORIGINAL_PRICE', 'Precio anterior:');
define('TEXT_INFO_PERCENTAGE', 'Porcentaje:');
define('TEXT_INFO_START_DATE', 'Válido desde:');
define('TEXT_INFO_EXPIRES_DATE', 'Válido hasta:');

define('TEXT_INFO_HEADING_DELETE_SPECIALS', 'Borrar oferta especial');
define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar esta oferta especial?');

define('TEXT_SPECIALS_NO_PID', '¡El artículo debe ser guardado primero, de lo contrario la oferta especial no puede ser generada correctamente!');

define('TEXT_CATSPECIALS_START_DATE_TT', 'Especifique la fecha a partir de la cual debe ser válido el precio de oferta.<br>');
define('TEXT_CATSPECIALS_EXPIRES_DATE_TT', 'Deje el campo <strong>Válido hasta</strong> vacío si desea que el precio de oferta sea válido sin limitación de tiempo.<br>');
define('TEXT_CATSPECIALS_SPECIAL_QUANTITY_TT', 'En el campo <strong>Cantidad</strong> uede introducir el número de unidades a las que se aplicará la oferta.<br>Bajo "Configuración" -> "Opciones de gestión de almacenes" -> "Verificar ofertas especiales" puede decidir si debe verificarse el stock de ofertas especiales.');
define('TEXT_CATSPECIALS_SPECIAL_PRICE_TT', 'También puede introducir valores porcentuales en el campo Precio de oferta, por ejemplo: <strong>20%</strong><br>Si introduce un nuevo precio, los decimales se deben separar con un \'.\' por ejemplo: <strong>49.99</strong>');
?>

<?php
/* --------------------------------------------------------------
   $Id: content_manager.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on:
   (c) 2003	 nextcommerce (content_manager.php,v 1.8 2003/08/25); www.nextcommerce.org
   
   Released under the GNU General Public License 
   --------------------------------------------------------------*/
   
define('HEADING_TITLE', 'Manager de contenido');
define('HEADING_CONTENT', 'Contenido de páginas');
define('HEADING_PRODUCTS_CONTENT', 'Contenido del artículo');
define('TABLE_HEADING_CONTENT_ID', 'ID');
define('TABLE_HEADING_CONTENT_TITLE', 'Título');
define('TABLE_HEADING_CONTENT_FILE', 'Archivo');
define('TABLE_HEADING_CONTENT_STATUS', 'Visible en el recuadro');
define('TABLE_HEADING_CONTENT_BOX', 'Recuadro');
define('TABLE_HEADING_PRODUCTS_ID', 'ID');
define('TABLE_HEADING_PRODUCTS', 'Artículo');
define('TABLE_HEADING_PRODUCTS_CONTENT_ID', 'ID');
define('TABLE_HEADING_LANGUAGE', 'Idioma');
define('TABLE_HEADING_CONTENT_NAME', 'Nombre/Nombre del archivo');
define('TABLE_HEADING_CONTENT_LINK', 'Enlace');
define('TABLE_HEADING_CONTENT_HITS', 'Visitas');
define('TABLE_HEADING_CONTENT_GROUP', 'coID');
define('TABLE_HEADING_CONTENT_SORT', 'Orden');
define('TEXT_YES', 'Sí');
define('TEXT_NO', 'No');
define('TABLE_HEADING_CONTENT_ACTION', 'Acción');
define('TEXT_DELETE', 'Borrar');
define('TEXT_EDIT', 'Editar');
define('TEXT_PREVIEW', 'Vista previa');
define('CONFIRM_DELETE', '¿Realmente quiere borrar el contenido?');
define('CONTENT_NOTE', '¡El contenido marcado con <font color="ff0000">*</font> pertenece al sistema y no puede ser borrado!');

 
 // edit
define('TEXT_LANGUAGE', 'Idioma:');
define('TEXT_STATUS', 'Visible:');
define('TEXT_STATUS_DESCRIPTION', '¿Mostrar enlace en el recuadro de información?');
define('TEXT_TITLE', 'Título:');
define('TEXT_TITLE_FILE', 'Título/Nombre del archivo:');
define('TEXT_SELECT', '-Por favor seleccione-');
define('TEXT_HEADING', 'Encabezado:');
define('TEXT_CONTENT', 'Texto:');
define('TEXT_UPLOAD_FILE', 'Subir archivo:');
define('TEXT_UPLOAD_FILE_LOCAL', '(de su sistema local)');
define('TEXT_CHOOSE_FILE', 'seleccionar archivo:');
define('TEXT_CHOOSE_FILE_DESC', 'También puede seleccionar un archivo existente de la lista.');
define('TEXT_NO_FILE', 'Borrar selección');
define('TEXT_CHOOSE_FILE_SERVER', '(Si guardó sus archivos en su servidor a través de FTP  <i>(media/content)</i>, puede seleccionar el archivo aquí.');
define('TEXT_CURRENT_FILE', 'Archivo actual:');
define('TEXT_FILE_DESCRIPTION', '<b>Información:</b><br />También tiene la posibilidad de incluir un archivo <b>.html</b> o <b>.htm</b> como contenido.<br /> Si selecciona o sube un archivo, el texto del campo de texto se ignora.<br /><br />');
define('ERROR_FILE', 'Formato de archivo incorrecto (sólo.html o.htm)');
define('ERROR_TITLE', 'Por favor, introduzca un título.');
define('ERROR_COMMENT', 'Por favor, introduzca una descripción del archivo.');
define('TEXT_FILE_FLAG', 'Recuadro:');
define('TEXT_PARENT', 'Documento principal:');
define('TEXT_PARENT_DESCRIPTION', 'Asignar a este documento como subcontenido');
define('TEXT_PRODUCT', 'Artículo:');
define('TEXT_LINK', 'Enlace:');
define('TEXT_SORT_ORDER', 'Orden:');
define('TEXT_GROUP', 'Grupo de idioma:');
define('TEXT_GROUP_DESC', 'Con este ID puede vincular los mismos temas en diferentes idiomas.');
 
define('TEXT_CONTENT_DESCRIPTION', 'Con este Manager de Contenidos tiene la posibilidad de añadir cualquier tipo de archivo a un artículo<br />P.ej. descripciones de artículos, manuales, fichas técnicas, muestras de audio, etc...<br />Estos elementos se muestran en la vista detallada del artículo.<br /><br />');
define('TEXT_FILENAME', 'Utilice Archivo:');
define('TEXT_FILE_DESC', 'Descripción:');
define('USED_SPACE', 'Memoria utilizada:');
define('TABLE_HEADING_CONTENT_FILESIZE', 'Tamaño del archivo');
define('TEXT_CONTENT_NOINDEX', 'noindex (El robot de búsqueda no debe incluir la página web en el índice.)');
define('TEXT_CONTENT_NOFOLLOW', 'nofollow (El robot de búsqueda puede incluir la página web, pero no debe seguir los hipervínculos de la página.)');
define('TEXT_CONTENT_NOODP', 'noodp (El motor de búsqueda no debe utilizar los textos de descripción de DMOZ (ODP) en la página de resultados.)');
define('TEXT_CONTENT_META_ROBOTS', 'Meta Robots');
 
define('TABLE_HEADING_STATUS_ACTIVE', 'Estado');
define('TEXT_STATUS_ACTIVE', 'Estado activo:');
define('TEXT_STATUS_ACTIVE_DESCRIPTION', '¿Activar contenido?');
  
define('TEXT_CONTENT_DOUBLE_GROUP_INDEX', '¡Doble índice de grupo de contenido! Por favor, guarda de nuevo. ¡Así el problema se resolverá automáticamente!');
 
?>

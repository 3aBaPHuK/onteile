<?php
/*******************************************************************************************
* routine to verify whether keys are enabled
* file: /verify_keys.php
* (c) noRiddle 02-2016
*
* phpMyAdmin to find all disabled keys (e.g. BMW): SELECT * FROM information_schema.statistics WHERE TABLE_SCHEMA = 'bmwbmw';
*******************************************************************************************/

//BOC indexes of which tables to show
$table_array = array('csv_import_aux', 'categories', 'categories_description', 'products', 'products_description', 'products_to_categories', 'products_images', 'products_attributes');
$group_perm_query = xtc_db_query("SHOW COLUMNS FROM csv_import_aux WHERE Field LIKE '%personal_offers_by_customers_status_%'");
while($group_perm_arr = xtc_db_fetch_array($group_perm_query)) {
    $table_array = array_merge($table_array, array($group_perm_arr['Field']));
}

$no_table_array = array('address_book', 'customers', 'sessions');
//EOC indexes of which tables to show

$indxs_table_arr = array();
$enbl_k_out = '';

//BOC queries for inconsitancies
if(isset($_GET['action']) && $_GET['action'] == 'proddesc_id_notin_prod') {
    //products_id in products_description not in products
    $proddesc_id_notin_prod_qu = xtc_db_query("SELECT pd.* FROM products_description pd WHERE pd.products_id NOT IN (SELECT p.products_id FROM products p)");
    $proddesc_id_notin_prod = xtc_db_num_rows($proddesc_id_notin_prod_qu);
    $proid_out1 = $proddesc_id_notin_prod > 0 ? '<span style="color:#c00;">'.sprintf(ADMIN_TOOLS_FIND_PRODDESCIDS_NOTIN_PROD_RES, $proddesc_id_notin_prod).'</span>' : '<span style="color:#0A7E00">OKAY</span>';
}
if(isset($_GET['action']) && $_GET['action'] == 'prodprodtocat_id_notin_prod') {
    //products_id in products_to_categories not in products
    $prodprodtocat_id_notin_prod_qu = xtc_db_query("SELECT pto.* FROM products_to_categories pto WHERE pto.products_id NOT IN (SELECT p.products_id FROM products p)");
    $prodprodtocat_id_notin_prod = xtc_db_num_rows($prodprodtocat_id_notin_prod_qu);
    $proid_out2 = $prodprodtocat_id_notin_prod > 0 ? '<span style="color:#c00;">'.sprintf(ADMIN_TOOLS_FIND_PRODTOCATIDS_NOTIN_PROD_RES, $prodprodtocat_id_notin_prod).'</span>' : '<span style="color:#0A7E00">OKAY</span>';
}
if(isset($_GET['action']) && $_GET['action'] == 'prodprod_id_notin_proddesc') {
    //products_id in products not in products_description
    $prodprod_id_notin_proddesc_qu = xtc_db_query("SELECT COUNT(*) as repetitions, products_id, language_id FROM products_description GROUP BY products_id HAVING repetitions < 2;");
    $prodprod_id_notin_proddesc = xtc_db_num_rows($prodprod_id_notin_proddesc_qu);
    $proid_out3 = $prodprod_id_notin_proddesc > 0 ? '<span style="color:#c00;">'.sprintf(ADMIN_TOOLS_FIND_PRODPRODIDS_NOTIN_PRODDESC_RES, $prodprod_id_notin_proddesc).'</span>' : '<span style="color:#0A7E00">OKAY</span>';
}
if(isset($_GET['action']) && $_GET['action'] == 'prodid_more_than_one_prodtocat') {
    //products_id more than one time in products_to_categories
    $prodid_more_than_one_prodtocat_qu = xtc_db_query("SELECT COUNT(*) as repetitions, products_id FROM products_to_categories GROUP BY products_id HAVING repetitions > 1");
    $prodid_more_than_one_prodtocat = xtc_db_num_rows($prodid_more_than_one_prodtocat_qu);
    $proid_out4 = $prodid_more_than_one_prodtocat > 0 ? '<span style="color:#c00;">'.sprintf(ADMIN_TOOLS_FIND_PRODIDS_MORETHANONE_PRODTOCAT_RES, $prodid_more_than_one_prodtocat).'</span>' : '<span style="color:#0A7E00">OKAY</span>';
}
if(isset($_GET['action']) && $_GET['action'] == 'prodid_in_prodtocat_top') {
    //products_id with categories_id 0 (TOP) in products_to_categories
    $prod_in_top_str = '';
    $prodid_in_prodtocat_top_qu = xtc_db_query("SELECT ptc.products_id, p.products_model FROM products_to_categories ptc LEFT JOIN products p ON p.products_id = ptc.products_id WHERE ptc.categories_id = '0'");
    if(xtc_db_num_rows($prodid_in_prodtocat_top_qu) > 0) {
        while($prodid_in_prodtocat_top = xtc_db_fetch_array($prodid_in_prodtocat_top_qu)) {
            $prod_in_top_str .= '('.$prodid_in_prodtocat_top['products_id'].', '.$prodid_in_prodtocat_top['products_model'].')<br />';
        }
        $proid_out5 = '<span style="color:#c00;">'.sprintf(ADMIN_TOOLS_FIND_PRODIDS_IN_PRODTOCAT_TOP_RES, $prod_in_top_str).'</span>';
    } else {
        $proid_out5 = '<span style="color:#0A7E00">OKAY</span>';
    }
}
//BOC queries for inconsitancies

$table_query = xtc_db_query("SHOW TABLES");
//while ($row = mysql_fetch_row($table_query)) {
while ($row = xtc_db_fetch_row($table_query)) {
    $tables[] = $row[0];
}
natsort($tables);
//echo '<pre>'.print_r($tables, true).'</pre>';

foreach($tables as $key => $table_name) {
    if(in_array($table_name, $table_array)) {
    //if(!in_array($table_name, $no_table_array) && strpos($table_name, 'mailbeez') === false) {
        $ind_sql = "SHOW INDEXES FROM ".$table_name;
        $ind_query = xtc_db_query($ind_sql);
        while($indxs = xtc_db_fetch_array($ind_query)) {
            $indxs_table_arr[$table_name][] = array('Key_name' => $indxs['Key_name'], 'Column_name' => $indxs['Column_name'], 'Cardinality' =>  $indxs['Cardinality'], 'Comment' => $indxs['Comment']);
        }
    }
}
//echo '<pre>'.print_r($indxs_table_arr, true).'</pre>';

if(isset($_GET['action']) && $_GET['action'] == 'enable_keys') {
    //BOC enable keys
    if(isset($_POST['enbl_keys'])) {
        foreach($_POST['enbl_keys'] as $tbl) {
            if(xtc_db_query("ALTER TABLE ".$tbl." ENABLE KEYS")) {
                $enbl_k_out .= sprintf(ADMIN_TOOLS_VERIFY_KEYS_ENABLE_SUCCESS_TXT, $tbl).'|';
            }
        }
        xtc_redirect(xtc_href_link(FILENAME_MASTER_ADMIN, 'script=verify_keys&keys_enabled='.$enbl_k_out));
    }
    //EOC enable keys
    //BOC analyze table
    if(isset($_POST['anlz_tbl'])) {
        foreach($_POST['anlz_tbl'] as $tbl) {
            if(xtc_db_query("ANALYZE TABLE ".$tbl)) {
                $enbl_k_out .= sprintf(ADMIN_TOOLS_VERIFY_KEYS_ANALYZE_SUCCESS_TXT, $tbl).'|';
            }
        }
        xtc_redirect(xtc_href_link(FILENAME_MASTER_ADMIN, 'script=verify_keys&table_analyzed='.$enbl_k_out));
    }
    //EOC analyze table
    //BOC optimize table
    if(isset($_POST['optmz_tbl'])) {
        foreach($_POST['optmz_tbl'] as $tbl) {
            if(xtc_db_query("OPTIMIZE TABLE ".$tbl)) {
                $enbl_k_out .= sprintf(ADMIN_TOOLS_VERIFY_KEYS_OPTIMIZE_SUCCESS_TXT, $tbl).'|';
            }
        }
        xtc_redirect(xtc_href_link(FILENAME_MASTER_ADMIN, 'script=verify_keys&table_optimized='.$enbl_k_out));
    }
    //EOC optimize table
}

if(isset($_GET['keys_enabled'])) {
    $eko = $_GET['keys_enabled'];
} else if(isset($_GET['table_analyzed'])) {
    $eko = $_GET['table_analyzed'];
} else if(isset($_GET['table_optimized'])) {
    $eko = $_GET['table_optimized'];
}
?>
<?php
/****************************************************************************************
* file: text_vin_credits.php
* use: if catalog_credits for VIN search were ordered give message in order mail
*
* (c) noRiddle 04-2018
****************************************************************************************/

if(isset($so_bought_vin_credits) && $so_bought_vin_credits > 0) {
    $smarty->assign('YOU_ORDERED_VIN_CREDIT', sprintf(TXT_SEND_ORDER_VI_CREDIT, $so_bought_vin_credits));
}
?>
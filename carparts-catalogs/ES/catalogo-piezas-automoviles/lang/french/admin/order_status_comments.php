<?php
/*************************************************************
* file: order_status_comments.php
* use: language constants for order_status_comments
* (c) noRiddle 12-2016
*************************************************************/

define('HEADING_TITLE', 'Textes de commentaires de statut de commande');

define('TEXT_SORT_ASC', 'croissant');
define('TEXT_SORT_DESC', 'décroissant');

define('TABLE_HEADING_ORDER_STATUS_COMMENTS_ID', 'ID');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_TITLE', 'Titre du commentaire');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_TEXT', 'Texte de commentaire');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_SORT_ORDER', 'Tri');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_STATUS', 'Statut');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_ACTION', 'Action');

define('TEXT_ORDER_STATUS_COMMENT', 'Modèle de commentaire %d');
define('TEXT_INFO_DELETE_COMMENT_INTRO', 'Êtes-vous sûr de vouloir supprimer ce modèle de commentaire?');
define('TEXT_DELETE_COMMENT', 'Supprimer le modèle de commentaire?');
define('TEXT_INFO_HEADING_DELETE_ORDER_STATUS_COMMENT', 'Supprimer le modèle de commentaire');
define('TEXT_DATE_ADDED_ORDER_STATUS_COMMENT', 'Ajouté le:');
define('TEXT_LAST_MODIFIED_ORDER_STATUS_COMMENT', 'Dernière modification le:');

define('TEXT_ORDER_STATUS_COMMENTS_STATUS', 'Statut:');
define('TEXT_ORDER_STATUS_COMMENTS_SORT_ORDER', 'Tri:');
define('TEXT_ORDER_STATUS_COMMENTS_TITLE', 'Titre du texte de commentaire:');
define('TEXT_ORDER_STATUS_COMMENTS_TEXT', 'Texte de commentaire:');

define('TEXT_DISPLAY_NUMBER_OF_ORDER_STYTUS_COMMENTS', 'Affichés sont les commentaires <b>%d</b> à <b>%d</b> (d\'un total de <b>%d</b> textes de commentaire)');
?>

<?php
/*********************************************************************************************************************************
* file: nr_order_in_other_shop.php
* use: system file to enable feature to order in other shop
* (c) noRiddle 04-2018
*********************************************************************************************************************************/

defined( '_VALID_XTC' ) or die( 'Direct Access to this location is not allowed.' );

class nr_order_in_other_shop {
    var $code, $title, $description, $enabled;

    function __construct() {
        $this->code = 'nr_order_in_other_shop';
        $this->title = MODULE_NR_ORDER_IN_OTHER_SHOP_TITLE;
        $this->description = MODULE_NR_ORDER_IN_OTHER_SHOP_DESCRIPTION;
        $this->sort_order = defined('MODULE_NR_ORDER_IN_OTHER_SHOP_SORT_ORDER') ? MODULE_NR_ORDER_IN_OTHER_SHOP_SORT_ORDER : 0;
        $this->availbl_lngs = xtc_get_languages();
        $this->cnt_lngs = count($this->availbl_lngs);
        $this->enabled = MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS == 'true' ? true : false;
    }

    function process($file) {
        //do nothing
    }

    function display() {
        return array('text' => '<br>' . xtc_button(BUTTON_SAVE) . '&nbsp;' . xtc_button_link(BUTTON_CANCEL, xtc_href_link(FILENAME_MODULE_EXPORT, 'set=' . $_GET['set'] . '&module='.$this->code)));
    }

    function check() {
        if(!isset($this->_check)) {
          $check_query = xtc_db_query("SELECT configuration_value FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS'");
          $this->_check = xtc_db_num_rows($check_query);
        }
        return $this->_check;
    }

    function install() {
        xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) VALUES ('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS', 'false',  '6', '1', 'xtc_cfg_select_option(array(\'true\', \'false\'), ', now())");
        xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, date_added) VALUES ('MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP', '',  '6', '1', now())");
        
        
        for($i = 0, $cl = count($this->availbl_lngs); $i < $cl; $i++) {
            $l_code = strtoupper($this->availbl_lngs[$i]['code']);
            //default texts
            if($l_code == 'DE') {
                $std_txt = 'Aufgrund eines logistischen Engpasses werden Sie für die Bestellung zu unserem %s-Partner weitergeleitet.'."\n".'Ihre Bestellung wird dort zu den gleichen Konditionen schnellstens bearbeitet.';
            } else if($l_code == 'EN') {
                $std_txt = 'Due to a logistical shortage we direct you to our %s partner for the order.'."\n".'Your order will be handled quickly under the same conditions.';
            }
            xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, set_function, date_added) VALUES ('MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_".$l_code."', '".$std_txt."',  '6', '1', 'xtc_cfg_textarea(', now())");
        }
    }

    function remove() {
        xtc_db_query("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key LIKE 'MODULE_NR_ORDER_IN_OTHER_SHOP_%'");
    }
    
    function keys_langs($langs) {
        $langs_arr = array();
        for($i = 0; $i < $this->cnt_lngs; $i++) {
            $langs_arr[] = 'MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_'.strtoupper($langs[$i]['code']);
        }
        
        return $langs_arr;
    }

    function keys() {
        $langs = array('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS',
                       'MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP'
                      );
        
        $langs = array_merge($langs, $this->keys_langs($this->availbl_lngs));
        
        return $langs;
    }
}
?>
<?php
/*****************************************************
* file: nr_diverse_needed_things.php
* path: /includes/extra/header/header_body/
*
* (c) 09-2019 noRiddle
*****************************************************/

//BOC get some account information to show to customer (in e.g. 'Mein Konto'), noRiddle
if(isset($_SESSION['customer_id'])) {
    if(isset($_SESSION['customers_status']['customers_status_id']) && in_array($_SESSION['customers_status']['customers_status_id'], $which_cust_stat_show)) {
        switch($_SESSION['customers_status']['customers_status_id']) {
            case '3':
                $cust_stat_name = SHOW_CUST_STATUS_NAME_GARAGE;
              break;
            case '4':
                $cust_stat_name = SHOW_CUST_STATUS_NAME_CLUB;
              break;
            case '5':
                $cust_stat_name = SHOW_CUST_STATUS_NAME_GARAGE;
              break;
            case '7':
                $cust_stat_name = SHOW_CUST_STATUS_NAME_GARAGE;
              break;
            case '15':
                $cust_stat_name = SHOW_CUST_STATUS_NAME_GARAGE;
              break;
            case '16':
                $cust_stat_name = SHOW_CUST_STATUS_NAME_ADAC;
              break;
            case '17':
                $cust_stat_name = SHOW_CUST_STATUS_NAME_GARAGE;
              break;
            default:
                $cust_stat_name = '';
              break;
        }

        if($cust_stat_name != '') {
            $smarty->assign('nr_cust_stat_name', $cust_stat_name);
        }
    }

    $smarty->assign('nr_cust_fl_name', $_SESSION['customer_first_name'].' '.$_SESSION['customer_last_name']);
    
    //BOC credits for VIN search, noRiddle
    $cred_qu_str = "SELECT catalog_credit FROM ".TABLE_CUSTOMERS." WHERE customers_id = ".(int)$_SESSION['customer_id'];
    $cred_qu = xtc_db_query($cred_qu_str);
    $cred_arr = xtc_db_fetch_array($cred_qu);
    $smarty->assign('VIN_CREDITS', $cred_arr['catalog_credit']);
    //EOC credits for VIN search, noRiddle
}
//EOC get some account information to show to customer (in e.g. 'Mein Konto'), noRiddle
?>
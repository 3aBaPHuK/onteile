<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_configuration.php 34 2013-01-06 08:29:47Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('AFFILIATE_EMAIL_ADDRESS_TITLE', 'E-Mail Adresse');
define('AFFILIATE_EMAIL_ADDRESS_DESC', 'Die e-Mail Adresse für das Affiliate Programm &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/e-mail-adresse.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_PERCENT_TITLE', 'Affiliate Pay Per Sale %-Rate');
define('AFFILIATE_PERCENT_DESC', 'Prozentuale Rate für das Pay Per Sale Affiliate Programm. &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-pay-per-sale-rate.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_THRESHOLD_TITLE', 'Auszahlungsgrenze');
define('AFFILIATE_THRESHOLD_DESC', 'Untere Grenze für die Auszahlung an Affiliates &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/auszahlungsgrenze.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_COOKIE_LIFETIME_TITLE', 'Cookie Lifetime');
define('AFFILIATE_COOKIE_LIFETIME_DESC', 'Wie lange (in Sekunden) der Lead eines Affiliates gültig bleibt. &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/cookie-lifetime.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_BILLING_TIME_TITLE', 'Abrechnungszeit');
define('AFFILIATE_BILLING_TIME_DESC', 'Die Zeit, die zwischen einer erfolgreichen Bestellung und der Gutschrift beim Affiliate vergehen soll. Benötigt falls Bestellungen Rückbelastet werden. &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/abrechnungszeit.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_PAYMENT_ORDER_MIN_STATUS_TITLE', 'Order Status');
define('AFFILIATE_PAYMENT_ORDER_MIN_STATUS_DESC', 'Der Status einer Berstellung, ab wann sie provisionsfähig ist. Mehrere durch Semikolon getrennt (z.B. 3;4;7) &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/order-status.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_CHECK_TITLE', 'Affiliate Scheck');
define('AFFILIATE_USE_CHECK_DESC', 'Affilaites per Scheck auszahlen &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-scheck.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_PAYPAL_TITLE', 'Affiliate Paypal');
define('AFFILIATE_USE_PAYPAL_DESC', 'Affiliates per Paypal auszahlen &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-paypal.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_BANK_TITLE', 'Affiliate Bank');
define('AFFILIATE_USE_BANK_DESC', 'Affiliates per Überweisung auszahlen &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-bank.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_INDIVIDUAL_PERCENTAGE_TITLE', 'Individueller Prozentsatz');
define('AFFILIATE_INDIVIDUAL_PERCENTAGE_DESC', 'Prozentsatz individuell per Affiliate festlegen &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/individueller-prozentsatz.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_TIER_TITLE', 'Klassensystem');
define('AFFILIATE_USE_TIER_DESC', 'Multiklassen Affiliate Programm verwenden &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/klassensystem.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_ALLOWED_TITLE', 'Standard für Subaffiliates');
define('AFFILIATE_TIER_ALLOWED_DESC', 'Dürfen neue Affiliates Standardmässig Subaffiliates haben. &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/standard-fuer-subaffiliates.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_LEVELS_TITLE', 'Anzahl Klassen');
define('AFFILIATE_TIER_LEVELS_DESC', 'Anzahl der beim Klassensystem zu verwendenden Klassen &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/anzahl-klassen.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_PERCENTAGE_TITLE', 'Prozentsätze für Klassensystem');
define('AFFILIATE_TIER_PERCENTAGE_DESC', 'Individuelle Prozentsätze für die Unterklassen<br>Beispiel 8.00;5.00;1.00 &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/prozentsaetze-fuer-klassensystem.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
?>

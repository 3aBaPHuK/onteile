<?php
/************************************************************************************
* file: account_success.php                                                         *
* use: confirm successful account creation for modified eCommerce                   *
* upload to a newly to be created content and redirect to it in /create_account.php *
* version: for modified 2.0.X | (c) noRiddle 01-2020                                *
************************************************************************************/

switch ($_SESSION['language']) {
    case 'german':
        define('SAC_LINK_SHOPPCART', 'Warenkorb');
        define('SAC_LINK_START', 'Startseite');
        //define('SAC_SUCCESS_HEADER', 'Herzlich Willkommen '.$_SESSION['customer_first_name'].' '.$_SESSION['customer_last_name'].'.<br />Ihr Konto wurde erfolgreich erstellt.');
      break;
    case 'english':
        define('SAC_LINK_SHOPPCART', 'Shopping cart');
        define('SAC_LINK_START', 'Welcome page');
        //define('SAC_SUCCESS_HEADER', 'Welcome '.$_SESSION['customer_first_name'].' '.$_SESSION['customer_last_name'].'<br />Your account has been successfully created.');
      break;
    case 'french':
        define('SAC_LINK_SHOPPCART', 'Panier');
        define('SAC_LINK_START', 'Page principal');
        //define('SAC_SUCCESS_HEADER', 'Bienvenu '.$_SESSION['customer_first_name'].' '.$_SESSION['customer_last_name'].'<br />. Votre compte était crée avec succès.');
      break;
    case 'spanish':
        define('SAC_LINK_SHOPPCART', 'Cesta de compra');
        define('SAC_LINK_START', 'Página principal');
        //define('SAC_SUCCESS_HEADER', 'Bienvenido '.$_SESSION['customer_first_name'].' '.$_SESSION['customer_last_name'].'<br />. Su cuenta era creada con éxtito.');
      break;
    default:
        define('SAC_LINK_SHOPPCART', 'Shopping cart');
        define('SAC_LINK_START', 'Welcome page');
        //define('SAC_SUCCESS_HEADER', 'Welcome '.$_SESSION['customer_first_name'].' '.$_SESSION['customer_last_name'].'<br />. Your account has been created successfully.');
}

$has_cart = ($_SESSION['cart_'.$_SESSION['shopsess']]->count_contents() > 0);
$has_cart_cart = (is_array($_SESSION['catalogue_cart'.$_SESSION['shopsess']]) && count($_SESSION['catalogue_cart'.$_SESSION['shopsess']]) > 0);
?>
<style>
.acc-moremarg {margin:25px 0;}
a[href="javascript:history.back(1)"] {display:none;}
</style>
<p class="acc-moremarg">
<?php echo $has_cart ? 
'<a href="'.xtc_href_link(FILENAME_SHOPPING_CART, '', 'SSL').'" title="">&raquo; '.SAC_LINK_SHOPPCART.'</a> &nbsp; | &nbsp; <a href="'.xtc_href_link(FILENAME_DEFAULT).'" title="">&raquo; '.SAC_LINK_START.'</a>'
: 
'<a href="'.xtc_href_link(FILENAME_DEFAULT, '', 'NONSSL').'" title="">&raquo; '.SAC_LINK_START.'</a>'; ?>
</p>
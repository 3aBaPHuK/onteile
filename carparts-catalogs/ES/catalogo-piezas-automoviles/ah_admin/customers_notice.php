<?php
/**
 * Manage Customers notice
 * 
 * @author    Timo Paul <mail@timopaul.biz>
 * @copyright (c) copyright 2014, Timo Paul Dienstleistungen
 * @license   http://www.gnu.org/licenses/gpl-2.0.html
 *            GNU General Public License (GPL), Version 2.0
 *
 * ================================================================
 *
 * reworked by noRiddle 03-2020
 * added use of modified standard objectinfo
 * added checkbox to check all customer groups at once
 * use datetimepicker instead of spiffyCal (which btw. didn't work anyway)
 * styled a bit in lower section in edit mode
 */

define('MODULE_CUSTOMERS_NOTICE_PROJECT', 'modified eCommerce CustomersNotice');
define('MODULE_CUSTOMERS_NOTICE_VERSION', 'nr_customers-notice_revorked_0.1');

require_once 'includes/application_top.php';

require_once DIR_FS_INC . 'xtc_wysiwyg.inc.php';

function formatCustomersNoticeDate($datetime) {
  if ('0000-00-00 00:00:00' == $datetime) {
    return '-';
  }
  return date('d.m.Y H:i:s', strtotime($datetime));
}

// get all customer statuses
$customers_statuses_array = array();
foreach (xtc_get_customers_statuses() as $s) {
  $customers_statuses_array[$s['id']] = $s;
}

// get available languages
require_once DIR_WS_CLASSES . 'language.php';
$languages = xtc_get_languages();

// create notice
$notice = array(
  'position' => 1,
  'startdate' => date('Y-m-d H:i:s'),
  'template' => 'default.html',
  'customers_status' => array(),
  'pages' => array(),
  'lang' => array(),
);
if (key_exists('nid', $_GET) && '' != $_GET['nid']) {
  $stmt = 'SELECT * ' . 
          'FROM ' . TABLE_CUSTOMERS_NOTICE . ' ' .
          'WHERE customers_notice_id = ' . xtc_db_input($_GET['nid']);
  $query = xtc_db_query($stmt);
  if ($row = xtc_db_fetch_array($query)) {
    $notice = $row;
    $notice['customers_status'] = explode(',', $notice['customers_status']);
    $notice['pages'] = explode(',', $notice['pages']);
    $stmt = 'SELECT * ' .
            'FROM ' . TABLE_CUSTOMERS_NOTICE_DESCRIPTION . ' ' .
            'WHERE customers_notice_id = ' . xtc_db_input($_GET['nid']);
    $query = xtc_db_query($stmt);
    while ($row = xtc_db_fetch_array($query)) {
      $notice['lang'][$row['languages_id']] = $row;
    }
  }
}

$action = key_exists('action', $_GET) ? $_GET['action'] : false;

switch ($action) {

  case 'update':
  case 'insert':
    
    // get values from request
    if (key_exists('customers_notice_id', $_POST)) {
      $notice['customers_notice_id'] = xtc_db_input($_POST['customers_notice_id']);
    }
    if (key_exists('status', $_POST)) {
      $notice['status'] = xtc_db_input($_POST['status']);
    }
    if (key_exists('position', $_POST)) {
      $notice['position'] = xtc_db_input($_POST['position']);
    }
    if (key_exists('startdate', $_POST)) {
      $notice['startdate'] = xtc_db_input($_POST['startdate']);
    }
    if (key_exists('enddate', $_POST)) {
      $notice['enddate'] = xtc_db_input($_POST['enddate']);
    }
    if (key_exists('template', $_POST)) {
      $notice['template'] = xtc_db_input($_POST['template']);
    }
    if (key_exists('customers_status', $_POST) && is_array($_POST['customers_status'])) {
      foreach ($_POST['customers_status'] as $cs) {
        $notice['customers_status'][] = xtc_db_input($cs);
      }
    }
    if (key_exists('pages', $_POST) && is_array($_POST['pages'])) {
      foreach ($_POST['pages'] as $p) {
        $notice['pages'][] = xtc_db_input($p);
      }
    }
    
    foreach ($languages as $l) {
      $notice['lang'][$l['id']] = array();
      if (key_exists('title', $_POST) && key_exists($l['id'], $_POST['title'])) {
        $notice['lang'][$l['id']]['title'] = $_POST['title'][$l['id']];
      }
      if (key_exists('description', $_POST) && key_exists($l['id'], $_POST['description'])) {
        $notice['lang'][$l['id']]['description'] = $_POST['description'][$l['id']];
      }
    }

    // check values
    if ('' == trim($_REQUEST['title'][$_SESSION['languages_id']])) {
      $messageStack->add(ERROR_MISSING_TITLE, 'error');
    }
    if ('' == trim($_REQUEST['description'][$_SESSION['languages_id']])) {
      $messageStack->add(ERROR_MISSING_DESCRIPTION, 'error');
    }
    //BOC different format because of datetimepicker, noRiddle
    //$datetimeFormat = '#^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}\s[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}$#';
    $datetimeFormat2 = '#^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}\s[0-9]{1,2}:[0-9]{1,2}[:]*[0-9]{0,2}$#'; //new format because of use of datepicker without seconds, noRiddle
    //if ('' != trim($_REQUEST['startdate']) && !preg_match($datetimeFormat, $_REQUEST['startdate'])) {
    if ('' != trim($_REQUEST['startdate']) && !preg_match($datetimeFormat2, $_REQUEST['startdate'])) {
      $messageStack->add(ERROR_INVALID_STARTDATE, 'error');
    }
    //if ('' != trim($_REQUEST['enddate']) && !preg_match($datetimeFormat, $_REQUEST['enddate'])) {
    if ('' != trim($_REQUEST['enddate']) && !preg_match($datetimeFormat2, $_REQUEST['enddate'])) {
      $messageStack->add(ERROR_INVALID_ENDDATE, 'error');
    }
    //EOC different format because of datetimepicker, noRiddle
    if (0 != $messageStack->size) {
      $action = 'edit';
      break;
    }
    
    // format values
    if (!xtc_not_null($notice['position']) || !is_int((int) $notice['position'])) {
      $notice['position'] = 1;
    } elseif (1 > (int) $notice['position']) {
      $notice['position'] = 1;
    } elseif (999 < (int) $notice['position']) {
      $notice['position'] = 999;
    }

    // insert or update
    $update = '' != $notice['customers_notice_id'];
    $sqlData = array(
      'status'            => $notice['status'],
      'position'          => $notice['position'],
      'startdate'         => $notice['startdate'],
      'enddate'           => $notice['enddate'],
      'template'          => $notice['template'],
      'customers_status'  => implode(',', $notice['customers_status']),
      'pages'             => implode(',', $notice['pages']),
    );
    
    xtc_db_perform(TABLE_CUSTOMERS_NOTICE, $sqlData, ($update ? 'update' : 'insert'), 'customers_notice_id = ' . $notice['customers_notice_id']);
    if (!$update) {
      $notice['customers_notice_id'] = xtc_db_insert_id();
    }
    foreach ($notice['lang'] as $languages_id => $lang) {
      $sqlData = array(
        'title' => xtc_db_prepare_input($lang['title']),
        'description' => xtc_db_prepare_input($lang['description']),
        'languages_id' => $languages_id,
      );
      if (!$update) {
        $sqlData['customers_notice_id'] = $notice['customers_notice_id'];
      }
      xtc_db_perform(TABLE_CUSTOMERS_NOTICE_DESCRIPTION, $sqlData, ($update ? 'update' : 'insert'), 'customers_notice_id = ' . $notice['customers_notice_id'] . ' AND languages_id = ' . $languages_id);
    }
    if (isset($_POST['save']) && BUTTON_SAVE == $_POST['save']) {
      xtc_redirect(xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action', 'flag')) . 'nid=' . $notice['customers_notice_id']));
    } else {
      $action = 'edit';
    }
    break;
    
  // update status
  case 'updatestatus':
    if (key_exists('flag', $_GET) && xtc_not_null($_GET['flag']) && key_exists('nid', $_GET) && xtc_not_null($_GET['nid'])) {
      $stmt = 'UPDATE ' . TABLE_CUSTOMERS_NOTICE . ' ' .
              'SET status = ' . (int) $_GET['flag'] . ' ' .
              'WHERE customers_notice_id = ' . (int) $_GET['nid'];
      xtc_db_query($stmt);
      xtc_redirect(xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action', 'flag'))));
    }
    break;
    
  // delete
  case 'delete-confirm': 
    $stmt = 'DELETE FROM ' . TABLE_CUSTOMERS_NOTICE . ' ' .
            'WHERE customers_notice_id = ' . (int) $_GET['nid'];
    xtc_db_query($stmt);
    $stmt = 'DELETE FROM ' . TABLE_CUSTOMERS_NOTICE_DESCRIPTION . ' ' .
            'WHERE customers_notice_id = ' . (int) $_GET['nid'];
    xtc_db_query($stmt);
    xtc_redirect(xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action', 'flag'))));
    
    break;
  
  // update position
  case 'posup': 
  case 'posdown': 
    $stmt = 'UPDATE ' . TABLE_CUSTOMERS_NOTICE . ' ' .
            'SET position = position ' . ('posup' == $action ? '+' : '-') . ' 1 ' .
            'WHERE customers_notice_id = ' . (int) $_GET['nid'];
    xtc_db_query($stmt);
    xtc_redirect(xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action'))));
    break;
}

require DIR_WS_INCLUDES . 'head.php';

// Include WYSIWYG if is activated
if (USE_WYSIWYG == 'true') {
	$query = xtc_db_query("SELECT code FROM ".TABLE_LANGUAGES." WHERE languages_id='".(int)$_SESSION['languages_id']."'");
	$data = xtc_db_fetch_array($query);
	// generate editor 
	echo PHP_EOL . (!function_exists('editorJSLink') ? '<script type="text/javascript" src="includes/modules/fckeditor/fckeditor.js"></script>' : '') . PHP_EOL;
	//if ($_GET['action'] == 'new'  || $_GET['action'] == 'edit')
	if (in_array($action, array('new', 'edit'))) {
	  for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      echo xtc_wysiwyg('customers_notice', $data['code'], $languages[$i]['id']);
	  }
	}
}
?>
<?php //throw out spiffyCal and use datetimepicker (see footer), noRiddle ?>
    <style>
    small {font-size: 75%;}
    .dataTableContent {vertical-align:top !important;} /*noRiddle*/
    </style>
</head>
<body>
    <!-- header //-->
    <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
    <!-- header_eof //-->
    <!-- body //-->
    <table class="tableBody">
      <tr>
        <?php //left_navigation
        if (USE_ADMIN_TOP_MENU == 'false') {
          echo '<td class="columnLeft2">'.PHP_EOL;
          echo '<!-- left_navigation //-->'.PHP_EOL;       
          require_once(DIR_WS_INCLUDES . 'column_left.php');
          echo '<!-- left_navigation eof //-->'.PHP_EOL; 
          echo '</td>'.PHP_EOL;      
        }
        ?>
        <!-- body_text //--> 
        <td class="boxCenter">
          <div class="pageHeadingImage"><?php echo xtc_image(DIR_WS_ICONS.'heading/icon_news.png'); ?></div>
          <div class="pageHeading">
            <?php echo HEADING_TITLE; ?><br /><span style="font-size:12px; line-height:14px;">
            <?php 
			switch ($action) {
				case 'new':
				  echo HEADING_SUBTITLE_NEW_NOTICE;
				break;
				case 'edit':
				  echo sprintf(HEADING_SUBTITLE_EDIT_NOTICE, strip_tags($notice['lang'][$_SESSION['languages_id']]['title']));
				break;
				default:
				  echo HEADING_SUBTITLE;
				break;
				}
			?>
            </span>
          </div>
          <p style="clear:both; font-size:10px; line-height:12px;">Version: <?php echo MODULE_CUSTOMERS_NOTICE_VERSION; ?><br />Original version by TimoPaul<br />Contributions: karsta (kgd), noRiddle</p>
                <?php if (in_array($action, array('new', 'edit'))) { ?>
                  <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin-top: 15px;">
                    <tr>
                      <td class="main">
                        <?php $update = 1 < count($notice) && key_exists('customers_notice_id', $notice) && '' != trim($notice['customers_notice_id']); ?>
                        <?php echo xtc_draw_form('notice', FILENAME_CUSTOMERS_NOTICE, 'action=' . ($update ? 'update' : 'insert'), 'POST'); ?>
                          <?php
                            echo xtc_draw_hidden_field(xtc_session_name(), xtc_session_id());
                            echo xtc_draw_hidden_field('action', ($update ? 'update' : 'insert'));
                            if ($update) {
                              echo xtc_draw_hidden_field('customers_notice_id', $notice['customers_notice_id']);
                            }
                          ?>
                          <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="formArea">
                                
                                <div style="width: 860px; padding: 5px;" class="cf">
                                  <link rel="stylesheet" type="text/css" href="includes/lang_tabs_menu/lang_tabs_menu.css">
                                  <script type="text/javascript" src="includes/lang_tabs_menu/lang_tabs_menu.js"></script>
                                  <?php
                                    $langtabs = '<div class="tablangmenu"><ul>';
                                    $csstabstyle = 'border: 1px solid #aaaaaa; padding: 5px; width: 850px; margin-top: -1px; margin-bottom: 10px; float: left; background-color: #F3F3F3;';
                                    $csstab = '<style type="text/css">';
                                    $csstab_nojs = '<style type="text/css">';
                                    foreach ($languages as $i => $l) {
                                      $tabtmp = "\'tab_lang_{$i}\',";
                                      $langtabs.= '<li onclick="showTab('. $tabtmp . count($languages) . ')" style="cursor: pointer;" id="tabselect_' . $i . '">' . xtc_image(DIR_WS_LANGUAGES . $l['directory'] . '/admin/images/' . $l['image'], $l['name']) . ' ' . $l['name'] . '</li>';
                                      $csstab .= '#tab_lang_' . $i . '{display: ' . (0 == $i ? 'block' : 'none') . '; ' . $csstabstyle . '}';
                                      $csstab_nojs .= '#tab_lang_' . $i . '{display: block;' . $csstabstyle . '}';
                                    }
                                    $csstab .= '</style>';
                                    $csstab_nojs .= '</style>';
                                    $langtabs.= '</ul></div>';
                                  ?>
                                  <?php if (USE_ADMIN_LANG_TABS != 'false') { ?>
                                    <script type="text/javascript">
                                      document.write('<?php echo $csstab; ?>');
                                      document.write('<?php echo $langtabs; ?>');
                                    </script>
                                  <?php } else { ?>
                                    <?php echo $csstab_nojs; ?>
                                  <?php } ?>
                                  <noscript>
                                    <?php echo $csstab_nojs;?>
                                  </noscript>
                                  <?php
                                    foreach ($languages as $i => $l) {
                                      echo ('<div id="tab_lang_' . $i . '">');
                                      $lng_image = xtc_image(DIR_WS_LANGUAGES . $l['directory'] . '/admin/images/' . $l['image'], $l['name']);
                                      ?>
                                      <div style="background: #000000; height: 10px; ">&nbsp;</div>
                                      <div class="main" style="background: #FFCC33; padding: 3px; line-height: 20px;">
                                        <?php echo $lng_image ?>&nbsp;<b><?php echo LABEL_TITLE; ?>&nbsp;</b>
                                        <?php echo xtc_draw_input_field('title[' . $l['id'] . ']', $notice['lang'][$l['id']]['title'], 'style="width: 80%" maxlength="255"'); ?>
                                      </div>
                                      <div class="main" style="padding: 3px; line-height:20px;">
                                         <b><?php echo $lng_image . '&nbsp;' . LABEL_DESCRIPTION; ?></b><br />
                                         <?php echo xtc_draw_textarea_field('description[' . $l['id'] . ']', 'soft', '100', '10', $notice['lang'][$l['id']]['description']); ?>
                                      </div>
                                      <?php
                                      echo ('</div>');
                                    }
                                  ?>
                                </div>

                                <table border="0" cellpadding="2" cellspacing="2" style="width: 860px; ">
                                  <tr>
                                    <td class="dataTableContent"><strong><?php echo LABEL_STATUS; ?></strong></td>
                                    <td class="dataTableContent">
                                      <?php 
                                        $values = array(
                                          array('id' => 1, 'text' => YES),
                                          array('id' => 0, 'text' => NO),
                                        );
                                        $GLOBALS['status'] = $notice['status']; // fix to select default value
                                        echo xtc_draw_pull_down_menu('status', $values, $notice['status']);
                                      ?>
                                    </td>
                                    <td class="dataTableContent"><strong><?php echo LABEL_POSITION; ?></strong></td>
                                    <td class="dataTableContent">
                                      <?php echo xtc_draw_input_field('position', $notice['position'], 'maxlength="3" style="width: 50px; "'); ?>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="dataTableContent"><strong><?php echo LABEL_STARTDATE; ?></strong><br /><small>(<?php echo DATETIME_FORMAT; ?>)</small></td>
                                    <td class="dataTableContent">
                                      <?php
                                      //echo xtc_draw_input_field('startdate', $notice['startdate'], 'style="width: 150px;" id="csn-startdate"');
                                      echo xtc_draw_input_field('startdate', ($notice['startdate'] == '0000-00-00 00:00:00' ? '' : $notice['startdate']), 'style="width: 150px;" id="csn-startdate"');
                                      ?>
                                    </td>
                                    <td class="dataTableContent"><strong><?php echo LABEL_ENDDATE; ?></strong><br /><small>(<?php echo DATETIME_FORMAT; ?>)</small></td>
                                    <td class="dataTableContent">
                                      <?php
                                      //echo xtc_draw_input_field('enddate', $notice['enddate'], 'style="width: 150px;" id="csn-enddate"');
                                      echo xtc_draw_input_field('enddate', ($notice['enddate'] == '0000-00-00 00:00:00' ? '' : $notice['enddate']), 'style="width: 150px;" id="csn-enddate"');
                                      ?>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="dataTableContent"><strong><?php echo LABEL_TEMPLATE; ?></strong></td>
                                    <td class="dataTableContent">
                                      <?php
                                        $path = DIR_FS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/module/customers_notice/';
                                        $templates = array();
                                        foreach (glob($path . '*.html') as $file) {
                                          $templates[] = array(
                                            'id' => basename($file),
                                            'text' => basename($file)
                                          );
                                        }
                                        echo xtc_draw_pull_down_menu('template', $templates, $notice['template']);
                                      ?>
                                    </td>
                                    <td class="dataTableContent" colspan="2"><?php echo LABEL_TEMPLATE_HINT; ?></td>
                                  </tr>
                                  <tr>
                                    <td class="dataTableContent" width="20%"><strong><?php echo LABEL_CUSTOMERS_GROUPS; ?></strong><br /><small>(<?php echo TEXT_OPTIONAL; ?>)</small></td>
                                    <td class="dataTableContent" width="30%">
                                      <?php
                                        //BOC new checkbox to check/uncheck all customer groups at once, noRiddle
                                        echo xtc_draw_selection_field('all_cst', 'checkbox', '', '', '', 'id="chck-all-cst"') . ' <label for="chck-all-cst">All</label><br />';
                                        //BOC new checkbox to check/uncheck all customer groups at once, noRiddle
                                        foreach ($customers_statuses_array as $g) {
                                          //BOC use label for more comfort when checking checkboxes, noRiddle
                                          //echo xtc_draw_selection_field('customers_status[]', 'checkbox', $g['id'], in_array($g['id'], $notice['customers_status'])) . ' ' . $g['text'] . '<br />';
                                          echo xtc_draw_selection_field('customers_status[]', 'checkbox', $g['id'], in_array($g['id'], $notice['customers_status']), '', 'id="cst-'.$g['id'].'"') . ' <label for="cst-'.$g['id'].'">' . $g['text'] . '</label><br />';
                                          //EOC use label for more comfort when checking checkboxes, noRiddle
                                        }
                                      ?>
                                    </td>
                                    <td class="dataTableContent" width="20%"><strong><?php echo LABEL_PAGES; ?></strong><br /><small>(<?php echo TEXT_OPTIONAL; ?>)</td>
                                    <td class="dataTableContent" width="30%">
                                      <?php
                                        //BOC new checkbox to check/uncheck all customer groups at once, noRiddle
                                        echo xtc_draw_selection_field('all_pgs', 'checkbox', '', '', '', 'id="chck-all-pgs"') . ' <label for="chck-all-pgs">All</label><br />';
                                        //BOC new checkbox to check/uncheck all customer groups at once, noRiddle
                                        foreach (array(
                                          'index',
                                          'category',
                                          'product_info',
                                          'shop_content',
                                          'shopping_cart',
                                          'account',
                                          'checkout',
                                        ) as $p) {
                                          echo xtc_draw_selection_field('pages[]', 'checkbox', $p, in_array($p, $notice['pages']), '', 'id="pg-'.$p.'"') . ' <label for="pg-'.$p.'">' . constant('FIELD_VALUE_PAGES_' . strtoupper($p)) . '</label><br />';
                                        }
                                      ?>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="center" class="dataTableContent" colspan="4">
                                      <a class="button" onclick="this.blur();" href="<?php echo xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('action'))); ?>"><?php echo BUTTON_CANCEL; ?></a>
                                      <input type="submit" name="update" class="button" onclick="this.blur();" value="<?php echo $update ? BUTTON_UPDATE : BUTTON_INSERT; ?>" />
                                      <input type="submit" name="save" class="button" onclick="this.blur();" value="<?php echo BUTTON_SAVE; ?>" />
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <br />
                        </form>
                      </td>
                    </tr>
                  </table>
                
                <?php } else { ?>
                      
                  <!-- notice table -->
<div class="main flt-l pdg2 mrg5">
<?php if (!isset($action) || !in_array($action, array('new', 'edit'))) { ?>
<a class="button" onclick="this.blur();" href="<?php echo xtc_href_link(FILENAME_CUSTOMERS_NOTICE); ?>?action=new"><?php echo BUTTON_CREATE_NOTICE; ?></a>
<?php } ?>
</div>
<div class="main flt-r pdg2 mrg5" style="margin-left:20px;">
<?php echo xtc_draw_form('search', FILENAME_CUSTOMERS_NOTICE, '', 'get'); ?>
<?php echo HEADING_TITLE_SEARCH . ' ' . xtc_draw_input_field('search', isset($_GET['search']) ? $_GET['search'] : '') . xtc_draw_hidden_field(xtc_session_name(), xtc_session_id()); ?>
</form>
</div>
<div class="main flt-r pdg2 mrg5" style="margin-right:20px;">
<?php echo xtc_draw_form('status', FILENAME_CUSTOMERS_NOTICE, '', 'get'); ?>
<?php
   $select_data = array(
        array('id' => '', 'text' => TEXT_SELECT),
        array('id' => '1', 'text' => TEXT_ACTIVE),
        array('id' => '0', 'text' => TEXT_INACTIVE),
       );
      echo HEADING_TITLE_STATUS . ' ';
      echo xtc_draw_pull_down_menu('status', $select_data, isset($_GET['status']) ? $_GET['status'] : '', 'onChange="this.form.submit();"');
      echo xtc_draw_hidden_field(xtc_session_name(), xtc_session_id());
?>
</form>
</div>

                  <table border="0" width="100%" cellspacing="0" cellpadding="2" style="margin-top: 15px;">
                    <tr>
                      <td valign="top">
                        <table border="0" width="100%" cellspacing="0" cellpadding="2">
                          <tr class="dataTableHeadingRow">
                            <td class="dataTableHeadingContent" width="40"><?php echo TABLE_HEADING_ID . xtc_sorting(FILENAME_CUSTOMERS_NOTICE, 'customers_notice_id'); ?></td>
                            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TITLE . xtc_sorting(FILENAME_CUSTOMERS_NOTICE, 'title'); ?></td>
                            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_STATUS . xtc_sorting(FILENAME_CUSTOMERS_NOTICE, 'status'); ?></td>
                            <td class="dataTableHeadingContent" width="40" colspan="2"><?php echo TABLE_HEADING_POSITION . xtc_sorting(FILENAME_CUSTOMERS_NOTICE, 'position'); ?></td>
                            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_STARTDATE . xtc_sorting(FILENAME_CUSTOMERS_NOTICE, 'startdate'); ?></td>
                            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ENDDATE . xtc_sorting(FILENAME_CUSTOMERS_NOTICE, 'enddate'); ?></td>
                            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TEMPLATE . xtc_sorting(FILENAME_CUSTOMERS_NOTICE, 'template'); ?></td>
                            <td class="dataTableHeadingContent" valign="top"><?php echo TABLE_HEADING_CUSTOMERS_STATUS; ?></td>
                            <td class="dataTableHeadingContent" valign="top"><?php echo TABLE_HEADING_PAGES; ?></td>
                            <td class="dataTableHeadingContent" valign="top">&nbsp;</td>
                          </tr>

                          <?php

                            $search = '';
                            if(isset($_GET['search']) && (xtc_not_null($_GET['search']))) {
                              $keywords = xtc_db_input(xtc_db_prepare_input($_GET['search']));
                              $search = " WHERE cnd.title LIKE '%".$keywords."%'";
                            }
                            if(isset($_GET['status']) && (xtc_not_null($_GET['status']))) {
                              $search .= ('' == $search ? " WHERE" : " AND") . " cn.status = ".(int)$_GET['status'];
                            }


                            $sort = '';
                            if (!isset($_GET['sorting']) || !xtc_not_null($_GET['sorting'])) {
                              $_GET['sorting'] = 'position';
                            }
                            $desc = '-desc' == substr($_GET['sorting'], -5);
                            $sort = preg_replace('#-desc$#', '', $_GET['sorting']);
                            $sort = " ORDER BY ".$sort.($desc ? ' DESC' : ' ASC');

                            //secure sql, added int cast to $_SESSION['languages_id'], noRiddle
                            $stmt = "SELECT cn.*, cnd.title, cnd.description
                                       FROM ".TABLE_CUSTOMERS_NOTICE." cn
                                  LEFT JOIN ".TABLE_CUSTOMERS_NOTICE_DESCRIPTION." cnd
                                         ON cn.customers_notice_id = cnd.customers_notice_id
                                        AND cnd.languages_id = ".(int)$_SESSION['languages_id']
                                        .$search
                                        .$sort;
                            $split = new splitPageResults($_GET['page'], 30, $stmt, $numrows);
                            $query = xtc_db_query($stmt);
                            while ($row = xtc_db_fetch_array($query)) {
                              //BOC added objectInfo, noRiddle
                              if((!isset($_GET['nid']) || (isset($_GET['nid']) && $_GET['nid'] == $row['customers_notice_id'])) 
                                && (!isset($cnInfo) && (isset($_GET['action']) ? substr($_GET['action'], 0, 3) != 'new' : true)))
                              {
                                $cnInfo = new objectInfo($row);
                                $cnInfo->customers_status = explode(',', $row['customers_status']);
                                $cnInfo->pages = explode(',', $row['pages']);
                              }

                              //if (isset($notice) && $notice['customers_notice_id'] == $row['customers_notice_id']) {
                              if((isset(($cnInfo)) && is_object($cnInfo)) && ($row['customers_notice_id'] == $cnInfo->customers_notice_id)) { //changed to modified standard with objectInfo (see above), noRiddle
                                echo '<tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'pointer\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $row['customers_notice_id'] . '&action=edit') . '\'">' . "\n";
                              } else {
                                echo '<tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\'; this.style.cursor=\'pointer\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $row['customers_notice_id']) . '\'">' . "\n";
                              }

                              ?>
                                <td class="dataTableContent"><?php echo $row['customers_notice_id']; ?></td>
                                <td class="dataTableContent"><?php echo $row['title']; ?></td>
                                <td class="dataTableContent">
                                  <?php
                                    if (1 == (int) $row['status']) {
                                      echo xtc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10);
                                      echo '&nbsp;&nbsp;';
                                      echo '<a href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'action=updatestatus&flag=0&nid=' . $row['customers_notice_id']) . '">';
                                        echo xtc_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10);
                                      echo '</a>';
                                    } else {
                                      echo '<a href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'action=updatestatus&flag=1&nid=' . $row['customers_notice_id']) . '">';
                                        echo xtc_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10);
                                      echo'</a>';
                                      echo '&nbsp;&nbsp;';
                                      echo xtc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                                    }
                                  ?>
                                </td>
                                <td class="dataTableContent"><?php echo $row['position']; ?></td>
                                <td class="dataTableContent">
                                  <?php
                                    if (999 > (int) $row['position']) {
                                      echo '<a href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'action=posup&nid=' . $row['customers_notice_id']) . '">';
                                        echo xtc_image(DIR_WS_IMAGES . 'arrow_up.gif', 'up', 12, 12);
                                      echo'</a>&nbsp;';
                                    } else {
                                      echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                                    }
                                    if (1 < (int) $row['position']) {
                                      echo '<a href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'action=posdown&nid=' . $row['customers_notice_id']) . '">';
                                        echo xtc_image(DIR_WS_IMAGES . 'arrow_down.gif', 'down', 12, 12);
                                      echo'</a>';
                                    }
                                  ?>
                                </td>
                                <td class="dataTableContent"><?php echo formatCustomersnoticeDate($row['startdate']); ?></td>
                                <td class="dataTableContent"><?php echo formatCustomersnoticeDate($row['enddate']); ?></td>
                                <td class="dataTableContent"><?php echo $row['template']; ?></td>
                                <td class="dataTableContent">
                                  <?php
                                    $statusNames = array();
                                    foreach(explode(',', $row['customers_status']) as $cs) {
                                      $statusNames[] = $customers_statuses_array[$cs]['text'];
                                    }
                                    $statusNames = array_filter($statusNames);
                                    //echo count($statusNames) ? implode(', ', $statusNames) : TEXT_ALL;
                                    echo (count($statusNames) > 0 && count($statusNames) != count($customers_statuses_array)) ? implode(', ', $statusNames) : TEXT_ALL; //noRiddle
                                  ?>
                                </td>
                                <td class="dataTableContent">
                                  <?php
                                    if (xtc_not_null($row['pages'])) {
                                      $pages = array();
                                      foreach (explode(',', $row['pages']) as $p) {
                                        $pages[] = constant('FIELD_VALUE_PAGES_' . strtoupper($p));
                                      }
                                      echo implode(', ', $pages);
                                    } else {
                                      echo TEXT_ALL;
                                    }
                                  ?>
                                </td>
                                <td class="dataTableContent">
                                  <?php
                                    //if (isset($notice) && $notice['customers_notice_id'] == $row['customers_notice_id']) {
                                    if((isset($cnInfo) && is_object($cnInfo)) && ($row['customers_notice_id'] == $cnInfo->customers_notice_id)) { //changed to modified standard with objectInfo (see above), noRiddle
                                      echo xtc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ICON_ARROW_RIGHT);
                                    } else {
                                      echo '<a href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $row['customers_notice_id']) . '">' . xtc_image(DIR_WS_IMAGES . 'icon_arrow_grey.gif', IMAGE_ICON_INFO) . '</a>';
                                    }
                                  ?>
                                </td>
                                </tr>
                              <?php
                            }

                          ?>
                        </table>
                      </td>
                      <?php
                        //if ($notice && count($notice['lang'])) { //commented out, we don't need that, noRiddle
                          $heading = array();
                          $contents = array();

                          switch ($action) {
                            //BOC changed to modified standard with objectinfo, noRiddle
                            // confirm delete
                            case 'delete':
                              $heading[] = array('text' => '<b>' . HEADING_BOX_TITLE_DELETE . '</b>');
                              $contents[] = array('text' => sprintf(TEXT_DELETE_NOTICE_CONFIRM, $cnInfo->title)); //$notice['lang'][$_SESSION['languages_id']]['title'])); //objectinfo, noRiddle
                              //$buttons[] = '<a class="button" onclick="this.blur()" href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $notice['customers_notice_id']) . '">' . BUTTON_CANCEL . '</a>';
                              $buttons[] = '<a class="button" onclick="this.blur()" href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $cnInfo->customers_notice_id) . '">' . BUTTON_CANCEL . '</a>'; //objectinfo, noRiddle
                              //$buttons[] = '<a class="button" onclick="this.blur()" href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $notice['customers_notice_id'] . '&action=delete-confirm') . '">' . BUTTON_DELETE_NOTICE_CONFIRMATION . '</a>';
                              $buttons[] = '<a class="button" onclick="this.blur()" href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $cnInfo->customers_notice_id . '&action=delete-confirm') . '">' . BUTTON_DELETE_NOTICE_CONFIRMATION . '</a>'; //objectinfo, noRiddle
                              $contents[] = array(
                                'align' => 'center',
                                'text' => implode(' ', $buttons)
                              );
                              break;

                            // info
                            default:
                                if(is_object($cnInfo)) { //added objectInfo (see above), noRiddle
                                  $heading[] = array('text' => '<b>' . sprintf(HEADING_BOX_TITLE_DEFAULT,  $cnInfo->title). '</b>'); //$notice['lang'][$_SESSION['languages_id']]['title']) . '</b>'); //objectinfo, noRiddle
                                  $contents[] = array('text' => '<strong>'.LABEL_STATUS.'</strong> '.(1 == (int) $cnInfo->status ? YES : NO)); //(1 == (int) $notice['status'] ? YES : NO));  //objectinfo, noRiddle
                                  $contents[] = array('text' => '<strong>'.LABEL_POSITION.'</strong> '.$cnInfo->position); //$notice['position']);  //objectinfo, noRiddle
                                  $contents[] = array('text' => '<strong>'.LABEL_STARTDATE.'</strong> '.formatCustomersnoticeDate($cnInfo->startdate)); //formatCustomersnoticeDate($notice['startdate']));  //objectinfo, noRiddle
                                  $contents[] = array('text' => '<strong>'.LABEL_ENDDATE.'</strong> '.formatCustomersnoticeDate($cnInfo->enddate)); //formatCustomersnoticeDate($notice['enddate']));  //objectinfo, noRiddle
                                  $contents[] = array('text' => '<strong>'.LABEL_TEMPLATE.'</strong> '.$cnInfo->template); //$notice['template']);  //objectinfo, noRiddle
                                  $groups = array();
                                  //foreach ($notice['customers_status'] as $cs) {
                                  foreach($cnInfo->customers_status as $cs) { //objectinfo, noRiddle
                                    $groups[] = $customers_statuses_array[$cs]['text'];
                                  }
                                  $groups = array_filter($groups);
                                  //$contents[] = array('text' => '<strong>' . LABEL_CUSTOMERS_GROUPS . '</strong> ' . (count($groups) ? '<br /> - ' . implode('<br /> - ', $groups) : TEXT_ALL));
                                  $contents[] = array('text' => '<strong>' . LABEL_CUSTOMERS_GROUPS . '</strong> ' . (count($groups) > 0 && count($groups) != count($customers_statuses_array) ? '<br /> - ' . implode('<br /> - ', $groups) : TEXT_ALL)); //noRiddle
                                  $pages = array();
                                  //foreach (array_filter($notice['pages']) as $p) {
                                  foreach (array_filter($cnInfo->pages) as $p) { //objectinfo, noRiddle
                                    $pages[] = constant('FIELD_VALUE_PAGES_' . strtoupper($p));
                                  }
                                  $contents[] = array('text' => '<strong>' . LABEL_PAGES . '</strong> ' . (count($pages) ? '<br /> - ' . implode('<br /> - ', $pages) : TEXT_ALL));
                                  //$contents[] = array('text' => '<strong>' . LABEL_DESCRIPTION . '</strong><br />' . nl2br($notice['lang'][$_SESSION['languages_id']]['description']));
                                  $contents[] = array('text' => '<strong>' . LABEL_DESCRIPTION . '</strong><br />' . nl2br((strlen(strip_tags($cnInfo->description)) > 30 ? substr(strip_tags($cnInfo->description),0,30).' ...' : strip_tags($cnInfo->description)))); //objectinfo and added substr, strip_tags, noRiddle
                                  $buttons = array();
                                  //$buttons[] = '<a class="button" onclick="this.blur()" href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $notice['customers_notice_id'] . '&action=edit') . '">' . BUTTON_EDIT_NOTICE . '</a>';
                                  $buttons[] = '<a class="button" onclick="this.blur()" href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $cnInfo->customers_notice_id . '&action=edit') . '">' . BUTTON_EDIT_NOTICE . '</a>'; //objectinfo, noRiddle
                                  //$buttons[] = '<a class="button" onclick="this.blur()" href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $notice['customers_notice_id'] . '&action=delete') . '">' . BUTTON_DELETE_NOTICE . '</a>';
                                  $buttons[] = '<a class="button" onclick="this.blur()" href="' . xtc_href_link(FILENAME_CUSTOMERS_NOTICE, xtc_get_all_get_params(array('nid', 'action')) . 'nid=' . $cnInfo->customers_notice_id . '&action=delete') . '">' . BUTTON_DELETE_NOTICE . '</a>'; //objectinfo, noRiddle
                                  $contents[] = array(
                                    'align' => 'center',
                                    'text' => implode(' ', $buttons)
                                  );
                                }
                              break;
                              //EOC changed to modified standard with objectinfo, noRiddle
                          }
                          // display box
						  if ( (xtc_not_null($heading)) && (xtc_not_null($contents)) ) {
							echo '            <td class="boxRight">' . "\n";
							$box = new box;
							echo $box->infoBox($heading, $contents);
							echo '            </td>' . "\n";
                          }    
                        //}  //commented out, we don't need that, noRiddle
                      ?>
                    </tr>
                  </table>
                <?php } // end of if-else (in_array($action, array('new', 'edit'))) ?>
 </td>
        <!-- body_text_eof //-->
      </tr>
    </table>
    <!-- body_eof //-->
    <!-- footer //-->
    <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
    <!-- footer_eof //-->
    <br />
    <?php //BOC add datetimepicker for date fields, noRiddle ?>
    <link type="text/css" href="includes/javascript/jQueryDateTimePicker/jquery.datetimepicker.css" rel="stylesheet" />
    <script type="text/javascript" src="includes/javascript/jQueryDateTimePicker/jquery.datetimepicker.full.min.js"></script>
    <script type="text/javascript">
    $(function(){
        $.datetimepicker.setLocale('<?php echo $_SESSION["language_code"]; ?>');

        $('#csn-startdate').datetimepicker({
            dayOfWeekStart:1,
            format:'Y-m-d H:i',
            scrollInput:false
        });
        $('#csn-enddate').datetimepicker({
            dayOfWeekStart:1,
            format:'Y-m-d H:i',
            scrollInput:false
        });
    });
    </script>
    <?php //EOC add datetimepicker for date fields, noRiddle ?>
    <?php //BOC some JS to check all customer groups at once, noRiddle ?>
    <script>
    /* <![CDATA[ */
    $(function(){
        var $cust_statuses = $('input[name="customers_status[]"]'),
            $pages = $('input[name="pages[]"]');
        $('#chck-all-cst').click(function() {
            $cust_statuses.prop('checked', $(this).is(':checked') );
        });
        $('#chck-all-pgs').click(function() {
            $pages.prop('checked', $(this).is(':checked') );
        });
    });
    </script>
    <?php //EOC some JS to check all customer groups at once, noRiddle ?>
  </body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
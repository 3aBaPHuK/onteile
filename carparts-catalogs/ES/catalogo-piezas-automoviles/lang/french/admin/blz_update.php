<?php
/* --------------------------------------------------------------
   $Id: blz_update.php 10838 2017-07-10 14:02:46Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Actualiser les codes bancaires de la Bundesbank allemande');
define('BLZ_INFO_TEXT', '<p>Cette formulaire actualise les tableaux des codes bancaires de la modified eCommerce Shopsoftware. Le tableau est utilisé pendant la commande afin de vérifier les codes bancaires.<br/> La Bundesbank met à disposition des données actualisées tous les trois mois. </p><p><strong>Remarques mise à jour:</strong></p><p>Pour actualiser, ouvrez <a href="https://www.bundesbank.de/Redaktion/DE/Standardartikel/Aufgaben/Unbarer_Zahlungsverkehr/bankleitzahlen_download.html" target="_blank"><strong>Site téléchargement codes bancaires de la Bundesbank</strong></a> dans un nouveau onglet. Dans le tiers inférieur de la page web de la Bundesbank, vous trouvez sous "Bankleitzahlendateien ungepackt" un lien de téléchargement pour  le fichier actualisé des codes bancaires en format texte (TXT).  Veuillez copier ce lien (clic droit sur le lien, puis copier l‘adresse du lien) et entrer dans ce champ. </p><p> Le bouton „Actualiser“ démarre le processus d’actualisation.<br/>L‘actualisation dure quelques secondes. </p><p><i>Lien exemple pour la période du 05.06.2017 au 03.09.2017:</i></p>');
define('BLZ_LINK_NOT_GIVEN_TEXT', '<span class="messageStackError">Aucun lien web vers le fichier des codes bancaires a été indiqué! </span><br /><br />');
define('BLZ_LINK_INVALID_TEXT', '<span class="messageStackError">Lien web vers le fichier des codes bancaires est invalide.<br/><br/> Seulement des fichiers TXT de la page d\'accueil de la Bundesbank (www.bundesbank.de) sont autorisés!</span><br /><br />');
define('BLZ_DOWNLOADED_COUNT_TEXT', 'Nombre des codes bancaires identifiés <u> précisement</u> (sans doubles!)');
define('BLZ_PHP_FILE_ERROR_TEXT', '<p><strong><span class="messageStackError"> Le paramètre PHP "allow_url_fopen" est désactivé ("off"). Il est nécessaire pour la fonction PHP <i>file()</i>. Afin d\'exécuter l\'actualisation automatiquement, changer le paramètre vers "on". </span></strong></p>');
define('BLZ_UPDATE_SUCCESS_TEXT', 'L\'ensemble de données a été sauvegardé avec succès!');
define('BLZ_UPDATE_ERROR_TEXT', 'Une erreur est survenue!');
define('BLZ_LINK_ERROR_TEXT', '<span class="messageStackError">Le lien que vous avez indiqué n‘existe pas! Veuillez vérifier le lien au sein du champ d‘édition sur la page suivante.</span>');
define('BLZ_LINES_PROCESSED_TEXT', 'Ensemble des codes bancaire enregistré.');
define('BLZ_SOURCE_TEXT', 'Source:');
?>

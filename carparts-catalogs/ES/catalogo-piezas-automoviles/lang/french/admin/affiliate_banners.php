<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_banners.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_banners.php, v 1.3 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Programme d\'affiliation gestion des bannières');

define('TABLE_HEADING_BANNERS', 'Bannière');
define('TABLE_HEADING_GROUPS', 'Groupe');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATISTICS', 'Statistiques');
define('TABLE_HEADING_PRODUCT_ID', 'Lien sur');

define('TEXT_BANNERS_TITLE', 'Titre de la bannière:');
define('TEXT_BANNERS_GROUP', 'Groupe de bannière:');
define('TEXT_BANNERS_NEW_GROUP', ', ou entrer un nouveau groupe de bannière');
define('TEXT_BANNERS_IMAGE', 'Image:');
define('TEXT_BANNERS_IMAGE_LOCAL', ', ou entrer le fichier local en dessous');
define('TEXT_BANNERS_IMAGE_TARGET', 'But d\'image (Sauvegarder):');
define('TEXT_BANNERS_HTML_TEXT', 'texte HTML:');
define('TEXT_BANNERS_TEXT', 'lien texte:');
define('TEXT_AFFILIATE_BANNERS_NOTE', '<b>Remarque bannière:</b><ul><li>Vous pouvez utiliser des bannières d\'image ou de texte HTML. Les deux en même temps ne sont pas possible.</li><li> Si vous utilisez les deux types de bannière en même temps, seulement la bannière de texte HTML sera affichée.</li></ul>');

define('TEXT_BANNERS_LINKED_PRODUCT', 'Lien vers: ');
define('TEXT_BANNERS_LINKED_HOME', '--page d\'accueil--');
define('TEXT_BANNERS_LINKED_PRODUCT_NOTE', 'Si vous souhaitez lier la bannière à une partie particulière de votre boutique, choisissez-la ci-dessus. P désigne produit, C désigne catégorie et M désigne contenue de boutique. ');

define('TEXT_BANNERS_DATE_ADDED', 'Ajouté le:');
define('TEXT_BANNERS_STATUS_CHANGE', 'Statut changé: %s');

define('TEXT_INFO_DELETE_INTRO', 'Vous en êtes certain, vous voulez supprimer la bannière?');
define('TEXT_INFO_DELETE_IMAGE', 'Supprimer l\'image de la bannière');

define('SUCCESS_BANNER_INSERTED', 'Succès: la bannière a été insérée.');
define('SUCCESS_BANNER_UPDATED', 'Succès: la bannière a été actualisée.');
define('SUCCESS_BANNER_REMOVED', 'Succès: la bannière a été supprimé.');

define('ERROR_BANNER_TITLE_REQUIRED', 'Erreur: Un titre de bannière est exigé.');
define('ERROR_BANNER_GROUP_REQUIRED', 'Erreur: Un groupe de bannière est exigé.');
define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Répertoire cible n\'existe pas %s.');
define('ERROR_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Erreur: Répertoire cible n\'est pas définable: %s');
define('ERROR_IMAGE_DOES_NOT_EXIST', 'Erreur: Image n\'existe pas.');
define('ERROR_IMAGE_IS_NOT_WRITEABLE', 'Erreur: l\'image ne peut pas être supprimée.');
?>

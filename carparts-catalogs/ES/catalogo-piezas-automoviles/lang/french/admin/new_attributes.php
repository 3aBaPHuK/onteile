<?php
/* --------------------------------------------------------------
   $Id: new_attributes.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on:
   (c) 2002-2003 osCommerce coding standards www.oscommerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('SORT_ORDER', 'Ordre');
define('ATTR_MODEL', 'N° d\'article');
define('ATTR_STOCK', 'Stock');
define('ATTR_WEIGHT', 'Poids');
define('ATTR_PREFIXWEIGHT', 'Préfixe Poids');
define('ATTR_PRICE', 'Prix');
define('ATTR_PREFIXPRICE', 'Préfixe du prix');
define('DL_COUNT', 'Téléchargements possibles:');
define('DL_EXPIRE', 'Temps de téléchargement (jours):');
define('TITLE_EDIT', 'Editer les attributs');
define('TITLE_UPDATED', 'Les attributs de produits ont été actualisés avec succès.');
define('SELECT_PRODUCT', 'Veuillez choisir un produit de la liste à éditer:');
define('SELECT_COPY', 'Les attributs existants peuvent également être copiés sur d\'autres produits. Choisissez le produit d\'origine pour ceci:');

// BOF - Tomcraft - 2009-11-11 - NEW SORT SELECTION
define('TEXT_OPTION_ID', 'ID de l\'option');
define('TEXT_OPTION_NAME', 'Nom de lôption');
define('TEXT_SORTORDER', 'Tri');
// EOF - Tomcraft - 2009-11-11 - NEW SORT SELECTION

define('ATTR_EAN', 'GTIN/EAN');
define('ATTR_SAVE_ACTIVE', 'Enregistrer les attributs dépliés');
  
define('ATTR_VPE', 'Unité de conditionnement');
?>

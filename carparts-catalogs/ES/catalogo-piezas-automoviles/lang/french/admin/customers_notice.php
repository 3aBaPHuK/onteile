<?php
define('HEADING_TITLE', 'Customer notice');
define('HEADING_SUBTITLE', 'Always time-controlled, for all or only some customer groups');
define('HEADING_SUBTITLE_NEW_NOTICE', 'generate new notice');
define('HEADING_SUBTITLE_EDIT_NOTICE', 'Edit notice "%s"');
define('HEADING_TITLE_SEARCH', 'Search');
define('HEADING_TITLE_STATUS', 'Status');
define('HEADING_BOX_TITLE_DEFAULT', 'notice "%s"');
define('HEADING_BOX_TITLE_DELETE', 'Delete notice');

define('BUTTON_CREATE_NOTICE', 'generate new notice');
define('BUTTON_EDIT_NOTICE', 'edit notice');
define('BUTTON_DELETE_NOTICE', 'delete notice');
define('BUTTON_DELETE_NOTICE_CONFIRMATION', 'Confirm deletion!');

define('LABEL_TITLE', 'Title:');
define('LABEL_DESCRIPTION', 'Text:');
define('LABEL_STATUS', 'Notice active ?');
define('LABEL_POSITION', 'Position:');
define('LABEL_STARTDATE', 'from:');
define('LABEL_ENDDATE', 'unil:');
define('LABEL_TEMPLATE', 'Template:');
define('LABEL_TEMPLATE_HINT', '(The template newsletter.html will be shown as a popup PopUp in case the customer is not a registrated newslatter recipient.)');
define('LABEL_CUSTOMERS_GROUP', 'Customer group:');
define('LABEL_CUSTOMERS_GROUPS', 'Customer groups:');
define('LABEL_PAGES', 'Pages:');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_TITLE', 'Title');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_POSITION', 'Pos.');
define('TABLE_HEADING_STARTDATE', 'Start');
define('TABLE_HEADING_ENDDATE', 'End');
define('TABLE_HEADING_TEMPLATE', 'Template');
define('TABLE_HEADING_CUSTOMERS_STATUS', 'Customer groups');
define('TABLE_HEADING_PAGES', 'Pages');

define('ERROR_MISSING_TITLE', 'Please add a title to your notice.');
define('ERROR_MISSING_DESCRIPTION', 'Please add some text for the notice.');
define('ERROR_INVALID_STARTDATE', 'The start date is invalid, please obeserve the format.');
define('ERROR_INVALID_ENDDATE', 'The end date is invalid, please obeserve the format.');

define('DATETIME_FORMAT', 'YYYY-MM-DD HH:MM'); //took of seconds, we use datetimepicker without seconds, noRiddle

define('TEXT_ACTIVE', 'active');
define('TEXT_INACTIVE', 'inactive');
define('TEXT_DELETE_NOTICE_CONFIRM', 'Sure you want to delete the notice "%s" ?');
define('TEXT_ALL', 'all');
define('TEXT_OPTIONAL', 'optional');

define('FIELD_VALUE_PAGES_INDEX', 'start page');
define('FIELD_VALUE_PAGES_CATEGORY', 'category');
define('FIELD_VALUE_PAGES_PRODUCT_INFO', 'product details');
define('FIELD_VALUE_PAGES_SHOP_CONTENT', 'content pages');
define('FIELD_VALUE_PAGES_SHOPPING_CART', 'shopping cart');
define('FIELD_VALUE_PAGES_ACCOUNT', 'account area');
define('FIELD_VALUE_PAGES_CHECKOUT', 'chackout area');
?>
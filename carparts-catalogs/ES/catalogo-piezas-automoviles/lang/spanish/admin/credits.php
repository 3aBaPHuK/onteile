<?php
/* --------------------------------------------------------------
   $Id: credits.php 3072 2012-06-18 15:01:13Z hhacker $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(customers.php,v 1.13 2002/06/15); www.oscommerce.com
   (c) 2003 nextcommerce (customers.php,v 1.8 2003/08/15); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Créditos');
define('HEADING_SUBTITLE', 'Agradecimiento');
define('TEXT_DB_VERSION', 'Versión de base de datos:');
define('TEXT_HEADING_GPL', 'Publicado bajo la GNU General Public License (Versión 2)');

define('TEXT_INFO_GPL', 'Este programa se publica con la esperanza de que usted lo encuentre útil, pero <strong>SIN CUALQUIER GARANTÍA</strong><br /> - incluso sin la garantía implícita de <strong>VIABILIDAD COMERCIAL</strong> o la <strong>APTITUD PARA UN PROPÓSITO PARTICULAR</strong>.<br /> Puede encontrar más detalles en la Licencia Pública General de GNU.<br /><br /> Debería haber recibido una copia de la Licencia Pública General de GNU junto con este programa.<br />Si no, escriba a la Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.<br />Detalles (en inglés) aquí: <a style="font-size: 12px; text-decoration: underline;" href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">http://www.gnu.org/licenses/gpl-2.0.html</a>.');

define('TEXT_HEADING_DEVELOPERS', 'Desarrollador del modified eCommerce Shopsoftware:');
define('TEXT_HEADING_SUPPORT', 'Apoyar el desarrollo futuro:');
define('TEXT_HEADING_DONATIONS', 'Donaciones:');
define('TEXT_HEADING_BASED_ON', 'El software de la tienda se basa en:');

define('TEXT_INFO_THANKS', 'Agradecemos a todos los programadores y desarrolladores que están trabajando en este proyecto. Si hemos olvidado a alguien en la lista de abajo, por favor informe el <a style="font-size: 12px; text-decoration: underline;" href="http://www.modified-shop.org/forum/" target="_blank">Foro</a> o uno de los desarrolladores mencionados.');
define('TEXT_INFO_DISCLAIMER', 'Este programa fue publicado con la esperanza de ser útil. Sin embargo, no damos ninguna garantía de que la implementación esté libre de errores.');
define('TEXT_INFO_DONATIONS', 'El modified eCommerce Shopsoftware es un proyecto de OpenSource - ponemos mucho trabajo y tiempo libre en este proyecto y por lo tanto agradeceríamos una donación como un pequeño reconocimiento.');
define('TEXT_INFO_DONATIONS_IMG_ALT', 'Apoya este proyecto con su donación');
define('BUTTON_DONATE', '<a href="http://www.modified-shop.org/spenden"><img src="https://www.paypal.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" alt="' . TEXT_INFO_DONATIONS_IMG_ALT . '" border="0"></a>');
?>

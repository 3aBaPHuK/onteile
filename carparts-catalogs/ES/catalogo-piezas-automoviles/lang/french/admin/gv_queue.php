<?php
/* -----------------------------------------------------------------------------------------
   $Id: gv_queue.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(gv_queue.php,v 1.2.2.1 2003/04/27); www.oscommerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/


define('HEADING_TITLE', 'File d\'attente de déblocage du bon d\'achat');

define('TABLE_HEADING_CUSTOMERS', 'Client');
define('TABLE_HEADING_ORDERS_ID', 'N° de commande');
define('TABLE_HEADING_VOUCHER_VALUE', 'Valeur du coupon');
define('TABLE_HEADING_DATE_PURCHASED', 'Date de commande');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_REDEEM_COUPON_MESSAGE_HEADER', 'Vous avez récemment commandé un bon d\'achat dans notre boutique en ligne, ' . "\n"
. 'qui, pour raisons de sécurité, n\'était pas déverrouillé.' . "\n" 
                                          . 'Vous pouvez maintenant utiliser votre crédit et visiter notre boutique en ligne' . "\n"
                                          . 'et envoyer une partie du montant de votre bon d\'achat à quelqu\'un par courriel' . "\n\n");

define('TEXT_REDEEM_COUPON_MESSAGE_AMOUNT', 'Le bon d\'achat que vous avez commandé a une valeur de %s' . "\n\n");

define('TEXT_REDEEM_COUPON_MESSAGE_BODY', '');
define('TEXT_REDEEM_COUPON_MESSAGE_FOOTER', '');
define('TEXT_REDEEM_COUPON_SUBJECT', 'Acheter le bon d\'achat');
?>

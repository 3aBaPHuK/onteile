<?php
/* --------------------------------------------------------------
   $Id: new_attributes.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on:
   (c) 2002-2003 osCommerce coding standards www.oscommerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('SORT_ORDER', 'Orden');
define('ATTR_MODEL', 'Número del artículo');
define('ATTR_STOCK', 'Almacén');
define('ATTR_WEIGHT', 'Peso');
define('ATTR_PREFIXWEIGHT', 'Prefijo de peso');
define('ATTR_PRICE', 'Precio');
define('ATTR_PREFIXPRICE', 'Prefijo de precio');
define('DL_COUNT', 'Posibles descargas:');
define('DL_EXPIRE', 'Tiempo de descarga (días):');
define('TITLE_EDIT', 'Editar atributos');
define('TITLE_UPDATED', 'Atributos del producto actualizados con éxito.');
define('SELECT_PRODUCT', 'Por favor, seleccione un producto de la lista para editar:');
define('SELECT_COPY', 'Los atributos existentes también pueden copiarse a otros productos seleccionando el producto original: ');

// BOF - Tomcraft - 2009-11-11 - NEW SORT SELECTION
define('TEXT_OPTION_ID', 'ID de opciones');
define('TEXT_OPTION_NAME', 'Nombre de la opción');
define('TEXT_SORTORDER', 'Clasificación');
// EOF - Tomcraft - 2009-11-11 - NEW SORT SELECTION

define('ATTR_EAN', 'GTIN/EAN');
define('ATTR_SAVE_ACTIVE', 'Guardar atributos desplegados');
  
define('ATTR_VPE', 'unidad de embalaje');
?>

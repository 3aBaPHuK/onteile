<?php
/********************************************************
* file: shopping_cart_link_ajax.php
* use: ajax for generate_shoppingcart_link.php
*
* (c) copyright 04-2015, noRiddle
********************************************************/
require_once ('includes/application_top.php');

//BOC ajax
if(isset($_POST['artnum_val'])) {
    //allow entry of manufacturers_model
    $p_artnumval_raw = strtr($_POST['artnum_val'], [' ' => '', '-' => '', '_' => '', '/' => '']);
    $p_artnumval = trim($p_artnumval_raw);
    
    $prove_query = xtc_db_query("SELECT p.products_id,
                                        p.products_price,
                                        p.products_status,
                                        p.gm_price_status,
                                        p.products_tax_class_id,
                                        tr.tax_rate,
                                        pd.products_name
                                   FROM ".TABLE_PRODUCTS." p
                              LEFT JOIN ".TABLE_TAX_RATES." tr
                                     ON tr.tax_rates_id = p.products_tax_class_id
                              LEFT JOIN ".TABLE_PRODUCTS_DESCRIPTION." pd
                                     ON pd.products_id = p.products_id
                                    AND language_id = ".(int)$_SESSION['languages_id']." 
                                  WHERE products_model = '".xtc_db_input($p_artnumval)."' 
                                     OR REPLACE(products_model,' ','') = '".xtc_db_input($p_artnumval)."'"
                               );

    $prove_prodid = xtc_db_fetch_array($prove_query);
    
    $products_price = ($prove_prodid['products_price'] * (1 + ($prove_prodid['tax_rate'] / 100)));
    
    if($_POST['artnum_val'] != '' && !empty($prove_prodid['products_id'])) {
        $msg = sprintf(GSCL_PROD_EXISTS_TXT, $prove_prodid['products_name'], $products_price, $prove_prodid['products_id']);
        if($prove_prodid['products_status'] != '1') {
            $msg .= GSCL_NOT_PROD_STAT1_TXT;
        }
        if($prove_prodid['gm_price_status'] != '0') {
            $expl_gmstat_txt = $prove_prodid['gm_price_status'] == '1' ? GSCL_STAT1_TXT : GSCL_STAT2_TXT;
            $msg .= sprintf(GSCL_NOT_GMSTAT0_TXT, $prove_prodid['gm_price_status'], $expl_gmstat_txt);
        }
    } else {
        $msg = GSCL_MODEL_EMPTY_OR_NOTINSHOP_TXT;
    }
    echo $msg;
} else {
    die('Direct Access to this location is not allowed.');
}
//EOC ajax
?>
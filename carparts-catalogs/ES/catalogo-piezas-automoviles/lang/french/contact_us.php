<?php
 //additional fields
define('EMAIL_COMPANY', 'Société: ');
define('EMAIL_STREET', 'Rue: ');
define('EMAIL_POSTCODE', 'Code Postale: ');
define('EMAIL_CITY', 'Ville: ');
define('EMAIL_PHONE', 'Téléphone: ');
define('EMAIL_FAX', 'Fax: ');
define('EMAIL_SENT_BY', 'Transmis de %s %s am %s à %s heures');
 //define('EMAIL_NOTIFY', 'ACHTUNG, diese E-Mail kann NICHT mit -ABSENDER ANTWORTEN- beantwortet werden!'); // Not used yet
 
 //default fields
define('EMAIL_NAME', 'Nom: ');
define('EMAIL_EMAIL', 'Courriel: ');
define('EMAIL_MESSAGE', 'Message: ');
 
 //contact-form error messages
 //BOC nicer error output, noRiddle
 //define('ERROR_EMAIL','<p><b>Ihre E-Mail-Adresse:</b> Keine oder ung&uuml;ltige Eingabe!</p>');
 //define('ERROR_VVCODE','<p><b>Sicherheitscode:</b> Keine &Uuml;bereinstimmung, bitte geben Sie den Sicherheitscode erneut ein!</p>');
 //define('ERROR_MSG_BODY','<p><b>Ihre Nachricht:</b> Keine Eingabe!</p>');	
define('ERROR_EMAIL', '<li><b>Votre courriel:</b> Aucune saisie ou saisie incorrecte !</li>');
define('ERROR_VVCODE', '<li><b>Code de sécurité:</b> Aucune concordance, veuillez saisir à nouveau le code de sécurité !</li>');
define('ERROR_MSG_BODY', '<li><b>Votre message:</b> Aucune saisie !</li>');
 //EOC nicer error output, noRiddle
 
 //BOC new constants for VIN, price question and file upload, noRiddle
define('AH_MAIL_PRICE_QUESTION_SUBJ', 'Demande de prix pour article:');
define('CONTACT_UPLOAD_TEXT', '%s sont autorisés dont la taille maximale est de %s.<br />Des espaces blancs ne sont pas autorisées au sein du nom de fichier');
define('CONTACT_ERROR_FILE_SIZE', '<li><b>Télécharger une image:</b> Fichier trop grand. %s sont autoriés.</li>');
define('CONTACT_ERROR_FILE_EXT', '<li><b>Télécharger une image:</b> Extension de fichier ne pas autorisée. %s sont autorisés.</li>');
define('CONTACT_ERROR_FILE_LENGTH_AND_CHARS', '<li><b>Télécharger un fichier:</b> Nom de fichier trop long ( %d characters sont autorisés plus extension de fichier)<br />ou des caractères non autorisés ont été trouvés au sein du nom du fichier.<br />Autorisés sont a-z, A-Z, 0-9 tiret bas et trait d\'union.</li>');
 //EOC new constants for VIN, price question and file upload, noRiddle
?>

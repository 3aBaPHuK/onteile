<?php
/******************************************************************
* file: pdf_bill_languages.php
* path: /lang/german/admin/
* use: explanatory texts for /admin/pdf_bill_languages.php
* original: unknown
* (c) noRiddle 4-2018
******************************************************************/

define('TEXT_PDF_SEITE_DESC', 'Der Text der unten auf der Rechnung erscheint für die Seitenzahl');
define('TEXT_PDF_SEITE_VON_DESC', 'Seite <b>von</b>');
define('TEXT_PDF_ANSCHRIFT_DESC', 'Shop-Betreiber Anschrift im Footer');
define('TEXT_PDF_KONTAKT_DESC', '');
define('TEXT_PDF_BANK_DESC', '');
define('TEXT_PDF_PAYPAL_DESC', '');
define('TEXT_PDF_GESCHAEFT_DESC', '');
define('TEXT_PDF_KUNDENNUMMER_DESC', '');
define('TEXT_PDF_RECHNUNGSNUMMER_DESC', '');
define('TEXT_PDF_BESTELLNUMMER_DESC', '');
define('TEXT_PDF_DATUM_DESC', '');
define('TEXT_PDF_ZAHLUNGSWEISE_DESC', '');
define('TEXT_PDF_SHOPADRESSEKLEIN_DESC', '');
define('TEXT_PDF_RECHNUNG_DESC', '');
define('TEXT_PDF_RECHNUNG_HEAD_DESC', '');
define('TEXT_PDF_LIEFERSCHEIN_DESC', '');
define('TEXT_PDF_LIEFERSCHEIN_HEAD_DESC', '');
define('TEXT_PDF_MAIL_RECHNUNG_DESC', '');
define('TEXT_PDF_MAIL_LIEFERSCHEIN_DESC', '');
define('TEXT_PDF_MENGE_DESC', '');
define('TEXT_PDF_ARTIKEL_DESC', '');
define('TEXT_PDF_ARTIKELNR_DESC', '');
define('TEXT_PDF_EINZELPREIS_DESC', '');
define('TEXT_PDF_PREIS_DESC', '');
define('TEXT_PDF_DANKE_MANN_DESC', '');
define('TEXT_PDF_DANKE_FRAU_DESC', '');
define('TEXT_PDF_DANKE_UNISEX_DESC', '');
define('TEXT_PDF_KOMMENTAR_DESC', '');
define('TEXT_PDF_SCHLUSSTEXT_DESC', '');
define('TEXT_PDF_LSCHLUSSTEXT_DESC', '');
define('TEXT_PDF_HEADER_DESC', '');
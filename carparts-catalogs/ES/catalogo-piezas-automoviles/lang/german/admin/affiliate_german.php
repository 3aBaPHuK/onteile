<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_german.php 16 2010-10-28 15:21:32Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_german.php, v 1.3 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   
   corrected E_NOTICE constnt already defined, 01-2018 noRiddle
   ---------------------------------------------------------------------------*/

// reports box text in includes/boxes/affiliate.php
defined('BOX_CONFIGURATION_900') OR define('BOX_CONFIGURATION_900', 'Affiliate Konfiguration');
defined('BOX_HEADING_AFFILIATE') OR define('BOX_HEADING_AFFILIATE', 'Partnerprogramm');
defined('BOX_AFFILIATE_CONFIGURATION') OR define('BOX_AFFILIATE_CONFIGURATION', 'Affiliate Konfiguration');
defined('BOX_AFFILIATE_SUMMARY') OR define('BOX_AFFILIATE_SUMMARY', 'Zusammenfassung');
defined('BOX_AFFILIATE') OR define('BOX_AFFILIATE', 'Partner');
defined('BOX_AFFILIATE_PAYMENT') OR define('BOX_AFFILIATE_PAYMENT', 'Provisionszahlungen');
defined('BOX_AFFILIATE_BANNERS') OR define('BOX_AFFILIATE_BANNERS', 'Banner');
defined('BOX_AFFILIATE_CONTACT') OR define('BOX_AFFILIATE_CONTACT', 'Kontakt');
defined('BOX_AFFILIATE_SALES') OR define('BOX_AFFILIATE_SALES', 'Partner Verk&auml;ufe');
defined('BOX_AFFILIATE_CLICKS') OR define('BOX_AFFILIATE_CLICKS', 'Klicks');

defined('BUTTON_ADD_BANNER') OR define('BUTTON_ADD_BANNER', 'Banner Erfassen');
defined('BUTTON_BILLING') OR define('BUTTON_BILLING', 'Abrechnung starten');
defined('BUTTON_BANNERS') OR define('BUTTON_BANNERS', 'Bannerverwaltung');
defined('BUTTON_CLICKTHROUGHS') OR define('BUTTON_CLICKTHROUGHS', 'Klickstatistik');
defined('BUTTON_SALES') OR define('BUTTON_SALES', 'Partnerverkäufe');
?>

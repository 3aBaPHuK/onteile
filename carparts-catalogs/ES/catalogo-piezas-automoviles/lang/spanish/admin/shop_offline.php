<?php
/* --------------------------------------------------------------
   $Id: countries.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(countries.php,v 1.8 2002/01/19); www.oscommerce.com 
   (c) 2003	 nextcommerce (countries.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 	'Tienda online/offline');
define('HEADING_SUB_TITLE', 	'Configuración');
define('BOX_SHOP_OFFLINE', 'Tienda online/offline -  se aplica a todas los idiomas');
define('SETTINGS_OFFLINE', 'Tienda offline <br /><span class="col-red">(Acceso sólo con datos de Admin a través de URL <a href="'. HTTP_SERVER.DIR_WS_CATALOG.'login_admin.php" target="_blank"><span class="col-red">'. HTTP_SERVER.DIR_WS_CATALOG.'login_admin.php</span></a>)</span>');
define('SETTINGS_OFFLINE_MSG', 'Mensaje offline');

define('SHOP_OFFLINE_ALLOWED_CUSTOMERS_GROUPS_TXT', '<b>Grupos de clientes permitidos: </b><br />(la tienda sigue siendo visible para estos grupos de clientes)');
define('SHOP_OFFLINE_ALLOWED_CUSTOMERS_EMAILS_TXT', '<b>Direcciones de correo electrónico permitidas (separadas por comas):</b> <br />(para los clientes con estas direcciones de correo electrónico, la tienda sigue siendo visible)');
?>

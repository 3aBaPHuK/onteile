<?php
/********************************************************
* file: /import_export.php
* (c) noRiddle 12-2015
********************************************************/

//BOC *** config ***
//define('IE_IMPORT_FACTOR', 2); //defined in table configuration now
//define('IE_EXPORT_FACTOR', 2); //defined in table configuration now
//define('IE_HOW_MANY_GROUPS', 16); //now read out from table csv_table_aux
//define('IE_PERSONAL_OFFERS_ARRAY', '0,1,2,3,4,8'); //now read out from table csv_table_aux
//define('IE_NUM_OF_ADDI_PICS', 3);
define('IE_NUM_OF_ADDI_PICS', MO_PICS); //MO_PICS has to be configured in backend => Configuration => Image Options //added or changed 09-2019
define('IE_CAT_DEPTH', 4);
//EOC *** config ***

require_once(DIR_FS_INC . 'xtc_format_filesize.inc.php');

//--------------------------------------
//BOC**************IMPORT***************
//--------------------------------------
$post_select_file = $_POST['select_file'];
$post_select_spec_file = $_POST['select_file_specials'];

//include(dirname(__FILE__).'/aux_'.$script.'/load_sql.php'); // include file below in import

//echo $load_sql_aux_table.'<br />';

//BOC functions
function truncate_table($table) {
    return xtc_db_query("TRUNCATE TABLE " . $table);
}
           
function optimize_deleted_tables($table) {
    xtc_db_query("OPTIMIZE TABLE " . $table);
}

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
    return false;
}

function check_special($prid) {
    $spec_qu = xtc_db_query("SELECT products_id FROM specials WHERE products_id = ".(int)$prid);
    if(!xtc_db_num_rows($spec_qu)) {
        $sp_chk = false;
    } else {
        $sp_chk_arr = xtc_db_fetch_array($spec_qu);
        $sp_chk = $sp_chk_arr['products_id'];
    }
    
    return $sp_chk;
}
//EOC functions

//BOC get all files in import directory
$files = array();
$files_specials = array(); //added new files array for specials import, 09-2017, noRiddle
$files[] = array('id' => '0', 'text' => ADMIN_TOOLS_SELECT_FILE);
$files_specials[] = array('id' => '0', 'text' => ADMIN_TOOLS_SELECT_FILE);  //added new files array for specials import, 09-2017, noRiddle
if ($dir= opendir(DIR_FS_CATALOG.'import/')){
    while  (($file = readdir($dir)) !== false) {
        if (is_file(DIR_FS_CATALOG.'import/'.$file) && ($file != '.htaccess') && ($file != 'index.html')) {
            $size = filesize(DIR_FS_CATALOG.'import/'.$file);
            if(strpos($file, 'specials') === false) {
                $files[] = array('id' => $file,
                                 'text' => $file.' | '.xtc_format_filesize($size)
                                );
            } else {
                $files_specials[] = array('id' => $file,
                                          'text' => $file.' | '.xtc_format_filesize($size)
                                         ); //added new files array for specials import, 09-2017, noRiddle
            }
        }
    }
    closedir($dir);
}
//EOC get all files in import directory
//--------------------------------------
//EOC**************IMPORT**************
//--------------------------------------

//--------------------------------------
//BOC**************EXPORT**************
//--------------------------------------
$post_select_content = $_POST['select_content'];
$shp_sfx = '_'.strtoupper($shop_stripped.'-'.$country_envir); //shop and country (set in ah2.0/config.php), 15-04-2018, noRiddle
//--------------------------------------
//EOC**************EXPORT**************
//--------------------------------------


//BOC switch action
switch ($_GET['action']) {
    case 'upload':
        $upload_file = xtc_db_prepare_input($_POST['file_upload']);
        if ($upload_file = &xtc_try_upload('file_upload', DIR_FS_CATALOG.'import/')) {
            $$upload_file_name = $upload_file->filename;
            //BOC show new loaded file without page reload
            $files[] = array('id' => $$upload_file_name, 'text' => $$upload_file_name);
            //EOC show new loaded file without page reload
            $upl_return_message = '<span style="color:#0A7E00;">'.ADMIN_TOOLS_UPLOAD_FILE_SUCCESS.'</span>';
        } else {
            $upl_return_message ='<span style="color:#c00;">ERROR</span>';
        }
        break;
    case 'import':
        if(isset($post_select_file)) {
            if($post_select_file != '0' && in_array_r($post_select_file, $files)) {
                //truncate helper table
                truncate_table('csv_import_aux');

                $_SESSION[$_SESSION['ma_shopsess'].'out'] = '';
                $_SESSION[$_SESSION['ma_shopsess'].'tot_time'] = 0; //set session to measure total time
                $auxtable_startime = microtime(true);
                //BOC new options, noRiddle
                if(isset($_POST['mod_delete_products']) && $_POST['mod_delete_products'] == '1') {
                    $cust_status_query = xtc_db_query("SELECT count(customers_status_id) AS total FROM " . TABLE_CUSTOMERS_STATUS . " WHERE language_id = '2'");
                    $cust_status = xtc_db_fetch_array($cust_status_query);
                    $cust_status_tot = $cust_status['total'];

                    if($cust_status_tot > 0) {
                        for($i = 0; $i < $cust_status_tot; $i++) {
                            truncate_table('personal_offers_by_customers_status_'.$i);
                            //optimize_deleted_tables('personal_offers_by_customers_status_'.$i);
                        }
                    }
                    truncate_table(TABLE_PRODUCTS);
                    truncate_table(TABLE_PRODUCTS_ATTRIBUTES); //added for 2.0.X, noRiddle
                    truncate_table(TABLE_PRODUCTS_DESCRIPTION);
                    truncate_table(TABLE_PRODUCTS_GRADUATED_PRICES);
                    truncate_table(TABLE_PRODUCTS_TO_CATEGORIES);

                    //optimize_deleted_tables(TABLE_PRODUCTS);
                    //optimize_deleted_tables(TABLE_PRODUCTS_DESCRIPTION);
                    //optimize_deleted_tables(TABLE_PRODUCTS_GRADUATED_PRICES);
                    //optimize_deleted_tables(TABLE_PRODUCTS_TO_CATEGORIES);
                }
                   
                if((isset($_POST['mod_delete_categories']) && $_POST['mod_delete_categories'] == '1') && (isset($_POST['mod_delete_products']) && $_POST['mod_delete_products'] == '1')) {
                    truncate_table(TABLE_CATEGORIES);
                    truncate_table(TABLE_CATEGORIES_DESCRIPTION);	

                    //optimize_deleted_tables(TABLE_CATEGORIES);
                    //optimize_deleted_tables(TABLE_CATEGORIES_DESCRIPTION);
                }
                    
                if(isset($_POST['mod_delete_specials']) && $_POST['mod_delete_specials'] == '1'){
                    truncate_table(TABLE_SPECIALS);

                    //optimize_deleted_tables(TABLE_SPECIALS);
                }
                   
                /*if(isset($_POST['mod_delete_images']) && $_POST['mod_delete_images'] == '1') {
                    truncate_table(TABLE_PRODUCTS_IMAGES);

                    //optimize_deleted_tables(TABLE_PRODUCTS_IMAGES);
                }*/ //we keep theses in case we have some additional images for start page products, 04-2021, noRiddle
                   
                if(isset($_POST['mod_delete_manufacturers']) && $_POST['mod_delete_manufacturers'] == '1'){
                    truncate_table(TABLE_MANUFACTURERS);

                    //optimize_deleted_tables(TABLE_MANUFACTURERS);
                }

                if(isset($_POST['mod_delete_reviews']) && $_POST['mod_delete_reviews'] == '1'){				
                    truncate_table(TABLE_REVIEWS);	
                    truncate_table(TABLE_REVIEWS_DESCRIPTION);

                    //optimize_deleted_tables(TABLE_REVIEWS);			
                    //optimize_deleted_tables(TABLE_REVIEWS_DESCRIPTION);
                }

                if(isset($_POST['mod_delete_attributes']) && $_POST['mod_delete_attributes'] == '1'){
                    truncate_table(TABLE_PRODUCTS_ATTRIBUTES);
                    truncate_table(TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD);

                    //optimize_deleted_tables(TABLE_PRODUCTS_ATTRIBUTES);
                    //optimize_deleted_tables(TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD);
                }

                if(isset($_POST['mod_delete_xsell']) && $_POST['mod_delete_xsell'] == '1'){
                    truncate_table(TABLE_PRODUCTS_XSELL);
                    truncate_table(TABLE_PRODUCTS_XSELL_GROUPS);

                    //optimize_deleted_tables(TABLE_PRODUCTS_XSELL);
                    //optimize_deleted_tables(TABLE_PRODUCTS_XSELL_GROUPS);
                }
                //EOC new options, noRiddle

                //BOC new image handling, 04-2021, noRiddle
                if(isset($_POST['mod_pim']) && $_POST['mod_pim'] == '1') {
                    $imgs_equ_modelno = true;
                } else {
                    $imgs_equ_modelno = false;
                }
                //EOC new image handling, 04-2021, noRiddle

                //BOC load data local infile
                //$auxtable_startime = microtime(true);
                //BOC read out header of csv to be able to filter products_id if exists
                $fp = fopen(DIR_FS_CATALOG.'import/'.$post_select_file, 'r');
                if(CSV_TEXTSIGN != '') {
                    $header = fgetcsv($fp, 20000, CSV_SEPERATOR, CSV_TEXTSIGN);
                } else {
                    $header = fgetcsv($fp, 20000, CSV_SEPERATOR);
                }
                if(in_array('products_id', $header)) {$has_prid = true;}
                fclose($fp); //we can close file here, 02-2020, noRiddle
                //BOC verify csv fields are right, 02-2020, noRiddle
                $goforit = false;
                include_once(dirname(__FILE__).'/aux_'.$script.'/load_sql.php');
                if($header == $cols_arr) {$goforit = true;} //verify csv fields are right, 02-2020, noRiddle
                //echo '<pre>'.print_r($header, true).'</pre>';
                //echo '<pre>'.print_r($cols_arr, true).'</pre>';
                //EOC read out header of csv to be able to filter products_id if exists
                if($goforit === true) { //verify csv fields are right, 02-2020, noRiddle
                    if(xtc_db_query($load_sql_aux_table)) {
                        $_SESSION[$_SESSION['ma_shopsess'].'out'] .= '<span style="color:#0A7E00;">'.ADMIN_TOOLS_DATA_SUCCESSFULLY_IN_AUX_TABLE.'</span>';
                        $_SESSION[$_SESSION['ma_shopsess'].'nR_postfile'] = $post_select_file; //add file to a session to be able to show it in message, noRiddle
                        $auxtable_endtime = microtime(true);
                        $auxtable_time = ($auxtable_endtime - $auxtable_startime);
                        $_SESSION[$_SESSION['ma_shopsess'].'out'] .= '<br /><span style="color:#0A7E00;">Elapsed time: '.number_format($auxtable_time, 4).' sec.</span>';
                        $_SESSION[$_SESSION['ma_shopsess'].'tot_time'] += number_format($auxtable_time, 4);
                    } else {
                        $_SESSION[$_SESSION['ma_shopsess'].'out'] = '<span style="color:#c00;">ERROR</span>';
                    }
                } else {
                    $_SESSION[$_SESSION['ma_shopsess'].'out'] = '<span style="color:#c00;">Feld-Struktur der CSV stimmt nicht mit Vorgabe und Hilfstabelle &uuml;berein. Bitte pr&uuml;fen.</span>';

                    for($i = 0, $ch = count($cols_arr); $i < $ch; $i++) {
                        if($header[$i] != $cols_arr[$i]) {
                            $header[$i] = '<span style="color:#c00;">'.$header[$i].'</span>';
                        }
                    }
                    
                    $_SESSION[$_SESSION['ma_shopsess'].'out'] .= '<br /><table><tr><th>CSV</th><th>Hilfstabelle</th></tr><tr><td><pre>'.print_r($header, true).'</pre></td><td><pre>'.print_r($cols_arr, true).'</pre></td></tr></table>';
                }
                //EOC verify csv fields are right, 02-2020, noRiddle
                //fclose($fp); //close file above, 03-2020, noRiddle
                //EOC load data local infile
            } else {
                $_SESSION[$_SESSION['ma_shopsess'].'out'] = '<span style="color:#c00;">'.ADMIN_TOOLS_NO_FILE_CHOOSEN.'</span>';
            }
        }
        //echo isset($_SESSION[$_SESSION['ma_shopsess'].'tot_time']) ? '<pre>$_SESSION[\'tot_time\'] = '.$_SESSION[$_SESSION['ma_shopsess'].'tot_time'].' sec.</pre>' : '<pre>$_SESSION[\'tot_time\'] is not set</pre>';
        //echo isset($_SESSION[$_SESSION['ma_shopsess'].'out']) ? '<pre>$_SESSION[\'out\'] = '.$_SESSION[$_SESSION['ma_shopsess'].'out'].'</pre>' : '<pre>$_SESSION[\'out\'] is not set</pre>';
        //BOC load into live tables with browser reload
        include(dirname(__FILE__).'/aux_'.$script.'/import_into_live_tables.php');
        $at_i = new admin_tools_import($imgs_equ_modelno);
        $at_i->write_reload();
        //EOC load into live tables with browser reload
        //echo isset($_SESSION[$_SESSION['ma_shopsess'].'tot_time']) ? '<pre>$_SESSION[\'tot_time\'] = '.$_SESSION[$_SESSION['ma_shopsess'].'tot_time'].' sec.</pre>' : '<pre>$_SESSION[\'tot_time\'] is not set</pre>';
        //echo isset($_SESSION[$_SESSION['ma_shopsess'].'out']) ? '<pre>$_SESSION[\'out\'] = '.$_SESSION[$_SESSION['ma_shopsess'].'out'].'</pre>' : '<pre>$_SESSION[\'out\'] is not set</pre>';
        break;
    //BOC added for specials import, 09-2017, noRiddle
    case 'import_specials':
        //added verification and adaptation (if necessary) of gm_price_status 01-09-2019, noRiddle
        //pattern: products_model|products_id|products_name_de|specials_new_products_price|guest_price|specials_quantity|start_date|expires_date
        //products_id and products_name_de are just informal for exports, we don't use them in import
        if(isset($post_select_spec_file)) {
            if($post_select_spec_file != '0' && in_array_r($post_select_spec_file, $files_specials)) {
                $spec_mp_start = microtime(true);

                //BOC define vars for articles not in shop
                $imp_noart_file_name = 'specials_not_in_shop'.date('d-m-Y_H:i:s').'_'.$shp_sfx.'.csv';
                $imp_noart_data_str = '';
                //EOC define vars for articles not in shop

                $fp = fopen(DIR_FS_CATALOG.'import/'.$post_select_spec_file, 'r');
                if(CSV_TEXTSIGN != '') {
                    $header = fgetcsv($fp, 20000, CSV_SEPERATOR, CSV_TEXTSIGN);
                } else {
                    $header = fgetcsv($fp, 20000, CSV_SEPERATOR);
                }

                if(CSV_TEXTSIGN != '') {
                    $cnt_not_inshop = 0; //counter for products which are not in shop
                    while($line = fgetcsv($fp, 20000, CSV_SEPERATOR, CSV_TEXTSIGN)) {
                        $line0 = xtc_db_input($line[0]);
                        //$spec_proid_qu = xtc_db_query("SELECT products_id FROM products WHERE products_model = '".str_replace(' ', '', $line0)."'");
                        $spec_proid_qu = xtc_db_query("SELECT products_id, gm_price_status FROM products WHERE products_model = '".str_replace(' ', '', $line0)."'");
                        $spec_proid_arr = xtc_db_fetch_array($spec_proid_qu);

                        if($spec_proid_arr['products_id'] != '') {
                            $line_data[$spec_proid_arr['products_id']] = array('products_id' => $spec_proid_arr['products_id'],
                                                                               'specials_quantity' => $line[5],
                                                                               'specials_new_products_price' => $line[3],
                                                                               'specials_date_added' => 'now()',
                                                                               'specials_last_modified' => 'now()',
                                                                               'start_date' => $line[6],
                                                                               'expires_date' => $line[7],
                                                                               'status' => '1',
                                                                               'not_avail' => ($spec_proid_arr['gm_price_status'] == '2' ? 'true' : 'false')
                                                                              );
                        } else {
                            //build string for products which are not in shop
                            $cnt_not_inshop++;
                            $imp_noart_data_str .=  CSV_TEXTSIGN.$line0.CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.''.CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.''.CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$line[3].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$line[4].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$line[5].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$line[6].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$line[7].CSV_TEXTSIGN."\r\n";
                        }
                    }
                } else {
                    $cnt_not_inshop = 0; //counter for products which are not in shop
                    while($line = fgetcsv($fp, 20000, CSV_SEPERATOR)) {
                        $line0 = xtc_db_input($line[0]);
                        //$spec_proid_qu = xtc_db_query("SELECT products_id FROM products WHERE products_model = '".str_replace(' ', '', $line0)."'");
                        $spec_proid_qu = xtc_db_query("SELECT products_id, gm_price_status FROM products WHERE products_model = '".str_replace(' ', '', $line0)."'");
                        $spec_proid_arr = xtc_db_fetch_array($spec_proid_qu);
                        
                        if($spec_proid_arr['products_id'] != '') {
                            $line_data[$spec_proid_arr['products_id']] = array('products_id' => $spec_proid_arr['products_id'],
                                                                               'specials_quantity' => $line[5],
                                                                               'specials_new_products_price' => $line[3],
                                                                               'specials_date_added' => 'now()',
                                                                               'specials_last_modified' => 'now()',
                                                                               'start_date' => $line[6],
                                                                               'expires_date' => $line[7],
                                                                               'status' => '1',
                                                                               'not_avail' => ($spec_proid_arr['gm_price_status'] == '2' ? 'true' : 'false')
                                                                              );
                        } else {
                            //build string for products which are not in shop
                            $cnt_not_inshop++;
                            $imp_noart_data_str .= $line0.CSV_SEPERATOR.''.CSV_SEPERATOR.''.CSV_SEPERATOR.$line[3].CSV_SEPERATOR.$line[4].CSV_SEPERATOR.$line[5].CSV_SEPERATOR.$line[6].CSV_SEPERATOR.$line[7]."\r\n";
                        }
                    }
                }
                //echo '<br /><br /><br /><br /><br /><pre>'.print_r($line_data, true).'</pre>';

                $spec_tot = '';
                $spec_cnt_upd = 0;
                $spec_cnt_ins = 0;
                if(!empty($line_data)) {
                    foreach($line_data as $spec_prid => $spec_dat_arr) {
                        $have_spec = check_special($spec_prid);
                        if($have_spec) {
                            $spec_cnt_upd++;
                            unset($spec_dat_arr['products_id']);
                            unset($spec_dat_arr['specials_date_added']);
                            if($spec_dat_arr['not_avail'] == 'true') {
                                xtc_db_query("UPDATE ".TABLE_PRODUCTS." SET gm_price_status = 0, products_quantity = ".(int)$spec_dat_arr['specials_quantity']." WHERE products_id = ".(int)$spec_prid);
                            }
                            unset($spec_dat_arr['not_avail']);

                            if(xtc_db_perform(TABLE_SPECIALS, $spec_dat_arr, 'update', 'products_id = '.(int)$spec_prid)) {
                                $spec_upd_err = array();
                            } else {
                                $spec_upd_err[$spec_cnt_upd] = 'true';
                            }
                        } else {
                            $spec_cnt_ins++;
                            unset($spec_dat_arr['specials_last_modified']);
                            if($spec_dat_arr['not_avail'] == 'true') {
                                xtc_db_query("UPDATE ".TABLE_PRODUCTS." SET gm_price_status = 0, products_quantity = ".(int)$spec_dat_arr['specials_quantity']." WHERE products_id = ".(int)$spec_prid);
                            }
                            unset($spec_dat_arr['not_avail']);

                            if(xtc_db_perform(TABLE_SPECIALS, $spec_dat_arr)) {
                                $spec_ins_err = array();
                            } else {
                                $spec_ins_err[$spec_cnt_ins] = 'true';
                            }
                        }
                    }
                } else {
                    $spec_tot .= '<span style="color:#c00;">ERROR: Es wurde keiner der Artikel-Nummern in der Import-Datei im Shop gefunden.</span>';
                }

                //BOC write csv with products which are not in shop
                if($cnt_not_inshop > 0) {
                    $imp_noart_header_str = 'products_model'.CSV_SEPERATOR.'products_id'.CSV_SEPERATOR.'products_name_de'.CSV_SEPERATOR.'specials_new_products_price'.CSV_SEPERATOR.'guest_price'.CSV_SEPERATOR.'specials_quantity'.CSV_SEPERATOR.'start_date'.CSV_SEPERATOR.'expires_date'."\r\n";
                    
                    $fp_nis = fopen(DIR_FS_CATALOG.'export/'.$imp_noart_file_name, "a+");
                    fwrite($fp_nis, $imp_noart_header_str);
                    fwrite($fp_nis, $imp_noart_data_str);
                    fclose($fp_nis);
                }
                //EOC write csv with products which are not in shop

                $spec_mp_end = microtime(true);
                $spec_tot_raw = $spec_mp_end - $spec_mp_start;
                if(empty($spec_upd_err) && empty($spec_ins_err)) {
                    $spec_tot .= '<span style="color:#0A7E00;">OKAY: '.$spec_cnt_upd.' Artikel upgedatet | '.$spec_cnt_ins.' Artikel neu eingefuegt</span>';
                    $spec_tot .= '<br /><span style="color:#0A7E00;">Elapsed time: '.number_format($spec_tot_raw, 4).' sec.</span>';
                    //message for products which are not in shop
                    if($cnt_not_inshop > 0) {
                        $spec_tot .= '<br /><span style="color:#c00;">Es gibt in der Import-Datei Artikel die es nicht im Shop gibt.<br />Bitte File <i>/export/'.$imp_noart_file_name.'</i> runterladen</span>';
                    }
                } else {
                    $spec_tot .= '<span style="color:#c00;">ERROR: ';
                    foreach($spec_upd_err as $cntu => $valu) {
                        $spec_tot .= '<br />Update bei Artikel '.$cnt.' fehlgeschlagen';
                    }
                    foreach($spec_ins_err as $cnti => $vali) {
                        $spec_tot .= '<br />Insert bei Artikel '.$spec_cnt_ins.' fehlgeschlagen';
                    }
                    $spec_tot .= '</span>';
                    //message for products which are not in shop
                    if($cnt_not_inshop > 0) {
                        $spec_tot .= '<br /><span style="color:#c00;">Es gibt in der Import-Datei Artikel die es nicht im Shop gibt.<br />Bitte File <i>/export/'.$imp_noart_file_name.'</i> runterladen</span>';
                    }
                }

                fclose($fp);

                //delete DB cache if activated
                if(defined('DB_CACHE') && DB_CACHE == 'true') {
                    clear_dir(SQL_CACHEDIR);
                }
            }
        }
        break;
    //EOC added for specials import, 09-2017, noRiddle
    case 'export':
        if(isset($post_select_content)) {
            $_SESSION[$_SESSION['ma_shopsess'].'expout'] = '';
            $_SESSION[$_SESSION['ma_shopsess'].'exptot_time'] = 0; //set session to measure total time
            $_SESSION[$_SESSION['ma_shopsess'].'exptot_sttime'] = microtime(true);
        
            //$_SESSION[$_SESSION['ma_shopsess'].'mom_date'] = date('d-m-Y_H:i:s'); //store date and time into var
            $_SESSION[$_SESSION['ma_shopsess'].'mom_date'] = date('Ymd'); //changed as to Andreas wish to use it as first part of filename, 15-04-2018, noRiddle

            //BOC define filename for different exports
            if($post_select_content == 'startpage') {
                $sel_prod = 'sp';
                //$exp_file_name = 'product_startpage_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                $exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'_product_startpage_export'.$shp_sfx.'.csv';
            } elseif ($post_select_content == 'gm_price_status_normal') {
                $sel_prod = 'gpsn';
                //$exp_file_name = 'product_gm_normal_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                $exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'_product_gm_normal_export'.$shp_sfx.'.csv';
            } elseif ($post_select_content == 'gm_price_status_onrequ') {
                $sel_prod = 'gpsor';
                //$exp_file_name = 'product_gm_onrequest_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                $exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'_product_gm_onrequest_export'.$shp_sfx.'.csv';
            } elseif ($post_select_content == 'gm_price_status_np') {
                $sel_prod = 'gpsnp';
                //$exp_file_name = 'product_notforsale_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                $exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'_product_notforsale_export'.$shp_sfx.'.csv';
            } elseif($post_select_content == 'products') {
                $sel_prod = 'p';
                //$exp_file_name = 'product_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                $exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'_product_export'.$shp_sfx.'.csv';
            } elseif ($post_select_content == 'set_by_admin') { //new filter for export of new field, noRiddle //added 30.06.2016
                $sel_prod = 'sba';
                //$exp_file_name = 'set_by_admin_export'.$_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'.csv';
                $exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'_set_by_admin_export'.$shp_sfx.'.csv';
            } elseif ($post_select_content == 'products_sbk') { //new filter for export of new field, noRiddle //added 04.05.2020
                $sel_prod = 'sbk';
                $exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'_prod_sbk_export'.$shp_sfx.'.csv';
            } elseif ($post_select_content == 'blackforest') { //new filter for export of blackforest parts, added or changed 09-2019
                $sel_prod = 'blckf';
                $exp_file_name = $_SESSION[$_SESSION['ma_shopsess'].'mom_date'].'_blackforest_export'.$shp_sfx.'.csv';
            } //last condition added for Blackforest, added or changed 09-2019

            //EOC define filename for different exports
            //BOC write header
            $header_str = '';
            $columns_query = xtc_db_query("SHOW COLUMNS FROM csv_import_aux");
            while($columns = xtc_db_fetch_array($columns_query)) {
                if($columns['Field'] != 'import_id' && $columns['Field'] != 'products_id') { //added products_id
                    $header_str .= $columns['Field'].CSV_SEPERATOR;
                }
            }
            $header_str = rtrim($header_str, CSV_SEPERATOR);
            //echo '<pre>'.$header_str.'</pre>';

            $_SESSION[$_SESSION['ma_shopsess'].'exp_file_name'] = $exp_file_name;
            $fp = fopen(DIR_FS_CATALOG.'export/'.$exp_file_name, "w+");
            fwrite($fp, $header_str."\r\n");
            fclose($fp);
            //EOC write header
            $_SESSION[$_SESSION['ma_shopsess'].'expout'] .= '<span style="color:#0A7E00;">'.ADMIN_TOOLS_HEADER_CSV_SET.'</span>';
            $_SESSION[$_SESSION['ma_shopsess'].'exptot_endtime'] = microtime(true);
            $_SESSION[$_SESSION['ma_shopsess'].'exptot_time'] += ($_SESSION[$_SESSION['ma_shopsess'].'exptot_endtime'] - $_SESSION[$_SESSION['ma_shopsess'].'exptot_sttime']);
            $_SESSION[$_SESSION['ma_shopsess'].'expout'] .= '<br /><span style="color:#0A7E00;">Elapsed time: '.number_format($_SESSION[$_SESSION['ma_shopsess'].'exptot_time'], 4).' sec.</span>';
            unset($_SESSION[$_SESSION['ma_shopsess'].'exptot_sttime']);
            unset($_SESSION[$_SESSION['ma_shopsess'].'exptot_endtime']);
            
            xtc_redirect(xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=export&sel_prod='.$sel_prod));
        }
        //echo isset($_SESSION[$_SESSION['ma_shopsess'].'exptot_time']) ? '<pre>$_SESSION[\'exptot_time\'] = '.$_SESSION[$_SESSION['ma_shopsess'].'exptot_time'].' sec.</pre>' : '<pre>$_SESSION[\'exptot_time\'] is not set</pre>';
        //echo isset($_SESSION[$_SESSION['ma_shopsess'].'expout']) ? '<pre>$_SESSION[\'expout\'] = '.$_SESSION[$_SESSION['ma_shopsess'].'expout'].'</pre>' : '<pre>$_SESSION[\'expout\'] is not set</pre>';
        //BOC load into csv with browser reload
        include(dirname(__FILE__).'/aux_'.$script.'/export_into_csv.php');
        $at_e = new admin_tools_export($_SESSION[$_SESSION['ma_shopsess'].'exp_file_name']);
        $at_e->write_csv();
        //EOC load into csv with browser reload
        //echo isset($_SESSION[$_SESSION['ma_shopsess'].'exptot_time']) ? '<pre>$_SESSION[\'exptot_time\'] = '.$_SESSION[$_SESSION['ma_shopsess'].'exptot_time'].' sec.</pre>' : '<pre>$_SESSION[\'exptot_time\'] is not set</pre>';
        //echo isset($_SESSION[$_SESSION['ma_shopsess'].'expout']) ? '<pre>$_SESSION[\'expout\'] = '.$_SESSION[$_SESSION['ma_shopsess'].'expout'].'</pre>' : '<pre>$_SESSION[\'expout\'] is not set</pre>';
        break;
    //BOC added for specials export, 09-2017, noRiddle
    case 'export_specials':
        /* use this for test in phpMyAdmin:
           SELECT p.products_model, s.specials_quantity, s.specials_new_products_price, s.start_date, s.expires_date
             FROM products p
        LEFT JOIN specials s
               ON s.products_id = p.products_id
            WHERE p.products_id IN(SELECT products_id FROM specials)
        */
        
        $spec_exp_start = microtime(true);
        
        //$exp_spec_file_name = 'product_specials_export_wgp'.date('d-m-Y_H:i:s').'.csv';
        $exp_spec_file_name = date('Ymd').'_product_specials_export_wgp'.$shp_sfx.'.csv';
        $spec_header_str = 'products_model'.CSV_SEPERATOR.'products_id'.CSV_SEPERATOR.'products_name_de'.CSV_SEPERATOR.'specials_new_products_price'.CSV_SEPERATOR.'guest_price'.CSV_SEPERATOR.'specials_quantity'.CSV_SEPERATOR.'start_date'.CSV_SEPERATOR.'expires_date'."\r\n";

        $spec_qu = xtc_db_query("SELECT p.products_model, p.products_id, pd.products_name, s.specials_quantity, s.specials_new_products_price, s.start_date, s.expires_date, po1.personal_offer
                                   FROM ".TABLE_PRODUCTS." p
                              LEFT JOIN ".TABLE_PRODUCTS_DESCRIPTION." pd
                                     ON pd.products_id = p.products_id
                                    AND pd.language_id = '2' #only german name needed
                              LEFT JOIN ".TABLE_SPECIALS." s
                                     ON s.products_id = p.products_id
                              LEFT JOIN personal_offers_by_customers_status_1 po1
                                     ON po1.products_id = p.products_id
                                    AND quantity = '1' 
                                   WHERE p.products_id IN(SELECT products_id FROM ".TABLE_SPECIALS.")");
        
        //write header
        $fp = fopen(DIR_FS_CATALOG.'export/'.$exp_spec_file_name, "a+");
        fwrite($fp, $spec_header_str);
        
        //write lines
        while($spec_arr = xtc_db_fetch_array($spec_qu)) {
            if(CSV_TEXTSIGN != '') {
                $spec_line = CSV_TEXTSIGN.$spec_arr['products_model'].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$spec_arr['products_id'].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$spec_arr['products_name'].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$spec_arr['specials_new_products_price'].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$spec_arr['personal_offer'].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$spec_arr['specials_quantity'].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$spec_arr['start_date'].CSV_TEXTSIGN.CSV_SEPERATOR.CSV_TEXTSIGN.$spec_arr['expires_date'].CSV_TEXTSIGN."\r\n";
            } else {
                $spec_line = $spec_arr['products_model'].CSV_SEPERATOR.$spec_arr['products_id'].CSV_SEPERATOR.$spec_arr['products_name'].CSV_SEPERATOR.$spec_arr['specials_new_products_price'].CSV_SEPERATOR.$spec_arr['personal_offer'].CSV_SEPERATOR.$spec_arr['specials_quantity'].CSV_SEPERATOR.$spec_arr['start_date'].CSV_SEPERATOR.$spec_arr['expires_date']."\r\n";
            }
            
            fwrite($fp, $spec_line);
        }
        
        if(fclose($fp)) {
            $spec_exp_end = microtime(true);
            $spec_totexp_raw = $spec_exp_end - $spec_exp_end;
            $spec_tot_exp = '<span style="color:#0A7E00;">OKAY: Sonderangebote wurden exportiert.<br />Filename: '.$exp_spec_file_name.'</span>';
        }
        
        break;
    //EOC added for specials export, 09-2017, noRiddle
    case 'save':
        $configuration_query = xtc_db_query("SELECT configuration_key, configuration_id, configuration_value, use_function, set_function from " . TABLE_CONFIGURATION . " WHERE configuration_group_id = '20' ORDER BY sort_order");
        while ($configuration = xtc_db_fetch_array($configuration_query)) {
            if($config_vals['configuration_key'] != 'COMPRESS_EXPORT' && $config_vals['configuration_key'] != 'CSV_CATEGORY_DEFAULT' && $config_vals['configuration_key'] != 'CSV_CAT_DEPTH') {
                $set_arr = array('configuration_value' => $_POST[$configuration['configuration_key']]);
                xtc_db_perform(TABLE_CONFIGURATION, $set_arr, 'update', 'configuration_key = \''.$configuration['configuration_key'].'\'');
            }
        }
        xtc_redirect(xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export'));
        break;
}
//EOC switch action
?>
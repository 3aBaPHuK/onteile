<?php
/******************************************************************************************************
* file: automatic_more_shops.php
* use: control file  to display all "more shops" logos with title and alt tags, done in box shops.php
* load into media/content and use file in content
* (c) noRiddle 07-2017
******************************************************************************************************/

define('AH_AUTOMATIC_MORE_SHOPS', true);

?>
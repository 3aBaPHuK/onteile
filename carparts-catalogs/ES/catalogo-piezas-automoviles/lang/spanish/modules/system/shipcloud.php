<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

if (defined('_VALID_XTC')) {
define('MODULE_SHIPCLOUD_TEXT_TITLE', 'shipcloud - la nueva generación de envío de paquetes');
define('MODULE_SHIPCLOUD_TEXT_DESCRIPTION', 'Imprima las etiquetas de los paquetes cómodamente desde la tienda.');
define('MODULE_SHIPCLOUD_STATUS_TITLE', 'Estado');
define('MODULE_SHIPCLOUD_STATUS_DESC', 'Activar módulo');
define('MODULE_SHIPCLOUD_API_TITLE', '<hr noshade>API');
define('MODULE_SHIPCLOUD_API_DESC', 'Clave de API de shipcloud');
define('MODULE_SHIPCLOUD_PARCEL_TITLE', '<hr noshade>tamaños de paquete');
define('MODULE_SHIPCLOUD_PARCEL_DESC', 'Por favor, introduzca los tamaños de paquete en cm como se indica a continuación: Longitud,anchura,altura;<br/>Se pueden especificar varias dimensiones de paquete separadas con punto y coma (;). p.ej: 20,40,30;15,20,20;');
define('MODULE_SHIPCLOUD_COMPANY_TITLE', '<hr noshade>Detalles de cliente<br/>');
define('MODULE_SHIPCLOUD_COMPANY_DESC', 'Empresa:');
define('MODULE_SHIPCLOUD_FIRSTNAME_TITLE', '');
define('MODULE_SHIPCLOUD_FIRSTNAME_DESC', 'Nombre:');
define('MODULE_SHIPCLOUD_LASTNAME_TITLE', '');
define('MODULE_SHIPCLOUD_LASTNAME_DESC', 'Apellido:');
define('MODULE_SHIPCLOUD_ADDRESS_TITLE', '');
define('MODULE_SHIPCLOUD_ADDRESS_DESC', 'Dirección:');
define('MODULE_SHIPCLOUD_POSTCODE_TITLE', '');
define('MODULE_SHIPCLOUD_POSTCODE_DESC', 'Código postal:');
define('MODULE_SHIPCLOUD_CITY_TITLE', '');
define('MODULE_SHIPCLOUD_CITY_DESC', 'Ciudad:');
define('MODULE_SHIPCLOUD_TELEPHONE_TITLE', '');
define('MODULE_SHIPCLOUD_TELEPHONE_DESC', 'Teléfono:');
define('MODULE_SHIPCLOUD_ACCOUNT_IBAN_TITLE', '');
define('MODULE_SHIPCLOUD_ACCOUNT_IBAN_DESC', 'IBAN:');
define('MODULE_SHIPCLOUD_ACCOUNT_BIC_TITLE', '');
define('MODULE_SHIPCLOUD_ACCOUNT_BIC_DESC', 'BIC:');
define('MODULE_SHIPCLOUD_BANK_NAME_TITLE', '<hr noshade>Detalles bancarios<br/>');
define('MODULE_SHIPCLOUD_BANK_NAME_DESC', 'Banco:');
define('MODULE_SHIPCLOUD_BANK_HOLDER_TITLE', '');
define('MODULE_SHIPCLOUD_BANK_HOLDER_DESC', 'Titular de la cuenta:');
define('MODULE_SHIPCLOUD_LOG_TITLE', '<hr noshade>Log');
define('MODULE_SHIPCLOUD_LOG_DESC', 'el archivo log se almacena en la carpeta /log .');
define('MODULE_SHIPCLOUD_EMAIL_TITLE', '<hr noshade>Notificación por correo electrónico');
define('MODULE_SHIPCLOUD_EMAIL_DESC', '¿Debe notificarse al cliente por correo electrónico?');
define('MODULE_SHIPCLOUD_EMAIL_TYPE_TITLE', '<hr noshade>Notificación');
define('MODULE_SHIPCLOUD_EMAIL_TYPE_DESC', '¿Debe el cliente ser notificado por la tienda o por shipcloud?<br><Aviso:</b>Para una notificación de la tienda, se debe crear un webhook a esta URL: '.xtc_catalog_href_link('callback/shipcloud/callback.php', '', 'SSL', false).' en shipcloud.');
}

define('SHIPMENT.TRACKING.SHIPCLOUD_LABEL_CREATED', 'Etiqueta de paquete creada en shipcloud');
define('SHIPMENT.TRACKING.LABEL_CREATED', 'Etiqueta de paquete creada ');
define('SHIPMENT.TRACKING.PICKED_UP', 'Paquete recogido por el transportista');
define('SHIPMENT.TRACKING.TRANSIT', 'El paquete está en camino.');
define('SHIPMENT.TRACKING.OUT_FOR_DELIVERY', 'El paquete se entrega');
define('SHIPMENT.TRACKING.DELIVERED', 'Paquete entregado');
define('SHIPMENT.TRACKING.AWAITS_PICKUP_BY_RECEIVER', 'Esperando la recogida por el cliente');
define('SHIPMENT.TRACKING.CANCELED', 'Se ha suprimido la etiqueta de paquete');
define('SHIPMENT.TRACKING.DELAYED', 'La entrega se retrasa.');
define('SHIPMENT.TRACKING.EXCEPTION', 'Se ha detectado un problema.');
define('SHIPMENT.TRACKING.NOT_DELIVERED', 'no entregado');
define('SHIPMENT.TRACKING.NOTIFICATION', 'Mensaje interno: El seguimiento de acontecimientos dentro del envío requiere información más elaborada.');
define('SHIPMENT.TRACKING.UNKNOWN', 'Estado desconocido');
?>

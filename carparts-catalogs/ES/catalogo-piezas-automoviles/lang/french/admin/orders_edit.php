<?php
/* --------------------------------------------------------------
   $Id: orders_edit.php,v 1.0 

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(orders.php,v 1.27 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (orders.php,v 1.7 2003/08/14); www.nextcommerce.org
   (c) 2006 XT-Commerce (orders_edit.php)

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

// Allgemeine Texte
define('TABLE_HEADING', 'Editer les données de la commande');
define('TABLE_HEADING_ORDER', 'Commande N°:');
define('TEXT_SAVE_ORDER', 'Terminer l\'édition de la commande et recalculer la commande.');

define('TEXT_EDIT_ADDRESS', 'Adresse et données du client');
define('TEXT_EDIT_PRODUCTS', 'Articles, options d\'article et prix');
define('TEXT_EDIT_OTHER', 'Frais de port, méthodes de paiement, devise, langues, sommes, TVA, promotions, etc.');
define('TEXT_EDIT_GIFT', 'Bon d\'achats et promotion');
define('TEXT_EDIT_ADDRESS_SUCCESS', 'Le changement d\'adresse a été enregistré.');

define('IMAGE_EDIT_ADDRESS', 'Editer ou insérer les adresses');
define('IMAGE_EDIT_PRODUCTS', 'Editer ou insérer les articles et options');
define('IMAGE_EDIT_OTHER', 'Editer ou insérer les frais de port, paiement, bon d\'achats etc.');

// Adressaenderung
define('TEXT_INVOICE_ADDRESS', 'Adresse du client');
define('TEXT_SHIPPING_ADDRESS', 'Adresse de livraison');
define('TEXT_BILLING_ADDRESS', 'Adresse de la facture');

define('TEXT_COMPANY', 'Entreprise:');
define('TEXT_NAME', 'Nom:');
define('TEXT_STREET', 'Rue:');
define('TEXT_ZIP', 'Code postal:');
define('TEXT_CITY', 'Ville:');
define('TEXT_COUNTRY', 'Pays:');
define('TEXT_CUSTOMER_GROUP', 'Groupe de clients de la commande');
define('TEXT_CUSTOMER_EMAIL', 'Adresse email:');
define('TEXT_CUSTOMER_TELEPHONE', 'Téléphone:');
define('TEXT_CUSTOMER_UST', 'Numéro de TVA:');
define('TEXT_CUSTOMER_CID', 'Numéro client:');
define('TEXT_ORDERS_ADDRESS_EDIT_INFO', 'Veuillez noter que les données saisies seront seulement changées dans la commande, et non dans le compte client!');

// Artikelbearbeitung

define('TEXT_SMALL_NETTO', '(net)');
define('TEXT_PRODUCT_ID', 'pID:');
define('TEXT_PRODUCTS_MODEL', 'Numéro d\'article:');
define('TEXT_QUANTITY', 'Quantité:');
define('TEXT_PRODUCT', 'Article:');
define('TEXT_TAX', 'TVA:');
define('TEXT_PRICE', 'Prix:');
define('TEXT_FINAL', 'Total:');
define('TEXT_PRODUCT_SEARCH', 'Recherche d\'articles:');

define('TEXT_PRODUCT_OPTION', 'Spéficifités d\'article:');
define('TEXT_PRODUCT_OPTION_VALUE', 'Valeur d\'option');
define('TEXT_PRICE_PREFIX', 'Préfixe de prix:');
define('TEXT_SAVE_ORDER', 'Terminer la commande et recalculer');
define('TEXT_INS', 'Ajouter:');
define('TEXT_SHIPPING', 'Module de frais de port');
define('TEXT_COD_COSTS', 'Frais de contre remboursement');
define('TEXT_VALUE', 'Prix');
define('TEXT_DESC', 'Insérer');

// Sonstiges

define('TEXT_PAYMENT', 'Mode de paiement:');
define('TEXT_SHIPPING', 'Mode de livraison:');
define('TEXT_LANGUAGE', 'Langue:');
define('TEXT_CURRENCIES', 'Devises:');
define('TEXT_ORDER_TOTAL', 'Résumé:');
define('TEXT_SAVE', 'Enregistrer');
define('TEXT_ACTUAL', 'Actuellement:');
define('TEXT_NEW', 'Nouveau:');
define('TEXT_PRICE', 'Frais:');

define('TEXT_ADD_TAX', 'inclus');
define('TEXT_NO_TAX', 'plus');

define('TEXT_ORDERS_EDIT_INFO', '<b>Remarques importantes:</b><br />
Choissisez le bon groupe de clients chez les adresses et données de client.<br />
Lors d\'un changement de groupe de clients, tous les éléments individuels de la facture doivent être enregistrés de nouveau!<br />
Les frais de port doivent être changés manuellement!<br />
Ici vous devez indiquer les frais de port en net ou brut selon le groupe de clients!<br />
');

define('TEXT_CUSTOMER_GROUP_INFO', 'Lors d\'un changement de groupe de clients, tous les éléments individuels de la facture doivent être enregistrés de nouveau!');

define('TEXT_ORDER_TITLE', 'Titre:');
define('TEXT_ORDER_VALUE', 'Valeur:');
define('ERROR_INPUT_TITLE', 'Pas de saisie pour le titre');
define('ERROR_INPUT_EMPTY', 'Pas de saisie pour le titre et la valeur');
define('ERROR_INPUT_SHIPPING_TITLE', 'Aucun module de frais de port n\'a été choisi!');

// note for graduated prices
define('TEXT_ORDERS_PRODUCT_EDIT_INFO', '<b>Remarque:</b> Le prix à l\'unité doit être adapté manuellement pour les prix échélonnés!');

define('TEXT_FIRSTNAME', 'Prénom:');
define('TEXT_LASTNAME', 'Nom:');

define('TEXT_GENDER', 'M./Mme.');
define('TEXT_MR', 'M.');
define('TEXT_MRS', 'Mme.');

define('TEXT_SAVE_CUSTOMERS_DATA', 'Enregistrer les données du client');

define('TEXT_PRODUCTS_SEARCH_INFO', ' Nom de l\'article ou N° d\'article ou GTIN/EAN');
define('TEXT_PRODUCTS_STATUS', 'Statut:');
define('TEXT_PRODUCTS_IMAGE', 'Image de l\'article:');
define('TEXT_PRODUCTS_QTY', 'Stock:');
define('TEXT_PRODUCTS_EAN', 'GTIN/EAN:');
define('TEXT_PRODUCTS_TAX_RATE', 'Taux d\'imposition:');
define('TEXT_PRODUCTS_DATE_AVAILABLE', 'Date de publication:');
define('TEXT_IMAGE_NONEXISTENT', '---');
?>

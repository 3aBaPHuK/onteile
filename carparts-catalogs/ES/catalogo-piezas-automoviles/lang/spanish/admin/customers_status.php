<?php
/* --------------------------------------------------------------
   $Id: customers_status.php 1062 2005-07-21 19:57:29Z gwinger $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(customers.php,v 1.76 2003/05/04); www.oscommerce.com 
   (c) 2003	 nextcommerce (customers_status.php,v 1.12 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Grupos de clientes');

define('ENTRY_CUSTOMERS_FSK18', '¿Bloquear la compra de artículos FSK18?');
define('ENTRY_CUSTOMERS_FSK18_DISPLAY', '¿Visualización de artículos FSK18?');
define('ENTRY_CUSTOMERS_STATUS_ADD_TAX', 'Indicar el IVA en la factura');
define('ENTRY_CUSTOMERS_STATUS_MIN_ORDER', 'Valor mínimo de pedido:');
define('ENTRY_CUSTOMERS_STATUS_MAX_ORDER', 'Valor máximo del pedido:');
define('ENTRY_CUSTOMERS_STATUS_BT_PERMISSION', 'Por domiciliación bancaria');
define('ENTRY_CUSTOMERS_STATUS_CC_PERMISSION', 'Por tarjeta de crédito');
define('ENTRY_CUSTOMERS_STATUS_COD_PERMISSION', 'Por pago contra reembolso');
define('ENTRY_CUSTOMERS_STATUS_DISCOUNT_ATTRIBUTES', 'Descuento');
define('ENTRY_CUSTOMERS_STATUS_PAYMENT_UNALLOWED', 'Introduzca las modalidades de pago no permitidas');
define('ENTRY_CUSTOMERS_STATUS_PUBLIC', 'Público');
define('ENTRY_CUSTOMERS_STATUS_SHIPPING_UNALLOWED', 'Introduzca modos de envío no permitidos');
define('ENTRY_CUSTOMERS_STATUS_SHOW_PRICE', 'Precio');
define('ENTRY_CUSTOMERS_STATUS_SHOW_PRICE_TAX', 'Precios con IVA incluido');
define('ENTRY_CUSTOMERS_STATUS_WRITE_REVIEWS', '¿Se permite al grupo de clientes escribir reseñas de productos?');
define('ENTRY_CUSTOMERS_STATUS_READ_REVIEWS', '¿Se permite al grupo de clientes leer reseñas de productos?');
define('ENTRY_CUSTOMERS_STATUS_REVIEWS_STATUS', '¿Desbloquear automáticamente las reseñas de productos?');
define('ENTRY_GRADUATED_PRICES', 'Precios escalonados');
define('ENTRY_NO', 'No');
define('ENTRY_OT_XMEMBER', '¿Descuento de cliente sobre el valor total del pedido? :');
define('ENTRY_YES', 'Sí');

define('ERROR_REMOVE_DEFAULT_CUSTOMERS_STATUS', 'Error: No se puede borrar el grupo de clientes por defecto. Defina primero un grupo de clientes por defecto diferente e inténtelo de nuevo.');
define('ERROR_STATUS_USED_IN_CUSTOMERS', 'Error: Este grupo de clientes está actualmente en uso por los clientes.');
define('ERROR_STATUS_USED_IN_HISTORY', 'Error: Este grupo de clientes se utiliza actualmente en el resumen de pedidos.');

define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_CUSTOMERS_GRADUATED', 'Precios escalonados');
define('TABLE_HEADING_CUSTOMERS_STATUS', 'Grupos de clientes');
define('TABLE_HEADING_CUSTOMERS_UNALLOW', 'modalidades de pago no permitidas');
define('TABLE_HEADING_CUSTOMERS_UNALLOW_SHIPPING', 'modos de envío no permitidos');
define('TABLE_HEADING_DISCOUNT', 'Descuento');
define('TABLE_HEADING_TAX_PRICE', 'Precio / IVA ');

define('TAX_NO', 'excl.');
define('TAX_YES', 'incl.');

define('TEXT_DISPLAY_NUMBER_OF_CUSTOMERS_STATUS', 'Grupos de clientes existentes:');

define('TEXT_INFO_CUSTOMERS_FSK18_DISPLAY_INTRO', '<strong>Artículos FSK18</strong>');
define('TEXT_INFO_CUSTOMERS_FSK18_INTRO', '<strong>Bloqueo FSK18</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_ADD_TAX_INTRO', '<strong>Si "Precios con IVA incluido" = "Sí", entonces ajuste a "No" </strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_MIN_ORDER_INTRO', '<strong>Introduzca un valor mínimo de pedido o deje este campo en blanco.</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_MAX_ORDER_INTRO', '<strong>Introduzca un valor máximo del pedido o deje este campo en blanco.</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_BT_PERMISSION_INTRO', '<strong>¿Desea permitir que este grupo de clientes pueda pagar mediante domiciliación bancaria?</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_CC_PERMISSION_INTRO', '<strong>¿Desea permitir que este grupo de clientes pueda pagar mediante tarjeta de crédito?</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_COD_PERMISSION_INTRO', '<strong>¿Desea permitir que este grupo de clientes pueda pagar mediante pago contra reembolso?</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_DISCOUNT_ATTRIBUTES_INTRO', '<strong>Descuento en atributos de artículos</strong><br />(Aplicar un descuento máximo de % a un artículo)');
define('TEXT_INFO_CUSTOMERS_STATUS_DISCOUNT_OT_XMEMBER_INTRO', '<strong>Descuento en el total del pedido</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_DISCOUNT_PRICE', 'Descuento (0 a 100%):');
define('TEXT_INFO_CUSTOMERS_STATUS_DISCOUNT_PRICE_INTRO', '<strong>Máximo descuento en productos (dependiendo del descuento establecido en el producto).</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_GRADUATED_PRICES_INTRO', '<strong>Precios escalonados</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_IMAGE', '<strong>Imagen de grupo de clientes:</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_NAME', '<strong>Nombre del grupo</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_PAYMENT_UNALLOWED_INTRO', '<strong>modalidades de pago no permitidas</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_PUBLIC_INTRO', '<b>Gruppe Öffentlich ?</b>');
define('TEXT_INFO_CUSTOMERS_STATUS_SHIPPING_UNALLOWED_INTRO', '<strong>modos de envío no permitidos</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_SHOW_PRICE_INTRO', '<strong>Visualización de precio en la tienda</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_SHOW_PRICE_TAX_INTRO', '<strong>¿Desea mostrar los precios con o sin IVA?</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_WRITE_REVIEWS_INTRO', '<strong>Escribir reseñas de producto</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_READ_REVIEWS_INTRO', '<strong>Leer reseñas de producto</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_REVIEWS_STATUS_INTRO', '<strong>Desbloquear reseñas de productos</strong>');

define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar este grupo de clientes?');
define('TEXT_INFO_EDIT_INTRO', 'Por favor, realice todos los ajustes necesarios.');
define('TEXT_INFO_INSERT_INTRO', 'Por favor, cree un nuevo grupo de clientes con los ajustes deseados.');

define('TEXT_INFO_HEADING_DELETE_CUSTOMERS_STATUS', 'Borrar grupo de clientes');
define('TEXT_INFO_HEADING_EDIT_CUSTOMERS_STATUS', 'Editar datos de grupo');
define('TEXT_INFO_HEADING_NEW_CUSTOMERS_STATUS', 'Nuevo grupo de clientes');

define('TEXT_INFO_CUSTOMERS_STATUS_BASE', '<strong>Grupo básico de clientes para los precios de los artículos</strong>');
define('ENTRY_CUSTOMERS_STATUS_BASE', 'Adoptar los precios de grupo de clientes del siguiente grupo de clientes. Si se selecciona = "Admin", no se copiarán precios para el nuevo grupo de clientes.');
define('ENTRY_CUSTOMERS_STATUS_BASE_EDIT', 'Adoptar los precios de grupo de clientes del siguiente grupo de clientes. Si se selecciona = "Admin", no se copiarán precios para el grupo de clientes.<br /><span class="col-red"><strong>ATENCIÓN:</strong></span> ¡Esto sobrescribe todos los precios de grupo de clientes existentes del grupo de clientes!');

define('TEXT_INFO_CUSTOMERS_GROUP_ADOPT_PERMISSION', '<strong>Adoptar derechos de visibilidad de otro grupo de clientes</strong>');
define('ENTRY_CUSTOMERS_GROUP_ADOPT_PERMISSION', 'Adoptar los derechos de visibilidad de categoría, artículo y contenido del siguiente grupo de clientes:');
define('CUSTOMERS_GROUP_ADOPT_PERMISSIONS', 'No adoptar derechos');

define('TEXT_INFO_CUSTOMERS_STATUS_SHOW_PRICE_TAX_TOTAL', '<b>Mostrar IVA a partir del importe de compra</b>');
define('ENTRY_CUSTOMERS_STATUS_SHOW_PRICE_TAX_TOTAL', 'Importe mínimo de compra');

define('TABLE_HEADING_CUSTOMERS_SPECIALS', 'Ofertas especiales');
define('TEXT_INFO_CUSTOMERS_STATUS_SPECIALS_INTRO', '<strong>Ofertas especiales</strong>');
define('ENTRY_CUSTOMERS_STATUS_SPECIALS', '¿Se permite al grupo de clientes ver las ofertas especiales?');

define('CUSTOMERS_GROUP_PUBLIC', 'público');
?>

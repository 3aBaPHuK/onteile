<?php
/** 
 * -----------------------------------------------------------------------------------------
 * PDF-RechnungNEXT by Robert Hoppe
 * Copyright 2011 Robert Hoppe - xtcm@katado.com - http://www.katado.com
 *
 * Please visit http://pdfnext.katado.com for newer Versions
 * contribution: fixed errors and adapted, noRiddle 10-2017
 * -----------------------------------------------------------------------------------------
 *  
 * Released under the GNU General Public License 
 * Initial Version By Leonid Lezner. leolezner@yahoo.de 
 */
require_once(DIR_FS_CATALOG . 'includes/classes/FPDF/PdfBrief.php');

class PdfRechnung extends PdfBrief
{
    // BillNr, Date
	var $rechnungsdaten_x = 110; //120; /*noRiddle*/
	var $rechnungsdaten_y = 40; //70; /*noRiddle*/
	var $rechnung_start = 90; //100; /*noRiddle*/
	var $menge_len = 15;
	var $artikel_len = 80;
	var $artikelnr_len = 25;
	var $einzelpreis_len = 28;
	var $preis_len = 28;
	
	var $max_y = 240;
	var $artikelnr_len_field = 10;
    
    var $pm; //new for payment method, 04-2021, noRiddle
	
    /**
     * Constructor
     * 
     * @access public
     * @var int $customers_id
     * @var int $bill_nr
     * @var int $oID
     * @var string $orders_date
     * @var $string $payment_method
     * @var boolean $deliverSlip
     * @return void
     */

    function __construct($pm) {
        parent::__construct($pm);
        $this->pm = $pm; //new for payment method, 04-2021, noRiddle
    }

   /* protected function additional_params() {
        return $this->pm;
    } //new function to use in parent class for payment method, 04-2021, noRiddle*/

	function Rechnungsdaten($customers_id, $bill_nr, $oID, $orders_date, $payment_method, $deliverSlip = false)  
	{ 
		$this->SetX($this->rechnungsdaten_x); 
		$this->SetLeftMargin($this->rechnungsdaten_x); 
		$this->SetY($this->rechnungsdaten_y); 
		//$this->SetFont($this->fontfamily,'', 11);
        $this->SetFont($this->fontfamily, 'B', 9);
	
        // Top Text
        //if(!$deliverSlip) {
        if($deliverSlip === false) { //corrected, noRiddle
            //BOC set colors, noRiddle
            $this->SetFillColor(83,114,166);
            $this->SetTextColor(255,255,255);
            //EOC set colors, noRiddle
            //$this->Cell(0, 6, TEXT_PDF_RECHNUNG_HEAD, 'LRTB', 1, 'C', 0);
            $this->Cell(0, 6, xtc_utf8_decode(TEXT_PDF_RECHNUNG_HEAD), 'LRTB', 1, 'C', 1); // allow fill color with last param to 1, noRiddle
            //BOC reset color
            $this->SetFillColor(230,230,230);
            $this->SetTextColor(0,0,0);
            //EOC reset color
        } else {
            //BOC set colors, noRiddle
            $this->SetFillColor(83,114,166);
            $this->SetTextColor(255,255,255);
            //EOC set colors, noRiddle
            //$this->Cell(0, 6, TEXT_PDF_LIEFERSCHEIN_HEAD, 'LRTB', 1, 'C', 0);
            $this->Cell(0, 6, xtc_utf8_decode(TEXT_PDF_LIEFERSCHEIN_HEAD), 'LRTB', 1, 'C', 1);  // allow fill color with last param to 1, noRiddle
            //BOC reset color
            $this->SetFillColor(230,230,230);
            $this->SetTextColor(0,0,0);
            //EOC reset color
        }

        // spacer
        $this->Cell(0, 1, '', 0, 1);

        // customers_id
        if(!empty($customers_id)) { //display this only if its not empty (see xtc_pdf_bill.inc.php), noRiddle
            //$this->Cell(0, 6, TEXT_PDF_KUNDENNUMMER . ': ' . $customers_id, 'LRT', 1, '', 0);
            $this->Cell(0, 5, xtc_utf8_decode(TEXT_PDF_KUNDENNUMMER) . ': ' . $customers_id, 'LRT', 1, '', 0); /*noRiddle*/
        }

        // no develiperSlip? We have a bill!
		//if(!$deliverSlip) {
        if($deliverSlip === false) { //corrected, noRiddle
            //BOC we handle that in our new file /admin(includes/modules/invoice_number/invoicepdf_number_functions, noRiddle
            // use oID instead of bill_nr
            /*if (PDF_USE_ORDERID == 'true') {
				$this->Cell(0, 6, TEXT_PDF_RECHNUNGSNUMMER . ': '. PDF_USE_ORDERID_PREFIX . $oID, 'LR', 1, '', 0); 
			} else { 
				$this->Cell(0, 6, TEXT_PDF_RECHNUNGSNUMMER . ': '. PDF_USE_ORDERID_PREFIX . $bill_nr, 'LR', 1, '', 0); 
			}*/
            //$this->Cell(0, 6, TEXT_PDF_RECHNUNGSNUMMER . ': ' . $bill_nr, 'LR', 1, '', 0);
            $this->Cell(0, 5, xtc_utf8_decode(TEXT_PDF_RECHNUNGSNUMMER) . ': ' . $bill_nr, 'LR', 1, '', 0);  /*noRiddle*/
            //EOC we handle that in our new file /admin(includes/modules/invoice_number/invoicepdf_number_functions, noRiddle
		} else { 
			//$this->Cell(0, 6, TEXT_PDF_BESTELLNUMMER . ': '. $oID, 'LR', 1, '', 0);
            $this->Cell(0, 5, xtc_utf8_decode(TEXT_PDF_BESTELLNUMMER) . ': '. $oID, 'LR', 1, '', 0); /*noRiddle*/
		} 
		
		 

    
        //if(!$deliverSlip) {
        if($deliverSlip === false) {  //corrected, noRiddle
			//$this->Cell(0, 6, TEXT_PDF_DATUM . ': ' . $orders_date, 'LR', 1, '', 0);
            //$this->Cell(0, 6, TEXT_PDF_ZAHLUNGSWEISE . ': ' . xtc_utf8_decode(html_entity_decode(strip_tags($payment_method))), 'LRB', 1, '', 0); //added xtc_utf8_decode, noRiddle
            $this->Cell(0, 5, xtc_utf8_decode(TEXT_PDF_DATUM) . ': ' . $orders_date, 'LR', 1, '', 0); /*noRiddle*/
            $this->Cell(0, 5, xtc_utf8_decode(TEXT_PDF_ZAHLUNGSWEISE) . ': ' . xtc_utf8_decode(html_entity_decode(strip_tags($payment_method))), 'LRB', 1, '', 0); //added xtc_utf8_decode, noRiddle /*noRiddle*/
		} else {
			//$this->Cell(0, 6, TEXT_PDF_DATUM . ': ' . $orders_date, 'LRB', 1, '', 0);
            $this->Cell(0, 5, xtc_utf8_decode(TEXT_PDF_DATUM) . ': ' . $orders_date, 'LRB', 1, '', 0); /*noRiddle*/
		}
	}
	
    /**
     * Start PDFBill
     *
     * @access public
     * @var string $customers_name
     * @var string $gender
     * @var boolean $deliverSlip
     * @return void
     */
	function RechnungStart($customers_name, $gender, $deliverSlip = false)
	{
		$this->SetX($this->left_textoffset);
		$this->SetLeftMargin($this->left_textoffset);
		$this->SetY($this->rechnung_start);

        if(TEXT_PDF_LIEFERSCHEIN != '' && $deliverSlip) {
            //$this->SetFont($this->fontfamily, 'B', 16);
            $this->SetFont($this->fontfamily, '', 12); /*noRiddle*/
            $this->Cell(0, 6, xtc_utf8_decode(TEXT_PDF_LIEFERSCHEIN), 0, 1); //TEXT_PDF_RECHNUNG.' '.HTTP_SERVER.'/'.SHOP
            //$this->Ln(); //we have line feed in ListeKopf already, don't waste so much space, noRiddle
        } else if(TEXT_PDF_RECHNUNG != '' && !$deliverSlip) {
            //$this->SetFont($this->fontfamily, 'B', 16);
            $this->SetFont($this->fontfamily, '', 12); /*noRiddle*/
            $this->Cell(0, 6, xtc_utf8_decode(TEXT_PDF_RECHNUNG), 0, 1); //TEXT_PDF_RECHNUNG.' '.HTTP_SERVER.'/'.SHOP
            //$this->Ln(); //we have line feed in ListeKopf already, don't waste so much space, noRiddle
        }
            
            $this->SetFont($this->fontfamily, '', 12);
		
		switch($gender) {
			case 'm':
				$message = xtc_utf8_decode(TEXT_PDF_DANKE_MANN); //added xtc_utf8_decode, noRiddle
				break;
			case 'f':
				$message = xtc_utf8_decode(TEXT_PDF_DANKE_FRAU); //added xtc_utf8_decode, noRiddle
				break;
			case 'u':
			default:
				$message = xtc_utf8_decode(TEXT_PDF_DANKE_UNISEX); //added xtc_utf8_decode, noRiddle
		}
	
		if($message != '')
            $this->MultiCell(0, 6, sprintf($message, xtc_utf8_decode($customers_name)), 0); //added xtc_utf8_decode, noRiddle | added if != '' to avoid space
	}
	
    /**
     * Listhead
     *
     * @access public
     * @var boolean $deliverSlip
     * @return void
     */
	function ListeKopf($deliverSlip = false) 
	{ 
		$this->SetFont($this->fontfamily, 'B', 10); 
		$this->Ln();

		//$this->Cell($this->menge_len, 6, TEXT_PDF_MENGE.' ', 'B', 0, 'R');
        $this->Cell($this->menge_len, 6, xtc_utf8_decode(TEXT_PDF_MENGE).' ', 'B', 0, 'L'); //put entry left, not right, noRiddle
		$this->Cell($this->artikel_len, 6, xtc_utf8_decode(TEXT_PDF_ARTIKEL), 'B', 0); 
		$this->Cell($this->artikelnr_len, 6, xtc_utf8_decode(TEXT_PDF_ARTIKELNR), 'B', 0); 

		$this->Cell($this->einzelpreis_len, 6, ($deliverSlip) ? '' : xtc_utf8_decode(TEXT_PDF_EINZELPREIS), 'B', 0, 'R'); 
        $this->Cell($this->preis_len, 6, ($deliverSlip) ? '' : xtc_utf8_decode(TEXT_PDF_PREIS), 'B', 0, 'R');

		$this->Ln(8); 
	}
	
	function get_used_lines($order_data_values) {
		$this->SetFont($this->fontfamily,'', 11);
		
		$len_1 = count($this->split_text_in_array($order_data_values['products_model'],$this->artikelnr_len));
		$len_2 = count($this->split_text_in_array(html_entity_decode($order_data_values['products_name']), ($this->artikel_len - 5)));
		
		if($len_1 < $len_2) {
			return $len_2;
		}
		return $len_1;
	}
	
    /**
     * Add Product to list
     *
     * @access public
     * @var int $menge
     * @var string $artikel
     * @var string $zuinfos
     * @var int $artnr
     * @var string $zuinfosartnr
     * @var double $einzelpreis
     * @var double $preis 
     * @return void
     */
	function ListeProduktHinzu($menge, $artikel, $zusinfos, $artnr, $zusinfoartnr, $einzelpreis, $preis)
	{
		$this->SetFont($this->fontfamily,'', 11);
				
        // split products description into parts
		//$parts = preg_split("/[\s]+/", html_entity_decode($artikel), -1, PREG_SPLIT_DELIM_CAPTURE);
		
		$parts = $this->split_text_in_array(xtc_utf8_decode(html_entity_decode($artikel)), $this->artikel_len - 5); //added xtc_utf8_decode, noRiddle
		$parts_artikelnumber = $this->split_text_in_array($artnr, $this->artikelnr_len);
		
		$max = count($parts);
		if($max < count($parts_artikelnumber)) {
			$max = count($parts_artikelnumber);
		}
		
		//if($this->getY()+(count($parts)/5*2) >= $this->max_y || $this->getY()+((count($parts)-1)/5*2) >= $this->max_y){
		//	$this->addPage();
		//}
	
		for($i = 0; $i < $max; $i++) {
			
			if($i == 0) {
				//$this->Cell($this->menge_len, 6, $menge.' x ', 0, 0, 'R');
                $this->Cell($this->menge_len, 6, $menge.' x ', 0, 0, 'C'); //put entry center, not right, noRiddle
			} else {
				$this->Cell($this->menge_len, 6, '', 0);
			}
			
			$this->Cell($this->artikel_len, 6, $parts[$i], 0);
			
			if($i == 0) {
				$this->Cell($this->artikelnr_len, 6, $parts_artikelnumber[$i], 0, 0, '');
				$this->Cell($this->einzelpreis_len, 6, $einzelpreis, 0, 0, 'R');
				$this->Cell($this->preis_len, 6, $preis, 0, 0, 'R');		
			} else {
				$this->Cell($this->artikelnr_len, 6, $parts_artikelnumber[$i], 0);
				$this->Cell($this->einzelpreis_len, 6, '', 0);
				$this->Cell($this->preis_len, 6, '', 0);
			}
	
			$this->Ln();
		}
		
		//$this->Ln(2);
        $this->Ln(1);
		
        // Additional Informations available?
		if($zusinfos != '') {
			$this->SetFont($this->fontfamily, 'I', 9);
			
            //BOC replace old function split with explode, noRiddle
			//$zusinfos_arr = split("\n", xtc_utf8_decode($zusinfos)); //added xtc_utf8_decode, noRiddle
			//$zusinfoartnr_arr = split("\n", $zusinfoartnr);
            $zusinfos_arr = explode("\n", xtc_utf8_decode($zusinfos)); //added xtc_utf8_decode, noRiddle
			$zusinfoartnr_arr = explode("\n", $zusinfoartnr);
            //EOC replace old function split with explode, noRiddle
			
			for($i = 0; $i < count($zusinfos_arr); $i++) {
				$this->Cell($this->menge_len, 6, '', 0, 0, '');
				$this->Cell($this->artikel_len, 6, $zusinfos_arr[$i], 0, 0, '');
				
				if($i < count($zusinfoartnr_arr)) {
					$this->Cell($this->artikelnr_len, 6, $zusinfoartnr_arr[$i], 0, 0, '');
                }
					
				$this->Ln();
			}
		}
	}
	
    /**
     * Order Total
     * 
     * @access public
     * @param mixed array $orderdata
     * @return void 
     */
	function Betrag($orderdata)
	{
		$this->SetFont($this->fontfamily, '', 11);
		
		$this->Cell($this->menge_len + $this->artikel_len + $this->einzelpreis_len + $this->artikelnr_len + $this->preis_len, 6, '', 'T');
		$this->Ln(2);
		
		foreach($orderdata as $info) {
			$text = $info['text'];
            $text = html_entity_decode($text);

			$info['title'] = str_replace("::", ":", $info['title']);
            $info['title'] = html_entity_decode($info['title']);
			
			if(strpos($text, "<b>") !== false) {
				$this->SetFont($this->fontfamily, 'B', 11);

				$text = strip_tags($text);
				$info['title'] = strip_tags($info['title']);
				
				if($info['class'] == 'ot_total') {
					$this->Ln(2);
					
					$sum_len = 25;
					
					$this->Cell($this->menge_len + $this->artikel_len + $this->einzelpreis_len + $this->artikelnr_len - $sum_len, 1, "", '', 0);
					$this->Cell($this->preis_len + $sum_len - 1, 1, "", 'T', 1);
					$this->Cell($this->menge_len + $this->artikel_len + $this->einzelpreis_len + $this->artikelnr_len - $sum_len, 1, "", '', 0);
					$this->Cell($this->preis_len + $sum_len - 1, 1, "", 'T', 1);
				}
			} else if(strpos($text, "<font") !== false) {
				$text = strip_tags($text);
				$this->SetTextColor(205,0,0);
			} else {
				$info['title'] = strip_tags($info['title']);
				$text = strip_tags($text);
				$this->SetFont($this->fontfamily, '', 11);
				$this->SetTextColor(0,0,0);
			}
			
			$this->Cell($this->menge_len + $this->artikel_len + $this->einzelpreis_len + $this->artikelnr_len, 6, $info['title'], 0, 0, 'R');
			$this->Cell($this->preis_len, 6, $text, 0, 1, 'R');
		}
	}
	
    /**
     * Add Comment
     *
     * @access public
     * @param string $text
     * @return void
     */
	function Kommentar($text)
	{
		if($text == '') {
            return;
        }
		
		$this->Ln(10);
		
		//BOC commented out, $order->info['comment'] is enough, set in xtc_pdf_bill.inc.php, noRiddle
        //$this->SetFont($this->fontfamily, 'B', 11);
		//$this->Cell($this->preis_len, 10, TEXT_PDF_KOMMENTAR, 0, 1);
        //EOC commented out, $order->info['comment'] is enough, set in xtc_pdf_bill.inc.php, noRiddle
		
		$this->SetFont($this->fontfamily, '', 11);
		$this->MultiCell(0, 4, $text);
	}
	
    /**
     * Bill End
     *
     * @access public
     * @param boolean $deliverSlip
     * @return void
     */
	function RechnungEnde($deliverSlip = false) {
		$endText = ($deliverSlip === true)? xtc_utf8_decode(TEXT_PDF_LSCHLUSSTEXT) : xtc_utf8_decode(TEXT_PDF_SCHLUSSTEXT); //added xtc_utf8_decode, noRiddle
		
		if(!empty($endText)) { 
			$this->Ln(10); 
			$this->SetFont($this->fontfamily, '', 12); 
			$this->MultiCell(0, 6, $endText);
		}
    }
    
    function split_text_in_array($text, $max_len) {
		$text_parts = preg_split("/[\s-]+/", $text);
		
		$text_array = array();
		// artikelnr. array erzeugen
		$tmp_part = '';
		$prefix='';
		foreach($text_parts as $part) {
			if($this->GetStringWidth($tmp_part.$prefix.$part) <= $max_len) {
				$tmp_part.=$prefix.$part;
				$prefix=' ';
			}else{
				array_push($text_array, $tmp_part);
				$prefix=' ';
				$tmp_part=$part;
			}
		}
		if(!empty($tmp_part)){
			array_push($text_array, $tmp_part);
		}
		for($i=0; $i<count($text_array); $i++){
			if(empty($text_array[$i])){
				unset($text_array[$i]);
			}
		}
		//$text_array = array_reverse($text_array);
		return $text_array;
	}
}
?>

<?php
/** 
 * -----------------------------------------------------------------------------------------
 * PDFBill NEXT by Robert Hoppe
 * Copyright 2011 Robert Hoppe - xtcm@katado.com - http://www.katado.com
 *
 * Please visit http://pdfnext.katado.com for newer Versions
 * -----------------------------------------------------------------------------------------
 *  
 * Released under the GNU General Public License 
 * 
 */
define('PDF_BILL_NR_TITLE', 'Asignar número de factura');
define('PDF_BILL_NR_HEAD', 'Número de factura generado automáticamente:');
define('PDF_BILL_NR_GIVEN', '<h3>¡Número de factura ya asignado!</h3><br />Aquí está de nuevo para control: ');
define('PDF_BILL_NR_INFO', '¡Por favor, cambiar en caso necesario y confirmar!');
define('PDF_BILL_NR_SUBMIT', 'Confirmar');
define('PDF_CLOSE_WINDOW', 'Cerrar ventana');
define('PDF_CANCEL', 'Cancelar y cerrar ventana');
?>

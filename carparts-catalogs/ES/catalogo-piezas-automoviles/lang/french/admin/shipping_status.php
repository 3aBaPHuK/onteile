<?php
/* --------------------------------------------------------------
   $Id: shipping_status.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(orders_status.php,v 1.7 2002/01/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (orders_status.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Statut de livraison');

define('TABLE_HEADING_SHIPPING_STATUS_IMAGE', 'Image');
define('TABLE_HEADING_SHIPPING_STATUS', 'Statut de livraison');
define('TABLE_HEADING_SORT', 'Tri');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Veuillez faire les changements nécessaires');
define('TEXT_INFO_SHIPPING_STATUS_NAME', 'Statut de livraison:');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez entrer le nouveau statut de livraison avec toutes les données pertinentes');
define('TEXT_INFO_DELETE_INTRO', 'Êtes-vous sûr de vouloir supprimer ce statut de livraison?');
define('TEXT_INFO_HEADING_NEW_SHIPPING_STATUS', 'Nouveau statut de livraison');
define('TEXT_INFO_HEADING_EDIT_SHIPPING_STATUS', 'Editer le statut de livraison');
define('TEXT_INFO_SHIPPING_STATUS_IMAGE', 'Image:');
define('TEXT_INFO_HEADING_DELETE_SHIPPING_STATUS', 'Supprimer le statut de livraison');
define('TEXT_DELETE_IMAGE', 'Supprimer l\'image');
define('TEXT_INFO_SHIPPING_STATUS_SORT_ORDER', 'Tri:');

define('ERROR_REMOVE_DEFAULT_SHIPPING_STATUS', 'Erreur: Le statut de livraison par défaut ne peut pas être supprimé. Veuillez définir un nouveau statut de livraison par défaut et rééssayez.');
define('ERROR_STATUS_USED_IN_ORDERS', 'Erreur: Ce statut de livraison est actuellement utilisé pour des articles.');
define('ERROR_STATUS_USED_IN_HISTORY', 'Erreur: Ce statut de livraison est actuellement utilisé pour des articles.');
?>

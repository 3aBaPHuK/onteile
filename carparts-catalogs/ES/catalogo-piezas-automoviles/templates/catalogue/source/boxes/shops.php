<?php
/*************************************************
* file: shops.php
* use: read out content for "more shops" box
* (c) noRiddle 2017
*************************************************/

//BOC config
$which_cID = '67';

$shp_path = HTTP_SERVER.'/';
$img_path = $shp_path.'global_images/weitereshops20/';

switch ($_SESSION['language_code']) {
    case 'de':
        $shp_title_alt = 'Ersatzteile';
      break;
    case 'en':
        $shp_title_alt = 'spare parts';
      break;
    case 'fr':
        $shp_title_alt = 'pièces de rechange';
      break;
    case 'it':
        $shp_title_alt = 'ricambi';
      break;
    case 'en_us':
        $shp_title_alt = 'spare parts';
      break;
    case 'es_mx':
        $shp_title_alt = 'refacciones';
      break;
    case 'es':
        $shp_title_alt = 'recambios';
      break;
}

$shp_land_page_arr = explode('.', $remote_url[$cat_env]['domain']);
$shp_land_page = $shp_land_page_arr[1]; //landing page
$shp_excl_arr = array('austausch-ersatzteile', 'katalog_shop_test', 'oem-kataloge', $shp_land_page);

$shps_specials = array('abarth' => 'fiat'.$shop_suffix, 
                       'chrysler' => 'chrysler-dodge-jeep-ram'.$shop_suffix, 
                       'dodge' => 'chrysler-dodge-jeep-ram'.$shop_suffix,
                       'jeep' => 'chrysler-dodge-jeep-ram'.$shop_suffix,
                       'ram' => 'chrysler-dodge-jeep-ram'.$shop_suffix,
                       'infinity' => 'nissan'.$shop_suffix,
                       'lancia' => 'alfaromeo'.$shop_suffix,
                       'lexus' => 'toyota'.$shop_suffix,
                       'vauxhall' => 'opel'.$shop_suffix,
                       'fiat-professional' => 'fiat'.$shop_suffix,
                       'ford-nutzfahrzeuge' => 'ford'.$shop_suffix,
                       'mercedes-nutzfahrzeuge' => 'mercedes'.$shop_suffix,
                       'volkswagen-nutzfahrzeuge' => 'volkswagen'.$shop_suffix
                      );

//take off not yet existing shops
if(!empty($shps_specials)) {
    foreach($shps_specials as $k => $shp) {
        if(!array_key_exists($shp, $data20)) {
            unset($shps_specials[$k]);
        }
    }
}

$shps_additional = array(); //array('home' => $shp_path);
                        
if($cat_env == 'de') {
    $shps_additional['sonderposten'] = 'https://www.originalersatzteile-online.com';
}
//EOC config

// include smarty
include(DIR_FS_BOXES_INC . 'smarty_default.php');

$cache_id = md5($_SESSION['language'].$_SESSION['customers_status']['customers_status_id'].'moreshops'.$which_cID);

if (!$box_smarty->is_cached(CURRENT_TEMPLATE.'/boxes/box_shops.html', $cache_id) || !$cache) {
    $content_string = '';
    $content_heading = '';
 
    if (GROUP_CHECK == 'true') {
        $group_check = "AND group_ids LIKE '%c_".$_SESSION['customers_status']['customers_status_id']."_group%'";
    }
 
    $shop_cont_qu = xtc_db_query("SELECT content_id,
                                         content_title,
                                         content_heading,
                                         content_text,
                                         content_file
                                    FROM ".TABLE_CONTENT_MANAGER."
                                   WHERE content_group = '".$which_cID."'
                                   ".CONTENT_CONDITIONS."
                                     AND languages_id='".(int) $_SESSION['languages_id']."'"
                                );

    if($content_data = xtc_db_fetch_array($shop_cont_qu)) {
        $content_heading .= $content_data['content_title'];

        if($content_data['content_file'] != '' && is_file(DIR_FS_CATALOG.'media/content/'.$content_data['content_file'])) {
            include (DIR_FS_CATALOG.'media/content/'.$content_data['content_file']);
            if(defined('AH_AUTOMATIC_MORE_SHOPS') && AH_AUTOMATIC_MORE_SHOPS === true) {
                //require_once($path.'ah/settings/mysql_config.php'); //$path is defined in central configure.php
                //require_once($path.'ah2.0/settings2.0/mysql_config.php'); //we have thís already via application_top.php

                $content_string .= '<div class="butt-margbott"><div class="cssButtonPos4">
                                        <a href="'.$remote_url[$cat_env]['domain'].'" target="_blank">
                                            <span class="cssButton cssButtonColor1" title="'.AH_ALL_BRANDS_VIEW_TXT.'">
                                                <span class="cssButtonText">'.AH_ALL_BRANDS_MAIN_TXT.'</span><br />
                                                <span class="cssButtonText">'.AH_ALL_BRANDS_VIEW_TXT.'</span>
                                            </span>
                                        </a>
                                    </div></div>';
                
                $sh_arr = array();

                /*foreach($data as $key => $val) {
                    //$what_repl = '-'.$shp_sufix;
                    //$strp_key = str_replace($what_repl, '', $key);
                    $strp_key = str_replace($shop_suffix, '', $key);
                    $sh_arr[$strp_key] = $key;
                }*/

                foreach($data20 as $key20 => $val20) {
                    if($data20[$key20]['sql_user'] != '') {
                        //$what_repl20 = '-'.$shp_sufix;
                        //$strp_key20 = str_replace($what_repl20, '', $key20);
                        $strp_key20 = str_replace($shop_suffix, '', $key20);
                        $sh_arr[$strp_key20] = $key20;
                    }
                }
                
                $sh_arr = array_merge($sh_arr, $shps_specials);
                unset($sh_arr['chrysler-dodge-jeep-ram']);

                ksort($sh_arr);
                
                $sh_arr = array_merge($sh_arr, $shps_additional);
                //echo '<br /><br /><pre>'.print_r($sh_arr, true).'</pre>';

                foreach($sh_arr as $k => $v) {
                    if(!in_array($v, $shp_excl_arr)) {
                        if($v != $shop) {
                            if($k != 'sonderposten' && $k != 'home') {
                                $shp_title_alt_eff = strtoupper($k).' '.ucwords($shp_title_alt);
                                $href_eff = $remote_url[$cat_env]['domain'].'/'.$v;
                            } else {
                                if($k == 'sonderposten') {
                                    $shp_title_alt_eff = $_SESSION['language_code'] == 'de' ? ucwords($k) : 'Specials';
                                    $href_eff = $v;
                                } else if ($k == 'home') {
                                    $shp_title_alt_eff = $_SESSION['language_code'] == 'de' ? 'Zur Marken-&Uuml;bersicht' : 'To the brand overview';
                                    $href_eff = $v;
                                }
                            }
                            
                            $content_string .= '<a href="'.$href_eff.'/'.'" title="'.$shp_title_alt_eff.'" target="_blank" class="more-shps">'
                                             .'<img class="unveil" src="'.DIR_WS_BASE.'templates/'.CURRENT_TEMPLATE.'/css/images/loading.gif" data-src="'.$img_path.$k.'.png'.'" alt="'.$shp_title_alt_eff.'" width="40" height="40" />'
                                             .'</a>';
                        }
                    }
                }
            } else {
                $content_string .= $content_data['content_text'];
            }
        } else {
            $content_string .= $content_data['content_text'];
        }
        
        $box_smarty->assign('BOX_HEADING', $content_heading);
        $box_smarty->assign('BOX_CONTENT', $content_string);
    }
}
 
if (!$cache) {
    $box_SHOPS = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_shops.html');
} else {
    $box_SHOPS = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_shops.html', $cache_id);
}
 
$smarty->assign('box_SHOPS', $box_SHOPS);
?>
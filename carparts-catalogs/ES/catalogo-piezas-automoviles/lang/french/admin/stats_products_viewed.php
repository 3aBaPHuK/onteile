<?php
/* --------------------------------------------------------------
   $Id: stats_products_viewed.php 3542 2012-08-27 10:52:20Z dokuman $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(stats_products_viewed.php,v 1.6 2002/03/30); www.oscommerce.com
   (c) 2003 nextcommerce (stats_products_viewed.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Articles les plus visités');
define('TABLE_HEADING_MODEL', 'Numéro d\'article');
define('TABLE_HEADING_NUMBER', 'N°');
define('TABLE_HEADING_PRODUCTS', 'Articles');
define('TABLE_HEADING_VIEWED', 'Visité');
?>

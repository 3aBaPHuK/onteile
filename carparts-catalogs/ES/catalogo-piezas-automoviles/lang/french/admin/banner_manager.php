<?php
/* --------------------------------------------------------------
   $Id: banner_manager.php 10156 2016-07-27 08:40:37Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(banner_manager.php,v 1.25 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (banner_manager.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'gestionnaire bannière');

define('TABLE_HEADING_BANNERS', 'Bannière');
define('TABLE_HEADING_GROUPS', 'Groupe');
define('TABLE_HEADING_STATISTICS', 'Afficher / clics');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_LANGUAGE', 'Langue');

define('TEXT_BANNERS_TITLE', 'Titre de la bannière:');
define('TEXT_BANNERS_URL', 'URL de la bannière:');
define('TEXT_BANNERS_URL_NOTE', 'URL cible en cliquant sur la bannière.');
define('TEXT_BANNERS_GROUP', 'Groupe de bannière:');
define('TEXT_BANNERS_NEW_GROUP', 'Choisissez le groupe de bannière souhaité dans le menu déroulant (si existant) ou indiquez un nouveau groupe de bannière ci-dessous.');
define('TEXT_BANNERS_NEW_GROUP_NOTE', 'Afin d\'afficher une bannière dans le modèle, élargissez le modèle. <br/>Exemple: groupe de bannière est bannière. La bannière peut être affichée dans le modèle sous index.html avec {$BANNER}');
define('TEXT_BANNERS_IMAGE', 'Image (fichier):');
define('TEXT_BANNERS_IMAGE_LOCAL', 'Choisissez l\'image souhaité en cliquant sur "Naviguer" ou choisissez une bannière existante. <br /><strong>Type de fichier permis:</strong> jpg, jpeg, jpe, gif, png, bmp, tiff, tif, bmp, swf, cab');
define('TEXT_BANNERS_IMAGE_TARGET', 'But d\'image (sauvegarder après):');
define('TEXT_BANNERS_HTML_TEXT', 'texte HTML:');
define('TEXT_BANNERS_HTML_TEXT_NOTE', 'Vous pouvez y entrer directement le code HTML d\'un service affilié pour affichage de la bannière.');
define('TEXT_BANNERS_EXPIRES_ON', 'Valable jusqu\'à:');
define('TEXT_BANNERS_OR_AT', ', ou chez');
define('TEXT_BANNERS_IMPRESSIONS', 'Impressions/ visualisations.');
define('TEXT_BANNERS_SCHEDULED_AT', 'Valable à partir de:');
define('TEXT_BANNERS_BANNER_NOTE', '<b> remarque bannière:</b><ul><li> Vous pouvez utiliser la bannière image ou la bannière texte-HTML. Les deux en même temps ne sont pas possible. </li><li>Si vous utilisez les deux type de bannière en même temps, seulement la bannière texte-HTML sera affichée.</li></ul>');
define('TEXT_BANNERS_INSERT_NOTE', '<b>Remarque:</b><ul><li> Vous devez avoir le droit d‘ecriture sur le répertoire d‘image! </li><li>Ne remplissez pas le champ /but d‘image (sauvegarder après)‘, si vous ne voulez pas copier d‘image sur votre serveur (p.ex. si l‘image se trouve déjà sur le serveur). </li><li>Le champ \'but d‘image (Sauvegarder après)\' doit être un champ avec un répertoire déjà existant qui finit  en \'/\' (p.ex.  banners/).</li></ul>');
define('TEXT_BANNERS_EXPIRCY_NOTE', '<b>Validité remarque:</b><ul><li>Ne remplissez qu‘un champ!</li><li>Si la bannière doit s‘afficher de manière illimité, laissez ce champ vide. </li></ul>');
define('TEXT_BANNERS_SCHEDULE_NOTE', '<b>Validité à partir de la remarque:</b><ul><li>En utilisant cette fonction, la bannière sera affichée à partir de la date indiquée. </li><li>Toutes les bannières avec cette fonction affichent désactivé jusqu‘à ce qu’ils sont activées. </li></ul>');

define('TEXT_BANNERS_DATE_ADDED', 'Ajouté le:');
define('TEXT_BANNERS_SCHEDULED_AT_DATE', 'Validité à partir de: <b>%s</b>');
define('TEXT_BANNERS_EXPIRES_AT_DATE', 'Validité jusqu\'à <b>%s</b>');
define('TEXT_BANNERS_EXPIRES_AT_IMPRESSIONS', 'Validité jusqu\'à <b>%s</b> impressions/visualisations');
define('TEXT_BANNERS_STATUS_CHANGE', 'Statut modifié: %s');

define('TEXT_BANNERS_DATA', 'D<br />A<br />T<br />E<br />N');
define('TEXT_BANNERS_LAST_3_DAYS', 'derniers 3 jours');
define('TEXT_BANNERS_BANNER_VIEWS', 'Visualisations de la bannière');
define('TEXT_BANNERS_BANNER_CLICKS', 'Clics sur la bannière');

define('TEXT_INFO_DELETE_INTRO', 'Vous en êtes sûr de vouloir supprimer la bannière?');
define('TEXT_INFO_DELETE_IMAGE', 'Supprimer l\'image de la bannière');

define('SUCCESS_BANNER_INSERTED', 'Succès: La bannière a été insérée.');
define('SUCCESS_BANNER_UPDATED', 'Succès: La bannière a été actualisée.');
define('SUCCESS_BANNER_REMOVED', 'Succès: La bannière a été supprimée.');
define('SUCCESS_BANNER_STATUS_UPDATED', 'Succès: Le statut de la bannière a été actualisé.');

define('ERROR_BANNER_TITLE_REQUIRED', 'Erreur: Un titre de bannière est nécessaire.');
define('ERROR_BANNER_GROUP_REQUIRED', 'Erreur: Un groupe de bannière est nécessaire.');
define('ERROR_BANNER_IMAGE_HTML_REQUIRED', 'Erreur: Une image de bannière ou un texte HTML est nécessaire.');
define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le répertoire cible %s n\'existe pas.' );
define('ERROR_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le répertoire cible %s n\'est pas enregistrable.');
define('ERROR_IMAGE_DOES_NOT_EXIST', 'Erreur: L\'image nexiste pas.');
define('ERROR_IMAGE_IS_NOT_WRITEABLE', 'Erreur: L\'image ne peut pas être supprimée.');
define('ERROR_UNKNOWN_STATUS_FLAG', 'Erreur: statut inconnu.');

define('ERROR_GRAPHS_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le répertoire \'graphs\' n\'existe pas! Veuillez créer un répertoire \'graphs\' dans le répertoire \'images\'.');
define('ERROR_GRAPHS_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le répertoire \'graphs\' n\'est pas verrouillé!');

// BOF - Tomcraft - 2009-11-06 - Use variable TEXT_BANNERS_DATE_FORMAT
define('TEXT_BANNERS_DATE_FORMAT', 'AAAA-MM-JJ');
// EOF - Tomcraft - 2009-11-06 - Use variable TEXT_BANNERS_DATE_FORMAT

define('TEXT_BANNERS_LANGUAGE', 'Langue:');
define('TEXT_BANNERS_LANGUAGE_NOTE', 'Choisissez la langue pour la bannière');
define('TEXT_NO_FILE', '-- aucun fichier--');
?>

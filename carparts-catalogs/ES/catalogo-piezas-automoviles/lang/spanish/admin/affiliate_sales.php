<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_sales.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_sales.php, v 1.5 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Ventas por el Programa de afiliados');
define('HEADING_TITLE_SEARCH', 'Número de pedido:');
define('HEADING_TITLE_STATUS', 'Estado:');

define('TABLE_HEADING_ONLINE', 'Clics');
define('TABLE_HEADING_AFFILIATE', 'Afiliado');
define('TABLE_HEADING_DATE', 'Fecha');
define('TABLE_HEADING_SALES', 'Importe de la comisión');
define('TABLE_HEADING_PERCENTAGE', 'Comisión');
define('TABLE_HEADING_STATUS', 'Estado');
define('TABLE_HEADING_ORDER_ID', 'Número de pedido');
define('TABLE_HEADING_VALUE', 'Valor del pedido');

define('TEXT_NO_SALES', 'No se registraron ventas.');
define('TEXT_DELETED_ORDER_BY_ADMIN', 'Borrado (admin)');
define('TEXT_DISPLAY_NUMBER_OF_SALES', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> ventas)');
define('TEXT_INFORMATION_SALES_TOTAL', 'Total de ventas de afiliado:');
define('TEXT_INFORMATION_AFFILIATE_TOTAL', 'Total de comisiones de afiliado:');
?>

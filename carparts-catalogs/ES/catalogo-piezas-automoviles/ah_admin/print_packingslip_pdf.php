<?php
/** 
 * -----------------------------------------------------------------------------------------
 * PDFBill NEXT by Robert Hoppe
 * Copyright 2011 Robert Hoppe - xtcm@katado.com - http://www.katado.com
 *
 * Please visit http://pdfnext.katado.com for newer Versions
 * -----------------------------------------------------------------------------------------
 *  
 * Released under the GNU General Public License 
 * 
 */
require('includes/application_top.php');
define('FPDF_FONTPATH', DIR_FS_CATALOG . DIR_WS_CLASSES . 'FPDF/font/');

// include needed classes
require_once(DIR_WS_CLASSES . 'order.php');
//require_once(DIR_FS_CATALOG . 'includes/external/phpmailer/class.phpmailer.php'); //commented out, not needed, noRiddle
require_once(DIR_FS_CATALOG . DIR_WS_CLASSES . 'FPDF/PdfRechnung.php');

// include needed functions
require_once(DIR_FS_INC . 'xtc_php_mail.inc.php');
require_once(DIR_FS_INC . 'xtc_pdf_bill.inc.php');

//require_once(DIR_FS_INC . 'xtc_get_order_data.inc.php'); //commented out, not needed, noRiddle
require_once(DIR_FS_INC . 'xtc_get_attributes_model.inc.php');
//require_once(DIR_FS_INC . 'xtc_not_null.inc.php');  //commented out, not needed, noRiddle
require_once(DIR_FS_INC . 'xtc_format_price_order.inc.php');
require_once(DIR_FS_INC . 'xtc_utf8_decode.inc.php');

// check for oID
if (!isset($_GET['oID'])) {
    die('Something went wrong! No oID was given!');
} else {
    $oID = xtc_db_input($_GET['oID']);
}

// Send PDF to customer if requested
if (isset($_GET['send'])) {
    // generate bill and send to customer
    xtc_pdf_bill($oID, true, true);

// without Mail - just generate
} else { 
    xtc_pdf_bill($oID, false, true);
}

// replace Variables for filePrefix
$sqlODetail = "
SELECT 
    customers_id,
    customers_cid,
    ibn_billnr,
    ibn_billdate
FROM  " . TABLE_ORDERS . "
WHERE orders_id = '" . $oID . "'
"; //get also ibn_billdate, 04-2021, noRiddle
$resODetail = xtc_db_query($sqlODetail);
$rowODetail = xtc_db_fetch_array($resODetail);

// use customers_id as the real id?
if (PDF_USE_CUSTOMER_ID == 'true') {
    $customers_id = $rowODetail['customers_id'];
} else {
    $customers_id = $rowODetail['customers_cid'];
}

// ibn_billnr if exists
$order_bill = $rowODetail['ibn_billnr'];

// create FilePrefix
$filePrefix = trim(PDF_FILENAME_SLIP); 
$filePrefix = str_replace('{oID}', $oID, $filePrefix);
$filePrefix = str_replace('{bill}', $order_bill, $filePrefix);
$filePrefix = str_replace('{cID}', $customers_id, $filePrefix);
$filePrefix = str_replace(' ', '_', $filePrefix);
if ($filePrefix == '') $filePrefix = $oID;
//BOC we will use a new file path (see /inc/xtc_pdf_bill.inc.php), 04-2021, noRiddle
//$pdf_bill = DIR_FS_ADMIN . 'invoice/' . $filePrefix . '.pdf';
$pdfbillnext_path_arr = explode('-', $rowODetail['ibn_billdate']);
if(file_exists(DIR_FS_ADMIN . 'invoice/' . $filePrefix . '.pdf')) {
    $pdf_bill = DIR_FS_ADMIN . 'invoice/' . $filePrefix . '.pdf';
} else {
    $pdf_bill = DIR_FS_ADMIN . 'invoice/' . $pdfbillnext_path_arr[0] .'/' . $pdfbillnext_path_arr[1] . '/' . $filePrefix . '.pdf';
}
//EOC we will use a new file path, 04-2021, noRiddle

// download redirect to pdf?
if (isset($_GET['download'])) {
  //xtc_redirect(xtc_href_link('invoice/' . $filePrefix . '.pdf', ''));
  header('Content-Description: File Transfer');
  header("Content-type: application/octet-stream");
  header("Content-disposition: attachment; filename=" . $filePrefix . ".pdf");
  header("Expires: 0");
  header('Cache-Control: must-revalidate');
  header("Pragma: no-cache");
  header("Content-Length: ".filesize($pdf_bill));
  readfile($pdf_bill);
  exit;
}

if (isset($_GET['show'])) {
  header("Content-Type: application/pdf");
  readfile($pdf_bill);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title><?php echo PDF_PRINT_PACKINGSLIP_PDF_TITLE; ?></title>
</head>
<body <?php if (isset($_GET['send'])) echo "onload='window.close();'"; ?>>
<h3><?php echo PDF_PRINT_PACKINGSLIP_PDF_TITLE; ?></h3>

<?php echo PDF_PRINT_PACKINGSLIP_SEND_TEXT; ?> <a href="<?php echo $_SERVER['PHP_SELF']; ?>?oID=<?php echo $_GET['oID']; ?>&send=1"><?php echo PDF_PRINT_PACKINGSLIP_SEND; ?></a>
<br/>
<br/>
<?php echo PDF_PRINT_PACKINGSLIP_DL_TEXT; ?> <a href="<?php echo $_SERVER['PHP_SELF']; ?>?oID=<?php echo $_GET['oID']; ?>&download=1"><?php echo PDF_PRINT_PACKINGSLIP_DL; ?></a>
<br/>
<br/>
<?php echo PDF_PRINT_PACKINGSLIP_SHOW_TEXT; ?> <a href="<?php echo $_SERVER['PHP_SELF']; ?>?oID=<?php echo $_GET['oID']; ?>&show=1"><?php echo PDF_PRINT_PACKINGSLIP_SHOW; ?></a>
<br/>
<br/>
<input type="button" value="<?php echo PDF_CLOSE_WINDOW; ?>" onclick="window.close()" />
</body>
</html>
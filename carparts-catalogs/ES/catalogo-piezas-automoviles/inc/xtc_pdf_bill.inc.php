<?php
/** 
 * -----------------------------------------------------------------------------------------
 * PDFBill NEXT by Robert Hoppe by Robert Hoppe
 * Copyright 2011 Robert Hoppe - xtcm@katado.com - http://www.katado.com
 *
 * Please visit http://pdfnext.katado.com for newer Versions
 * contribution: corrections and adaptations, (c) noRiddle 10-2017
 * -----------------------------------------------------------------------------------------
 *  
 * Released under the GNU General Public License 
 * 
 */
function xtc_pdf_bill ($oID, $send = false, $deliverySlip = false)
{
    // Create Order object from $oID
    $order = new order($oID);

    // Set language for bill/slip
    $language = $order->info['language'];
    if ($language == '') {
        $language = $_SESSION['language'];
    }

    // get language file
    //BOC implement custom made langauge constants from database (was implemented in shop version 1.06), noRiddle
    //require_once(DIR_FS_CATALOG .'lang/' . $language . '/modules/contribution/pdfbill.php');
    $languages_designer_query=xtc_db_query("SELECT lang_".$_SESSION['languages_id'].", lang_value FROM pdf_bill_languages");
	while ( $languages_designer = xtc_db_fetch_array($languages_designer_query)) {
		define($languages_designer['lang_value'], $languages_designer['lang_'.$_SESSION['languages_id']]);
	}
    //EOC implement custom made langauge constants from database (was implemented in shop version 1.06), noRiddle

    // Get Payment method | put this here from below
    if ($order->info['payment_method'] != '' && $order->info['payment_method'] != 'no_payment') {
        $paymentFile = DIR_FS_CATALOG . 'lang/' . $language . '/modules/payment/' . $order->info['payment_method'] . '.php';
        include($paymentFile);
        
        //BOC parameter vor payment method needed, noRiddle
        if($order->info['payment_method'] == 'paypalplus') {
            $ppp_special = false;
            require_once(DIR_FS_EXTERNAL.'paypal/classes/PayPalInfo.php');
            $paypal = new PayPalInfo($order->info['payment_method']);
            // payment
            $pp_info_array = $paypal->order_info($order->info['order_id']);
            if($pp_info_array['payment_method'] == 'pay_upon_invoice') {
                $pay_param = 'pui';
                $ppp_special = true;
            } else {
                $pay_param = 'some';
            }
        } else if($order->info['payment_method'] == 'paypalinstallment') {
            $pay_param = 'ppi';
        } else if($order->info['payment_method'] == 'paypalcart') { //added 09-12-2019 (use: see /includes/classes/FPDF/PdfBrief.php function Footer()), noRiddle
            $pay_param = 'ppc';
        } else if($order->info['payment_method'] == 'eustandardtransfer' || $order->info['payment_method'] == 'moneyorder') {
            $pay_param = 'giro';
        } else {
            $pay_param = 'some';
        }
        //EOC parameter vor payment method needed, noRiddle

        //$payment_method = constant(strtoupper('MODULE_PAYMENT_' . $order->info['payment_method'] . '_TEXT_TITLE'));
        $payment_method = constant(strtoupper('MODULE_PAYMENT_' . $order->info['payment_method'] . '_TEXT_TITLE')).($ppp_special === true ? ' (Invoice)' : ''); //added text for paypalplus invoice, noRiddle
		
    } else {
        $pay_param = '';
    }

    // Create PDF Object
    $pdf = new PdfRechnung($pay_param);
    $pdf->Init("Rechnung");

    // Get Customers ID
// START - Innergemeinschaftliche Lieferungen
/* ORIGINAL
    $sqlGetCustomer = "SELECT customers_id, customers_cid FROM " . TABLE_ORDERS . " WHERE orders_id = '" . (int)$oID . "'";
    $resGetCustomer = xtc_db_query($sqlGetCustomer);
    $rowGetCustomer = xtc_db_fetch_array($resGetCustomer);
*/
    $sqlGetCustomer = "SELECT customers_id, customers_cid, customers_status, customers_vat_id FROM " . TABLE_ORDERS . " WHERE orders_id = '" . (int)$oID . "'";
    $resGetCustomer = xtc_db_query($sqlGetCustomer);
    $rowGetCustomer = xtc_db_fetch_array($resGetCustomer);
	
	// set real customers_id
    $customers_id_real = $rowGetCustomer['customers_id'];
    $customers_status = $rowGetCustomer['customers_status'];
	
	// Kunden USTID
	$customers_vat_id = $rowGetCustomer['customers_vat_id'];
// END - Innergemeinschaftliche Lieferungen

    // use customers_id as the real id?
    if (PDF_USE_CUSTOMER_ID == 'true') {
        $customers_id = $rowGetCustomer['customers_id'];
    } else {
        $customers_id = $rowGetCustomer['customers_cid'];
    }

    // Get customer gender
    $sqlGetGender = "SELECT customers_gender FROM " . TABLE_CUSTOMERS . " WHERE customers_id = '" . (int)$rowGetCustomer['customers_id'] . "'";
    $resGetGender = xtc_db_query($sqlGetGender);
    $rowGetGender = xtc_db_fetch_array($resGetGender);
    $customer_gender = $rowGetGender['customers_gender'];

    // Change Address on Delivery Slip
    if ($deliverySlip === true) { 
        $customer_address = xtc_address_format($order->customer['format_id'], $order->delivery, 1, '', '<br>'); 
    } else { 
        $customer_address = xtc_address_format($order->customer['format_id'], $order->billing, 1, '', '<br>'); 
    }

    // PDF Address and Logo PDF-Output
    $pdf->Adresse(str_replace("<br>", "\n", $customer_address), xtc_utf8_decode(TEXT_PDF_SHOPADRESSEKLEIN));
    $pdf->Logo(DIR_FS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/img/logo_invoice.png');

    // Convert Datum into  tt.mm.jj umwandeln
    //preg_match("/(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2}):(\d{2})/", $order->info['date_purchased'], $dt);
    //$date_purchased = time(); // Current Date
    //$date_purchased = strtotime($order->info['date_purchased']);
    $date_purchased = isset($order->info['ibn_billdate']) && $order->info['ibn_billdate'] != '0000-00-00' ? xtc_date_short($order->info['ibn_billdate']) :  xtc_date_short($order->info['date_purchased']);

    // Get ibn_billnr, ibn_billdate and customers vat_id
    $sqlOrder = "
    SELECT 
        ibn_billnr,
		ibn_billdate,
        customers_vat_id 
    FROM " . TABLE_ORDERS . " 
    WHERE 
        orders_id = '" . $oID . "'";
    $resOrder = xtc_db_query($sqlOrder);
    $rowOrder = xtc_db_fetch_array($resOrder);
    $order_bill = $rowOrder['ibn_billnr'];
    //$order_billdate = $rowOrder['ibn_billdate'];
    $order_billdate = $date_purchased;
    $order_vat_id = $rowOrder['customers_vat_id'];
    

    // Create Bill Data
    if ($deliverySlip === true) {	
		  //$pdf->Rechnungsdaten($customers_id, $order_bill, $oID, date("d.m.Y"), $payment_method, $order_vat_id, $deliverySlip);
          $pdf->Rechnungsdaten($customers_id, $order_bill, $oID, date("d.m.Y"), $payment_method, $deliverySlip); //corrected one parameter too much, noRiddle
    } else { 
	    //$pdf->Rechnungsdaten($customers_id, $order_bill, $oID, xtc_date_short($order_billdate), $payment_method, $order_vat_id, $deliverySlip);
	    //$pdf->Rechnungsdaten($customers_id, $order_bill, $oID, $order_billdate, $payment_method, $order_vat_id, $deliverySlip);
        $pdf->Rechnungsdaten($customers_id, $order_bill, $oID, $order_billdate, $payment_method, $deliverySlip); //corrected one parameter too much, noRiddle
    }
    $pdf->RechnungStart($order->customer['lastname'], $customer_gender, $deliverySlip);

    // add BillPay Support
    if($order->info['payment_method'] == 'billpay' || $order->info['payment_method'] == 'billpaydebit') {
        // we need a workaround for billpay because its expecting $_GET['oid']
        if (!isset($_GET['oID']) || $_GET['oID'] != $oID) {
            // save for compatibility reasons oID
            if (isset($_GET['oID']) && is_numeric($_GET['oID'])) {
                $oldOID = $_GET['oID'];
            }

            // overwrite GET - shame on this
            $_GET['oID'] = $oID;
        }

        // get billpay stuff - uncomment the first line if you're having require-problems
        #require_once(DIR_FS_CATALOG . DIR_WS_INCLUDES . '/billpay/utils/billpay_display_pdf_data.php');
        require_once(DIR_FS_EXTERNAL . 'billpay/utils/billpay_display_pdf_data.php');

        // restore oID for compatibility reasons
        if (isset($oldOID)) {
            $_GET['oID'] = $oldOID;
            unset($oldOID);
        }
    }
    
    $pdf->ListeKopf($deliverySlip);

    // Product Informations
    $sqlProdInfos = "
    SELECT
        products_id,
        orders_products_id,
        products_model,
        products_name,
		products_order_description,
        products_price,
        final_price,
        products_quantity
    FROM " . TABLE_ORDERS_PRODUCTS."
    WHERE orders_id = '" . (int)$oID . "'";
    $resProdInfos = xtc_db_query($sqlProdInfos);

    // init order_data
    $order_data = array();

    // Add Products with attributes to PDF
    while ($order_data_values = xtc_db_fetch_array($resProdInfos)) {
        $sqlAttributes = "
        SELECT
            products_options,
            products_options_values,
            price_prefix,
            options_values_price
        FROM " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . "
        WHERE orders_products_id = '" . $order_data_values['orders_products_id'] . "'";
        $resAttributes = xtc_db_query($sqlAttributes);

        // init attribute strings
        $attributes_data = '';
        $attributes_model = '';

        // fetch attributes
        while ($attributes_data_values = xtc_db_fetch_array($resAttributes)) {
            $attributes_data .= $attributes_data_values['products_options'] . ': ' . $attributes_data_values['products_options_values'] . "\n";
            $attributes_model .= xtc_get_attributes_model (
                $order_data_values['products_id'], 
                $attributes_data_values['products_options_values'], 
                $attributes_data_values['products_options']
            )."\n";
        }

        // Deliverslip is without price
        if ($deliverySlip === true) { 
            $pdf->ListeProduktHinzu (
                $order_data_values['products_quantity'],
                $order_data_values['products_name'], 
                //trim(str_replace("■", "",(html_entity_decode(strip_tags($order_data_values['products_order_description']))))),
                trim(html_entity_decode($attributes_data)), 
                $order_data_values['products_model'], 
                trim(html_entity_decode($attributes_model)), 
                '', 
                ''
            ); 
        } else { 
            // get truncate length of product_model
            if (is_numeric(PDF_PRODUCT_MODEL_LENGTH) && PDF_PRODUCT_MODEL_LENGTH > 0) {
                $truncAfter = PDF_PRODUCT_MODEL_LENGTH;
            } else {
                $truncAfter = 7;
            }

            $pdf->ListeProduktHinzu(
                $order_data_values['products_quantity'], 
                $order_data_values['products_name'], 
                trim(html_entity_decode($attributes_data)), 
                // cut product_model to defined length
                substr($order_data_values['products_model'], 0, $truncAfter), 
                trim(html_entity_decode($attributes_model)), 
                xtc_utf8_decode(xtc_format_price_order($order_data_values['products_price'], 1, $order->info['currency'])), //added xtc_utf8_decode() (e.g. for pound sign), noRiddle
                xtc_utf8_decode(xtc_format_price_order($order_data_values['final_price'], 1, $order ->info['currency'])) //added xtc_utf8_decode() (e.g. for pound sign), noRiddle
            ); 
        }
    }

    // init order_data for order total
    $order_data = array();
    
    // dont show price on packaging slip
    if ($deliverySlip === false) {
        // Add Total to PDF
        $sqlOrderTotal = "
        SELECT
            title,
            text,
            class,
            value,
            sort_order
        FROM " . TABLE_ORDERS_TOTAL . "
        WHERE orders_id = '" . (int)$oID . "'
        ORDER BY sort_order ASC";
        $resOrderTotal = xtc_db_query($sqlOrderTotal);


        // fetch order data
        while ($oder_total_values = xtc_db_fetch_array($resOrderTotal)) {
            $order_data[] = array (
                'title' => xtc_utf8_decode(html_entity_decode($oder_total_values['title'])),
                'class'=> $oder_total_values['class'],
                'value'=> $oder_total_values['value'],
                'text' => xtc_utf8_decode($oder_total_values['text'])
            );
        }
    }

    // Generate PDF
    $pdf->Betrag($order_data);
    $pdf->RechnungEnde($deliverySlip);

// START - Innergemeinschaftliche Lieferungen
    //BOC EU TEXT by customers groups - www.rpa-com.de 
    if ($deliverySlip === false) {
      $eu_customer_groups_arr = array();
      if (defined('PDF_BILL_EU_CUSTOMERS_GROUP_ID')) {
        $eu_customer_groups_ids  = preg_replace("'[\r\n\s]+'",'',PDF_BILL_EU_CUSTOMERS_GROUP_ID);
        $eu_customer_groups_arr = explode(',',$eu_customer_groups_ids);
      }
      if (count($eu_customer_groups_arr) && in_array($customers_status,$eu_customer_groups_arr) && trim($customers_vat_id) != '') {
        $pdf->TextEU($customers_vat_id);
      }
    }
    //EOC EU TEXT  by customers groups - www.rpa-com.de 
// END - Innergemeinschaftliche Lieferungen	
	
    //BOC only for packing slips, noRiddle
    if($deliverySlip === true && PDF_COMMENT_ON_PACKING_SLIP == 'true') {
        $pdf->Kommentar(xtc_utf8_decode($order->info['comments']));
    }
    //EOC only for packing slips, noRiddle

    // Generate into given Directory
    if ($deliverySlip === false) {
        $filePrefix = PDF_FILENAME;
    } else {
        $filePrefix = PDF_FILENAME_SLIP;
    }

    // replace Variables for filePrefix
    $filePrefix = trim($filePrefix);
    $filePrefix = str_replace('{oID}', $oID, $filePrefix);
    $filePrefix = str_replace('{bill}', $order_bill, $filePrefix);
    $filePrefix = str_replace('{cID}', $customers_id, $filePrefix);
    $filePrefix = str_replace(' ', '_', $filePrefix);
    if ($filePrefix == '') $filePrefix = $oID;

    // Filename for BILL or SLIP
    //$filename = DIR_FS_DOCUMENT_ROOT.DIR_ADMIN  . 'invoice/' . $filePrefix . '.pdf';
    $filename = DIR_FS_DOCUMENT_ROOT.DIR_ADMIN  . 'invoice/' . date('Y') . '/' . date('m') . '/' . $filePrefix . '.pdf'; //new directory structure, 04-2021, noRiddle
    //BOC create dir structure first if needed, 04-2021, noRiddle
    $dedicated_dir_struct = dirname($filename);
    if(!is_dir($dedicated_dir_struct)) {
        mkdir($dedicated_dir_struct, 0755, true);
    }
    //EOC create dir structure first if needed, 04-2021, noRiddle
    $pdf->Output($filename , 'F');

    // Send PDF to Customer and maybe to copy-Address
    if ($send == true) {
        // attachment file
        $attachement_filename = $filename; 

        // mail name
        $name = $order->customer['firstname']." ".$order->customer['lastname'];

        // create new Smarty Object
        $smarty = new Smarty;

        // personalized mails - Only if supported
        if (defined('FEMALE')) {
            if ($customer_gender == 'f') { 
                $smarty->assign('GENDER', FEMALE); 
            } elseif ($customer_gender == 'm') { 
                $smarty->assign('GENDER', MALE); 
            } else { 
                $smarty->assign('GENDER', ''); 
            }

            $smarty->assign('LASTNAME', $order->customer['lastname']);
        }

        // assign language to template for caching
        $smarty->assign('language', $_SESSION['language']);
        $smarty->caching = false;

        // set dirs manual
        $smarty->template_dir = DIR_FS_CATALOG.'templates';
        $smarty->compile_dir = DIR_FS_CATALOG.'templates_c';
        $smarty->config_dir = DIR_FS_CATALOG.'lang';

        $smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');
        $smarty->assign('logo_path', HTTP_SERVER.DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/img/');

        // text assigns
        if ($deliverySlip === true) {
            $smarty->assign('PDF_TYPE', xtc_utf8_decode(TEXT_PDF_MAIL_LIEFERSCHEIN));
        } else {
            $smarty->assign('PDF_TYPE', xtc_utf8_decode(TEXT_PDF_MAIL_RECHNUNG));
        }
        
        $smarty->assign('ORDER_NO', $oID); //order number didn't have an assign for the mail templates, noRiddle

        // should we forward the packaging slip to a defined mail?
        //if(PDF_MAIL_SLIP_FORWARDER == 'true') {
        if(PDF_MAIL_SLIP_ONLY_LOGISTICIAN == 'true') {
            $mailTemplate = 'forwarder_mail';
        } else {
            $mailTemplate = 'invoice_mail';
        }

        $html_mail = $smarty->fetch(DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/admin/mail/' . $_SESSION['language'] . '/' . $mailTemplate . '.html');
        $txt_mail = $smarty->fetch(DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/admin/mail/' . $_SESSION['language'] . '/' . $mailTemplate . '.txt');

        // generate mail subject
        if ($deliverySlip === true) {
            $subject_text = PDF_MAIL_SUBJECT_SLIP;
        } else {
            $subject_text = PDF_MAIL_SUBJECT;
        }
        $mail_subject = str_replace('{oID}', $oID, $subject_text);

        //BOC send packing slip only to logistician ?, noRiddle
        if($deliverySlip === false) {
            // send customer mail with pdf bill
            xtc_php_mail(EMAIL_BILLING_ADDRESS, EMAIL_BILLING_NAME, $order->customer['email_address'], $name, '', EMAIL_BILLING_REPLY_ADDRESS, EMAIL_BILLING_REPLY_ADDRESS_NAME, $attachement_filename, '', $mail_subject, $html_mail, $txt_mail);
        } else {
            if(PDF_MAIL_SLIP_ONLY_LOGISTICIAN == 'true') {
                if(PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL != '') {
                    xtc_php_mail(EMAIL_BILLING_ADDRESS, EMAIL_BILLING_NAME, PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL, PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME, '', EMAIL_BILLING_REPLY_ADDRESS, EMAIL_BILLING_REPLY_ADDRESS_NAME, $attachement_filename, '', $mail_subject, $html_mail, $txt_mail);
                }
            } else {
                xtc_php_mail(EMAIL_BILLING_ADDRESS, EMAIL_BILLING_NAME, $order->customer['email_address'], $name, '', EMAIL_BILLING_REPLY_ADDRESS, EMAIL_BILLING_REPLY_ADDRESS_NAME, $attachement_filename, '', $mail_subject, $html_mail, $txt_mail);
            }
        }


        // send copy if needed
        if ($deliverySlip === true && PDF_MAIL_SLIP_COPY != '') {
            $copyMail = PDF_MAIL_SLIP_COPY;
        } else if ($deliverySlip === false && PDF_MAIL_BILL_COPY != '') {
            $copyMail = PDF_MAIL_BILL_COPY;
        }

        // copy mail needed?
        if (isset($copyMail) && $copyMail != '') {
            xtc_php_mail(EMAIL_BILLING_ADDRESS, EMAIL_BILLING_NAME, $copyMail, $name, '', EMAIL_BILLING_REPLY_ADDRESS, EMAIL_BILLING_REPLY_ADDRESS_NAME, $attachement_filename, '', $mail_subject, $html_mail, $txt_mail);
        }

        // Update Status to notified
        $customer_notified = '1';

        // switch status with deliverSlip
        if ($deliverySlip === true) {
            $comments = PDF_STATUS_COMMENT_SLIP;
        } else { 
            $comments = PDF_STATUS_COMMENT;
        }

        if (PDF_UPDATE_STATUS == 'true') { //the insert into orders_status_history must also depend on PDF_UPDATE_STATUS == 'true', noRiddle
            // switch Order Status id with deliverSlip
            if ($deliverySlip === true) {
                $orders_status_id = (is_numeric(PDF_STATUS_ID_SLIP))? PDF_STATUS_ID_SLIP : '1';
            } else {
                $orders_status_id = (is_numeric(PDF_STATUS_ID_BILL))? PDF_STATUS_ID_BILL : '1';
            }
            $orders_status_id = trim($orders_status_id);

            // insert notice
            $sqlStatus = "
            INSERT INTO " . TABLE_ORDERS_STATUS_HISTORY . " (
                orders_id, 
                orders_status_id, 
                date_added, 
                customer_notified, 
                comments
            ) VALUES (
                '" . xtc_db_input($oID) ."', 
                '" . $orders_status_id . "', 
                now(), 
                '" . $customer_notified . "',
                 '" . xtc_db_input($comments) . "'
            )";
            $resStatus = xtc_db_query($sqlStatus);

        // update orders_status on order
        //if (PDF_UPDATE_STATUS == 'true') { // ?? the insert into orders_status_history must also depend on PDF_UPDATE_STATUS == 'true', noRiddle
            $sqlUpdateStatus = "UPDATE " . TABLE_ORDERS . " SET orders_status = '" . $orders_status_id . "' WHERE orders_id = '" . xtc_db_input($oID) . "'";
            $resUpdateStatus = xtc_db_query($sqlUpdateStatus); 
        }
    }
}
?>
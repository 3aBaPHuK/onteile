<?php
/***************************************************
* file: ot_bulkgoods.php
* use: define bulk goods fees
* (c) noRiddle 05-2017
***************************************************/

class ot_bulkgoods {
  var $title, $output;

  function __construct() {
    global $xtPrice;
    
    $this->code = 'ot_bulkgoods';
    $this->title = MODULE_ORDER_TOTAL_BULKGOODS_TITLE;
    $this->description = MODULE_ORDER_TOTAL_BULKGOODS_DESCRIPTION;
    $this->enabled = ((MODULE_ORDER_TOTAL_BULKGOODS_STATUS == 'true') ? true : false);
    $this->sort_order = MODULE_ORDER_TOTAL_BULKGOODS_SORT_ORDER;
    $this->icon = '';
    $this->nr_get_all_custstatus = $this->nr_get_all_custstatus();
    $this->num_custstatus = defined('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT') ? MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT : '';
    
    if($this->check() > 0) {
        if(!defined('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT')) {
            xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, date_added) values ('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT', '".json_encode((object)$this->nr_get_all_custstatus)."', '6', '0', now())");
        }
    }

    $this->output = array();
  }
  
  //BOC helper class function
  function nr_get_all_custstatus() {
    $custstat_array = array();
    $custstat_qu = xtc_db_query("SELECT customers_status_id, customers_status_name FROM ".TABLE_CUSTOMERS_STATUS." WHERE language_id = '".(int)$_SESSION['languages_id']."' ORDER BY customers_status_id");
    while($cusstat_arr = xtc_db_fetch_array($custstat_qu)) {
        $custstat_array[$cusstat_arr['customers_status_id']] = $cusstat_arr['customers_status_name'];
    }
    
    return $custstat_array;
  }
  //EOC helper class function
  
  function process() {
    global $order, $xtPrice;
    //exclude selfpickup, free shipping and nox module, noRiddle
    $excl_shpp_arr = array('selfpickup_selfpickup', 'freeamount_freeamount', 'nox_nox', 'noxday_noxday');
    if (in_array($order->info['shipping_class'], $excl_shpp_arr)) return;

    $bulk_qty = 0;
    $bulk_costs = 0;
    $bulk_cust_status = $_SESSION['customers_status']['customers_status'];

    for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) {
      $t_query = xtc_db_query('SELECT products_bulk FROM products WHERE products_id = '.(int)$order->products[$i]['id']);
      $t = xtc_db_fetch_array($t_query);
      if ($t['products_bulk'] > 0) {
        $bulk_qty += $order->products[$i]['qty'];

        //BOC three option for calculating the bulk costs
        if (MODULE_ORDER_TOTAL_BULKGOODS_METHOD == '1') {
          $bulk_costs += ($order->products[$i]['qty'] * $t['products_bulk']);
          $bulk_costs = $bulk_costs * (float)constant('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$bulk_cust_status);
        } else if (MODULE_ORDER_TOTAL_BULKGOODS_METHOD == '2') {
          $bulk_costs += $t['products_bulk'];
          $bulk_costs = $bulk_costs * (float)constant('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$bulk_cust_status);
        } else if (MODULE_ORDER_TOTAL_BULKGOODS_METHOD == '3') {
          if ($t['products_bulk'] > $bulk_costs) {
            $bulk_costs = $t['products_bulk'];
            $bulk_costs = $bulk_costs * (float)constant('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$bulk_cust_status);
          } else {
            $bulk_costs = $bulk_costs;
            $bulk_costs = $bulk_costs * (float)constant('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$bulk_cust_status);
          }
        }
        //EOC three option for calculating the bulk costs

      }
    }

    if ($bulk_qty > 0) {
      $bulk_tax = xtc_get_tax_rate(MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS, $order->delivery['country']['id'], $order->delivery['zone_id']);
      $bulk_tax_description = xtc_get_tax_description(MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS, $order->delivery['country']['id'], $order->delivery['zone_id']);
      
      if ($_SESSION['customers_status']['customers_status_show_price_tax'] == 1) {
	
        //BOC Berechnung der Mehrwertsteuer auf die jeweiligen Zuschläge
        //$module = substr($_SESSION['shipping']['id'], 0, strpos($_SESSION['shipping']['id'], '_'));
        //$shipping_tax = xtc_get_tax_rate($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
        //$shipping_tax_description = xtc_get_tax_description($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
                
        /*$tax = $bulk_costs / (100 + $shipping_tax) * $shipping_tax; // Hier wird die Mehrwertsteuer berechnet
        $tax = $xtPrice->xtcFormat($tax,false,0,true);
        $order->info['tax'] += $tax;
        $order->info['tax_groups'][TAX_ADD_TAX . "$shipping_tax_description"] += $tax;*/
        
        $order->info['tax'] += xtc_calculate_tax($bulk_costs, $bulk_tax);
        $order->info['tax_groups'][TAX_ADD_TAX . "$bulk_tax_description"] += xtc_calculate_tax($bulk_costs, $bulk_tax);
        $order->info['total'] += $bulk_costs + xtc_calculate_tax($bulk_costs, $bulk_tax);
        $bulk_order_fee = xtc_add_tax($bulk_costs, $bulk_tax);
        $order->info['subtotal'] += $bulk_order_fee;
        //EOC Berechnung der Mehrwertsteuer auf die jeweiligen Zuschläge

      } else if ($_SESSION['customers_status']['customers_status_show_price_tax'] == 0 && $_SESSION['customers_status']['customers_status_add_tax_ot'] == 1) {
        /*$module = substr($_SESSION['shipping']['id'], 0, strpos($_SESSION['shipping']['id'], '_'));
        $shipping_tax = xtc_get_tax_rate($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
        $shipping_tax_description = xtc_get_tax_description($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
        $tax = $xtPrice->xtcFormat(xtc_add_tax($bulk_costs, $shipping_tax),false,0,false)-$bulk_costs;
        $tax = $xtPrice->xtcFormat($tax,false,0,true);

        $order->info['tax'] = $order->info['tax'] += $tax;
        $order->info['tax_groups'][TAX_NO_TAX . "$shipping_tax_description"] = $order->info['tax_groups'][TAX_NO_TAX . "$shipping_tax_description"] += $tax;*/
        
        $bulk_order_fee = $bulk_costs;
        $order->info['tax'] += xtc_calculate_tax($bulk_costs, $bulk_tax);
        $order->info['tax_groups'][TAX_NO_TAX . "$bulk_tax_description"] += xtc_calculate_tax($bulk_costs, $bulk_tax);
        $order->info['subtotal'] += $bulk_order_fee;
        $order->info['total'] += $bulk_order_fee;
      } else if ($_SESSION['customers_status']['customers_status_show_price_tax'] == 0 && $_SESSION['customers_status']['customers_status_add_tax_ot'] != 1) {
        $bulk_order_fee = $bulk_costs;
        $order->info['subtotal'] += $bulk_order_fee;
        $order->info['total'] += $bulk_order_fee;
      }

      //$order->info['total'] += $bulk_costs;
      
      $this->output[] = array('title' => MODULE_ORDER_TOTAL_BULKGOODS_CC_TITLE . ':',
                              //'text' => $xtPrice->xtcFormat($bulk_costs, true, 0, true),
                              //'value' => $xtPrice->xtcFormat($bulk_costs, false, 0, true));
                              'text' => $xtPrice->xtcFormat($bulk_order_fee, true),
                              'value' => $bulk_order_fee);
    }
  }
  
  function check() {
    if (!isset($this->_check)) {
      $check_query = xtc_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_ORDER_TOTAL_BULKGOODS_STATUS'");
      $this->_check = xtc_db_num_rows($check_query);
    }

    return $this->_check;
  }
  
  function install() {
    xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) values ('MODULE_ORDER_TOTAL_BULKGOODS_STATUS', 'true','6', '1', 'xtc_cfg_select_option(array(\'true\', \'false\'),', now())");
    xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, date_added) values ('MODULE_ORDER_TOTAL_BULKGOODS_SORT_ORDER', '36','6', '2', now())");
    xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, use_function, set_function, date_added) values ('MODULE_ORDER_TOTAL_BULKGOODS_METHOD', '1', '6', '3', 'xtc_get_bulkgoods_method', 'xtc_cfg_pull_down_bulkgoods_costs_method(', now())");
    xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, use_function, set_function, date_added) values ('MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS', '0','6', '4', 'xtc_get_tax_class_title', 'xtc_cfg_pull_down_tax_classes(', now())"); 
    xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) values ('MODULE_ORDER_TOTAL_BULKGOODS_SHOW_IN_CHECKOUT_SHIPPING', 'true','6', '5', 'xtc_cfg_select_option(array(\'true\', \'false\'),', now())");

    $c = 0;
    foreach($this->nr_get_all_custstatus as $k => $cs_name) {
        $c++;
        $srt = 5 + $c;
        xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, date_added) values ('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_".$k."', '', '6', '".$srt."', now())");
    }
  }
  
  function remove() {
    xtc_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key LIKE 'MODULE_ORDER_TOTAL_BULKGOODS_%'");
  }
  
  function keys() {
    $keys = array();
    $keys_query = xtc_db_query("SELECT configuration_key FROM " . TABLE_CONFIGURATION . " WHERE configuration_key LIKE 'MODULE_ORDER_TOTAL_BULKGOODS_%' ORDER BY sort_order");
    while($key = xtc_db_fetch_array($keys_query)) {
      $keys[] = $key['configuration_key'];
    }
    return $keys;
  }
}
?>
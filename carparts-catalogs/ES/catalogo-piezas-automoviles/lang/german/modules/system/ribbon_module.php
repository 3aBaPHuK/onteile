<?php
/*******************************************************
* file: ribbon_module.php
* use: language file for system module
* (c) noRiddle 07-2017
* Released under the GNU General Public License
*******************************************************/

define('MODULE_RIBBON_MODULE_TEXT_TITLE', 'Ribbon-Modul');
define('MODULE_RIBBON_MODULE_TEXT_DESCRIPTION', 'Stellt die Erweiterung zur Verfügung auf Sonderangeboten ein Band mit Prozentzeichen anzuzeigen.');
define('MODULE_RIBBON_MODULE_STATUS_TITLE', 'Modul aktivieren ?');
define('MODULE_RIBBON_MODULE_STATUS_DESC', 'Ribbon-Modul aktivieren');
?>
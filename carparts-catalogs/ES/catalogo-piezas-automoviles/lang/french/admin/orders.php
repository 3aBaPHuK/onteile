<?php
/* --------------------------------------------------------------
   $Id: orders.php 10896 2017-08-11 11:31:54Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce; www.oscommerce.com 
   (c) 2003      nextcommerce; www.nextcommerce.org
   (c) 2006      xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/
define('TEXT_BANK', 'Prélèvement automatique');
define('TEXT_BANK_OWNER', 'Propriétaire du compte:');
define('TEXT_BANK_NUMBER', 'Numéro du compte:');
define('TEXT_BANK_BLZ', 'Code bancaire:');
define('TEXT_BANK_NAME', 'Banque:');
define('TEXT_BANK_BIC', 'BIC:');
define('TEXT_BANK_IBAN', 'IBAN:');
define('TEXT_BANK_FAX', 'L\'autorisation de prélèvement sera confirmé par fax');
define('TEXT_BANK_STATUS', 'Statut de vérification:');
define('TEXT_BANK_PRZ', 'Méthode de vérification:');
define('TEXT_BANK_OWNER_EMAIL', 'Adresse email du propriétaire du compte:');

define('TEXT_BANK_ERROR_1', 'Le numéro de compte ne correspond pas au code bancaire!');
define('TEXT_BANK_ERROR_2', 'Aucune méthode de vérification n\'est définie pour ce numéro de compte!');
define('TEXT_BANK_ERROR_3', 'Le numéro de compte ne peut pas être vérifié! La méthode de vérification n\'est pas mise en place.');
define('TEXT_BANK_ERROR_4', 'Le numéro de compte ne peut pas être vérifié techniquement!');
define('TEXT_BANK_ERROR_5', 'Le code bancaire n\'a pas été trouvé!');
define('TEXT_BANK_ERROR_8', 'Le code bancaire n\'a pas été indiqué!');
define('TEXT_BANK_ERROR_9', 'Le numéro de compte n\'a pas été indiqué!');
define('TEXT_BANK_ERRORCODE', 'Code d\'erreur:');

define('HEADING_TITLE', 'Commandes');
define('HEADING_TITLE_SEARCH', 'N° de commande:');
define('HEADING_TITLE_STATUS', 'Statut:');

define('TABLE_HEADING_COMMENTS', 'Commentaire');
define('TABLE_HEADING_CUSTOMERS', 'Clients');
define('TABLE_HEADING_ORDER_TOTAL', 'Valeur totale');
define('TABLE_HEADING_DATE_PURCHASED', 'Date de commande');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_QUANTITY', 'Quantité');
define('TABLE_HEADING_PRODUCTS_MODEL', 'N° d\'article');
define('TABLE_HEADING_PRODUCTS', 'Articles');
define('TABLE_HEADING_TAX', 'TVA');
define('TABLE_HEADING_TOTAL', 'Total');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Prix (hors TVA)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Prix (TVA incluse)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total (hors TVA)');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total');
define('TABLE_HEADING_AFTERBUY', 'Afterbuy');
define('TABLE_HEADING_CUSTOMER_NOTIFIED', 'Client notifié');
define('TABLE_HEADING_DATE_ADDED', 'ajouté le:');

define('ENTRY_CUSTOMER', 'Client:');
define('ENTRY_SOLD_TO', 'Adresse de facturation:');
define('ENTRY_STREET_ADDRESS', 'Rue:');
define('ENTRY_SUBURB', 'Adresse complémentaire:');
define('ENTRY_CITY', 'Ville:');
define('ENTRY_POST_CODE', 'Code postal:');
define('ENTRY_STATE', 'Département:');
define('ENTRY_COUNTRY', 'Pays:');
define('ENTRY_TELEPHONE', 'Téléphone:');
define('ENTRY_EMAIL_ADDRESS', 'Adresse email:');
define('ENTRY_DELIVERY_TO', 'Adresse de livraison:');
define('ENTRY_SHIP_TO', 'Adresse de livraison:');
define('ENTRY_SHIPPING_ADDRESS', 'Adresse de livraison:');
define('ENTRY_BILLING_ADDRESS', 'Adresse de facturation:');
define('ENTRY_PAYMENT_METHOD', 'Mode de paiement:');
define('ENTRY_SHIPPING_METHOD', 'Mode de livraison:');
define('ENTRY_CREDIT_CARD_TYPE', 'Type de carte de crédit:');
define('ENTRY_CREDIT_CARD_OWNER', 'Propriétaire de carte de crédit:');
define('ENTRY_CREDIT_CARD_NUMBER', 'Numéro de carte de crédit:');
define('ENTRY_CREDIT_CARD_CVV', 'Code de sécurité (CVV):');
define('ENTRY_CREDIT_CARD_EXPIRES', 'Date d\'expiration:');
define('ENTRY_SUB_TOTAL', 'Sous-total:');
define('ENTRY_TAX', 'TVA:');
define('ENTRY_SHIPPING', 'Frais de port:');
define('ENTRY_TOTAL', 'Total:');
define('ENTRY_DATE_PURCHASED', 'Date de commande:');
define('ENTRY_STATUS', 'Statut:');
define('ENTRY_DATE_LAST_UPDATED', 'dernière actualisation:');
define('ENTRY_NOTIFY_CUSTOMER', 'Notifier le client:');
define('ENTRY_NOTIFY_COMMENTS', 'Joindre les commentaires:');
define('ENTRY_PRINTABLE', 'Imprimer la facture');

define('TEXT_INFO_HEADING_DELETE_ORDER', 'Supprimer la commande');
define('TEXT_INFO_DELETE_INTRO', 'Êtes-vous sûr de vouloir supprimer la commande?');
define('TEXT_INFO_RESTOCK_PRODUCT_QUANTITY', 'Réapprovisionner le nombre d\'articles au stock');
define('TEXT_DATE_ORDER_CREATED', 'créé le:');
define('TEXT_DATE_ORDER_LAST_MODIFIED', 'Dernière modification:');
define('TEXT_INFO_PAYMENT_METHOD', 'Mode de paiement:');
define('TEXT_INFO_SHIPPING_METHOD', 'Mode de livraison:');

define('TEXT_ALL_ORDERS', 'Toutes les commandes');
define('TEXT_NO_ORDER_HISTORY', 'Pas d\'historique de commandes disponible');

define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_SUBJECT', 'Changement de statut de votre commande');
define('EMAIL_TEXT_ORDER_NUMBER', 'N° de commande:');
define('EMAIL_TEXT_INVOICE_URL', 'Vous pouvez visualiser votre commande sous l\'adresse suivante:');
define('EMAIL_TEXT_DATE_ORDERED', 'Date de commande:');
define('EMAIL_TEXT_STATUS_UPDATE', 'Le statut de votre commande a été actualisé.' . "\n\n" . 'Nouveau statut: %s' . "\n\n" . 'Si vous avez des questions concernant votre commande, veuillez répondre à ce mail.' . "\n\n" . 'Cordialement' . "\n");
define('EMAIL_TEXT_COMMENTS_UPDATE', 'Commentaires concernant votre commande:' . "\n\n%s\n\n");

define('ERROR_ORDER_DOES_NOT_EXIST', 'Erreur: Cette commande n\'existe pas!.');
define('SUCCESS_ORDER_UPDATED', 'Succès: Votre commande a été actualisé avec succès.');
define('WARNING_ORDER_NOT_UPDATED', 'Remarque: Rien n\'a été changé. Votre commande n\'a donc pas été actualisé.');

define('TABLE_HEADING_DISCOUNT', 'Réduction');
define('ENTRY_CUSTOMERS_GROUP', 'Groupe de clients:');
define('ENTRY_CUSTOMERS_VAT_ID', 'Numéro TVA:');
define('TEXT_VALIDATING', 'Pas confirmé');

// BOF - Tomcraft - 2009-10-03 - Paypal Express Modul
define('TEXT_INFO_PAYPAL_DELETE', 'Supprimer les données de transaction PayPal également.');
// EOF - Tomcraft - 2009-10-03 - Paypal Express Modul

// BOF - Tomcraft - 2010-04-22 - Added a missing language definition
define('TEXT_PRODUCTS', 'Articles');
// EOF - Tomcraft - 2010-04-22 - Added a missing language definition

//BOF - web28 - 2010-03-20 - Send Order by Admin
define('COMMENT_SEND_ORDER_BY_ADMIN' , 'Confirmation de commande envoyée');
define('BUTTON_ORDER_CONFIRMATION', 'Envoyer la confirmation de commande');
define('SUCCESS_ORDER_SEND', 'Confirmation de commande envoyée');
//EOF - web28 - 2010-03-20 - Send Order by Admin

// web28 2010-12-07 add new defines
define('ENTRY_CUSTOMERS_ADDRESS', 'Adresse du client:');
define('TEXT_ORDER', 'Commande:');
define('TEXT_ORDER_HISTORY', 'Historique de commandes:');
define('TEXT_ORDER_STATUS', 'Statut de commande:');

define('TABLE_HEADING_ORDERS_ID', 'N° de commande');
define('TEXT_SHIPPING_TO', 'Envoi à');

define('TABLE_HEADING_COMMENTS_SENT', 'Commentaire envoyé');

define('TABLE_HEADING_TRACK_TRACE', 'Track & Trace');
define('TABLE_HEADING_CARRIER', 'Mode de livraison');
define('TABLE_HEADING_PARCEL_LINK', 'Numéro de suivi / Numéro d\'étiquette code barres / Numéro de commande / ID de l\'envoi / Numéro de colis');

define('TEXT_INFO_HEADING_REVERSE_ORDER', 'Annuler la commande');
define('TEXT_INFO_REVERSE_INTRO', 'Êtes-vous sûr de vouloir annuler la commande?');

define('TABLE_HEADING_SHIPCLOUD', 'Shipcloud:');
define('TABLE_HEADING_PARCEL_ID', 'Numéro d\'étiquette code barres');
define('TEXT_SHIPCLOUD_STANDARD', 'Standard');
define('TEXT_SHIPCLOUD_ONE_DAY', 'Express');
define('TEXT_SHIPCLOUD_ONE_DAY_EARLY', 'Express 10:00');
define('TEXT_SHIPCLOUD_RETURNS', 'Retour');
define('TEXT_SHIPCLOUD_LETTER', 'Lettre poste');
define('TEXT_SHIPCLOUD_BOOKS', 'Livres postaux');
define('TEXT_SHIPCLOUD_PARCEL_LETTER', 'Envoi de marchandises');
define('TEXT_WEIGHT_PLACEHOLDER', 'Poids / Kg');
define('TEXT_SHIPCLOUD_INSURANCE_NO', 'Augmentation d\'assurance NON');
define('TEXT_SHIPCLOUD_INSURANCE_YES', 'Augmentation d\'assurance OUI');
define('TEXT_SHIPCLOUD_BULK', 'Articles encombrants');
define('TEXT_SHIPCLOUD_PARCEL', 'Colis');

define('DOWNLOAD_LABEL', 'Télécharger l\'étiquette codes barres');
define('CREATE_LABEL', 'Créer l\'étiquette codes barres');
define('TEXT_DELETE_SHIPMENT_SUCCESS', 'Etiquette codes barres shipcloud supprimée.');
define('TEXT_LABEL_CREATED', 'Etiquette codes barres créé.');
define('TEXT_CARRIER_ERROR', 'Le prestataire d\'envoi n\'est pas activé dans votre compte shipcloud ou la clé API n\'est pas valide.');
define('TEXT_CARRIER_PLACEHOLDER_1', 'Déscription du colis');
define('TEXT_CARRIER_PLACEHOLDER_2', 'Déscription d\'expédition');

define('TEXT_DOWNLOADS', 'Téléchargements');
define('TABLE_HEADING_FILENAME', 'Nom du fichier');
define('TABLE_HEADING_EXPIRES', 'Date d\'expiration');
define('TABLE_HEADING_DOWNLOADS', 'Nombre de téléchargements');
define('TABLE_HEADING_DAYS', 'Nombre de jours');

define('ENTRY_SEND_TRACKING_INFO', 'Informations de livraison:');

define('TEXT_ORDERS_STATUS_FILTER', 'Filtre de statut de commande');

define('TABLE_HEADING_DATE', 'Date');

define('BUTTON_ORDER_MAIL_STEP', 'Envoyer la confirmation de commande');
define('COMMENT_SEND_ORDER_MAIL_STEP' , 'Confirmation de commande envoyée');
define('SUCCESS_ORDER_MAIL_STEP_SEND', 'Confirmation de commande envoyée avec succès');
?>

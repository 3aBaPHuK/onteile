<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_sales.php 40 2013-01-08 16:36:44Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_sales.php, v 1.16 2003/09/22);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

require('includes/application_top.php');

if (!isset($_SESSION['affiliate_id'])) {
    xtc_redirect(xtc_href_link(FILENAME_AFFILIATE, '', 'SSL'));
}

// create smarty elements
$smarty = new Smarty;

// include needed functions
require_once(DIR_FS_INC . 'affiliate_period.inc.php');
require_once(DIR_FS_INC . 'affiliate_get_status_list.inc.php');
require_once(DIR_FS_INC . 'affiliate_get_status_array.inc.php');
require_once(DIR_FS_INC . 'affiliate_get_level_list.inc.php');
require_once(DIR_FS_INC . 'xtc_date_short.inc.php');

$breadcrumb->add(NAVBAR_TITLE, xtc_href_link(FILENAME_AFFILIATE, '', 'SSL'));
$breadcrumb->add(NAVBAR_TITLE_SALES, xtc_href_link(FILENAME_AFFILIATE_SALES, '', 'SSL'));

if (!isset($_GET['page'])) $_GET['page'] = 1;

if (xtc_not_null($_GET['a_period'])) {
    $period_split = explode('-', xtc_db_prepare_input( $_GET['a_period'] ) );
    $period_clause = " AND year(a.affiliate_date) = " . $period_split[0] . " and month(a.affiliate_date) = " . $period_split[1];
}
if (xtc_not_null($_GET['a_status'])) {
    $a_status = xtc_db_prepare_input( $_GET['a_status'] );
    $status_clause = " AND o.orders_status = '" . $a_status . "'";
}
if ( is_numeric( $_GET['a_level'] )  ) {
      $a_level = xtc_db_prepare_input( $_GET['a_level'] );
      $level_clause = " AND a.affiliate_level = '" . $a_level . "'";
}
$affiliate_sales_raw = "select a.affiliate_payment, a.affiliate_date, a.affiliate_value, a.affiliate_percent,
    a.affiliate_payment, a.affiliate_level AS level,
    o.orders_status as orders_status_id, os.orders_status_name as orders_status,
    MONTH(aa.affiliate_date_account_created) as start_month, YEAR(aa.affiliate_date_account_created) as start_year
    from " . TABLE_AFFILIATE . " aa
    left join " . TABLE_AFFILIATE_SALES . " a on (aa.affiliate_id = a.affiliate_id )
    left join " . TABLE_ORDERS . " o on (a.affiliate_orders_id = o.orders_id)
    left join " . TABLE_ORDERS_STATUS . " os on (o.orders_status = os.orders_status_id and language_id = '" . $_SESSION['languages_id'] . "')
    where a.affiliate_id = '" . $_SESSION['affiliate_id'] . "' " .
    $period_clause . $status_clause . $level_clause . "
    group by aa.affiliate_date_account_created, o.orders_status, os.orders_status_name,
        a.affiliate_payment, a.affiliate_date, a.affiliate_value, a.affiliate_percent,
        o.orders_status, os.orders_status_name
    order by affiliate_date DESC";

$count_key = 'aa.affiliate_date_account_created, o.orders_status, os.orders_status_name, a.affiliate_payment, a.affiliate_date, a.affiliate_value, a.affiliate_percent, o.orders_status, os.orders_status_name';

$affiliate_sales_split = new splitPageResults($affiliate_sales_raw, $_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $count_key);

$check_sql = xtc_db_query("SELECT affiliate_tiers_allowed FROM " . TABLE_AFFILIATE . " WHERE affiliate_id = '" . $_SESSION['affiliate_id'] . "'");
$check = xtc_db_fetch_array($check_sql);
if ( AFFILIATE_USE_TIER == 'true' && $check['affiliate_tiers_allowed'] == 1) {
	$smarty->assign('AFFILIATE_USE_TIER', 'true');
	$smarty->assign('level_selector', affiliate_get_level_list('a_level', xtc_db_prepare_input($_GET['a_level']), 'onChange="this.form.submit();"'));
}

require(DIR_FS_CATALOG .'templates/'.CURRENT_TEMPLATE. '/source/boxes.php');
require(DIR_WS_INCLUDES . 'header.php');

$smarty->assign('affiliate_sales_split_numbers', $affiliate_sales_split->number_of_rows);
$smarty->assign('FORM_ACTION', xtc_draw_form('params', xtc_href_link(FILENAME_AFFILIATE_SALES ), 'get', 'SSL' ));

$period_start_year = '';
$period_start_month = '';

if ($affiliate_sales_split->number_of_rows > 0) {
	$affiliate_sales_values = xtc_db_query($affiliate_sales_split->sql_query);
	
    $sum_of_earnings = 0;
    $as_array = array();

    while($affiliate_sales = xtc_db_fetch_array($affiliate_sales_values)) {
    	
    	if($period_start_month == '') {
    		$period_start_month = $affiliate_sales['start_month'];
    	}
    	
    	if($period_start_year == '') {
    		$period_start_year = $affiliate_sales['start_year'];
    	}
    	
    	$as_array[] = array('affiliate_date' => xtc_date_short($affiliate_sales['affiliate_date']),
    						'affiliate_value' => $xtPrice->xtcFormat($affiliate_sales['affiliate_value'], true),
    						'affiliate_percent' => $affiliate_sales['affiliate_percent'] . " %",
    						'affiliate_level' => (($affiliate_sales['level'] > 0) ? $affiliate_sales['level'] : TEXT_AFFILIATE_PERSONAL_LEVEL_SHORT),
    						'affiliate_payment' => $xtPrice->xtcFormat($affiliate_sales['affiliate_payment'], true),
    						'affiliate_orders_status' => (($affiliate_sales['orders_status'] != '')?$affiliate_sales['orders_status']:TEXT_DELETED_ORDER_BY_ADMIN));
    	
    	if (in_array($affiliate_sales['orders_status_id'], explode(';', AFFILIATE_PAYMENT_ORDER_MIN_STATUS))) $sum_of_earnings += $affiliate_sales['affiliate_payment'];    	
	}
	$smarty->assign('as_array', $as_array);
} else {
	$affiliate_sales_values = xtc_db_query( "select MONTH(affiliate_date_account_created) as start_month,
                                      YEAR(affiliate_date_account_created) as start_year
                                      FROM " . TABLE_AFFILIATE . " WHERE affiliate_id = '" . $_SESSION['affiliate_id'] . "'" );
    $affiliate_sales = xtc_db_fetch_array( $affiliate_sales_values );
    if($period_start_month == '') {
		$period_start_month = $affiliate_sales['start_month'];
	}
	
	if($period_start_year == '') {
		$period_start_year = $affiliate_sales['start_year'];
	}
}

$smarty->assign('period_selector', affiliate_period('a_period', $period_start_year, $period_start_month, true, xtc_db_prepare_input($_GET['a_period'] ), 'onChange="this.form.submit();"' ));
$smarty->assign('status_selector', affiliate_get_status_list('a_status', xtc_db_prepare_input($_GET['a_status']), 'onChange="this.form.submit();"' ));

if ($affiliate_sales_split->number_of_rows > 0) {
	$smarty->assign('affiliate_sales_count', $affiliate_sales_split->display_count(TEXT_DISPLAY_NUMBER_OF_SALES));
	$smarty->assign('affiliate_sales_links', $affiliate_sales_split->display_links(MAX_DISPLAY_PAGE_LINKS, xtc_get_all_get_params(array('page', 'info', 'x', 'y'))));
}

$smarty->assign('affiliate_sales_total', $xtPrice->xtcFormat($sum_of_earnings,true));
$smarty->assign('language', $_SESSION['language']);
$smarty->caching = 0;
$main_content = $smarty->fetch(CURRENT_TEMPLATE . '/module/affiliate_sales.html');
$smarty->assign('main_content',$main_content);

if (!defined(RM))
	$smarty->load_filter('output', 'note');
	
$smarty->display(CURRENT_TEMPLATE . '/index.html');

include ('includes/application_bottom.php');
?>
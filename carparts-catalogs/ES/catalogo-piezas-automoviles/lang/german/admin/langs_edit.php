<?php
/***********************************************************************************************************
* file: langs_edit.php
* use: language file for /Admin/langs_edit.php
*
* (c) noRiddle 06-2018
***********************************************************************************************************/

define('NR_LANGS_EDIT_GUIDE_HEADING', 'Anleitung');
define('NR_LANGS_EDIT_GUIDE_TXT', '<ul>
                                    <li><u>Vorbereitung:</u>
                                        <ul>
                                            <li>Basis-Sprache wählen und in Datenbank laden.<br />
                                                !! Diese Basis-Sprache sollte nicht mehr geändert werden wenn man bereits Sprachen editiert und gespeichert hat. Die Zuordnung der Zeilen ist ansonsten nicht möglich !!</li>
                                            <li>Hilfs-Sprachen wählen und in Datenbank laden.<br />
                                                Jede Sprache die editiert werden soll muß als Verzeichnis in <i>/lang/helpfiles/</i> mit wenigstens einer Datei vorhanden sein. Wenn keine Vorlagen existieren einfach eine Datei einer anderen Sprache dort reinladen.</li>
                                        </ul>
                                    </li>
                                    <li><span class="red"><u>Unbedingt beachten !!</u></span>
                                        <ul>
                                            <li>Einfache Anführungszeichen sind unbedingt zu erhalten.<br />
                                            Wenn sie Teil des Textes sind (z.B. in Engl. "don\'t") müssen sie escape-t werden, was bedeutet, es muß ihnen ein Backslash vorangestellt werden.<br />
                                            Das sieht so aus: "don\\\'t" oder "d\\\'accord" .</li>
                                            <li>HTML und andere Sonderzeichen die zu PHP gehören müssen ebenfalls unbedingt erhalten werden.<br />
                                            Mit ein wenig Erfahrung sieht man was Text ist und übersetzt werden muß und welche Teile zu Programm-Code gehören (HTML, Javascript oder PHP).</li>
                                        </ul>
                                    </li>
                                    <li><u>Speichern:</u>
                                        <ul>
                                            <li>Wenn Sprachen zum Editieren geöffnet sind alle 10 Minuten automatisch gespeichert um Ablauf der Session und Logout zu verhindern.</li>
                                            <li>Beim Speichern editierter Sprachen werden diese erstmal in der Datenbank gespeichert.<br />
                                            Erst wenn "Sprachdateien für gewähltes File generieren" geklickt wurde werden die zugehörigen Dateien generiert und in <i>/lang/translated/</i> in einem Ordner der wie die Sprache lautet gespeichert.</li>
                                        </ul>
                                    </li>
                                  </ul>'
);

define('NR_LANGS_EDIT_LOAD_LANGS_HEADING', 'Sprachen in DB laden');
define('NR_LANGS_EDIT_LOAD_LANGS_FILETREE_HEADING', 'Datei-Baum');
define('NR_LANGS_EDIT_LOAD_BASIS_LANG', 'Gewünschte Basis-Sprache in DB laden:');
define('NR_LANGS_EDIT_WARNING_LOAD_BASIS_JS', 'Die Basis-Sprache wirklich neu laden ?');
define('NR_LANGS_EDIT_LOAD_HELPER_LANG', 'Zu editierende Sprachen in DB laden:');
define('NR_LANGS_EDIT_WARN_NO_BASIS_LANG', 'Es muß erst eine Basis-Sprache geladen werden !');
define('NR_LANGS_EDIT_WARN_HELP_DIR_NOT_EXISTS', 'Das Verzeichnis <i>/lang/helpfiles/</i> existiert nicht.<br />Wenn Sprachdateien-Vorlagen existieren diese bitte in <i>/lang/helpfiles/SPRACHE/</i> laden.<br />Für jede Sprache SPRACHE mit der kleingeschriebenen engl. Bezeichnung der Sprache ersetzen. (Beispiel: /lang/helpfiles/italian/ für Italienisch)');
define('NR_LANGS_EDIT_LOAD_EXIST_LANG', 'Bereits vorhandene Sprachen in DB laden:');
define('NR_LANGS_EDIT_LOADED_TXT', 'Geladen:');
define('NR_LANGS_EDIT_HELPINGLANG_TXT', 'Hilfssprache');
define('NR_LANGS_EDIT_EXISTINGLANG_TXT', 'existiert bereits live');

define('NR_LANGS_EDIT_ATTENTION_READ_TXT', 'ACHTUNG, bitte lesen !! ');
define('NR_LANGS_EDIT_EXPL_LOAD_BASIC_TXT', '!! Nicht ändern wenn bereits Sprachen bearbeitet wurden. !!<br />
    Die gewählte Sprache wird komplett neu eingelesen.<br />
    Um weitere Sprachen richtig zuzuordnen muß (müssen) nach erneutem Laden der Basis-Sprache die zu bearbeitende(n) Sprache(n) ebenfalls erneut geladen werden.<br />
    !! Bereits gespeicherte Daten gehen dann verloren und können nicht zugeordnet werden !!');
define('NR_LANGS_EDIT_EXPL_LOAD_HELPER_TXT', 'Im Verzeichnis <i>/lang/helpfiles/</i> liegen Verzeichnisse für weitere Sprachen die als Sprachpakete z.B. von modified runtergeladen wurden.<br />
    Diese können als Vorlage beim Übersetzen dienen.<br />
    Beim Laden in die DB wird programmatisch jeder Wert dem korrespondierenden Wert der links geladenen Basis-Sprache zugeordnet.');
define('NR_LANGS_EDIT_EXPL_LOAD_EXIST_TXT', 'Die gewählte Sprache ist eine Sprache deren Dateien bereits im Live-Verzeichnis existieren und wird komplett neu eingelesen.<br />
    Es können mehrere Sprachen hintereinander eingelesen werden. Sie können als Übersetzungevorbild dienen.<br />
    !! Bitte beachten, daß eine Sprache die mehrfach geladen wird die vorher geladene überschreibt !!');
define('NR_LANGS_EDIT_EXPL_SHOW_LANGS_TXT', 'Die oben als Basis-Sprache geladene Sprache wird als Vorbild immer automatisch mit angezeigt.<br />
    Bereits als live existierende Sprachen werden, wenn markiert, lediglich als Vorbild mitgeladen und sind nicht editierbar.<br />
    !! Wenn bereits eigene Übersetzungen gespeichert wurden werden automatisch diese geladen !!');
define('NR_LANGS_EDIT_EXPL_GENERATE_FILES', 'Nachdem die Übersetzungen gespeichert wurden sollte man die Dateien der editierten Sprachen für das gewählte File generieren.');
define('NR_LANGS_EDIT_FILL_EMPTY_WITH_ENGL', 'Leere Werte mit engl. Werten füllen ?');

define('NR_LANGS_EDIT_LOAD_IN_DB_TXT', 'in DB laden');
define('NR_LANGS_EDIT_SHOW_MARKED_LANGS_TO_EDIT', 'Markierte Sprachen für gewählte Datei zum editieren anzeigen');
define('NR_LANGS_EDIT_SAVE_TRANSL_TO_DB', 'Änderungen in DB speichern');
define('NR_LANGS_EDIT_GENERATE_FILES', 'Sprach-Dateien für gewähltes File generieren');

//messagestack
define('NR_LANGS_EDIT_NOLANG_CHOOSEN_OR_NO_DIR', 'Keine Sprache ausgewählt oder Sprache nicht als Directory vorhanden!');
define('NR_LANGS_EDIT_CONSTS_LOADED', 'Definierte Konstanten aus %s für %s wurden eingelesen');
define('NR_LANGS_EDIT_CONFS_LOADED', 'Definierte Smarty-Conf Variablen aus %s für %s wurden eingelesen');
define('NR_LANGS_EDIT_UNALLOWED_LANGS_MSG', '!! Unerlaubte Sprache(n) können nicht angezeigt werden !!');
define('NR_LANGS_EDIT_GENERATED_FILES_SUCCESS', '%s in %s wurde in <i>/%s</i> geschrieben.');
define('NR_LANGS_EDIT_GENERATED_FILES_ERROR', '%s in %s konnte nicht in <i>/%s</i> geschrieben werden.<br />Es sind für die Datei keine Daten in der Datenbank vorhanden.<br />Es müssen erst Daten für die Datei in der Datenbank gespeichert werden.');
?>
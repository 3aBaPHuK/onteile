<?php
/* *******************************************************************************
* ah_module_export.php, noRiddle 07-2017
**********************************************************************************/

define('HEADING_TITLE_MODULES_AH_EXPORT', 'Kunden- /Bestellungen-Export');

define('TABLE_HEADING_AH_MODULES', 'Modul');
define('TABLE_HEADING_AH_SORT_ORDER', 'Reihenfolge');
define('TABLE_HEADING_AH_STATUS', 'Status');
define('TABLE_HEADING_AH_ACTION', 'Aktion');

define('TEXT_MODULE_AH_DIRECTORY', 'Modul Verzeichnis:');
define('TABLE_HEADING_AH_FILENAME', 'Modulname (f&uuml;r internen Gebrauch)');
define('ERROR_AH_EXPORT_FOLDER_NOT_WRITEABLE', '%s Verzeichniss nicht beschreibbar!');
define('TEXT_AH_MODULE_INFO', 'Bitte &uuml;berpr&uuml;fen Sie die Module beim jeweiligen Anbieter auf die aktuellste Version!');

define('TABLE_HEADING_AH_MODULES_INSTALLED', 'Folgende Module wurden installiert');
define('TABLE_HEADING_AH_MODULES_NOT_INSTALLED', 'Folgende Module sind noch verf&uuml;gbar');
?>
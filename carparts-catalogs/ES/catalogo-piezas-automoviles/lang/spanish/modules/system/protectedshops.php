<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PROTECTEDSHOPS_TEXT_TITLE', 'Actualización automática de tiendas protegidas');
define('MODULE_PROTECTEDSHOPS_TEXT_DESCRIPTION', 'Tiendas protegidas - Actualización automática para textos jurídicos automáticos<br/><br/><b>IMPORTANTE:</b> antes de utilizar el módulo, se debe introducir y guardar un token válido antes de que se pueda realizar la asignación de las páginas de contenido.<hr noshade>');
define('MODULE_PROTECTEDSHOPS_STATUS_TITLE', 'Estado');
define('MODULE_PROTECTEDSHOPS_STATUS_DESC', 'Estado del módulo');
define('MODULE_PROTECTEDSHOPS_TOKEN_TITLE', 'Token de autentificación');
define('MODULE_PROTECTEDSHOPS_TOKEN_DESC', 'El token de autentificación lo encuentra en el menú del proveedor de tiendas protegidas.');
define('MODULE_PROTECTEDSHOPS_TYPE_TITLE', '<hr noshade>Guardar como');
define('MODULE_PROTECTEDSHOPS_TYPE_DESC', '¿Deben guardarse los datos en un archivo o en la base de datos?');
define('MODULE_PROTECTEDSHOPS_FORMAT_TITLE', 'Tipo de formato');
define('MODULE_PROTECTEDSHOPS_FORMAT_DESC', '¿Deben guardarse los datos como texto, HTML o HtmlLite (sólo salto de línea, negrita, subrayado y cursiva)?');
define('MODULE_PROTECTEDSHOPS_AUTOUPDATE_TITLE', '<hr noshade>Actualización automática');
define('MODULE_PROTECTEDSHOPS_AUTOUPDATE_DESC', '¿Deben actualizarse automáticamente los datos?');
define('MODULE_PROTECTEDSHOPS_UPDATE_INTERVAL_TITLE', 'Intervalo de actualización');
define('MODULE_PROTECTEDSHOPS_UPDATE_INTERVAL_DESC', '¿En qué intervalos deben actualizarse los datos?');
define('MODULE_PROTECTEDSHOPS_ACTION_TITLE', '<hr noshade>Acción');
define('MODULE_PROTECTEDSHOPS_ACTION_DESC', '¿Se deben guardar las configuraciones o actualizar manualmente los documentos?<br/><br/><b>Importante::</b> si se han cambiado las configuraciones, éstas deben guardarse primero antes de que se puedan crear los documentos con estas configuraciones.<br/>');
define('TEXT_SAVE', 'Guardar las configuraciones');
define('TEXT_PROCESS', 'Actualizar documentos');

?>

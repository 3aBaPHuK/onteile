<?php
/************************************************************************
* CoKeBuMo imageslider plugin                                           *
*                                                                       *
* Imageslider for modified eCommerce shopsoftware 1.06 rev 4356         *
* plugin derived from imageslider by Hetfield                           *
* based on                                                              *
* - responsive kenburner slider by codecanyon, licence necessary        *
*                                                                       *
* copyright (c) for all changes and adaptations:                        *
* noRiddle of webdesign-endres.de 2013                                  *
************************************************************************/

require('includes/application_top.php');
require_once(DIR_FS_INC . 'xtc_wysiwyg.inc.php');

//BOC configuration
$base_path = $path; // coming from ah2.0/config.php //'/usr/www/users/onteile/carparts-catalogs.com';
$copy_shop = 'DE/oem-kataloge';
//EOC configuration

function xtc_get_imageslider_image($imageslider_id, $language_id) {
    $imageslider_query = xtc_db_query("select imagesliders_image from ".TABLE_IMAGESLIDERS_INFO." where imagesliders_id = '".$imageslider_id."' and languages_id = '".$language_id."'");
    $imageslider = xtc_db_fetch_array($imageslider_query);	
    return $imageslider['imagesliders_image'];
}
function xtc_get_imageslider_url($imageslider_id, $language_id) {
    $imageslider_query = xtc_db_query("select imagesliders_url from ".TABLE_IMAGESLIDERS_INFO." where imagesliders_id = '".$imageslider_id."' and languages_id = '".$language_id."'");
    $imageslider = xtc_db_fetch_array($imageslider_query);	
    return $imageslider['imagesliders_url'];
}
function xtc_get_imageslider_url_target($imageslider_id, $language_id) {
    $imageslider_query = xtc_db_query("select imagesliders_url_target from ".TABLE_IMAGESLIDERS_INFO." where imagesliders_id = '".$imageslider_id."' and languages_id = '".$language_id."'");
    $imageslider = xtc_db_fetch_array($imageslider_query);	
    return $imageslider['imagesliders_url_target'];
}
function xtc_get_imageslider_url_typ($imageslider_id, $language_id) {
    $imageslider_query = xtc_db_query("select imagesliders_url_typ from ".TABLE_IMAGESLIDERS_INFO." where imagesliders_id = '".$imageslider_id."' and languages_id = '".$language_id."'");
    $imageslider = xtc_db_fetch_array($imageslider_query);	
    return $imageslider['imagesliders_url_typ'];
}
function xtc_get_imageslider_title($imageslider_id, $language_id) {
    $imageslider_query = xtc_db_query("select imagesliders_title from ".TABLE_IMAGESLIDERS_INFO." where imagesliders_id = '".$imageslider_id."' and languages_id = '".$language_id."'");
    $imageslider = xtc_db_fetch_array($imageslider_query);	
    return $imageslider['imagesliders_title'];
}
// BOC new for image-alt-tag, noRiddle
function xtc_get_imageslider_alt($imageslider_id, $language_id) {
    $imageslider_query = xtc_db_query("select imagesliders_alt from ".TABLE_IMAGESLIDERS_INFO." where imagesliders_id = '".$imageslider_id."' and languages_id = '".$language_id."'");
    $imageslider = xtc_db_fetch_array($imageslider_query);	
    return $imageslider['imagesliders_alt'];
}
// EOC new for image-alt-tag, noRiddle
function xtc_get_imageslider_description($imageslider_id, $language_id) {
    $imageslider_query = xtc_db_query("select imagesliders_description from ".TABLE_IMAGESLIDERS_INFO." where imagesliders_id = '".$imageslider_id."' and languages_id = '".$language_id."'");
    $imageslider = xtc_db_fetch_array($imageslider_query);	
    return $imageslider['imagesliders_description'];
}
// BOC new for data-attributes
function xtc_get_imageslider_data_attributes($imageslider_id, $language_id, $data) {
    $imageslider_query = xtc_db_query("SELECT 
                                        imagesliders_data_kenburn,
                                        imagesliders_data_transition,
                                        imagesliders_data_startalign,
                                        imagesliders_data_endalign,
                                        imagesliders_data_zoom,
                                        imagesliders_data_zoomfact,
                                        imagesliders_data_panduration
                                  FROM ".TABLE_IMAGESLIDERS_INFO." 
                                 WHERE imagesliders_id = '".$imageslider_id."' 
                                   AND languages_id = '".$language_id."'
                                    ");
    $imageslider = xtc_db_fetch_array($imageslider_query);	
    return $imageslider[$data];
}
// EOC new for data-attributes
    
// BOC new for Caption direction and effect
function xtc_get_imageslider_caption_attributes($imageslider_id, $language_id, $data) {
    $imageslider_query = xtc_db_query("SELECT
                                        imagesliders_desc_direct,
                                        imagesliders_desc_effect
                                  FROM ".TABLE_IMAGESLIDERS_INFO." 
                                 WHERE imagesliders_id = '".$imageslider_id."' 
                                   AND languages_id = '".$language_id."'
                                    ");
    $imageslider = xtc_db_fetch_array($imageslider_query);	
    return $imageslider[$data];
}
// EOC new for caption direction and effect

// BOC new for video URL and video description, noRiddle
function xtc_get_imagesliders_video_data($imageslider_id, $language_id, $data) {
    $imageslider_query = xtc_db_query("SELECT
                                        imagesliders_video_url,
                                        imagesliders_video_expl
                                  FROM ".TABLE_IMAGESLIDERS_INFO." 
                                 WHERE imagesliders_id = '".$imageslider_id."' 
                                   AND languages_id = '".$language_id."'
                                    ");
    $imageslider = xtc_db_fetch_array($imageslider_query);	
    return $imageslider[$data];
}
// EOC new for video URL and video description, noRiddle

//BOC new for dropdown of image names to be able to copy a whole slide
function xtc_get_imagenames_for_dropdown() {
    $img_names_dropdown_arr = array();
    $img_names_dropdown_arr[] = array('id' => 'def', 'text' => TXT_DEFAULT_COPY_IMAGE);
    $img_names_query = xtc_db_query("SELECT imagesliders_id, imagesliders_name FROM ".TABLE_IMAGESLIDERS." ORDER BY sorting ASC");
    while($img_names_arr = xtc_db_fetch_array($img_names_query)) {
        $img_names_dropdown_arr[] = array('id' => $img_names_arr['imagesliders_id'], 'text' => $img_names_arr['imagesliders_name']);
    }
    return $img_names_dropdown_arr;
}
//EOC new for dropdown of image names to be able to copy a whole slide

//BOC ajax change of speed on overview page
if(isset($_POST['new_speed']) && isset($_POST['imgsl_id']) && $_POST['new_speed'] != ''){
    $post_newspeed = xtc_db_input((float)$_POST['new_speed']);
    $post_id = xtc_db_input((int)$_POST['imgsl_id']);
    xtc_db_query("UPDATE ".TABLE_IMAGESLIDERS_INFO." SET imagesliders_data_panduration = ".$post_newspeed." WHERE imagesliders_id = ".$post_id);
    echo number_format($post_newspeed, 1);
    exit();
}
//EOC ajax change of speed on overview page

//BOC ajax change of sorting on overview page
if(isset($_POST['new_sort']) && isset($_POST['imgslider_id']) && $_POST['new_sort'] != ''){
    $post_newsort = xtc_db_input((int)$_POST['new_sort']);
    $post_imgid = xtc_db_input((int)$_POST['imgslider_id']);
    xtc_db_query("UPDATE ".TABLE_IMAGESLIDERS." SET sorting = ".$post_newsort." WHERE imagesliders_id = ".$post_imgid);
    echo $post_newsort;
    exit();
}
//EOC ajax change of sorting on overview page

	
switch ($_GET['action']) {
    case 'insert':
    case 'save':
        $imagesliders_id = xtc_db_prepare_input($_GET['iID']);
        $imagesliders_name = xtc_db_prepare_input($_POST['imagesliders_name']);	  
        $imagesliders_status = xtc_db_prepare_input($_POST['imagesliders_status']);
        $imagesliders_sorting = xtc_db_prepare_input($_POST['imagesliders_sorting']);

        $sql_data_array = array('imagesliders_name' => $imagesliders_name,
                                'status' => $imagesliders_status,
                                'sorting' => $imagesliders_sorting
        );

        if ($_GET['action'] == 'insert') {
            $insert_sql_data = array('date_added' => 'now()');
            $sql_data_array = xtc_array_merge($sql_data_array, $insert_sql_data);
            xtc_db_perform(TABLE_IMAGESLIDERS, $sql_data_array);
            $imagesliders_id = xtc_db_insert_id();
        } elseif ($_GET['action'] == 'save') {
            $update_sql_data = array('last_modified' => 'now()');
            $sql_data_array = xtc_array_merge($sql_data_array, $update_sql_data);
            xtc_db_perform(TABLE_IMAGESLIDERS, $sql_data_array, 'update', "imagesliders_id = '" . xtc_db_input($imagesliders_id) . "'");
        }    

        $languages = xtc_get_languages();
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            if ($_POST['imagesliders_image_delete'. $i] == true) {
                @unlink(DIR_FS_CATALOG_IMAGES.xtc_get_imageslider_image($imagesliders_id, $languages[$i]['id']));
                $imagepfad = '';
            }
            if ($image = xtc_try_upload('imagesliders_image'.$i, DIR_FS_CATALOG_IMAGES.'imagesliders/'.$languages[$i]['directory'].'/')) {
                $imagepfad = 'imagesliders/'.$languages[$i]['directory'].'/'.$image->filename;
            } else {
                if ($_POST['imagesliders_image_delete'. $i] == false) {
                    $imagepfad = xtc_get_imageslider_image($imagesliders_id, $languages[$i]['id']);
                } 
            }
            
            $imagesliders_url_array = $_POST['imagesliders_url'];
            $imagesliders_url_target_array = $_POST['imagesliders_url_target'];
            $imagesliders_url_typ_array = $_POST['imagesliders_url_typ'];
            $imagesliders_alt_array = $_POST['imagesliders_alt']; //added for image-alt-tag, noRiddle
            $imagesliders_description_array = $_POST['imagesliders_description'];
            
            $imagesliders_data_kenburn_array = $_POST['imagesliders_data_kenburn'];
            $imagesliders_data_transition_array = $_POST['imagesliders_data_transition'];
            $imagesliders_data_startalign_array = $_POST['imagesliders_data_startalign'];
            $imagesliders_data_endalign_array = $_POST['imagesliders_data_endalign'];
            $imagesliders_data_zoom_array = $_POST['imagesliders_data_zoom'];
            $imagesliders_data_zoomfact_array = $_POST['imagesliders_data_zoomfact'];
            $imagesliders_data_panduration_array = $_POST['imagesliders_data_panduration'];
            $imagesliders_desc_direct_array = $_POST['imagesliders_desc_direct'];
            $imagesliders_desc_effect_array = $_POST['imagesliders_desc_effect'];
            $imagesliders_video_url_array = $_POST['imagesliders_video_url'];
            $imagesliders_video_expl_array = $_POST['imagesliders_video_expl'];
             
            $language_id = $languages[$i]['id'];

            $sql_data_array = array('imagesliders_url' => xtc_db_prepare_input($imagesliders_url_array[$language_id]),
                                    'imagesliders_url_target' => xtc_db_prepare_input($imagesliders_url_target_array[$language_id]),
                                    'imagesliders_url_typ' => xtc_db_prepare_input($imagesliders_url_typ_array[$language_id]),
                                    'imagesliders_image' => $imagepfad,
                                    'imagesliders_alt' => xtc_db_prepare_input($imagesliders_alt_array[$language_id]), //added for image-alt-tag, noRiddle
                                    'imagesliders_description' => xtc_db_prepare_input($imagesliders_description_array[$language_id])
                                    );
                                    if($imagesliders_data_kenburn_array[$language_id] != '') {
                                        $sql_data_array = array_merge($sql_data_array, array('imagesliders_data_kenburn' => xtc_db_prepare_input($imagesliders_data_kenburn_array[$language_id]))); //data-attributes, noRiddle
                                    }
                                    if($imagesliders_data_transition_array[$language_id] != '') {
                                        $sql_data_array = array_merge($sql_data_array, array('imagesliders_data_transition' => xtc_db_prepare_input($imagesliders_data_transition_array[$language_id]))); //data-attributes, noRiddle
                                    }
             $sql_data_array += array('imagesliders_data_startalign' => xtc_db_prepare_input($imagesliders_data_startalign_array[$language_id]), //data-attributes, noRiddle
                                      'imagesliders_data_endalign' => xtc_db_prepare_input($imagesliders_data_endalign_array[$language_id]), //data-attributes, noRiddle
                                      'imagesliders_data_zoom' => xtc_db_prepare_input($imagesliders_data_zoom_array[$language_id]), //data-attributes, noRiddle
                                      'imagesliders_data_zoomfact' => xtc_db_prepare_input($imagesliders_data_zoomfact_array[$language_id]), //data-attributes, noRiddle
                                      'imagesliders_data_panduration' => xtc_db_prepare_input($imagesliders_data_panduration_array[$language_id]) //data-attributes, noRiddle
                                     );
                                    if($imagesliders_desc_direct_array[$language_id] != '') {
                                        $sql_data_array = array_merge($sql_data_array, array('imagesliders_desc_direct' => xtc_db_prepare_input($imagesliders_desc_direct_array[$language_id]))); //caption direction, noRiddle
                                    }
                                    if($imagesliders_desc_effect_array[$language_id] != '') {
                                        $sql_data_array = array_merge($sql_data_array, array('imagesliders_desc_effect' => xtc_db_prepare_input($imagesliders_desc_effect_array[$language_id]))); //caption effect, noRiddle
                                    }
             $sql_data_array += array('imagesliders_video_url' => xtc_db_prepare_input($imagesliders_video_url_array[$language_id]), // added for video url, noRiddle
                                      'imagesliders_video_expl' => xtc_db_prepare_input($imagesliders_video_expl_array[$language_id]) // added for video caption, noRiddle
                                     );

            if ($_GET['action'] == 'insert') {
                $insert_sql_data = array('imagesliders_id' => $imagesliders_id,
                                        'languages_id' => $language_id);
                $sql_data_array = xtc_array_merge($sql_data_array, $insert_sql_data);
                xtc_db_perform(TABLE_IMAGESLIDERS_INFO, $sql_data_array);
            } elseif ($_GET['action'] == 'save') {
                $has_lang_qu = xtc_db_query("SELECT languages_id FROM ".TABLE_IMAGESLIDERS_INFO." WHERE imagesliders_id = ".(int)$imagesliders_id." AND languages_id = ".(int)$language_id);
                if(xtc_db_num_rows($has_lang_qu)) {
                    xtc_db_perform(TABLE_IMAGESLIDERS_INFO, $sql_data_array, 'update', "imagesliders_id = '" . xtc_db_input($imagesliders_id) . "' and languages_id = '" . (int)$language_id . "'");
                } else {
                    $insert_sql_data = array('imagesliders_id' => $imagesliders_id,
                                             'languages_id' => $language_id);
                    $sql_data_array = xtc_array_merge($insert_sql_data, $sql_data_array);
                    xtc_db_perform(TABLE_IMAGESLIDERS_INFO, $sql_data_array);
                }
            }
        }

        xtc_redirect(xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $imagesliders_id));
    break;

    case 'deleteconfirm':
        $imagesliders_id = xtc_db_prepare_input($_GET['iID']);

        if ($_POST['delete_image'] == 'on') {        
            $languages = xtc_get_languages();
            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                $image_location = DIR_FS_CATALOG_IMAGES.xtc_get_imageslider_image($imagesliders_id, $languages[$i]['id']);
                if (file_exists($image_location)) {
                    @unlink($image_location);
                }
            }
        }

        xtc_db_query("delete from " . TABLE_IMAGESLIDERS . " where imagesliders_id = '" . xtc_db_input($imagesliders_id) . "'");
        xtc_db_query("delete from " . TABLE_IMAGESLIDERS_INFO . " where imagesliders_id = '" . xtc_db_input($imagesliders_id) . "'");

        xtc_redirect(xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page']));
    break;
	  
    case 'setflag':
        $imagesliders_id = xtc_db_prepare_input((int)$_GET['iID']);
        $imagesliders_status = xtc_db_prepare_input((int)$_GET['flag']);
        xtc_db_query("UPDATE " . TABLE_IMAGESLIDERS . " SET status = ".xtc_db_input($imagesliders_status)." WHERE imagesliders_id = '" . xtc_db_input($imagesliders_id) . "'");
    break;
    
    //BOC new case copy, noRiddle
    case 'copy':
        if(isset($_POST['copy_image']) && is_numeric($_POST['copy_image'])) {
            $languages = xtc_get_languages();
            $post_copy_img = (int)$_POST['copy_image'];
            $is_str = '';
            $is_sel_str = '';
            $isi_str = '';
            $isi_sel_str = '';
            
            $show_cols_is_qu = xtc_db_query("SHOW COLUMNS FROM ".TABLE_IMAGESLIDERS." WHERE FIELD != 'imagesliders_id' AND FIELD != 'last_modified'");
            $show_cols_isi_qu = xtc_db_query("SHOW COLUMNS FROM ".TABLE_IMAGESLIDERS_INFO." WHERE FIELD != 'url_clicked' AND FIELD != 'date_last_click'");
            while($show_cols_is = xtc_db_fetch_array($show_cols_is_qu)) {
                $is_str .= $show_cols_is['Field'].',';
                $is_sel_str .= ($show_cols_is['Field'] == 'imagesliders_name' ? "CONCAT(".$show_cols_is['Field'].", '(copy)')" : ($show_cols_is['Field'] == 'date_added' ? "NOW()" : $show_cols_is['Field'])).",";
            }
            $is_str = rtrim($is_str, ',');
            $is_sel_str = rtrim($is_sel_str, ',');
            
            $copy_is_sql = "INSERT INTO ".TABLE_IMAGESLIDERS." (".$is_str.") (SELECT ".$is_sel_str." FROM ".TABLE_IMAGESLIDERS." WHERE imagesliders_id = ".xtc_db_input($post_copy_img).")";
            xtc_db_query($copy_is_sql);
            //echo '<pre>'.$copy_is_sql.'</pre>';
            $new_id = xtc_db_insert_id();
            
            $isi_ins_str .= 'imagesliders_id,';
            while($show_cols_isi = xtc_db_fetch_array($show_cols_isi_qu)) {
                $isi_str .= $show_cols_isi['Field'].',';
                $isi_sel_str .= ($show_cols_isi['Field'] == 'imagesliders_id' ? $new_id : $show_cols_isi['Field']).",";
            }
            $isi_str = rtrim($isi_str, ',');
            $isi_sel_str = rtrim($isi_sel_str, ',');
            
            for ($l = 0, $sl = sizeof($languages); $l < $sl; $l++) {
                $copy_isi_sql = "INSERT INTO ".TABLE_IMAGESLIDERS_INFO." (".$isi_str.") (SELECT ".$isi_sel_str." FROM ".TABLE_IMAGESLIDERS_INFO." WHERE imagesliders_id = ".xtc_db_input($post_copy_img)." AND languages_id = ".$languages[$l]['id'].")";
                xtc_db_query($copy_isi_sql);
                //echo '<pre>'.$copy_isi_sql.'</pre>';
            }
        } else {
            $copy_warning = 'KEIN BILD ZUM KOPIEREN AUSGEWAEHLT';
        }
        xtc_redirect(xtc_href_link(FILENAME_IMAGESLIDERS, 'page='.$_GET['page'].(isset($copy_warning) ? '&copy_warning='.$copy_warning : '')));
    break;
    //EOC new case copy, noRiddle
    
    //BOC new case copy slider, noRiddle
    case 'copy_slider':
        if($shop != $copy_shop) {
            if(isset($_POST['slider_from_shop']) && $_POST['slider_from_shop'] == $copy_shop) {
                //BOC paths for file copying
                //$base_path = '/usr/www/users/onteile'; set this in config on top of this file
                $from_shop = $base_path.'/'.$copy_shop.'/images/';
                $to_shop = $base_path.'/'.$shop.'/images/';
                //EOC paths for file copying
                
                //BOC input field for id range
                $post_which_ids_copy = $_POST['which_ids_copy'] != '' ? $_POST['which_ids_copy'] : '';
                $post_which_ids_copy_arr = $post_which_ids_copy != '' ? explode('-', $post_which_ids_copy) : '';
                //EOC input field for id range
            
                //BOC connect to other db
                $conn_array = array('srv' => $dataC20[$copy_shop]['sql_server'],
                                    'db'  => $dataC20[$copy_shop]['sql_db'],
                                    'usr' => $dataC20[$copy_shop]['sql_user'],
                                    'pw'  => $dataC20[$copy_shop]['sql_pass']
                                   );
                       
                //$ext_dblink = mysql_connect($conn_array['srv'], $conn_array['usr'], $conn_array['pw']);
                $ext_dblink = mysqli_connect($conn_array['srv'], $conn_array['usr'], $conn_array['pw'], $conn_array['db']);
                if (!$ext_dblink) die('Connection to external database failed: ' . mysqli_error());

                //if (!mysql_select_db($conn_array['db'], $ext_dblink)) die('Couldn\'t select database');
                if (!mysqli_select_db($ext_dblink, $conn_array['db'])) die('Couldn\'t select database');
                mysqli_set_charset($ext_dblink, 'utf8'); //set charset
                //EOC connect to other db
            
                //$img_slider_query = mysql_query("SELECT i.*, ii.* FROM imagesliders i LEFT JOIN imagesliders_info ii ON ii.imagesliders_id = i.imagesliders_id WHERE i.status = '0'", $ext_dblink);
                if(is_array($post_which_ids_copy_arr)) {
                    if(count($post_which_ids_copy_arr) == 2) {
                        $between_str = ' AND imagesliders_id BETWEEN '.$post_which_ids_copy_arr[0].' AND '.$post_which_ids_copy_arr[1];
                    } elseif(count($post_which_ids_copy_arr) == 1) {
                        $between_str = ' AND imagesliders_id = '.(int)$post_which_ids_copy_arr[0];
                    } else {
                        $between_str = '';
                    }
                } else {
                    $between_str = '';
                }
                
                //$img_slider_query = mysql_query("SELECT * FROM imagesliders WHERE status = '0'".$between_str, $ext_dblink);
                $img_slider_query = mysqli_query($ext_dblink, "SELECT * FROM imagesliders WHERE status = '0'".$between_str);
                                   
                //while($img_slider_arr = mysql_fetch_assoc($img_slider_query)) {
                while($img_slider_arr = mysqli_fetch_array($img_slider_query, MYSQLI_ASSOC)) {
                    //$image_slider_info_query = mysql_query("SELECT * FROM imagesliders_info WHERE imagesliders_id = ".$img_slider_arr['imagesliders_id'], $ext_dblink);
                    $image_slider_info_query = mysqli_query($ext_dblink, "SELECT * FROM imagesliders_info WHERE imagesliders_id = ".$img_slider_arr['imagesliders_id']);
                    
                    unset($img_slider_arr['imagesliders_id']);
                    unset($img_slider_arr['last_modified']);
                    $img_slider_arr['date_added'] = 'now()';
                    
                    xtc_db_perform(TABLE_IMAGESLIDERS, $img_slider_arr);
                    //echo '<pre>'.print_r($img_slider_arr, true).'</pre>';
                    
                    $new_id_qu = xtc_db_query("SELECT MAX(imagesliders_id) AS max FROM ".TABLE_IMAGESLIDERS);
                    $new_id_arr = xtc_db_fetch_array($new_id_qu);
                    $new_id = $new_id_arr['max'];
                    //echo '<pre>'.$new_id.'</pre>';
                    
                    //while($img_slider_info_arr = mysql_fetch_assoc($image_slider_info_query)) {
                    while($img_slider_info_arr = mysqli_fetch_array($image_slider_info_query, MYSQLI_ASSOC)) {
                        $img_slider_info_arr['imagesliders_id'] = $new_id;
                        
                        xtc_db_perform(TABLE_IMAGESLIDERS_INFO, $img_slider_info_arr);
                        //echo '<pre>'.print_r($img_slider_info_arr, true).'</pre>';
                        
                        //BOC copy files
                        copy($from_shop.$img_slider_info_arr['imagesliders_image'], $to_shop.$img_slider_info_arr['imagesliders_image']);
                        
                        if(strpos($img_slider_info_arr['imagesliders_description'], 'src="') !== false) {
                            $img_desc = $img_slider_info_arr['imagesliders_description'];
                            $begin_img_str = strpos($img_desc, 'src="');
                            $end_img_str = (strpos($img_slider_info_arr['imagesliders_description'], '.png"') !== false) ? strpos($img_desc, '.png"') : strpos($img_desc, '.jpg"'); //support png and jpg
                            $pic_path = substr($img_desc, ($begin_img_str + 5), (($end_img_str - $begin_img_str) - 1));
                            $strlen_copy_shop = (strlen($copy_shop) +1);
                            $pic_path_new = substr($pic_path, $strlen_copy_shop, (strlen($pic_path) - $strlen_copy_shop));
                            //echo '<pre>'.$pic_path.'</pre>';
                            //echo '<pre>'.$pic_path_new.'</pre>';
                            //echo '<pre>'.$begin_img_str.' | '.$end_img_str.'</pre>';
                            
                            copy($base_path.$pic_path, $base_path.'/'.$shop.$pic_path_new);
                            //echo '<pre>'.$base_path.'/'.$shop.$pic_path_new.'</pre>';
                        }
                        //EOC copy files
                    }
                }
                //mysql_close($ext_dblink);
                mysqli_close($ext_dblink);
            }
            
            xtc_redirect(xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page']));
        }
    break;
    //EOC new case copy slider, noRiddle
}
require (DIR_WS_INCLUDES.'head.php');
?>
<?php
if (USE_WYSIWYG == 'true') {
	$query = xtc_db_query("SELECT code FROM ".TABLE_LANGUAGES." WHERE languages_id='".$_SESSION['languages_id']."'");
	$data = xtc_db_fetch_array($query);

	$languages = xtc_get_languages();
    
    if ($_GET['action'] == 'new' || $_GET['action'] == 'edit') {
        for ($i = 0; $i < sizeof($languages); $i ++) {
            echo xtc_wysiwyg('imagesliders_description', $data['code'], $languages[$i]['id']);
        }
        for ($i = 0; $i < sizeof($languages); $i ++) {
            echo xtc_wysiwyg('imagesliders_video_expl', $data['code'], $languages[$i]['id']);
        }
    }
}
?>
<style>
/*#imgslider p {margin:8px 0;}*/
#imgslider .sml-red {font-size:10px; color:#c00;}
#imgslider .imgslider-link {color:#07A6A9;}
#imgslider .imgslider-link:hover {color:#f00;}
#imgslider input[type=radio] {vertical-align:middle; margin:0 2px;}
#imgslider .dataTableContent {padding:2px 4px;}
#imgslider .smallText {padding:5px;}
#imgslider .page-head {padding:0 5px;}
#imgslider .feat-head {font-size:12px; font-weight:bold; padding:2px;}
#imgslider .infoBoxLineContent {padding:2px 20px 2px 2px; font-size:10px;}
#imgslider .infoBoxContent .more-info, #imgslider .infoBoxLineContent .more-info {width:20px; height:16px; position:relative; cursor:pointer;}
#imgslider .infoBoxContent .more-info img, #imgslider .infoBoxLineContent .more-info img {vertical-align:middle;}
#imgslider .infoBoxContent .now-more-info, #imgslider .infoBoxLineContent .now-more-info {
display:none;
position:absolute;
left:16px;
top:-14px;
width:180px;
font-size:10px;
color:#000;
background:#fff;
background:rgba(255,255,255,.8);
padding:2px;
border:1px solid #07c;
border-radius:5px;
box-shadow:0 0 5px #767676;
}
#imgslider .infoBoxContent .more-info:hover .now-more-info, #imgslider .infoBoxLineContent .more-info:hover .now-more-info {display:block; z-index:1;}

#imgslider .acc-head {background:#ccc; cursor:pointer; border-radius:6px 6px 0 0;}
#imgslider .acc-head:hover {background:#fff;}
#imgslider .acc-cont {border:solid #fff; border-width:0 2px 2px; margin-bottom:20px;}

#imgslider .togg-all {margin:8px 0; text-align:center;}
#imgslider .op-all, #imgslider .cl-all {
color:#fff;
line-height:18px;
padding:1px 6px 2px 26px;
border:1px solid #949494;
border-radius:6px;
cursor:pointer;
}
#imgslider .infoBoxContentH {padding:2px 2px 2px 26px; font-size:16px; color:#fff; background:#a3a3a3 url(images/acc-closed.gif) left center no-repeat; border-radius:6px 6px 0 0;}
#imgslider .open .infoBoxContentH {background-image:url(images/acc-open.gif);}
#imgslider .acc-head.open {background-color:#fff;}
#imgslider .op-all {background:#a3a3a3 url(images/acc-closed.gif) left center no-repeat;}
#imgslider .cl-all {background:#a3a3a3 url(images/acc-open.gif) left center no-repeat;}
#imgslider .op-all:hover, .cl-all:hover {opacity:.85;}
#imgslider .st-rel {position:relative;}
#imgslider .st-rel {cursor:ne-resize;;}
#imgslider .st-rel:hover .img-preview img {position:absolute; right:95%; top:-50%; width:350px; z-index:1;}
#imgslider .speed-change, #imgslider .sorting-change {display:block; cursor:pointer;}
#imgslider .speed-input, #imgslider .sorting-input {display:none; width:40px;}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER).DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/css/jquery.colorbox.css'; ?>" />
<script type="text/javascript" src="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER).DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/javascript/jquery.colorbox.min.js'; ?>"></script>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
    $(".cbimages").colorbox({rel:'cbimages', scalePhotos:true, maxWidth: "90%", maxHeight: "90%", fixed: true, current: ""});
    $(".iframe").colorbox({iframe:true, width:"780", height:"560", maxWidth: "90%", maxHeight: "90%", fixed: true});

    $(document).bind('cbox_complete', function(){
        if($('#cboxTitle').height() > 20){
            $("#cboxTitle").hide();
            $("<div>"+$("#cboxTitle").html()+"</div>").css({color: $("#cboxTitle").css('color')}).insertAfter("#cboxPhoto");
            $.fn.colorbox.resize();
        }
    });   
});
/*]]>*/
</script>
<script type="text/javascript" src="includes/general.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
$(function(){
    //*** BOC multi functional accordion ***
    var $acch = $('.acc-head');
    var $accdiv = $('.acc-cont');
    $accdiv.slice(1).hide();
    $acch.first().addClass('open');
    $acch.click(function(e){
        e.preventDefault();
        
        var elHeight = $acch.height();
        var pos = $acch.index($(this));
        var effpos = elHeight * (pos + 1)+200;
        //var itOff = $(this).offset().top;
        //var itPos = itOff -200; //  - 60
        function scrollToTop () {
            setTimeout(function(){$('html, body').animate({scrollTop:effpos}, 800);},100);
        }    
        
        $('.acc-cont:visible').length > 1 ? $(this).removeClass('open') : $acch.removeClass('open');
        if ($(this).next($accdiv).is(':hidden')) {
            $acch.removeClass('open');
            $(this).addClass('open');
            $accdiv.slideUp(1000);
            $(this).next('div').slideDown(1000); scrollToTop();
        } else {
            $(this).next('div').slideUp(1000);
        }
    });
    $('.op-all').click(function(){
        $accdiv.slideDown(1000);
        $acch.addClass('open');
    });
    $('.cl-all').click(function(){
        $accdiv.slideUp(1000);
        $acch.removeClass('open');
    });
    //*** EOC multi functional accordion ***
    
    //BOC generate posibility to change speed in overview via ajax
    $('.speed-change').click(function() {
        $(this).toggle();
        $(this).next('.speed-input').toggle().focus();
    });
    $('.speed-input').blur(function() {
        var $this = $(this),
            inputval = $(this).val(),
            id = $(this).attr('id');
        $.post(window.location,
                {
                new_speed: ""+inputval+"",
                imgsl_id:  ""+id+""
                },
                function(data){
                    $this.prev('.speed-change').text(data);
                }
            );
        $(this).toggle();
        $(this).prev('.speed-change').toggle();
    });
    //EOC generate posibility to change speed in overview via ajax
    
    //BOC generate posibility to change sorting in overview via ajax
    $('.sorting-change').click(function() {
        $(this).toggle();
        $(this).next('.sorting-input').toggle().focus();
    });
    $('.sorting-input').blur(function() {
        var $this = $(this),
            inputsortval = $(this).val(),
            sortid = $(this).data('id');
        $.post(window.location,
                {
                new_sort: ""+inputsortval+"",
                imgslider_id:  ""+sortid+""
                },
                function(data){
                    $this.prev('.sorting-change').text(data);
                }
            );
        $(this).toggle();
        $(this).prev('.sorting-change').toggle();
    });
    //EOC generate posibility to change sorting in overview via ajax
    
    //BOC check also english radio buttons when german is checked
    $('input[name$="[2]"]').click(function() {
        var $t_val = $(this).val(),
            $t_attr_n = $(this).attr('name'),
            t_atrr_n_r = $t_attr_n.replace('[2]','[1]');
        $('input[name="'+t_atrr_n_r+'"][value="'+$t_val+'"]').prop('checked', $(this).is(':checked') );
        //alert($(this).attr('name'));
    });
    //EOC check also english radio buttons when german is checked
});
/* ]]> */
</script>

</head>
<body id="imgslider">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
        <?php //left_navigation
        if (USE_ADMIN_TOP_MENU == 'false') {
            echo '<td class="columnLeft2">'.PHP_EOL;
            echo '<!-- left_navigation //-->'.PHP_EOL;       
            require_once(DIR_WS_INCLUDES . 'column_left.php');
            echo '<!-- left_navigation eof //-->'.PHP_EOL; 
            echo '</td>'.PHP_EOL;      
        }
        ?>
        <td class="boxCenter" width="100%" valign="top">
            <div class="page-head">
                <p><?php echo HEADING_TITLE_II; ?></p>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
<?php if (($_GET['action'] != 'new') && ($_GET['action'] != 'edit')) { ?>		
                    <td valign="top">		
                        <table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr>
                                <?php
                                if($shop != $copy_shop) {
                                    echo '<td colspan="2">'.xtc_draw_form('copy_slider_from_othershop', FILENAME_IMAGESLIDERS, 'action=copy_slider&amp;page='.$_GET['page'], 'post', 'accept-charset="UTF-8"');
                                    echo '<table><tr><td class="smallText">';
                                    //echo xtc_draw_form('copy_slider_from_othershop', FILENAME_IMAGESLIDERS, 'action=copy_slider&amp;page='.$_GET['page'], 'post', 'accept-charset="UTF-8"');
                                    echo xtc_draw_hidden_field('slider_from_shop', $copy_shop);
                                    echo xtc_draw_input_field('which_ids_copy');
                                    echo '<br /><span class="sml-red">'.TXT_IDS_RANGE.'</span>';
                                    echo '</td>';
                                    echo '<td class="smallText">';
                                    echo xtc_button(sprintf(TXT_BUTTON_COPY_SLIDER, $copy_shop));
                                    echo '<br /><span class="sml-red">'.sprintf(TXT_COPY_SLIDER_EXPL, $copy_shop).'</span>';
                                    //echo '</form>';
                                    echo '</td></tr></table>';
                                    echo '</form></td>';
                                }
                                ?>
                                <td align="right" colspan="<?php echo ($shop != $copy_shop ? '3' : '4'); ?>" class="smallText">
                                    <?php
                                    //BOC new copy function, noRiddle
                                    if(isset($_GET['copy_warning'])) {echo '<pre style="color:#c00; font-weight:bold;">'.$_GET['copy_warning'].'</pre>';}
                                    $dd_arr = xtc_get_imagenames_for_dropdown();
                                    echo xtc_draw_form('copy_single_image', FILENAME_IMAGESLIDERS, 'action=copy&amp;page='.$_GET['page'], 'post', 'accept-charset="UTF-8"');
                                    echo xtc_draw_pull_down_menu('copy_image', $dd_arr, 'def', 'id="copy_image"').' ';
                                    echo xtc_button(TXT_BUTTON_COPY_IMAGE);
                                    echo '</form>';
                                    //EOC new copy function, noRiddle
                                    ?> 
                                </td>
                                <td align="right" colspan="5" class="smallText">
                                    <?php echo xtc_button_link(TXT_BUTTON_NEWIMAGE, xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $iInfo->imagesliders_id . '&amp;action=new')); ?>
                                </td>
                            </tr>
                            <tr class="dataTableHeadingRow">
                                <td style="width:350px" class="dataTableHeadingContent"><?php echo TABLE_HEADING_IMAGESLIDERS; ?></td>
                                <td class="dataTableHeadingContent"></td>
                                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SPEED; ?></td>
                                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SORTING; ?></td>
                                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_STATUS; ?></td>
                                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                            </tr>
<?php  
}
$imagesliders_query_raw = "select imagesliders_id, imagesliders_name, status, sorting, date_added, last_modified from " . TABLE_IMAGESLIDERS . " order by sorting";
$imagesliders_split = new splitPageResults($_GET['page'], '30', $imagesliders_query_raw, $imagesliders_query_numrows);
$imagesliders_query = xtc_db_query($imagesliders_query_raw);
while ($imagesliders = xtc_db_fetch_array($imagesliders_query)) {
    if (((!$_GET['iID']) || (@$_GET['iID'] == $imagesliders['imagesliders_id'])) && (!$iInfo) && (substr($_GET['action'], 0, 3) != 'new')) {
        $iInfo = new objectInfo($imagesliders);
        $iInfo->imagesliders_image = xtc_get_imageslider_image($iInfo->imagesliders_id, $language_id);
    }
    if (($_GET['action'] != 'new') && ($_GET['action'] != 'edit')) {
        if ( (is_object($iInfo)) && ($imagesliders['imagesliders_id'] == $iInfo->imagesliders_id) ) {
            //echo '<tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $imagesliders['imagesliders_id'] . '&amp;action=edit') . '\'">' . "\n";
            echo '<tr class="dataTableRowSelected">' . "\n"; //don't go to edit page since we want to change speed with ajax on row, noRiddle
        } else {
            //echo '<tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $imagesliders['imagesliders_id']) . '\'">' . "\n";
            echo '<tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'">' . "\n"; //don't check item since we want to edit speed on the fly via ajax, noRiddle
        }
        
        $pandur = xtc_get_imageslider_data_attributes($imagesliders['imagesliders_id'], $_SESSION['languages_id'], 'imagesliders_data_panduration');
?>
                                <td class="dataTableContent"><?php echo $imagesliders['imagesliders_name']; ?></td>
                                <td class="dataTableContent st-rel"><?php echo '<span class="img-preview">'.xtc_info_image(xtc_get_imageslider_image($imagesliders['imagesliders_id'], $_SESSION['languages_id']), $imagesliders['imagesliders_name'], '60').'</span>'; ?></td>
                                <td class="dataTableContent">
                                    <span class="speed-change"><?php echo $pandur; ?></span>
                                    <input type="text" name="speed-input" id="<?php echo $imagesliders['imagesliders_id']; ?>" class="speed-input" value ="<?php echo $pandur; ?>" />
                                </td>
                                <td class="dataTableContent">
                                    <span class="sorting-change"><?php echo $imagesliders['sorting']; ?></span>
                                    <input type="text" name="sorting-input" data-id="<?php echo $imagesliders['imagesliders_id']; ?>" class="sorting-input" value ="<?php echo $imagesliders['sorting']; ?>" />
                                    
                                    <?php //echo $imagesliders['sorting']; ?>
                                </td>
                                <td class="dataTableContent">
                                    <?php
                                    if ($imagesliders['status'] == '0') {
                                        echo xtc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . xtc_href_link(FILENAME_IMAGESLIDERS, 'action=setflag&amp;flag=1&amp;iID=' . $imagesliders['imagesliders_id']) . (isset($_GET['page']) ? '&page='.(int)$_GET['page'] : '').'">' . xtc_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
                                    } else {
                                        echo '<a href="' . xtc_href_link(FILENAME_IMAGESLIDERS, 'action=setflag&amp;flag=0&amp;iID=' . $imagesliders['imagesliders_id']) . (isset($_GET['page']) ? '&page='.(int)$_GET['page'] : '').'">' . xtc_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . xtc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                                    }
                                    ?>
                                </td>
                                <td class="dataTableContent" style="text-align:right;">
                                    <?php if ( (is_object($iInfo)) && ($imagesliders['imagesliders_id'] == $iInfo->imagesliders_id) ) { echo xtc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); } else { echo '<a href="' . xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $imagesliders['imagesliders_id']) . '">' . xtc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;
                                </td>
                            </tr>
<?php
    }
}
if (($_GET['action'] != 'new') && ($_GET['action'] != 'edit')) {
?>
                            <tr>
                                <td colspan="6">
                                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                        <tr>
                                            <td class="smallText" valign="top"><?php echo $imagesliders_split->display_count($imagesliders_query_numrows, '30', $_GET['page'], TEXT_DISPLAY_NUMBER_OF_IMAGESLIDERS); ?></td>
                                            <td class="smallText" align="right"><?php echo $imagesliders_split->display_links($imagesliders_query_numrows, '30', MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="5" class="smallText">
                                    <?php
                                    //BOC new copy function, noRiddle
                                    if(isset($_GET['copy_warning'])) {echo '<pre style="color:#c00; font-weight:bold;">'.$_GET['copy_warning'].'</pre>';}
                                    $dd_arr = xtc_get_imagenames_for_dropdown();
                                    echo xtc_draw_form('copy_single_image', FILENAME_IMAGESLIDERS, 'action=copy&amp;page='.$_GET['page'], 'post', 'accept-charset="UTF-8"');
                                    echo xtc_draw_pull_down_menu('copy_image', $dd_arr, 'def', 'id="copy_image"').' ';
                                    echo xtc_button(TXT_BUTTON_COPY_IMAGE);
                                    echo '</form>';
                                    //EOC new copy function, noRiddle
                                    ?> 
                                </td>
                                <td align="right" colspan="5" class="smallText">
                                    <?php echo xtc_button_link(TXT_BUTTON_NEWIMAGE, xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $iInfo->imagesliders_id . '&amp;action=new')); ?>
                                </td>
                            </tr>
                        </table>
                    </td>
<?php 
}
$heading = array();
$contents = array();
switch ($_GET['action']) {
    case 'new':
        $heading[] = array('text' => '<b>' . TEXT_HEADING_NEW_IMAGESLIDER . '</b>');
        $contents = array('form' => xtc_draw_form('imagesliders', FILENAME_IMAGESLIDERS, 'action=insert', 'post', 'enctype="multipart/form-data"'));
        $contents[] = array('text' => ''.TEXT_NEW_INTRO.'');
        $contents[] = array('text' => '<table><tr><td class="infoBoxLineContent" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_NAME . ' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_NAME.'</span></span>'.' '. xtc_draw_input_field('imagesliders_name', '', 'style="width:200px;"').'</td>'.
            '<td class="infoBoxLineContent" valign="top"><span class="feat-head">' . TABLE_HEADING_SORTING . ': </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_SORT.'</span></span>'.' '. xtc_draw_input_field('imagesliders_sorting', '', 'style="width:60px;"').'</td>'.
            '<td class="infoBoxLineContent" valign="top"><span class="feat-head">' . TABLE_HEADING_STATUS . ': </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_ACTIVE.'</span></span>'.' '. xtc_draw_selection_field('imagesliders_status', 'radio', '0').ACTIVE.'&nbsp;&nbsp;&nbsp;'.xtc_draw_selection_field('imagesliders_status', 'radio', '1').NOTACTIVE.'</td></tr></table>');

        $languages = xtc_get_languages();
	  
        $imageslider_image_string = '';
        $imageslider_image_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imageslider_image_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td class="infoBoxContent">' . xtc_draw_file_field('imagesliders_image'.$i) . '</td></tr></table>';
        }
        $imageslider_image_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_IMAGE . ' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_UPLOAD.'</span></span></td></tr></table></div>' . $imageslider_image_string);

        $imageslider_url_string = '';
        $url_target_array   = array ();
        $url_target_array[] = array ('id' => '0', 'text' => NONE_TARGET); 
        $url_target_array[] = array ('id' => '1', 'text' => TARGET_BLANK); 
        $url_target_array[] = array ('id' => '2', 'text' => TARGET_TOP); 
        $url_target_array[] = array ('id' => '3', 'text' => TARGET_SELF); 
        $url_target_array[] = array ('id' => '4', 'text' => TARGET_PARENT);
        $imageslider_url_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imageslider_url_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td class="infoBoxContent">' . TEXT_TYP . '<br />'. 
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '0').TYP_NONE.'<br />'.
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '1').TYP_EXTERN.'<br />'.
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '2').TYP_INTERN.'<br />'.
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '3').TYP_PRODUCT.'<br />'.
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '4').TYP_CATEGORIE.'<br />'.
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '5').TYP_CONTENT.'<br /><br />'.
                TEXT_URL . xtc_draw_input_field('imagesliders_url[' . $languages[$i]['id'] . ']', '', 'style="width:500px;"') . '&nbsp;' . TEXT_TARGET . '&nbsp;' . xtc_draw_pull_down_menu('imagesliders_url_target[' . $languages[$i]['id'] . ']', $url_target_array) . '<br /><br /></td></tr></table>';
        }
        $imageslider_url_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_URL .'</span></td></tr></table></div>' . $imageslider_url_string);
      
        // BOC new for images-alt-tag, noRiddle
        $imageslider_alt_string = '';
        $imageslider_alt_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imageslider_alt_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td>' . xtc_draw_input_field('imagesliders_alt[' . $languages[$i]['id'] . ']', '', 'style="width:300px;"').'</td></tr></table>';
        }
        $imageslider_alt_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="30%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_ALT .' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_ALT.'</span></span></td></tr></table></div>' .  $imageslider_alt_string);
        // EOC new for images-alt-tag, noRiddle
      
        // BOC choose data-attributes, noRiddle
        $imagesliders_data_attributes_string = '';
        $imagesliders_data_attributes_string .= '<div class="acc-cont">';
        for($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imagesliders_data_attributes_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td class="infoBoxContent">' . TXT_GENERAL_KENBURNS . '<br />'.
                TXT_IF_KENBURNS_ON.'<br />'.
                xtc_draw_selection_field('imagesliders_data_kenburn[' . $languages[$i]['id'] . ']', 'radio', 'on').TXT_KENBURN_ON.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_kenburn[' . $languages[$i]['id'] . ']', 'radio', 'off').TXT_KENBURN_OFF.'<br />'.
                TXT_GENERAL_TRANSITION.'<br />'.
                xtc_draw_selection_field('imagesliders_data_transition[' . $languages[$i]['id'] . ']', 'radio', 'fade').TXT_TRANSITION_FADE.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_transition[' . $languages[$i]['id'] . ']', 'radio', 'slide').TXT_TRANSITION_SLIDE.'<br />'.
                TXT_GENERAL_STARTALIGN.'<br />'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'left,top').TXT_STARTALIGN_LEFTTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'left,center').TXT_STARTALIGN_LEFTCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'left,bottom').TXT_STARTALIGN_LEFTBOTTOM.'<br />'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'center,top').TXT_STARTALIGN_CENTERTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'center,center').TXT_STARTALIGN_CENTERCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'center,bottom').TXT_STARTALIGN_CENTERBOTTOM.'<br />'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'right,top').TXT_STARTALIGN_RIGHTTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'right,center').TXT_STARTALIGN_RIGHTCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'right,bottom').TXT_STARTALIGN_RIGHTBOTTOM.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'random').TXT_STARTALING_RANDOM.'<br />'.
                TXT_GENERAL_ENDALIGN.'<br />'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'left,top').TXT_ENDALIGN_LEFTTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'left,center').TXT_ENDALIGN_LEFTCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'left,bottom').TXT_ENDALIGN_LEFTBOTTOM.'<br />'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'center,top').TXT_ENDALIGN_CENTERTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'center,center').TXT_ENDALIGN_CENTERCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'center,bottom').TXT_ENDALIGN_CENTERBOTTOM.'<br />'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'right,top').TXT_ENDALIGN_RIGHTTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'right,center').TXT_ENDALIGN_RIGHTCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'right,bottom').TXT_ENDALIGN_RIGHTBOTTOM.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'random').TXT_ENDALIGN_RANDOM.'<br />'.
                TXT_GENERAL_ZOOM_AND_PAN.'<br />'.
                xtc_draw_input_field('imagesliders_data_zoom[' . $languages[$i]['id'] . ']', '', 'style="width:100px;"').'&nbsp;'.TXT_ZOOM.'<br />'.
                xtc_draw_input_field('imagesliders_data_zoomfact[' . $languages[$i]['id'] . ']', '', 'style="width:40px;"').'&nbsp;'.TXT_ZOOMFACT.'<br />'.
                xtc_draw_input_field('imagesliders_data_panduration[' . $languages[$i]['id'] . ']', '', 'style="width:40px;"').'&nbsp;'.TXT_PANDURATION.
                '</td></tr></table>';
        }
        $imagesliders_data_attributes_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="30%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_DATA_ATTRIBUTES .' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_DATA_ATTRIBUTES.'</span></span></td></tr></table></div>' .  $imagesliders_data_attributes_string);
        // EOC choose data-attributes, noRiddle
            
        // BOC choose where text will be displayed and appearing effect
        $imagesliders_caption_attributes_string = '';
        $imagesliders_caption_attributes_string .= '<div class="acc-cont">';
        for($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imagesliders_caption_attributes_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td class="infoBoxContent">' . TXT_GENERAL_CAPTION . '<br />'.
                TXT_CAPTION_DIRECTION_AND_EFFECT.'<br />'.
                xtc_draw_selection_field('imagesliders_desc_direct[' . $languages[$i]['id'] . ']', 'radio', 'cp-left').TXT_CAPTION_LEFT.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_desc_effect[' . $languages[$i]['id'] . ']', 'radio', 'faderight').TXT_CAPTION_FADE_RIGHT.'<br />'.
                xtc_draw_selection_field('imagesliders_desc_direct[' . $languages[$i]['id'] . ']', 'radio', 'cp-right').TXT_CAPTION_RIGHT.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_desc_effect[' . $languages[$i]['id'] . ']', 'radio', 'fadeleft').TXT_CAPTION_FADE_LEFT.'<br />'.
                xtc_draw_selection_field('imagesliders_desc_direct[' . $languages[$i]['id'] . ']', 'radio', 'cp-top').TXT_CAPTION_TOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_desc_effect[' . $languages[$i]['id'] . ']', 'radio', 'fadedown').TXT_CAPTION_FADE_DOWN.'<br />'.
                xtc_draw_selection_field('imagesliders_desc_direct[' . $languages[$i]['id'] . ']', 'radio', 'cp-bottom').TXT_CAPTION_BOTTOM.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_desc_effect[' . $languages[$i]['id'] . ']', 'radio', 'fadeup').TXT_CAPTION_FADE_UP.'<br />'.
                xtc_draw_selection_field('imagesliders_desc_effect[' . $languages[$i]['id'] . ']', 'radio', 'fade').TXT_CAPTION_FADE.
                '</td></tr></table>';
        }
        $imagesliders_caption_attributes_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="30%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_CAPTION_ATTRIBUTES .' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_CAPTION_ATTRIBUTES.'</span></span></td></tr></table></div>' .  $imagesliders_caption_attributes_string);
        // EOC choose where text will be displayed and appearing effect
			
        $imageslider_description_string = '';
        $imageslider_description_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imageslider_description_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td>' . xtc_draw_textarea_field('imagesliders_description['.$languages[$i]['id'].']', 'soft', '70', '25', '', 'style="width:850px"').'</td></tr></table>'; 
        }
        $imageslider_description_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_DESCRIPTION .'</span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGESLIDERS_DESCRIPTION.'</span></span></td></tr></table></div>' .  $imageslider_description_string);
            
        // BOC post video url, noRiddle
        $imagesliders_video_url_string = '';
        $imagesliders_video_url_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imagesliders_video_url_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td>' . xtc_draw_input_field('imagesliders_video_url[' . $languages[$i]['id'] . ']', '', 'style="width:500px;"').'</td></tr></table>';
        }
        $imagesliders_video_url_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="50%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_VIDEO_URL .' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_VIDEO_URL.'</span></span></td></tr></table></div>' .  $imagesliders_video_url_string);
        // EOC post video url, noRiddle
            
        // BOC textfield for video description, noRiddle
        $imagesliders_video_expl_string = '';
        $imagesliders_video_expl_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imagesliders_video_expl_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td>' . xtc_draw_textarea_field('imagesliders_video_expl['.$languages[$i]['id'].']', 'soft', '70', '25', '', 'style="width:850px;"').'</td></tr></table>'; 
        }
        $imagesliders_video_expl_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_VIDEO_DESC .'</span></td></tr></table></div>' .  $imagesliders_video_expl_string);
        // EOC textfield for video description, noRiddle

        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_CREDITS_DESC .'</span></td></tr></table></div>'.'<div class="acc-cont">' . TEXT_IMAGESLIDERS_CREDITS_EXPL . '</div>');
      
        $contents[] = array('align' => 'left', 'text' => '<br />' . xtc_button(BUTTON_SAVE) . '&nbsp;' . xtc_button_link(BUTTON_CANCEL, xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $_GET['iID'])));
    break;

    case 'edit':
        $heading[] = array('text' => '<b>' . TEXT_HEADING_EDIT_IMAGESLIDER . '</b>&nbsp;&nbsp;&nbsp;&mdash;&nbsp;<span class="togg-all"><span class="op-all">open all</span> &nbsp; <span class="cl-all">close all</span></span>&nbsp;&mdash;');

        $contents = array('form' => xtc_draw_form('imagesliders', FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $iInfo->imagesliders_id . '&amp;action=save', 'post', 'enctype="multipart/form-data"'));
        $contents[] = array('text' => TEXT_EDIT_INTRO);
        $contents[] = array('text' => '<div><table><tr><td class="infoBoxLineContent" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_NAME . ' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_NAME.'</span></span>'.' '. xtc_draw_input_field('imagesliders_name', $iInfo->imagesliders_name, 'style="width:200px;"').'</td> '.
            '<td class="infoBoxLineContent" valign="top"><span class="feat-head">' . TABLE_HEADING_SORTING . ': </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_SORT.'</span></span>'.' '. xtc_draw_input_field('imagesliders_sorting', $iInfo->sorting, 'style="width:60px;"').'</td>'.
            '<td class="infoBoxLineContent" valign="top"><span class="feat-head">' . TABLE_HEADING_STATUS . ': </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_ACTIVE.'</span></span>'.' '. xtc_draw_selection_field('imagesliders_status', 'radio', '0',$iInfo->status==0 ? true : false).ACTIVE.'&nbsp;&nbsp;&nbsp;'.xtc_draw_selection_field('imagesliders_status', 'radio', '1',$iInfo->status==1 ? true : false).NOTACTIVE.'</td></tr></table></div>');
	  
        $languages = xtc_get_languages();
      
        $imageslider_image_string = '';
        $image = '';
        $imginf = '';
        $imageslider_image_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imginf = getimagesize('../images/'.xtc_get_imageslider_image($iInfo->imagesliders_id, $languages[$i]['id']));
            $imageslider_image_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td class="infoBoxContent">' . xtc_draw_file_field('imagesliders_image'.$i) . '<br />' . xtc_info_image(xtc_get_imageslider_image($iInfo->imagesliders_id, $languages[$i]['id']), $iInfo->imagesliders_name, '400') . ' <span><a class="cbimages" title="'.TEXT_IMAGE_DIMENSIONS.$imginf[0]. ' &times; ' .$imginf[1].'" href="../images/'.xtc_get_imageslider_image($iInfo->imagesliders_id, $languages[$i]['id']).'"> <img src="images/page_find.gif" alt="" /> &raquo '.TEXT_IMAGE_DIMENSIONS.$imginf[0].' &times; '.$imginf[1].'</a></span><br />' . xtc_draw_selection_field('imagesliders_image_delete'. $i, 'checkbox', 'imagesliders_image'. $i) .' '. TEXT_HEADING_DELETE_IMAGESLIDER .'</td></tr></table>';
        }
        $imageslider_image_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_IMAGE . ' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_UPLOAD.'</span></span></td></tr></table></div>' . $imageslider_image_string);
	
        $imageslider_url_string = '';
        $url_target_array   = array ();
        $url_target_array[] = array ('id' => '0', 'text' => NONE_TARGET); 
        $url_target_array[] = array ('id' => '1', 'text' => TARGET_BLANK); 
        $url_target_array[] = array ('id' => '2', 'text' => TARGET_TOP); 
        $url_target_array[] = array ('id' => '3', 'text' => TARGET_SELF); 
        $url_target_array[] = array ('id' => '4', 'text' => TARGET_PARENT);
        $imageslider_url_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imageslider_url_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td class="infoBoxContent">' . TEXT_TYP . '<br />'. 
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '0',xtc_get_imageslider_url_typ($iInfo->imagesliders_id, $languages[$i]['id'])==0 ? true : false).TYP_NONE.'<br />'.
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '1',xtc_get_imageslider_url_typ($iInfo->imagesliders_id, $languages[$i]['id'])==1 ? true : false).TYP_EXTERN.'<br />'.
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '2',xtc_get_imageslider_url_typ($iInfo->imagesliders_id, $languages[$i]['id'])==2 ? true : false).TYP_INTERN.'<br />'.
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '3',xtc_get_imageslider_url_typ($iInfo->imagesliders_id, $languages[$i]['id'])==3 ? true : false).TYP_PRODUCT.'<br />'.
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '4',xtc_get_imageslider_url_typ($iInfo->imagesliders_id, $languages[$i]['id'])==4 ? true : false).TYP_CATEGORIE.'<br />'.
                xtc_draw_selection_field('imagesliders_url_typ[' . $languages[$i]['id'] . ']', 'radio', '5',xtc_get_imageslider_url_typ($iInfo->imagesliders_id, $languages[$i]['id'])==5 ? true : false).TYP_CONTENT.'<br /><br />'.
                TEXT_URL . xtc_draw_input_field('imagesliders_url[' . $languages[$i]['id'] . ']', xtc_get_imageslider_url($iInfo->imagesliders_id, $languages[$i]['id']), 'style="width:500px;"') . '&nbsp;' . TEXT_TARGET . '&nbsp;' . xtc_draw_pull_down_menu('imagesliders_url_target[' . $languages[$i]['id'] . ']', $url_target_array, xtc_get_imageslider_url_target($iInfo->imagesliders_id, $languages[$i]['id'])) . '<br /><br /></td></tr></table>';
        }
        $imageslider_url_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_URL .'</span></td></tr></table></div>' . $imageslider_url_string);
	
        // BOC new for images-alt-tag, noRiddle
        $imageslider_alt_string = '';
        $imageslider_alt_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imageslider_alt_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td>' . xtc_draw_input_field('imagesliders_alt[' . $languages[$i]['id'] . ']', xtc_get_imageslider_alt($iInfo->imagesliders_id, $languages[$i]['id']), 'style="width:300px;"').'</td></tr></table>';
        }
        $imageslider_alt_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_ALT .' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_ALT.'</span></span></td></tr></table></div>' . $imageslider_alt_string);
        // BOC new for images-alt-tag, noRiddle

        // BOC choose data-attributes, noRiddle
        $imagesliders_data_attributes_string = '';
        $imagesliders_data_attributes_string .= '<div class="acc-cont">'; 
        for($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imagesliders_data_attributes_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td class="infoBoxContent">' . TXT_GENERAL_KENBURNS . '<br />'.
                TXT_IF_KENBURNS_ON.'<br />'.
                xtc_draw_selection_field('imagesliders_data_kenburn[' . $languages[$i]['id'] . ']', 'radio', 'on',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_kenburn') == 'on' ? true : false).TXT_KENBURN_ON.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_kenburn[' . $languages[$i]['id'] . ']', 'radio', 'off',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_kenburn') == 'off' ? true : false).TXT_KENBURN_OFF.'<br /><hr />'.
                TXT_GENERAL_TRANSITION.'<br />'.
                xtc_draw_selection_field('imagesliders_data_transition[' . $languages[$i]['id'] . ']', 'radio', 'fade',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_transition') == 'fade' ? true : false).TXT_TRANSITION_FADE.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_transition[' . $languages[$i]['id'] . ']', 'radio', 'slide',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_transition') == 'slide' ? true : false).TXT_TRANSITION_SLIDE.'<br /><hr />'.
                TXT_GENERAL_STARTALIGN.'<br />'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'left,top',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_startalign') == 'left,top' ? true : false).TXT_STARTALIGN_LEFTTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'left,center',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_startalign') == 'left,center' ? true : false).TXT_STARTALIGN_LEFTCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'left,bottom',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_startalign') == 'left,bottom' ? true : false).TXT_STARTALIGN_LEFTBOTTOM.'<br />'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'center,top',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_startalign') == 'center,top' ? true : false).TXT_STARTALIGN_CENTERTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'center,center',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_startalign') == 'center,center' ? true : false).TXT_STARTALIGN_CENTERCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'center,bottom',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_startalign') == 'center,bottom' ? true : false).TXT_STARTALIGN_CENTERBOTTOM.'<br />'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'right,top',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_startalign') == 'right,top' ? true : false).TXT_STARTALIGN_RIGHTTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'right,center',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_startalign') == 'right,center' ? true : false).TXT_STARTALIGN_RIGHTCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'right,bottom',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_startalign') == 'right,bottom' ? true : false).TXT_STARTALIGN_RIGHTBOTTOM.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_startalign[' . $languages[$i]['id'] . ']', 'radio', 'random',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_startalign') == 'random' ? true : false).TXT_STARTALING_RANDOM.'<br /><hr />'.
                TXT_GENERAL_ENDALIGN.'<br />'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'left,top',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_endalign') == 'left,top' ? true : false).TXT_ENDALIGN_LEFTTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'left,center',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_endalign') == 'left,center' ? true : false).TXT_ENDALIGN_LEFTCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'left,bottom',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_endalign') == 'left,bottom' ? true : false).TXT_ENDALIGN_LEFTBOTTOM.'<br />'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'center,top',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_endalign') == 'center,top' ? true : false).TXT_ENDALIGN_CENTERTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'center,center',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_endalign') == 'center,center' ? true : false).TXT_ENDALIGN_CENTERCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'center,bottom',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_endalign') == 'center,bottom' ? true : false).TXT_ENDALIGN_CENTERBOTTOM.'<br />'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'right,top',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_endalign') == 'right,top' ? true : false).TXT_ENDALIGN_RIGHTTOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'right,center',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_endalign') == 'right,center' ? true : false).TXT_ENDALIGN_RIGHTCENTER.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'right,bottom',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_endalign') == 'right,bottom' ? true : false).TXT_ENDALIGN_RIGHTBOTTOM.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_data_endalign[' . $languages[$i]['id'] . ']', 'radio', 'random',xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_endalign') == 'random' ? true : false).TXT_ENDALIGN_RANDOM.'<br /><hr />'.
                TXT_GENERAL_ZOOM_AND_PAN.'<br />'.
                xtc_draw_input_field('imagesliders_data_zoom[' . $languages[$i]['id'] . ']', xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_zoom'), 'style="width:100px;"').'&nbsp;'.TXT_ZOOM.'<br />'.
                xtc_draw_input_field('imagesliders_data_zoomfact[' . $languages[$i]['id'] . ']', xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_zoomfact'), 'style="width:40px;"').'&nbsp;'.TXT_ZOOMFACT.'<br />'.
                xtc_draw_input_field('imagesliders_data_panduration[' . $languages[$i]['id'] . ']', xtc_get_imageslider_data_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_data_panduration'), 'style="width:40px;"').'&nbsp;'.TXT_PANDURATION.
                '<br /><br /></td></tr></table>';
        }
        $imagesliders_data_attributes_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="30%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_DATA_ATTRIBUTES .' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_DATA_ATTRIBUTES.'</span></span></td></tr></table></div>' .  $imagesliders_data_attributes_string);
        // EOC choose data-attributes, noRiddle
            
        // BOC choose where text will be displayed and appearing effect
        $imagesliders_caption_attributes_string = '';
        $imagesliders_caption_attributes_string .= '<div class="acc-cont">';
        for($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imagesliders_caption_attributes_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td class="infoBoxContent">' . TXT_GENERAL_CAPTION . '<br />'.
                TXT_CAPTION_DIRECTION_AND_EFFECT.'<br />'.
                xtc_draw_selection_field('imagesliders_desc_direct[' . $languages[$i]['id'] . ']', 'radio', 'cp-left',xtc_get_imageslider_caption_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_desc_direct') == 'cp-left' ? true : false).TXT_CAPTION_LEFT.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_desc_effect[' . $languages[$i]['id'] . ']', 'radio', 'faderight',xtc_get_imageslider_caption_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_desc_effect') == 'faderight' ? true : false).TXT_CAPTION_FADE_RIGHT.'<br />'.
                xtc_draw_selection_field('imagesliders_desc_direct[' . $languages[$i]['id'] . ']', 'radio', 'cp-right',xtc_get_imageslider_caption_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_desc_direct') == 'cp-right' ? true : false).TXT_CAPTION_RIGHT.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_desc_effect[' . $languages[$i]['id'] . ']', 'radio', 'fadeleft',xtc_get_imageslider_caption_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_desc_effect') == 'fadeleft' ? true : false).TXT_CAPTION_FADE_LEFT.'<br />'.
                xtc_draw_selection_field('imagesliders_desc_direct[' . $languages[$i]['id'] . ']', 'radio', 'cp-top',xtc_get_imageslider_caption_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_desc_direct') == 'cp-top' ? true : false).TXT_CAPTION_TOP.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_desc_effect[' . $languages[$i]['id'] . ']', 'radio', 'fadedown',xtc_get_imageslider_caption_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_desc_effect') == 'fadedown' ? true : false).TXT_CAPTION_FADE_DOWN.'<br />'.
                xtc_draw_selection_field('imagesliders_desc_direct[' . $languages[$i]['id'] . ']', 'radio', 'cp-bottom',xtc_get_imageslider_caption_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_desc_direct') == 'cp-bottom' ? true : false).TXT_CAPTION_BOTTOM.'&nbsp;&nbsp;'.
                xtc_draw_selection_field('imagesliders_desc_effect[' . $languages[$i]['id'] . ']', 'radio', 'fadeup',xtc_get_imageslider_caption_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_desc_effect') == 'fadeup' ? true : false).TXT_CAPTION_FADE_UP.'<br />'.
                xtc_draw_selection_field('imagesliders_desc_effect[' . $languages[$i]['id'] . ']', 'radio', 'fade',xtc_get_imageslider_caption_attributes($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_desc_effect') == 'fade' ? true : false).TXT_CAPTION_FADE.
                '</td></tr></table>';
        }
        $imagesliders_caption_attributes_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="30%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_CAPTION_ATTRIBUTES .' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_CAPTION_ATTRIBUTES.'</span></span></td></tr></table></div>' .  $imagesliders_caption_attributes_string);
        // EOC choose where text will be displayed and appearing effect
			
        $imageslider_description_string = '';
        $imageslider_description_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imageslider_description_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td>' . xtc_draw_textarea_field('imagesliders_description['.$languages[$i]['id'].']','soft','70','25',(($imageslider_description[$languages[$i]['id']]) ? stripslashes($imageslider_description[$languages[$i]['id']]) : xtc_get_imageslider_description($iInfo->imagesliders_id, $languages[$i]['id'])), 'style="width:850px;"').'</td></tr></table>'; 
        }
        $imageslider_description_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_DESCRIPTION .'</span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGESLIDERS_DESCRIPTION.'</span></span></td></tr></table></div>' . $imageslider_description_string);
            
        // BOC post video url, noRiddle
        $imagesliders_video_url_string = '';
        $imagesliders_video_url_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imagesliders_video_url_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td>' . xtc_draw_input_field('imagesliders_video_url[' . $languages[$i]['id'] . ']', xtc_get_imagesliders_video_data($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_video_url'), 'style="width:500px;"').'</td></tr></table>';
        }
        $imagesliders_video_url_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="50%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_VIDEO_URL .' </span><span class="more-info"><img src="images/info.png" alt="" /><span class="now-more-info">'.INFO_IMAGE_VIDEO_URL.'</span></span></td></tr></table></div>' .  $imagesliders_video_url_string);
        // EOC post video url, noRiddle
            
        // BOC textfield for video description, noRiddle
        $imagesliders_video_expl_string = '';
        $imagesliders_video_expl_string .= '<div class="acc-cont">';
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $imagesliders_video_expl_string .= '<table width="100%"><tr><td class="infoBoxContent" width="1%" valign="top">' . xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/admin/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</td><td>' . xtc_draw_textarea_field('imagesliders_video_expl['.$languages[$i]['id'].']', 'soft', '70', '25', (($imagesliders_video_expl[$languages[$i]['id']]) ? stripslashes($imagesliders_video_expl[$languages[$i]['id']]) : xtc_get_imagesliders_video_data($iInfo->imagesliders_id, $languages[$i]['id'], 'imagesliders_video_expl')), 'style="width:850px;"').'</td></tr></table>'; 
        }
        $imagesliders_video_expl_string .= '</div>';
        $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_VIDEO_DESC .'</span></td></tr></table></div>' .  $imagesliders_video_expl_string);
        // EOC textfield for video description, noRiddle

            $contents[] = array('text' => '<div class="acc-head"><table width="100%"><tr><td class="infoBoxContentH" width="100%" valign="top"><span class="feat-head">' . TEXT_IMAGESLIDERS_CREDITS_DESC .'</span></td></tr></table></div>'.'<div class="acc-cont">' . TEXT_IMAGESLIDERS_CREDITS_EXPL . '</div>');
			
        $contents[] = array('align' => 'left', 'text' => '<br />' . xtc_button(BUTTON_SAVE) . '&nbsp;' . xtc_button_link(BUTTON_CANCEL, xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $iInfo->imagesliders_id)) . ' &nbsp; ' . xtc_button_link(BUTTON_BACK, xtc_href_link(FILENAME_IMAGESLIDERS, '')));
    break;

    case 'delete':
        $heading[] = array('text' => '<b>' . TEXT_HEADING_DELETE_IMAGESLIDER . '</b>');

        $contents = array('form' => xtc_draw_form('imagesliders', FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $iInfo->imagesliders_id . '&amp;action=deleteconfirm'));
        $contents[] = array('text' => TEXT_DELETE_INTRO);
        $contents[] = array('text' => '<br /><b>' . $iInfo->imagesliders_name . '</b>');
        //$contents[] = array('text' => '<br />' . xtc_draw_checkbox_field('delete_image', '', true) . ' ' . TEXT_DELETE_IMAGE);
        $contents[] = array('text' => '<br />' . xtc_draw_checkbox_field('delete_image') . ' ' . TEXT_DELETE_IMAGE); // don't check checkbox per default, noRiddle
        $contents[] = array('align' => 'left', 'text' => '<br />' . xtc_button(BUTTON_DELETE) . '&nbsp;' . xtc_button_link(BUTTON_CANCEL, xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $iInfo->imagesliders_id)));
    break;

    default:
        if (is_object($iInfo)) {
            $heading[] = array('text' => '<b>' . $iInfo->imagesliders_name . '</b>');

            $contents[] = array('align' => 'left', 'text' => xtc_button_link(BUTTON_EDIT, xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $iInfo->imagesliders_id . '&amp;action=edit')) . '&nbsp;' . xtc_button_link(BUTTON_DELETE, xtc_href_link(FILENAME_IMAGESLIDERS, 'page=' . $_GET['page'] . '&amp;iID=' . $iInfo->imagesliders_id . '&amp;action=delete')));
            $contents[] = array('text' => '<br />' . TEXT_DATE_ADDED . ' ' . xtc_date_short($iInfo->date_added));
            if (xtc_not_null($iInfo->last_modified)) $contents[] = array('text' => TEXT_LAST_MODIFIED . ' ' . xtc_date_short($iInfo->last_modified));
                $contents[] = array('text' => '<br />' . xtc_get_imageslider_image($iInfo->imagesliders_id, $languages[$i]['id']));
        }
    break;
}

if ( (xtc_not_null($heading)) && (xtc_not_null($contents)) ) {
    echo '<td valign="top" width="265" style="padding-left:5px;">';

    $box = new box;
    echo $box->infoBox($heading, $contents);
    echo '</td>' . "\n";
}
?>
    </tr>
</table>
</td>
</tr>
</table>

<!-- footer //-->
<?php //require(DIR_WS_INCLUDES . 'footer.php'); // don't include footer because of conflicts with mailhive, hard code it ?>
<?php defined( '_VALID_XTC' ) or die( 'Direct Access to this location is not allowed.' ); ?>
<br />
<table border="0" width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td align="center" class="smallText">
      <?php
      /*
      The following copyright announcement is in compliance
      to section 2c of the GNU General Public License, and
      thus can not be removed, or can only be modified
      appropriately.

      Please leave this comment intact together with the
      following copyright announcement.
  
      Copyright announcement changed due to the permissions
      from LG Hamburg from 28th February 2003 / AZ 308 O 70/03
    */
      ?>
      <a style="text-decoration:none;" href="http://www.modified-shop.org" target="_blank"><span style="color:#B0347E;">mod</span><span style="color:#6D6D6D;">ified eCommerce Shopsoftware</span></a><span style="color:#555555;">&nbsp;&copy;2009-<?php echo date("Y"); ?>&nbsp;provides no warranty and is redistributable under the <a style="color:#555555;text-decoration:none;" href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">GNU General Public License (Version 2)</a><br />eCommerce Engine 2006 based on <a style="text-decoration:none; color:#555555;" href="http://www.xt-commerce.com/" rel="nofollow" target="_blank">xt:Commerce</a></span>
    </td>
  </tr>
  <tr>
    <td><?php echo xtc_image(DIR_WS_IMAGES . 'pixel_trans.gif', '', '1', '5'); ?></td>
  </tr>
</table>
<!-- footer_eof //-->
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
<?php
/***************************************
* file: do_application_bottom.php
* use: do what you need here ;-)
* (c) noRiddle 05-2017 | 04-2018
***************************************/

//BOC close DB from shops
if(isset($_GET['c']) && array_key_exists($_GET['c'], $_SESSION['nr_ctlg_brands'])) {
    if($shp_db_link != false)
        mysqli_close($shp_db_link);
    $shp_db_link = false;
}
//BOC close DB from shops

//BOC order in other shop system module
if(defined('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS')) {
    if(MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS == 'true') {
        if(strpos($PHP_SELF, 'shopping_cart.php') !== false) {
            mysqli_close($ext_shp_db_link);
        }
    }
}
//EOC order in other shop system module

//BOC test to kill certain connections to central customer data database
/*
$ccdb_envir = ${'ccdb_'.$cat_env};

$cdb_link = new mysqli($ccdb_envir['central_customers_db']['sql_server'], $ccdb_envir['central_customers_db']['sql_user'], $ccdb_envir['central_customers_db']['sql_pass'], $ccdb_envir['central_customers_db']['sql_db']) or die(mysqli_error());
if ($cdb_link->connect_errno) {
    die("keine Verbindung möglich: " . $cdb_link->connect_error);
}

$proc_result = $cdb_link->query("SHOW FULL PROCESSLIST");
while ($proc_row = $proc_result->fetch_assoc()) {
    if ($proc_row['Time'] > 20 && $proc_row['Command'] == 'Sleep') {
        $kill_proc_sql = 'KILL '.$proc_row['Id'];
        $cdb_link->query($kill_proc_sql);
    }
}

if($cdb_link) {
    $cdb_link->close();
}
*/ //after moving to new Server with MariaDB seems not to be necessary any more, 03-2021, noRiddle
//EOC test to kill certain connections to central customer data database
?>
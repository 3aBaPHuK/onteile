<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_german.php 40 2013-01-08 16:36:44Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_german.php, v 1.12 2003/08/18);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('BOX_INFORMATION_AFFILIATE', 'Programa de afiliados');
define('BOX_AFFILIATE_INFO', 'Información acerca del programa');
define('BOX_AFFILIATE_SUMMARY', 'Resumen de la cuenta de afiliado');
define('BOX_AFFILIATE_ACCOUNT', 'Editar cuenta de afiliado');
define('BOX_AFFILIATE_CLICKRATE', 'Resumen de clics');
define('BOX_AFFILIATE_PAYMENT', 'Pagos de comisión');
define('BOX_AFFILIATE_SALES', 'Resumen de ventas');
define('BOX_AFFILIATE_BANNERS', 'Banner');
define('BOX_AFFILIATE_TEXTLINKS', 'Enlaces de texto');
define('BOX_AFFILIATE_PRODUCTLINK', 'Enlaces de producto');
define('BOX_AFFILIATE_CONTACT', 'Contáctenos');
define('BOX_AFFILIATE_FAQ', 'Preguntas Frecuentes');
define('BOX_AFFILIATE_AGB', 'Condiciónes de participación');
define('BOX_AFFILIATE_LOGIN', 'Iniciar sesión de afiliado');
define('BOX_AFFILIATE_LOGOUT', 'Cerrar sesión de afiliado');

define('ENTRY_AFFILIATE_ACCEPT_AGB', 'Por favor, confirme aquí que ha leído y está de acuerdo con nuestros <a class="iframe" target="_blank" href="%s">términos y condiciones</a>.');
define('ENTRY_AFFILIATE_AGB_ERROR', 'Tiene que acceptar nuestros términos y condiciónes generales.');
define('ENTRY_AFFILIATE_PAYMENT_CHECK_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_CHECK_ERROR', ' <small><font color="#FF0000">requerido</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_PAYPAL_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_PAYPAL_ERROR', ' <small><font color="#FF0000">requerido</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_BANK_NAME_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_BANK_NAME_ERROR', ' <small><font color="#FF0000">requerido</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NAME_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NAME_ERROR', ' <small><font color="#FF0000">requerido</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NUMBER_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NUMBER_ERROR', ' <small><font color="#FF0000">requerido</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_BANK_BRANCH_NUMBER_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_BANK_BRANCH_NUMBER_ERROR', ' <small><font color="#FF0000">requerido</font></small>');
define('ENTRY_AFFILIATE_PAYMENT_BANK_SWIFT_CODE_TEXT', '');
define('ENTRY_AFFILIATE_PAYMENT_BANK_SWIFT_CODE_ERROR', ' <small><font color="#FF0000">requerido</font></small>');
define('ENTRY_AFFILIATE_COMPANY_TEXT', '');
define('ENTRY_AFFILIATE_COMPANY_ERROR', ' <small><font color="#FF0000">requerido</font></small>');
define('ENTRY_AFFILIATE_COMPANY_TAXID_TEXT', '');
define('ENTRY_AFFILIATE_COMPANY_TAXID_ERROR', ' <small><font color="#FF0000">requerido</font></small>');
define('ENTRY_AFFILIATE_HOMEPAGE_TEXT', '*'); //' <small><font color="#000000"> (http://)</font></small>');
define('ENTRY_AFFILIATE_HOMEPAGE_ERROR', ' <small><font color="#FF0000">requerido (http://)</font></small>');
define('PASSWORD_HIDDEN', 'Su contraseña ha sido oculta.');

define('TEXT_AFFILIATE_PERIOD', 'Período: ');
define('TEXT_AFFILIATE_STATUS', 'Estado: ');
define('TEXT_AFFILIATE_LEVEL', 'Nivel: ');
define('TEXT_AFFILIATE_ALL_PERIODS', 'Todos los períodos');
define('TEXT_AFFILIATE_ALL_STATUS', 'Todos los estados');
define('TEXT_AFFILIATE_ALL_LEVELS', 'Todos los niveles');
define('TEXT_AFFILIATE_PERSONAL_LEVEL', 'Personal');
define('TEXT_AFFILIATE_LEVEL_SUFFIX', 'Nivel ');
define('TEXT_AFFILIATE_NAME', 'Nombre de Banner: ');
define('TEXT_AFFILIATE_INFO', 'Copie y pegue el código HTML en su sitio web.');
define('TEXT_DISPLAY_NUMBER_OF_CLICKS', 'Mostrando <b>%d</b> a <b>%d</b> (de <b>%d</b> Clics totales)');
define('TEXT_DISPLAY_NUMBER_OF_SALES', 'Mostrando <b>%d</b> a <b>%d</b> (de <b>%d</b> Ventas totales)');
define('TEXT_DISPLAY_NUMBER_OF_PAYMENTS', 'Mostrando <b>%d</b> a <b>%d</b> (de <b>%d</b> Pagos totales)');
define('TEXT_DELETED_ORDER_BY_ADMIN', 'Borrado (Admin)');
define('TEXT_AFFILIATE_PERSONAL_LEVEL_SHORT', 'Pers.');

define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - Nueva contraseña para el programa de afiliados');

define('MAIL_AFFILIATE_SUBJECT', 'Bienvenido al programa de afiliados de ' . STORE_NAME);

define('EMAIL_SUBJECT', 'Programa de afiliados');

define('NAVBAR_TITLE', 'Programa de afiliados');
define('NAVBAR_TITLE_AFFILIATE', 'Iniciar sesión');
define('NAVBAR_TITLE_BANNERS', 'Banner');
define('NAVBAR_TITLE_CLICKS', 'Clics');
define('NAVBAR_TITLE_CONTACT', 'Contacto');
define('NAVBAR_TITLE_DETAILS', 'Editar datos');
define('NAVBAR_TITLE_DETAILS_OK', 'Datos modificados');
define('NAVBAR_TITLE_FAQ', 'Preguntas frecuentes');
define('NAVBAR_TITLE_INFO', 'Información');
define('NAVBAR_TITLE_LOGOUT', 'Cerrar sesión');
define('NAVBAR_TITLE_PASSWORD_FORGOTTEN', 'Contraseña olvidada');
define('NAVBAR_TITLE_PAYMENT', 'Pago');
define('NAVBAR_TITLE_SALES', 'Ventas');
define('NAVBAR_TITLE_SIGNUP', 'Registrarse');
define('NAVBAR_TITLESIGNUP_OK', 'Registrado');
define('NAVBAR_TITLE_SUMMARY', 'Resumen');
define('NAVBAR_TITLE_TERMS', 'Términos y condiciones');

define('IMAGE_BANNERS', 'Banner de afiliado');
define('IMAGE_CLICKTHROUGHS', 'Informe sobre clics');
define('IMAGE_SALES', 'Informe sobre ventas');
?>

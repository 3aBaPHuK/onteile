<?php
/* --------------------------------------------------------------
   $Id: tax_rates.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(tax_rates.php,v 1.9 2003/03/13); www.oscommerce.com 
   (c) 2003	 nextcommerce (tax_rates.php,v 1.4 2003/08/1); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Tasas de impuestos');

define('TABLE_HEADING_TAX_RATE_PRIORITY', 'Prioridad');
define('TABLE_HEADING_TAX_CLASS_TITLE', 'Clase de impuestos');
define('TABLE_HEADING_COUNTRIES_NAME', 'País');
define('TABLE_HEADING_ZONE', 'Zona fiscal');
define('TABLE_HEADING_TAX_RATE', 'Tasa de impuestos');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_INFO_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');
define('TEXT_INFO_DATE_ADDED', 'agregado el:');
define('TEXT_INFO_LAST_MODIFIED', 'última modificación:');
define('TEXT_INFO_CLASS_TITLE', 'Nombre de la clase de impuestos:');
define('TEXT_INFO_COUNTRY_NAME', 'País:');
define('TEXT_INFO_ZONE_NAME', 'Zona fiscal:');
define('TEXT_INFO_TAX_RATE', 'Tasa de impuestos (%):');
define('TEXT_INFO_TAX_RATE_PRIORITY', 'Se añaden tasas de impuestos de la misma prioridad, otras se mezclan. <br/><br/><b>IMPORTANTE:</b> Para los servicios realizados electrónicamente, esto debe ajustarse a 99<br /><br />prioridad:');
define('TEXT_INFO_RATE_DESCRIPTION', 'Descripción:');
define('TEXT_INFO_INSERT_INTRO', 'Introduzca la nueva tasa de impuestos con todos los datos relevantes.');
define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar esta tasa de impuestos?');
define('TEXT_INFO_HEADING_NEW_TAX_RATE', 'Nueva tasa de impuestos');
define('TEXT_INFO_HEADING_EDIT_TAX_RATE', 'Editar tasa de impuestos');
define('TEXT_INFO_HEADING_DELETE_TAX_RATE', 'Borrar tasa de impuestos');
?>

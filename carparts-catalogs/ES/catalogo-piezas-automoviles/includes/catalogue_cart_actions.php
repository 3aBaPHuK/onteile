<?php
/********************************************
* file: catalogue_cart_actions.php
* use: cart actions for catalogue
* (c) noRiddle 10-2016
********************************************/

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if(xtc_not_null($action) || isset($_POST['check_to_buy'])) {
    if(xtc_not_null($action)) {
        $parameters = array ('action', 'brd', 'mod');
    }
    if(isset($_POST['check_to_buy'])) {
        $parameters = array ('coID', 'c', 'vid', 'uid', 'gid', 'coi', 'spi2', 'ssd');
    }

    if (DISPLAY_CART == 'true') {
        $goto = basename($PHP_SELF); //'catalogue_shopping_cart.php'; //we don't want to display catalogue cart, noRiddle
    } else {
        $goto = basename($PHP_SELF);
    }
    
    if (!is_object($_SESSION['catalogue_cart'.$_SESSION['shopsess']])) {
        $_SESSION['catalogue_cart'.$_SESSION['shopsess']] = new shoppingCart();
    }
}

//BOC code for catalogue shopping cart
if(isset($_POST['check_to_buy'])) {
    $post_arr = array();
    foreach($_POST['check_to_buy'] as $modl) {
        $modl = trim(preg_replace('#[^0-9a-z]#i', '', $modl));
        for($i = 0, $ct_pm = count($_POST['products_model']); $i < $ct_pm; $i++) {
            $post_pm_i = trim(preg_replace('#[^0-9a-z]#i', '', $_POST['products_model'][$i]));
            $post_brand_i = trim(preg_replace('#[^a-z-_]#i', '', $_POST['prod_brand'][$i]));
            $lax_brand = trim(preg_replace('#[^0-9a-z_]#i', '', $_POST['lax_brand']));
            
            if($modl == $post_pm_i && !in_array($post_pm_i, $post_arr)) { // && !in_array($post_pm_i, $post_arr)
                //$cart_qty = isset($_SESSION['catalogue_cart'.$_SESSION['shopsess']]->contents[$post_brand_i][$post_pm_i]) ? ((int)$_POST['prod_model_qty'][$i] + (int)$_SESSION['catalogue_cart'.$_SESSION['shopsess']]->contents[$post_brand_i][$post_pm_i]['quantity']) : (int)$_POST['prod_model_qty'][$i];
                $cart_qty[$i] = ((int)$_POST['prod_model_qty'][$i] + (int)$_SESSION['catalogue_cart'.$_SESSION['shopsess']]->get_quantity($post_brand_i, $post_pm_i));
                if(isset($_SESSION['customer_id'])) {
                    $post_arr[$post_pm_i] = array('customers_id' => $_SESSION['customer_id'],
                                                  'products_brand' => $post_brand_i,
                                                  'lax_brand' => $lax_brand,
                                                  'products_model' => $post_pm_i,
                                                  'products_id' => (int)$_POST['products_id'][$i],
                                                  'products_name' => $_POST['products_name'][$i],
                                                  'quantity' => $cart_qty[$i], //(int)$_POST['prod_model_qty'][$i],
                                                  'products_price' => $_POST['prod_price'][$i],
                                                  'products_tax_class_id' => (int)$_POST['prod_txcl_id'][$i]
                                                 );
                } else {
                    $post_arr[$post_pm_i] = array('sess_id' => session_id(),
                                                  'products_brand' => $post_brand_i,
                                                  'lax_brand' => $lax_brand,
                                                  'products_model' => $post_pm_i,
                                                  'products_id' => (int)$_POST['products_id'][$i],
                                                  'products_name' => $_POST['products_name'][$i],
                                                  'quantity' => $cart_qty[$i], //(int)$_POST['prod_model_qty'][$i],
                                                  'products_price' => $_POST['prod_price'][$i],
                                                  'products_tax_class_id' => (int)$_POST['prod_txcl_id'][$i]
                                                 );

                }
                //echo '<pre>$_SESSION[catalogue_cart]->in_cart($post_brand_i, $post_pm_i): '.($_SESSION['catalogue_cart'.$_SESSION['shopsess']]->in_cart($post_brand_i, $post_pm_i) ? 'true<br />' : 'false<br />').'</pre>';
            }
            //echo '<pre>'.($modl == $post_pm_i ? '$modl: '.$modl.' == $post_pm_i: '.$post_pm_i.' | true<br />' : '$modl: '.$modl.' == $post_pm_i: '.$post_pm_i.' | false<br />').'</pre>';
        }
    }
    foreach($post_arr as $model_arr) {
        $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->add_cart($model_arr);
    }
    if($goto == 'catalogue_shopping_cart.php') {
        xtc_redirect(xtc_href_link($goto, xtc_get_all_get_params($parameters), 'SSL'));
    } else {
        xtc_redirect(xtc_href_link($goto, xtc_get_all_get_params(), 'SSL'));
    }
}
//echo '<pre>$post_arr: '.print_r($post_arr, true).'</pre>';
//echo '<pre>$_SESSION[catalogue_cart]: '.print_r($_SESSION['catalogue_cart'.$_SESSION['shopsess']], true).'</pre>';
//EOC code for catalogue shopping cart

//echo '<pre>$_POST[products_model]: '.print_r($_POST['products_model'], true).'</pre>';
//echo '<pre>$_POST[products_brand]: '.print_r($_POST['products_brand'], true).'</pre>';

// Shopping cart actions
if (xtc_not_null($action)) {
    // redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled
    if ($session_started == false) {
        xtc_redirect(xtc_href_link(FILENAME_COOKIE_USAGE));
    }

    switch ($action) {
        case 'catalogue_remove_product':
            $mod = trim(preg_replace('#[^0-9a-z]#i', '', $_GET['mod']));
            $brd = trim(preg_replace('#[^a-z-_]#i', '', $_GET['brd']));
            $_SESSION['catalogue_cart'.$_SESSION['shopsess']] -> remove($brd, $mod);
            xtc_redirect(xtc_href_link($goto, xtc_get_all_get_params($parameters), 'SSL'));
        break;

        // customer wants to update the product quantity in the shopping cart
        case 'catalogue_update_product':
            for ($i = 0, $n = sizeof($_POST['products_model']); $i < $n; $i++) {
                $cart_quantity = $_POST['cart_quantity'][$i] = xtc_remove_non_numeric($_POST['cart_quantity'][$i]);
                $_POST['products_model'][$i] = trim(preg_replace('#[^0-9a-z]#i', '', $_POST['products_model'][$i]));
                $_POST['products_brand'][$i] = trim(preg_replace('#[^a-z-_]#i', '', $_POST['products_brand'][$i]));

                if ($cart_quantity == 0) $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->remove($_POST['products_brand'][$i] ,$_POST['products_model'][$i]);
             
                if (in_array($_POST['products_model'][$i], (isset($_POST['cart_delete']) && is_array($_POST['cart_delete']) ? $_POST['cart_delete'] : array ()))) {
                    $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->remove($_POST['products_brand'][$i], $_POST['products_model'][$i]);
                } else {
                    if ($cart_quantity > MAX_PRODUCTS_QTY)
                        $cart_quantity = MAX_PRODUCTS_QTY;

                    $add_arr = array('products_brand' => $_POST['products_brand'][$i],
                                     'products_model' => $_POST['products_model'][$i],
                                     'quantity' => $cart_quantity
                                    );
                    $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->add_cart($add_arr, false);
                    unset($cart_quantity);
                }
            }
            xtc_redirect(xtc_href_link($goto, xtc_get_all_get_params($parameters), 'SSL'));
        break;
    }
}
?>
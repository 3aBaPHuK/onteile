<?php
/* -----------------------------------------------------------------------------------------
   $Id: ups.php 5121 2013-07-18 11:38:19Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(UPS.php,v 1.4 2003/02/18 04:28:00); www.oscommerce.com 
   (c) 2003	 nextcommerce (UPS.php,v 1.5 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   German Post (Deutsche Post WorldNet)
   Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl
   Changes for personal use: Copyright (C) 2004 Comm4All, Bernd Blazynski | http://www.comm4all.com & http://www.cheapshirt.de

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/


define('MODULE_SHIPPING_UPS_TEXT_TITLE', 'United Parcel Service Standard');
define('MODULE_SHIPPING_UPS_TEXT_DESCRIPTION', 'United Parcel Service Standard - módulo de envío');
define('MODULE_SHIPPING_UPS_TEXT_WAY', ' Envíos a ');
define('MODULE_SHIPPING_UPS_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_UPS_TEXT_FREE', '¡A partir EUR ' . MODULE_SHIPPING_UPS_FREEAMOUNT . ' del valor del pedido le enviamos su pedido libre de costos de envío!');
define('MODULE_SHIPPING_UPS_TEXT_LOW', '¡A partir EUR ' . MODULE_SHIPPING_UPS_FREEAMOUNT . ' valor del pedido le enviamos su pedido a costos de envío reducidos!');
define('MODULE_SHIPPING_UPS_INVALID_ZONE', 'No es posible realizar un envío a este país.');
define('MODULE_SHIPPING_UPS_UNDEFINED_RATE', 'Los costos de envío no se pueden calcular por el momento.');

define('MODULE_SHIPPING_UPS_STATUS_TITLE' , 'UPS Standard');
define('MODULE_SHIPPING_UPS_STATUS_DESC' , '¿Desea ofrecer envíos a través de UPS Standard?');
define('MODULE_SHIPPING_UPS_HANDLING_TITLE' , 'Recargo');
define('MODULE_SHIPPING_UPS_HANDLING_DESC' , 'Recargo de procesamiento para este modo de envío en Euro');
define('MODULE_SHIPPING_UPS_TAX_CLASS_TITLE' , 'Tasa de impuesto');
define('MODULE_SHIPPING_UPS_TAX_CLASS_DESC' , 'Seleccione el tipo de IVA para este modo de envío.');
define('MODULE_SHIPPING_UPS_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_UPS_ZONE_DESC' , 'Si selecciona una zona, este modo de envío sólo se ofrecerá en esa zona.');
define('MODULE_SHIPPING_UPS_SORT_ORDER_TITLE' , 'Orden de la visualización');
define('MODULE_SHIPPING_UPS_SORT_ORDER_DESC' , 'Menor se muestra en primer lugar.');
define('MODULE_SHIPPING_UPS_ALLOWED_TITLE' , 'Zonas de envío individuales');
define('MODULE_SHIPPING_UPS_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE');
define('MODULE_SHIPPING_UPS_FREEAMOUNT_TITLE' , 'libre de costos de envío doméstico');
define('MODULE_SHIPPING_UPS_FREEAMOUNT_DESC' , 'Valor mínimo de pedido para envíos nacionales libres de costos de envío y envíos internacionales reducidos.');

define('MODULE_SHIPPING_UPS_COUNTRIES_1_TITLE' , 'Estados para UPS Standard Zona 1');
define('MODULE_SHIPPING_UPS_COUNTRIES_1_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 1 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPS_COST_1_TITLE' , 'Tarifas para UPS Standard Zona 1');
define('MODULE_SHIPPING_UPS_COST_1_DESC' , 'Costos de envío basados en el peso dentro de la zona 1. Ejemplo: Envío entre 0 y 4 kg cuesta 5,15 EUR = 4:5.15,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_2_TITLE' , 'Estados para UPS Standard Zona 3');
define('MODULE_SHIPPING_UPS_COUNTRIES_2_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 3 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPS_COST_2_TITLE' , 'Tarifas para UPS Standard Zona 3');
define('MODULE_SHIPPING_UPS_COST_2_DESC' , 'Costos de envío basados en el peso dentro de la zona 3. Ejemplo: Envío entre 0 y 4 kg cuesta 13,75 EUR = 4:13.75,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_3_TITLE' , 'Estados para UPS Standard Zona 31');
define('MODULE_SHIPPING_UPS_COUNTRIES_3_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 31 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPS_COST_3_TITLE' , 'Tarifas para UPS Standard Zona 31');
define('MODULE_SHIPPING_UPS_COST_3_DESC' , 'Costos de envío basados en el peso dentro de la zona 31. Ejemplo: Envío entre 0 y 4 kg cuesta 23,50 EUR = 4:23.50,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_4_TITLE' , 'Estados para UPS Standard Zona 4');
define('MODULE_SHIPPING_UPS_COUNTRIES_4_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 4 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPS_COST_4_TITLE' , 'Tarifas para UPS Standard Zona 4');
define('MODULE_SHIPPING_UPS_COST_4_DESC' , 'Costos de envío basados en el peso dentro de la zona 4. Ejemplo: Envío entre 0 y 4 kg cuesta 25,40 EUR = 4:25.40,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_5_TITLE' , 'Estados para UPS Standard Zona 41');
define('MODULE_SHIPPING_UPS_COUNTRIES_5_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 41 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPS_COST_5_TITLE' , 'Tarifas para UPS Standard Zona 41');
define('MODULE_SHIPPING_UPS_COST_5_DESC' , 'Costos de envío basados en el peso dentro de la zona 41. Ejemplo: Envío entre 0 y 4 kg cuesta 30,00 EUR = 4:30.00,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_6_TITLE' , 'Estados para UPS Standard Zona 5');
define('MODULE_SHIPPING_UPS_COUNTRIES_6_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 5 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPS_COST_6_TITLE' , 'Tarifas para UPS Standard Zona 5');
define('MODULE_SHIPPING_UPS_COST_6_DESC' , 'Costos de envío basados en el peso dentro de la zona 5. Ejemplo: Envío entre 0 y 4 kg cuesta 34,35 EUR = 4:34.35,...');

define('MODULE_SHIPPING_UPS_COUNTRIES_7_TITLE' , 'Estados para UPS Standard Zona 6');
define('MODULE_SHIPPING_UPS_COUNTRIES_7_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 6 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPS_COST_7_TITLE' , 'Tarifas para UPS Standard Zona 6');
define('MODULE_SHIPPING_UPS_COST_7_DESC' , 'Costos de envío basados en el peso dentro de la zona 6. Ejemplo: Envío entre 0 y 4 kg cuesta 37,10 EUR = 4:37.10,...');
?>

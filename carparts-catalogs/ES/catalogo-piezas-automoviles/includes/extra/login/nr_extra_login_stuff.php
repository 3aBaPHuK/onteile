<?php
/*****************************************************
* file: nr_extra_login_stuff.php
* path: path: /includes/extra/login/
*
* (c) 09-2019 noRiddle
*****************************************************/

//BOC set a session when just logged in for use of account information (see e.g. general_bottom.hs.php in template), noRiddle
if(isset($_SESSION['customer_id'])) {
    if(isset($_GET['action']) && ($_GET['action'] == 'process')) {
        $_SESSION['just_logged_in'] = true;
    }
}
//EOC set a session when just logged in for use of account information (see e.g. general_bottom.hs.php in template), noRiddle
?>
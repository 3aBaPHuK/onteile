<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

if (defined('_VALID_XTC')) {
define('MODULE_SHIPCLOUD_TEXT_TITLE', 'shipcloud - la nouvelle génération d\'expédition de colis');
define('MODULE_SHIPCLOUD_TEXT_DESCRIPTION', 'Imprimer étiquettes du colis directement depuis la boutique.');
define('MODULE_SHIPCLOUD_STATUS_TITLE', 'Statut');
define('MODULE_SHIPCLOUD_STATUS_DESC', 'Activer module');
define('MODULE_SHIPCLOUD_API_TITLE', '<hr noshade>API');
define('MODULE_SHIPCLOUD_API_DESC', 'Clé API de shipcloud');
define('MODULE_SHIPCLOUD_PARCEL_TITLE', '<hr noshade>Tailles des colis');
define('MODULE_SHIPCLOUD_PARCEL_DESC', 'Veuillez saisir les tailles des colis en cm de manière suivante : longueur,largeur,hauteur;<br/>Divisez plusieurs mesures de colis avec un point-virgule (;). p.ex : 20,40,30;15,20,20;');
define('MODULE_SHIPCLOUD_COMPANY_TITLE', '<hr noshade>Détails de client<br/>');
define('MODULE_SHIPCLOUD_COMPANY_DESC', 'Entreprise :');
define('MODULE_SHIPCLOUD_FIRSTNAME_TITLE', '');
define('MODULE_SHIPCLOUD_FIRSTNAME_DESC', 'Prénom :');
define('MODULE_SHIPCLOUD_LASTNAME_TITLE', '');
define('MODULE_SHIPCLOUD_LASTNAME_DESC', 'Nom :');
define('MODULE_SHIPCLOUD_ADDRESS_TITLE', '');
define('MODULE_SHIPCLOUD_ADDRESS_DESC', 'Adresse :');
define('MODULE_SHIPCLOUD_POSTCODE_TITLE', '');
define('MODULE_SHIPCLOUD_POSTCODE_DESC', 'Code postal :');
define('MODULE_SHIPCLOUD_CITY_TITLE', '');
define('MODULE_SHIPCLOUD_CITY_DESC', 'Ville :');
define('MODULE_SHIPCLOUD_TELEPHONE_TITLE', '');
define('MODULE_SHIPCLOUD_TELEPHONE_DESC', 'Téléphone :');
define('MODULE_SHIPCLOUD_ACCOUNT_IBAN_TITLE', '');
define('MODULE_SHIPCLOUD_ACCOUNT_IBAN_DESC', 'IBAN :');
define('MODULE_SHIPCLOUD_ACCOUNT_BIC_TITLE', '');
define('MODULE_SHIPCLOUD_ACCOUNT_BIC_DESC', 'BIC :');
define('MODULE_SHIPCLOUD_BANK_NAME_TITLE', '<hr noshade>Détails de la banque<br/>');
define('MODULE_SHIPCLOUD_BANK_NAME_DESC', 'Banque :');
define('MODULE_SHIPCLOUD_BANK_HOLDER_TITLE', '');
define('MODULE_SHIPCLOUD_BANK_HOLDER_DESC', 'Propriétaire du compte :');
define('MODULE_SHIPCLOUD_LOG_TITLE', '<hr noshade>Log');
define('MODULE_SHIPCLOUD_LOG_DESC', 'le fichier journal sera déposé dans le dossier /log.');
define('MODULE_SHIPCLOUD_EMAIL_TITLE', '<hr noshade>Notification par mail');
define('MODULE_SHIPCLOUD_EMAIL_DESC', 'Souhaitez-vous que le client sera notifié par mail ?');
define('MODULE_SHIPCLOUD_EMAIL_TYPE_TITLE', '<hr noshade>Notification');
define('MODULE_SHIPCLOUD_EMAIL_TYPE_DESC', 'Souhaitez-vous que le client sera notifié de la part de la boutique ou de shipcloud ?<br><Remarque :</b>Pour une notification de la part de la boutique, il faut qu\'un webhook soit créé dans shipcloud pour l\'URL suivante : '.xtc_catalog_href_link('callback/shipcloud/callback.php', '', 'SSL', false).'');
}

define('SHIPMENT.TRACKING.SHIPCLOUD_LABEL_CREATED', 'Étiquette de colis sur shipcloud créé');
define('SHIPMENT.TRACKING.LABEL_CREATED', 'Étiquette de colis créé');
define('SHIPMENT.TRACKING.PICKED_UP', 'Colis retiré par le livreur');
define('SHIPMENT.TRACKING.TRANSIT', 'Colis sur le chemin');
define('SHIPMENT.TRACKING.OUT_FOR_DELIVERY', 'Colis sera livré');
define('SHIPMENT.TRACKING.DELIVERED', 'Colis livré');
define('SHIPMENT.TRACKING.AWAITS_PICKUP_BY_RECEIVER', 'Attendre la récupération de la part du client');
define('SHIPMENT.TRACKING.CANCELED', 'Étiquette de colis supprimée');
define('SHIPMENT.TRACKING.DELAYED', 'Expédition retardée');
define('SHIPMENT.TRACKING.EXCEPTION', 'Problème avec la livraison');
define('SHIPMENT.TRACKING.NOT_DELIVERED', 'Le colis n\'a pas pu être livré.');
define('SHIPMENT.TRACKING.NOTIFICATION', 'Message interne : resultats de tracking au sein de l\'expédition demandent des informations plus complexes.');
define('SHIPMENT.TRACKING.UNKNOWN', 'Statut inconnu');
?>

<?php
/*************************************************************
* file: order_status_comments.php
* use: language constants for order_status_comments
* (c) noRiddle 12-2016
*************************************************************/

define('HEADING_TITLE', 'Bestellstatus-Kommentartexte');

define('TEXT_SORT_ASC', 'aufsteigend');
define('TEXT_SORT_DESC', 'absteigend');

define('TABLE_HEADING_ORDER_STATUS_COMMENTS_ID', 'ID');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_TITLE', 'Kommentar-Titel');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_TEXT', 'Kommentar-Text');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_SORT_ORDER', 'Sortierung');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_STATUS', 'Status');
define('TABLE_HEADING_ORDER_STATUS_COMMENTS_ACTION', 'Aktion');

define('TEXT_ORDER_STATUS_COMMENT', 'Kommentarvorlage %d');
define('TEXT_INFO_DELETE_COMMENT_INTRO', 'Sind Sie sicher, dass Sie diese Kommentar-Vorlage l&ouml;schen m&ouml;chten ?');
define('TEXT_DELETE_COMMENT', 'Kommentar-Vorlage löschen ?');
define('TEXT_INFO_HEADING_DELETE_ORDER_STATUS_COMMENT', 'Kommentar-Vorlage l&ouml;schen');
define('TEXT_DATE_ADDED_ORDER_STATUS_COMMENT', 'Hinzugefügt am:');
define('TEXT_LAST_MODIFIED_ORDER_STATUS_COMMENT', 'Letzte Änderung am:');

define('TEXT_ORDER_STATUS_COMMENTS_STATUS', 'Status:');
define('TEXT_ORDER_STATUS_COMMENTS_SORT_ORDER', 'Sortierung:');
define('TEXT_ORDER_STATUS_COMMENTS_TITLE', 'Titel des Kommentartextes:');
define('TEXT_ORDER_STATUS_COMMENTS_TEXT', 'Kommentartext:');

define('TEXT_DISPLAY_NUMBER_OF_ORDER_STYTUS_COMMENTS', 'Angezeigt werden <b>%d</b> bis <b>%d</b> (von insgesamt <b>%d</b> Kommentar-Texten)');
?>
<?php
/* --------------------------------------------------------------
   $Id: customers_status.php 1062 2005-07-21 19:57:29Z gwinger $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(customers.php,v 1.76 2003/05/04); www.oscommerce.com 
   (c) 2003	 nextcommerce (customers_status.php,v 1.12 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Groupes de clients');

define('ENTRY_CUSTOMERS_FSK18', 'Bloquer l\'achat des articles FSK18?');
define('ENTRY_CUSTOMERS_FSK18_DISPLAY', 'Affichage des articles FSK18?');
define('ENTRY_CUSTOMERS_STATUS_ADD_TAX', 'Afficher TVA sur la facture');
define('ENTRY_CUSTOMERS_STATUS_MIN_ORDER', 'Valeur minimale de commande:');
define('ENTRY_CUSTOMERS_STATUS_MAX_ORDER', 'Valeur maximale de commande:');
define('ENTRY_CUSTOMERS_STATUS_BT_PERMISSION', 'Par prélèvement automatique');
define('ENTRY_CUSTOMERS_STATUS_CC_PERMISSION', 'Par carte de crédit');
define('ENTRY_CUSTOMERS_STATUS_COD_PERMISSION', 'Par nom');
define('ENTRY_CUSTOMERS_STATUS_DISCOUNT_ATTRIBUTES', 'Rabais');
define('ENTRY_CUSTOMERS_STATUS_PAYMENT_UNALLOWED', 'Entrez des modes de paiement non autorisés');
define('ENTRY_CUSTOMERS_STATUS_PUBLIC', 'Publique');
define('ENTRY_CUSTOMERS_STATUS_SHIPPING_UNALLOWED', 'Entrer des modes d\'expédition non autorisés');
define('ENTRY_CUSTOMERS_STATUS_SHOW_PRICE', 'Prix');
define('ENTRY_CUSTOMERS_STATUS_SHOW_PRICE_TAX', 'Prix incl. TVA');
define('ENTRY_CUSTOMERS_STATUS_WRITE_REVIEWS', 'Groupe de clients a le droit d\'écrire des évaluation de produits?');
define('ENTRY_CUSTOMERS_STATUS_READ_REVIEWS', 'Groupe de clients a le droit de lire des évaluations de produits?');
define('ENTRY_CUSTOMERS_STATUS_REVIEWS_STATUS', 'Déverrouiller des évaluations de produit automatiquement?');
define('ENTRY_GRADUATED_PRICES', 'Prix échelonnés');
define('ENTRY_NO', 'Non');
define('ENTRY_OT_XMEMBER', 'Rabais client sur valeur totale de la commande? :');
define('ENTRY_YES', 'Oui');

define('ERROR_REMOVE_DEFAULT_CUSTOMERS_STATUS', 'Erreur: Le groupe de clients par défaut ne peut pas être supprimé. Veuillez d\'abord établir un groupe de clients par défaut et réessayer.');
define('ERROR_STATUS_USED_IN_CUSTOMERS', 'Erreur: Ce groupe de clients est actuellement utilisé.');
define('ERROR_STATUS_USED_IN_HISTORY', 'Erreur: Ce groupe de client est actuellement utilisé dans l\'aperçu des commandes.');

define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_CUSTOMERS_GRADUATED', 'Prix échelonné');
define('TABLE_HEADING_CUSTOMERS_STATUS', 'Groupe de clients');
define('TABLE_HEADING_CUSTOMERS_UNALLOW', 'Modes de paiement non autorisés');
define('TABLE_HEADING_CUSTOMERS_UNALLOW_SHIPPING', 'Modes d\'expédition non autorisés');
define('TABLE_HEADING_DISCOUNT', 'Rabais');
define('TABLE_HEADING_TAX_PRICE', 'Prix / TVA');

define('TAX_NO', 'excl.');
define('TAX_YES', 'incl.');

define('TEXT_DISPLAY_NUMBER_OF_CUSTOMERS_STATUS', 'Groupes de clients existants:');

define('TEXT_INFO_CUSTOMERS_FSK18_DISPLAY_INTRO', '<strong>Articles FSK18</strong>');
define('TEXT_INFO_CUSTOMERS_FSK18_INTRO', '<strong>Bloquage FSK18</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_ADD_TAX_INTRO', '<strong>Si "Prix incl. TVA" = "Oui", régler sur "Non" </strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_MIN_ORDER_INTRO', '<strong>Enrez une valeur minimale de commande ou laissez ce champ vide.</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_MAX_ORDER_INTRO', '<strong>Enrez une valeur maximale de commande ou laissez ce champ vide.</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_BT_PERMISSION_INTRO', '<strong>Voulez-vous autoriser le paiement par prélèvement pour ce groupe de clients?</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_CC_PERMISSION_INTRO', '<strong>Voulez-vous autoriser le paiement par carte crédit pour ce groupe de clients?</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_COD_PERMISSION_INTRO', '<strong>Voulez-vous autoriser le paiement par nom pour ce groupe de clients?</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_DISCOUNT_ATTRIBUTES_INTRO', '<strong>Rabais sur attributs de l\'article</strong><br />(Max. % Rabais sur un article)');
define('TEXT_INFO_CUSTOMERS_STATUS_DISCOUNT_OT_XMEMBER_INTRO', '<strong>Rabais sur la commande totale</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_DISCOUNT_PRICE', 'Rabais (0 à 100%):');
define('TEXT_INFO_CUSTOMERS_STATUS_DISCOUNT_PRICE_INTRO', '<strong>Rabais maximale sur des produits  (dépendant du rabais du produit).</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_GRADUATED_PRICES_INTRO', '<strong>Prix échelonnés</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_IMAGE', '<strong>Image du groupe de clients:</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_NAME', '<strong>Nom du groupe</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_PAYMENT_UNALLOWED_INTRO', '<strong>Modes de paiement non autorisés</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_PUBLIC_INTRO', '<strong>Groupe publique?</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_SHIPPING_UNALLOWED_INTRO', '<strong>Modes d\'expédition non autorisés</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_SHOW_PRICE_INTRO', '<strong>Affichage des prix dans la boutique</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_SHOW_PRICE_TAX_INTRO', '<strong>Afficher les prix incl. TVA ou excl. TVA?</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_WRITE_REVIEWS_INTRO', '<strong>Écrire des évaluations de produit</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_READ_REVIEWS_INTRO', '<strong>Lire des évaluations de produit</strong>');
define('TEXT_INFO_CUSTOMERS_STATUS_REVIEWS_STATUS_INTRO', '<strong>Déverrouiller des évaluations de produit</strong>');

define('TEXT_INFO_DELETE_INTRO', 'Voulez-vous vraiment supprimer ce groupe de clients?');
define('TEXT_INFO_EDIT_INTRO', 'Veuillez effectuer tous les réglages nécessaires');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez créer un nouveau groupe de clients avec les réglages souhaités');

define('TEXT_INFO_HEADING_DELETE_CUSTOMERS_STATUS', 'Supprimer groupe de clients');
define('TEXT_INFO_HEADING_EDIT_CUSTOMERS_STATUS', 'Modifier données de groupe');
define('TEXT_INFO_HEADING_NEW_CUSTOMERS_STATUS', 'Nouveau groupe de clients');

define('TEXT_INFO_CUSTOMERS_STATUS_BASE', '<strong>Groupe de clients vasic pour des prix d\'articles</strong>');
define('ENTRY_CUSTOMERS_STATUS_BASE', 'Appliquer les prix d\'articles des groupes de clients suivants. Si sélection = "Admin les prix pour le nouveau groupe de clients ne sont pas prises en charge.');
define('ENTRY_CUSTOMERS_STATUS_BASE_EDIT', 'Appliquer les prix d\'articles des groupes de clients suivants. Si sélection = "Admin les prix pour le nouveau groupe de clients ne sont pas prises en charge.<br /><span class="col-red"><strong>ATTENTION:</strong></span> Tous les prix des groupes de clients déjà existants seront écrasés!');

define('TEXT_INFO_CUSTOMERS_GROUP_ADOPT_PERMISSION', '<strong>Appliquer les droits de visibilité d\'un autre groupe de clients</strong>');
define('ENTRY_CUSTOMERS_GROUP_ADOPT_PERMISSION', 'Appliquer les droits de catégorie, d\'article, de contenu et de visibilité du groupe suivant:');
define('CUSTOMERS_GROUP_ADOPT_PERMISSIONS', 'Ne pas appliquer de droits');

define('TEXT_INFO_CUSTOMERS_STATUS_SHOW_PRICE_TAX_TOTAL', '<b>Afficher TVA sur Montant des achats</b>');
define('ENTRY_CUSTOMERS_STATUS_SHOW_PRICE_TAX_TOTAL', 'Montant minimal d\'achat');

define('TABLE_HEADING_CUSTOMERS_SPECIALS', 'Offres spéciales');
define('TEXT_INFO_CUSTOMERS_STATUS_SPECIALS_INTRO', '<strong>Offres spéciales</strong>');
define('ENTRY_CUSTOMERS_STATUS_SPECIALS', 'Groupe autorisé à voir des offres spéciales?');

define('CUSTOMERS_GROUP_PUBLIC', 'publique');
?>

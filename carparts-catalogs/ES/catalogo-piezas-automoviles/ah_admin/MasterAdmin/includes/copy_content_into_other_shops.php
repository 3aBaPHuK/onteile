<?php
/**************************************************************
* file: copy_content_into_other_shops.php
* use: copy contents from one shop into all the others
* (c) noRiddle 10-2016
**************************************************************/

//write all shops into array
$out = '';
$out_singl = '';
$out_allowed = '';
$shop_arr = array();
$shop_arr[] = array('id' => '0', 'text' => ADMIN_TOOLS_COPY_CONTENTS_CHOOSE);
//BOC add new landing page in shop, 03-2021, noRiddle
//$shp_land_page = trim(DIR_WS_CATALOG, '/'); //could do this on landing page but want to have it the same everywhere, also in shops
$shp_land_page_arr = explode('.', $domain);
$shp_land_page = $shp_land_page_arr[0]; //landing page
//EOC add new landing page in shop, 03-2021, noRiddle
foreach($data20 as $shp => $dab_dat) {
    if($data20[$shp]['online'] == 'yes') {
        $shptxt = $shp == $shp_land_page ? 'LandingPage' : $shp;
        $shop_arr[] = array('id' => $shp, 'text' => $shptxt);
    } else {
        unset($data20[$shp]);
    }
}
$shp_exclude_arr = array('katalog_shop_test', 'oem-kataloge', 'oem-catalogs', 'catalogo-autoricambi', 'catalogue-pieces-automobiles', 'catalogos-refacciones');

//write id of available languages into array
$available_langs = array();
if (!isset($lng) || (isset($lng) && !is_object($lng))) {
    require_once(DIR_WS_CLASSES . 'language.php');
    $lng = new language;
}
foreach($lng->catalog_languages as $lng_cd => $lng_arr) {
    $available_langs[] = $lng_arr['id'];
}

//helper to see output
//echo '<br /><br /><br /><br /><br /><br />';

//echo '<pre>'.print_r($shop_arr, true).'</pre>';

//BOC get allowed contents to copy
$db_data_ccont = ${'ccdb_'.strtolower($cat_env)}['central_customers_db'];

$db_cent_ccont = mysqli_connect($db_data_ccont['sql_server'], $db_data_ccont['sql_user'], $db_data_ccont['sql_pass'], $db_data_ccont['sql_db']);
if(!$db_cent_ccont) $alert .= 'Verbindung zu zentraler Kunden-DB schlug fehl: ' . mysqli_error($db_cent_ccont);
if (!mysqli_select_db($db_cent_ccont, $db_data_ccont['sql_db'])) die('Konnte Datenbank fuer zentrale Kunden-DB nicht selektieren');
mysqli_set_charset($db_cent_ccont, 'utf8');
        
$get_allowed_cc_qu = mysqli_query($db_cent_ccont, "SELECT copy_allowed_contents FROM copy_allowed_contents");
if(!$get_allowed_cc_qu) $alert .= 'Abfragefehler: ' . mysqli_error($db_cent_ccont);
$allowed_cc_arr = xtc_db_fetch_array($get_allowed_cc_qu);
$allowed_cc = explode(',', $allowed_cc_arr['copy_allowed_contents']); //added
$allowed_cc = array_map('trim', $allowed_cc);
//echo '<br /><br /><br /><br /><br /><br /><pre>'.print_r($allowed_cc, true).'</pre>';
//EOC get allowed contents to copy

//BOC copy to all shops
$post_fr_wh = (isset($_POST['cont_grps_from_where']) && $_POST['cont_grps_from_where'] != '0') ? $_POST['cont_grps_from_where'] : '';
$post_fgrp = (isset($_POST['from_grp']) && $_POST['from_grp'] != '') ? (int)$_POST['from_grp'] : '';
$post_tgrp = (isset($_POST['to_grp']) && $_POST['to_grp'] != '') ? (int)$_POST['to_grp'] : '';
//EOC copy to all shops

//BOC copy to single shop
$post_fr_wh_singl = (isset($_POST['cont_grps_from_where_singl']) && $_POST['cont_grps_from_where_singl'] != '0') ? $_POST['cont_grps_from_where_singl'] : '';
$post_to_wh_singl = (isset($_POST['cont_grps_to_where_singl']) && $_POST['cont_grps_to_where_singl'] != '0') ? $_POST['cont_grps_to_where_singl'] : '';
$post_fgrp_singl = (isset($_POST['from_grp_singl']) && $_POST['from_grp_singl'] != '') ? (int)$_POST['from_grp_singl'] : '';
$post_tgrp_singl = (isset($_POST['to_grp_singl']) && $_POST['to_grp_singl'] != '') ? (int)$_POST['to_grp_singl'] : '';
//EOC copy to single shop

//BOC save allowed contents
if(isset($_POST['allowed_copy_conts']) && $_POST['allowed_copy_conts'] != '') {
    $post_allowed_copy_conts = xtc_db_input($_POST['allowed_copy_conts']);
    $post_allowed_copy_conts = trim($post_allowed_copy_conts, ',');
    $allwd_ins_qu = "UPDATE copy_allowed_contents SET copy_allowed_contents = '".$post_allowed_copy_conts."'";
    if(mysqli_query($db_cent_ccont, $allwd_ins_qu)) {
        $out_allowed .= '<span style="color:#046200;">Die erlaubten Contens wurden gespeichert</span>';
    } else {
        $out_allowed .= 'Abfragefehler: ' . mysqli_error($db_cent_ccont);
    }
}
//EOC save allowed contents

//BOC copy to all shops
if($post_fr_wh != '' && $post_fgrp != '' && $post_tgrp != '') {
    $c_from_link = mysqli_connect($data20[$post_fr_wh]['sql_server'], $data20[$post_fr_wh]['sql_user'], $data20[$post_fr_wh]['sql_pass'], $data20[$post_fr_wh]['sql_db']);
    if (!$c_from_link) $out .= 'Verbindung zu "'.$shp.'" schlug fehl: ' . mysqli_error($c_from_link);

    if (!mysqli_select_db($c_from_link, $data20[$post_fr_wh]['sql_db'])) die('Konnte Datenbank fuer "'.$shp.'" nicht selektieren');
    mysqli_set_charset($c_from_link, 'utf8');

    $from_qu = mysqli_query($c_from_link, "SELECT * FROM content_manager WHERE content_group BETWEEN ".$post_fgrp." AND ".$post_tgrp);
    if (!$from_qu) $out .= 'Abfragefehler: ' . mysqli_error($c_from_link);

    while($from_arr = mysqli_fetch_array($from_qu, MYSQLI_ASSOC)) {
        if(in_array($from_arr['languages_id'], $available_langs)) {
            //BOC store parent group and language since content_id will be different in target shop
            if($from_arr['parent_id'] > 0) {
                $par_grp_lng_qu_str = "SELECT content_group, languages_id FROM content_manager WHERE content_id = ".(int)$from_arr['parent_id'];
                $par_grp_lng_qu = mysqli_query($c_from_link, $par_grp_lng_qu_str);
                $par_grp_lng_arr = mysqli_fetch_array($par_grp_lng_qu, MYSQLI_ASSOC);
                $from_arr['parent_id'] = $par_grp_lng_arr['content_group'].'999'.$par_grp_lng_arr['languages_id'];
            } else {
                $from_arr['parent_id'] = $from_arr['parent_id'];
            }
            //EOC store parent group and language since content_id will be different in target shop
            
            unset($from_arr['content_id']); //unset content_id
            $cop_arr[$from_arr['content_group']][$from_arr['languages_id']] = $from_arr;
        }
    }
    //echo '<pre>$cop_arr: '.print_r($cop_arr, true).'</pre>';

    mysqli_close($c_from_link);
    
    //BOC take off not allowed goups from array
    $selected_grps = array();
    foreach($cop_arr as $grp => $val) {
        $selected_grps[] = $grp;
    }
    $no_copy_array = array_diff($selected_grps, $allowed_cc);
    //echo '<br /><br /><br /><br /><br /><br /><pre>'.print_r($no_copy_array, true).'</pre>';
    //EOC take off not allowed goups from array

    $not_allowed_grps_error = false;
    $nocont_error = false;
    $wrong_grps_arr = array();
    
    if(!empty($cop_arr)) {
        foreach($cop_arr as $grp => $lng_arr) {
            if(!in_array($grp, $no_copy_array)) {
                foreach($lng_arr as $lngid => $val) {
                    $ins_str = "('',";
                    $upd_str = '';
                    
                    foreach($val as $k => $v){
                        $ins_str .= "'".addslashes($v)."'".',';
                        $upd_str .= $k.'='."'".addslashes($v)."'".',';
                    }
                    
                    $ins_str = rtrim($ins_str, ',');
                    $ins_str .= ')';
                    $upd_str = rtrim($upd_str, ',');
                    $upd_str .= " WHERE content_group = '".$grp."' AND languages_id = '".$lngid."'";
                
                    //echo '<pre>$ins_str: '.$ins_str.'</pre>';
                    //echo '<pre>$upd_str: '.$upd_str.'</pre>';
                    
                    $inst_updt_qu_arr[$grp][$lngid] = array($ins_str, $upd_str);
                }
            }  else {
                if($post_fgrp == $post_tgrp) {
                    $not_allowed_grps_error = true;
                    //$wrong_grps_arr[] = $grp;
                    $wrong_grp = $grp;
                }
            }
        }
        //echo '<pre>$inst_updt_qu_arr: '.print_r($inst_updt_qu_arr, true).'</pre>';
    } else {
        $nocont_error = true;
    }
    
    unset($data20[$_POST['cont_grps_from_where']]); //unset the shop from which to copy

    if($not_allowed_grps_error === false && $nocont_error === false) {
        //BOC possibillity to exclude landing page, 05-2021, noRiddle
        if(isset($_POST['excl_landingpage']) && $_POST['excl_landingpage'] == '1') {
            $shp_exclude_arr[] = $shp_land_page;
        }
        //EOC possibillity to exclude landing page, 05-2021, noRiddle
        foreach($data20 as $shp => $db_dat) {
            if(!in_array($shp, $shp_exclude_arr)) { //exclude catalogs with copying into all shops
                $c_to_link = mysqli_connect($db_dat['sql_server'], $db_dat['sql_user'], $db_dat['sql_pass'], $db_dat['sql_db']);
                if (!$c_to_link) $out .= 'Verbindung zu "'.$shp.'" schlug fehl: ' . mysqli_error($c_to_link);

                if (!mysqli_select_db($c_to_link, $db_dat['sql_db'])) die('Konnte Datenbank fuer "'.$shp.'" nicht selektieren');
                mysqli_set_charset($c_to_link, 'utf8');
                
                foreach($inst_updt_qu_arr as $group => $lid) {
                    foreach($lid as $l_id => $arr) {
                        $to_qu = mysqli_query($c_to_link, "SELECT content_id FROM content_manager WHERE content_group = ".$group." AND languages_id = '".$l_id."'");
                        if (!$to_qu) $out .= 'Abfragefehler: ' . mysqli_error($c_to_link);

                        if(mysqli_num_rows($to_qu)) {
                            $update_qu = 'UPDATE content_manager SET '.$arr[1];
                            //echo '<pre>'.$shp.'-$update_qu: '.$update_qu.'</pre><hr />';

                            $upd_qu = mysqli_query($c_to_link, $update_qu);
                            if(!$upd_qu) {
                                $out .= 'Update des Contents '.$group.' von '.$shp.' Sprache '.$l_id.' schlug fehl:<br />' . mysqli_error($c_to_link);
                            } else {
                                $out .= '<span style="color:#046200;">Kopie des Contents '.$group.' in '.$shp.' Sprache '.$l_id.': okay</span><br />';
                            }
                        } else {
                            $insert_qu = 'INSERT INTO content_manager VALUES'.$arr[0];
                            //echo '<pre>'.$shp.'-$insert_qu: '.$insert_qu.'</pre><hr />';
                            
                            $ins_qu = mysqli_query($c_to_link, $insert_qu);
                            if(!$ins_qu) {
                                $out .= 'Insert des Contents '.$group.' von '.$shp.' Sprache '.$l_id.' schlug fehl:<br />' . mysqli_error($c_to_link);
                            } else {
                                $out .= '<span style="color:#046200;">Kopie des Contents '.$group.' in '.$shp.' Sprache '.$l_id.': okay</span><br />';
                            }
                        }
                    }
                }
                
                //BOC fix parent_ids
                $fix_parent_id_qu_str = "SELECT content_id, parent_id FROM content_manager WHERE parent_id LIKE '%9992' OR parent_id LIKE '%9991'";
                $fix_parent_id_qu = mysqli_query($c_to_link, $fix_parent_id_qu_str);
                if(mysqli_num_rows($fix_parent_id_qu)) {
                    while($fix_parent_id_arr = mysqli_fetch_array($fix_parent_id_qu, MYSQLI_ASSOC)) {
                        $par_id_arr = explode('999', $fix_parent_id_arr['parent_id']);
                        $fix_parid_upd_qu = "UPDATE content_manager cm
                                         INNER JOIN content_manager cm2
                                                 ON cm2.content_group = '".(int)$par_id_arr[0]."'
                                                AND cm2.languages_id = '".(int)$par_id_arr[1]."'
                                                SET cm.parent_id = cm2.content_id
                                              WHERE cm.content_id = '".(int)$fix_parent_id_arr['content_id']."'";

                        mysqli_query($c_to_link, $fix_parid_upd_qu);
                    }
                }
                //EOC fix parent_ids

                mysqli_close($c_to_link);
                
                //delete DB cache if activated
                if(defined('DB_CACHE') && DB_CACHE == 'true') {
                    clear_dir($path.$shp.'/cache/'); //$path coming from central config.php
                }
            }
        }
    } else {
        if($not_allowed_grps_error === true) {
            $out_allowed .= 'Es wurde ein unerlaubter Content angegeben, unerlaubte Gruppe: '.$wrong_grp;
        } else if($nocont_error === true) {
            $out .= 'Es gibt keinen Content in der ausgewählten Range im Ausgangs-Shop '.$post_fr_wh;
        }
    }
} else {
    if(isset($_POST['cont_grps_from_where']) && ($_POST['cont_grps_from_where'] == '0' || $_POST['from_grp'] == '' || $_POST['to_grp'] == '')) {
        $ellol = 'Bitte alle Felder ausfuellen !';
    }
}
//EOC copy to all shops

//BOC copy to single shop
if($post_fr_wh_singl != '' && $post_to_wh_singl != '' && $post_fgrp_singl != '' && $post_tgrp_singl != '') {
    if($post_fr_wh_singl != $post_to_wh_singl) {
        $c_from_link = mysqli_connect($data20[$post_fr_wh_singl]['sql_server'], $data20[$post_fr_wh_singl]['sql_user'], $data20[$post_fr_wh_singl]['sql_pass'], $data20[$post_fr_wh_singl]['sql_db']);
        if (!$c_from_link) $out_singl .= 'Verbindung zu "'.$post_fr_wh_singl.'" schlug fehl: ' . mysqli_error($c_from_link);

        if (!mysqli_select_db($c_from_link, $data20[$post_fr_wh_singl]['sql_db'])) die('Konnte Datenbank fuer "'.$post_fr_wh_singl.'" nicht selektieren');
        mysqli_set_charset($c_from_link, 'utf8');

        $from_qu = mysqli_query($c_from_link, "SELECT * FROM content_manager WHERE content_group BETWEEN ".$post_fgrp_singl." AND ".$post_tgrp_singl);
        if (!$from_qu) $out_singl .= 'Abfragefehler: ' . mysqli_error($c_from_link);

        while($from_arr = mysqli_fetch_array($from_qu, MYSQLI_ASSOC)) {
            if(in_array($from_arr['languages_id'], $available_langs)) {
                //BOC store parent group and language since content_id will be different in target shop
                if($from_arr['parent_id'] > 0) {
                    $par_grp_lng_qu_str = "SELECT content_group, languages_id FROM content_manager WHERE content_id = ".(int)$from_arr['parent_id'];
                    $par_grp_lng_qu = mysqli_query($c_from_link, $par_grp_lng_qu_str);
                    $par_grp_lng_arr = mysqli_fetch_array($par_grp_lng_qu, MYSQLI_ASSOC);
                    $from_arr['parent_id'] = $par_grp_lng_arr['content_group'].'999'.$par_grp_lng_arr['languages_id'];
                } else {
                    $from_arr['parent_id'] = $from_arr['parent_id'];
                }
                //EOC store parent group and language since content_id will be different in target shop
                
                unset($from_arr['content_id']); //unset content_id
                $cop_arr[$from_arr['content_group']][$from_arr['languages_id']] = $from_arr;
            }
        }
        //echo '<br><br><br><br><pre>$cop_arr: '.print_r($cop_arr, true).'</pre>';
        //echo '<pre>$cop_arr: '.print_r($par_ids_arr, true).'</pre>';
    
        mysqli_close($c_from_link);
        
        //BOC take off not allowed goups from array
        $selected_grps = array();
        foreach($cop_arr as $group => $c_arr) {
            $selected_grps[] = $group;
        }
        $no_copy_array = array_diff($selected_grps, $allowed_cc);
        //echo '<br /><br /><br /><br /><pre>'.print_r($no_copy_array, true).'</pre>';
        //EOC take off not allowed goups from array
        
        $not_allowed_grps_error = false;
        $nocont_error = false;
        $wrong_grps_arr = array();
        
        if(!empty($cop_arr)) {
            foreach($cop_arr as $grp => $lng_arr) {
                if(!in_array($grp, $no_copy_array)) {
                    foreach($lng_arr as $lngid => $val) {
                        $ins_str = "('',";
                        $upd_str = '';
                    
                        foreach($val as $k => $v){
                            $ins_str .= "'".addslashes($v)."'".',';
                            $upd_str .= $k.'='."'".addslashes($v)."'".',';
                        }
                    
                        $ins_str = rtrim($ins_str, ',');
                        $ins_str .= ')';
                        $upd_str = rtrim($upd_str, ',');
                        $upd_str .= " WHERE content_group = '".$grp."' AND languages_id = '".$lngid."'";
                
                        //echo '<pre>$ins_str: '.$ins_str.'</pre>';
                        //echo '<pre>$upd_str: '.$upd_str.'</pre>';
                    
                        $inst_updt_qu_arr[$grp][$lngid] = array($ins_str, $upd_str);
                    }
                } else {
                    if($post_fgrp_singl == $post_tgrp_singl) {
                        $not_allowed_grps_error = true;
                        //$wrong_grps_arr[] = $grp;
                        $wrong_grp = $grp;
                    }
                }
            }
            //echo '<pre>$inst_updt_qu_arr: '.print_r($inst_updt_qu_arr, true).'</pre>';
        } else {
            $nocont_error = true;
        }
        
        $c_to_link = mysqli_connect($data20[$post_to_wh_singl]['sql_server'], $data20[$post_to_wh_singl]['sql_user'], $data20[$post_to_wh_singl]['sql_pass'], $data20[$post_to_wh_singl]['sql_db']);
        if (!$c_to_link) $out_singl .= 'Verbindung zu "'.$post_to_wh_singl.'" schlug fehl: ' . mysqli_error($c_to_link);

        if (!mysqli_select_db($c_to_link, $data20[$post_to_wh_singl]['sql_db'])) die('Konnte Datenbank fuer "'.$post_to_wh_singl.'" nicht selektieren');
        mysqli_set_charset($c_to_link, 'utf8');
        
        if($not_allowed_grps_error === false && $nocont_error === false) {
            foreach($inst_updt_qu_arr as $group => $lid) {
                foreach($lid as $l_id => $arr) {
                    $to_qu = mysqli_query($c_to_link, "SELECT content_id FROM content_manager WHERE content_group = ".$group." AND languages_id = '".$l_id."'");
                    if (!$to_qu) $out_singl .= 'Abfragefehler: ' . mysqli_error($c_to_link);

                    if(mysqli_num_rows($to_qu)) {
                        $update_qu = 'UPDATE content_manager SET '.$arr[1];
                        //echo '<pre>'.$post_to_wh_singl.'-$update_qu: '.$update_qu.'</pre><hr />';
                        
                        $upd_qu = mysqli_query($c_to_link, $update_qu);
                        if(!$upd_qu) {
                            $out_singl .= 'Update des Contents '.$group.' von '.$post_to_wh_singl.' Sprache '.$l_id.' schlug fehl:<br />' . mysqli_error($c_to_link);
                        } else {
                            $out_singl .= '<span style="color:#046200;">Kopie des Contents '.$group.' in '.$post_to_wh_singl.' Sprache '.$l_id.': okay</span><br />';
                        }
                    } else {
                        $insert_qu = 'INSERT INTO content_manager VALUES'.$arr[0];
                        //echo '<pre>'.$post_to_wh_singl.'-$insert_qu: '.$insert_qu.'</pre><hr />';
                        
                        $ins_qu = mysqli_query($c_to_link, $insert_qu);
                        if(!$ins_qu) {
                            $out_singl .= 'Insert des Contents '.$group.' von '.$post_to_wh_singl.' Sprache '.$l_id.' schlug fehl:<br />' . mysqli_error($c_to_link);
                        } else {
                            $out_singl .= '<span style="color:#046200;">Kopie des Contents '.$group.' in '.$post_to_wh_singl.' Sprache '.$l_id.': okay</span><br />';
                        }
                    }
                }
            }
        } else {
            if($not_allowed_grps_error === true) {;
                $out_allowed .= 'Es wurde ein unerlaubter Content angegeben, unerlaubte Gruppe: '.$wrong_grp;
            } else if($nocont_error === true) {
                $out_singl .= 'Es gibt keinen Content in der ausgewählten Range im Ausgangs-Shop '.$post_fr_wh_singl;
            }
        }
        
        //BOC fix parent_ids
        $fix_parent_id_qu_str = "SELECT content_id, parent_id FROM content_manager WHERE parent_id LIKE '%9992' OR parent_id LIKE '%9991'";
        $fix_parent_id_qu = mysqli_query($c_to_link, $fix_parent_id_qu_str);
        if(mysqli_num_rows($fix_parent_id_qu)) {
            while($fix_parent_id_arr = mysqli_fetch_array($fix_parent_id_qu, MYSQLI_ASSOC)) {
                $par_id_arr = explode('999', $fix_parent_id_arr['parent_id']);
                $fix_parid_upd_qu = "UPDATE content_manager cm
                                 INNER JOIN content_manager cm2
                                         ON cm2.content_group = '".(int)$par_id_arr[0]."'
                                        AND cm2.languages_id = '".(int)$par_id_arr[1]."'
                                        SET cm.parent_id = cm2.content_id
                                      WHERE cm.content_id = '".(int)$fix_parent_id_arr['content_id']."'";

                mysqli_query($c_to_link, $fix_parid_upd_qu);
            }
        }
        //EOC fix parent_ids
        
        mysqli_close($c_to_link);
        
        //delete DB cache if activated
        if(defined('DB_CACHE') && DB_CACHE == 'true') {
            clear_dir($path.$post_to_wh_singl.'/cache/'); //$path coming from central config.php
        }
    } else {
        $out_singl .= 'Es kann nicht in denselben Shop kopiert werden welcher als Vorlage gewählt wurde. Bitte als Ziel einen anderen Shop wählen.';
    }
} else {
    if(isset($_POST['cont_grps_from_where_singl']) && ($_POST['cont_grps_from_where_singl'] == '0' || $_POST['cont_grps_to_where_singl'] == '0' || $_POST['from_grp_singl'] == '' || $_POST['to_grp_singl'] == '')) {
        $ellol_singl = 'Bitte alle Felder ausfuellen !';
    }
}
//EOC copy to single shop

//close central DB for allowed contents to copy
mysqli_close($db_cent_ccont);
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id: upse.php 4200 2013-01-10 19:47:11Z Tomcraft1980 $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce( fedexeu.php,v 1.01 2003/02/18 03:25:00); www.oscommerce.com 
   (c) 2003	 nextcommerce (fedexeu.php,v 1.5 2003/08/1); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   fedex_europe_1.02        	Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/



define('MODULE_SHIPPING_UPSE_TEXT_TITLE', 'United Parcel Service Express');
define('MODULE_SHIPPING_UPSE_TEXT_DESCRIPTION', 'United Parcel Service Express - módulo de envío');
define('MODULE_SHIPPING_UPSE_TEXT_WAY', ' Envíos a ');
define('MODULE_SHIPPING_UPSE_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_UPSE_INVALID_ZONE', 'No es posible realizar un envío a este país.');
define('MODULE_SHIPPING_UPSE_UNDEFINED_RATE', 'Los costos de envío no se pueden calcular por el momento.');

define('MODULE_SHIPPING_UPSE_STATUS_TITLE' , 'UPS Express');
define('MODULE_SHIPPING_UPSE_STATUS_DESC' , '¿Desea ofrecer envíos a través de UPS Express?');
define('MODULE_SHIPPING_UPSE_HANDLING_TITLE' , 'Recargo');
define('MODULE_SHIPPING_UPSE_HANDLING_DESC' , 'Recargo de procesamiento para este modo de envío en Euro');
define('MODULE_SHIPPING_UPSE_TAX_CLASS_TITLE' , 'Tasa de impuesto');
define('MODULE_SHIPPING_UPSE_TAX_CLASS_DESC' , 'Seleccione el tipo de IVA para este modo de envío.');
define('MODULE_SHIPPING_UPSE_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_UPSE_ZONE_DESC' , 'Si selecciona una zona, este modo de envío sólo se ofrecerá en esa zona.');
define('MODULE_SHIPPING_UPSE_SORT_ORDER_TITLE' , 'Orden de la visualización');
define('MODULE_SHIPPING_UPSE_SORT_ORDER_DESC' , 'Menor se muestra en primer lugar.');
define('MODULE_SHIPPING_UPSE_ALLOWED_TITLE' , 'Zonas de envío individuales');
define('MODULE_SHIPPING_UPSE_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE');




/* UPS Express

*/

define('MODULE_SHIPPING_UPSE_COUNTRIES_1_TITLE' , 'Estados para UPS Express Zona 1');
define('MODULE_SHIPPING_UPSE_COUNTRIES_1_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 1 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_1_TITLE' , 'Tarifas para UPS Express Zona 1');
define('MODULE_SHIPPING_UPSE_COST_1_DESC' , 'Costos de envío basados en el peso dentro de la zona 1. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 22,70 = 0.5:22.7,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_2_TITLE' , 'Estados para UPS Express Zona 2');
define('MODULE_SHIPPING_UPSE_COUNTRIES_2_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 2 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_2_TITLE' , 'Tarifas para UPS Express Zona 2');
define('MODULE_SHIPPING_UPSE_COST_2_DESC' , 'Costos de envío basados en el peso dentro de la zona 2. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 51,55 = 0.5:51.55,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_3_TITLE' , 'Estados para UPS Express Zona 3');
define('MODULE_SHIPPING_UPSE_COUNTRIES_3_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 3 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_3_TITLE' , 'Tarifas para UPS Express Zona 3');
define('MODULE_SHIPPING_UPSE_COST_3_DESC' , 'Costos de envío basados en el peso dentro de la zona 3. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 60,70 = 0.5:60.70,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_4_TITLE' , 'Estados para UPS Express Zona 4');
define('MODULE_SHIPPING_UPSE_COUNTRIES_4_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 4 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_4_TITLE' , 'Tarifas para UPS Express Zona 4');
define('MODULE_SHIPPING_UPSE_COST_4_DESC' , 'Costos de envío basados en el peso dentro de la zona 4. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 66,90 = 0.5:66.90,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_5_TITLE' , 'Estados para UPS Express Zona 41');
define('MODULE_SHIPPING_UPSE_COUNTRIES_5_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 41 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_5_TITLE' , 'Tarifas para UPS Express Zona 41');
define('MODULE_SHIPPING_UPSE_COST_5_DESC' , 'Costos de envío basados en el peso dentro de la zona 41. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 82,10 = 0.5:82.10,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_6_TITLE' , 'Estados para UPS Express Zona 42');
define('MODULE_SHIPPING_UPSE_COUNTRIES_6_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 42 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_6_TITLE' , 'Tarifas para UPS Express Zona 42');
define('MODULE_SHIPPING_UPSE_COST_6_DESC' , 'Costos de envío basados en el peso dentro de la zona 42. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 82,90 = 0.5:82.90,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_7_TITLE' , 'Estados para UPS Express Zona 5');
define('MODULE_SHIPPING_UPSE_COUNTRIES_7_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 5 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_7_TITLE' , 'Tarifas para UPS Express Zona 5');
define('MODULE_SHIPPING_UPSE_COST_7_DESC' , 'Costos de envío basados en el peso dentro de la zona 5. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 59,00 = 0.5:59.00,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_8_TITLE' , 'Estados para UPS Express Zona 6');
define('MODULE_SHIPPING_UPSE_COUNTRIES_8_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 6 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_8_TITLE' , 'Tarifas para UPS Express Zona 6');
define('MODULE_SHIPPING_UPSE_COST_8_DESC' , 'Costos de envío basados en el peso dentro de la zona 6. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 84,50 = 0.5:84.50,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_9_TITLE' , 'Estados para UPS Express Zona 7');
define('MODULE_SHIPPING_UPSE_COUNTRIES_9_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 7 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_9_TITLE' , 'Tarifas para UPS Express Zona 7');
define('MODULE_SHIPPING_UPSE_COST_9_DESC' , 'Costos de envío basados en el peso dentro de la zona 7. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 71,85 = 0.5:71.85,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_10_TITLE' , 'Estados para UPS Express Zona 8');
define('MODULE_SHIPPING_UPSE_COUNTRIES_10_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 8 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_10_TITLE' , 'Tarifas para UPS Express Zona 8');
define('MODULE_SHIPPING_UPSE_COST_10_DESC' , 'Costos de envío basados en el peso dentro de la zona 8. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 80,05 = 0.5:80.05,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_11_TITLE' , 'Estados para UPS Express Zona 9');
define('MODULE_SHIPPING_UPSE_COUNTRIES_11_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 9 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_11_TITLE' , 'Tarifas para UPS Express Zona 9');
define('MODULE_SHIPPING_UPSE_COST_11_DESC' , 'Costos de envío basados en el peso dentro de la zona 9. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 85,20 = 0.5:85.20,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_12_TITLE' , 'Estados para UPS Express Zona 10');
define('MODULE_SHIPPING_UPSE_COUNTRIES_12_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 10 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_12_TITLE' , 'Tarifas para UPS Express Zona10');
define('MODULE_SHIPPING_UPSE_COST_12_DESC' , 'Costos de envío basados en el peso dentro de la zona 10. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 93,10 = 0.5:93.10,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_13_TITLE' , 'Estados para UPS Express Zona 11');
define('MODULE_SHIPPING_UPSE_COUNTRIES_13_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 11 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_13_TITLE' , 'Tarifas para UPS Express Zona 11');
define('MODULE_SHIPPING_UPSE_COST_13_DESC' , 'Costos de envío basados en el peso dentro de la zona 11. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 103,50 = 0.5:103.50,...');

define('MODULE_SHIPPING_UPSE_COUNTRIES_14_TITLE' , 'Estados para UPS Express Zona 12');
define('MODULE_SHIPPING_UPSE_COUNTRIES_14_DESC' , 'Abreviaturas ISO de los estados separados por comas para la Zona 12 (introduzca WORLD para el resto del mundo.):');
define('MODULE_SHIPPING_UPSE_COST_14_TITLE' , 'Tarifas para UPS Express Zona 12');
define('MODULE_SHIPPING_UPSE_COST_14_DESC' , 'Costos de envío basados en el peso dentro de la zona 12. Ejemplo: Envío entre 0 y 0,5kg cuesta EUR 105,20 = 0.5:105.20,...');
?>

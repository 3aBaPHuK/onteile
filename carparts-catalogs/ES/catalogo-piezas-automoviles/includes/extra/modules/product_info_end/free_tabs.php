<?php
/******************************************************
* file: free_tabs.php
* use: generate any number of new tabs in product_info
*
* (c) noRiddle 02-2018
******************************************************/

//BOC freely_configurable_tabs, 2011, noRiddle
$prod_desc = html_entity_decode(stripslashes($product->data['products_description']), ENT_COMPAT, strtoupper($_SESSION['language_charset']));	
$tab_title = array();
preg_match_all('#<h3 class="desc-tabs">([A-Za-zäöüÄÖÜßàáèéëîìíôçñ0-9_\. \-\?\':!]*)</h3>#', $prod_desc, $tab_title);
$tab_cont = preg_split('#<h3 class="desc-tabs">[A-Za-zäöüÄÖÜßàáèéëîìíôçñ0-9_\. \-\?\':!]*</h3>#', $prod_desc, -1, PREG_SPLIT_NO_EMPTY);

$tabs_data = array();

if(count($tab_cont) > 1) {
	$prod_desc = array_shift($tab_cont);
    for($tc = 0; $tc < count($tab_cont); $tc++) {
        $title_id = $tc;
        $title = $tab_title[1][$tc];
        //$tabs_data[] = array('ID' => $title_id, 'TITLE' => $title, 'CONTENT' => $tab_cont[$tc]);
        $tabs_data[] = array('TITLE' => $title, 'CONTENT' => $tab_cont[$tc]);
    }
}

$info_smarty->assign('PRODUCTS_DESCRIPTION', $prod_desc);
if(!empty($tabs_data)) {
    $info_smarty->assign('TABS', $tabs_data);
}
//EOC freely_configurable_tabs
?>
<?php

 define('EMAIL_COMPANY', 'Company: ');
 define('EMAIL_STREET', 'Street: ');
 define('EMAIL_POSTCODE', 'Postcode: ');
 define('EMAIL_CITY', 'City: ');
 define('EMAIL_PHONE', 'Telephone: ');
 define('EMAIL_FAX', 'Fax: ');
 define('EMAIL_SENT_BY', 'Sent by %s %s at %s to %s clock');
 //define('EMAIL_NOTIFY', 'Attention, this e-mail can NOT be answered with -ANSWER THE SENDER-!'); // Not used yet
 
 //default fields
 define('EMAIL_NAME', 'Name: ');
 define('EMAIL_EMAIL', 'E-mail: ');
 define('EMAIL_MESSAGE', 'Message: ');
 
 //contact-form error messages
 //BOC nicer error output, noRiddle
 //define('ERROR_EMAIL','<p><b>Your e-mail address:</b> None or invalid input!</p>'); // already defined in english.php
 //define('ERROR_VVCODE','<p><b>Security code:</b> No match, please enter your security code again!</p>'); // already defined in english.php
 //define('ERROR_MSG_BODY','<p><b>Your message:</b> No input!</p>'); // already defined in english.php
 define('ERROR_EMAIL','<li><b>Your e-mail address:</b> None or invalid input!</li>');
 define('ERROR_VVCODE','<li><b>Security code:</b> No match, please enter your security code again!</li>');
 define('ERROR_MSG_BODY','<li><b>Your message:</b> No input!</li>');
 //EOC nicer error output, noRiddle
 
 //BOC new constants for VIN, price question and file upload, noRiddle
 define('AH_MAIL_PRICE_QUESTION_SUBJ', 'Price question for product:');
 define('CONTACT_UPLOAD_TEXT', 'Allowed are %s with a size of maximum %s.<br />No white space characters are allowed within the filename.');
 define('CONTACT_ERROR_FILE_SIZE', '<li><b>Upload file:</b> File to big. Allowed is %s.</li>');
 define('CONTACT_ERROR_FILE_EXT', '<li><b>Upload file:</b> File extension not allowed. Allowed are %s.</li>');
 define('CONTACT_ERROR_FILE_LENGTH_AND_CHARS', '<li><b>Upload file:</b> Filename is too long (allowed are maximum %d characters plus file extension)<br />or not allowed characters where found within the filename.<br />Allowed are a-z, A-Z, 0-9 underscore and hyphen.</li>');
 //EOC new constants for VIN, price question and file upload, noRiddle
?>
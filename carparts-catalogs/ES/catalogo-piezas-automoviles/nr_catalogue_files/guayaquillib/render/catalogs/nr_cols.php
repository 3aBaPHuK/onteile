<?php
/*********************************
* new template file nr_cols.php
* (c) noRiddle 07-2016
*********************************/

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'template.php';

class GuayaquilCatalogsList extends GuayaquilTemplate
{
	var $iconsFolder, $columns, $excl_arr, $rem_url, $cat_env, $catalogs = NULL;

	function __construct(IGuayaquilExtender $extender) {
		parent::__construct($extender);
        global $remote_url, $cat_env;

		$this->iconsFolder = 'images/75x75/';
        $this->columns = array('icon', 'name', 'version');
        //define exclude array for not to be displayed brands, noRiddle
        $this->excl_arr = array(); //array('subaru japan', 'subaru usa', 'zaz');
        $this->iconsFolder = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->iconsFolder).'/';
        $this->remote_url = $remote_url;
        $this->cat_env = $cat_env;
	}

	function Draw($catalogs) {
        $html = '<div id="main-brands"'.(!isset($_SESSION['customer_id']) ? ' class="n-logg-i"' : '').'><ul>';
//echo '<pre>'.print_r($catalogs, true).'</pre>';
        $new_arr = array(); //define new array to build to sort as we like later, noRiddle
		foreach ($catalogs->row as $row) {
            //BOC  put all in array to sort later, noRiddle
            $ctlg_name = (string)$row->attributes()->name; //strpos(strtolower((string)$row->attributes()->name), 'opel') !== false ? 'Vauxhall' : (string)$row->attributes()->name; //fix name with Opel/Vauxhall on .co.uk, noRiddle
            $new_arr[$ctlg_name] = $row;
            
            //$link = $this->FormatLink('catalog', $row, (string)$row->code); //original code, noRiddle
            
            //$html .= $this->DrawRow($row, $link);  //original code, noRiddle
            //EOC  put all in array to sort later, noRiddle
        }

        ksort($new_arr);
//echo '<pre>'.print_r($new_arr, true).'</pre>';
        foreach($new_arr as $brd => $row) {
//echo '<pre>'.print_r($row, true).'</pre>';
//echo '<p>'.$brd.'</p>';
            if($brd != 'Dacia') { //we want to map dacia to renault because the models sold in Europe are in the Renault rubrique, noRiddle
                $link = $this->FormatLink('catalog', $row, (string)$row->code);
            } else {
                $link = $this->FormatLink('catalog', $new_arr['Renault/Dacia'], (string)$new_arr['Renault/Dacia']->code);
            }
            $html .= $this->DrawRow($row, $link);
        }
        
        $html .= '</ul></div>';

		return $html;
        //return '<pre>'.print_r($catalogs, true).'</pre>';
        //return '<pre>'.print_r($new_arr, true).'</pre>';
	}

	function DrawRow($catalog, $link) {
        //BOC exclude subaru japan and subaru usa, noRiddle
        //if(strtolower($catalog['name']) != 'subaru japan' && strtolower($catalog['name']) != 'subaru usa') {
        if(!in_array(strtolower($catalog['name']), $this->excl_arr) && !in_array(strtolower($catalog['name']), $this->remote_url[$this->cat_env]['disallow_brands'])) { //exclude some brands 8see central config), 11-2020, noRiddle
            //$html = '<li onmouseout="this.className=\'\';" onmouseover="this.className=\'over\';" onclick="window.location=\''.$link.'\'">';
            $html = '<li>'; //we don't need JS here, create a real link, noRiddle
            foreach ($this->columns as $column)
                $html .= $this->DrawCell($catalog, strtolower($column), $link);

            $html .= '</li>';
        }
        //EOC exclude subaru japan and subaru usa, noRiddle
        
		return $html;
	}

    function DrawCell($catalog, $column, $link) {
        return $this->DrawCellValue($catalog, $column, $link);
    }
    
    function DrawCellValue($catalog, $column, $link) {
        switch ($column) {
			case 'icon':
				//BOC exclude subaru japan and subaru usa, noRiddle
                $ctlg_icon = $catalog['icon'] ; //$catalog['icon'] == 'opel.png' ? 'vauxhall.png' : $catalog['icon']; //convert opel to vauxhall, noRiddle
                //return !in_array(strtolower($catalog['name']), $this->excl_arr) ? '<div class="nr-logo '.$catalog['code'].'"><a href="'.$link.'"><img border="0" width="75" height="75" src="'.$this->iconsFolder.strtolower($catalog['icon']).'"></a></div>' : '';
                return (!in_array(strtolower($catalog['name']), $this->excl_arr)  && !in_array(strtolower($catalog['name']), $this->remote_url[$this->cat_env]['disallow_brands'])) ? '<div class="nr-logo '.$catalog['code'].'"><a href="'.$link.'"><img class="unveil" border="0" width="75" height="75" src="'.DIR_WS_BASE.'templates/'.CURRENT_TEMPLATE.'/css/images/loading.gif" data-src="'.$this->iconsFolder.strtolower($ctlg_icon).'" alt="'.$catalog['name'].'"></a></div>' : ''; //exclude some brands 8see central config), 11-2020, noRiddle
                //EOC exclude subaru japan and subaru usa, noRiddle

			case 'name':
				//BOC exclude subaro japan and subaru usa, noRiddle
                $ctlg_name = $catalog['name']; //$catalog['name'] == 'Opel/Vauxhall' ? 'Vauxhall / Opel' : str_replace('/', ' / ', $catalog['name']); //fix name with Opel/Vauxhall on .co.uk, noRiddle
                return (!in_array(strtolower($catalog['name']), $this->excl_arr) && !in_array(strtolower($catalog['name']), $this->remote_url[$this->cat_env]['disallow_brands'])) ? '<div class="nr-brandname"><a href="'.$link.'"><span>'.$ctlg_name.'</span></a></div>' : ''; //exclude some brands 8see central config), 11-2020, noRiddle
                //EOC exclude subaru japan and subaru usa, noRiddle

			case 'version':
				return '';
		}
        
		return '';
    }
}

?>
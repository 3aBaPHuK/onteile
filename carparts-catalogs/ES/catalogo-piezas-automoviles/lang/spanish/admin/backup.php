<?php
/* --------------------------------------------------------------
   $Id: backup.php 10762 2017-06-07 11:57:15Z web28 $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(backup.php,v 1.21 2002/06/15); www.oscommerce.com 
   (c) 2003	 nextcommerce (backup.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Respaldo de base de datos');

define('TABLE_HEADING_TITLE', 'Título');
define('TABLE_HEADING_FILE_DATE', 'Fecha');
define('TABLE_HEADING_FILE_SIZE', 'Tamaño');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_INFO_HEADING_NEW_BACKUP', 'Nuvevo respaldo');
define('TEXT_INFO_HEADING_RESTORE_LOCAL', 'Restaurar localmente');
define('TEXT_INFO_NEW_BACKUP', 'Por favor, NO interrumpa el proceso de respaldo. Este puede tardar unos minutos.');
define('TEXT_INFO_UNPACK', '<br /><br />(después de que fueron extraídos los ficheros del archivo)');
define('TEXT_INFO_RESTORE', 'NO interrumpa el proceso de restuaración.<br /><br />¡Mientras más grande sea el fichero de respaldo - más tiempo tardará la restuaración!<br /><br />Por favor, utilice el mysql client si es posible.<br /><br />Ejemplo:<br /><br /><b>mysql -h' . DB_SERVER . ' -u' . DB_SERVER_USERNAME . ' -p ' . DB_DATABASE . ' < %s </b> %s');
define('TEXT_INFO_RESTORE_LOCAL', 'NO interrumpa el proceso de restuaración.<br /><br />¡Mientras más grande sea el fichero de respaldo - más tiempo tardará la restuaración!');
define('TEXT_INFO_RESTORE_LOCAL_RAW_FILE', 'El fichero que se subirá tiene que ser un así llamado raw sql fichero (sólo texto).');
define('TEXT_INFO_DATE', 'Fecha:');
define('TEXT_INFO_SIZE', 'Tamaño:');
define('TEXT_INFO_COMPRESSION', 'Comprimir:');
define('TEXT_INFO_USE_GZIP', 'Con GZIP');
define('TEXT_INFO_USE_ZIP', 'Con ZIP');
define('TEXT_INFO_USE_NO_COMPRESSION', 'Sin compresión (Raw SQL)');
define('TEXT_INFO_DOWNLOAD_ONLY', 'Sólo descargar (no guardar en el servidor)');
define('TEXT_INFO_BEST_THROUGH_HTTPS', '¡Utilizar una conexión HTTPS segura!');
define('TEXT_NO_EXTENSION', 'Ninguna');
define('TEXT_BACKUP_DIRECTORY', 'Directorio de respaldo:');
define('TEXT_LAST_RESTORATION', 'Última restauración:');
define('TEXT_FORGET', '(<u>olvidado</u>)');
define('TEXT_DELETE_INTRO', '¿Está seguro de que quiere borrar este respaldo?');

define('ERROR_BACKUP_DIRECTORY_DOES_NOT_EXIST', 'Error: Este directorio de respaldo es inexistente.');
define('ERROR_BACKUP_DIRECTORY_NOT_WRITEABLE', 'Error: Este directorio de respaldo es protegido contra escritura.');
define('ERROR_DOWNLOAD_LINK_NOT_ACCEPTABLE', 'Error: El enlace de descarga no aceptable.');

define('SUCCESS_LAST_RESTORE_CLEARED', 'Éxito: La última fecha de restauración se ha borrado.');
define('SUCCESS_DATABASE_SAVED', 'Éxito: La base de datos se ha respaldado.');
define('SUCCESS_DATABASE_RESTORED', 'Éxito: La base de datos se ha restaurado.');
define('SUCCESS_BACKUP_DELETED', 'Éxito: El fichero de respaldo se ha borrado.');
define('SUCCESS_BACKUP_UPLOAD', 'Exitoso: El fichero de Backup se ha subido con éxito.');

//TEXT_COMPLETE_INSERTS
define('TEXT_COMPLETE_INSERTS', "<b>Completos 'INSERT's</b><br> - nombres de campo se introducen en cada línea de INSERT (amplía el Backup)");

define('TEXT_INFO_TABLES_IN_BACKUP', '<br />' . "\n" .'<b>Tablas en este Backup:</b>' . "\n");
define('TEXT_INFO_NO_INFORMATION', 'No información existente');
//UTF-8 convert
define('TEXT_CONVERT_TO_UTF', 'Convertir base de datos a UTF-8');
define('TEXT_IMPORT_UTF', 'Restaurar base de datos UTF-8');

//TEXT_REMOVE_COLLATE
define('TEXT_REMOVE_COLLATE', "<b>Sin codificación de caracteres 'COLLATE' und 'DEFAULT CHARSET'</b><br> - No se inserta la información de codificación de caracteres. Útil para migrar a otra codificación de caracteres de la base de datos.");

//TEXT_REMOVE_ENGINE
define('TEXT_REMOVE_ENGINE', "<b>Sin motor de respaldo 'ENGINE'</b><br> - No se inserta la información del motor de respaldo (MyISAM,InnoDB).");
?>

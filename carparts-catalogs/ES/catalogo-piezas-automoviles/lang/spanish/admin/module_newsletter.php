<?php
  /* --------------------------------------------------------------
  $Id: module_newsletter.php 2515 2011-12-12 16:15:44Z dokuman $

  modified eCommerce Shopsoftware
  http://www.modified-shop.org

  Copyright (c) 2009 - 2013 [www.modified-shop.org]
  --------------------------------------------------------------
  based on:
  (c) 2006 xt:Commerce

  Released under the GNU General Public License
  --------------------------------------------------------------*/

define('HEADING_TITLE', 'Newsletter');
define('TITLE_CUSTOMERS', 'Grupo de clientes');
define('TITLE_STK', 'Suscrito');
define('TEXT_TITLE', 'Asunto:');
define('TEXT_TO', 'Para: ');
define('TEXT_CC', 'Cc: ');
define('TEXT_BODY', 'Contenido: ');
define('TITLE_NOT_SEND', 'Título');
define('TITLE_ACTION', 'Acción');
define('TEXT_EDIT', 'Editar');
define('TEXT_DELETE', 'Borrar');
define('TEXT_SEND', 'Enviar');
define('CONFIRM_DELETE', '¿Está seguro?');
define('TITLE_SEND', 'Enviado');
define('TEXT_NEWSLETTER_ONLY', 'También a los miembros del grupo que no se hayan suscrito al newsletter.');
define('TEXT_USERS', ' suscriptores de ');
define('TEXT_CUSTOMERS', ' Clientes )</i>');
define('TITLE_DATE', 'Fecha');
define('TEXT_SEND_TO', 'Destinatario:');
define('TEXT_PREVIEW', '<b>Prevista:</b>');
define('TEXT_REMOVE_LINK', 'Cancelar suscripción al newsletter');

// BOF - DokuMan - 2011-12-12 - Texts for Newsletter E-Mail send status
define('INFO_NEWSLETTER_SEND', '%d newsletter enviado');
define('INFO_NEWSLETTER_LEFT', '%d newsletter');
// EOF - DokuMan - 2011-12-12 - Texts for Newsletter E-Mail send status

define('TEXT_NEWSLETTER_INFO', '<strong>ATENCIÓN:</strong> ¡Para enviar newsletters se recomienda utilizar programas externos!<br /><br />Si se utiliza el módulo de la tienda de newsletters, se debe preguntar al proveedor cuántos correos electrónicos se pueden enviar en un período de tiempo determinado.<br />Con muchos proveedores hay restricciones o el envío sólo se permite a través de servidores de correo electrónico especiales.<br /><br />Por defecto, la firma ya está adjunta al newsletter. Sin embargo, si desea insertar la firma en un formato diferente utilizando el editor, por favor, introduzca el código [NOSIGNATUR] (incl. corchetes) al final del newsletter.');

?>

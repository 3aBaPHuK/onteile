<?php
/* --------------------------------------------------------------
   $Id: categories.php 10211 2016-08-07 08:15:31Z GTB $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(categories.php,v 1.22 2002/08/17); www.oscommerce.com 
   (c) 2003	 nextcommerce (categories.php,v 1.10 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/
   
// BOF - Tomcraft - 2009-11-02 - Admin language tabs
define('TEXT_EDIT_STATUS', 'Statut actif');
// BOF - Tomcraft - 2009-11-02 - Admin language tabs
define('HEADING_TITLE', 'Catégorie');
define('HEADING_TITLE_SEARCH', 'Recherche:');
define('HEADING_TITLE_GOTO', 'Aller vers:');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_CATEGORIES_PRODUCTS', 'catégories/ articles');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_STARTPAGE', 'Page d\'accueil');
define('TABLE_HEADING_STOCK', 'Stock');
define('TABLE_HEADING_SORT', 'Sorte');
define('TABLE_HEADING_EDIT', 'Edit');
// BOF - Tomcraft - 2010-04-07 - Added definition for products model
define('TABLE_HEADING_PRODUCTS_MODEL', 'Numéro d\'article');
// EOF - Tomcraft - 2010-04-07 - Added definition for products model

// BOF - Hendrik - 2010-08-11 - Thumbnails in admin products list
define('TABLE_HEADING_IMAGE', 'Image');
// EOF - Hendrik - 2010-08-11 - Thumbnails in admin products list

define('TEXT_ACTIVE_ELEMENT', 'Élément actif');
define('TEXT_MARKED_ELEMENTS', 'Éléments marqués');
define('TEXT_INFORMATIONS', 'Informations');
define('TEXT_INSERT_ELEMENT', 'Nouveau élément');

define('TEXT_WARN', 'Avertissement de l\'inventaire');
define('TEXT_WARN_MAIN', 'Article principal');
define('TEXT_NEW_PRODUCT', 'Nouveau article dans "%s"');
define('TEXT_EDIT_PRODUCT', 'Modifier article dans "%s"');
define('TEXT_CATEGORIES', 'Catégories:');
define('TEXT_PRODUCTS', 'Produits:');
define('TEXT_PRODUCTS_PRICE_INFO', 'Prix:');
define('TEXT_PRODUCTS_TAX_CLASS', 'Tranche d\'imposition:');
define('TEXT_PRODUCTS_AVERAGE_RATING', 'Évaluation moyenne:');
define('TEXT_PRODUCTS_QUANTITY_INFO', 'Stock:');
define('TEXT_PRODUCTS_DISCOUNT_ALLOWED_INFO', 'Rabais maximal autorisé:');
define('TEXT_DATE_ADDED', 'Ajouté le:');
define('TEXT_DATE_AVAILABLE', 'Date de parution:');
define('TEXT_LAST_MODIFIED', 'Dernière modification:');
define('TEXT_IMAGE_NONEXISTENT', 'Image n\'existe pas');
define('TEXT_NO_CHILD_CATEGORIES_OR_PRODUCTS', 'Veuillez insérer une nouvelle catégorie ou un article dans <strong>%s</strong>.');
define('TEXT_PRODUCT_MORE_INFORMATION', 'Pour pus d\'informations, veuillez visiter la <a href="http://%s" target="_blank"><u>page d\'accueil</u></a> du fabricant.');
define('TEXT_PRODUCT_DATE_ADDED', 'Nous avons ajouté cet article dans notre catalogue le %s.');
define('TEXT_PRODUCT_DATE_AVAILABLE', 'Cet article est disponible à partir du %s.');

define('TEXT_EDIT_INTRO', 'Veuillez exécuter les modifications nécessaires.');
define('TEXT_EDIT_CATEGORIES_ID', 'ID de la catégorie:');
define('TEXT_EDIT_CATEGORIES_NAME', 'Nom de la catégorie:');
define('TEXT_EDIT_CATEGORIES_HEADING_TITLE', 'Titre de la catégorie:');
define('TEXT_EDIT_CATEGORIES_DESCRIPTION', 'Description de la catégorie:');
define('TEXT_EDIT_CATEGORIES_IMAGE', 'Image de la catégorie:');

define('TEXT_EDIT_SORT_ORDER', 'Classement du tri:');

define('TEXT_INFO_COPY_TO_INTRO', 'Veuillez choisir une nouvelle catégorie dans laquelle vous voulez coper l\'article:');
define('TEXT_INFO_CURRENT_CATEGORIES', 'Catégories actuelles:');

define('TEXT_INFO_HEADING_NEW_CATEGORY', 'Nouvelle catégorie dans "%s"');
define('TEXT_INFO_HEADING_EDIT_CATEGORY', 'Modifier la catégorie "%s"');
define('TEXT_INFO_HEADING_DELETE_CATEGORY', 'Supprimer la catégorie');
define('TEXT_INFO_HEADING_MOVE_CATEGORY', 'Déplacer la catégorie');
define('TEXT_INFO_HEADING_DELETE_PRODUCT', 'Supprimer l\'article');
define('TEXT_INFO_HEADING_MOVE_PRODUCT', 'Déplacer l\'article');
define('TEXT_INFO_HEADING_COPY_TO', 'Copier vers');
define('TEXT_INFO_HEADING_MOVE_ELEMENTS', 'Déplacer des éléments');
define('TEXT_INFO_HEADING_DELETE_ELEMENTS', 'Supprimer des éléments');

define('TEXT_DELETE_CATEGORY_INTRO', 'Vous en êtes sûr de vouloir supprimer cette catégorie?');
define('TEXT_DELETE_PRODUCT_INTRO', 'Vous en êtes sûr de vouloir supprimer cet article?');

define('TEXT_DELETE_WARNING_CHILDS', '<b>Avertissement:</b> Il existe encore %s (sous)catégories qui sont liées à la catégorie!');
define('TEXT_DELETE_WARNING_PRODUCTS', '<b>Avertissement:</b> Il existe encore %s articles qui sont liés à cette catégorie!');

define('TEXT_MOVE_WARNING_CHILDS', '<b>Info:</b> Il existe encore %s (sous)catégories qui sont liées à la catégorie!');
define('TEXT_MOVE_WARNING_PRODUCTS', '<b>Info:</b> Il existe encore %s articles qui sont liés à cette catégorie!');

define('TEXT_MOVE_PRODUCTS_INTRO', 'Veuillez choisir la catégorie principale vers laquelle vous voulez déplacer <b>%s</b>.');
define('TEXT_MOVE_CATEGORIES_INTRO', 'Veuillez choisir la catégorie principale vers laquelle vous voulez déplacer <b>%s</b>.');
define('TEXT_MOVE', 'Déplacer <b>%s</b> vers:');
define('TEXT_MOVE_ALL', 'Déplacer tous vers:');

define('TEXT_NEW_CATEGORY_INTRO', 'Veuillez insérer la nouvelle catégorie avec toutes données importantes.');
define('TEXT_CATEGORIES_NAME', 'Nom de la catégorie:');
define('TEXT_CATEGORIES_IMAGE', 'Image de la catégorie:');

define('TEXT_META_TITLE', 'Meta Title:');
define('TEXT_META_DESCRIPTION', 'Meta Description:');
define('TEXT_META_KEYWORDS', 'Meta Keywords:');

define('TEXT_SORT_ORDER', 'Ordre du tri:');

define('TEXT_PRODUCTS_STATUS', 'Statut de l\'article:');
define('TEXT_PRODUCTS_STARTPAGE', 'Afficher sur la page d\'accueil:');
define('TEXT_PRODUCTS_STARTPAGE_YES', 'Oui');
define('TEXT_PRODUCTS_STARTPAGE_NO', 'Non');
define('TEXT_PRODUCTS_STARTPAGE_SORT', 'Ordre du tri (page d\'accueil):');
define('TEXT_PRODUCTS_DATE_AVAILABLE', 'Date de parution:');
// BOF - Hetfield - 2010-01-28 - Changing product available in correctly names for status
define('TEXT_PRODUCT_AVAILABLE', 'Activé');
define('TEXT_PRODUCT_NOT_AVAILABLE', 'Désactivé');
// EOF - Hetfield - 2010-01-28 - Changing product available in correctly names for status
define('TEXT_PRODUCTS_MANUFACTURER', 'Fabricant de l\'article:');
define('TEXT_PRODUCTS_MANUFACTURER_MODEL', 'Numéro d\'article du fabricant  (HAN/MPN):');
define('TEXT_PRODUCTS_NAME', 'Nom de \'article:');
define('TEXT_PRODUCTS_DESCRIPTION', 'Description de l\'article:');
define('TEXT_PRODUCTS_QUANTITY', 'Stock:');
define('TEXT_PRODUCTS_MODEL', 'Numéro d\'article:');
define('TEXT_PRODUCTS_IMAGE', 'Image de l\'article:');
define('TEXT_PRODUCTS_URL', 'Lien du fabricant:');
define('TEXT_PRODUCTS_URL_WITHOUT_HTTP', '<small>(sans http:// principal)</small>');
define('TEXT_PRODUCTS_PRICE', 'Prix de l\'article:');
define('TEXT_PRODUCTS_WEIGHT', 'Poids de l\'article:');
define('TEXT_PRODUCTS_EAN', 'GTIN/EAN');
define('TEXT_PRODUCT_LINKED_TO', 'Mise en lien dans:');
define('TEXT_DELETE', 'Supprimer');
define('EMPTY_CATEGORY', 'Vider la catégorie');

define('TEXT_HOW_TO_COPY', 'Méthode de copie des articles:');
define('TEXT_COPY_AS_LINK', 'Créer un lien');
define('TEXT_COPY_AS_DUPLICATE', 'Dupliquer');

define('ERROR_CANNOT_LINK_TO_SAME_CATEGORY', 'Erreur: Les articles ne peuvent pas être mises en lien au sein de la même catégorie.');
define('ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le répertoire  \'images\' n\'est pas verrouillé dans le répertoire des catalogues:'. DIR_FS_CATALOG_IMAGES);
define('ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le répertoire  \'images\' n\'existe pas dans le répertoire des catalogues:'. DIR_FS_CATALOG_IMAGES);

define('TEXT_PRODUCTS_DISCOUNT_ALLOWED', 'Rabais maximal autorisé:');
define('HEADING_PRICES_OPTIONS', '<b>Options de prix</b>');
define('HEADING_PRODUCT_IMAGES', '<b>Images des articles</b>');
define('TEXT_PRODUCTS_WEIGHT_INFO', '<small>(kg)</small>');
define('TEXT_PRODUCTS_SHORT_DESCRIPTION', 'brève description:');
define('TEXT_PRODUCTS_KEYWORDS', 'Notions supplémentaire pour la recherche:');
define('TXT_STK', 'Pièce:');
define('TXT_PRICE', 'a:');
define('TXT_NETTO', 'Prix net:');
define('TXT_STAFFELPREIS', 'Prix échelonnés');

define('HEADING_PRODUCTS_MEDIA', '<b> Médium de l\'article</b>');
define('TABLE_HEADING_PRICE', 'Prix');

define('TEXT_FSK18', 'FSK 18:');
define('TEXT_CHOOSE_INFO_TEMPLATE_CATEGORIE', 'Modèle pour aperçu des catégories');
define('TEXT_CHOOSE_INFO_TEMPLATE_LISTING', 'Modèle pour aperçu des articles');
// BOF - Tomcraft - 2009-11-02 - Admin language tabs
//define('TEXT_PRODUCTS_SORT','Reihung:');
define('TEXT_PRODUCTS_SORT', 'Ordre de tri:');
// EOF - Tomcraft - 2009-11-02 - Admin language tabs
define('TEXT_EDIT_PRODUCT_SORT_ORDER', 'Classement des articles');
define('TXT_PRICES', 'Prix');
define('TXT_NAME', 'Nom de l\'article');
define('TXT_ORDERED', 'Articles commandés');
// BOF - Tomcraft - 2009-11-02 - Admin language tabs
define('TXT_SORT', 'Ordre du tri');
// EOF - Tomcraft - 2009-11-02 - Admin language tabs
define('TXT_WEIGHT', 'Poids');
define('TXT_QTY', 'En stock');
// BOF - Tomcraft - 2009-09-12 - add option to sort by date and products model
define('TXT_DATE', 'Date de l\'ajout');
define('TXT_MODEL', 'Numéro d\'article');
// EOF - Tomcraft - 2009-09-12 - add option to sort by date and products model

define('TEXT_MULTICOPY', 'Multiple');
define('TEXT_MULTICOPY_DESC', 'Copier des éléments dans des catégories suivantes: <br/> (Si choisi, paramètres "seul" seront ignorés.)');
define('TEXT_SINGLECOPY', 'Seul');
define('TEXT_SINGLECOPY_DESC', 'Copier des éléments dans la catégorie suivante: <br /> (Pour ce faire pas de catégorie sous "Multiple" doit être activé.)');
define('TEXT_SINGLECOPY_CATEGORY', 'Catégorie:');

define('TEXT_PRODUCTS_VPE', 'VPE');
define('TEXT_PRODUCTS_VPE_VISIBLE', 'Affichage VPE:');
define('TEXT_PRODUCTS_VPE_VALUE', 'Valeur:');

define('CROSS_SELLING_1', 'Cross Selling');
define('CROSS_SELLING_2', 'pour article');
define('CROSS_SELLING_SEARCH', 'Recherche de produit:<br/><small><i>entrer numéro d\'article</i></small>' );
define('BUTTON_EDIT_CROSS_SELLING', 'Cross Selling');
define('HEADING_DEL', 'Supprimer');
define('HEADING_ADD', 'Ajouter?');
define('HEADING_GROUP', 'Groupe');
define('HEADING_SORTING', 'Ordre');
define('HEADING_MODEL', 'Numéro d\'article');
define('HEADING_NAME', 'Article');
define('HEADING_CATEGORY', 'Catégorie');
define('HEADING_IMAGE', 'Image');

// BOF - Tomcraft - 2009-11-02 - Admin language tabs
define('TEXT_SORT_ASC', 'croissant');
define('TEXT_SORT_DESC', 'décroissant');
// EOF - Tomcraft - 2009-11-02 - Admin language tabs

// BOF - Tomcraft - 2009-11-06 - Use variable TEXT_PRODUCTS_DATE_FORMAT
define('TEXT_PRODUCTS_DATE_FORMAT', 'AAAA-MM-JJ');
// EOF - Tomcraft - 2009-11-06 - Use variable TEXT_PRODUCTS_DATE_FORMAT

// BOF - web28 - 2010-08-03 - add metatags max charakters info
define('TEXT_CHARACTERS', 'Signe');
// EOF - web28 - 2010-08-03 - add metatags max charakters info

define('TEXT_ATTRIBUTE_COPY', 'Copier avec les attributs de l\'article');
define('TEXT_ATTRIBUTE_COPY_INFO', 'Copier les attributs de l\'article avec<br/> Seulement conseillé pour de seules copies (1 article)');

define('TEXT_PRODUCTS_ORDER_DESCRIPTION', 'Description de la commande - Affichage à la fin de la commande, dans le mail de la commande, la version imprimée de la commande');

define('TEXT_HOW_TO_LINK', 'Page consulté après copier/ mettre en lien</b>');
define('TEXT_HOW_TO_LINK_INFO', 'Masque de saisie de l\'article <br/>(Pour plusieur articles vers le dernier de la liste)');

define('TEXT_SET_GROUP_PERMISSIONS', 'Se charger des droits des groupes des clients pour tous les sous-dossiers et articles?');

define('HEADING_TITLE_ONLY_INACTIVE_PRODUCTS', 'Afficher seulement des articles inactives');

// BOF - Timo Paul (mail[at]timopaul[dot]biz) - 2014-01-17 - duplicate products content and links
define('TEXT_CONTENT_COPY', 'Copier avec le contenu de l\'article');
define('TEXT_CONTENT_COPY_INFO', 'Copier avec e contenu de l\'article<br/ >Seulement conseillé pour de seules copies (1 article)');
define('TEXT_LINKS_COPY', 'Copier avec les liens des articles');
define('TEXT_LINKS_COPY_INFO', 'Copier avec les liens des articles <br/ >Seulement conseillé pour de seules copies (1 article)');
// EOF - Timo Paul (mail[at]timopaul[dot]biz) - 2014-01-17 - duplicate products content and links

define('TEXT_GRADUATED_PRICES_INFO', 'Le nombre des champs pour les prix échelonnés peut être modifé sous "<b>Configuration- section administration options - nombre prix échelonnés</b>".' );
define('TEXT_CATEGORY_SETTINGS', 'Paramètres de la catégorie:');

define('ERROR_QTY_SAVE_CHANGED', 'Le stock a été modifié et n\'a pas été enregistré pendant la modification de l\'article.');

define('TEXT_NO_MOVE_POSSIBLE', 'L\'article ne peut pas être déplacé.');

define('TEXT_IN', 'dans:');

define('TEXT_PRODUCTS_ATTRIBUTES_RECALCULATE', 'Calculer de nouveau les attributs de modification du taux de l\'impôt');

define('HEADING_TITLE_CAT_BREADCRUMB', 'dans "%s"');

define('TEXT_PRODUCTS_TAGS', 'Paramètres de l\'article');

define('TEXT_GRADUATED_PRICES_GROUP_INFO', 'Le groupe de client n\'a actuellement pas d\'autorisation  de voir des prix échelonnés. Cela peut être modifé à tout moment dans les paramètres des groupes des clients.');

define('TEXT_NO_FILE', 'Pas de fichier modèle disponible!');

define('ERROR_COPY_METHOD_NOT_SPECIFIED', 'Méthode de copie n\'est pas indiqué.');
define('ERROR_COPY_METHOD_NOT_ALLOWED', 'Métode de copie "Mettre en lien" n\'est pas autorisé dans les catégories.');

define('TEXT_TAGS_COPY', 'Copier avec les paramètres de l\'article');
define('TEXT_TAGS_COPY_INFO', 'Copier avec les paramètres de l\'article <br/ > Seulement conseillé pour de seules copies (1 article)');

define('TEXT_PRODUCTS_LAST_MODIFIED', 'Actualisé dernièrement le:');
?>

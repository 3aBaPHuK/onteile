<?php
/**********************************************
* file reworked and adadapted by noRiddle
*
* (c) noRiddle 03-2018
**********************************************/

require_once dirname(__FILE__) . '/../template.php';

class GuayaquilVehiclesList extends GuayaquilTemplate
{
    var $priorColumns = array();
    var $columns = array();
    var $vehicles = NULL;
    var $datacolumns = NULL;
    var $catalog = NULL;
    var $cnt_vcls; //count vehicles in result, noRiddle

    function __construct(IGuayaquilExtender $extender)
    {
        parent::__construct($extender);

        //$this->AppendJavaScript(dirname(__FILE__) . '/../jquery.tooltip.js'); //include this in /templates/catalogue/javascript/general.js.php, noRiddle
        //$this->AppendJavaScript(dirname(__FILE__).'/vehicletable.js'); //include this in /templates/catalogue/javascript/general.js.php, noRiddle
    }

    function Draw($catalog, $vehicles)
    {
        $this->vehicles = $vehicles;
        $this->catalog  = $catalog;
        $data_columns = array();
        
        //echo '<pre>'.print_r($vehicles, true).'</pre>';

        $columns = array('brand'=>$this->GetLocalizedString('ColumnVehicleBrand'),'name'=>$this->GetLocalizedString('ColumnVehicleName'));

        foreach ($vehicles->row->attributes() as $key => $value)
        {
            $data_columns[strtolower($key)] = (string)$value;
        }

        foreach ($vehicles->row->attribute as $attr)
        {
            $data_columns[strtolower($attr->attributes()->key)] = (string)$attr->attributes()->value;
            $columns[strtolower($attr->attributes()->key)] = (string) $attr->attributes()->name;
        }

        $this->datacolumns = $data_columns;
        $this->priorColumns = $this->columns;
        $this->columns = $columns;
        
        //$html = '<table class="guayaquil_table" border=1 width="100%">';
        //$html = '<table class="guayaquil_table vehicle-table" width="100%">'; //define border in CSS, noRiddle | put table around each vehicle (see below)
        //$html .= $this->DrawHeader(); put this above each vehicle (see below), noRiddle

        $this->cnt_vcls = 0; //count vehicles in result, noRiddle
        foreach ($vehicles->row as $row) {
            //$html = '<table class="guayaquil_table vehicle-table" width="100%">'; //define border in CSS, noRiddle
            $html .= $this->DrawRow($row, $catalog, $this->cnt_vcls);
            $this->cnt_vcls++; //count vehicles in result, noRiddle
            //$html .= '</table>';
        }
        
        //$html .= '</table>'; //put table around each vehicle (see below)
        //BOC don't need link to laximo, noRiddle
        //$html .= '<div style="visibility: visible; display: block; height: 20px; text-align: right;"><a href="http://dev.laximo.ru" rel="follow" style="visibility: visible; display: inline; font-size: 10px; font-weight: normal; text-decoration: none;">guayaquil</a></div>';
        $html .= '<div style="display:block; height:20px; text-align:right;"></div>';
        //EOC don't need link to laximo, noRiddle

        return $html;
    }

    function DrawHeader()
    {
        $html = '<tr>';

        foreach ($this->columns as $key=>$column){
            if (isset($this->datacolumns[$key])&&(in_array($key, $this->priorColumns))){
                $html .= $this->DrawHeaderCell(strtolower($column));
            }
        }
        //$html .= '<th style = "display:none">tooltip</th>';
        $html .= '</tr>';
        return $html;
    }

    function DrawHeaderCell($column)
    {
        return '<th>' . $this->DrawHeaderCellValue($column) . '</th>';
    }

    function DrawHeaderCellValue($column)
    {
        return $column;//$this->GetLocalizedString('ColumnVehicle' . (string)$column);
    }

    function PrepareVehicleInfo($row)
    {
        $info = "";

        foreach ($this->columns as $key=>$col_name)
            if (isset($row[$key])) {
                $c = @$row[$col_name];
                if ($info) $info .= "; ";
                $col_title = $col_name;//$this->GetLocalizedString('ColumnVehicle' . (string)$col_name);
                $info .= $col_title . ": " . $c;
            }

        return $info;
    }

    function DrawRow($row, $catalog, $cnt_vcls)
    {
        $row->addAttribute('vehicle_info', $this->PrepareVehicleInfo($row));

        $link = $this->FormatLink('vehicle', $row, $catalog);
        
        //echo '<pre>'.print_r($row, true).'</pre>';
        
        //BOC must loop here to get counter (commented out below), noRiddle
        $tooltip = '';
        $cnt = 0; //set counter, noRiddle (do it above)
        $tmp_html = '';
        foreach ($this->columns as $key=>$column) {
            if (isset($this->datacolumns[$key])){
                if (in_array($key, $this->priorColumns)) {
                    $cnt++; //count the columns to use value in colspan below, noRiddle (do it above)
                    $tmp_html .= $this->DrawCell($row, strtolower($key), $link);
                }
                $tooltip .= $this->DrawToolTipValue($row, strtolower($key), (string)$column, $cnt_vcls);
            }
        }
        //EOC must loop here to get counter (commented out below), noRiddle
        
        //echo '<pre>'.print_r($this->att_arr, true).'</pre>';
        
        
        $html = '<div class="around-tbl"><table class="guayaquil_table vehicle-table" width="100%">'; //define border in CSS, noRiddle | put table around each vehicle
        
        //BOC display a button to go to vehicle, noRiddle
        //$link_button = '<p class="vehicles-butt"><a href="'.$link.'"><span class="btn button-catalogue"><i class="glyphicon glyphicon-eye-open"></i> '.$this->GetLocalizedString('nr_searchparts').' <i class="glyphicon glyphicon-arrow-down"></span></a></p>';
        $link_button = '<p class="vehicles-butt"><a href="'.$link.'"><span class="cssButton cssButtonColor1"><i class="fa fa-arrow-down"></i><span class="cssButtonText"> '.$this->GetLocalizedString('nr_searchparts').' <i class="fa fa-arrow-down"></i></span></span></a></p>';
        $link_button2 = '<p class="vehicles-butt"><a href="'.$link.'"><span class="cssButton cssButtonColor1"><i class="fa fa-arrow-up"></i><span class="cssButtonText"> '.$this->GetLocalizedString('nr_searchparts').' <i class="fa fa-arrow-up"></i></span></span></a></p>';

        //$html .= '<tr><td colspan="'.$cnt.'">'.$link_button.'</td></tr>'; //commented out, 01-2021, noRiddle
        //EOC display a button to go to vehicle, noRiddle

        //$html .= $this->DrawHeader(); //header above each vehicle, noRiddle  //commented out, 01-2021, noRiddle
        
        //$html = '<tr class="vehic-head" onmouseout="this.className=\'\';" onmouseover="this.className=\'over\';" onclick="window.location=\'' . $link . '\'">';
        $html .= '<tr class="vehic-head">'; //noRiddle

        //BOC do it above because we need counter there already, noRiddle
        //$tooltip = '';

        /*foreach ($this->columns as $key=>$column)
        {
            if (isset($this->datacolumns[$key])){
                if (in_array($key, $this->priorColumns))
                    $html .= $this->DrawCell($row, strtolower($key), $link);
                $tooltip.=$this->DrawToolTipValue($row, strtolower($key), (string)$column);
            }
        }*/
        
        //BOC do it above because we need counter there already, noRiddle
        /*$cnt = 0; //set counter, noRiddle
        foreach ($this->columns as $key=>$column) {
            if (isset($this->datacolumns[$key])){
                if (in_array($key, $this->priorColumns)) {
                    $cnt++; //count the columns to use value in colspan below, noRiddle
                    $html .= $this->DrawCell($row, strtolower($key), $link);
                }
                $tooltip .= $this->DrawToolTipValue($row, strtolower($key), (string)$column);
            }
        }*/
        $html .= $tmp_html;
        //EOC do it above because we need counter there already, noRiddle

        //$html .= '<td class = "ttp" style = "display:none;">'.$tooltip.'</td>'; //display tooltip, see below, noRiddle
        $html .= '</tr>';

        //BOC display tooltip in table, noRiddle
        //$html .= '<tr><td colspan="'.($cnt-1).'"><ul>'.$tooltip.'</ul></td><td class="td-cent"><a href="'.$link.'"><span class="btn button-catalogue">'.$this->GetLocalizedString('nr_searchparts').'</span></a></td></tr>';
        //$html .= '<tr><td class="end-vehic" colspan="'.($cnt).'">'.$link_button.$tooltip.'</td></tr>'.'<tr><td class="end-vehic" colspan="'.($cnt).'"><div class="spacer"></div></td></tr>';
        //$html .= '<tr><td class="end-vehic" colspan="'.($cnt).'">'.$tooltip.'</td></tr>'.'<tr><td class="end-vehic" colspan="'.($cnt).'"><div class="spacer"></div></td></tr>';
        $html .= '<tr><td class="end-vehic" colspan="'.($cnt).'">'.$tooltip.'</td></tr>';
        //EOC display tooltip in table, noRiddle

        $html .= '<tr><td colspan="'.$cnt.'">'.$link_button2.'</td></tr>'; //added button at bottom as well, 06-2018 noRiddle

        $html .= '</table></div>'; //put table around each vehicle

        return $html;
    }

    function DrawCell($row, $column, $link)
    {

        return '<td>' . $this->DrawCellValue($row, $column, $link) . '</td>';

    }

    function DrawToolTipValue($row, $column, $name, $cnt_vcls)
    {
        foreach ($row->attributes() as $key => $value) {
            if (strtolower($key) == $column) {
                //return '<span class = "item">'. (string)$name . ':' . '<span style="display:inline-block; max-width:300px; float:right">' . (string)$value . '</span></span>';
                return $beg_list_html . '<div class="col-md-4 padd-r-15">'.(string)$name . ': '.mb_strimwidth((string)$value, 0, 500, '...').'</div>';
            }
        }

        foreach ($row->attribute as $attr) {
            if (strtolower($attr->attributes()->key) == $column) {
                //return '<span class = "item">'. (string)$name . ':' . '<span style=" display:inline-block; max-width:300px; float:right">' . (string)$attr->attributes()->value . '</span></span>';
                //BOC mark divergent attributes, noRiddle
                $this->att_arr[$cnt_vcls][(string)$name] = (string)$attr->attributes()->value;
                $add_diff_class = $cnt_vcls != 0 && $this->att_arr[$cnt_vcls-1][(string)$name] != (string)$attr->attributes()->value ? ' class="red"' : '';
                $add_diff_expl = $cnt_vcls != 0 && $this->att_arr[$cnt_vcls-1][(string)$name] != (string)$attr->attributes()->value ? ' ('.$this->GetLocalizedString('nr_diff_from_vehicle_above').')' : '';
                return '<div class="col-md-4 padd-r-15"><span'.$add_diff_class.'>'.(string)$name . ': '.mb_strimwidth((string)$attr->attributes()->value, 0, 500, '...').'</span>'.$add_diff_expl.'</div>';
                //EOC mark divergent attributes, noRiddle
            } 
        }
    }

    function DrawCellValue($row, $column, $link)
    {
        foreach ($row->attributes() as $key => $value)
            if (strtolower($key) == $column)
                //return '<a href="' . $link . '">' . (string)$value . '</a>';
                return (string)$value; //noRiddle
        foreach ($row->attribute as $attr)
            if (strtolower($attr->attributes()->key) == $column)
                //return '<a href="' . $link . '">' . (string)$attr->attributes()->value . '</a>';
                return (string)$attr->attributes()->value; //noRiddle
    }
}

?>
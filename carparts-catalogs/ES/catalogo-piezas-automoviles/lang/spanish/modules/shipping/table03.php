<?php
/* -----------------------------------------------------------------------------------------
   $Id: table.php 5118 2013-07-18 10:58:36Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project (earlier name of osCommerce)
   (c) 2002-2003 osCommerce (table.php,v 1.6 2003/02/16); www.oscommerce.com 
   (c) 2003 nextcommerce (table.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 XT-Commerce

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_TABLE03_TEXT_TITLE', 'costos de envío internacional DHL');
define('MODULE_SHIPPING_TABLE03_TEXT_DESCRIPTION', 'costos de envío internacional DHL');
define('MODULE_SHIPPING_TABLE03_TEXT_WAY', 'El mejor camino (%01.2f kg)');
define('MODULE_SHIPPING_TABLE03_TEXT_WEIGHT', 'Peso');
define('MODULE_SHIPPING_TABLE03_TEXT_AMOUNT', 'Cantidad');
define('MODULE_SHIPPING_TABLE03_UNDEFINED_RATE', 'Los costos de envío no se pueden calcular por el momento.');
define('MODULE_SHIPPING_TABLE03_INVALID_ZONE', 'No es posible realizar un envío a este país.');

define('MODULE_SHIPPING_TABLE03_STATUS_TITLE' , 'Activar costos de envío tabulares');
define('MODULE_SHIPPING_TABLE03_STATUS_DESC' , '¿Desea ofrecer costos de envío tabulares?');
define('MODULE_SHIPPING_TABLE03_ALLOWED_TITLE' , 'Zonas de envío permitidas');
define('MODULE_SHIPPING_TABLE03_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE (deje este campo vacío si desea permitir todas las zonas))');
define('MODULE_SHIPPING_TABLE03_MODE_TITLE' , 'Método de costos de envío');
define('MODULE_SHIPPING_TABLE03_MODE_DESC' , 'Los costos de envío se basan en el costo total o el peso total de los productos solicitados.');
define('MODULE_SHIPPING_TABLE03_TAX_CLASS_TITLE' , 'Clase de impuestos');
define('MODULE_SHIPPING_TABLE03_TAX_CLASS_DESC' , 'Aplicar la siguiente clase de impuestos a los costos de envío.');
define('MODULE_SHIPPING_TABLE03_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_TABLE03_ZONE_DESC' , 'Si se ha seleccionado una zona, este método de envío se utilizará sólo para esa zona.');
define('MODULE_SHIPPING_TABLE03_SORT_ORDER_TITLE' , 'Orden de clasificación');
define('MODULE_SHIPPING_TABLE03_SORT_ORDER_DESC' , 'Orden de la visualización');
define('MODULE_SHIPPING_TABLE03_NUMBER_ZONES_TITLE' , 'Número de zonas');
define('MODULE_SHIPPING_TABLE03_NUMBER_ZONES_DESC' , 'Número de zonas proporcionadas');
define('MODULE_SHIPPING_TABLE03_DISPLAY_TITLE' , 'Activar la visualización');
define('MODULE_SHIPPING_TABLE03_DISPLAY_DESC' , '¿Desea mostrar si no es posible realizar envíos a ese país o si no se pueden calcular los costos de envío?');

for ($module_shipping_table_i = 1; $module_shipping_table_i <= MODULE_SHIPPING_TABLE03_NUMBER_ZONES; $module_shipping_table_i ++) {
define('MODULE_SHIPPING_TABLE03_COUNTRIES_'.$module_shipping_table_i.'_TITLE' , '<hr/>Zona '.$module_shipping_table_i.' países');
define('MODULE_SHIPPING_TABLE03_COUNTRIES_'.$module_shipping_table_i.'_DESC' , 'Lista separada por comas de los códigos de países ISO de 2 caracteres que forman parte de la zona '.$module_shipping_table_i.' (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_TABLE03_COST_'.$module_shipping_table_i.'_TITLE' , 'Zona '.$module_shipping_table_i.' costos de envío');
define('MODULE_SHIPPING_TABLE03_COST_'.$module_shipping_table_i.'_DESC' , 'Costos de envío por zona '.$module_shipping_table_i.' Destinos, basados en un grupo de pesos máximos de pedido o en el valor de la cesta de la compra, dependiendo de la configuración del módulo. Ejemplo: 3:8.50,7:10.50,.... Un peso/precio inferior o igual a 3 costaría 8,50 para los países de destino de la zona '.$module_shipping_table_i.' .');
define('MODULE_SHIPPING_TABLE03_HANDLING_'.$module_shipping_table_i.'_TITLE' , 'Zona '.$module_shipping_table_i.' Recargo de manejo');
define('MODULE_SHIPPING_TABLE03_HANDLING_'.$module_shipping_table_i.'_DESC' , 'Recargo de manejo para esta zona de envío');
  //BOC new field for shippingtime, noRiddle
define('MODULE_SHIPPING_TABLE03_SHIPPINGTIME_'.$module_shipping_table_i.'_TITLE' , 'Zona '.$module_shipping_table_i.' tiempo de envío');
define('MODULE_SHIPPING_TABLE03_SHIPPINGTIME_'.$module_shipping_table_i.'_DESC' , 'Introduzca el tiempo de envío para esta zona. Ejemplo: 2-3 (día/s hábile/s sera añadido automaticamente)');
  //EOC new field for shippingtime, noRiddle
  //BOC new flat costs depending on order_total amount, noRiddle 
define('MODULE_SHIPPING_TABLE03_FLAT_'.$module_shipping_table_i.'_TITLE' , 'Zona '.$module_shipping_table_i.' costos a tarifa plana');
define('MODULE_SHIPPING_TABLE03_FLAT_'.$module_shipping_table_i.'_DESC' , 'Los costos de envío a tarifa plana se basan en el costo total de la mercancía pedida. <br />Ejemplo:  25:5.50,50:8.50,etc..<br />Desde 25 EUR se cobrarán 5.50 EUR, etc.');
  //EOC new flat costs depending on order_total amount, noRiddle
}
define('MODULE_SHIPPING_TABLE03_SHIPPING_INFO', 'tiempo de envío');
define('MODULE_SHIPPING_TABLE03_SHIPPING_INFO_UNITS', 'día hábil');
define('MODULE_SHIPPING_TABLE03_SHIPPING_INFO_UNITP', 'días hábiles');
?>

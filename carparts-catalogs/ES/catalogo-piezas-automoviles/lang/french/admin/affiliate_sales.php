<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_sales.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_sales.php, v 1.5 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Vente sur le programme affilié');
define('HEADING_TITLE_SEARCH', 'Numéro de commande:');
define('HEADING_TITLE_STATUS', 'Statut');

define('TABLE_HEADING_ONLINE', 'Clics');
define('TABLE_HEADING_AFFILIATE', 'Partenaire');
define('TABLE_HEADING_DATE', 'Date');
define('TABLE_HEADING_SALES', 'Montant de commission');
define('TABLE_HEADING_PERCENTAGE', 'Commission');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ORDER_ID', 'Numéro de commande');
define('TABLE_HEADING_VALUE', 'Valeur de la commande');

define('TEXT_NO_SALES', 'Pas de ventes ont été enregistrées');
define('TEXT_DELETED_ORDER_BY_ADMIN', 'Supprimé (Admin)');
define('TEXT_DISPLAY_NUMBER_OF_SALES', 'Affichant <b>%d</b> à <b>%d</b>(d\'au total<b>%d</b> ventes)');
define('TEXT_INFORMATION_SALES_TOTAL', 'Total des ventes des partenaires:');
define('TEXT_INFORMATION_AFFILIATE_TOTAL', 'Total des commissions des partenaires:');
?>

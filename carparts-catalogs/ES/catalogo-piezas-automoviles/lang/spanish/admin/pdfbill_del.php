<?php
  /* --------------------------------------------------------------
   $Id: backup.php 1780 2011-02-08 02:09:45Z cybercosmonaut $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Archivos PDFBILL Next');

define('TABLE_HEADING_TITLE', 'Nombre del archivo');
define('TABLE_HEADING_FILE_DATE', 'Fecha');
define('TABLE_HEADING_FILE_SIZE', 'Tamaño');

define('TEXT_LOG_DIRECTORY', 'Directorio de datos:');
define('TEXT_DELETE_INTRO', '¿Está seguro de que quiere borrar este archivo?');

define('SUCCESS_LOG_DELETED', 'Éxito: El archivo ha sido borrado.');

define('ERROR_LOG_DIRECTORY_DOES_NOT_EXIST', 'Error: El directorio no existe.');
define('ERROR_LOG_DIRECTORY_NOT_WRITEABLE', 'Error: El directorio está protegido contra escritura.');
define('ERROR_DOWNLOAD_LINK_NOT_ACCEPTABLE', 'Error: Enlace de descarga no aceptable.');
?>

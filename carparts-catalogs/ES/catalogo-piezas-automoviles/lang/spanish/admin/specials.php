<?php
/* --------------------------------------------------------------
   $Id: specials.php 4200 2013-01-10 19:47:11Z Tomcraft1980 $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(specials.php,v 1.10 2002/01/31); www.oscommerce.com 
   (c) 2003	 nextcommerce (specials.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Ofertas especiales');

define('TABLE_HEADING_PRODUCTS', 'Artículo');
define('TABLE_HEADING_PRODUCTS_QUANTITY', 'Número de artículos (stock)');
define('TABLE_HEADING_SPECIALS_QUANTITY', 'Número de ofertas especiales');
define('TABLE_HEADING_START_DATE', 'Válido desde');
define('TABLE_HEADING_EXPIRES_DATE', 'Válido hasta');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Precio del artículo');
define('TABLE_HEADING_STATUS', 'Estado');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_SPECIALS_PRODUCT', 'Artículo:');
define('TEXT_SPECIALS_SPECIAL_PRICE', 'Precio de la oferta:');
define('TEXT_SPECIALS_SPECIAL_QUANTITY', 'Cantidad:');
define('TEXT_SPECIALS_START_DATE', 'Válido desde: <small>(AAAA-MM-DD)</small>');
define('TEXT_SPECIALS_EXPIRES_DATE', 'Válido hasta: <small>(AAAA-MM-DD)</small>');

define('TEXT_INFO_DATE_ADDED', 'agregado el:');
define('TEXT_INFO_LAST_MODIFIED', 'última modificación:');
define('TEXT_INFO_NEW_PRICE', 'nuevo precio:');
define('TEXT_INFO_ORIGINAL_PRICE', 'precio anterior:');
define('TEXT_INFO_PERCENTAGE', 'Porcentaje:');
define('TEXT_INFO_START_DATE', 'Válido desde:');
define('TEXT_INFO_EXPIRES_DATE', 'Válido hasta:');
define('TEXT_INFO_STATUS_CHANGE', 'Desactivado el:');

define('TEXT_INFO_HEADING_DELETE_SPECIALS', 'Borrar oferta especial');
define('TEXT_INFO_DELETE_INTRO', '¿Está seguro de que quiere borrar la oferta especial?');

define('TEXT_IMAGE_NONEXISTENT', '¡No hay imagen disponible!');

define('TEXT_SPECIALS_PRICE_TIP', 'También puede introducir valores porcentuales en el campo Precio de oferta, p.ej.: <strong>20%</strong><br>Si introduce un nuevo precio, los decimales deben separarse  con un \'.\' ,por ejemplo: <strong>49.99</strong>');
define('TEXT_SPECIALS_QUANTITY_TIP', 'En el campo<strong>Cantidad</strong> puede introducir el número de piezas para las que la oferta debe ser válida. <br>Bajo "Configuración" -> "Opciones de gestión de almacenes" -> "Revisar ofertas especiales" puede decidir si se debe revisar el stock de ofertas especiales.');
define('TEXT_SPECIALS_START_DATE_TIP', 'Especifique la fecha a partir de la cual el precio de oferta será válido.<br>');
define('TEXT_SPECIALS_EXPIRES_DATE_TIP', 'Deje vacío el campo <strong>Válido hasta</strong> si desea que el precio de oferta sea válido indefinidamente.<br>');
?>

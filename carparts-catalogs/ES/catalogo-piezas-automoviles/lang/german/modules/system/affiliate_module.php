<?php
/*******************************************************
* file: affiliate_module.php
* use: language file for system module
* (c) noRiddle 07-2017
* Released under the GNU General Public License
*******************************************************/

define('MODULE_AFFILIATE_MODULE_TEXT_TITLE', 'Affiliate-Modul');
define('MODULE_AFFILIATE_MODULE_TEXT_DESCRIPTION', 'Stellt die Affiliate-Erweiterung zur Verfügung.');
define('MODULE_AFFILIATE_MODULE_STATUS_TITLE', 'Modul aktivieren ?');
define('MODULE_AFFILIATE_MODULE_STATUS_DESC', 'Affiliate-Modul aktivieren');
?>
<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_english.php 16 2010-10-28 15:21:32Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_german.php, v 1.3 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   
   corrected E_NOTICE constnt already defined, 01-2018 noRiddle
   ---------------------------------------------------------------------------*/

// reports box text in includes/boxes/affiliate.php
defined('BOX_CONFIGURATION_900') OR define('BOX_CONFIGURATION_900', 'Affiliate Configuration');
defined('BOX_HEADING_AFFILIATE') OR define('BOX_HEADING_AFFILIATE', 'Affiliate Program');
defined('BOX_AFFILIATE_CONFIGURATION') OR define('BOX_AFFILIATE_CONFIGURATION', 'Affiliate Configuration');
defined('BOX_AFFILIATE_SUMMARY') OR define('BOX_AFFILIATE_SUMMARY', 'Summary');
defined('BOX_AFFILIATE') OR define('BOX_AFFILIATE', 'Affiliates');
defined('BOX_AFFILIATE_PAYMENT') OR define('BOX_AFFILIATE_PAYMENT', 'Affiliate Payments');
defined('BOX_AFFILIATE_BANNERS') OR define('BOX_AFFILIATE_BANNERS', 'Affiliate Banners');
defined('BOX_AFFILIATE_CONTACT') OR define('BOX_AFFILIATE_CONTACT', 'Affiliate Contakt');
defined('BOX_AFFILIATE_SALES') OR define('BOX_AFFILIATE_SALES', 'Affiliate Sales');
defined('BOX_AFFILIATE_CLICKS') OR define('BOX_AFFILIATE_CLICKS', 'Affiliate Clicks');

defined('BUTTON_ADD_BANNER') OR define('BUTTON_ADD_BANNER', 'Add Banner');
defined('BUTTON_BILLING') OR define('BUTTON_BILLING', 'Start Billing');
defined('BUTTON_BANNERS') OR define('BUTTON_BANNERS', 'Bannermanager');
defined('BUTTON_CLICKTHROUGHS') OR define('BUTTON_CLICKTHROUGHS', 'Clickthroughs');
defined('BUTTON_SALES') OR define('BUTTON_SALES', 'Partner Sales');
?>
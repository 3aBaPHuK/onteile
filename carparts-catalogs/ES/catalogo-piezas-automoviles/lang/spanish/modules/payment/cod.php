<?php
/* -----------------------------------------------------------------------------------------
   $Id: cod.php 998 2005-07-07 14:18:20Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.7 2002/04/17); www.oscommerce.com 
   (c) 2003	 nextcommerce (cod.php,v 1.5 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/
define('MODULE_PAYMENT_TYPE_PERMISSION', 'cod');
define('MODULE_PAYMENT_COD_TEXT_TITLE', 'Apellido');
define('MODULE_PAYMENT_COD_TEXT_DESCRIPTION', 'Apellido');
define('MODULE_PAYMENT_COD_TEXT_INFO', 'Por favor, tenga en cuenta que se debe pagar una tarifa adicional de envío de 2 euros al transportista local.');
define('MODULE_PAYMENT_COD_ZONE_TITLE', 'Zona de pago');
define('MODULE_PAYMENT_COD_ZONE_DESC', 'Si se selecciona una zona, el método de pago se aplica sólo a esa zona.');
define('MODULE_PAYMENT_COD_ALLOWED_TITLE', 'Zonas permititdas');
define('MODULE_PAYMENT_COD_ALLOWED_DESC', 'Introduzca <b>individualmente</b>  las zonas permitidas para este módulo. (por ejemplo AT,DE (si está vacío, se permiten todas las zonas))');
define('MODULE_PAYMENT_COD_STATUS_TITLE', 'Activar módulo pago contra reembolso');
define('MODULE_PAYMENT_COD_STATUS_DESC', '¿Quiere aceptar pagos contra reembolso?');
define('MODULE_PAYMENT_COD_SORT_ORDER_TITLE', 'Orden de visualización');
define('MODULE_PAYMENT_COD_SORT_ORDER_DESC', 'Orden de la visualización. Primero se muestra el dígito más pequeño.');
define('MODULE_PAYMENT_COD_ORDER_STATUS_ID_TITLE', 'Determinar estado del pedido');
define('MODULE_PAYMENT_COD_ORDER_STATUS_ID_DESC', 'Cambiar los pedidos realizados con este módulo a este estado');
define('MODULE_PAYMENT_COD_LIMIT_ALLOWED_TITLE', 'Cantidad máxima');
define('MODULE_PAYMENT_COD_LIMIT_ALLOWED_DESC', '¿A partir de qué cantidad ya no se permite el pago contra reembolso?<br />El valor introducido se compara con el subtotal, que se redondea.<br />Esto significa que sólo se tiene en cuenta el valor puro de las mercancías, excluidos los costos de envío y los posibles recargos.');
define('MODULE_PAYMENT_COD_DISPLAY_INFO_TITLE', 'Visualización en la caja');
define('MODULE_PAYMENT_COD_DISPLAY_INFO_DESC', '¿Debe aparecer un aviso de los costos adicionales en la caja?');
define('MODULE_PAYMENT_COD_DISPLAY_INFO_TEXT', 'Por favor, tenga en cuenta que se debe pagar una tarifa adicional de envío de 2 euros al transportista local.<br/>');
?>

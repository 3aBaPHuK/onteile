<?php
/***********************************************
*
*
*
*
***********************************************/
?>
<script>
$.alerts.overlayOpacity = .2;
$.alerts.overlayColor = '#000';
function alert(message, title) {
    title = title || 'Information';
    jAlert(message, title);
}
</script>
<script>
<?php if (!strstr($PHP_SELF, FILENAME_CATALOGUE_SHOPPING_CART) && !strstr($PHP_SELF, 'checkout')) { ?>
$(function() {
    $('#toggle_cat_cart').click(function() {
        $('.toggle_cat_cart').slideToggle('slow');
        $('.toggle_wishlist').slideUp('slow');
        $('.account-short').slideUp('slow'); <?php //new for account information ?>
        ac_closing();
        return false;
    });
    $("html").not('.toggle_cat_cart').bind('click', function(e) {
        $('.toggle_cat_cart').slideUp('slow');
    });
    <?php if(isset($_SESSION['new_prid_in_catcart_'.$_SESSION['shopsess']])) {
        unset($_SESSION['new_prid_in_catcart_'.$_SESSION['shopsess']]); ?>
        $('.toggle_cat_cart').slideToggle('slow');
        timer = setTimeout(function(){$('.toggle_cat_cart').slideUp('slow');}, 3000);
        $('.toggle_cat_cart').mouseover(function() {clearTimeout(timer);});
    <?php } ?>
});     

$(function() {
    $('#toggle_wishlist').click(function() {
    $('.toggle_wishlist').slideToggle('slow');
    $('.toggle_cat_cart').slideUp('slow');
    $('.account-short').slideUp('slow'); <?php //new for account information ?>
    ac_closing();
    return false;
});
$("html").not('.toggle_wishlist').bind('click',function(e) {
    $('.toggle_wishlist').slideUp('slow');
});
    <?php if (DISPLAY_CART == 'false' && isset($_SESSION['new_products_id_in_wishlist_'.$_SESSION['shopsess']])) {
        unset($_SESSION['new_products_id_in_wishlist_'.$_SESSION['shopsess']]); ?>
        $('.toggle_wishlist').slideToggle('slow');
        timer = setTimeout(function(){$('.toggle_wishlist').slideUp('slow');}, 3000);
        $('.toggle_wishlist').mouseover(function() {clearTimeout(timer);});
    <?php } ?>
}); 
</script>    
<?php } else {
    unset($_SESSION['new_prid_in_catcart_'.$_SESSION['shopsess']]);
    unset($_SESSION['new_products_id_in_wishlist_'.$_SESSION['shopsess']]);
} ?>
<script>
<?php //BOC new for account information, noRiddle ?>
$(function() {
    $('#account-short').click(function() {
        $('.account-short').slideToggle('slow');
        $('.toggle_cat_cart').slideUp('slow');
        $('.toggle_wishlist').slideUp('slow');
            
        return false;
    });
    $('html').not('.account-short').bind('click', function(e) {
        $('.account-short').slideUp('slow');
    });
<?php if(isset($_SESSION['just_logged_in']) && $_SESSION['just_logged_in'] === true) {
    unset($_SESSION['just_logged_in']); ?>
    $('.account-short').slideToggle('slow');
    timer = setTimeout(function(){$('.account-short').slideUp('slow');}, 3000);
    $('.account-short').mouseover(function() {clearTimeout(timer);});
<?php } ?>
});
</script>
<?php //EOC new for account information, noRiddle ?>
<?php //BOC make captcha reloadable, noRiddle
$use_capt_arr = isset($use_captcha) && is_array($use_captcha) ? $use_captcha : array();
if(basename($PHP_SELF) == 'login.php' && $_SESSION['customers_login_tries'] >= LOGIN_NUM) {$use_capt_arr[] = 'login';}
foreach($use_capt_arr as $site_frag) {
    if(strpos($PHP_SELF, $site_frag) !== false || ($site_frag == 'contact' && isset($_GET['coID']) && $_GET['coID'] == 7)) {
?>
<script>
$(function() {
    $('#rel-capt').click(function(){
        var d = new Date();	
        $(this).prev('img').attr('src', '<?php echo xtc_href_link(FILENAME_DISPLAY_VVCODES, '', 'SSL'); ?>?' + d.getTime());  
    });
});
</script>
<?php 
    } 
} //EOC make captcha reloadable, noRiddle ?>
<?php
//BOC accordion for admin FAQs
if(strpos($PHP_SELF, FILENAME_CONTENT) && $_GET['coID'] >= 10000 && $_GET['coID'] <= 11000) {
?>
<script>
//*** BOC multi functional accordion, noRiddle ***/
$(function() {
    var $acch3 = $('.acc').find('h3'),
        $accdiv = $('.acc').find('div');
    $accdiv.slice(1).hide();
    $acch3.first().addClass('open');
    $acch3.click(function(e){
        e.preventDefault();
        /*$('.acc div:visible').length > 1 ? $(this).removeClass('open') : $acch3.removeClass('open');*/
        if ($(this).next('div').is(':hidden')) {
            $(this).addClass('open').siblings('h3').removeClass('open');
            $(this).next('div').slideDown(500).siblings('div:visible').slideUp(500);
        } else {
            $(this).removeClass('open').next('div').slideUp(500);
        }
    });
});
//*** EOC multi functional accordion, noRiddle ***
</script>
<?php
}
//EOC accordion for admin FAQs
//BOC show pass implementation, 05-2021, noRiddle
?>
<script>
$(function() {
    if($('input[type="password"]').length) {
        let $ipw = $('input[type="password"]'),
            sh_pw = ' <i class="shw-pw fa fa-eye"></i>';
        $ipw.each(function() {
            let th = $(this).outerHeight(),
                icfs = 22,
                nth = th/2 - icfs/2;
            $(this).wrap('<div class="pssw-wrap" style="position:relative;"></div>');
            $(sh_pw).insertAfter($(this)).css({'position':'absolute', 'top':nth, 'right':'5px', 'font-size':icfs+'px', 'color':'#555', 'cursor':'pointer'});
        });
        $('.shw-pw').on('click', function() {
            let $tpi = $(this).prev('input');

            $(this).toggleClass("fa-eye fa-eye-slash");

            if($tpi.attr('type') == 'password') {
                $tpi.prop('type', 'text');
            } else if($tpi.attr('type') == 'text') {
                $tpi.prop('type', 'password');
            }
        });
    }
});
</script>
<?php
//EOC show pass implementation, 05-2021, noRiddle
?>
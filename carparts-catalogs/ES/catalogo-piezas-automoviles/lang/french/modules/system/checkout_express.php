<?php
/* -----------------------------------------------------------------------------------------
   $Id: checkout_express.php 10597 2017-01-23 18:10:51Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_CHECKOUT_EXPRESS_STATUS_TITLE', 'Statut');
define('MODULE_CHECKOUT_EXPRESS_STATUS_DESC', 'Statut de module');
define('MODULE_CHECKOUT_EXPRESS_STATUS_INFO', 'Actif');
define('MODULE_CHECKOUT_EXPRESS_TEXT_TITLE', 'Checkout express');
define('MODULE_CHECKOUT_EXPRESS_TEXT_DESCRIPTION', 'Checkout express rend possible la sauvegarde de l\'expédition et des informations de paiement. <br/><br/><b>Attention:</b> Cela n\'est que possible avec un modèle compatible avec la version de boutique 2.0.0.0 (et plus récent)!');
define('MODULE_CHECKOUT_EXPRESS_CONTENT_TITLE', 'Contenu');
define('MODULE_CHECKOUT_EXPRESS_CONTENT_DESC', 'Vous pouvez créer une autre page de contenu et expliquer le checkout express.');

define('MODULE_CHECKOUT_EXPRESS_DESCRIPTION_INSTALL', '<br/><br/>En plus vous pouvez installer une page d\'information pour le checkout express.' );
define('MODULE_CHECKOUT_EXPRESS_BUTTON_INSTALL', 'Insérer contenu');
?>

﻿<?php
/**********************************************
* file reworked and adadapted by noRiddle
*
* (c) noRiddle 03-2018
**********************************************/

if(!isset($_SESSION['customer_id'])) xtc_redirect(xtc_href_link(FILENAME_DEFAULT)); //redirect to start page if not logged in, noRiddle

// Include soap request class
include('nr_catalogue_files/guayaquillib/data/requestOem.php');
// Include view class
include('nr_catalogue_files/guayaquillib/render/vehicles/vehicletable.php');

include('nr_catalogue_files/extender.php');

class VehiclesExtender extends CommonExtender
{
    function FormatLink($type, $dataItem, $catalog, $renderer)
    {
        if (!$catalog)
            $catalog = $dataItem['catalog'];
            //BOC set link to content 1003 respect. 1004 which includes qgroups.php respect. vehicle.php, noRiddle
            //$link = ($renderer->qg == 1 ? 'qgroups' : 'vehicle') . '.php?c=' . $catalog . '&vid=' . $dataItem['vehicleid'] . '&ssd=' . $dataItem['ssd'] . ($renderer->qg == -1 ? '&checkQG': ''). '&path_data=' . urlencode(base64_encode(substr($dataItem['vehicle_info'], 0, 300)));
            $link = ($renderer->qg == 1 ? xtc_href_link(FILENAME_CONTENT, 'coID=1003') : xtc_href_link(FILENAME_CONTENT, 'coID=1004')) . '?c=' . $catalog . '&vid=' . $dataItem['vehicleid'] . '&ssd=' . $dataItem['ssd'] . ($renderer->qg == -1 ? '&checkQG': ''). '&path_data=' . urlencode(base64_encode(substr($dataItem['vehicle_info'], 0, 300)));
            //EOC set link to content 1003 respect. 1004 which includes qgroups.php respect. vehicle.php, noRiddle

        return $link;
        //return 'vehicle.php?c='.$catalog.'&vid='.$dataItem['vehicleid'].'&ssd='.$dataItem['ssd'];
    }
}

// Create request object
$catalogCode = array_key_exists('c', $_GET) ? $_GET['c'] : false;
$request = new GuayaquilRequestOEM($catalogCode, array_key_exists('ssd', $_GET) ? $_GET['ssd'] : '', Config::$catalog_data);
if (Config::$useLoginAuthorizationMethod) {
    $request->setUserAuthorizationMethod(Config::$userLogin, Config::$userKey);
}

// Append commands to request
$findType = $_GET['ft'];
if ($findType == 'findByVIN') {
    //BOC replace oO (like in Oliver) with 0 (zero), acc. to http://www.gerstelblog.de/2015/05/13/eine-frage-warum-enthaelt-der-herstellercode-von-opel-eine-null/ O doesn't eist in VIN, noRiddle 21-06-2018
    $_GET['vin'] = str_replace(array('O', 'o'), '0', $_GET['vin']); 
    //EOC replace oO (like in Oliver) with 0 (zero), noRiddle 21-06-2018
    $request->appendFindVehicleByVIN($_GET['vin']);
    //BOC store vin in session to have it available later in shop, noRiddle
    if(isset($_SESSION['vin_val_catalog'])) unset($_SESSION['vin_val_catalog']);
    if(isset($_GET['vin'])) {
        $_SESSION['vin_val_catalog'] = $_GET['vin'];
    }
    //EOC store vin in session to have it available later in shop, noRiddle
} else if ($findType == 'findByFrame') {
    $request->appendFindVehicleByFrame($_GET['frame'], $_GET['frameNo']);
    //BOC delete our vin session, noRiddle
    if(isset($_SESSION['vin_val_catalog'])) unset($_SESSION['vin_val_catalog']);
    //EOC delete our vin session, noRiddle
} else if ($findType == 'execCustomOperation') {
    $request->appendExecCustomOperation($_GET['operation'], $_GET['data']);
    //BOC delete our vin session, noRiddle
    if(isset($_SESSION['vin_val_catalog'])) unset($_SESSION['vin_val_catalog']);
    //EOC delete our vin session, noRiddle
} else if ($findType == 'findByWizard2') {
    $request->appendFindVehicleByWizard2($_GET['ssd']);
    //BOC delete our vin session, noRiddle
    if(isset($_SESSION['vin_val_catalog'])) unset($_SESSION['vin_val_catalog']);
    //EOC delete our vin session, noRiddle
}

//echo '<br /><br /><pre>$_SESSION[vin_val_catalog]: '.$_SESSION['vin_val_catalog'].'</pre>';

if ($catalogCode) {
    $request->appendGetCatalogInfo();
}
// Execute request
$data = $request->query();

// Check errors
if ($request->error != '') {
    echo $request->error;
} else {
    $vehicles = $data[0];
    $cataloginfo = $catalogCode ? $data[1]->row : false;
    
    //BOC put this on top, noRiddle
    /*$brand_img = (string)$vehicles->row['catalog'];
    //BOC new html, noRiddle
    echo '<div id="main-top">';
    echo '<div class="log-and-butt">';
    echo '<div class="brand-logo"><img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/'.Config::$nr_icon_arr[$brand_img].'" alt ="" /></div>';
    echo '<div class="top-buttons">';
    /*echo '<a href="/katalog_shop_test/"><span class="btn button-catalogue"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_brand_choice').'</span></a>&nbsp;'
         .'<a href="'.xtc_href_link(FILENAME_CATALOGUE_SHOPPING_CART).'"><span class="btn button-catalogue"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_shopp_cart').'</span></a>';*/
    /*echo '</div>';
    echo '</div>';*/
    //EOC new html, noRiddle
    
    /*if (($cataloginfo && CommonExtender::isFeatureSupported($cataloginfo, 'vinsearch')) || ($findType == 'findByVIN')) {
        $formvin = array_key_exists('vin', $_GET) ? $_GET['vin'] : '';
        echo '<div class="vin-search">'; //noRiddle
        include('forms/vinsearch.php');
        echo '</div>'; //noRiddle
    }*/

    /*if (($cataloginfo && CommonExtender::isFeatureSupported($cataloginfo, 'framesearch')) || $findType == 'findByFrame') {
        $formframe = array_key_exists('frame', $_GET) ? $_GET['frame'] : '';
        $formframeno = array_key_exists('frameNo', $_GET) ? $_GET['frameNo'] : '';
        include('forms/framesearch.php');
    }*/ //we don't need this, noRiddle
    
    //echo '</div>'; //noRiddle
    //EOC put this on top, noRiddle

    if (is_object($vehicles) == false || $vehicles->row->getName() == '') {
        if ($_GET['ft'] == 'findByVIN') {
            //BOC new url if nothing found, noRiddle
            //echo CommonExtender::FormatLocalizedString('FINDFAILED', $_GET['vin']);
            $my_url = isset($_GET['c']) && $_GET['c'] != '' ? xtc_href_link(FILENAME_CONTENT, 'coID=1000').'?c='.$_GET['c'] : ''; //'/'.$shop_suffix.'/';
            if(CommonExtender::isFeatureSupported($cataloginfo, 'wizardsearch2')) {$my_url .= '&spi2=t';}
            echo '<p style="color:#c00;">'.CommonExtender::FormatLocalizedString('FINDFAILEDVIN', array($_GET['vin'], $my_url)).'</p>';
            //EOC new url if nothing found, noRiddle
        } else {
            echo '<p style="color:#c00;">'.CommonExtender::FormatLocalizedString('FINDFAILED', $_GET['frame'] . '-' . $_GET['frameNo']).'</p>';
        }
    } else {
        $brand_img = (string)$vehicles->row['catalog']; //get parameter for brand image, noRiddle
        
        //echo '<h1>' . CommonExtender::LocalizeString('Cars') . '</h1><br>';
        echo '<h1>'
         //.'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.Config::$nr_icon_arr[$_GET['c']].'" alt ="" />&nbsp;'
         .'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['brand_icon'].'" alt ="'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].'" />&nbsp;'
         .CommonExtender::LocalizeString('Cars')
         .'</h1>';
        
        //BOC put this on top, noRiddle
        $brand_img = (string)$vehicles->row['catalog'];
        //BOC new html, noRiddle
        echo '<div id="main-top cf">';
        echo '<div class="log-and-butt cf">';
        //echo '<div class="brand-logo"><img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/'.Config::$nr_icon_arr[$brand_img].'" alt ="" /></div>';
        echo '<div class="top-buttons cf">';
        /*echo '<a href="/katalog_shop_test/"><span class="btn button-catalogue"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_brand_choice').'</span></a>&nbsp;'
             .'<a href="'.xtc_href_link(FILENAME_CATALOGUE_SHOPPING_CART).'"><span class="btn button-catalogue"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_shopp_cart').'</span></a>';*/
        echo '</div>';
        echo '</div>';
        echo '</div>';
        //EOC new html, noRiddle

        // Create data renderer
        $renderer = new GuayaquilVehiclesList(new VehiclesExtender());
        $renderer->columns = array('name', 'date', 'datefrom', 'dateto', 'model', 'framecolor', 'trimcolor', 'modification', 'grade', 'frame', 'engine', 'engineno', 'transmission', 'doors', 'manufactured', 'options', 'creationregion', 'destinationregion', 'description', 'remarks');

        $renderer->qg = !$cataloginfo ? -1 : (CommonExtender::isFeatureSupported($cataloginfo, 'quickgroups') ? 1 : 0);

        // Draw data
        echo $renderer->Draw($catalogCode, $vehicles);
        
        if (($cataloginfo && CommonExtender::isFeatureSupported($cataloginfo, 'vinsearch')) || ($findType == 'findByVIN')) {
            $formvin = array_key_exists('vin', $_GET) ? $_GET['vin'] : '';
            //BOC temporarily no VIN search for..., 17-07-2020, noRiddle, handle this in array in cebtral config, 10-2020, noRiddle
            if(!in_array($_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'], $remote_url[$cat_env]['disallow_vin_search'])) {
                echo '<div class="vin-search">'; //noRiddle
                include('forms/vinsearch.php');
                echo '</div>'; //noRiddle
            }
            //EOC temporarily no VIN search for..., 17-07-2020, noRiddle
        }
        //echo '<pre>'.$vehicles->row['catalog'].'</pre>';
        //echo '<pre>'.print_r($vehicles, true).'</pre>';
    }
    
    //BOC write statistic values of searches by clients in database, noRiddle
    include('nr_catalogue_files/nr_includes/write_stat_for_search.php');
    //EOC write statistic values of searches by clients in database, noRiddle
    //BOC keep track of credits for VIN search, noRiddle
    if(isset($_GET['ft']) && $_GET['ft'] == 'findByVIN' && isset($_GET['vin']) && $_GET['vin'] != '') {
        //BOC store searched VIN in session to prevent credit substraction in case same VIN is searched for (e.g. browser back button), 25-07-2018, noRiddle
        if((isset($_SESSION['searched_vin']) && $_SESSION['searched_vin'] != $_GET['vin']) || !isset($_SESSION['searched_vin'])) { 
            if(is_object($vehicles) && $vehicles->row->getName() != '') { //don't substract credit if VIN not found, noRiddle
                $nr_credit_qu_str = "SELECT catalog_credit FROM ".TABLE_CUSTOMERS." WHERE customers_id = ".(int)$_SESSION['customer_id'];
                $nr_credit_qu = xtc_db_query($nr_credit_qu_str);
                $nr_credit_arr = xtc_db_fetch_array($nr_credit_qu);
                if($nr_credit_arr['catalog_credit'] > 0) {
                    $nr_credit_updstr = "UPDATE ".TABLE_CUSTOMERS." SET catalog_credit = (catalog_credit - 1) WHERE customers_id = ".(int)$_SESSION['customer_id'];
                    xtc_db_query($nr_credit_updstr);
                    $_SESSION['searched_vin'] = $_GET['vin']; //store VIN in session, 25-07-2018, noRiddle
                } else {
                    //do we have search by car params ? (stored in session in catalogs.php), noRiddle
                    //$nr_have_spi = (isset($_SESSION['nr_ctlg_brands'][$_GET['c']]['have_parame_search']) && $_SESSION['nr_ctlg_brands'][$_GET['c']]['have_parame_search'] == 'yes') ? '&spi2=t' : '';
                    $nr_have_spi = CommonExtender::isFeatureSupported($cataloginfo, 'wizardsearch2') ? '&spi2=t' : '';
                    xtc_redirect(xtc_href_link(FILENAME_CONTENT, 'coID=1000&c='.$_GET['c'].$nr_have_spi));
                }
            }
        }
        //EOC store searched VIN in session to prevent credit substraction in case same VIN is searched for (e.g. browser back button), 25-07-2018, noRiddle
    }
    //EOC keep track of credits for VIN search, noRiddle
}
//var_dump($data);
//echo '<pre>'.print_r($data, true).'</pre>';
?>
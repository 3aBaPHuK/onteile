<?php
/* -----------------------------------------------------------------------------------------
   $Id: invoice.php 998 2005-07-07 14:18:20Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com
   (c) 2003	 nextcommerce (invoice.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_INVOICE_TEXT_DESCRIPTION', 'Factura');
define('MODULE_PAYMENT_INVOICE_TEXT_TITLE', 'Factura');
define('MODULE_PAYMENT_INVOICE_TEXT_INFO', '');
define('MODULE_PAYMENT_INVOICE_STATUS_TITLE' , 'Activar módulo de factura');
define('MODULE_PAYMENT_INVOICE_STATUS_DESC' , '¿Quiere aceptar pagos por factura?');
define('MODULE_PAYMENT_INVOICE_ORDER_STATUS_ID_TITLE' , 'Determinar estado del pedido');
define('MODULE_PAYMENT_INVOICE_ORDER_STATUS_ID_DESC' , 'Cambiar los pedidos realizados con este módulo a este estado');
define('MODULE_PAYMENT_INVOICE_SORT_ORDER_TITLE' , 'Orden de visualización');
define('MODULE_PAYMENT_INVOICE_SORT_ORDER_DESC' , 'Orden de la visualización. Primero se muestra el dígito más pequeño.');
define('MODULE_PAYMENT_INVOICE_ZONE_TITLE' , 'Zona del pago');
define('MODULE_PAYMENT_INVOICE_ZONE_DESC' , 'Si se selecciona una zona, el método de pago se aplica sólo a esa zona.');
define('MODULE_PAYMENT_INVOICE_ALLOWED_TITLE' , 'Zonas permititdas');
define('MODULE_PAYMENT_INVOICE_ALLOWED_DESC' , 'Introduzca <b>individualmente</b>  las zonas permitidas para este módulo. (por ejemplo AT,DE (si está vacío, se permiten todas las zonas))');
define('MODULE_PAYMENT_INVOICE_MIN_ORDER_TITLE' , 'Pedidos necesarios');
define('MODULE_PAYMENT_INVOICE_MIN_ORDER_DESC' , 'El número mínimo de pedidos que un cliente debe tener para que la opción esté disponible.');
define('MODULE_PAYMENT_INVOICE_MIN_ORDER_STATUS_ID_TITLE' , 'Estado del pedido Número de pedidos');
define('MODULE_PAYMENT_INVOICE_MIN_ORDER_STATUS_ID_DESC' , 'Estado del pedido de un pedido para que se pueda utilizar para calcular el número de pedidos realizados.');
define('MODULE_PAYMENT_INVOICE_MAX_AMOUNT_TITLE' , 'Importe máximo');
define('MODULE_PAYMENT_INVOICE_MAX_AMOUNT_DESC' , 'Introduzca aquí el importe máximo para el pago por factura.');
?>

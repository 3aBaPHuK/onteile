<?php
/* -----------------------------------------------------------------------------------------
   $Id: freeamount.php 4855 2013-06-03 12:15:20Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce( freeamount.php,v 1.01 2002/01/24 03:25:00); www.oscommerce.com 
   (c) 2003	 nextcommerce (freeamount.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   freeamountv2-p1         	Autor:	dwk

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_FREEAMOUNT_TEXT_TITLE', 'Sans frais de port');
define('MODULE_SHIPPING_FREEAMOUNT_TEXT_DESCRIPTION', 'Livraison gratuite');
define('MODULE_SHIPPING_FREEAMOUNT_TEXT_WAY', 'A partir d\'une commande de %s nous livrons votre commande gratuitement');
define('MODULE_SHIPPING_FREEAMOUNT_INVALID_ZONE', 'Malheureusement l\'envoi n\'est pas possible dans ce pays');
define('MODULE_SHIPPING_FREEAMOUNT_SORT_ORDER', 'Ordre de tri');

define('MODULE_SHIPPING_FREEAMOUNT_ALLOWED_TITLE' , 'Zones d\'envoi permises');
define('MODULE_SHIPPING_FREEAMOUNT_ALLOWED_DESC' , 'Veuillez indiquer les zones <b>individuellement</b> pour lesquelles l\'envoi est possible. Par exemple AT,DE (Laissez ce champ libre si vous voulez permettre toutes les zones)');
define('MODULE_SHIPPING_FREEAMOUNT_STATUS_TITLE' , 'Activer la livraison gratuite');
define('MODULE_SHIPPING_FREEAMOUNT_STATUS_DESC' , 'Voulez-vous proposer la livraison gratuite?');
define('MODULE_SHIPPING_FREEAMOUNT_DISPLAY_TITLE' , 'Activer l\'affichage');
define('MODULE_SHIPPING_FREEAMOUNT_DISPLAY_DESC' , 'Voulez-vous afficher quand le montant minimum pour la livraison gratuite n\'est pas atteint?');
define('MODULE_SHIPPING_FREEAMOUNT_ZONE_TITLE' , 'Zone d\'envoi');
define('MODULE_SHIPPING_FREEAMOUNT_ZONE_DESC' , 'Si vous choisissez une zone ce type d\'envoi est seulement porposé dans cette zone.');
define('MODULE_SHIPPING_FREEAMOUNT_SORT_ORDER_TITLE' , 'Ordre de tri');
define('MODULE_SHIPPING_FREEAMOUNT_SORT_ORDER_DESC' , 'Ordre d\'affichage');
define('MODULE_SHIPPING_FREEAMOUNT_NUMBER_ZONES_TITLE' , 'Nombre de zones');
define('MODULE_SHIPPING_FREEAMOUNT_NUMBER_ZONES_DESC' , 'Nombre des zones à disposition');
define('MODULE_SHIPPING_FREEAMOUNT_DISPLAY_TITLE' , 'Activer l\'affichage');
define('MODULE_SHIPPING_FREEAMOUNT_DISPLAY_DESC' , 'Voulez-vous afficher quand l\'envoi dans le pays n\'est pas possible ou quand les frais de port ne peuvent pas être calculés?');

for ($module_shipping_freeamount_i = 1; $module_shipping_freeamount_i <= MODULE_SHIPPING_FREEAMOUNT_NUMBER_ZONES; $module_shipping_freeamount_i ++) {
define('MODULE_SHIPPING_FREEAMOUNT_COUNTRIES_'.$module_shipping_freeamount_i.'_TITLE' , '<hr/>Zone '.$module_shipping_freeamount_i.' Pays');
define('MODULE_SHIPPING_FREEAMOUNT_COUNTRIES_'.$module_shipping_freeamount_i.'_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone '.$module_shipping_freeamount_i.' (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_FREEAMOUNT_AMOUNT_'.$module_shipping_freeamount_i.'_TITLE' , 'Zone '.$module_shipping_freeamount_i.' Montant minimum');
define('MODULE_SHIPPING_FREEAMOUNT_AMOUNT_'.$module_shipping_freeamount_i.'_DESC' , 'Commande minimum pour la zone '.$module_shipping_freeamount_i.' pour que la livraison soit gratuite.');
}
?>

<?php
/***********************************************************************************
* file: nr_search_result_help.php
* use: redirect to certain content if product is not found (see search_results.php)
*
***********************************************************************************/

if (GROUP_CHECK == 'true') {
    $s_coid = '';
    $cont_permission_query1 = xtc_db_query("SELECT group_ids FROM ".TABLE_CONTENT_MANAGER." WHERE content_group = 7655 AND languages_id = '".(int)$_SESSION['languages_id']."'");
    $cont_permission_query2 = xtc_db_query("SELECT group_ids FROM ".TABLE_CONTENT_MANAGER." WHERE content_group = 7656 AND languages_id = '".(int)$_SESSION['languages_id']."'");
    $cont_permission1 = xtc_db_fetch_array($cont_permission_query1);
    $cont_permission2 = xtc_db_fetch_array($cont_permission_query2);
    $cont_perm1_arr = explode(',', rtrim($cont_permission1['group_ids'], ','));
    $cont_perm2_arr = explode(',', rtrim($cont_permission2['group_ids'], ','));
    $cont_perm1_arr = array_map('trim', $cont_perm1_arr);
    $cont_perm2_arr = array_map('trim', $cont_perm2_arr);

    $group = 'c_'.$_SESSION['customers_status']['customers_status_id'].'_group';
    if(in_array($group, $cont_perm1_arr)) {
        $s_coid = '7655';
    } elseif(in_array($group, $cont_perm2_arr)) {
        $s_coid = '7656';
    } else {
        $s_coid = '7655';
    }
} else {
    $s_coid = '7656';
}
$_SESSION['search_errorno'] = $errorno; //will be unset in /shop_content.php
xtc_redirect(xtc_href_link(FILENAME_CONTENT, 'coID='.(int)$s_coid));
?>
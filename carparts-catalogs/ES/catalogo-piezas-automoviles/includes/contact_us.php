<?php
/* -----------------------------------------------------------------------------------------
   $Id: contact_us.php 10579 2017-01-19 11:21:35Z web28 $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2006 XT-Commerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  defined('DISPLAY_PRIVACY_CHECK') or define('DISPLAY_PRIVACY_CHECK', 'true');

  //use contact_us.php language file
  require_once (DIR_WS_LANGUAGES.$_SESSION['language'].'/contact_us.php');

  //BOC define constants and vars for upload, noRiddle
  //BEGINN configuration
  //define('FILENAME_CONTACT_UPLOAD', DIR_FS_CATALOG . 'uploads/'); // directory for uploaded files
  define('FILENAME_CONTACT_UPLOAD', $main_path . ($remote_url[$cat_env]['folder'] != '' ? $remote_url[$cat_env]['folder'].'/' : '') . constant('ENVIR_'.strtoupper($cat_env).'_UPLOAD_DIR') . '/'); // directory for uploaded files //var $path from /ah2.0/config.php
  define('CONTACT_UPLOAD_MAX_SIZE', '4000000'); // = Bytes
  $extension_array = array('jpg', 'jpeg', 'pdf'); // allowed file extensions
  $allowed_mime = array('image/jpeg', 'application/pdf'); //allowed MIME types
  $upld_length = 100; // allowed no. of characters for filename (without extension)
  //END configuration
  
  if(CONTACT_UPLOAD_MAX_SIZE >= 1000000) {
        $max_picsize = (float)CONTACT_UPLOAD_MAX_SIZE/1000000 . ' MB';
    } else {
        $max_picsize = (int)CONTACT_UPLOAD_MAX_SIZE/1000 . ' KB';
    }
    $extensions = $extension_array;
    $ext_write = implode(', ', $extensions);
  //EOC define constants and vars for upload, noRiddle

  // captcha
  $use_captcha = array('contact');
  if (defined('MODULE_CAPTCHA_ACTIVE')) {
    $use_captcha = explode(',', MODULE_CAPTCHA_ACTIVE);
  }
  defined('MODULE_CAPTCHA_CODE_LENGTH') or define('MODULE_CAPTCHA_CODE_LENGTH', 6);
  defined('MODULE_CAPTCHA_LOGGED_IN') or define('MODULE_CAPTCHA_LOGGED_IN', 'True');
  
  $action = isset($_GET['action']) && $_GET['action'] != '' ? $_GET['action'] : '';
  $privacy = isset($_POST['privacy']) && $_POST['privacy'] == 'privacy' ? true : false;
  //BOC new field for "send mail to client himself", noRiddle
  $send_me_mail = isset($_POST['send_me_mail']) && $_POST['send_me_mail'] == 'send_me_mail' ? true : false;
  //EOC new field for "send mail to client himself", noRiddle
  
  $error = false;
  if ($action == 'send') {

    //BOC new for upload, noRiddle
    $err_msg = '';

    if(isset($_FILES['contact_upload']['tmp_name']) && !empty($_FILES['contact_upload']['tmp_name'])) {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mtype = finfo_file($finfo, $_FILES['contact_upload']['tmp_name']);
        finfo_close($finfo);
        
        $imagename = $_FILES['contact_upload']['name'];
        //$imagetype = $_FILES['contact_upload']['type'];
        $imagetype = $mtype;
        $imagesize = $_FILES['contact_upload']['size'];
        $imageerror = $_FILES['contact_upload']['error'];
        $imagetmpname = $_FILES['contact_upload']['tmp_name'];
        if(xtc_not_null($imagetmpname) && ($imagetmpname != 'none') && is_uploaded_file($imagetmpname)) {
            if($imagesize > CONTACT_UPLOAD_MAX_SIZE) {
                $err_msg .= sprintf(CONTACT_ERROR_FILE_SIZE, $max_picsize);
                $error = true;
            }
            if(!preg_match('#^[a-zA-Z0-9\-_]{1,'.$upld_length.'}\.[a-zA-Z0-9]{1,6}$#', $imagename)) {
                $err_msg .=  sprintf(CONTACT_ERROR_FILE_LENGTH_AND_CHARS, $upld_length);
                $error = true;
            }
            if (!in_array(strtolower(substr($imagename, strrpos($imagename, '.')+1)), $extensions) || !in_array($imagetype, $allowed_mime)) {
                $err_msg .= sprintf(CONTACT_ERROR_FILE_EXT, $ext_write);
                $error = true;
            }
        }
    }

    if(isset($err_msg) && $err_msg != '') {
        $messageStack->add('contact_us', $err_msg);
    }
    //EOC new for upload, noRiddle

    $valid_params = array(
      'name',
      'email',
      'message_body',
      'company',
      'street',
      'postcode',
      'city',
      'phone',
      'fax',
      'vehicle_ident', //added vehicle_ident, noRiddle
    );

    // prepare variables
    foreach ($_POST as $key => $value) {
      if (!is_object(${$key}) && in_array($key , $valid_params)) {
        ${$key} = xtc_db_prepare_input($value);
      }
    }

    //jedes Feld kann hier auf die gewŁnschte Bedingung getestet und eine Fehlermeldung zugeordnet werden
    if (!xtc_validate_email(trim($email))) {
      $messageStack->add('contact_us', ERROR_EMAIL);
      $error = true;
    }
    
    //BOC error if VIN not indicated or not plausible, noRiddle
    //if(isset($_GET['products_model']) || isset($_GET['products_id'])) {
        if(trim($vehicle_ident) == '') {
            $messageStack->add('contact_us', AH_ERROR_VIN_EMPTY);
            $error = true;
        //} elseif($vehicle_ident != '' && ($vehicle_ident != 'no' && (strlen($vehicle_ident) != 17 || ctype_alnum($vehicle_ident) === false))) {
        } elseif(strlen($vehicle_ident) != 17 || ctype_alnum($vehicle_ident) === false) {
            $messageStack->add('contact_us', AH_ERROR_VIN_NOPLAUSIBLE);
            $error = true;
        }
    //}
    //EOC error if VIN not indicated or not plausible, noRiddle
    
    if (in_array('contact', $use_captcha) && (!isset($_SESSION['customer_id']) || MODULE_CAPTCHA_LOGGED_IN == 'True')) {
      if (!isset($_SESSION['vvcode'])
          || !isset($_POST['vvcode'])
          || $_SESSION['vvcode'] == ''
          || $_POST['vvcode'] == ''
          || strtoupper($_POST['vvcode']) != $_SESSION['vvcode']
          ) 
      {
        $messageStack->add('contact_us', ERROR_VVCODE);
        $error = true;
      }
      unset($_SESSION['vvcode']);
    }
    
    if (trim($message_body) == '') {
      $messageStack->add('contact_us', ERROR_MSG_BODY);
      $error = true;
    }

    if (DISPLAY_PRIVACY_CHECK == 'true' && empty($privacy)) {
      $messageStack->add('contact_us', ENTRY_PRIVACY_ERROR);
      $error = true;
    }

    if ($messageStack->size('contact_us') > 0) {
      $messageStack->add('contact_us', ERROR_MAIL);
      $smarty->assign('error_message', $messageStack->output('contact_us'));
    }

    //Wenn kein Fehler Email formatieren und absenden
    if ($error === false) {
      // Datum und Uhrzeit
      $datum = date("d.m.Y");
      $uhrzeit = date("H:i");

      $additional_fields = '';
      if (isset($company))  $additional_fields =  EMAIL_COMPANY. $company . "\n" ;
      if (isset($street))   $additional_fields .= EMAIL_STREET . $street . "\n" ;
      if (isset($postcode)) $additional_fields .= EMAIL_POSTCODE . $postcode . "\n" ;
      if (isset($city))     $additional_fields .= EMAIL_CITY . $city . "\n" ;
      if (isset($phone))    $additional_fields .= EMAIL_PHONE . $phone . "\n" ;
      if (isset($fax))      $additional_fields .= EMAIL_FAX . $fax . "\n" ;
      //BOC for product question, noRiddle
      if (isset($_POST['products_name_field'])) $additional_fields .= EMAIL_PRODUCTS_PRICE_QUESTION . $_POST['products_name_field'] . "\n";
      //EOC for product question, noRiddle
      //BOC VIN, noRiddle
      if(isset($vehicle_ident)) $additional_fields .= EMAIL_VIN . $vehicle_ident . "\n" ;
      //EOC VIN, noRiddle

      //BOC new for upload, noRiddle
      //$attach_files = array();
      if(xtc_not_null($imagetmpname) && ($imagetmpname != 'none') && is_uploaded_file($imagetmpname)) {
        if ($imageerror == UPLOAD_ERR_OK) {
            //$new_img = time(). '_' . $imagename;
            //$new_img = date('j-m-y_H:i'). '_' .$imagename;
            if(isset($_SESSION['customer_id'])) {
                $new_img = $_SESSION['customer_id'].'_'.date('j-m-y-H:i').'_'.time().'_'.$imagename;
            } else {
                $new_img = '0_'.date('j-m-y-H:i').'_'.time().'_'.$imagename;
            }
            //BOC create subdir with name of customers_id
            if(isset($_SESSION['customer_id'])) {
                $upl_cust_id = $_SESSION['customer_id'];
                if(!is_dir(FILENAME_CONTACT_UPLOAD.'cust_'.$upl_cust_id)) {
                    mkdir(FILENAME_CONTACT_UPLOAD.'cust_'.$upl_cust_id, 0755);
                }
            } else {
                $upl_cust_id = '0';
            }
            //EOC create subdir with name of customers_id
            
            //move uploaded file
            //move_uploaded_file($imagetmpname, FILENAME_CONTACT_UPLOAD . $new_img);
            move_uploaded_file($imagetmpname, FILENAME_CONTACT_UPLOAD . 'cust_' . $upl_cust_id . '/' . $new_img);
            
            $attach_file = FILENAME_CONTACT_UPLOAD . 'cust_' . $upl_cust_id . '/' . $new_img;
        }
      }
      //EOC new for upload, noRiddle

      if (file_exists(DIR_FS_DOCUMENT_ROOT.'templates/'.CURRENT_TEMPLATE.'/mail/'.$_SESSION['language'].'/contact_us.html') 
          && file_exists(DIR_FS_DOCUMENT_ROOT.'templates/'.CURRENT_TEMPLATE.'/mail/'.$_SESSION['language'].'/contact_us.txt')
          ) 
      {
        $smarty->assign('language', $_SESSION['language']);
        $smarty->assign('tpl_path', HTTP_SERVER.DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/');    
        $smarty->assign('logo_path', HTTP_SERVER.DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/img/');
        $smarty->assign('NAME', $name);
        $smarty->assign('EMAIL', $email);
        $smarty->assign('DATE', $datum);
        $smarty->assign('TIME', $uhrzeit);
        $smarty->assign('ADDITIONAL_FIELDS', nl2br($additional_fields));
        $smarty->assign('MESSAGE', nl2br($message_body));
     
        // dont allow cache
        $smarty->caching = false;
     
        $html_mail = $smarty->fetch(CURRENT_TEMPLATE.'/mail/'.$_SESSION['language'].'/contact_us.html');
        $txt_mail = $smarty->fetch(CURRENT_TEMPLATE.'/mail/'.$_SESSION['language'].'/contact_us.txt');
        $txt_mail = str_replace(array('<br />', '<br/>', '<br>'), '', $txt_mail);
      } else {
        $txt_mail = sprintf(EMAIL_SENT_BY, CONTACT_US_NAME, CONTACT_US_EMAIL_ADDRESS, $datum , $uhrzeit) . "\n" .
                "--------------------------------------------------------------" . "\n" .
                EMAIL_NAME. $name . "\n" .
                EMAIL_EMAIL. trim($email) . "\n" .
                $additional_fields .
                "\n".EMAIL_MESSAGE."\n ". $message_body . "\n";
        $html_mail = nl2br($txt_mail);
      }
      
      if (defined('MODULE_CONTACT_US_STATUS') && MODULE_CONTACT_US_STATUS == 'true') {
        require_once (DIR_FS_INC.'ip_clearing.inc.php');

        $sql_data_array = array(
          'customers_id' => (int)$_SESSION['customer_id'],
          'customers_name' => $name,
          'customers_email_address' => $email,
          'customers_ip' => ip_clearing($_SESSION['tracking']['ip']),
          'date_added' => 'now()',
        );
        xtc_db_perform('contact_us_log', $sql_data_array);
      }

      //BOC added to e-mail subject if price question, noRiddle
      if(isset($_GET['products_model'])) {
        $cont_us_subj = CONTACT_US_EMAIL_SUBJECT.' | '.AH_MAIL_PRICE_QUESTION_SUBJ.' '.$_POST['products_name_field'];
      } else {
        $cont_us_subj = CONTACT_US_EMAIL_SUBJECT;
      }
      //EOC added to e-mail subject if price question, noRiddle

      xtc_php_mail(CONTACT_US_EMAIL_ADDRESS,
                   CONTACT_US_NAME,
                   CONTACT_US_EMAIL_ADDRESS,
                   CONTACT_US_NAME,
                   CONTACT_US_FORWARDING_STRING.(($send_me_mail === true) ? ','.$email : ''), //was CONTACT_US_FORWARDING_STRING, | added customer mail if checkbox is checked, noRiddle
                   trim($email),
                   $name,
                   $attach_file, //'', attach uploaded file, noRiddle
                   '',
                   $cont_us_subj, //was CONTACT_US_EMAIL_SUBJECT, changed to above defined variable, noRiddle
                   $html_mail,
                   $txt_mail
                   );

      xtc_redirect(xtc_href_link(FILENAME_CONTENT, 'action=success&coID='.(int) $_GET['coID']));
    }
  }

  $smarty->assign('CONTACT_HEADING', $shop_content_data['content_heading']);
  if (isset ($_GET['action']) && ($_GET['action'] == 'success')) {
    $smarty->assign('success', '1');
    $smarty->assign('BUTTON_CONTINUE', '<a href="'.xtc_href_link(FILENAME_DEFAULT).'">'.xtc_image_button('button_continue.gif', IMAGE_BUTTON_CONTINUE).'</a>');

  } else {
    if ($shop_content_data['content_file'] != '') {
      ob_start();
      if (strpos($shop_content_data['content_file'], '.txt'))
        echo '<pre>';
      include (DIR_FS_CATALOG.'media/content/'.$shop_content_data['content_file']);
      if (strpos($shop_content_data['content_file'], '.txt'))
        echo '</pre>';
      $contact_content = ob_get_contents();
      ob_end_clean();
    } else {
      $contact_content = $shop_content_data['content_text'];
    }
    
    require (DIR_WS_INCLUDES.'header.php');

    if (isset ($_SESSION['customer_id']) && $action == '') {
      $c_query = xtc_db_query("SELECT c.customers_email_address,
                                      c.customers_telephone,
                                      c.customers_fax,
                                      ab.entry_company,
                                      ab.entry_street_address,
                                      ab.entry_city,
                                      ab.entry_postcode
                                 FROM ".TABLE_CUSTOMERS." c
                                 JOIN ".TABLE_ADDRESS_BOOK." ab
                                      ON ab.customers_id = c.customers_id
                                         AND ab.address_book_id = c.customers_default_address_id
                                WHERE c.customers_id = '".(int)$_SESSION['customer_id']."'");
      $c_data  = xtc_db_fetch_array($c_query);
      $c_data = array_map('stripslashes', $c_data);
      $name = $_SESSION['customer_first_name'].' '.$_SESSION['customer_last_name'];
      $email = $c_data['customers_email_address'];
      $phone = $c_data['customers_telephone'];
      $fax = $c_data['customers_fax'];
      $company = $c_data['entry_company'];
      $street = $c_data['entry_street_address'];
      $postcode = $c_data['entry_postcode'];
      $city = $c_data['entry_city'];
    } elseif ($action == '') {
    	$name = '';
    	$email = '';
    	$phone = '';
    	$company = '';
    	$street = '';
    	$postcode = '';
    	$city = '';
    	$fax = '';
    }

    $products_info = '';
    if (isset($_GET['products_id']) && $_GET['products_id']  && isset($_GET['inq']) && $_GET['inq']) {
      $product_inq = new product((int)$_GET['products_id']);
      $products_info = defined('PRODUCT_INQUIRY') ? PRODUCT_INQUIRY . "\n" : '';
      $products_info .= HEADER_ARTICLE . ': '. $product_inq->data['products_name'] . "\n";  
      $products_info .= ($product_inq->data['products_model'] ? HEADER_MODEL . ': ' .$product_inq->data['products_model'] : '') . "\n";
    }
    if (!$error) $message_body = $products_info . "\n";

    //BOC set value for enquiry of product, noRiddle
    $product_field_info = '';
    if(isset($_GET['products_model']) && !empty($_GET['products_model'])) {
        $product_field_info .= trim($_GET['products_name']) . ' - ' . trim($_GET['products_model']);
    }
    //EOC set value for enquiry of product, noRiddle

    $smarty->assign('CONTACT_CONTENT', $contact_content);
    //BOC new form tag to keep GET params and for upload field, noRiddle
    //$smarty->assign('FORM_ACTION', xtc_draw_form('contact_us', xtc_href_link(FILENAME_CONTENT, 'action=send&coID='.(int) $_GET['coID'], 'SSL')));
    $con_prid = isset($_GET['products_id']) && !empty($_GET['products_id']) ? (int)$_GET['products_id'] : '';
    $con_prodname = isset($_GET['products_name']) && !empty($_GET['products_name']) ? $_GET['products_name'] : '';
    $con_prodmodel = isset($_GET['products_model']) && !empty($_GET['products_model']) ? $_GET['products_model'] : '';
    $smarty->assign('FORM_ACTION', xtc_draw_form('contact_us', xtc_href_link(FILENAME_CONTENT, 'action=send&coID='.(int) $_GET['coID'] . ($con_prid != '' ? '&products_id='.$con_prid : '') . ($con_prodname != '' ? '&products_name='.$con_prodname : '') . ($con_prodmodel != '' ? '&products_model='.$con_prodmodel : ''), 'SSL'), 'post', 'enctype="multipart/form-data"'));
    //EOC new form tag to keep GET params and for upload field, noRiddle

    if (in_array('contact', $use_captcha) && (!isset($_SESSION['customer_id']) || MODULE_CAPTCHA_LOGGED_IN == 'True')) {
      $smarty->assign('VVIMG', '<img src="'.xtc_href_link(FILENAME_DISPLAY_VVCODES, '', 'SSL').'" alt="Captcha" />');
      $smarty->assign('INPUT_CODE', xtc_draw_input_field('vvcode', '', 'size="'. MODULE_CAPTCHA_CODE_LENGTH .'" maxlength="'.MODULE_CAPTCHA_CODE_LENGTH.'"', 'text', false));
    }
    if (DISPLAY_PRIVACY_CHECK == 'true') {
      $smarty->assign('PRIVACY_CHECKBOX', xtc_draw_checkbox_field('privacy', 'privacy', $privacy, 'id="privacy"'));
      $smarty->assign('PRIVACY_LINK', $main->getContentLink(2, MORE_INFO, $request_type));
    }
    //BOC new field for "send mail to client himself", noRiddle
    $smarty->assign('SEND_ME_MAIL', xtc_draw_checkbox_field('send_me_mail', 'send_me_mail', $send_me_mail, 'id="send_me_mail"'));
    //EOC new field for "send mail to client himself", noRiddle
    $smarty->assign('INPUT_NAME', xtc_draw_input_field('name', $name, 'size="30"'));
    $smarty->assign('INPUT_EMAIL', xtc_draw_input_field('email', $email, 'size="30"'));
    $smarty->assign('INPUT_PHONE', xtc_draw_input_field('phone', $phone, 'size="30"'));
    $smarty->assign('INPUT_COMPANY', xtc_draw_input_field('company', $company, 'size="30"'));
    $smarty->assign('INPUT_STREET', xtc_draw_input_field('street', $street, 'size="30"'));
    $smarty->assign('INPUT_POSTCODE', xtc_draw_input_field('postcode', $postcode, 'size="30"'));
    $smarty->assign('INPUT_CITY', xtc_draw_input_field('city', $city, 'size="30"'));
    $smarty->assign('INPUT_FAX', xtc_draw_input_field('fax', $fax, 'size="30"'));
    //BOC assign new files, VIN and  products_name_field, noRiddle
    if(isset($_GET['products_model'])) {
        $smarty->assign('INPUT_PRODUCT', xtc_draw_input_field('products_name_field', $product_field_info, 'size="60"'));
    }
    //BOC populate field with VIN from catalogue if available (set in vehicles.php of catalogue), noRiddle
    //$smarty->assign('INPUT_VIN', xtc_draw_input_field('vehicle_ident', $vehicle_ident, 'size="17"'));
    if(isset($_SESSION['vin_val_catalog'])) {
        $vin_id = $_SESSION['vin_val_catalog'];
        unset($_SESSION['vin_val_catalog']);
    } else {
        $vin_id = $vehicle_ident;
    }
    $smarty->assign('INPUT_VIN', xtc_draw_input_field('vehicle_ident', $vin_id, 'size="17"'));
    //EOC populate filed with VIN from catalogue if available (set in vehicles.php of catalogue), noRiddle
    //EOC assign new file-, VIN-field and  products_name_field, noRiddle
    //BOC new for upload file, noRiddle
    $smarty->assign('INPUT_UPLOAD', xtc_draw_input_field('contact_upload', '', '', 'file', true));
    $smarty->assign('UPLOAD_TEXT', sprintf(CONTACT_UPLOAD_TEXT, $ext_write, $max_picsize));
    //EOC new for upload file, noRiddle
    $smarty->assign('INPUT_TEXT', xtc_draw_textarea_field('message_body', 'soft', 45, 15, $message_body));
    $smarty->assign('BUTTON_SUBMIT', xtc_image_submit('button_send.gif', IMAGE_BUTTON_SEND));
    $smarty->assign('FORM_END', '</form>');
  }

  $smarty->assign('language', $_SESSION['language']);
  $smarty->caching = 0;
  $main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/contact_us.html');
?>
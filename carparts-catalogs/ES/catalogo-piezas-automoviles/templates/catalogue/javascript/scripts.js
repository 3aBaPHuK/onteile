$.alerts.overlayOpacity = .2;
$.alerts.overlayColor = '#000';
function alert(message, title) {
    title = title || 'Information';
    jAlert(message, title);
}
function ac_closing() {
    setTimeout("$('#suggestions').slideUp();", 100);
    ac_page = 1;
}
$(document).ready(function(){
    $(".cbimages").colorbox({rel:'cbimages', scalePhotos:true, maxWidth: "90%", maxHeight: "90%", fixed: true});
    //$(".iframe").colorbox({iframe:true, width:"780", height:"560", maxWidth: "90%", maxHeight: "90%", fixed: true});
    $('body').on('click', '.iframe', function() {$(".iframe").colorbox({iframe:true, width:"780", height:"560", maxWidth: "90%", maxHeight: "90%", fixed: true});}); //also for dynamically added elements, noRiddle
    $(".unveil").show();
    $(".unveil").unveil(200);
    $('select').SumoSelect();
    /* Mark Selected */
    var tmpStr = '';
    $('.filter_bar .SumoSelect').each(function(index){
      ($(this).find('select').val() == '') ? $(this).find('p').removeClass("Selected") : $(this).find('p').addClass("Selected");
    });

    $('.bxcarousel_bestseller').bxSlider({
      minSlides: 2,
      maxSlides: 8,
      pager: ($(this).children('li').length > 1), /*FIX for only one entry*/
      slideWidth: 124,
      slideMargin: 18
    });
    $('.bxcarousel_slider').bxSlider({
      adaptiveHeight: false,
      mode: 'fade',
      auto: true,
      speed: 2000,
      pause: 6000
    });
    $(document).bind('cbox_complete', function(){
      if($('#cboxTitle').height() > 20){
        $("#cboxTitle").hide();
        $("<div>"+$("#cboxTitle").html()+"</div>").css({color: $("#cboxTitle").css('color')}).insertAfter("#cboxPhoto");
        /*$.fn.colorbox.resize(); // Tomcraft - 2016-06-05 - Fix Colorbox resizing*/
      }
    });
});
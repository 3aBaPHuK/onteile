<?php
/*********************************************************
* file: brand_specials.php
* use: display some specials from brand shop
* (c) noRiddle 11-2016
*********************************************************/

//BOC config
$lim = 12; //how many specials to show
//EOC config

//define shop
$the_queried_shop = isset($_GET['c']) && array_key_exists($_GET['c'], $_SESSION['nr_ctlg_brands']) ? $remote_url[$cat_env]['domain'].'/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].$shop_suffix : '';

//BOC functions
/*function close_shop_db_specials() {
    if($shp_db_link != false)
        mysqli_close($shp_db_link);
    $shp_db_link = false;
}*/  //do this in application_top, noRiddle

function get_specials_data() {
    global $shp_db_link, $lim;
    $data_arr = array();
    //$persoff_table = 'personal_offers_by_customers_status_'.strtoupper($_SESSION['customers_status']['customers_status_id']);
    $spec_actual_cust_stat = empty($_SESSION['customers_status']['customers_status_id']) ? DEFAULT_CUSTOMERS_STATUS_ID_GUEST : $_SESSION['customers_status']['customers_status_id']; //this is done in price class as well, why I don't actually know
    $persoff_table = 'personal_offers_by_customers_status_'.$spec_actual_cust_stat;

    //get startpage products
    //TRIM is set in import script on all products_model, we don't need it here; thus index is used and query is faster
    $shp_qu_str = "SELECT p.products_id,
                         #REPLACE(p.products_model,' ','') as products_model,
                          p.products_model,
                          p.products_price,
                          p.products_tax_class_id,
                          p.products_image,
                          p.gm_price_status,
                          pd.products_name,
                          pd.products_description,
                          po.personal_offer,
                          s.specials_new_products_price
                     FROM ".TABLE_PRODUCTS." p
                LEFT JOIN ".TABLE_PRODUCTS_DESCRIPTION." pd
                       ON pd.products_id = p.products_id
                      AND pd.language_id = ".(int)$_SESSION['languages_id']." 
                LEFT JOIN ".$persoff_table." po
                       ON po.products_id = p.products_id AND quantity = '1'
                LEFT JOIN ".TABLE_SPECIALS." s
                       ON s.products_id = p.products_id
                      AND s.status = '1' #only active specials, added 10-2017, noRiddle
                    WHERE p.products_status = '1'
                      AND p.products_startpage = '1'
                      AND p.gm_price_status = '0'
                 ORDER BY p.products_price ASC
                    LIMIT ".(int)$lim;
                      
    //if(is_resource($shp_db_link)) { //gives false, strange
        $shp_qu = mysqli_query($shp_db_link, $shp_qu_str);
        if(!$shp_qu) return '<pre>Query zu Shop fehlgeschlagen: '.mysqli_error($shp_db_link).'</pre>';
        if(mysqli_num_rows($shp_qu)) {
            while($shp_arr = mysqli_fetch_array($shp_qu, MYSQLI_ASSOC)) { 
                $data_arr[] = $shp_arr;
            }
        } else {
            $data_arr = array();
        }
    //}

    return $data_arr;
}


include(DIR_FS_BOXES_INC . 'smarty_default.php');
$box_smarty->caching = 0; //overwrite, we don't want caching here

//echo '<br>br>br><pre>'.print_r($_SESSION['nr_ctlg_brands'], true).'</pre>';
//echo '<br><br><br><pre>array_key_exists($_GET[c], $_SESSION[nr_ctlg_brands]): '.(array_key_exists($_GET['c'], $_SESSION['nr_ctlg_brands']) ? 'true' : 'false').'</pre>';
//echo '<br><br><br><pre>'.$shop_data[$the_shop]['sql_user'].'</pre>';

if($shp_db_link) {
    if(isset($_GET['c']) && array_key_exists($_GET['c'], $_SESSION['nr_ctlg_brands'])) {
        //include_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'nr_catalogue_files/guayaquillib/render/details/shop_dbs/conn_to_shop_db.php'); //do this in application_top, noRiddle
        //echo '<pre>'.print_r($shop_data, true).'<pre>'; //do this in application_top, noRiddle
        //echo '<br><br><br><pre>is_resource($shp_db_link) ? '.(is_resource($shp_db_link) ? 'true' : 'false').'</pre>'; //do this in application_top, noRiddle

        $specials_arr = get_specials_data();
        //echo '<pre>'.print_r($specials_arr, true).'</pre>';
        if(!empty($specials_arr)) {
            //include_once(DIR_WS_CLASSES.'nr_foreign_xtcPrice.php');
            //$nr_foreign_xtcPrice = new nr_foreign_xtcPrice($_SESSION['currency'], $_SESSION['customers_status']['customers_status_id']); //instantiate price class for respective shop

            $special_prod_arr = array();
            //$spec_indx = 0;
            foreach($specials_arr as $key => $prod_arr) {
                $nr_ms_tax_rate = isset($nr_foreign_xtcPrice->TAX[$prod_arr['products_tax_class_id']]) ? $nr_foreign_xtcPrice->TAX[$prod_arr['products_tax_class_id']] : 0;
                $nr_spec_price[$key] = $nr_foreign_xtcPrice->xtcGetPrice($prod_arr['products_id'], $format = true, 1, $prod_arr['products_tax_class_id'], $prod_arr['products_price'], 1);

                $nr_spec_tx_inf = $main->getTaxInfo($nr_ms_tax_rate);
                
                $spec_hidd_price = xtc_draw_hidden_field('prod_price[]', ($prod_arr['specials_new_products_price'] != '' ? $prod_arr['specials_new_products_price'] : ($prod_arr['personal_offer'] != '' ? $prod_arr['personal_offer'] : $prod_arr['products_price'])));

                $special_prod_arr[$key] = array('PRODUCTS_SHOP' => $the_queried_shop,
                                                'PRODUCTS_ID' => $prod_arr['products_id'],
                                                'PRODUCTS_MODEL' => $prod_arr['products_model'],
                                                'PRODUCTS_NAME' => $prod_arr['products_name'],
                                                'PRICE_ALLOWED' => (($_SESSION['customers_status']['customers_status_show_price'] != '0') ? 'true' : 'false'),
                                                'PRODUCTS_TAXINFO' => $nr_spec_tx_inf,
                                                'PRODUCTS_IMAGE' => (!empty($prod_arr['products_image']) ? $prod_arr['products_image'] : 'noimage.gif'), //$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].'-original-teil.jpg'),
                                                'PRODUCTS_FORMSTART' => xtc_draw_form('specials_into_cart', xtc_href_link(FILENAME_CONTENT, xtc_get_all_get_params())),
                                                'PRODUCTS_FORMEND' => '</form>',
                                                'PRODUCTS_QTY' => xtc_draw_input_field('prod_model_qty[]', '1', 'class="clog-detlist-ipt"')
                                                                 .xtc_draw_hidden_field('products_model[]', $prod_arr['products_model'])
                                                                 .xtc_draw_hidden_field('products_id[]', $prod_arr['products_id'])
                                                                 .xtc_draw_hidden_field('products_name[]', $prod_arr['products_name'])
                                                                 .xtc_draw_hidden_field('prod_brand[]', $_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'])
                                                                 .xtc_draw_hidden_field('lax_brand', $_GET['c'])
                                                                 //.xtc_draw_hidden_field('prod_price[]', $nr_spec_price[$key]['netto'])
                                                                 .$spec_hidd_price
                                                                 .xtc_draw_hidden_field('prod_txcl_id[]', $prod_arr['products_tax_class_id']),
                                                'PRODUCTS_CHK' => xtc_draw_hidden_field('check_to_buy[]', $prod_arr['products_model']), //xtc_draw_checkbox_field('check_to_buy[]', $prod_arr['products_model'])
                                                'PRODUCTS_BUTTON' => xtc_image_submit('button_in_cart.gif', IMAGE_BUTTON_IN_CART) //IMAGE_BUTTON_IN_CART
                                               );

                foreach((array)$nr_spec_price[$key] as $spec_key => $spec_entry) {                  
                    $special_prod_arr[$key]['PRODUCTS_PRICE_'.strtoupper($spec_key)] = $spec_entry;
                    $special_prod_arr[$key]['PRODUCTS_PRICE_ARRAY'][0]['PRODUCTS_PRICE_'.strtoupper($spec_key)] = $spec_entry;
                }
                $special_prod_arr[$key]['PRODUCTS_PRICE_ARRAY'][0]['PRICE_ALLOWED'] = $special_prod_arr[$key]['PRICE_ALLOWED'];
            }
            $box_smarty->assign('SPECIALS_HEADING', sprintf(S_SPECIALS_HEADING_TXT, strtoupper($_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'])));
        }

        //close external shop DB
        //close_shop_db_specials();  //do this in application_top, noRiddle
        
        //echo '<br><br><br><pre>'.print_r($special_prod_arr[2], true).'</pre>';
    }
}
if (isset($special_prod_arr) && !empty($special_prod_arr)) {
    $box_smarty->assign('BOX_RESULT', $special_prod_arr);

    $box_brand_specials = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_brand_specials.html');
    $smarty->assign('box_BRAND_SPECIALS', $box_brand_specials);
}
?>
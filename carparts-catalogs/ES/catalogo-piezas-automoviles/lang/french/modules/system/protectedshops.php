<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PROTECTEDSHOPS_TEXT_TITLE', 'Boutiques protégées Auto Updater');
define('MODULE_PROTECTEDSHOPS_TEXT_DESCRIPTION', 'Boutiques protégées - Auto Updater pour textes juridiques automatiques<br/><br/><b>IMPORTANT :</b> avant l\'utilisation du module il est nécessaire d\'indiquer et enregistrer un jeton valide avant que l\'attribution des pages contenu soit possible.<hr noshade>');
define('MODULE_PROTECTEDSHOPS_STATUS_TITLE', 'Statut');
define('MODULE_PROTECTEDSHOPS_STATUS_DESC', 'Statut de module');
define('MODULE_PROTECTEDSHOPS_TOKEN_TITLE', 'Jeton d\'authentification');
define('MODULE_PROTECTEDSHOPS_TOKEN_DESC', 'Jeton d\'authentification que vous trouverez dans le menu du fournisseur de Protected Shops.');
define('MODULE_PROTECTEDSHOPS_TYPE_TITLE', '<hr noshade>Enregistrer sous');
define('MODULE_PROTECTEDSHOPS_TYPE_DESC', 'Enregistrer les données dans un fichier ou dans une base de données ?');
define('MODULE_PROTECTEDSHOPS_FORMAT_TITLE', 'Type format');
define('MODULE_PROTECTEDSHOPS_FORMAT_DESC', 'Enregistrer les données comme texte, HTML ou HtmlLite (saut de ligne, caractères en gras, souligné ou italique) ?');
define('MODULE_PROTECTEDSHOPS_AUTOUPDATE_TITLE', '<hr noshade>Auto-Updater');
define('MODULE_PROTECTEDSHOPS_AUTOUPDATE_DESC', 'Mettre à jour les données automatiquement ?');
define('MODULE_PROTECTEDSHOPS_UPDATE_INTERVAL_TITLE', 'Mis à jour intervalle');
define('MODULE_PROTECTEDSHOPS_UPDATE_INTERVAL_DESC', 'Mettre à jour les données dans quelle intervalle ?');
define('MODULE_PROTECTEDSHOPS_ACTION_TITLE', '<hr noshade>Action');
define('MODULE_PROTECTEDSHOPS_ACTION_DESC', 'Enregistrer les paramètres ou mettre à jour les document manuellement ?<br/><br/><b>Attention :</b> Si les paramètres ont été modifiés, ceux-ci doivent d’abord être enregistrés, avant que la création de documents soit possible.<br/>');
define('TEXT_SAVE', 'Enregistrer paramètres');
define('TEXT_PROCESS', 'Mettre à jour documents');

?>

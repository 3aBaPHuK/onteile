<?php
/* -----------------------------------------------------------------------------------------
   $Id: image_processing_step.php 2992 2012-06-07 16:59:49Z web28 $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com
   (c) 2003	 nextcommerce (invoice.php,v 1.6 2003/08/24); www.nextcommerce.org
   (c) 2006 XT-Commerce (image_processing_step.php 950 2005-05-14; www.xt-commerce.com
   --------------------------------------------------------------
   Contribution
   image_processing_step (step-by-step Variante B) by INSEH 2008-03-26

   new javascript reload / only missing image/ max images  by web28 2011-03-17

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_STEP_IMAGE_PROCESS_TEXT_DESCRIPTION', 'Todas las imágenes en los directorios<br /><br />
/images/product_images/popup_images/<br />
/images/product_images/info_images/<br />
/images/product_images/thumbnail_images/ <br /> <br /> serán recreadas.<br /> <br />
Para ello, el script procesa sólo un número limitado de %s imágenes y se llama a sí mismo de nuevo después.<br /> <br />');
define('MODULE_STEP_IMAGE_PROCESS_TEXT_TITLE', 'Procesamiento de imágenes - Imágenes de productos');
define('MODULE_STEP_IMAGE_PROCESS_STATUS_DESC', 'Estado del módulo');
define('MODULE_STEP_IMAGE_PROCESS_STATUS_TITLE', 'Estado');
define('IMAGE_EXPORT', 'Pulse Iniciar para iniciar el procesamiento de lotes. Este proceso puede tomar algún tiempo - ¡no lo interrumpa bajo ninguna circunstancia!');
define('IMAGE_EXPORT_TYPE', '<hr noshade><strong>procesamiento de lotes:</strong>');

define('IMAGE_STEP_INFO', 'Imágenes creadas: ');
define('IMAGE_STEP_INFO_READY', ' - ¡Listo!');
define('TEXT_MAX_IMAGES', 'máx. imágenes por recarga de página');
define('TEXT_ONLY_MISSING_IMAGES', 'Crear sólo imágenes faltantes');
define('MODULE_STEP_READY_STYLE_TEXT', '<div class="ready_info">%s</div>');
define('MODULE_STEP_READY_STYLE_BACK', MODULE_STEP_READY_STYLE_TEXT);
define('TEXT_LOWER_FILE_EXT', 'Convertir la extensión de archivo a minúsculas, por ejemplo: <b> JPG -> jpg</b>');
define('IMAGE_COUNT_INFO', 'Número de imágenes en %s: %s pcs. ');

define('TEXT_LOGFILE', 'Activar el logging, útil para la localización de fallos. El archivo log se almacena en la carpeta /log en el directorio principal.');

?>

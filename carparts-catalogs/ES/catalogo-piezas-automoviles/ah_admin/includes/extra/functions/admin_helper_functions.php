<?php
/********************************************************************************************
* file: admin_helper_functions.php
* use define needed functions which normally reside in admin//includes/functions/general.php
* (c) noRiddle 05-2017
********************************************************************************************/

//BOC helper configuration functions for ot_bulkgoods.php
function xtc_cfg_pull_down_bulkgoods_costs_method($key = '', $view = 0) {
    $name = 'configuration[MODULE_ORDER_TOTAL_BULKGOODS_METHOD]';
    $bulk_method_array = array ();
    $bulk_method_array[] = array ('id' => '1', 'text' => NR_ADD_UP_ALL_BULKGOODS_COSTS);
    $bulk_method_array[] = array ('id' => '2', 'text' => NR_ADD_UP_ONE_BULKGOODS_COST_PER_ARTICLE);
    $bulk_method_array[] = array ('id' => '3', 'text' => NR_ONLY_HIGHEST_BULKGOODS_COST_PER_SHOPPING_CART);
    return ($view == 1 ? $bulk_method_array[($key - 1)]['text'] : xtc_draw_pull_down_menu($name, $bulk_method_array, $key));
}

function xtc_get_bulkgoods_method($key) {
    return xtc_cfg_pull_down_bulkgoods_costs_method($key, 1);
}
//EOC helper configuration functions for ot_bulkgoods.php
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id: gv_mail.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(gv_mail.php,v 1.5.2.2 2003/04/27); www.oscommerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Envoyer le bon dachat aux clients');

define('TEXT_CUSTOMER', 'Client:');
define('TEXT_SUBJECT', 'Sujet:');
define('TEXT_FROM', 'De:');
define('TEXT_TO', 'Courriel à:');
define('TEXT_AMOUNT', 'Valeur:');
define('TEXT_MESSAGE', 'Message:');
define('TEXT_SINGLE_EMAIL', '<span class="smallText">Utilisez ce champ seulement pour des courriels individuels, sinon utilisez le champ Client</span>');
define('TEXT_SELECT_CUSTOMER', 'Séléctionner le client');
define('TEXT_ALL_CUSTOMERS', 'Tous les clients');
define('TEXT_NEWSLETTER_CUSTOMERS', 'A tous les abonnés de la Newsletter');

define('NOTICE_EMAIL_SENT_TO', 'Remarque: Courriel a été envoyé à: %s');
define('ERROR_NO_CUSTOMER_SELECTED', 'Erreur: Aucun client n\'a été séléctionné.');
define('ERROR_NO_AMOUNT_SELECTED', 'Erreur: Vous n\'avez pas indiqué de montant pour le bon d\'achat.');

define('TEXT_GV_WORTH', 'La valeur du bon d\'achat est de');
define('TEXT_TO_REDEEM', 'Pour comptabiliser le bon d\'achat, cliquez sur le lien ci-dessous. Veuillez noter votre code de coupon individuel pour des raisons de sécurité.');
define('TEXT_WHICH_IS', 'Votre code de coupon est:');
define('TEXT_IN_CASE', 'Si vous rencontrez des problèmes lors de la comptabilisation');
define('TEXT_OR_VISIT', 'visitez notre site web');
define('TEXT_ENTER_CODE', 'et entrez votre code de coupon manuellement');

define ('TEXT_REDEEM_COUPON_MESSAGE_HEADER', 'Sie haben k&uuml;rzlich in unserem Online-Shop einen Gutschein gekauft, welcher aus Sicherheitsgr&uuml;nden nicht sofort freigeschaltet wurde. Dieses Guthaben steht Ihnen nun zur Verf&uuml;gung.');
define ('TEXT_REDEEM_COUPON_MESSAGE_AMOUNT', "\n\n" . 'Der Wert Ihres Gutscheines betr&auml;gt %s');
define ('TEXT_REDEEM_COUPON_MESSAGE_BODY', "\n\n" . 'Sie k&ouml;nnen nun &uuml;ber Ihr pers&ouml;nliches Konto den Gutschein an jemanden versenden.');
define ('TEXT_REDEEM_COUPON_MESSAGE_FOOTER', "\n\n");

?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id: zones.php 5118 2013-07-18 10:58:36Z Tomcraft $   

    modified eCommerce Shopsoftware
    http://www.modified-shop.org

    Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(zones.php,v 1.3 2002/04/17); www.oscommerce.com 
   (c) 2003	 nextcommerce (zones.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2003  xt-commerce.com (zones.php  2005-04-29); www.xt-commerce.com

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_ZONES_TEXT_TITLE', 'Costos de envío por zonas');
define('MODULE_SHIPPING_ZONES_TEXT_DESCRIPTION', 'Costos de envío por zonas');
define('MODULE_SHIPPING_ZONES_TEXT_WAY', 'Envío a:');
define('MODULE_SHIPPING_ZONES_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_ZONES_INVALID_ZONE', 'No es posible realizar un envío a este país.');
define('MODULE_SHIPPING_ZONES_UNDEFINED_RATE', 'Los costos de envío no se pueden calcular por el momento.');

define('MODULE_SHIPPING_ZONES_STATUS_TITLE' , 'Activar el método de costos de envío por zonas');
define('MODULE_SHIPPING_ZONES_STATUS_DESC' , '¿Desea ofrecer costos de envío por zonas?');
define('MODULE_SHIPPING_ZONES_ALLOWED_TITLE' , 'Zonas de envío permitidas');
define('MODULE_SHIPPING_ZONES_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE (deje este campo vacío si desea permitir todas las zonas))');
define('MODULE_SHIPPING_ZONES_TAX_CLASS_TITLE' , 'Clase de impuestos');
define('MODULE_SHIPPING_ZONES_TAX_CLASS_DESC' , 'Aplicar la siguiente clase de impuestos a los costos de envío.');
define('MODULE_SHIPPING_ZONES_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_ZONES_ZONE_DESC' , 'Si selecciona una zona, este modo de envío sólo se ofrecerá en esa zona.');
define('MODULE_SHIPPING_ZONES_SORT_ORDER_TITLE' , 'Orden de la visualización');
define('MODULE_SHIPPING_ZONES_SORT_ORDER_DESC' , 'Orden de la visualización');
define('MODULE_SHIPPING_ZONES_NUMBER_ZONES_TITLE' , 'Número de zonas');
define('MODULE_SHIPPING_ZONES_NUMBER_ZONES_DESC' , 'Número de zonas proporcionadas');
define('MODULE_SHIPPING_ZONES_DISPLAY_TITLE' , 'Activar la visualización');
define('MODULE_SHIPPING_ZONES_DISPLAY_DESC' , '¿Desea mostrar si no es posible realizar envíos a ese país o si no se pueden calcular los costos de envío?');

for ($module_shipping_zones_i = 1; $module_shipping_zones_i <= MODULE_SHIPPING_ZONES_NUMBER_ZONES; $module_shipping_zones_i ++) {
define('MODULE_SHIPPING_ZONES_COUNTRIES_'.$module_shipping_zones_i.'_TITLE' , '<hr/>Zona '.$module_shipping_zones_i.' países');
define('MODULE_SHIPPING_ZONES_COUNTRIES_'.$module_shipping_zones_i.'_DESC' , 'Lista separada por comas de los códigos de países ISO de 2 caracteres que forman parte de la zona '.$module_shipping_zones_i.' (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_ZONES_COST_'.$module_shipping_zones_i.'_TITLE' , 'Zona '.$module_shipping_zones_i.' costos de envío');
define('MODULE_SHIPPING_ZONES_COST_'.$module_shipping_zones_i.'_DESC' , 'Costos de envío por zona '.$module_shipping_zones_i.' Destinos, basados en un grupo de pesos máximos de pedido o en el valor de la cesta de la compra, dependiendo de la configuración del módulo. Ejemplo: 3:8.50,7:10.50,.... Un peso/precio inferior o igual a 3 costaría 8,50 para los países de destino de la zona '.$module_shipping_zones_i);
define('MODULE_SHIPPING_ZONES_HANDLING_'.$module_shipping_zones_i.'_TITLE' , 'Zona '.$module_shipping_zones_i.' Recargo de manejo');
define('MODULE_SHIPPING_ZONES_HANDLING_'.$module_shipping_zones_i.'_DESC' , 'Recargo de manejo para esta zona de envío');
}
?>

<?php
/********************************************************************************************************
* file: generate_shoppingcart_link.php for use in backend of shop
* form to generate Link to put arcticles directly in shopping cart
* function of builded link depends on modification in /includes/cart_actions.php
* !! no attributes allowed
* for modified eCommerce 2.0.1 r10403 | 2.0.2 r10591
* (c) copyright, 04-2015 noRiddle
********************************************************************************************************/

//SQL
//ALTER TABLE admin_access ADD generate_shoppingcart_link INT( 1 ) NOT NULL DEFAULT '0';
//UPDATE admin_access SET generate_shoppingcart_link = 1 WHERE customers_id = '1';
//ALTER TABLE admin_access ADD shopping_cart_link_ajax INT( 1 ) NOT NULL DEFAULT '0';
//UPDATE admin_access SET shopping_cart_link_ajax = 1 WHERE customers_id = '1';

require_once ('includes/application_top.php');

//$link_part_1 = HTTP_SERVER.'/'.$shop.'?action=custom&amp;p_id=';
$link_part_1 = HTTP_SERVER.'/'.$shop.'?action=custom_cart&amp;p_id=';
$prod_id_arr = array();
$attr_deoposit = array();
$problem_prod = array();
$shpp_link = '';
$shpp_link_err = false;

if(isset($_POST['gc_art_num'])) {  
    for($i = 0, $count_artnums = count($_POST['gc_art_num']); $i < $count_artnums; $i++) {
        if($_POST['gc_art_num'][$i] != '') {
            //allow entry of manufacturers_model
            $post_artnum_raw[$i] = strtr($_POST['gc_art_num'][$i], [' '=>'', '-'=>'', '_'=>'', '/'=>'']);
            //$post_artnum[$i] = trim($_POST['gc_art_num'][$i]);
            $post_artnum[$i] = trim($post_artnum_raw[$i]);

            $post_pcs[$i] = $_POST['gc_pcs'][$i];
            $prod_id_query[$i] = xtc_db_query("SELECT p.products_id, p.products_status, p.gm_price_status, pa.options_id, pa.options_values_id
                                             FROM ".TABLE_PRODUCTS." p
                                        LEFT JOIN ".TABLE_PRODUCTS_ATTRIBUTES." pa
                                               ON pa.products_id = p.products_id
                                            WHERE products_model = '".xtc_db_input($post_artnum[$i])."'");
            $prod_id[$i] = xtc_db_fetch_array($prod_id_query[$i]);
            if(!empty($prod_id[$i])) {
                //BOC new for shop version 2.0.2.2, add attribute deposit if exists, noRiddle
                if($prod_id[$i]['options_id'] != '' && $prod_id[$i]['options_id'] != 'NULL' && $prod_id[$i]['options_values_id'] == '1') {
                    $attr_deoposit[$i] = '{'.$prod_id[$i]['options_id'].'}'.$prod_id[$i]['options_values_id'];
                }
                $prod_id_arr[] = $prod_id[$i]['products_id'].(isset($attr_deoposit[$i]) ? $attr_deoposit[$i] : '').'-'.$post_pcs[$i];
                //EOC new for shop version 2.0.2.2, add attribute deposit if exists, noRiddle
                if($prod_id[$i]['products_status'] != '1' || $prod_id[$i]['gm_price_status'] != '0') {
                    $problem_prod[$post_artnum[$i]] = array('products_status' => $prod_id[$i]['products_status'],
                                                            'gm_price_status' => $prod_id[$i]['gm_price_status']
                                                           );
                }
            }
        }    
    }
    
    $link_part_2 = implode('~', $prod_id_arr);
    
    if(count($_POST['gc_art_num']) > 0) {
        if(count($_POST['gc_art_num']) == 1 && empty($link_part_2)) {
            $shpp_link .= GSCL_NO_MODEL_NO_TXT;
            $shpp_link_err = true;
        } elseif(count($_POST['gc_art_num']) > count($prod_id_arr)) {
            $shpp_link .= GSCL_ONE_MODEL_NO_EXISTS_TXT;
            $shpp_link_err = true;
        } elseif(!empty($problem_prod)) {
            $shpp_link .= sprintf(GSCL_STATUS_WRONG_TXT, implode(', ', array_keys($problem_prod)));
            $shpp_link_err = true;
        } else {
            $shpp_link .= $link_part_1.$link_part_2;
        }
    }
}

require (DIR_WS_INCLUDES.'head.php');
?>
<style>
body {overflow-y:scroll;}
#gen-cont {margin:10px 0; padding:0 20px; width:980px; color:#676767;}
#inp-wrap > ul {margin:16px 0 35px;}
.inp-cont {margin:6px 0;}
p {margin: 12px 0 8px;}
.inp-cont > input, #gen-cont input[type=text], #gen-cont textarea {
padding: 2px 3px;
letter-spacing:0.05em;
border: 1px solid #ccc;
border-radius: 5px;
box-shadow:1px 1px 5px #ccc inset;
}
/*#gen-cont input[type=text] {width:940px; color:#c00;}*/
#gen-cont textarea {width:900px; max-width:900px; height:80px; max-height:100px; color:green; vertical-align:middle;}
#gen-cont textarea.shpl-err {color:red;}
#gen-cont .inp-cont > input {width:200px; color:#676767;}
#gen-cont .inp-cont input.sml-inp {width:40px; text-align:center;}
.butt1, .butt2, .butt3, .butt4 {display:inline-block; margin:25px 5px 0; padding:2px 3px; border-radius:6px; box-shadow:1px 1px 5px #808080; cursor:pointer;}
.butt1 {color:#4c854b; border:1px solid #4c854b;}
.butt2 {color:#784e4e; border:1px solid #784e4e;}
.butt3 {color:#41558f; border:1px solid #41558f;}
.butt4 {color:#4b4b4b; border:1px solid 4b4b4b;}
#link-out {margin:30px 0 15px;}
#credits {margin:35px auto 0; text-align:center; color:#349586; font-size:12px;}
#credits p {margin:0;}
</style>
</head>
<body>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<div id="gen-cont">
    <?php echo xtc_draw_form('generate_shpp_link', 'generate_shoppingcart_link.php'); //, 'action=submit_modelnos', 'post'); ?>
        <div id="inp-wrap">
            <?php echo GSCL_EXPLANATION_TXT; ?>
            <div class="inp-cont">
                <?php echo GSCL_MODEL_NO_TXT; ?> <input class="inp-artnum" type="text" name="gc_art_num[]" value="" /> <?php echo GSCL_PCS_TXT; ?> <input class="sml-inp" type="text" name="gc_pcs[]" value="1" /> - <span class="msg"></span>
            </div>
        </div>
        <p>
            <button type="button" id="more-inp" class="butt1"><?php echo GSCL_MORE_INPUT_TXT; ?></button>
            <button type="button" id="less-inp" class="butt2"><?php echo GSCL_LESS_INPUT_TXT; ?></button>
            <button type="submit" class="butt3"><?php echo GSCL_GENERATE_LINK_TXT; ?></button>
            <!--<button type="reset" id="reset" name="reset" class="butt4">x Alle Eingaben löschen</button>-->
            <input type="reset" class="butt4" value="<?php echo GSCL_DELETE_ALL_ENTRIES_TXT; ?>" />
        </p>
    </form>
    <?php if(isset($_POST['gc_art_num'])) {
        //echo '<div id="link-out"><p>Link: <input type="text" readonly="readonly" value="'.$shpp_link.'" /></p></div>';
        echo '<div id="link-out"><p>Link: <textarea '.($shpp_link_err === true ? 'class="shpl-err" ' : '').'readonly="readonly">'.$shpp_link.'</textarea></p></div>';
    } ?>
</div>
<div id="credits">
    <p>&copy; by noRiddle | kontakt@revilonetz.de | 2014 - <?php echo date('Y'); ?></p>
</div>
<?php //require(DIR_WS_INCLUDES . 'footer.php'); // don't include footer because of conflicts with mailhive, hard ocde it ?>
<?php defined( '_VALID_XTC' ) or die( 'Direct Access to this location is not allowed.' ); ?>
<br />
<table border="0" width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td align="center" class="smallText">
      <?php
      /*
      The following copyright announcement is in compliance
      to section 2c of the GNU General Public License, and
      thus can not be removed, or can only be modified
      appropriately.

      Please leave this comment intact together with the
      following copyright announcement.
  
      Copyright announcement changed due to the permissions
      from LG Hamburg from 28th February 2003 / AZ 308 O 70/03
    */
      ?>
      <a style="text-decoration:none;" href="http://www.modified-shop.org" target="_blank"><span style="color:#B0347E;">mod</span><span style="color:#6D6D6D;">ified eCommerce Shopsoftware</span></a><span style="color:#555555;">&nbsp;&copy;2009-<?php echo date("Y"); ?>&nbsp;provides no warranty and is redistributable under the <a style="color:#555555;text-decoration:none;" href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">GNU General Public License (Version 2)</a><br />eCommerce Engine 2006 based on <a style="text-decoration:none; color:#555555;" href="http://www.xt-commerce.com/" rel="nofollow" target="_blank">xt:Commerce</a></span>
    </td>
  </tr>
  <tr>
    <td><?php echo xtc_image(DIR_WS_IMAGES . 'pixel_trans.gif', '', '1', '5'); ?></td>
  </tr>
</table>


<!--<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>-->
<script>
/* <![CDATA[ */
//BOC ajax function
function ajax_func() {
    $('.inp-artnum').each(function() {
        $(this).focusout(function() {
            var artnum_str = $(this).val(),
                msg_span = $(this).siblings('span.msg'),
                csfrn = $('form[name="generate_shpp_link"] input[type="hidden"]').attr('name'), //token system
                csfrt = $('form[name="generate_shpp_link"] input[type="hidden"]').val(); //token system
            //console.log(csfrt);
            var params = {artnum_val: ""+artnum_str+""};
            params[csfrn] = ""+csfrt+"";
            $.post("shopping_cart_link_ajax.php",
                params,
                function(data){
                    msg_span.empty().html(data);
                }
            );
        });
    });
}
//EOC ajax function

$(function(){ //when document ready
    //BOC copy and remove input fields
    var $buttm = $('#more-inp'),
        $buttl = $('#less-inp'),
        $inpwrap = $('#inp-wrap');
        
    $buttm.click(function(e) {
        var $inpcont = $('.inp-cont'),
            $inpclonelast = $inpcont.last().clone();
            e.preventDefault();
            $inpclonelast.find('input').val(''); //empty quantity field before copying
            $inpclonelast.find('input.sml-inp').val('1'); //set value of quantity to default 1
            $inpclonelast.find('span.msg').empty(); //empty validation message for model number
            $inpclonelast.appendTo($inpwrap);
            
            ajax_func(); // call ajax function also on dynamically added inputs
    });
    
    $buttl.click(function(e) {
        var $inpcont = $('.inp-cont'),
            $inpcont_length = $inpcont.length;
            e.preventDefault();
            if($inpcont_length > 1) {
                $inpcont.last().remove();
            } else {
                alert('Sie koennen das Eingabefeld nicht loeschen<br />Ein Feld bleibt bestehen.');
            }
    });
    //EOC copy and remove input fields
    
    ajax_func(); //call ajax function
});
/* ]]> */
</script>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
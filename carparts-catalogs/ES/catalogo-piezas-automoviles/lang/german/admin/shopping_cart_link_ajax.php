<?php
/*********************************************************************
* file: shopping_cart_link_ajax.php
* path: /lang/german/admin/
* use: language file for module
* 
* (c) copyright 10-2020, noRiddle
*********************************************************************/

define('GSCL_PROD_EXISTS_TXT', '<br /><span style="color:green;">OK, Artikelnummer existiert | Artikelname: %s | Listenpreis inkl. MwSt.: %s | Produkt-ID: %s</span>');
define('GSCL_NOT_PROD_STAT1_TXT', '<br /><span style="color:red;">! Artikel hat nicht products_status 1 (= Artikel ist deaktiviert) ! Bitte im Shop prüfen.</span>');
define('GSCL_STAT1_TXT', '(= Preisanfrage)');
define('GSCL_STAT2_TXT', '(= nicht mehr verfügbar)');
define('GSCL_NOT_GMSTAT0_TXT', '<br /><span style="color:red;">! Artikel hat gm_price_status %s %s ! Bitte im Shop prüfen.</span>');
define('GSCL_MODEL_EMPTY_OR_NOTINSHOP_TXT', '<br /><span style="color:red;">! Artikelnummer leer oder nicht im Shop vorhanden !</span>');
<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_help.php 40 2013-01-08 16:36:44Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_help1.php, v 1.4 2003/02/17);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

require('includes/application_top.php');

//BOC let only admin call this file, noRiddle
if (!isset($_SESSION['customer_id']) || (isset($_SESSION['customer_id']) && $_SESSION['customers_status']['customers_status_id'] != '0')) {
    xtc_redirect(xtc_href_link(FILENAME_LOGIN, '', 'NONSSL')); 
} 
//EOC let only admin call this file, noRiddle

if(isset($_GET['issue'])) {
	$issue = (int)$_GET['issue'];
}

// create smarty elements
$smarty = new Smarty;

$smarty->assign(array(
			'HTML_PARAMS' => HTML_PARAMS,
			'HREF' => (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG,
			'TITLE' => TITLE));

$smarty->assign('help_file', 'help' . $issue);

$smarty->assign('language', $_SESSION['language']);
$smarty->caching = 0;

$smarty->display(CURRENT_TEMPLATE . '/module/affiliate_help.html');

include ('includes/application_bottom.php');

?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id: ap.php 5118 2013-07-18 10:58:36Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ap.php,v 1.02 2003/02/18); www.oscommerce.com 
   (c) 2003	 nextcommerce (ap.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   austrian_post_1.05       	Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/
   

define('MODULE_SHIPPING_AP_TEXT_TITLE', 'Correos austriacos AG');
define('MODULE_SHIPPING_AP_TEXT_DESCRIPTION', 'Correos austriacos AG  -  Módulo de envío mundial');
define('MODULE_SHIPPING_AP_TEXT_WAY', ' Envíos a ');
define('MODULE_SHIPPING_AP_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_AP_INVALID_ZONE', 'No es posible realizar un envío a este país.');
define('MODULE_SHIPPING_AP_UNDEFINED_RATE', 'Los costos de envío no se pueden calcular por el momento.');

define('MODULE_SHIPPING_AP_STATUS_TITLE' , 'Correos austriacos AG');
define('MODULE_SHIPPING_AP_STATUS_DESC' , '¿Desea ofrecer envíos a través de Correos austriacos AG?');
define('MODULE_SHIPPING_AP_TAX_CLASS_TITLE' , 'Tasa de impuesto');
define('MODULE_SHIPPING_AP_TAX_CLASS_DESC' , 'Seleccione el tipo de IVA para este modo de envío.');
define('MODULE_SHIPPING_AP_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_AP_ZONE_DESC' , 'Si selecciona una zona, este modo de envío sólo se ofrecerá en esa zona.');
define('MODULE_SHIPPING_AP_SORT_ORDER_TITLE' , 'Orden de la visualización');
define('MODULE_SHIPPING_AP_SORT_ORDER_DESC' , 'Menor se muestra en primer lugar.');
define('MODULE_SHIPPING_AP_ALLOWED_TITLE' , 'Zonas de envío individuales');
define('MODULE_SHIPPING_AP_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE');
define('MODULE_SHIPPING_AP_NUMBER_ZONES_TITLE' , 'Número de zonas');
define('MODULE_SHIPPING_AP_NUMBER_ZONES_DESC' , 'Número de zonas proporcionadas');
define('MODULE_SHIPPING_AP_DISPLAY_TITLE' , 'Activar la visualización');
define('MODULE_SHIPPING_AP_DISPLAY_DESC' , '¿Desea mostrar si no es posible realizar envíos a ese país o si no se pueden calcular los costos de envío?');

for ($module_shipping_ap_i = 1; $module_shipping_ap_i <= MODULE_SHIPPING_AP_NUMBER_ZONES; $module_shipping_ap_i ++) {
define('MODULE_SHIPPING_AP_COUNTRIES_'.$module_shipping_ap_i.'_TITLE' , '<hr/>Zona '.$module_shipping_ap_i.' países');
define('MODULE_SHIPPING_AP_COUNTRIES_'.$module_shipping_ap_i.'_DESC' , 'Lista separada por comas de los códigos de países ISO (2 caracteres) que forman parte de la zona '.$module_shipping_ap_i.' (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_AP_COST_'.$module_shipping_ap_i.'_TITLE' , 'Zona '.$module_shipping_ap_i.' Tabla de tarifas hasta 20 kg');
define('MODULE_SHIPPING_AP_COST_'.$module_shipping_ap_i.'_DESC' , 'Costos de envío de la zona '.$module_shipping_ap_i.' Destinos, basados en un grupo de pesos máximos de pedido. Ejemplo: 3:8.50,7:10.50,... Un peso menor o igual a 3 costaría 8,50 para los países de la zona '.$module_shipping_ap_i.' de destino.');
define('MODULE_SHIPPING_AP_HANDLING_'.$module_shipping_ap_i.'_TITLE' , 'Zona '.$module_shipping_ap_i.' Tarifa de manejo');
define('MODULE_SHIPPING_AP_HANDLING_'.$module_shipping_ap_i.'_DESC' , 'Tarifa de manejo para esta zona de envío');
}
?>

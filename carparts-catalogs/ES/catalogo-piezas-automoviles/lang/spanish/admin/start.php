<?php
/* --------------------------------------------------------------
   $Id: start.php 2585 2012-01-03 14:25:49Z dokuman $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2003 nextcommerce (start.php,v 1.1 2003/08/19); www.nextcommerce.org
   (c) 2006 xt:Commerce (start.php 890 2005-04-27); www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('ATTENTION_TITLE', '! ¡ATENCIÓN! !');

// text for Warnings:
if (!defined('APS_INSTALL')) { //DokuMan - use alternative text for TEXT_FILE_WARNING when using APS package installation
define('TEXT_FILE_WARNING_WRITABLE', '<b>ADVERTENCIA:</b><br />Los siguientes archivos pueden ser escritos por el servidor. Por favor, cambie los derechos de acceso (Permissions) a este archivo por razones de seguridad. <b>(444)</b> in unix, <b>(read-only)</b> in Win32.');
} else {
define('TEXT_FILE_WARNING_WRITABLE', '<b>ADVERTENCIA:</b><br />Los siguientes archivos pueden ser escritos por el servidor. Por favor, cambie los derechos de acceso (Permissions) a este archivo por razones de seguridad. <b>(444)</b> in unix, <b>(read-only)</b> in Win32.<br />Si la instalación fue realizada por un paquete de software de un proveedor, es posible que los derechos de acceso deban ajustarse de forma diferente (HostEurope: <b>CHMOD 400</b> o <b>CHMOD 440</b>)');
}
define('TEXT_FILE_WARNING', '<b>ADVERTENCIA:</b><br />Los siguientes archivos pueden ser escritos por el servidor. Por favor, cambie los derechos de acceso (Permissions) a este archivo por razones de seguridad. <b>(777)</b> in unix, <b>(read-write)</b> in Win32.');
define('TEXT_FOLDER_WARNING', '<b>ADVERTENCIA:</b><br />Los siguientes directorios deben ser escribibles por el servidor. Por favor, cambie los derechos de acceso (Permissions) a estos directorios. <b>(777)</b> in unix, <b>(read-write)</b> in Win32.');
define('REPORT_GENERATED_FOR', 'Informe para:');
define('REPORT_GENERATED_ON', 'Creado el:');
define('FIRST_VISIT_ON', 'Primera visita:');
define('HEADING_QUICK_STATS', 'Breve panorama general');
define('VISITS_TODAY', 'Visitas de hoy:');
define('UNIQUE_TODAY', 'Visitantes individuales:');
define('DAILY_AVERAGE', 'Promedio diario:');
define('TOTAL_VISITS', 'Número total de visitas:');
define('TOTAL_UNIQUE', 'Total de visitantes individuales:');
define('TOP_REFFERER', 'Referencia superior:');
define('TOP_ENGINE', 'Motor de búsqueda superior:');
define('DAY_SUMMARY', 'Panorama general de 30 Días:');
define('VERY_LAST_VISITORS', 'Últimos 10 visitantes:');
define('TODAY_VISITORS', 'Visitantes de hoy:');
define('LAST_VISITORS', 'Últimos 100 visitantes:');
define('ALL_LAST_VISITORS', 'Todos los visitantes:');
define('DATE_TIME', 'Fecha / Hora:');
define('IP_ADRESS', 'Dirección IP:');
define('OPERATING_SYSTEM', 'Sistema Operativo');
define('REFFERING_HOST', 'Referring Host:');
define('ENTRY_PAGE', 'Página de inicio:');
define('HOURLY_TRAFFIC_SUMMARY', 'Resumen de Tráfico por Hora');
define('WEB_BROWSER_SUMMARY', 'Panorama general del navegador web');
define('OPERATING_SYSTEM_SUMMARY', 'Panorama general del sistema Operativo');
define('TOP_REFERRERS', '10 referencias superiores:');
define('TOP_HOSTS', 'Top Ten Hosts');
define('LIST_ALL', 'Mostrar todos');
define('SEARCH_ENGINE_SUMMARY', 'Panorama general de los motores de búsqueda');
define('SEARCH_ENGINE_SUMMARY_TEXT', ' ( Los porcentajes se basan en el número total de visitas a través de los motores de búsqueda. )');
define('SEARCH_QUERY_SUMMARY', 'Panorama general de consultas de búsqueda');
define('SEARCH_QUERY_SUMMARY_TEXT', ' ) ( Los porcentajes se basan en el número total de consultas de búsqueda registradas. )');
define('REFERRING_URL', 'Url de referencia');
define('HITS', 'Hits');
define('PERCENTAGE', 'participación porcentual');
define('HOST', 'Host');

// NEU HINZUGEFUEGT 04.12.2008 - Neue Startseite im Admin BOF

// BOF - vr 2010-04-01 -  Added missing definitions, see below
define('HEADING_TITLE', 'Pedidos');
// EOF - vr 2010-04-01 -  Added missing definitions
define('HEADING_TITLE_SEARCH', 'Número de pedido');
define('HEADING_TITLE_STATUS', 'Estado:');
define('TABLE_HEADING_AFTERBUY', 'Afterbuy');
define('TABLE_HEADING_CUSTOMERS', 'Clientes');
define('TABLE_HEADING_ORDER_TOTAL', 'Valor total');
define('TABLE_HEADING_DATE_PURCHASED', 'Fecha de pedido');
define('TABLE_HEADING_STATUS', 'Estado');
//define('TABLE_HEADING_ACTION', 'Aktion');
define('TABLE_HEADING_QUANTITY', 'Cantidad');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Número de artículo');
define('TABLE_HEADING_PRODUCTS', 'artículo');
define('TABLE_HEADING_TAX', 'IVA');
define('TABLE_HEADING_TOTAL', 'Suma total');
define('TABLE_HEADING_DATE_ADDED', 'agregado el:');
define('ENTRY_CUSTOMER', 'Cliente:');
define('TEXT_DATE_ORDER_CREATED', 'Fecha del pedido:');
define('TEXT_INFO_PAYMENT_METHOD', 'Modalidad de pago:');
define('TEXT_VALIDATING', 'No confirmado');
define('TEXT_ALL_ORDERS', 'Todos los pedidos');
define('TEXT_NO_ORDER_HISTORY', 'No hay historial de pedidos disponible');
define('TEXT_DATE_ORDER_LAST_MODIFIED', 'Última modificación: ');

// BOF - Tomcraft - 2009-11-25 - Added missing definitions for /admin/start.php/
define('TOTAL_CUSTOMERS', 'Total de clientes');
define('TOTAL_SUBSCRIBERS', 'Suscripción al Newsletter');
define('TOTAL_PRODUCTS_ACTIVE', 'Artículos activos');
define('TOTAL_PRODUCTS_INACTIVE', 'Articulos inactivos');
define('TOTAL_PRODUCTS', 'Articulos en total');
define('TOTAL_SPECIALS', 'Ofertas especiales');
// EOF - Tomcraft - 2009-11-25 - Added missing definitions for /admin/start.php/
// BOF - Tomcraft - 2009-11-30 - Added missing definitions for /admin/start.php/
define('UNASSIGNED', 'No asignado');
define('TURNOVER_TODAY', 'Ventas de hoy');
define('TURNOVER_YESTERDAY', 'Ventas de ayer');
define('TURNOVER_THIS_MONTH', 'mes actual');
define('TURNOVER_LAST_MONTH', 'último mes (todos)');
define('TURNOVER_LAST_MONTH_PAID', 'último mes (pagado)');
define('TOTAL_TURNOVER', 'Ventas totales');
// EOF - Tomcraft - 2009-11-30 - Added missing definitions for /admin/start.php/

// BOF - vr 2010-04-01 -  Added missing definitions
// main heading
define('HEADING_TITLE', 'Bienvenido al área de Admin');
// users online
define('TABLE_CAPTION_USERS_ONLINE', 'En línea');
define('TABLE_CAPTION_USERS_ONLINE_HINT', '***Para información sobre un usuario - haga clic en el nombre del usuario***');
define('TABLE_HEADING_USERS_ONLINE_SINCE', 'En línea desde');
define('TABLE_HEADING_USERS_ONLINE_NAME', 'Nombre');
define('TABLE_HEADING_USERS_ONLINE_LAST_CLICK', 'ultimo clic');
define('TABLE_HEADING_USERS_ONLINE_INFO', 'Información');
define('TABLE_CELL_USERS_ONLINE_INFO', 'más...');
// new customers
define('TABLE_CAPTION_NEW_CUSTOMERS', 'Clientes');
define('TABLE_CAPTION_NEW_CUSTOMERS_COMMENT', '(los últimos 15)');
define('TABLE_HEADING_NEW_CUSTOMERS_LASTNAME', 'Apellido');
define('TABLE_HEADING_NEW_CUSTOMERS_FIRSTNAME', 'Nombre');
define('TABLE_HEADING_NEW_CUSTOMERS_REGISTERED', 'inscrito el');
define('TABLE_HEADING_NEW_CUSTOMERS_EDIT', 'editar');
define('TABLE_HEADING_NEW_CUSTOMERS_ORDERS', 'Pedidos');
define('TABLE_CELL_NEW_CUSTOMERS_EDIT', 'editar...');
define('TABLE_CELL_NEW_CUSTOMERS_DELETE', 'borrar...');
define('TABLE_CELL_NEW_CUSTOMERS_ORDERS', 'mostrar...');
// new orders
define('TABLE_CAPTION_NEW_ORDERS', 'Pedidos');
define('TABLE_CAPTION_NEW_ORDERS_COMMENT', '(los últimos 20)');
define('TABLE_HEADING_NEW_ORDERS_ORDER_NUMBER', 'Número del pedido');
define('TABLE_HEADING_NEW_ORDERS_ORDER_DATE', 'Fecha del pedido');
define('TABLE_HEADING_NEW_ORDERS_CUSTOMERS_NAME', 'Nombre del cliente');
define('TABLE_HEADING_NEW_ORDERS_EDIT', 'editar');
define('TABLE_HEADING_NEW_ORDERS_DELETE', 'borrar');
// newsfeed
define('TABLE_CAPTION_NEWSFEED', 'Visite el');
// birthdays
define('TABLE_CAPTION_BIRTHDAYS', 'Lista de cumpleaños');
define('TABLE_CELL_BIRTHDAYS_TODAY', 'Los clientes que cumplen años hoy');
define('TABLE_CELL_BIRTHDAYS_THIS_MONTH', 'Clientes que cumplen años este mes');
// EOF - vr 2010-04-01 -  Added missing definitions
define('HEADING_CAPTION_STATISTIC', 'Estadísticas');
// security check

// DB version check
define('ERROR_DB_VERSION_UPDATE', '<strong>ADVERTENCIA:</strong> Su base de datos necesita ser actualizada, por favor ejecute el <a href="'.DIR_WS_CATALOG.'_installer/">instalador</a> :');
define('ERROR_DB_VERSION_UPDATE_INFO', 'La base de datos debe actualizarse de la versión %s a %s.');

// EMail check
define('ERROR_EMAIL_CHECK', '<strong>ADVERTENCIA:</strong> Las siguientes direcciones de correo electrónico son aparentemente incorrectas:');
define('ERROR_EMAIL_CHECK_INFO', '%s: <%s>');

// security check DB FILE permission
define('WARNING_DB_FILE_PRIVILEGES', '<strong>ADVERTENCIA:</strong> ¡Los privilegios FILE se han activado en la base de datos ’'.DB_DATABASE.'‘ para el usuario de la tienda ’'.DB_SERVER_USERNAME.'‘ !');

// register_globals check
define('WARNING_REGISTER_GLOBALS', '<strong>ADVERTENCIA:</strong> Esta función está <strong>DEPRECATED</strong> (obsoleta) desde PHP 5.3.0 y  <strong>RETIRADO</strong> desde PHP 5.4.0. Por favor, póngase en contacto con su hoster para desactivar "register_globals" .');
?>

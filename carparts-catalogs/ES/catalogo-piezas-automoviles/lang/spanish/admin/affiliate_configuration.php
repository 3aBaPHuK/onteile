<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_configuration.php 34 2013-01-06 08:29:47Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('AFFILIATE_EMAIL_ADDRESS_TITLE', 'Dirección de correo electrónico');
define('AFFILIATE_EMAIL_ADDRESS_DESC', 'La dirección de correo electrónico para el programa de afiliados     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/e-mail-adresse.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_PERCENT_TITLE', 'Pago de Afiliados por Venta %-Tasa');
define('AFFILIATE_PERCENT_DESC', 'Tasa porcentual para el Programa de Afiliados Pago por Venta.     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-pay-per-sale-rate.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_THRESHOLD_TITLE', 'Límite de pago');
define('AFFILIATE_THRESHOLD_DESC', 'Límite inferior para el pago a afiliados     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/auszahlungsgrenze.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_COOKIE_LIFETIME_TITLE', 'Durabilidad de Cookies');
define('AFFILIATE_COOKIE_LIFETIME_DESC', 'Cuánto tiempo (en segundos) el lead de un afiliado sigue siendo válido.     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/cookie-lifetime.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_BILLING_TIME_TITLE', 'Tiempo de facturación');
define('AFFILIATE_BILLING_TIME_DESC', 'El tiempo que debe transcurrir entre una orden exitosa y la entrada del crédito al afiliado. Requerido si las órdenes se devuelven.     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/abrechnungszeit.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_PAYMENT_ORDER_MIN_STATUS_TITLE', 'Estado de pedido');
define('AFFILIATE_PAYMENT_ORDER_MIN_STATUS_DESC', 'El estado de un pedido, desde que es comisionable. En caso de varios se separan por punto y coma (por ejemplo 3;4;7)     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/order-status.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_CHECK_TITLE', 'Cheque de afiliado');
define('AFFILIATE_USE_CHECK_DESC', 'Pagar a los Afiliados con cheque     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-scheck.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_PAYPAL_TITLE', 'PayPal de Afiliado');
define('AFFILIATE_USE_PAYPAL_DESC', 'Pagar a los Afiliados con PayPal     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-paypal.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_BANK_TITLE', 'Banco del afiliado');
define('AFFILIATE_USE_BANK_DESC', 'Pagar a los Afiliados por transferencia bancaria     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-bank.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_INDIVIDUAL_PERCENTAGE_TITLE', 'Porcentaje individual');
define('AFFILIATE_INDIVIDUAL_PERCENTAGE_DESC', 'Establecer el porcentaje individualmente por afiliado     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/individueller-prozentsatz.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_TIER_TITLE', 'Sistema de clasificación');
define('AFFILIATE_USE_TIER_DESC', 'Utilizar Programa de afiliado de clasificaciones múltiples     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/klassensystem.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_ALLOWED_TITLE', 'Estándar para subafiliados');
define('AFFILIATE_TIER_ALLOWED_DESC', 'A los nuevos afiliados se les permite tener subafiliados por defecto.     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/standard-fuer-subaffiliates.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_LEVELS_TITLE', 'Número de clasificaciones');
define('AFFILIATE_TIER_LEVELS_DESC', 'Número de clasificaciones que están por utilizar en el sistema de clasificación     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/anzahl-klassen.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_PERCENTAGE_TITLE', 'Porcentajes para el sistema de clasificación');
define('AFFILIATE_TIER_PERCENTAGE_DESC', 'Porcentajes individuales para los subcategorías<br>Ejemplo 8.00;5.00;1.00     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/prozentsaetze-fuer-klassensystem.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id: gv_mail.php 899 2005-04-29 02:40:57Z hhgag $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(gv_mail.php,v 1.5.2.2 2003/04/27); www.oscommerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Enviar cupón a clientes');

define('TEXT_CUSTOMER', 'Cliente:');
define('TEXT_SUBJECT', 'Asunto:');
define('TEXT_FROM', 'remitente:');
define('TEXT_TO', 'Correo electrónico a:');
define('TEXT_AMOUNT', 'Valor:');
define('TEXT_MESSAGE', 'Mensaje:');
define('TEXT_SINGLE_EMAIL', '<span class="smallText">Utilice este campo sólo para correos electrónicos individuales, de lo contrario, utilice el campo Cliente</span>');
define('TEXT_SELECT_CUSTOMER', 'Seleccionar cliente');
define('TEXT_ALL_CUSTOMERS', 'Todos los clientes');
define('TEXT_NEWSLETTER_CUSTOMERS', 'A todos los suscriptores del newsletter');

define('NOTICE_EMAIL_SENT_TO', 'Aviso: El correo electrónico ha sido enviado a: %s');
define('ERROR_NO_CUSTOMER_SELECTED', 'Error: No se ha seleccionado ningún cliente.');
define('ERROR_NO_AMOUNT_SELECTED', 'Error: No ha entrado ningún importe para el cupón.');

define('TEXT_GV_WORTH', 'El valor del cupón es ');
define('TEXT_TO_REDEEM', 'Para cancelar su cupón, haga clic en el enlace de abajo. Por favor, anote el código de su cupón personal como precaución.');
define('TEXT_WHICH_IS', 'Su código de cupón es: ');
define('TEXT_IN_CASE', ' En el improbable caso de que haya algún problema con la contabilidad.');
define('TEXT_OR_VISIT', 'visita nuestro sitio web ');
define('TEXT_ENTER_CODE', ' e introduzca el código del cupón manualmente.');

define ('TEXT_REDEEM_COUPON_MESSAGE_HEADER', 'Sie haben k&uuml;rzlich in unserem Online-Shop einen Gutschein gekauft, welcher aus Sicherheitsgr&uuml;nden nicht sofort freigeschaltet wurde. Dieses Guthaben steht Ihnen nun zur Verf&uuml;gung.');
define ('TEXT_REDEEM_COUPON_MESSAGE_AMOUNT', "\n\n" . 'El valor de su cupón es %s');
define ('TEXT_REDEEM_COUPON_MESSAGE_BODY', "\n\n" . 'Sie k&ouml;nnen nun &uuml;ber Ihr pers&ouml;nliches Konto den Gutschein an jemanden versenden.');
define ('TEXT_REDEEM_COUPON_MESSAGE_FOOTER', "\n\n");

?>

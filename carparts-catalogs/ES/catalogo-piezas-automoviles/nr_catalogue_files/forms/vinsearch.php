<?php
//BOC filter by coID to display different text above VIN-search form, 06-2018 noRiddle
if($_GET['coID'] == '1001') {
    echo '<h2>'.CommonExtender::LocalizeString('VehicSearchByVIN').'</h2>';
} else {
    echo '<h2>'.CommonExtender::LocalizeString('SearchByVIN').'</h2>';
}
//EOC filter by coID to display different text above VIN-search form, 06-2018 noRiddle

include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'render'.DIRECTORY_SEPARATOR.'catalog'.DIRECTORY_SEPARATOR.'vinsearchform.php');

class VinSearchExtender extends CommonExtender
{
    function FormatLink($type, $dataItem, $catalog, $renderer)
    {
        //BOC set link to content 1001 which includes vehicles.php, noRiddle
        //return 'vehicles.php?ft=findByVIN&c='.$catalog.'&vin=$vin$&ssd=';
        return xtc_href_link(FILENAME_CONTENT, 'coID=1001').'?ft=findByVIN&c='.$catalog.'&vin=$vin$&ssd=';
        //EOC set link to content 1001 which includes vehicles.php, noRiddle
    }   
}

//BOC display vin search form only if customer has got credit, noRiddle
$nr_credit_query_str = "SELECT catalog_credit FROM ".TABLE_CUSTOMERS." WHERE customers_id = ".(int)$_SESSION['customer_id'];
$nr_credit_query = xtc_db_query($nr_credit_query_str);
$nr_credit_arr = xtc_db_fetch_array($nr_credit_query);
if($nr_credit_arr['catalog_credit'] > 0) {
    $renderer = new GuayaquilVinSearchForm(new VinSearchExtender());
    echo $renderer->Draw(array_key_exists('c', $_GET) ? $_GET['c'] : '', $cataloginfo, @$formvin);

    //echo '<br><br>'; //commented out, noRidldle
} else {
    echo '<div class="red">'.NR_NOW_CREDIT_FOR_VINSEARCH.'</div>';
}
//EOC display vin search form only if customer has got credit, noRiddle
?>



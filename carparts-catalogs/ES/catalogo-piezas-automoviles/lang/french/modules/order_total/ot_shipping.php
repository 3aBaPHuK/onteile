<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_shipping.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ot_shipping.php,v 1.4 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (ot_shipping.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_SHIPPING_TITLE', 'Frais de port');
define('MODULE_ORDER_TOTAL_SHIPPING_DESCRIPTION', 'Frais de port d\'une commande');

define('FREE_SHIPPING_TITLE', 'Sans frais de port');
define('FREE_SHIPPING_DESCRIPTION', 'Sans frais de port à partir d\'une valeur de la commande de %s');
  
define('MODULE_ORDER_TOTAL_SHIPPING_STATUS_TITLE', 'Frais de port');
define('MODULE_ORDER_TOTAL_SHIPPING_STATUS_DESC', 'Afficher les frais de port');
  
define('MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER_TITLE', 'Ordre de triage');
define('MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER_DESC', 'Ordre d\'affichage');
  
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_TITLE', 'Permettre sans frais de port');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_DESC', 'Permettre livraison sans frais de port ?');
  
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_TITLE', 'Sans frais de port pour commandes nationales à partir de');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_DESC', 'Sans frais de port à partir d\'une valeur de la commande de.');

define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_INTERNATIONAL_TITLE', 'Sans frais de port pour commandes internationales à partir de');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_INTERNATIONAL_DESC', 'Sans frais de port à partir d\'une valeur de la commande de.');
  
define('MODULE_ORDER_TOTAL_SHIPPING_DESTINATION_TITLE', 'Sans frais de port par zones');
define('MODULE_ORDER_TOTAL_SHIPPING_DESTINATION_DESC', 'Calculer sans frais de port par zones.');
  
define('MODULE_ORDER_TOTAL_SHIPPING_TAX_CLASS_TITLE', 'Catégorie fiscale');
define('MODULE_ORDER_TOTAL_SHIPPING_TAX_CLASS_DESC', 'Sélectionner catégorie fiscale pour les frais de port (uniquement pour le traitement de la commande)');
?>

<?php
/***************************************
* file: ot_bulkgoods
* use: language file for above ot module
* (c) noRiddle 05-2017
***************************************/

define('MODULE_ORDER_TOTAL_BULKGOODS_HELP_LINK', ' <a onclick="window.open(\'popup_help.php?type=order_total&modul=ot_bulkgoods&lng=german\', \'Aide\', \'scrollbars=yes,resizable=yes,menubar=yes,width=800,height=600\'); return false" target="_blank" href="popup_help.php?type=order_total&modul=ot_sperrgut&lng=german"><b>[AIDE]</b></a>');
define('MODULE_ORDER_TOTAL_BULKGOODS_HELP_TEXT', '<h2>Module de marchandise en vrac configurant</h2>
!! PRUEFEN ZEILENVERSCHIEBUNG !!
!! PRUEFEN ZEILENVERSCHIEBUNG !!
!! PRUEFEN ZEILENVERSCHIEBUNG !!
!! PRUEFEN ZEILENVERSCHIEBUNG !!
!! PRUEFEN ZEILENVERSCHIEBUNG !!
!! PRUEFEN ZEILENVERSCHIEBUNG !!
<li>Les frais de marchandise en vrac seront automatiquement listés sur la page "checkout_confirmation", sur la commande dans le back office et dans la confirmation de la commande.</li>
</ul><br /><p><a href="http://www.revilonetz.de/kontakt" target="_blank">noRiddle</a> vous souhaite beaucoup de plaisir et de succès avec le module</p>');

define('MODULE_ORDER_TOTAL_BULKGOODS_TITLE', 'Frais de marchandise en vrac'.MODULE_ORDER_TOTAL_BULKGOODS_HELP_LINK);
define('MODULE_ORDER_TOTAL_BULKGOODS_DESCRIPTION', 'Frais de marchandise en vrac d\'une commande');
  
define('MODULE_ORDER_TOTAL_BULKGOODS_CC_TITLE', 'Supplément de marchandise en vrac');
 
define('MODULE_ORDER_TOTAL_BULKGOODS_STATUS_TITLE', 'Marchandise en vrac active');
define('MODULE_ORDER_TOTAL_BULKGOODS_STATUS_DESC', 'Activer les frais de marchandise en vrac?');
  
define('MODULE_ORDER_TOTAL_BULKGOODS_SORT_ORDER_TITLE', 'Ordre de triage');
define('MODULE_ORDER_TOTAL_BULKGOODS_SORT_ORDER_DESC', 'Ordre d\'affichage.');

define('MODULE_ORDER_TOTAL_BULKGOODS_COSTS_TITLE', 'Frais par marchandise en vrac');
define('MODULE_ORDER_TOTAL_BULKGOODS_COSTS_DESC', '');

define('MODULE_ORDER_TOTAL_BULKGOODS_METHOD_TITLE', 'Comment les frais de marchandise en vrac doit-ils être facturés ?');
define('MODULE_ORDER_TOTAL_BULKGOODS_METHOD_DESC', '<ul style="list-style-type:decimal;"><li>extrapoler tous les frais de marchandise en vrac = les frais seront multiplier avec le nombre d\'articles</li><li>frais de marchandise en vrac uniquement une fois par article = les frais seront fracturés uniquement une fois par article, indépendamment de la quantité</li><li>uniquement une fois les frais de marchandise en vrac les plus élevés par panier = intuitif</li></ul>');

define('MODULE_ORDER_TOTAL_BULKGOODS_SHOW_IN_CHECKOUT_SHIPPING_TITLE', 'Afficher sur "checkout_shipping"');
define('MODULE_ORDER_TOTAL_BULKGOODS_SHOW_IN_CHECKOUT_SHIPPING_DESC', '<ul><li>Avec un clic sur \'OUI\' un lien vers les frais de marchandise en vrac avec les modes de livraison disponible sur la page "checkout_shipping" sera affiché.</li><li>N\'oubliez pas d\'indiquer les informations sur les frais de marchandise en vrac dans le contenu pour les frais de port.</li></ul>');
  
//BOC dropdown array bulgoods module
define('NR_ADD_UP_ALL_BULKGOODS_COSTS', 'Extrapoler tous les frais de marchandise en vrac');
define('NR_ADD_UP_ONE_BULKGOODS_COST_PER_ARTICLE', 'Uniquement une fois les frais de marchandise en vrac par article');
define('NR_ONLY_HIGHEST_BULKGOODS_COST_PER_SHOPPING_CART', 'Uniquement une fois les frais de marchandise en vrac les plus élevés par panier');
//EOC dropdown array bulkgoods module

define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT_TITLE', 'Statut du client automatiquement identifié');
define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT_DESC', 'Ne pas éditer !!, sera automatiquement pris de la base de données.');

define('MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS_TITLE', 'Catégorie fiscale');
define('MODULE_ORDER_TOTAL_BULKGOODS_TAX_CLASS_DESC', 'Sélectionnez la catégorie fiscale suivante pour les frais de marchandise en vrac');

//BOC customer status factors
//if(MODULE_ORDER_TOTAL_BULKGOODS_STATUS == 'true') {
if(strpos(MODULE_ORDER_TOTAL_INSTALLED, 'ot_bulkgoods.php')) {
    $custstat_array = json_decode(MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTAT, true);
    foreach($custstat_array as $k => $cs_name) {
define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$k.'_TITLE', 'Majoration de facteur pour le groupe de client "<i>'.$cs_name.'</i>"');
define('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$k.'_DESC', 'Facteur de multiplication pour le groupe de clients '.$k.' (= "<i>'.$cs_name.'</i>")<br />(La valeur qui est associé comme marchandise en vrac chez le produit, sera multiplié avec la valeur indiquée ici chez ce groupe de clients.)');
    }
}
?>

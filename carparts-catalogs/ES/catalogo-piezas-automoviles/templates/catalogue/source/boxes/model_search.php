<?php
/*********************************************************
* file: model_search.php
* use: search for model no in shop
* (c) noRiddle 11-2016
*********************************************************/

//define shop
$the_queried_shop = isset($_GET['c']) && array_key_exists($_GET['c'], $_SESSION['nr_ctlg_brands']) ? $remote_url[$cat_env]['domain'].'/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].$shop_suffix : '';
$no_price_img = 'nr_catalogue_files/guayaquillib/render/details/images/price_question.png';
$no_avail_img = 'nr_catalogue_files/guayaquillib/render/details/images/not_available.png';
$prodrepl_img = 'nr_catalogue_files/guayaquillib/render/details/images/product_replace.png';

//BOC functions
/*function close_shop_sb_modelsearch() {
    if($shp_db_link != false)
        mysqli_close($shp_db_link);
    $shp_db_link = false;
}*/  //do this in application_top, noRiddle

function ms_find_prices_and_statuses_from_shop($mod_no, $shp_db_link) {
    //$data_arr = array();
    //$persoff_table = 'personal_offers_by_customers_status_'.strtoupper($_SESSION['customers_status']['customers_status_id']);
    $ms_actual_cust_stat = empty($_SESSION['customers_status']['customers_status_id']) ? DEFAULT_CUSTOMERS_STATUS_ID_GUEST : $_SESSION['customers_status']['customers_status_id']; //this is done in price class as well, why I don't actually know
    $persoff_table = 'personal_offers_by_customers_status_'.$ms_actual_cust_stat;
    $result_data = array();
    
    $shp_qu_str = "SELECT p.products_id,
                          REPLACE(p.products_model,' ','') as products_model,
                          p.products_replace,
                          p.products_price,
                          p.products_tax_class_id,
                          p.products_image,
                          p.products_status,
                          p.gm_min_order,
                          p.gm_graduated_qty,
                          p.gm_price_status,
                          pd.products_name,
                          pd.products_description,
                          po.personal_offer,
                          s.specials_new_products_price
                     FROM ".TABLE_PRODUCTS." p
                LEFT JOIN ".TABLE_PRODUCTS_DESCRIPTION." pd
                       ON pd.products_id = p.products_id
                      AND pd.language_id = '".(int)$_SESSION['languages_id']."' 
                LEFT JOIN ".$persoff_table." po
                       ON po.products_id = p.products_id AND quantity = '1'
                LEFT JOIN ".TABLE_SPECIALS." s
                       ON s.products_id = p.products_id
                    WHERE REPLACE(p.products_model,' ','') LIKE '%".$mod_no."%' 
                      AND products_status = '1'";
                      
    //if(is_resource($shp_db_link)) { //gives false, strange
        $shp_qu = mysqli_query($shp_db_link, $shp_qu_str);
        if(!$shp_qu) return '<pre>Query zu Shop fehlgeschlagen: '.mysqli_error($shp_db_link).'</pre>';
        if(mysqli_num_rows($shp_qu)) {
            while($shp_arr = mysqli_fetch_array($shp_qu, MYSQLI_ASSOC)) {
                $result_data[] = $shp_arr;
            }
        } else {
            //$shp_arr = array();
            $result_data = array();
        }
    //}
    //return $shp_arr;
    return $result_data;
}

include(DIR_FS_BOXES_INC . 'smarty_default.php');
$box_smarty->caching = 0; //overwrite, we don't want caching here

$result_string = '';

//if(isset($_GET['c']) && array_key_exists($_GET['c'], Config::$nr_brand_arr)) {
if(isset($_GET['c']) && array_key_exists($_GET['c'], $_SESSION['nr_ctlg_brands'])) {
    //BOC warning if shop not existent or offline, 04-2021, noRiddle
    if(!isset($shp_db_link)) {
        $noshp_cont = $main->getContentData(3000, $_SESSION['languages_id'], $_SESSION['customers_status']['customers_status_id']);
        $noshp_cont = str_replace(array('[{shop_name}]', '[{landingpage_kontakt_link}]'), array(strtoupper($_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand']), 'xxx_7.html'), $noshp_cont);
        $warn_noshop_out = $noshp_cont['content_text'];
        $box_smarty->assign('WARN_NO_SHOP', $warn_noshop_out);
    }
    //EOC warning if shop not existent or offline, 04-2021, noRiddle

    //include_once (dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'/nr_catalogue_files/guayaquillib/render/details/shop_dbs/conn_to_shop_db.php');  //do this in application_top, noRiddle
    //echo '<pre>'.print_r($shop_data, true).'<pre>';
    //echo '<pre>'.var_dump($shp_db_link).'<pre>';

    $model_form = xtc_draw_form('search_model', xtc_href_link(FILENAME_CONTENT, xtc_get_all_get_params()));
    $model_inpt = xtc_draw_input_field('inpt_mdl', AH_IMAGE_BUTTON_SEARCH, 'id="quick-model-search" placeholder="'.AH_IMAGE_BUTTON_SEARCH.'" onfocus="if(this.value==this.defaultValue) this.value=\'\';" onblur="if(this.value==\'\') this.value=this.defaultValue;"');
    //$model_subm = '<button type="submit" class="btn button-catalogue">'.IMAGE_BUTTON_SEARCH_TXT.'</button>';
    $model_subm = xtc_image_submit('button_quick_find.gif', IMAGE_BUTTON_SEARCH, '');
    
    $box_smarty->assign('get_c', true);
    $box_smarty->assign('model_search_form_start', $model_form);
    $box_smarty->assign('model_search_form_end', '</form>');
    $box_smarty->assign('model_search_input', $model_inpt);
    $box_smarty->assign('model_search_submit', $model_subm);
    
    if(isset($_POST['inpt_mdl']) && $_POST['inpt_mdl'] != '') {
        $post_mdl = preg_replace('#[^0-9a-z]#i', '', $_POST['inpt_mdl']);
        $res = ms_find_prices_and_statuses_from_shop($post_mdl, $shp_db_link);

        if(!empty($res)) {
            //echo '<br><br><br><pre>'.print_r($res, true).'<pre>';
            $ms_shp202_arr = array(); //array('mercedes');
            $ms_act_coid = !in_array($_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'], $ms_shp202_arr) ? '7' : '500';
            $inf_link = $remote_url[$cat_env]['domain'].'/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].$shop_suffix.'/shop_content.php?coID='.$ms_act_coid;
            $result_string = '';

            foreach($res as $ms_key => $ms_data) {
                if($ms_data['gm_price_status'] == '1' || (!empty(trim($ms_data['products_replace'])) && strtolower($ms_data['products_replace']) == 'anfrage')) {
                    $inf_link .= '&products_id='.$ms_data['products_id'].'&products_name='.$ms_data['products_name'].'&products_model=Model-No.:'.$ms_data['products_model'];
                }
                
                $nr_ms_tax_rate = isset($nr_foreign_xtcPrice->TAX[$ms_data['products_tax_class_id']]) ? $nr_foreign_xtcPrice->TAX[$ms_data['products_tax_class_id']] : 0;
                $nr_ms_price = $nr_foreign_xtcPrice->xtcGetPrice($ms_data['products_id'], $format = true, 1, $ms_data['products_tax_class_id'], $ms_data['products_price'], 1);

                $nr_ms_tx_inf = $ms_data['gm_price_status'] == '0' ? ($nr_ms_tax_rate != TAX_INFO_EXCL ? '('.$main->getTaxInfo($nr_ms_tax_rate).')' : '') : '';
                //echo '<pre>'.print_r($nr_ms_price, true).'<pre>';
                
                $ms_hidd_price = xtc_draw_hidden_field('prod_price[]', ($ms_data['specials_new_products_price'] != '' ? $ms_data['specials_new_products_price'] : ($ms_data['personal_offer'] != '' ? $ms_data['personal_offer'] : $ms_data['products_price'])));

                //BOC format price like in our new s_price_listing.html, noRiddle
                //BOC Produkt Sonderpreis Langform mit Ausgabe Alter Preis, Neuer Preis, Sie sparen
                if($nr_ms_price['flag'] == 'Special' || $nr_ms_price['flag'] == 'SpecialDiscount') {
                    $ms_price = ' <span class="s_old_price">'
                                       .'<span class="s_small_price">'.INSTEAD.'</span>'.$nr_ms_price['old_price']
                                .'</span>'
                                .'<span class="s_new_price">'
                                    .'<span class="price-green"><span class="s_small_price">'.ONLY.'</span> '.$nr_ms_price['special_price'].'</span>'
                                .'</span>'
                                .'<span class="s_save_price">'.YOU_SAVE.' '.$nr_ms_price['save_percent'].'% / '.$nr_ms_price['save_diff'].'</span>';
                //EOC Produkt Sonderpreis Langform mit Ausgabe Alter Preis, Neuer Preis, Sie sparen
                } else if($nr_ms_price['flag'] == 'SpecialGraduated') {
                    if($nr_ms_price['uvp'] != '') {
                        //BOC Produkt UVP Preis mit Ausgabe Ihr Preis, UVP
                        //BOC filter whether old/new price shall be shown (see nr_foreign_xtcPrice class}
                        if($nr_ms_price['show_persoff'] === true) {
                            $ms_price = '<span class="s_uvp_price">'
                                             .'<span class="price-green"><span class="s_small_price">'.YOUR_PRICE.'</span> '.$nr_ms_price['special_price'].'</span>'
                                         .'</span>'
                                         .'<span class="s_item_price"><span class="s_small_price">'.MSRP.'</span> '.$nr_ms_price['old_price'].'</span>';
                        } else {
                            $ms_price = '<span class="s_uvp_price">'
                                           .'<span class="price-green"><span class="s_small_price">'.YOUR_PRICE.'</span> '.$nr_ms_price['special_price'].'</span>'
                                        .'</span>';
                        }
                    //EOC filter whether old/new price shall be shown (see nr_foreign_xtcPrice class}
                    //BOC Produkt UVP Preis mit Ausgabe Ihr Preis, UVP
                    } else {
                        //BOC Produkt Staffelpreis mit Ausgabe ab Preis, Stückpreis
                        $ms_price = '<span class="s_graduated_price">'
                                        .'<span class="price-green"><span class="s_small_price">'.FROM.'</span> '.$nr_ms_price['old_price'].'</span>'
                                    .'</span>'
                                    .'<span class="s_item_price"><span class="s_small_price">'.UNIT_PRICE.'</span> '.$nr_ms_price['special_price'].'</span>';
                        //EOC Produkt Staffelpreis mit Ausgabe ab Preis, Stückpreis
                    }
                } else if($nr_ms_price['flag'] == 'NotAllowed') {
                    $ms_price = '<span class="s_no_price">'.$nr_ms_price['not_allowed'].'</span>';
                } else {
                    //BOC Produkt Standardpreis mit Ausgabe ab Preis
                    $ms_price = '<span class="s_standard_price"><span class="price-green">';
                        if($nr_ms_price['from'] != '') {
                            $ms_price .= '<span class="s_small_price">'.$nr_ms_price['from'].'</span>';
                        }
                        $ms_price .= $nr_ms_price['standard_price'];
                    $ms_price .= '</span></span>';
                    //EOC Produkt Standardpreis mit Ausgabe ab Preis
                }
                //EOC format price like in our new s_price_listing.html, noRiddle

                $nr_ms_qty = $ms_data['gm_min_order'] > 1 ?  $ms_data['gm_min_order'] : 1; //determine quantity for cart

                //BOC price empty ?, checkbox ?
                if($ms_data['gm_price_status'] == '0') {
                    $qty_field = xtc_draw_input_field('prod_model_qty[]', $nr_ms_qty, 'class="clog-detlist-ipt"');
                    $chck_field = xtc_draw_checkbox_field('check_to_buy[]', $ms_data['products_model']);
                } else if($ms_data['gm_price_status'] == '1' || (!empty(trim($ms_data['products_replace'])) && strtolower($ms_data['products_replace']) == 'anfrage')) {
                    $ms_price = 'X';
                    $qty_field = '<span class="unit-inf-ic"><a href="'.$inf_link.'" target="_blank"><img src="'.$no_price_img .'" alt="price question" /></a></span>';
                    $chck_field = '';
                } else if($ms_data['gm_price_status'] == '2' && $ms_data['products_status'] == '1' && !empty(trim($ms_data['products_replace'])) && strtolower($ms_data['products_replace']) != 'anfrage') {
                    $ms_price = 'X';
                    $qty_field = '<span class="repl-inf-ic"><img src="'.$prodrepl_img .'" alt="not available" /></span>';
                    $chck_field = '';
                } else if($ms_data['gm_price_status'] == '2' && empty(trim($ms_data['products_replace']))) {
                    $ms_price = '';
                    $qty_field = '<span class="noavail-inf-ic"><img src="'.$no_avail_img .'" alt="not available" /></span>';
                    $chck_field = '';
                }
                //EOC price empty ?, checkbox ?
                
                //product image
                $ms_prod_img = !empty($ms_data['products_image']) ? $ms_data['products_image'] : 'noimage.gif';
                
                //BOC handle products_replace
                if($ms_data['gm_price_status'] == '2' && $ms_data['products_status'] == '1' && !empty(trim($ms_data['products_replace'])) && strtolower($ms_data['products_replace']) != 'anfrage') {
                    $our_desc = '<a href="" class="prod-repl-dat" data-prod_repl="'.trim($ms_data['products_replace']).'">&raquo; '.S_PART_WAS_REPLACED_WITH.'<span>'.trim($ms_data['products_replace']).'</span></a>';
                } else {
                    $our_desc = $ms_data['products_description'];
                }
                //EOC handle products_replace

                $result_string .= '<div class="gdCategory">'
                                 .'<h3>'.$ms_data['products_name'].'</h3>'
                                 .'<div class="gdSubCategory">'
                                 .'<table class="gdUnit"><tbody><tr>'
                                 .'<td class="gdImageCol" valign="top" align="center">'
                                 .'<div class="gdImage" style="max-width:175px; max-height:175px;">'
                                    .'<a class="cbimages" href="'.$the_queried_shop.'/images/product_images/popup_images/'.$ms_prod_img.'" title="'.$ms_data['products_name'].'">'
                                    .'<img src="'.$the_queried_shop.'/images/product_images/info_images/'.$ms_prod_img.'" alt ="'.$ms_data['products_name'].'" />'
                                    .'</a>'
                                 .'</div>'
                                 .'</td>'
                                 .'<td class="gdDetailCol" valign="top">'
                                 .xtc_draw_form('into_cat_shp_cart0', xtc_href_link(FILENAME_CONTENT, xtc_get_all_get_params()), 'post', 'class="into-cat-shp-cart"')
                                 .'<table class="guayaquil_table" id="g_DetailTable1" width="96%"><thead><tr>'
                                 .'<th class="c_oem th_2">'.S_TXT_PARTNO.'</th>'
                                 .'<th class="c_name th_3">'.S_TXT_NAME.'</th>'
                                 .'<th class="c_tooltip th_5">'.S_TXT_QTY.'</th>'
                                 .'<th class="c_chkb th_9">'.S_TXT_CHECK.'</th>'
                                 .'</tr></thead>'
                                 .'<tbody><tr><td colspan="4" class="tbl-cont">'
                                 .'<div class="inside-tbl"><table><tbody>'
                                 .'<tr class="g_collapsed g_highlight" name="0" id="d_0">'
                                 .'<td name="c_oem" class="g_ttd td_2">'.strtoupper($ms_data['products_model']).'</td>'
                                 .'<td name="c_name" class="g_ttd td_3">&raquo; '
                                 .$ms_data['products_name']
                                 //.($oldprice != '' ? '<br />'.$oldprice : '').'<br /><span class="price-green">'.$fin_price.' '.$nr_ms_tx_inf.'</span>'
                                 .'<div class="clb_price">'.$ms_price.'</div> <div class="clb_tax">'.$nr_ms_tx_inf.'</div>'
                                 .$our_desc //$ms_data['products_description'] //see above
                                 .'</td>'
                                 .'<td name="c_tooltip" class="g_rowdatahint td_5">'
                                    //.xtc_draw_input_field('prod_model_qty[]', '1', 'class="clog-detlist-ipt"')
                                    .$qty_field
                                    .xtc_draw_hidden_field('products_model[]', $ms_data['products_model'])
                                    .xtc_draw_hidden_field('products_id[]', $ms_data['products_id'])
                                    .xtc_draw_hidden_field('products_name[]', $ms_data['products_name'])
                                    //.xtc_draw_hidden_field('prod_brand[]', Config::$nr_brand_arr[$_GET['c']])
                                    .xtc_draw_hidden_field('prod_brand[]', $_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'])
                                    .xtc_draw_hidden_field('lax_brand', $_GET['c'])
                                    //.xtc_draw_hidden_field('prod_price[]', $sum_price)
                                    //.xtc_draw_hidden_field('prod_price[]', (isset($nr_ms_price['special_price_netto']) ? $nr_ms_price['special_price_netto'] : $nr_ms_price['netto']))
                                    .$ms_hidd_price
                                    .xtc_draw_hidden_field('prod_txcl_id[]', $ms_data['products_tax_class_id'])
                                 .'</td>'
                                 //.'<td name="c_chkb" class="g_ttd td_9">'.xtc_draw_checkbox_field('check_to_buy[]', $ms_data['products_model']).'</td>'
                                 .'<td name="c_chkb" class="g_ttd td_9">'.$chck_field.'</td>'
                                 .'</tr></tbody></table></div>'
                                 .'</td></tr></tbody></table>'
                                 .'<p class="subm-top">'
                                 .'<span class="cssButton cssButtonColor2"><i class="fa fa-arrow-down"></i> <i class="fa fa-shopping-cart"></i><span class="cssButtonText">'.S_TXT_INTOCART.'</span><button type="submit" class="cssButtonText" title="'.S_TXT_INTOCART.'">'.S_TXT_INTOCART.'</button><i class="fa fa-shopping-cart"></i> <i class="fa fa-arrow-down"></i></span>'
                                 .'</p>'
                                 .'<p class="subm-bott">'
                                 .'<span class="cssButton cssButtonColor2"><i class="fa fa-arrow-up"></i> <i class="fa fa-shopping-cart"></i><span class="cssButtonText">'.S_TXT_INTOCART.'</span><button type="submit" class="cssButtonText" title="'.S_TXT_INTOCART.'">'.S_TXT_INTOCART.'</button><i class="fa fa-shopping-cart"></i> <i class="fa fa-arrow-up"></i></span>'
                                 .'</p>'
                                 .'</form>'
                                 .'</td></tr></tbody></table></div></div>';
            }
        } else {
            if($_SESSION['language'] = 'german') {
                $result_string .= '<p style="color:#c00;">Der Artikel wurde im Shop leider nicht gefunden.</p>';
            } else if($_SESSION['language'] = 'english') {
                $result_string .= '<p style="color:#c00;">The product was not found in shop, sorry.</p>';
            }
        }
    }
    
    //close external shop DB
    //close_shop_sb_modelsearch();  //do this in application_top, noRiddle
    
    //assign shop to be able to build link to contact in html file, noRiddle
    //$box_smarty->assign('SHOP', Config::$nr_brand_arr[$_GET['c']]);
    $box_smarty->assign('SHOP', $_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand']);
    $box_smarty->assign('SHOP_SUFFIX', $shop_suffix);
    //brand icon
    //$box_smarty->assign('BRAND_IC', Config::$nr_icon_arr[$_GET['c']]);
    $box_smarty->assign('BRAND_IC', $_SESSION['nr_ctlg_brands'][$_GET['c']]['brand_icon']);

    
}

if ($result_string != '') {
    $box_smarty->assign('BOX_RESULT', $result_string);
}

$box_model_search = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_model_search.html');
$smarty->assign('box_MODEL_SEARCH', $box_model_search);
?>
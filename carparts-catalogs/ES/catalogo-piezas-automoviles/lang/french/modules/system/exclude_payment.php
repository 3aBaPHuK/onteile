<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_EXCLUDE_PAYMENT_TEXT_TITLE', 'Modes de paiement dépendant du mode d\'expédition');
define('MODULE_EXCLUDE_PAYMENT_TEXT_DESCRIPTION', '');
define('MODULE_EXCLUDE_PAYMENT_STATUS_TITLE' , 'Statut');
define('MODULE_EXCLUDE_PAYMENT_STATUS_DESC' , 'Activer module ?');
define('MODULE_EXCLUDE_PAYMENT_NUMBER_TITLE' , 'Nombre de modes d\'expédition');
define('MODULE_EXCLUDE_PAYMENT_NUMBER_DESC' , 'Nombre de modes d\'expédition à configurer.');

for ($module_exclude_payment_i = 1; $module_exclude_payment_i <= MODULE_EXCLUDE_PAYMENT_NUMBER; $module_exclude_payment_i ++) {
define('MODULE_EXCLUDE_PAYMENT_SHIPPING_'.$module_exclude_payment_i.'_TITLE' , '<hr noshade>'.$module_exclude_payment_i.'. Mode d\'expédition');
define('MODULE_EXCLUDE_PAYMENT_SHIPPING_'.$module_exclude_payment_i.'_DESC' , 'Sélectionnez un mode d\'expédition pour lequel vous souhaitez exclure un mode de paiement.');
define('MODULE_EXCLUDE_PAYMENT_PAYMENT_'.$module_exclude_payment_i.'_TITLE' , $module_exclude_payment_i.'. Mode de paiement exclu');
define('MODULE_EXCLUDE_PAYMENT_PAYMENT_'.$module_exclude_payment_i.'_DESC' , 'Sélectionnez le mode de paiement que vous souhaitez exclure.');
}

?>

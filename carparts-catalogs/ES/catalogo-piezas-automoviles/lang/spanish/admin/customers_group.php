<?php
/**
 * Sprachdatei für globale Kundengruppenberechtigungen
 * @author André Estel
 * @copyright 2008 Estelco
 * @license GPLv2
 */

define('HEADING_TITLE', 'Configurar permisos de grupo de clientes');

define('TEXT_CATEGORIES', 'Categorías');
define('TEXT_PRODUCTS', 'Productos');
define('TEXT_CONTENT', 'Contenido de páginas (Manager de Contenido)');
define('TEXT_PRODUCTS_CONTENT', 'Contenido del artículo (Manager de Contenido)');
define('TEXT_PERMISSION', 'Permiso');
define('TEXT_SET', 'dar');
define('TEXT_UNSET', 'retirar');
define('TEXT_SEND', 'enviar');

define('ERROR_PLEASE_SELECT_CUSTOMER_GROUP', 'Por favor, seleccione al menos un grupo de clientes.');
define('ERROR_PLEASE_SELECT_SHOP_AREA', 'Por favor, seleccione al menos un área de la tienda.');
define('TEXT_CATEGORIES_SUCCESSFULLY_SET', 'Permisos de categoría establecidos con éxito.');
define('TEXT_CATEGORIES_SUCCESSFULLY_UNSET', 'Permisos de categoría eliminados con éxito.');
define('TEXT_PRODUCTS_SUCCESSFULLY_SET', 'Permisos de producto establecidos con éxito.');
define('TEXT_PRODUCTS_SUCCESSFULLY_UNSET', 'Permisos de producto eliminados con éxito.');
define('TEXT_CONTENT_SUCCESSFULLY_SET', 'Permisos de contenido de páginas establecidos con éxito.');
define('TEXT_CONTENT_SUCCESSFULLY_UNSET', 'Permisos de contenido de páginas eliminados con éxito.');
define('TEXT_PRODUCTS_CONTENT_SUCCESSFULLY_SET', 'Permisos de contenido del artículo establecidos con éxito.');
define('TEXT_PRODUCTS_CONTENT_SUCCESSFULLY_UNSET', 'Permisos de contenido del artículo eliminados con éxito.');
?>

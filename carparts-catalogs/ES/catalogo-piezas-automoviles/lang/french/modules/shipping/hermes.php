<?php
/* -----------------------------------------------------------------------------------------
   $Id: hermes.php 5121 2013-07-18 11:38:19Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(flat.php,v 1.6 2003/02/16); www.oscommerce.com
   (c) 2003	 nextcommerce (flat.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_HERMES_TEXT_TITLE', 'Envoi avec Hermes');
define('MODULE_SHIPPING_HERMES_TEXT_DESCRIPTION', 'Service colis Hermes');
define('MODULE_SHIPPING_HERMES_TEXT_WAY_DE', 'Dans toute l\'Allemagne:');
define('MODULE_SHIPPING_HERMES_TEXT_WAY_EU', 'International:');
define('MODULE_SHIPPING_HERMES_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_HERMES_TAX_CLASS_TITLE' , 'Taux d\'imposition');
define('MODULE_SHIPPING_HERMES_TAX_CLASS_DESC' , 'Veuillez séléctionner le taux de TVA pour ce type d\'envoi');

define('MODULE_SHIPPING_HERMES_STATUS_TITLE' , 'Activer la boutique de colis Hermes');
define('MODULE_SHIPPING_HERMES_STATUS_DESC' , 'Module de Leonid Lezner');

define('MODULE_SHIPPING_HERMES_NATIONAL_TITLE' , 'Envoi national (DE)');
define('MODULE_SHIPPING_HERMES_NATIONAL_DESC' , 'Prix pour les classes: S;M,L');

define('MODULE_SHIPPING_HERMES_INTERNATIONAL_TITLE' , 'Envoi international (tous sauf DE)');
define('MODULE_SHIPPING_HERMES_INTERNATIONAL_DESC' , 'Prix pour les classes: S;M;L');

define('MODULE_SHIPPING_HERMES_GEWICHT_TITLE' , 'Définition de classes');
define('MODULE_SHIPPING_HERMES_GEWICHT_DESC' , 'Poids maximum (kg) pour les classes: S;M;L');

define('MODULE_SHIPPING_HERMES_MAXGEWICHT_TITLE' , 'Poids maximal');
define('MODULE_SHIPPING_HERMES_MAXGEWICHT_DESC' , 'Poids maximal pour ce type d\'envoi');

define('MODULE_SHIPPING_HERMES_SORT_ORDER_TITLE' , 'Ordre d\'affichage');
define('MODULE_SHIPPING_HERMES_SORT_ORDER_DESC' , 'Les plus bas sont affichés d\'abord.');

define('MODULE_SHIPPING_HERMES_ALLOWED_TITLE' , 'Zones d\'envoi individuelles');
define('MODULE_SHIPPING_HERMES_ALLOWED_DESC' , 'Veuillez indiquer les zones <b>individuellement</b> pour lesquelles l\'envoi est possible. Par exemple AT,DE');
?>

<?php
/***********************************************
*
*
*
* � copyright, 03-2021, noRiddle
***********************************************/
?>
<script>
$(document).ready(function(){
    $(".cbimages").colorbox({rel:'cbimages', scalePhotos:true, maxWidth: "90%", maxHeight: "90%", fixed: true});
    //$(".iframe").colorbox({iframe:true, width:"780", height:"560", maxWidth: "90%", maxHeight: "90%", fixed: true});
    $('body').on('click', '.iframe', function() {$(".iframe").colorbox({iframe:true, width:"780", height:"560", maxWidth: "90%", maxHeight: "90%", fixed: true});}); //also for dynamically added elements, noRiddle
    $(".unveil").show();
    $(".unveil").unveil(200);
    //$('select').SumoSelect();
    $('select').not('.goog-te-combo').SumoSelect(); //not for google languages dropdwon, is buggy, 03-2021, noRiddle
    /* Mark Selected */
    var tmpStr = '';
    $('.filter_bar .SumoSelect').each(function(index){
      ($(this).find('select').val() == '') ? $(this).find('p').removeClass("Selected") : $(this).find('p').addClass("Selected");
    });

    $(document).bind('cbox_complete', function(){
      if($('#cboxTitle').height() > 20){
        $("#cboxTitle").hide();
        $("<div>"+$("#cboxTitle").html()+"</div>").css({color: $("#cboxTitle").css('color')}).insertAfter("#cboxPhoto");
        /*$.fn.colorbox.resize(); // Tomcraft - 2016-06-05 - Fix Colorbox resizing*/
      }
    });
    
    jQuery.extend(jQuery.colorbox.settings, {
        current: "<?php echo TEXT_COLORBOX_CURRENT; ?>",
        previous: "<?php echo TEXT_COLORBOX_PREVIOUS; ?>",
        next: "<?php echo TEXT_COLORBOX_NEXT; ?>",
        close: "<?php echo TEXT_COLORBOX_CLOSE; ?>",
        xhrError: "<?php echo TEXT_COLORBOX_XHRERROR; ?>",
        imgError: "<?php echo TEXT_COLORBOX_IMGERROR; ?>",
        slideshowStart: "<?php echo TEXT_COLORBOX_SLIDESHOWSTART; ?>",
        slideshowStop: "<?php echo TEXT_COLORBOX_SLIDESHOWSTOP; ?>"
    });
    
    <?php if(isset($_GET['coID']) && ($_GET['coID'] == '1004' || $_GET['coID'] == '1006' || $_GET['coID'] == '1007')) { ?>
    jQuery('.guayaquil_zoom').colorbox({
        href: function () {
            var url = jQuery(this).attr('full');
            return url;
        },
        photo:true,
        rel: "img_group",
        opacity: 0.3,
        title : function () {
            var title = jQuery(this).attr('title');
            var url = jQuery(this).attr('link');
            return '<a href="' + url + '">' + title + '</a>';
        },
        current: '<?php echo TEXT_COLORBOX_CURRENT; ?>', //'Pic {current} of {total}',
        maxWidth : '90%', //'98%',
        maxHeight : '90%', //'98%',
        fixed: true, //added fixed positioning, noRiddle
        left: "5%" //added left position, noRiddle
    })
    <?php } ?>
});
</script>
<?php
/* --------------------------------------------------------------
  $Id$

  modified eCommerce Shopsoftware
  http://www.modified-shop.org

  Copyright (c) 2009 - 2013 [www.modified-shop.org]
  --------------------------------------------------------------
  based on:
  (c) 2013 Falk Wolsky
  
  Released under the GNU General Public License
  --------------------------------------------------------------*/

define('MODULE_EASYBILL_CSV_TEXT_DESCRIPTION', 'Exportación - easyBill.de<br />Crear fácilmente facturas en línea');
define('MODULE_EASYBILL_CSV_TEXT_TITLE', 'easybill - CSV Exportación');
define('MODULE_EASYBILL_CSV_ORDER_STATUS_DESC', '¿Con qué estado de pedido se deben exportar los pedidos?  <br/>(es posible una selección múltiple con la tecla SHIFT pulsada)');
define('MODULE_EASYBILL_CSV_ORDER_STATUS_TITLE', '<hr /><strong>estado de pedido</strong>');
define('MODULE_EASYBILL_CSV_CUSTOMER_STATUS_DESC', '¿Con qué estado de cliente deben exportarse los pedidos? <br/>(es posible una selección múltiple con la tecla SHIFT pulsada)');
define('MODULE_EASYBILL_CSV_CUSTOMER_STATUS_TITLE', '<hr /><strong>estado de cliente</strong>');
define('MODULE_EASYBILL_CSV_ORDER_DATE_DESC', '¿Desde qué fecha de pedido se deben exportar los pedidos?');
define('MODULE_EASYBILL_CSV_ORDER_DATE_TITLE', '<strong>fecha de pedido</strong>');
define('MODULE_EASYBILL_CSV_EXPORT_YES', 'Sólo descargar');
define('MODULE_EASYBILL_CSV_EXPORT_NO', 'Guardar en el servidor');
define('MODULE_EASYBILL_CSV_EXPORT_CRON', 'Crear URL de Cronjob');
define('MODULE_EASYBILL_CSV_CRON_URL_TITLE', '<hr /><strong>URL para Cronjob</strong>');
define('MODULE_EASYBILL_CSV_CRON_URL_DESC', 'Esta URL debe ser introducida en easyBill para que los pedidos puedan ser recogidos automáticamente.');
define('MODULE_EASYBILL_CSV_EXPORT', 'Por favor, NO interrumpa la exportación. Esto puede tardar unos minutos.');
define('MODULE_EASYBILL_CSV_EXPORT_TYPE', '<hr /><strong>Modo de almacenamiento:</strong>');
define('MODULE_EASYBILL_CSV_ERROR', 'No se encontró ningún pedido para los criterios seleccionados.');
define('MODULE_EASYBILL_CSV_STANDARD_TAX_CLASS', '1');

?>

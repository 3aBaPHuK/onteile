<?php
/* -----------------------------------------------------------------------------------------
   $Id: shopvote.php 10898 2017-08-11 16:02:44Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHOPVOTE_STATUS_TITLE', 'Estado');
define('MODULE_SHOPVOTE_STATUS_DESC', 'Estado del módulo');
define('MODULE_SHOPVOTE_STATUS_INFO', 'Activo');
define('MODULE_SHOPVOTE_TEXT_TITLE', 'Shopvote');
define('MODULE_SHOPVOTE_TEXT_DESCRIPTION', 'Con este plugin puede integrar fácilmente en su tienda las mejores funciones del portal de evaluación SHOPVOTE. La función EasyReviews integrada envía automáticamente una solicitud de evaluación a sus clientes de forma legalmente segura. Sus clientes pueden evaluar su empresa de forma rápida y sencilla. La función RatingStars garantiza, por un lado, la presentación óptima de sus evaluaciones en su sitio web y, por otro lado, la generación automática de estrellas de evaluación en los resultados de búsqueda orgánicos de Google.');
define('MODULE_SHIPPING_EASYREVIEWS_TITLE', 'Código EasyReviews');
define('MODULE_SHIPPING_EASYREVIEWS_DESC', 'Ahora copie el código JavaScript/HTML del área de comerciantes y péguelo aquí. Las variables "CUSTOMERMAIL" y "ORDERNUMBER" se sustituyen automáticamente por el módulo con los datos correspondientes.');
define('MODULE_SHIPPING_RATINGSTARS_TITLE', 'Código RatingStars');
define('MODULE_SHIPPING_RATINGSTARS_DESC', 'La integración es muy sencilla mediante el uso de los siguientes gráficos de ShopVote "AllVotes I + II" y "VoteBadge I - III". Los gráficos los encuentra en el área de comerciantes de SHOPVOTE bajo "Graphics & Seals". Una vez que haya seleccionado un gráfico, haga que se muestre el código e insértelo aquí.');

?>

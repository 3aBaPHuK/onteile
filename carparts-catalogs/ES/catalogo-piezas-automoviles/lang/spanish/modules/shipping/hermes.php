<?php
/* -----------------------------------------------------------------------------------------
   $Id: hermes.php 5121 2013-07-18 11:38:19Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(flat.php,v 1.6 2003/02/16); www.oscommerce.com
   (c) 2003	 nextcommerce (flat.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_HERMES_TEXT_TITLE', 'Envío con Hermes');
define('MODULE_SHIPPING_HERMES_TEXT_DESCRIPTION', 'Hermes Paket Service');
define('MODULE_SHIPPING_HERMES_TEXT_WAY_DE', 'en toda Alemania: ');
define('MODULE_SHIPPING_HERMES_TEXT_WAY_EU', 'Internacional: ');
define('MODULE_SHIPPING_HERMES_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_HERMES_TAX_CLASS_TITLE' , 'Tasa de impuesto');
define('MODULE_SHIPPING_HERMES_TAX_CLASS_DESC' , 'Seleccione el tipo de IVA para este modo de envío.');

define('MODULE_SHIPPING_HERMES_STATUS_TITLE' , 'Acitvar Hermes Paket Shop');
define('MODULE_SHIPPING_HERMES_STATUS_DESC' , 'Módulo de Leonid Lezner');

define('MODULE_SHIPPING_HERMES_NATIONAL_TITLE' , 'Envío nacional (DE)');
define('MODULE_SHIPPING_HERMES_NATIONAL_DESC' , 'Precios para los clases: S;M;L');

define('MODULE_SHIPPING_HERMES_INTERNATIONAL_TITLE' , 'Envíos internacionales (todos excepto DE)');
define('MODULE_SHIPPING_HERMES_INTERNATIONAL_DESC' , 'Precios para los clases: S;M;L');

define('MODULE_SHIPPING_HERMES_GEWICHT_TITLE' , 'Definición de clase');
define('MODULE_SHIPPING_HERMES_GEWICHT_DESC' , 'Max. Peso (kg) para las clases: S;M;L');

define('MODULE_SHIPPING_HERMES_MAXGEWICHT_TITLE' , 'Peso máximo');
define('MODULE_SHIPPING_HERMES_MAXGEWICHT_DESC' , 'Max. Peso para la clase de envío (kg)');

define('MODULE_SHIPPING_HERMES_SORT_ORDER_TITLE' , 'Orden de la visualización');
define('MODULE_SHIPPING_HERMES_SORT_ORDER_DESC' , 'Menor se muestra en primer lugar.');

define('MODULE_SHIPPING_HERMES_ALLOWED_TITLE' , 'Zonas de envío individuales');
define('MODULE_SHIPPING_HERMES_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE');
?>

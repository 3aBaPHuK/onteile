<?php
/* *******************************************************************************
* bestellungen.php, noRiddle 07-2017
* based on gm_module_export.php 2008-08-12 mb
* Gambio OHG
* http://www.gambio.de
* Copyright (c) 2008 Gambio OHG
* Released under the GNU General Public License
* --------------------------------------------------------------
* reworked for modified 2.0.X by noRiddle 02-2018
* -------------------------------------------------------------- 
* XT-Commerce - community made shopping
* http://www.xt-commerce.com

* Copyright (c) 2003 XT-Commerce
* --------------------------------------------------------------
* based on: 
* (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
* (c) 2002-2003 osCommerce(modules.php,v 1.45 2003/05/28); www.oscommerce.com 
* (c) 2003	 nextcommerce (modules.php,v 1.23 2003/08/19); www.nextcommerce.org
*
* Released under the GNU General Public License 
**********************************************************************************/
defined('_VALID_XTC') or die('Direct Access to this location is not allowed.');

define('MODULE_BESTELLUNGEN_TEXT_DESCRIPTION', 'Export - BESTELLUNGEN');
define('MODULE_BESTELLUNGEN_TEXT_TITLE', 'BESTELLUNGEN - CSV');
define('MODULE_BESTELLUNGEN_FILE_TITLE', '<hr noshade>Dateiname');
define('MODULE_BESTELLUNGEN_FILE_DESC', 'Geben Sie einen Dateinamen ein. Falls die Exportadatei am Server gespeichert werden soll ist das Verzeichnis (Verzeichnis /export/)');
define('MODULE_BESTELLUNGEN_STATUS_DESC', 'Modulstatus');
define('MODULE_BESTELLUNGEN_STATUS_TITLE', 'Status');
define('MODULE_BESTELLUNGEN_CURRENCY_TITLE', 'W&auml;hrung');
define('MODULE_BESTELLUNGEN_CURRENCY_DESC', 'Welche W&auml;hrung soll exportiert werden?');
define('EXPORT_YES', 'Nur herunterladen');
define('EXPORT_NO', 'Nur auf dem Server speichern (/export/)');
define('CURRENCY', '<hr noshade><b>W&auml;hrung:</b>');
define('CURRENCY_DESC', 'W&auml;hrung in der Exportdatei');
define('EXPORT', 'Bitte den Sicherungsprozess AUF KEINEN FALL unterbrechen. Dieser kann einige Minuten in Anspruch nehmen.');
define('EXPORT_TYPE', '<hr noshade><b>Speicherart:</b>');
define('EXPORT_STATUS_TYPE', '<hr noshade><b>Kundengruppe:</b>');
define('EXPORT_STATUS', 'Bitte w&auml;hlen Sie die Kundengruppe, die Basis f&uuml;r den Exportierten Preis bildet. (Falls Sie keine Kundengruppenpreise haben, w&auml;hlen Sie <i>Gast</i>):</b>');
define('ORDERS_STATUS', '<hr noshade><b>Bestellstatus:</b>');
define('ORDERS_STATUS_DESC', 'Bitte wählen Sie den Bestellstatus der Bestellungen, die Sie exportieren wollen:');
define('ORDERS_STATUS_NEW_DESC', 'Bitte wählen Sie den Bestellstatus, der nach dem Export für die jeweilige Bestellung gelten soll:<br /><span style="color:#c00;">Bitte bedacht benutzen !</span><br />Wenn "Bestellstatus" oben auf "alle" steht wird kein Status geändert.');
define('DATE_FORMAT_EXPORT', '%d.%m.%Y'); // this is used for strftime()
define('GM_PAKET', '<hr noshade><b>Paketgr&ouml;&szlig;e:</b><br><input type="text" name="gm_paket" value="NP" size="4" />');
// include needed functions 

class bestellungen {
	var $code, $title, $description, $enabled;

	function __construct() {
		global $order;

		$this->code = 'bestellungen';
		$this->title = MODULE_BESTELLUNGEN_TEXT_TITLE;
		$this->description = MODULE_BESTELLUNGEN_TEXT_DESCRIPTION;
		$this->sort_order = MODULE_BESTELLUNGEN_SORT_ORDER;
		$this->enabled = ((MODULE_BESTELLUNGEN_STATUS == 'True') ? true : false);
        $this->exp_path = 'export/'; //must be without leading slash (see /admin/ah_export.php)
	}

	function process($file) {
		if($file != '') {
            @xtc_set_time_limit(0);
            if ($_POST['oders_status'] != '') {
                $orders_query_where = " WHERE orders_status = ".(int)$_POST['oders_status'];
            }
            $schema = '';
            //delivery_name,delivery_firstname,delivery_lastname,delivery_company,delivery_street_address,delivery_suburb,delivery_city,delivery_postcode,delivery_state,delivery_country,delivery_country_iso_code_2,				o.comments,

            $orders_query = "SELECT o.orders_id,
                                    o.customers_id,
                                    o.customers_telephone,
                                    o.customers_email_address,
                                    o.billing_firstname,
                                    o.billing_lastname,
                                    o.billing_company,
                                    o.billing_street_address,
                                    o.billing_suburb,
                                    o.billing_city,
                                    o.billing_postcode,
                                    o.billing_state,
                                    o.billing_country,
                                    o.billing_country_iso_code_2,
                                    o.billing_address_format_id,
                                    o.payment_method,
                                    o.shipping_method,
                                    o.shipping_class,
                                    o.date_purchased,
                                    o.orders_status,
                                    o.currency,
                                    o.shipping_class,
                                    c.customers_gender
                               FROM orders o
                          LEFT JOIN customers c ON o.customers_id = c.customers_id
                                    ". $orders_query_where;

            $customers_query = xtc_db_query($orders_query);
            while ($customers = xtc_db_fetch_array($customers_query)) {
                $ot_data = array();//fix
                $qt = xtc_db_query("SELECT class, value FROM orders_total WHERE orders_id = ".(int)$customers['orders_id']);
                while ($ot_array = xtc_db_fetch_array($qt)) {
                  $ot_data[$ot_array['class']] = $ot_array['value'];
                }
                $ordered_products = '';
                $qp = xtc_db_query("SELECT products_model, products_quantity, products_price, products_tax, final_price FROM orders_products WHERE orders_id = ".(int)$customers['orders_id']);
                while ($op_array = xtc_db_fetch_array($qp)) {
                  $netto_price = $op_array['products_price']*100/(100+$op_array['products_tax']);
                  $ordered_products .= $op_array['products_model'].';'.$op_array['products_quantity'].';'.$netto_price.';';
                }

    //$schema = 'Bestellnummer;Versandart;Zahlungsart;Kundennummer;Email;Telefon;Anrede(m/f);Vorname;Nachname;Firma;Strasse;Länderkürzel(iso_code_2);PLZ;Bundesland;Ort;Land;Mindermengenzuschlag;Versandkosten;Summe netto;Mwst;---;Teilenummer;Anzahl;Einzelpreis;Teilenummer;Anzahl;Einzelpreis netto;loop' . "\n";
            $schema_entry = $customers['orders_id'] . ";" .
                $customers['shipping_class'] . ";" .
                $customers['payment_method'] . ";" .
                $customers['customers_id'] . ";" .
                $customers['customers_email_address'] . ";" .
                $customers['customers_telephone'] . ";" .
                $customers['customers_gender'] . ";" .
                $customers['billing_firstname'] . ";" .
                $customers['billing_lastname'] . ";" .
                $customers['billing_company'] . ";" .
                $customers['billing_street_address'] . " " . $customers['billing_suburb'] . ";" .
                $customers['billing_country_iso_code_2'] . ";" .
                $customers['billing_postcode'] . ";" .
                $customers['billing_state'] . ";" .
                $customers['billing_city'] . ";" .
                $customers['billing_country'] . ";" .
                $ot_data['ot_loworderfee'] . ";" .
                $ot_data['ot_shipping'] . ";" .
                $ot_data['ot_total_netto'] . ";" .
                $ot_data['ot_tax'] . ";" .
                "---;" .
                $ordered_products .
                "\n";
                $schema .= $schema_entry;
            }

            if(empty($schema)) {
                $schema = ' ';
            }

			// create File
			$fp = fopen(DIR_FS_DOCUMENT_ROOT . 'export/' . $file, "w+");
			fputs($fp, $schema);
			fclose($fp);

			switch($_POST['export']) {
				case 'yes':
					// send File to Browser
					$extension = substr($file, -3);
					$fp = fopen(DIR_FS_DOCUMENT_ROOT . $this->exp_path . $file, "rb");
					$buffer = fread($fp, filesize(DIR_FS_DOCUMENT_ROOT . $this->exp_path . $file));
					fclose($fp);
					header('Content-type: application/x-octet-stream');
					header('Content-disposition: attachment; filename=' . $file);
					echo $buffer;
                    
                    unlink(DIR_FS_DOCUMENT_ROOT . $this->exp_path . $file);
                    
					if ($_POST['oders_status_new'] != '' AND $_POST['oders_status'] != '') {
						$ord_upd_qu = "UPDATE ".TABLE_ORDERS." SET orders_status = '".(int)$_POST['oders_status_new']."' WHERE orders_status = ".(int)$_POST['oders_status'];
                        $ord_stat_hist_arr = array ('orders_id' => $customers['orders_id'],
                                                    'orders_status_id' => (int)$_POST['oders_status_new'],
                                                    'date_added' => 'now()',
                                                    'customer_notified' => '0',
                                                    'comments' => ''
                                                   );
  
						xtc_db_query($ord_upd_qu);
                        xtc_db_perform(TABLE_ORDERS_STATUS_HISTORY, $ord_stat_hist_arr);
					}
					exit;
                break;
                case 'no':
                    if ($_POST['oders_status_new'] != '' AND $_POST['oders_status'] != '') {
						$ord_upd_qu = "UPDATE ".TABLE_ORDERS." SET orders_status = '".(int)$_POST['oders_status_new']."' WHERE orders_status = ".(int)$_POST['oders_status'];
                        $ord_stat_hist_arr = array ('orders_id' => $customers['orders_id'],
                                                    'orders_status_id' => (int)$_POST['oders_status_new'],
                                                    'date_added' => 'now()',
                                                    'customer_notified' => '0',
                                                    'comments' => ''
                                                   );
  
						xtc_db_query($ord_upd_qu);
                        xtc_db_perform(TABLE_ORDERS_STATUS_HISTORY, $ord_stat_hist_arr);
					}
                break;
			}
		}
	}

	function display() {

		//$customers_statuses_array = xtc_get_customers_statuses(); //what for ?, noRiddle

		// build Currency Select

		$orders_status_array = array(array('id' => '', 'text' => ($_SESSION['languages_id'] == '2' ? 'alle' : 'all')));
		$orders_status_query = xtc_db_query("SELECT orders_status_name, orders_status_id FROM ".TABLE_ORDERS_STATUS." WHERE language_id = ".(int)$_SESSION['languages_id']." ORDER BY sort_order"); //changed ORDER BY; noRiddle
		while ($orders_status = xtc_db_fetch_array($orders_status_query)) {
			$orders_status_array[] = array('id' => $orders_status['orders_status_id'],
                                           'text' => $orders_status['orders_status_name']
                                          );
		}
		$orders_status_new_array = $orders_status_array;
		$orders_status_new_array[0]['text'] = ($_SESSION['languages_id'] == '2' ? 'nicht ändern' : 'don\'t change');
		return array (
			'text' => ORDERS_STATUS . '<br>' .
			ORDERS_STATUS_DESC . '<br>' .
			xtc_draw_pull_down_menu('oders_status', $orders_status_array) . '<br><br>' .
            ORDERS_STATUS_NEW_DESC . '<br>' .
            xtc_draw_pull_down_menu('oders_status_new', $orders_status_new_array) . '<br>' .
            EXPORT_TYPE . '<br>' .
            EXPORT . '<br>' .
            xtc_draw_radio_field('export', 'no', false) . EXPORT_NO . '<br>' .
            xtc_draw_radio_field('export', 'yes', true) . EXPORT_YES . '<br>' .
            '<br><div align="center">' . xtc_button(BUTTON_EXPORT) .
            xtc_button_link(BUTTON_CANCEL, xtc_href_link('ah_module_export.php', 'set='.$_GET['set'].'&module=bestellungen')) .'</div>'
        );

	//	GM_PAKET.'<br>'.
	}

	function check() {
		if (!isset ($this->_check)) {
			$check_query = xtc_db_query("SELECT configuration_value FROM ".TABLE_CONFIGURATION." WHERE configuration_key = 'MODULE_BESTELLUNGEN_STATUS'");
			$this->_check = xtc_db_num_rows($check_query);
		}
		return $this->_check;
	}

	function install() {
		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, set_function, date_added) values ('MODULE_BESTELLUNGEN_FILE', 'shop_order_export.csv',  '6', '1', '', now())");
		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value,  configuration_group_id, sort_order, set_function, date_added) values ('MODULE_BESTELLUNGEN_STATUS', 'True',  '6', '1', 'xtc_cfg_select_option(array(\'True\', \'False\'), ', now())");
	}

	function remove() {
		xtc_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
	}

	function keys() {
		return array (
			'MODULE_BESTELLUNGEN_STATUS',
			'MODULE_BESTELLUNGEN_FILE'
		);
	}

}
?>
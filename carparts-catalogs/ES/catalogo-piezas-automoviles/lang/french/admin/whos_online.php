<?php
/* --------------------------------------------------------------
   $Id: whos_online.php 10502 2016-12-14 16:25:46Z GTB $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(whos_online.php,v 1.7 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (whos_online.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Qui est en ligne');

define('TABLE_HEADING_ONLINE', 'en ligne');
define('TABLE_HEADING_CUSTOMER_ID', 'ID');
define('TABLE_HEADING_FULL_NAME', 'Nom');
define('TABLE_HEADING_IP_ADDRESS', 'Adresse IP');
define('TABLE_HEADING_COUNTRY', 'Pays');
define('TABLE_HEADING_ENTRY_TIME', 'Heure de début');
define('TABLE_HEADING_LAST_CLICK', 'Dernier Click');
define('TABLE_HEADING_LAST_PAGE_URL', 'Dernière URL');
define('TABLE_HEADING_HTTP_REFERER', 'HTTP Referer');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_SHOPPING_CART', 'Panier');
define('TEXT_SHOPPING_CART_SUBTOTAL', 'Au total');
define('TEXT_NUMBER_OF_CUSTOMERS', '% clients sont en ligne en ce moment');
define('TEXT_EMPTY_CART', 'Le panier du client est vide');
define('TEXT_SESSION_IS_ENCRYPTED', '<hr><b>REMARQUE</b>:<br />Le contenu du panier ne peut pas être affiché.<br />La session est chiffrée avec Suhosin<br />(suhosin.session.encrypt = On)<br /> Pour déactiver le chiffrement contactez votre fournisseur.');
define('TEXT_ACTIVATE_WHOS_ONLINE', 'Activer qui est en ligne');
?>

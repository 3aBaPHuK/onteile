<?php
/* --------------------------------------------------------------
   $Id: removeoldpics.php 3503 2012-08-23 11:24:07Z dokuman $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(manufacturers.php,v 1.14 2003/02/16); www.oscommerce.com
   (c) 2003 nextcommerce (manufacturers.php,v 1.4 2003/08/14); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Borrar fotos de artículos antiguos');
define('LINK_INFO_TEXT', '<p>Con esta función puede borrar imágenes de artículos superfluos en los directorios:</p>
- /images/product_images/info_images<br/>
- /images/product_images/original_images<br/>
- /images/product_images/popup_images<br/>
- /images/product_images/thumbnail_images<br/>
<p>puede ser borrado del servidor web si no hay referencia a estas imágenes de artículo en la base de datos.<br/>Si una imagen ya no es utilizada por ningún producto, la imagen puede ser borrada sin problemas del servidor web.</p><br/>');
define('LINK_ORIGINAL', 'Borrar imágenes originales antiguas');
define('LINK_INFO', 'Borrar imágenes de información antiguas');
define('LINK_THUMBNAIL', 'Borrar imágenes en miniatura antiguas');
define('LINK_POPUP', 'Borrar imágenes popup antiguas');
define('LINK_MESSAGE', 'Se han borrado las imágenes superfluas de los artículos del directorio "/images/product_images/%s_images" .');
define('LINK_MESSAGE_NO_DELETE', 'No se han encontrado imágenes de artículos superfluas en el directorio "/images/product_images/%s_images" .');
?>

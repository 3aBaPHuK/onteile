<?php
/*******************************************************************************************
* file: /verify_keys.html.php
* (c) noRiddle 02-2016
*******************************************************************************************/

$indxs_out_str = '';
?>
<div class="script-code">
    <h2><?php echo ADMIN_TOOLS_VERIFY_KEYS_TITLE.'<br /><span class="sml">'.ADMIN_TOOLS_VERIFY_KEYS_DESC.'</span>'; ?></h2>
    <?php echo ADMIN_TOOLS_VERIFY_KEYS_GUIDANCE; ?>
    <div class="div-marg">
        <div class="bord">
            <?php echo xtc_draw_form('enable_keys_and_analyze_table', FILENAME_MASTER_ADMIN, 'script=verify_keys&action=enable_keys'); ?>
            <table class="table-indxs">
                    <?php
                    $indxs_out_str .= '<tr>'
                                   .'<th>Table</th><th>Key_name</th><th>Column_name</th><th>Cardinality</th><th>Comment</th><th>Enable keys <input type="checkbox" name="enblkeys" id="enblkeys" /></th><th>Analyze table <input type="checkbox" name="anlztabls" id="anlztabls" /></th><th>Optimize table <input type="checkbox" name="optmztabls" id="optmztabls" /></th><th>Send <button type="submit" onclick="this.blur();">'.BUTTON_SEND.'</button></th><th>Result</th>'
                                   .'</tr>';
                    foreach($indxs_table_arr as $table => $arr) {
                        $carr = count($arr);
                        $ccarr = (count($arr) - 1);
                        $thickbord = 'bord-2';
                        foreach($arr as $c => $names) {
                            $indxs_out_str .= '<tr>'
                                           .'<td class="'.($c == $ccarr ? $thickbord : '').'">'.$table.'</td>';
                            foreach($names as $name => $value) {
                                if($name == 'Cardinality' && $value == '') {
                                    $empt_val = ' style="background:#E6BBBB"';
                                } else if($name == 'Comment' && $value == 'disabled') {
                                    $empt_val = ' style="color:red"';
                                } else {
                                    $empt_val = '';
                                }
                                $indxs_out_str .= '<td class="'.($c == $ccarr ? $thickbord : '').'"'.$empt_val.'>'.$value.'</td>';
                            }
                            
                            //BOC checkbox to enable keys
                            if($c == '0') {
                                $indxs_out_str .= '<td rowspan="'.$carr.'" class="ctr '.$thickbord.'">'
                                               .'<input class="enbl-idxs" name="enbl_keys[]" value="'.$table.'" type="checkbox" />'
                                               .'</td>';
                            }
                            //EOC checkbox to enable keys
                            //BOC checkbox to analyze table
                            if($c == '0') {
                                $indxs_out_str .= '<td rowspan="'.$carr.'" class="ctr '.$thickbord.'">'
                                               .'<input class="anl-tbl" name="anlz_tbl[]" value="'.$table.'" type="checkbox" />'
                                               .'</td>';
                            }
                            //EOC checkbox to analyze table
                            //BOC checkbox to optimize
                            if($c == '0') {
                                $indxs_out_str .= '<td rowspan="'.$carr.'" class="ctr '.$thickbord.'">'
                                               .'<input class="opt-tbl" name="optmz_tbl[]" value="'.$table.'" type="checkbox" />'
                                               .'</td>';
                            }
                            //BOC checkbox to optimize
                            //BOC submit
                            if($c == '0') {
                                $eko_tbl = '';
                                $tbl_str = '<i>'.$table.'</i>';
                                $eko_arr = explode('|', $eko);
                                foreach($eko_arr as $val) {
                                    $eko_tbl .= strpos($val, $tbl_str) ? $val : '';
                                }
                                
                                $indxs_out_str .= '<td rowspan="'.$carr.'" class="'.$thickbord.'"><button type="submit" onclick="this.blur();">'.BUTTON_SEND.'</button></td>';
                                $indxs_out_str .= '<td rowspan="'.$carr.'" class="'.$thickbord.'"><div id="keys-out"><span style="color:#0A7E00;">'.$eko_tbl.'</span></div></td>';
                            }
                            //EOC submit
                            $indxs_out_str .= '</tr>';
                        }
                    }
                    //$indxs_out_str .= '</tr>';
                    echo $indxs_out_str;
                    ?>
            </table>
            </form>
        </div>
    </div>
    <div class="div-marg">
        <div class="bord">
            <table>
                <tr>
                    <td>
                        <?php echo xtc_draw_form('proddesc_id_notin_prod', FILENAME_MASTER_ADMIN, 'script=verify_keys&action=proddesc_id_notin_prod'); ?>
                            <table class="table-indxs">
                                <tr>
                                    <td class="thirtyperc"><?php echo ADMIN_TOOLS_FIND_PRODDESCIDS_NOTIN_PROD; ?></td>
                                    <td><button type="submit" onclick="this.blur();"><?php echo BUTTON_SEND; ?></button></td>
                                    <td><?php echo $proid_out1; ?></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo xtc_draw_form('prodprodtocat_id_notin_prod', FILENAME_MASTER_ADMIN, 'script=verify_keys&action=prodprodtocat_id_notin_prod'); ?>
                            <table class="table-indxs">
                                <tr>
                                    <td class="thirtyperc"><?php echo ADMIN_TOOLS_FIND_PRODTOCATIDS_NOTIN_PROD; ?></td>
                                    <td><button type="submit" onclick="this.blur();"><?php echo BUTTON_SEND; ?></button></td>
                                    <td><?php echo $proid_out2; ?></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
                <tr>
                <tr>
                    <td>
                        <?php echo xtc_draw_form('prodprod_id_notin_proddesc', FILENAME_MASTER_ADMIN, 'script=verify_keys&action=prodprod_id_notin_proddesc'); ?>
                            <table class="table-indxs">
                                <tr>
                                    <td class="thirtyperc"><?php echo ADMIN_TOOLS_FIND_PRODPRODIDS_NOTIN_PRODDESC; ?></td>
                                    <td><button type="submit" onclick="this.blur();"><?php echo BUTTON_SEND; ?></button></td>
                                    <td><?php echo $proid_out3; ?></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo xtc_draw_form('prodid_more_than_one_prodtocat', FILENAME_MASTER_ADMIN, 'script=verify_keys&action=prodid_more_than_one_prodtocat'); ?>
                            <table class="table-indxs">
                                <tr>
                                    <td class="thirtyperc"><?php echo ADMIN_TOOLS_FIND_PRODIDS_MORETHANONE_PRODTOCAT; ?></td>
                                    <td><button type="submit" onclick="this.blur();"><?php echo BUTTON_SEND; ?></button></td>
                                    <td><?php echo $proid_out4; ?></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo xtc_draw_form('prodid_in_prodtocat_top', FILENAME_MASTER_ADMIN, 'script=verify_keys&action=prodid_in_prodtocat_top'); ?>
                            <table class="table-indxs">
                                <tr>
                                    <td class="thirtyperc"><?php echo ADMIN_TOOLS_FIND_PRODIDS_IN_PRODTOCAT_TOP; ?></td>
                                    <td><button type="submit" onclick="this.blur();"><?php echo BUTTON_SEND; ?></button></td>
                                    <td><?php echo $proid_out5; ?></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
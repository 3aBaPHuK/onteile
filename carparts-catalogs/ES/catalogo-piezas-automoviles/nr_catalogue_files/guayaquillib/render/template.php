<?php
/**********************************************
* file reworked and adadapted by noRiddle
*
* (c) noRiddle 03-2018
**********************************************/

//BOC make Config available, noRiddle
//include_once('nr_catalogue_files/config.php');
//include_once('nr_catalogue_files/localization_'.Config::$ui_localization.'.php');
//EOC make Config available, noRiddle

interface IGuayaquilExtender
{
	public function GetLocalizedString($name, $params = false, $renderer);

	public function AppendJavaScript($filename, $renderer);
	
	public function AppendCSS($filename, $renderer);

	public function FormatLink($type, $dataItem, $catalog, $renderer);

	public function Convert2uri($filename);
}

class GuayaquilTemplate
{
	var $extender;

	public function __construct(IGuayaquilExtender $extender)
	{
		$this->extender = $extender;

		static $guayaquil_templateinitialized;
		
		if (!isset($guayaquil_templateinitialized))
		{
			//$this->AppendCSS(dirname(__FILE__).'/guayaquil.css'); //include this in /templates/catalogue/css/general.css.php, noRiddle
			//$this->AppendJavaScript(dirname(__FILE__).'/jquery.js'); //not needed since shop has got jQuery already in /includes/header.php, noRiddle
			
			$guayaquil_templateinitialized = 1;
		}
	}

	public function GetLocalizedString($name, $params = false)
	{
		if ($this->extender == NULL)
			return $name;

		return $this->extender->GetLocalizedString($name, $params, $this);
	}

	public function FormatLink($type, $dataItem, $catalog)
	{
		if ($this->extender == NULL)
			die('Add IGuayaquilExtender object to template or redefine method FormatLink');

		return $this->extender->FormatLink($type, $dataItem, $catalog, $this);
	}

	public function AppendJavaScript($filename)
	{
		if ($this->extender == NULL)
			die('Add IGuayaquilExtender object to template or redefine method AppendJavaScript');

		return $this->extender->AppendJavaScript($filename, $this);
	}

	public function AppendCSS($filename)
	{
		if ($this->extender == NULL)
			die('Add IGuayaquilExtender object to template or redefine method AppendCSS');

		return $this->extender->AppendCSS($filename, $this);
	}

	public function Convert2uri($filename)
	{
		if ($this->extender == NULL)
			die('Add IGuayaquilExtender object to template or redefine method Convert2uri');

		return $this->extender->Convert2uri($filename, $this);
	}
}

class GuayaquilToolbar
{
    static $toolbar;

    var $buttons;

    public static function AddButton($title, $url, $onclick = null, $alt = null, $img = null, $id = null)
    {
        if (!self::$toolbar)
            self::$toolbar = new GuayaquilToolbar();

        self::$toolbar->buttons[] = array('title' => $title, 'url' => $url, 'onclick' => $onclick, 'alt' => $alt, 'img' => $img, 'id' => $id);
    }

    public static function Draw()
    {
        //global $data; //make $data global, noRiddle
        if (!GuayaquilToolbar::$toolbar)
            return '';

        $html = '';

        $toolbar = GuayaquilToolbar::$toolbar;
        
        //BOC extend toolbar, noRiddle
        $html .= '<div id="main-top cf">';
        $html .= '<div class="log-and-butt cf">';
        //$html .= '<div class="brand-logo"><img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/'.Config::$nr_icon_arr[$_GET['c']].'" alt ="" /></div>';
        $html .= '<div class="top-buttons cf">';
        
        $btns_cnt = 0; // = count($toolbar->buttons); //define array size, noRiddle
        foreach ($toolbar->buttons as $button) {
            //$html .= $toolbar->DrawButton($button);
            $btns_cnt++; //augment counter, noRiddle
            $html .= $toolbar->DrawButton($button, $btns_cnt); //add counter, noRiddle
        }
            
        /*$html .= '<a href="/katalog_shop_test/"><span class="btn button-catalogue"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_brand_choice').'</span></a>&nbsp;'
                .'<a href="'.xtc_href_link(FILENAME_CATALOGUE_SHOPPING_CART).'"><span class="btn button-catalogue"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;'.CommonExtender::FormatLocalizedString('nr_shopp_cart').'</span></a>';*/
        $html .= '</div>';
        $html .= '</div>'; //end log-and-butt
        $html .= '</div>'; //end main-top
        if(isset($_GET['coID']) && $_GET['coID'] == '1003') {
            $html .= '</div>'; //end col-md-4
        }
        //EOC extend toolbar, noRiddle

        return $toolbar->DrawContainer($html);
    }

    private function DrawContainer($content)
    {
        if ($content)
            //BOC new top menu, noRiddle
            /*return '<b class="xtop"><b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b></b><div id="guayaquil_toolbar" class="xboxcontent">
                    '.$content.'
                </div><b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b></b><br>';*/
            
            return $content;
            //EOC new top menu, noRiddle
        return '';
    }

    private function DrawButton($button, $btns_cnt)
    {
        //BOC new top menu, noRiddle
        /*return '<span class="g_ToolbarButton" '.($button['id'] ? 'id="'.$button['id'].'"' : '').'>
                <a href="'.$button['url'].'" '.($button['onclick'] ? ' onClick="'.$button['onclick'].'"' : '').'>'.
                   ($button['img'] ? '<img src="'.$button['img'].'" alt="'.$button['alt'].'">' : '').' '.
                   $button['title'].'
               </a>
           </span>';*/
        
        
        //$nr_html = '<a href="'.$button['url'].'" '.($button['onclick'] ? ' onClick="'.$button['onclick'].'"' : '').'>'.($button['img'] ? '<img src="'.$button['img'].'" alt="'.$button['alt'].'">' : '').'<span class="btn button-catalogue"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;'.$button['title'].'</span></a>&nbsp;';
        
        $lr_flag = $btns_cnt % 2 == 0 ? 'fr' : 'fl';
        
        $nr_html = '<a href="'.$button['url'].'" '.($button['onclick'] ? ' onClick="'.$button['onclick'].'"' : '').'>'.($button['img'] ? '<img src="'.$button['img'].'" alt="'.$button['alt'].'">' : '');
        $nr_html .= '<span class="cssButton cssButtonColor1 cssbtn-'.$lr_flag.'"><i class="fa fa-eye"></i><span class="cssButtonText">';
        $nr_html .= $button['img'] ? ('<img src="'.$button['img'].'" alt="'.$button['alt'].'"> '.$button['title']) : $button['title'];
        $nr_html .= '</span></span></a>';

        return $nr_html;
        //EOC new top menu, noRiddle
    }
}
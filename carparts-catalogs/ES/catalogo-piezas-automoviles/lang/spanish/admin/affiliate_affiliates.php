<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_affiliates.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_affiliates.php, v 1.9 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Participantes del Programa de Afiliados');
define('HEADING_TITLE_SEARCH', 'Búsqueda:');

define('TABLE_HEADING_FIRSTNAME', 'Nombre');
define('TABLE_HEADING_LASTNAME', 'Apellido');
define('TABLE_HEADING_COMMISSION', 'Comisión');
define('TABLE_HEADING_ACCOUNT', 'Estado de cuenta');
define('TABLE_HEADING_USERHOMEPAGE', 'Página web');
define('TABLE_HEADING_ACCOUNT_CREATED', 'Acceso creado el');
define('TABLE_HEADING_ACTION', 'Acción');
define('TABLE_HEADING_AFFILIATE_ID', 'ID de afiliado');

define('TEXT_DATE_ACCOUNT_CREATED', 'Acceso creado el:');
define('TEXT_DATE_ACCOUNT_LAST_MODIFIED', 'última modificación:');
define('TEXT_INFO_DATE_LAST_LOGON', 'último inicio de sesión:');
define('TEXT_INFO_NUMBER_OF_LOGONS', 'Número de inicios de sesión:');
define('TEXT_INFO_COMMISSION', 'Comisión');
define('TEXT_INFO_TIERS_ALLOWED', 'Puede tener subafiliados:');
define('TEXT_INFO_NUMBER_OF_SALES', 'Número de ventas:');
define('TEXT_INFO_COUNTRY', 'País:');
define('TEXT_INFO_SALES_TOTAL', 'Suma de todas las ventas:');
define('TEXT_INFO_AFFILIATE_TOTAL', 'Comisión:');
define('TEXT_DELETE_INTRO', '¿Está seguro de eliminar este afiliado?');
define('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'Eliminar afiliado');
define('TEXT_DISPLAY_NUMBER_OF_AFFILIATES', 'Se muestran <b>%d</b> hasta <b>%d</b> (de un total de <b>%d</b> afiliados)');

define('ENTRY_AFFILIATE_PAYMENT_DETAILS', 'a nombre de:');
define('ENTRY_AFFILIATE_PAYMENT_CHECK', 'Cheque para:');
define('ENTRY_AFFILIATE_PAYMENT_PAYPAL', 'correo electrónico de la cuenta PayPal:');
define('ENTRY_AFFILIATE_PAYMENT_BANK_NAME', 'Nombre del banco:');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NAME', 'Titular de la cuenta:');
define('ENTRY_AFFILIATE_PAYMENT_BANK_ACCOUNT_NUMBER', 'Número de la cuenta:');
define('ENTRY_AFFILIATE_PAYMENT_BANK_BRANCH_NUMBER', 'Código bancario:');
define('ENTRY_AFFILIATE_PAYMENT_BANK_SWIFT_CODE', 'Código SWIFT:');
define('ENTRY_AFFILIATE_COMPANY', 'Empresa');
define('ENTRY_AFFILIATE_COMPANY_TAXID', 'Número de IVA:');
define('ENTRY_AFFILIATE_HOMEPAGE', 'Página web');
define('ENTRY_AFFILIATE_COMMISSION', 'Comisión por venta en %');
define('ENTRY_AFFILIATE_TIERS_ALLOWED', 'Puede tener subafiliados: ');

define('CATEGORY_COMMISSION', 'Comisión individual');
define('CATEGORY_PAYMENT_DETAILS', 'El pago se realiza a través de:');

define('TYPE_BELOW', 'Por favor introducir abajo');
define('PLEASE_SELECT', 'Seleccionar');
?>

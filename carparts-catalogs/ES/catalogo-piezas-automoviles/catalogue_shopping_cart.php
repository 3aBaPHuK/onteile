<?php
/********************************************
* file: catalogue_shopping_cart.php
* use: shopping cart for catalogue
* (c) noRiddle 10-2016
********************************************/
$cart_empty = false;
require ("includes/application_top.php");
// create smarty elements
$smarty = new Smarty;
require (DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/source/boxes.php');
// include needed functions
require_once (DIR_FS_INC.'xtc_image_submit.inc.php');

$breadcrumb->add(NAVBAR_TITLE_SHOPPING_CART, xtc_href_link(FILENAME_CATALOGUE_SHOPPING_CART, '', $request_type));

//echo '<br><br><br><pre>'.print_r($_SESSION['catalogue_cart'.$_SESSION['shopsess']], true).'</pre>';

require (DIR_WS_INCLUDES.'header.php');

//BOC restore also contents which has been in database and client came from other shop already logged in, noRiddle
//(normally restore_contents is only called in login.php and create_account as well as create_guest_account)
if(isset($_SESSION['customer_id'])) {
    $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->restore_contents();
}
//EOC restore also contents which has been in database and client came from other shop already logged in, noRiddle

if ($_SESSION['catalogue_cart'.$_SESSION['shopsess']]->count_all_contents() > 0) {
    $catalogue_products = $_SESSION['catalogue_cart'.$_SESSION['shopsess']]->get_products();
    //echo '<pre>$catalogue_products: '.print_r($catalogue_products, true).'</pre>';
    
    require (DIR_WS_MODULES.'catalogue_order_details_cart.php');
} else {
    // empty cart
    $cart_empty = true;
    $smarty->assign('cart_empty', $cart_empty);
    $smarty->assign('BUTTON_CONTINUE', '<a href="'.xtc_href_link(FILENAME_DEFAULT).'">'.xtc_image_button('button_continue.gif', IMAGE_BUTTON_CONTINUE).'</a>');
}

unset($_SESSION['new_prid_in_catcart_'.$_SESSION['shopsess']]);

//BOF - web28 - 2011-05-15 - new continue shopping link
if (!empty($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], FILENAME_CATALOGUE_SHOPPING_CART) === false  && strpos($_SERVER['HTTP_REFERER'],'in_cart') === false) {
  $_SESSION['continue_link'] = $_SERVER['HTTP_REFERER'];
}
if(!empty($_SESSION['continue_link'])) {
  $smarty->assign('CONTINUE_LINK',$_SESSION['continue_link']);
}
$smarty->assign('BUTTON_CONTINUE_SHOPPING', xtc_image_button('button_continue_shopping.gif', IMAGE_BUTTON_CONTINUE_SHOPPING));
//EOF - web28 - 2011-05-15 - new continue shopping link

$smarty->assign('language', $_SESSION['language']);
$main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/catalogue_shopping_cart.html');
$smarty->assign('main_content', $main_content);
$smarty->caching = 0;
if (!defined('RM')) {
  $smarty->load_filter('output', 'note');
}
$smarty->display(CURRENT_TEMPLATE.'/index.html');

include ('includes/application_bottom.php');
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id: janolaw.php 2011-11-24 modified-shop $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com
   (c) 2003   nextcommerce (invoice.php,v 1.6 2003/08/24); www.nextcommerce.org
   (c) 2005 XT-Commerce - community made shopping http://www.xt-commerce.com ($Id: billiger.php 950 2005-05-14 16:45:21Z mz $)
   (c) 2008 Gambio OHG (billiger.php 2008-11-11 gambio)

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_JANOLAW_TEXT_TITLE', 'janolaw AGB Hosting-Service');
define('MODULE_JANOLAW_TEXT_DESCRIPTION', '<a href="http://www.janolaw.de/internetrecht/agb/agb-hosting-service/modified/index.html?partnerid=8764#menu" target="_blank"><img src="images/janolaw/janolaw_185x35.png" border=0></a><br /><br />El gran portal jurídico alemán janolaw ofrece soluciones personalizadas para sus preguntas legales - desde la línea directa de abogados hasta contratos individuales con garantía de abogado. Con AGB Hosting-Service para tiendas de Internet usted puede adaptar individualmente a su tienda los documentos jurídicos básicos como términos y condiciones generales, instrucciones de revocación, Impressum y declaración de protección de datos y dejar que el equipo de janolaw se encargue de la actualización contínua de los mismos. No es posible una mayor protección.<br /><br /><a href="http://www.janolaw.de/internetrecht/agb/agb-hosting-service/modified/index.html?partnerid=8764#menu" target="_blank"><strong><u>Aquí puede encontrar la oferta<u></strong></a>');
define('MODULE_JANOLAW_USER_ID_TITLE', '<hr noshade>ID de usuario');
define('MODULE_JANOLAW_USER_ID_DESC', 'Su ID de usuario');
define('MODULE_JANOLAW_SHOP_ID_TITLE', 'ID de la tienda');
define('MODULE_JANOLAW_SHOP_ID_DESC', 'El ID de la tienda de su tienda online');
define('MODULE_JANOLAW_STATUS_DESC', '¿Activar módulo?');
define('MODULE_JANOLAW_STATUS_TITLE', 'Estado');
define('MODULE_JANOLAW_TYPE_TITLE', '<hr noshade>Guardar como');
define('MODULE_JANOLAW_TYPE_DESC', '¿Deben guardarse los datos en un archivo o en la base de datos?');
define('MODULE_JANOLAW_FORMAT_TITLE', 'Tipo de formato');
define('MODULE_JANOLAW_FORMAT_DESC', '¿Deben guardarse los datos como texto o HTML?');
define('MODULE_JANOLAW_UPDATE_INTERVAL_TITLE', '<hr noshade>Intervalo de actualización');
define('MODULE_JANOLAW_UPDATE_INTERVAL_DESC', '¿En qué intervalos deben actualizarse los datos?');
define('MODULE_JANOLAW_ERROR', 'Por favor, compruebe la asignación de los documentos.');

define('MODULE_JANOLAW_TYPE_DATASECURITY_TITLE', '<hr noshade>Texto jurídico sobre protección de datos');
define('MODULE_JANOLAW_TYPE_DATASECURITY_DESC', 'Por favor, indique en qué página debe insertarse automáticamente este texto jurídico.');
define('MODULE_JANOLAW_PDF_DATASECURITY_TITLE', 'PDF como Descarga');
define('MODULE_JANOLAW_PDF_DATASECURITY_DESC', '¿Deben los datos también ser guardados como PDF y un enlace insertado? <br/><b>Importante:</b> ¡Esto sólo funciona en la versión HTML!');
define('MODULE_JANOLAW_MAIL_DATASECURITY_TITLE', 'PDF como archivo adjunto por correo electrónico');
define('MODULE_JANOLAW_MAIL_DATASECURITY_DESC', '¿Debe enviarse el PDF como archivo adjunto a la confirmación del encargo?');

define('MODULE_JANOLAW_TYPE_TERMS_TITLE', '<hr noshade>Texto jurídico sobre términos y condiciones generales');
define('MODULE_JANOLAW_TYPE_TERMS_DESC', 'Por favor, indique en qué página debe insertarse automáticamente este texto jurídico.');
define('MODULE_JANOLAW_PDF_TERMS_TITLE', 'PDF como Descarga');
define('MODULE_JANOLAW_PDF_TERMS_DESC', '¿Deben los datos también ser guardados como PDF y un enlace insertado? <br/><b>Importante:</b> ¡Esto sólo funciona en la versión HTML!');
define('MODULE_JANOLAW_MAIL_TERMS_TITLE', 'PDF como archivo adjunto por correo electrónico');
define('MODULE_JANOLAW_MAIL_TERMS_DESC', '¿Debe enviarse el PDF como archivo adjunto a la confirmación del encargo?');

define('MODULE_JANOLAW_TYPE_LEGALDETAILS_TITLE', '<hr noshade>Texto jurídico sobre Impressum');
define('MODULE_JANOLAW_TYPE_LEGALDETAILS_DESC', 'Por favor, indique en qué página debe insertarse automáticamente este texto jurídico.');
define('MODULE_JANOLAW_PDF_LEGALDETAILS_TITLE', 'PDF como Descarga');
define('MODULE_JANOLAW_PDF_LEGALDETAILS_DESC', '¿Deben los datos también ser guardados como PDF y un enlace insertado? <br/><b>Importante:</b> ¡Esto sólo funciona en la versión HTML!');
define('MODULE_JANOLAW_MAIL_LEGALDETAILS_TITLE', 'PDF como archivo adjunto por correo electrónico');
define('MODULE_JANOLAW_MAIL_LEGALDETAILS_DESC', '¿Debe enviarse el PDF como archivo adjunto a la confirmación del encargo?');

define('MODULE_JANOLAW_TYPE_REVOCATION_TITLE', '<hr noshade>Texto jurídico sobre revocación');
define('MODULE_JANOLAW_TYPE_REVOCATION_DESC', 'Por favor, indique en qué página debe insertarse automáticamente este texto jurídico.');
define('MODULE_JANOLAW_PDF_REVOCATION_TITLE', 'PDF como Descarga');
define('MODULE_JANOLAW_PDF_REVOCATION_DESC', '¿Deben los datos también ser guardados como PDF y un enlace insertado? <br/><b>Importante:</b> ¡Esto sólo funciona en la versión HTML!');
define('MODULE_JANOLAW_MAIL_REVOCATION_TITLE', 'PDF como archivo adjunto por correo electrónico');
define('MODULE_JANOLAW_MAIL_REVOCATION_DESC', '¿Debe enviarse el PDF como archivo adjunto a la confirmación del encargo?');

define('MODULE_JANOLAW_TYPE_WITHDRAWAL_TITLE', '<hr noshade>Texto jurídico sobre el formulario de revocación');
define('MODULE_JANOLAW_TYPE_WITHDRAWAL_DESC', 'Por favor, indique en qué página debe insertarse automáticamente este texto jurídico.<br/><br/><b>Importante:</b> esto sólo funciona a partir de la versión 3. El cambio puede ser hecho por Janolaw.');
define('MODULE_JANOLAW_PDF_WITHDRAWAL_TITLE', 'PDF como Descarga');
define('MODULE_JANOLAW_PDF_WITHDRAWAL_DESC', '¿Deben los datos también ser guardados como PDF y un enlace insertado? <br/><b>Importante:</b> ¡Esto sólo funciona en la versión HTML!');
define('MODULE_JANOLAW_MAIL_WITHDRAWAL_TITLE', 'PDF como archivo adjunto por correo electrónico');
define('MODULE_JANOLAW_MAIL_WITHDRAWAL_DESC', '¿Debe enviarse el PDF como archivo adjunto a la confirmación del encargo?');
define('MODULE_JANOLAW_WITHDRAWAL_COMBINE_TITLE', 'Combinación de instrucciones de revocación y formulario de revocación');
define('MODULE_JANOLAW_WITHDRAWAL_COMBINE_DESC', '¿Debe crearse una combinación de instrucciones de revocación con un formulario de revocación?');

?>

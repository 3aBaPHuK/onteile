<?php
/*
  $Id: backup.php,v 1.16 2002/03/16 21:30:02 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Banque de données gestionnaire de sauvegarde');

define('TEXT_INFO_DO_BACKUP', 'La banque de données sera sauvegardée!');
define('TEXT_INFO_DO_BACKUP_OK', 'La banque de données a été sauvegardée avec succès!');
define('TEXT_INFO_DO_GZIP', 'Le fichier de sauvegarde est en train de compression!');
define('TEXT_INFO_WAIT', 'Veuillez patienter!');

define('TEXT_INFO_DO_RESTORE', 'La banque de données est en restauration!');
define('TEXT_INFO_DO_RESTORE_OK', 'La banque de données a été restaurée avec succès!');
define('TEXT_INFO_DO_GUNZIP', 'Le fichier de sauvegarde est en train de compression!');

define('ERROR_BACKUP_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le répertoire de sauvegarde n\'existe pas. Veuillez corriger l\'erreur sous configure.php.');
define('ERROR_BACKUP_DIRECTORY_NOT_WRITEABLE', 'Erreur: Écrire dans le répertoire de sauvegarde n\'est pas possible.');
define('ERROR_DOWNLOAD_LINK_NOT_ACCEPTABLE', 'Erreur: Le lien de téléchargement n\'est pas acceptable.');
define('ERROR_DECOMPRESSOR_NOT_AVAILABLE', 'Erreur: Pas de décompresseur disponible.');
define('ERROR_UNKNOWN_FILE_TYPE', 'Erreur: type de fichier inconnu.');
define('ERROR_RESTORE_FAILES', 'Erreur: Restauration échouée.');
define('ERROR_DATABASE_SAVED', 'Erreur: La banque de données ne peut pas être sauvegardée.');
define('ERROR_TEXT_PATH', 'Erreur: Le chemin vers mysqldump ne pouvait pas être trouvé ou indiqué!');

define('SUCCESS_LAST_RESTORE_CLEARED', 'Succès: La dernière date de restauration a été supprimée.');
define('SUCCESS_DATABASE_SAVED', 'Succès: La banque de données a été sauvegardée.');
define('SUCCESS_DATABASE_RESTORED', 'Succès: La banque de données a été restauré.');
define('SUCCESS_BACKUP_DELETED', 'Succès: La sauvegarde a été supprimée.');

define('TEXT_BACKUP_UNCOMPRESSED', 'Le fichier de sauvegarde a été compressé:');

define('TEXT_SIMULATION', '<br>(Simulation avec fichier log)');

?>

<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_configuration.php 34 2013-01-06 08:29:47Z Hubi $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('AFFILIATE_EMAIL_ADDRESS_TITLE', 'adresse email');
define('AFFILIATE_EMAIL_ADDRESS_DESC', 'L’adresse email pour le programme affilié <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/e-mail-adresse.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_PERCENT_TITLE', 'Affiliate Pay Per Sale %-Rate');
define('AFFILIATE_PERCENT_DESC', 'Le taux en pourcentage pour le programme Pay Per Sale.   <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-pay-per-sale-rate.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_THRESHOLD_TITLE', 'limite de paiement');
define('AFFILIATE_THRESHOLD_DESC', 'Notre limite de paiement aux partenaires affiliés <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/auszahlungsgrenze.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_COOKIE_LIFETIME_TITLE', 'Cookie Lifetime');
define('AFFILIATE_COOKIE_LIFETIME_DESC', 'Combien de temps (en secondes) le clic d\'un partenaire affilié est valable. <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/cookie-lifetime.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_BILLING_TIME_TITLE', 'période de facturation');
define('AFFILIATE_BILLING_TIME_DESC', 'Le temps qui doit passer entre une commande avec succès et le crédit chez le partenaire affilié. Nécessaire en cas de retour    <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/abrechnungszeit.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_PAYMENT_ORDER_MIN_STATUS_TITLE', 'Order status');
define('AFFILIATE_PAYMENT_ORDER_MIN_STATUS_DESC', 'Le montant d\'une commande á partir duquel elle donne droit à une commission. Plusieurs séparés par point-virgule (p.ex. 3;7;9)<a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/order-status.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_CHECK_TITLE', 'chèque du partenaire affilié');
define('AFFILIATE_USE_CHECK_DESC', 'Payer le partenaire par chèque <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-scheck.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_PAYPAL_TITLE', 'PayPal partenaire');
define('AFFILIATE_USE_PAYPAL_DESC', 'Payer le partenaire par PayPal <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-paypal.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_BANK_TITLE', 'Banque du partanaire');
define('AFFILIATE_USE_BANK_DESC', 'Payer le partenaire par virement  <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/affiliate-bank.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_INDIVIDUAL_PERCENTAGE_TITLE', 'Pourcentage individuel');
define('AFFILIATE_INDIVIDUAL_PERCENTAGE_DESC', 'Fixer le pourcantage par le partenaire  <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/individueller-prozentsatz.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_TIER_TITLE', 'système de classification');
define('AFFILIATE_USE_TIER_DESC', 'Utiliser des classes multiples des programmes affiliés <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/klassensystem.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_ALLOWED_TITLE', 'Standard pour des sous-partenaires');
define('AFFILIATE_TIER_ALLOWED_DESC', 'Ont de nouveaux partenaires le droit d\'avoir des sous-partenaires. <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/standard-fuer-subaffiliates.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_LEVELS_TITLE', 'Nombre classes');
define('AFFILIATE_TIER_LEVELS_DESC', 'Nombre des classes utilisées au sein du système de classification <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/anzahl-klassen.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_PERCENTAGE_TITLE', 'Pourcentage pour le système de classification');
define('AFFILIATE_TIER_PERCENTAGE_DESC', 'Pourcentage individuel pour les classes inférieurs <br>exemple 8.00;5.00;1.00     <a href="http://www.netz-designer.de/gambio-affiliate/administration/parameter/prozentsaetze-fuer-klassensystem.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
?>

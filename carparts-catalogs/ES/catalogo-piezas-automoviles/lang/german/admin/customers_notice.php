<?php
define('HEADING_TITLE', 'Kunden Hinweise');
define('HEADING_SUBTITLE', 'Immer zeitgesteuert, f&uuml;r alle oder nur f&uuml;r einige Kunden-Gruppen');
define('HEADING_SUBTITLE_NEW_NOTICE', 'neuen Hinweis erstellen');
define('HEADING_SUBTITLE_EDIT_NOTICE', 'Hinweis "%s" bearbeiten');
define('HEADING_TITLE_SEARCH', 'Suchen');
define('HEADING_TITLE_STATUS', 'Status');
define('HEADING_BOX_TITLE_DEFAULT', 'Hinweis "%s"');
define('HEADING_BOX_TITLE_DELETE', 'Hinweis l&ouml;schen');

define('BUTTON_CREATE_NOTICE', 'neuen Hinweis erstellen');
define('BUTTON_EDIT_NOTICE', 'Hinweis bearbeiten');
define('BUTTON_DELETE_NOTICE', 'Hinweis l&ouml;schen');
define('BUTTON_DELETE_NOTICE_CONFIRMATION', 'L&ouml;schen best&auml;tigen!');

define('LABEL_TITLE', 'Titel:');
define('LABEL_DESCRIPTION', 'Text:');
define('LABEL_STATUS', 'Hinweis aktiv ?');
define('LABEL_POSITION', 'Position:');
define('LABEL_STARTDATE', 'von:');
define('LABEL_ENDDATE', 'bis:');
define('LABEL_TEMPLATE', 'Template:');
define('LABEL_TEMPLATE_HINT', '(Das Template newsletter.html wird bei nicht aktiviertem Newsletter als PopUp angezeigt, wenn der Kunde noch kein Newsletterempf&auml;nger ist.)');
define('LABEL_CUSTOMERS_GROUP', 'Kundengruppe:');
define('LABEL_CUSTOMERS_GROUPS', 'Kundengruppen:');
define('LABEL_PAGES', 'Seiten:');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_TITLE', 'Titel');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_POSITION', 'Pos.');
define('TABLE_HEADING_STARTDATE', 'Start');
define('TABLE_HEADING_ENDDATE', 'Ende');
define('TABLE_HEADING_TEMPLATE', 'Template');
define('TABLE_HEADING_CUSTOMERS_STATUS', 'Kundengruppen');
define('TABLE_HEADING_PAGES', 'Seiten');

define('ERROR_MISSING_TITLE', 'Bitte gib deinem Hinweis eine &Uuml;berschrift.');
define('ERROR_MISSING_DESCRIPTION', 'Bitte gib etwas Text f&uuml;r den Hinweis ein.');
define('ERROR_INVALID_STARTDATE', 'Das Start-Datum ist ung&uuml;ltig, bitte achte auf das Format.');
define('ERROR_INVALID_ENDDATE', 'Das End-Ddatum ist ung&uuml;ltig, bitte achte auf das Format.');

define('DATETIME_FORMAT', 'YYYY-MM-DD HH:MM'); //took of seconds, we use datetimepicker without seconds, noRiddle

define('TEXT_ACTIVE', 'aktiv');
define('TEXT_INACTIVE', 'inaktiv');
define('TEXT_DELETE_NOTICE_CONFIRM', 'Hinweis "%s" wirklich l&ouml;schen?');
define('TEXT_ALL', 'alle');
define('TEXT_OPTIONAL', 'optional');

define('FIELD_VALUE_PAGES_INDEX', 'Startseite');
define('FIELD_VALUE_PAGES_CATEGORY', 'Kategorie');
define('FIELD_VALUE_PAGES_PRODUCT_INFO', 'Produktdetails');
define('FIELD_VALUE_PAGES_SHOP_CONTENT', 'Content-Seiten');
define('FIELD_VALUE_PAGES_SHOPPING_CART', 'Warenkorb');
define('FIELD_VALUE_PAGES_ACCOUNT', 'Kontobereich');
define('FIELD_VALUE_PAGES_CHECKOUT', 'Checkoutbereich');
?>
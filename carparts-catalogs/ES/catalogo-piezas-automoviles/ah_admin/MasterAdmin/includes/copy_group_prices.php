<?php
/*******************************************************************************************
* copy certain group prices into other tables with group prices
* file: /copy_group_prices.php
* (c) noRiddle 12-2015
*******************************************************************************************/

$pers_off_col_query = xtc_db_query("SHOW COLUMNS FROM csv_import_aux LIKE 'personal_offers_by_customers_status_%'");
while($pers_off_cols = xtc_db_fetch_array($pers_off_col_query)) {
    $ie_personal_offers_array[] = $pers_off_cols['Field'];
}
//echo '<pre>'.print_r($ie_personal_offers_array, true).'</pre>';

//BOC copy all
$pers_off_to_copy_arr = array(1,3,7,8);

function copy_tables_to_euql($output = 'no') {
    global $pers_off_to_copy_arr;

    $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] == '';
        
    foreach($pers_off_to_copy_arr as $k) {
        switch ($k) {
            //BOC 1 in 2, 6, 11, 12
            case '1':
                xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_2");
                xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_6");
                xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_11");
                xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_12");
                
                if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_2 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 2 übernommen</span><br />';
                }
                if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_6 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 6 übernommen</span><br />';
                }
                if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_11 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 11 übernommen</span><br />';
                }
                if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_12 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 12 übernommen</span><br /><br />';
                }
            break;
            //EOC 1 in 2, 6, 11, 12
            //BOC 3 in 5
            case  '3':
                xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_5");

                if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_5 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 5 übernommen</span><br /><br />';
                }
            break;
            //EOC 3 in 5
            //BOC 7 in 15, 16(commented out)
            case '7':
                xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_15");
                //xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_16");

                if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_15 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 15 übernommen</span><br /><br />';
                }
                //BOC we don't need group 16 here any more, will be used for sth else, noRiddle
                /*if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_16 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 16 übernommen</span><br /><br />';
                }*/
                //EOC we don't need group 16 here any more, will be used for sth else, noRiddle
            break;
            //EOC 7 in 15, 16
            //BOC 8 in 9, 10, 13, 14
            case '8':
                xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_9");
                xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_10");
                xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_13");
                xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_14");

                if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_9 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 9 übernommen</span><br />';
                }

                if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_10 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 10 übernommen</span><br />';
                }

                if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_13 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 13 übernommen</span><br />';
                }

                if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_14 SELECT * FROM personal_offers_by_customers_status_".$k)) {
                    if($output == 'yes')
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$k.' wurden in Kundenstatus 14 übernommen</span><br />';
                }
            break;
            //EOC 8 in 9, 10, 13, 14
        }
    }
    return true;
}

if(isset($_GET['action']) && $_GET['action'] == 'copy_persoff_all') {
    if(isset($_POST['copy_all_persoff']) && $_POST['copy_all_persoff'] == '1') {
        copy_tables_to_euql('yes');
        
        //delete DB cache if activated
        if(defined('DB_CACHE') && DB_CACHE == 'true') {
            clear_dir(SQL_CACHEDIR);
        }
        
        xtc_redirect(xtc_href_link(FILENAME_MASTER_ADMIN, 'script=copy_group_prices'));
    }
}
//EOC copy all

//BOC change all prices with factor, 02-2021, noRiddle
$last_persoff_factor_qu = xtc_db_query("SELECT * FROM nr_pers_off_factor");
$last_persoff_factor_arr = array();
while($last_persoff_factor_array = xtc_db_fetch_array($last_persoff_factor_qu)) {
    $last_persoff_factor_arr[$last_persoff_factor_array['personal_offers_table']] = $last_persoff_factor_array;
    $last_persoff_factor_arr[$last_persoff_factor_array['personal_offers_table']]['last_modified'] = $last_persoff_factor_arr[$last_persoff_factor_array['personal_offers_table']]['last_modified'] > 0 ? date('d.m.Y H.i:s', strtotime($last_persoff_factor_arr[$last_persoff_factor_array['personal_offers_table']]['last_modified'])) : '';
    $last_persoff_factor_arr[$last_persoff_factor_array['personal_offers_table']]['revoce_fact'] = round(100/($last_persoff_factor_array['factor'] * 100), 4);
}

//we do this via ajax
if((isset($_POST['grp']) && is_numeric($_POST['grp'])) && isset($_POST['fact_val'])) {
    //$out = '<pre>'.print_r($_POST, true).'</pre>';

    $out = '';

    if($_POST['fact_val'] > 0) {
        $rnd_fact_val = round((float)str_replace(',', '.', $_POST['fact_val']), 4);
        $upd_qu_st = "UPDATE personal_offers_by_customers_status_".(int)$_POST['grp']." SET personal_offer = personal_offer * ".$rnd_fact_val;
        $upd_fact_qu_str = "UPDATE nr_pers_off_factor SET factor = ".$rnd_fact_val.", last_modified = NOW() WHERE personal_offers_table = ".(int)$_POST['grp'];
        if(xtc_db_query($upd_qu_st) && xtc_db_query($upd_fact_qu_str)) {
            $out .= '<span style="color:#0A7E00;">Preise in personal_offers_by_customers_status_'.$_POST['grp'].' wurden mit Faktor '.$rnd_fact_val.' multipliziert</span><br />';
        } else {
            $out .= '<span style="color:#c00;">Faktor für personal_offers_by_customers_status_'.$_POST['grp'].' hat abgebrochen.</span><br />';
        }
    } else {
        $out .= '<span style="color:#c00;">Faktor für personal_offers_by_customers_status_'.$_POST['grp'].' wurde nicht gesetzt.</span><br />';
    }

    if(isset($_POST['is_last']) && $_POST['is_last'] == 'true') {
        if(copy_tables_to_euql()) {
            $out .= '<br /><span style="color:#0A7E00;">Ebenfalls alle erforderlichen Tabellen kopiert.</span><pre style="color:#0A7E00;"><hr />READY !</pre>';
        }  //execute copying of tables to equal tables

        if(defined('DB_CACHE') && DB_CACHE == 'true') {
            clear_dir(SQL_CACHEDIR);
        }
    }

    echo $out;
    exit();
}
//EOC change all prices with factor, 02-2021, noRiddle

//BOC copy prices with factor
if(isset($_GET['action']) && $_GET['action'] == 'copy_persoff_price_factor') {
    if(isset($_POST['copy_persoff_from_with_factor']) && isset($_POST['copy_persoff_to_with_factor']) && isset($_POST['copy_persoff_factor'])) {
        if($_POST['copy_persoff_from_with_factor'] != '' && $_POST['copy_persoff_to_with_factor'] != '') {
            $_SESSION[$_SESSION['ma_shopsess'].'copy_with_factor_out'] = '';
            $post_cpf = (int)$_POST['copy_persoff_from_with_factor'];
            $post_cptwf = (int)$_POST['copy_persoff_to_with_factor'];

            if($_POST['copy_persoff_factor'] == '') {
                $post_cpfact = 1;
            } else {
                $post_cpfact = (float)str_replace(',', '.', $_POST['copy_persoff_factor']);
                $post_cpfact = round($post_cpfact, 4); //new, prepare for db field which allows only 4 digits after comma
            }

            if($post_cpf == $post_cptwf) {
                $post_with_fact_qu1 = "UPDATE personal_offers_by_customers_status_".$post_cptwf. " SET personal_offer = personal_offer * ".$post_cpfact;

                if(xtc_db_query($post_with_fact_qu1)) {
                    $_SESSION[$_SESSION['ma_shopsess'].'copy_with_factor_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$post_cpf.' wurden mit Faktor '.$post_cpfact.' multipliziert';
                }
            } else {
                /*$post_with_fact_qu = "UPDATE personal_offers_by_customers_status_".$post_cptwf." po".$post_cptwf." 
                                  INNER JOIN personal_offers_by_customers_status_".$post_cpf." po".$post_cpf." ON po".$post_cpf.".products_id = po".$post_cptwf.".products_id AND po".$post_cpf.".quantity = po".$post_cptwf.".quantity
                                        SET po".$post_cptwf.".personal_offer = po".$post_cpf.".personal_offer * ".$post_cpfact;*/
                //echo '<pre>'.$post_with_fact_qu.'</pre>';
                
                $post_with_fact_qu1 = "TRUNCATE TABLE personal_offers_by_customers_status_".$post_cptwf;
                //$post_with_fact_qu2 = "INSERT INTO personal_offers_by_customers_status_".$post_cptwf." SELECT * FROM personal_offers_by_customers_status_".$post_cpf;
                //$post_with_fact_qu3 = "UPDATE personal_offers_by_customers_status_".$post_cptwf." SET personal_offer = personal_offer * ".$post_cpfact;
                $post_with_fact_qu2 = "INSERT INTO personal_offers_by_customers_status_".$post_cptwf." (price_id, products_id, quantity, personal_offer) SELECT price_id, products_id, quantity, personal_offer * ".$post_cpfact." FROM personal_offers_by_customers_status_".$post_cpf;
                
                //if(xtc_db_query($post_with_fact_qu)) {
                //if(xtc_db_query($post_with_fact_qu1) && xtc_db_query($post_with_fact_qu2) && xtc_db_query($post_with_fact_qu3)) {
                if(xtc_db_query($post_with_fact_qu1) && xtc_db_query($post_with_fact_qu2)) {
                    $_SESSION[$_SESSION['ma_shopsess'].'copy_with_factor_out'] .= '<span style="color:#0A7E00;">Preise aus personal_offers_by_customers_status_'.$post_cpf.' wurden in Kundenstatus '.$post_cptwf.' mit Faktor '.$post_cpfact.' kopiert</span>';
                }
            }

            //delete DB cache if activated
            if(defined('DB_CACHE') && DB_CACHE == 'true') {
                clear_dir(SQL_CACHEDIR);
            }
        } else {
            $_SESSION[$_SESSION['ma_shopsess'].'copy_with_factor_out'] .= '<span style="color:#c00;">Es fehlen Angaben, bitte überprüfen</span>';
        }
        
        xtc_redirect(xtc_href_link(FILENAME_MASTER_ADMIN, 'script=copy_group_prices'));
    }
}
//EOC copy prices with factor

//BOC copy at one's choice
foreach($ie_personal_offers_array as $po) {
    $poj_arr = explode('_', $po);
    $j = end($poj_arr);
    //echo $j.'<br />';
    
    //BOC copy
    if(isset($_GET['action']) && $_GET['action'] == 'copy_persoff_single') {
        if(isset($_POST['get_pers_off_'.$j]) && isset($_POST['set_pers_off_'.$j]) && $_POST['set_pers_off_'.$j] != '') {
            $_SESSION[$_SESSION['ma_shopsess'].'persoff_single_out'] = '';
            $post_get_persoff = $_POST['get_pers_off_'.$j];
            $post_set_persoff = $_POST['set_pers_off_'.$j];
            $all_where_to_copy = explode(',', $post_set_persoff);
            //echo $post_get_persoff.' copy to '.$post_set_persoff;
            
            foreach($all_where_to_copy as $pers_no) {
                if($post_get_persoff == $pers_no) {
                    $_SESSION[$_SESSION['ma_shopsess'].'persoff_single_out'] .= '<span style="color:#c00;">In dieselbe Tabelle wie die Ausgangstabelle kopieren ist nicht m&ouml;glich !</span>';
                } else {
                    xtc_db_query("TRUNCATE TABLE personal_offers_by_customers_status_".$pers_no);
                    
                    if(xtc_db_query("INSERT INTO personal_offers_by_customers_status_".$pers_no." SELECT * FROM personal_offers_by_customers_status_".$post_get_persoff)) {
                        $_SESSION[$_SESSION['ma_shopsess'].'persoff_single_out'] .= '<span style="color:#0A7E00;">'.sprintf(ADMIN_TOOLS_COPY_PERSOFF_SUCCESS_TXT, $post_get_persoff, $pers_no).'</span>';
                    }
                }
            }
            
            //delete DB cache if activated
            if(defined('DB_CACHE') && DB_CACHE == 'true') {
                clear_dir(SQL_CACHEDIR);
            }
            
            xtc_redirect(xtc_href_link(FILENAME_MASTER_ADMIN, 'script=copy_group_prices'));
        }
    }
    //EOC copy
}
//EOC copy at one's choice
?>
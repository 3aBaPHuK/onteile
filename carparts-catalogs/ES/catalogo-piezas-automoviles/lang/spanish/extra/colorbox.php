<?php
 //additional fields
define('TEXT_COLORBOX_CURRENT', 'Imagen {current} de {total}');
define('TEXT_COLORBOX_PREVIOUS', 'Atrás');
define('TEXT_COLORBOX_NEXT', 'Adelante');
define('TEXT_COLORBOX_CLOSE', 'Cerrar');
define('TEXT_COLORBOX_XHRERROR', 'Este contenido no se ha podido cargar.');
define('TEXT_COLORBOX_IMGERROR', 'Esta imagen no se ha podido cargar.');
define('TEXT_COLORBOX_SLIDESHOWSTART', 'Iniciar Slideshow');
define('TEXT_COLORBOX_SLIDESHOWSTOP', 'Detener Slideshow');
?>

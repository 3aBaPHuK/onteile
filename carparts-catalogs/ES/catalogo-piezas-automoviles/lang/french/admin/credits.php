<?php
/* --------------------------------------------------------------
   $Id: credits.php 3072 2012-06-18 15:01:13Z hhacker $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(customers.php,v 1.13 2002/06/15); www.oscommerce.com
   (c) 2003 nextcommerce (customers.php,v 1.8 2003/08/15); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Crédits');
define('HEADING_SUBTITLE', 'Remerciements');
define('TEXT_DB_VERSION', 'Version base de données:');
define('TEXT_HEADING_GPL', 'Publié sous GNU General Public License (Version 2)');

define('TEXT_INFO_GPL', 'Ce programme est distribué dans l\'espoir qu\'il sera utile, mais <strong> SANS AUCUN GARANTIE</strong><br /> - même sans garantie implicite de la <strong> maturité du marché</strong>ou de l\'<strong>adéquation à un objet particulier</strong>.<br /> Vous trouvez des détails dans GNU General Public License.<br /><br /> Avec ce programme vous avez reçu une copie du GNU General Public License.<br />Si ce n’est pas le cas, écrivez à  Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.<br />Détail (en anglais) ici: <a style="font-size: 12px; text-decoration: underline;" href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">http://www.gnu.org/licenses/gpl-2.0.html</a>.');

define('TEXT_HEADING_DEVELOPERS', 'Développeur de la modified eCommerce Shopsoftware:');
define('TEXT_HEADING_SUPPORT', 'Soutenez le développement:');
define('TEXT_HEADING_DONATIONS', 'Donner:');
define('TEXT_HEADING_BASED_ON', 'Le shopsoftware se base sur:');

define('TEXT_INFO_THANKS', 'Nous remercions tous les développeurs qui ont participé à ce projet. Si nous avons oublié quelqu\'un dans le listage ci-dessous, contactez nous via le <a style="font-size: 12px; text-decoration: underline;" href="http://www.modified-shop.org/forum/" target="_blank">forum</a> ou contactez un des développeurs nommés.');
define('TEXT_INFO_DISCLAIMER', 'Ce programme est distribué dans l\'espoir qu\'il sera utile. Sans aucun garantie à une implémentation parfaite.');
define('TEXT_INFO_DONATIONS', 'La modified eCommerce Shopsoftware est un projet OpenSource - nous investissions beaucoup de travail et de temps libre dans ce projet. Nous somme reconnaissantes pour tout don comme petite réconnaissance.');
define('TEXT_INFO_DONATIONS_IMG_ALT', 'Soutenez le projet avec votre don');
define('BUTTON_DONATE', '<a href="http://www.modified-shop.org/spenden"><img src="https://www.paypal.com/de_DE/DE/i/btn/btn_donateCC_LG.gif" alt="' . TEXT_INFO_DONATIONS_IMG_ALT . '" border="0"></a>');
?>

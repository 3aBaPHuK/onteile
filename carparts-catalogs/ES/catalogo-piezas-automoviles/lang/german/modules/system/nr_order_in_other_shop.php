<?php
/************************************************************
* file: nr_order_in_other_shop.php
* use: language file for system file
* (c) noRiddle 04-2018
************************************************************/

define('MODULE_NR_ORDER_IN_OTHER_SHOP_TITLE', 'Bestellung in anderem Shop');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_DESCRIPTION', 'Modul dient dazu Ware im Warenkorb in einem anderen Shop zu bestellen (z.B. im Falle eines Logistik-Engpasses).');

define('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS_TITLE', 'Status');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS_DESC', 'Modul aktivieren ?');

define('MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP_TITLE', 'In welchem Shop soll die Bestellung ausgeführt werden ?');
define('MODULE_NR_ORDER_IN_OTHER_SHOP_WHICH_SHOP_DESC', 'Bitte hier in Kleinbuchstaben lediglich den Shopnamen ohne "-ersatzteile" oder "-spare-parts" eingeben.<br />Beispiele: skoda, bmw, bmw-motorrad, chrysler-dodge-jeep-ram');

$nr_oios_lngs = xtc_get_languages();
for($oios = 0, $coios = count($nr_oios_lngs); $oios < $coios; $oios++) {
    define('MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_'.strtoupper($nr_oios_lngs[$oios]['code']).'_TITLE', xtc_image(DIR_WS_LANGUAGES . $nr_oios_lngs[$oios]['directory'] .'/admin/images/'. $nr_oios_lngs[$oios]['image'], $nr_oios_lngs[$oios]['name']).' Hinweistext über "Zur Kasse"-Button '.strtoupper($nr_oios_lngs[$oios]['code']));
    define('MODULE_NR_ORDER_IN_OTHER_SHOP_TXT_'.strtoupper($nr_oios_lngs[$oios]['code']).'_DESC', 'Hier den Text (Begründung) eingeben warum die Bestellung in einem anderen Shop vollendet werden soll.<br />Als Platzhalter für den oben eingetragenen Shop bitte <i>%s</i> benutzen.<br /><span style="display:inline-block; font-size:11px; line-height:12px;">(Mittels der Backup-Funktion kann jeder Text gesichert werden.<br />Bei Deinstallation, anschließender Installation und nicht Wiederherstellung des Backups wird der Standart-Text wieder hier erscheinen.)</span>');
}
?>
<?php

require_once(__DIR__.'/guayaquillib/render/template.php');

abstract class LanguageTemplate implements IGuayaquilExtender {
    public static $language_data = array(
        'catalog title' => 'Catalogue title',
        'catalog date' => 'Catalogue date',
        'searchbyvin' => 'Search a car by VIN', //new parameter, noRiddle
        'vehicsearchbyvin' => 'Search a different car by VIN', //new parameter, noRiddle
        'searchbycustom' => 'Search a car by',
        'inputvin' => 'Enter VIN (17 symbols), for example: %s',
        'search' => 'Search',
        'search_placeholder' => 'enter VIN', //new parameter, noRiddle
        'searchbyframe' => 'Search a car by frame',
        'inputframe' => 'Enter code and number of frame, for example %s:',
        'searchdetail' => 'Search for detail',
        'searchbyoem' => 'Search by part no.:',
        'search by wizard' => 'Identification of car by parameters',
        'search by wizard sub' => '(It is sufficient to choose the data you know.)',
        'findfailedvin' => 'The searched VIN %s was unfortunately not found in our database.<br />(No credit has been substracted.)<br />Please try with <a href="%s">"Identification of car by parameters" (if available)</a>.<br />Please verify whether you chose the right brand (e.g. with Fiat there is also "Fiat Professional" for utility vehicles).<br />If you don\'t know how to proceed, write us an e-mail (Contact button top right).',
        'findfailed' => 'Search by %s %s was not successful',
        'cars' => 'Cars',
        'columnvehiclename' => 'Name',
        'columnvehiclemodification' => 'Modification',
        'columnvehiclegrade' => 'Grade',
        'columnvehicleframe' => 'Frame',
        'columnvehicledate' => 'Date',
        'columnvehicleengine' => 'Engine',
        'columnvehicleengineno' => 'Engine No',
        'columnvehicletransmission' => 'Trans.',
        'columnvehicledoors' => 'Doors',
        'columnvehiclemanufactured' => 'Manufactured',
        'columnvehicleoptions' => 'Options',
        'columnvehiclecreationregion' => 'Region',
        'columnvehicleframes' => 'Frames',
        'columnvehicleframecolor' => 'Frame color',
        'columnvehicletrimcolor' => 'Trim color',
        'columnvehiclemodel' => 'Model',
        'columnvehicledatefrom' => 'Manufactured from',
        'columnvehicledateto' => 'Manufactured till',
        'columnvehicledestinationregion' => 'For region',
        'columnvehiclebrand' => 'Brand',
        'categories' => 'Categories',
        //'carname' => 'Car: %s',
        'carname' => '%s', //noRiddle
        'selecteddetail' => 'Selected part %s',
        'unitname' => 'Unit: %s',
        'columndetailcodeonimage' => 'no. in pic',
        'columndetailchkb' => 'Check', //new element for checkbox, noRiddle
        'columndetailqty' => 'qty.', //new element for qty, noRiddle
        'columndetailname' => 'Name',
        'columndetailoem' => 'Part no.',
        'columndetailamount' => 'Amount',
        'columndetailnote' => 'Note',
        'columndetailprice' => 'Price',
        'addtocarthint' => 'Add to cart',
        'price_question_txt' => 'ask fo the price', //new element for price question text, noRiddle
        'togglereplacements' => 'Show/hide replacements',
        'replacementway' => 'Replacement type',
        'replacementwayfull' => 'Full duplicate',
        'replacementwayforward' => 'Replacement with shown duplicate is possible but reverse replacement is not guaranteed',
        'replacementwaybackward' => 'Replacement is NOT possible',
        'wheretobuy' => 'Where to buy',
        'searchbyoemresult' => 'Search results by OEM %s',
        'vehiclelink' => 'Change to search by pictures',
        'groupdetails' => 'Search by main- and sub-groups',
        'imagedetails' => 'Search by categories and pictures', //new, noRiddle
        'quickgroupslink' => 'Change to search by groups',
        'list vehicles' => 'List vehicles',
        'unit_legend' => 'Legend',
        'unit_legend_mouse_wheel' => 'mouse wheel',
        'unit_legend_image_resizing' => 'resize image with mouse wheel',
        'unit_legend_show_replacement_parts' => 'Show spare parts duplicates', //'показ дубликатов запчастей',
        'unit_legend_mouse_image_drag' => 'Mouse image drag',
        'unit_legend_mouse_scroll_image' => 'scroll image with left mouse button',
        'unit_legend_add_to_cart' => 'Add part to cart',
        'unit_legend_hover_parts' => 'Hover mouse on the part',
        'unit_legend_highlight_parts' => 'highlight parts on picture or in table<br />(will be visible in picture and table)',
        'unit_legend_show_hind' => 'Show detailed information about part',
        'otherunitparts' => 'Show all unit parts of this group',
        'enter_group_name' => 'Enter group name or parts search term',
        'reset_group_name' => 'Reset',
        'applicabilitybrandselectortitle' => 'Select option to find applicability',
        'applicability' => '(Show applicability)',
        //BOC new for translation in /guayaquillib/render/qdetails/default.php, noRiddle
        'nothing_found_see_catalogue' => 'Nothing found, please make use of the <a href="%s">isllustrated catalogue</a> for a detailed search',
        //EOC new for translation in /guayaquillib/render/qdetails/default.php, noRiddle
        //BOC new lang params, noRiddle
        'nr_brand_choice' => 'brand choice',
        'nr_shopp_cart' => 'to shopping cart',
        'nr_into_shopp_cart' => '',
        'nr_into_shopp_cart_t' => '',
        'nr_not_available' => 'not available',
        'nr_prod_replaced' => 'has been replaced',
        'filter_choose_hint' => 'Please choose the applicable from the list:',
        'filter_choose_please' => 'please choose',
        'filter_ignore_selection' => 'ignore filter option',
        'filter_submit' => 'send',
        'nr_searchparts' => 'Search parts for the indicated vehicle',
        'products_in_cart_after_login' => 'Please check your shopping cart. It still contains products from your last visit.',
        'explain_group_search' => 'Search with help of a search term or click on the group names below to specify your search.',
        'nr_diff_from_vehicle_above' => 'divergent from vehicle above this vehicle',
        /*'txt_not_looged_in' => 'Dear customer, as part of our contract with the supplier of the spare parts catalog software we are committed to offer the catalog only to our (potential) customers and not for public use. <ul><li>We therefore ask you to register. If you\'ve got an account in one of our genuine parts shops, you can login with this account in this catalogue also.</li><li>With this registration you can shop parts of any brand on www.online-teile.com. Of course, we dont share informations about you, for details pls see our <a class="iframe" href="%s">Privacy Policy</a>.</li><li>If you run a workshop <strong>workshop</strong> or you are a member of a registered <strong>brand club</strong> or you need a <strong>VAT-free</strong> invoice within the European Union, please look <a class="iframe" href="%s"><strong>here</strong></a> for more information.</li><li>If you are a member of the <strong>ADAC</strong>, you can get the discounts you see on <a class="iframe" title="ADAC-Information" href="https://www.online-teile.com/ADAC/en" target="_blank" rel="nofollow">https://www.online-teile.com/ADAC/</a>. The membership number is required before you send your order.</li></ul>',*/
        /*'txt_not_looged_in' => 'Dear customer, we are glad that you are here and want to use our free Spare Parts Catalog with more than 40 brands ! We would be glad if you buy your parts then in the appending brand shops ! We promise good prices and quick delivery. And of course we only sell genuine brand parts with highest quality level !<ul><li>Please <a href="//www.online-teile.com/oem-kataloge/login.php" class="link-blue">register</a>. If you already have an <a href="//www.online-teile.com/oem-kataloge/login.php" class="link-blue">account</a> in one of our shops, you can <a href="//www.online-teile.com/oem-kataloge/login.php" class="link-blue">login</a> with this account into this catalog also.</li><li>With this account you can use not only the catalog but also all shops on <a href="//www.online-teile.com/en" target="_blank" class="link-blue">www.online-teile.com</a>. Your data is safe, pls see our <a class="iframe link-blue" href="%s">Privacy Policy</a>.</li><li>If you run a <strong>workshop</strong> or you are a member of a <strong>registered brand club</strong> or you need a <strong>VAT-free invoice</strong> within the European Union, please look <a class="iframe link-blue" href="%s">here</a> for more information.</li><li>If you are a member of the <strong>ADAC</strong>, you can get the discounts you see on <a class="iframe link-blue" title="ADAC-Information" href="https://www.online-teile.com/ADAC/en" target="_blank" rel="nofollow">https://www.online-teile.com/ADAC/</a>. The membership number is required before you send your order.</li>
</ul>',*/
        'txt_see_repl_part' => 'see next position',
        'name_not_known' => 'Name not known',
        'back_to_group_link' => 'Back to the group selection',
        //BOC new lang params for es_mx for wizard, noRiddle
        'catalog no' => 'catalogue no.', //beside the list generated by Andreas' cooperator, added by me, noRiddle
        'a/t-get.,schaltget' => 'gear type',
        'aggregatart' => 'aggregate type',
        'antriebsstrang' => 'wheel drive',
        'anzahl der sitze' => 'number of seats',
        'anzahl der turten' => 'number of doors',
        'area' => 'area',
        'baubedingungen' => 'body variant',
        'baumuster' => 'type',
        'bed type' => 'chassis',
        'body' => 'body',
        'body type' => 'body',
        'bodywork type' => 'body',
        'cab' => 'cabin',
        'catalog' => 'catalogue',
        'catalogue' => 'type',
        'chassis' => 'chassis',
        'classification' => 'model',
        'dach' => 'roof',
        'deck,kabinen-' => 'cabin',
        'displacement' => 'engine capacity',
        'door' => 'number of doors',
        'doors' => 'number of doors',
        'drive' => 'L/ R hand drive',
        'eng spec' => 'emission standard',
        'engine' => 'engine',
        'engine type' => 'engine type',
        'engine type and suffix' => 'engine type and suffix',
        'fahrerhaus' => 'cabin',
        'fahrersitz' => 'L/ R hand drive',
        'family' => 'family',
        'frame' => 'frame',
        'fuel' => 'fuel',
        'gearbox type' => 'gearbox type',
        'getriebe' => 'gearbox type',
        'getriebetyp' => 'gearbox type',
        'grade' => 'grade',
        'hinterreifen' => 'rear wheels',
        'intake' => 'intake',
        'jahr' => 'year',
        'karosse' => 'body',
        'karosserie' => 'body',
        'katalog Nr.' => 'catalogue number',
        'klasse' => 'category',
        'kraftst.einspritzung' => 'fuel injection',
        'kraftstoff' => 'fuel',
        'ladeflache' => 'load area',
        'm/ getriebeschaltung' => 'gearbox type',
        'market' => 'market',
        'model' => 'model',
        'model code' => 'model code',
        'model name' => 'model name',
        'model year' => 'model year',
        'modell-Name' => 'model',
        'motor' => 'motor',
        'motor 1' => 'motor',
        'motorleistung' => 'engine power',
        'nutzlast' => 'load area',
        'power' => 'power',
        'production year' => 'production year',
        'region' => 'region',
        'restriction' => 'restriction',
        'roof' => 'roof',
        'seat type' => 'seating arrangement',
        'series' => 'series',
        'sortimentsklasse' => 'vehicle type',
        'special car' => 'grade',
        'special version' => 'gear type',
        'steering' => 'L/ R hand drive',
        'sub model' => 'sub model',
        'suspension' => 'suspension',
        'suspension type' => 'suspension type',
        'transmission' => 'gearbox type',
        'trim level' => 'trim level',
        'type' => 'type',
        'variant' => 'variant',
        'vehicle category' => 'vehicle category',
        'vehicle family' => 'type',
        'vehicle line' => 'type',
        'vehicle name' => 'type',
        'vehicle type' => 'type',
        'vehicle/engine type' => 'vehicle/engine type',
        'verkaufsbezeichnung' => 'model',
        'verkaufsgebiet' => 'sales territory',
        'version' => 'version',
        'wheel base' => 'wheel base',
        'wheel drive' => 'wheel drive',
        'year' => 'year'
        //EOC new lang params for es_mx for wizard, noRiddle
        //EOC new lang params, noRiddle
    );
}

//fill array values where php is used, noRiddle
LanguageTemplate::$language_data['nr_into_shopp_cart'] = '<span class="no-mob">Add checked parts '.(isset($_GET['coID']) && $_GET['coID'] == '1007' ? 'of this group' : '').' to the shopping cart</span><span class="y-mob"><i class="fa fa-check-square"></i></span>';
LanguageTemplate::$language_data['nr_into_shopp_cart_t'] = 'Add checked parts '.(isset($_GET['coID']) && $_GET['coID'] == '1007' ? 'of this group' : '').' to the shopping cart';
<?php
/* -----------------------------------------------------------------------------------------
   $Id: general_bottom.css.php 56 2016-04-28 11:45:58Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2006 XT-Commerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

   // This CSS file get includes at the BOTTOM of every template page in shop
   // you can add your template specific css scripts here

  $css_array = array(
    DIR_TMPL_CSS.'jquery.colorbox.css',
    //DIR_TMPL_CSS.'jquery.sumoselect.css', //have this loaded on top in main css, noRiddle
    DIR_TMPL_CSS.'jquery.alerts.css',
    DIR_TMPL_CSS.'jquery.bxslider.css',
    DIR_TMPL_CSS.'jquery.sidebar.css',
    DIR_TMPL_CSS.'font-awesome.css',
    //'nr_catalogue_files/guayaquillib/render/guayaquil.css', //have this in top section, noRiddle
    DIR_TMPL_CSS.'jquery.cookieconsent.css',
    DIR_TMPL_CSS.'customers_notice.css', //added, 10-2020, noRiddle
    DIR_TMPL.'kb-plugin/css/settings.css',
    DIR_TMPL_CSS.'media-queries.css', // must be last entry
  );
  $css_min = DIR_TMPL_CSS.'tpl_plugins.min.css';
  
  $this_f_time = filemtime(DIR_FS_CATALOG.DIR_TMPL_CSS.'general_bottom.css.php');

  if (COMPRESS_STYLESHEET == 'true') {
    require_once(DIR_FS_BOXES_INC.'combine_files.inc.php');
    $css_array = combine_files($css_array,$css_min,true,$this_f_time);
  }
  
  foreach ($css_array as $css) {
    $css .= strpos($css,$css_min) === false ? '?v=' . filemtime(DIR_FS_CATALOG.$css) : '';
    echo '<link rel="stylesheet" property="stylesheet" href="'.DIR_WS_BASE.$css.'" type="text/css" media="screen" />'.PHP_EOL;
  }
?>
<!--[if lte IE 8]>
<link rel="stylesheet" property="stylesheet" href="<?php echo DIR_WS_BASE.DIR_TMPL_CSS; ?>ie8fix.css" type="text/css" media="screen" />
<![endif]-->
<?php
//BOC cokebumoslider, noRiddle
//if (basename($PHP_SELF) == FILENAME_DEFAULT && !isset($_GET['cPath']) && !isset($_GET['manufacturers_id'])) {

//<link rel="stylesheet" href="<?php echo DIR_WS_BASE.'templates/'.CURRENT_TEMPLATE; /kb-plugin/css/settings.css" type="text/css" media="screen" />

//}
//EOC cokebumoslider, noRiddle
?>
<?php //BOC include guayaquil.css, noRiddle
//<link rel="stylesheet" href="nr_catalogue_files/guayaquillib/render/guayaquil.css" type="text/css" /> //have hat in compressed file (see above), noRiddle
 ?>
<?php if(isset($_GET['coID']) && $_GET['coID'] == '1003') { ?>
<link rel="stylesheet" href="nr_catalogue_files/guayaquillib/render/qgroups/groups.css" type="text/css" />
<?php } ?>
<?php //if(isset($_GET['coID']) && $_GET['coID'] == '1006') { 
//<link rel="stylesheet" href="nr_catalogue_files/guayaquillib/render/unit/unit.css" type="text/css" /> //integrated this in guayaquil.css
//} ?>
<?php //EOC include guayaquil.css, noRiddle ?>
<?php  //BOC different colorbox css for catalog images, noRiddle
if(isset($_GET['coID']) && ($_GET['coID'] == '1004' || $_GET['coID'] == '1006' || $_GET['coID'] == '1007')) {
?>
<style>
#cboxTitle {
width:100%;
bottom:42px;
padding:0;
}
</style>
<?php
}
//EOC different colorbox css for catalog images, noRiddle
?>
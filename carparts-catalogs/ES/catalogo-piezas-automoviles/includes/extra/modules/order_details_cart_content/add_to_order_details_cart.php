<?php
/***********************************************
* file: add_to_order_details_cart.php
* use: add new product fields to shopping cart
* (c) noRiddle 05-2017
***********************************************/

if($products[$i]['bulk'] > 0 && MODULE_ORDER_TOTAL_BULKGOODS_METHOD != '3') {
    $nr_bulk_factor = (constant('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$_SESSION['customers_status']['customers_status']) > 0) ? ((float)constant('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$_SESSION['customers_status']['customers_status'])) : 1;
    $nr_bulk_qty = (MODULE_ORDER_TOTAL_BULKGOODS_METHOD == '1' ? $products[$i]['quantity'] : 1);
    
    $nr_tot_bulk = $products[$i]['bulk'] * $nr_bulk_factor * $nr_bulk_qty;
    
    $module_content[$i]['PRODUCTS_BULK'] = $xtPrice->xtcFormat($nr_tot_bulk, true, $products[$i]['tax_class_id'], true);
}

//echo '<br /><br /><pre>'.print_r($products, true).'<pre>';

//BOC build cart link for order in other shop
if(defined('MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS')) {
    if(MODULE_NR_ORDER_IN_OTHER_SHOP_STATUS == 'true') {
        $redirect_model_qty_arr[$products[$i]['model']] = $products[$i]['quantity'];
    }
}
//EOC build cart link for order in other shop
?>
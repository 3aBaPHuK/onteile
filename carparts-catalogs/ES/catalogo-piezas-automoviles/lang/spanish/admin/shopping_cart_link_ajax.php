<?php
/*********************************************************************
* file: shopping_cart_link_ajax.php
* path: /lang/spanish/admin/
* use: language file for module
* 
* (c) copyright 10-2020, noRiddle
*********************************************************************/

define('GSCL_PROD_EXISTS_TXT', '<br /><span style="color:green;">OK, número de artículo encontrado | Nombre del artículo: %s | Precio de lista incl. IVA: %s | ID de producto: %s</span>');
define('GSCL_NOT_PROD_STAT1_TXT', '<br /><span style="color:red;">! Artículo no tiene products_status 1 (= artículo esta deactivado) ! Verifiquelo en la tienda por favor.</span>');
define('GSCL_STAT1_TXT', '(= Consulta de precio)');
define('GSCL_STAT2_TXT', '(= no más disponible)');
define('GSCL_NOT_GMSTAT0_TXT', '<br /><span style="color:red;">! Artículo tiene gm_price_status %s %s ! Verifiquelo en la tienda por favor.</span>');
define('GSCL_MODEL_EMPTY_OR_NOTINSHOP_TXT', '<br /><span style="color:red;">! Número de artículo vacío o no encontrado en la tienda !</span>');
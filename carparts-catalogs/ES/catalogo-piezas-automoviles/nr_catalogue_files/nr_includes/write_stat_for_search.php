<?php
/*************************************************
* file: write_stat_for_search.php
* use: write statistic values of searches by clients in database
*
* (c) noRiddle 02-2018

CREATE TABLE IF NOT EXISTS stat_catalog (
    stat_id INT NOT NULL AUTO_INCREMENT,
    cust_id INT NOT NULL,
    cust_ip VARCHAR(50) NOT NULL DEFAULT '',
    date_added DATETIME DEFAULT '0000-00-00 00:00:00',
    search_c VARCHAR(16) NOT NULL,
    brand VARCHAR(64) NOT NULL,
    vin_or_wiz VARCHAR(16) NOT NULL,
    search_vin VARCHAR(17) NOT NULL,
    found_vin CHAR(1) NOT NULL,
    PRIMARY KEY (stat_id),
    KEY idx_cust_id (cust_id),
    KEY idx_date_added (date_added),
);

ALTER TABLE stat_catalog ADD found_vin CHAR(1) NOT NULL AFTER search_vin;

-- need this ??  
    search_ssd VARCHAR(64) NOT NULL,
    hits_ssd INT NOT NULL,
    search_ft VARCHAR(32) NOT NULL,
    hits_ft INT NOT NULL,
    search_vid VARCHAR(32) NOT NULL,
    hits_vid INT NOT NULL,
    search_gid VARCHAR(16) NOT NULL,
    hits_gid INT NOT NULL,
    search_cid VARCHAR(16) NOT NULL,
    hits_cid INT NOT NULL,
    
);

// correct database after new commercial vehicles where added by Laximo

UPDATE stat_catalog SET brand = 'MERCEDES-BENZ COMMERCIAL' WHERE search_c = 'MBC201711';
UPDATE stat_catalog SET brand = 'KIA COMMERCIAL' WHERE search_c = 'KIACV201803';
UPDATE stat_catalog SET brand = 'HYUNDAI COMMERCIAL' WHERE search_c = 'HYWCV201803';
UPDATE stat_catalog SET brand = 'ISUZU COMMERCIAL' WHERE search_c = 'ISUZUCV201701';
UPDATE stat_catalog SET brand = 'VOLVO TRUCKS' WHERE search_c = 'VCV201606';

*************************************************/

if(isset($_SESSION['customer_id'])) {
    if(isset($_GET['ft']) && $_GET['ft'] != '') {
        $stat_get_c = preg_replace('#[^0-9A-Z _\-]#i', '', $_GET['c']); //e.g. CITROEN00
        //echo '<br /><br /><br /><pre>'.$stat_get_c.'</pre>';
        $stat_brand_qu_str = "SELECT lax_brand, lax_name FROM laximo_data_helper WHERE c_param = '".xtc_db_input($stat_get_c)."'";
        $stat_brand_qu = xtc_db_query($stat_brand_qu_str);
        if(xtc_db_num_rows($stat_brand_qu)) {
            $stat_brand_arr = xtc_db_fetch_array($stat_brand_qu);
        } else {
            //$stat_brand_arr['lax_brand'] = $vehicles->row['brand'];
            $stat_brand_arr['lax_name'] = $vehicles->row['name'];
        }

        $stat_search_vin = '';
        $stat_vin_found = '';
        if($_GET['ft'] == 'findByVIN') {
            $stat_search_vin = strtoupper($_GET['vin']);
            $stat_search_vin = preg_replace('#[^0-9a-z]#i', '', $stat_search_vin);
            $stat_vin_found = (is_object($vehicles) == false || $vehicles->row->getName() == '') ? '0' : '1';
        }

        $stat_cl_ip = xtc_get_ip_address();

        $ins_stat_cat_sql = array('cust_id' => (int)$_SESSION['customer_id'],
                                  'cust_ip' => $stat_cl_ip,
                                  'date_added' => 'now()',
                                  'search_c' => $_GET['c'],
                                  //'brand' => $stat_brand_arr['lax_brand'],
                                  'brand' => strtoupper($stat_brand_arr['lax_name']), //change to name since new commercial vehicles have same brand (e.g. brand Kia => name Kia or Kia commercial)
                                  'vin_or_wiz' => $_GET['ft'],
                                  'search_vin' => $stat_search_vin,
                                  'found_vin' => $stat_vin_found
                                 );

        xtc_db_perform('stat_catalog', $ins_stat_cat_sql);
    }
}
?>
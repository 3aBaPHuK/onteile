<?php
define('BOX_ORDER_MAIL_ADVERTISE', 'Publicité dans la confirmation de commande');
define('TEXT_DISPLAY_NUMBER_OF_ORDER_MAIL_ADVERTISE', 'Affichés sont les textes de publicité <b>%d</b> à <b>%d</b> (d\'un total de <b>%d</b> textes de publicité)');

define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_ID', 'ID');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_TITLE', 'Titre de publicité');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_TEXT', 'Texte de publicité');
define('TABLE_HEADING_ORDER_MAIL_ADVERTISE_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Veuillez faire tous les changements nécessaires.');
define('TEXT_INFO_INSERT_ORDER_MAIL_ADVERTISE_INTRO', 'Veuillez insérer un nouveau texte de publicité');
define('TEXT_INFO_DELETE_ORDER_MAIL_ADVERTISE_INTRO', 'Êtes-vous sûr de vouloir supprimer ce texte de publicité?');
define('TEXT_INFO_HEADING_NEW_ORDER_MAIL_ADVERTISE', 'Nouveau texte de publicité');
define('TEXT_INFO_HEADING_EDIT_ORDER_MAIL_ADVERTISE', 'Editer le texte de publicité');
define('TEXT_INFO_HEADING_DELETE_ORDER_MAIL_ADVERTISE', 'Supprimer le texte de publicité');
?>

<?php

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'template.php';

class GuayaquilFilter extends GuayaquilTemplate
{
    var $catalog;
    var $ssd;
    var $unit;
    var $isEmpty = false;

	function Draw($catalog, $filters, $ssd, $unit)
	{
		$this->catalog = $catalog;
		$this->ssd = $ssd;
        $this->unit = $unit;
		$html = $this->DrawJS($filters);
		$html .= $this->DrawHeader($filters);
		$html .= $this->DrawBody($filters);
		$html .= $this->DrawFooter($filters);

		//BOC don't need link to laximo, noRiddle
        //$html .= '<div style="visibility: visible; display: block; height: 20px; text-align: right;"><a href="http://dev.laximo.ru" rel="follow" style="visibility: visible; display: inline; font-size: 10px; font-weight: normal; text-decoration: none;">guayaquil</a></div>';
        $html .= '<div style="display:block; height:20px; text-align:right;"></div>';
        //EOC don't need link to laximo, noRiddle

		return $html;
	}

    private function DrawJS($filters)
    {
        return;
        $html  = '<script type="text/javascript"> ';
        $html .= 'function ProcessFilters(form) { ';
        $html .= ' var ssd = \''.$this->ssd.'\';';
        $html .= ' }';
        $html .= '</script> ';

        return '';
    }

	function DrawHeader($filters)
	{
        //BOC localize here, noRiddle
        /*return 'Выберите из списка значение условия:<br><br>
            <form onsubmit="ProcessFilters(false); return false;" id="guayaquilFilterForm"><table class="g_filter_table">';*/
            
        return $this->GetLocalizedString('filter_choose_hint').'<br><br>
            <form onsubmit="ProcessFilters(false); return false;" id="guayaquilFilterForm"><table class="g_filter_table">';
            //CommonExtender::FormatLocalizedString
        //BOC localize here, noRiddle
	}

	function DrawBody($filters)
	{
        $html = '';
        $count = 0;
		foreach ($filters as $filter)
        {
            $html .= $this->DrawFilter($filter);
            $count ++;
        }
        if (!$count)
            return $this->DrawEmpty();

		return $html;
	}

	function DrawFilter($filter)
	{
        return '<tr><td valign="middle" style="text-align:right;">'.$this->DrawFilterName($filter).'</td><td>'.$this->DrawFilterBox($filter).'</td></tr>';
	}

	function DrawFilterName($filter)
	{
        return $filter['name'];
	}

	function DrawFilterBox($filter)
	{
        if (((string)$filter['type']) == 'list')
            return $this->DrawFilterBoxList($filter);

        return $this->DrawFilterBoxInput($filter);
	}

    function DrawFilterBoxInput($filter)
    {
        return '<input type=text class="g_filter g_filter_box" regexp="'.str_replace('"', '""', $filter['regexp']).'" ssd="'.str_replace('"', '""', $filter['ssdmodification']).'">';
    }

    function GetReturnURL()
    {
    }

	function DrawFilterBoxList($filter)
	{
        $html = '<select class="g_filter_box g_filter">';
        //$html .= '<option value="">-- Не указано --</option>';
        $html .= '<option value="">'.$this->GetLocalizedString('filter_choose_please').'</option>'; //localize, noRiddle

        $count = 0;
        foreach ($filter->values->row as $value)
        {
            $note = (string)$value;
            if ($note)
                return $this->DrawFilterBoxInputList($filter);

            $html .= '<option value="'.str_replace('"', '""', $value['ssdmodification']).'">'.$value['name'].'</option>';
            $count++;
        }

        if (!$count)
            return $this->DrawEmpty();

        $html .= '</select>';

        return $html;
	}

    private function DrawFilterBoxInputList($filter)
    {
        static $id = 0;
        $html = '';
        $count = 0;

        foreach ($filter->values->row as $value)
        {
            $html .= '<div class="g_filter_label"><label>
                    <input type="radio" name="filter_'.$id.'" class="g_filter g_filter_radio" ssd="'.str_replace('"', '""', $value['ssdmodification']).'"> <span class="g_filter_name">'.$value['name'].'</span><br>
                    <div class="g_filter_note">'.str_replace("\n", '<br>', (string)$value).'</div>
                </label></div>';
            $count ++;
        }

        if ($count > 5)
            $html = '<div class="g_filter_scroller">'.$html.'</div>';

        $html = '<div class="g_filter">'.$html.'</div>';

        $id++;

        return $html;
    }

    private function DrawEmpty()
    {
        $this->isEmpty = true;
        //BOC localize and build link, noRidlle
        //return 'Не найдено ни одного варианта условий, нажмите кнопку "Пропустить выбор" и перейдите на иллюстрацию';
        
        $link = $this->FormatLink('vehicle', null, $this->catalog);
        return $this->GetLocalizedString('nothing_found_see_catalogue' , $link);
        //BOC localize and build link, noRidlle
    }

	function DrawFooter($filters)
	{
        //BOC css buttons and localize, noRiddle
        /*$html = '<tr><td colspan="2" align="center">
            <input type="button" value="Пропустить выбор" onclick="window.location=\''.$this->FormatLink('skip', null, $this->catalog, $this).'\'; return false;">
            <input type="submit" value="Подтвердить" '.($this->isEmpty ? 'disabled="disabled"' : '').'>
            </td></tr>';*/
        $html = '<tr><td></td><td align="center">'
                //.'<button class="btn button-catalogue" type="button" onclick="window.location=\''.$this->FormatLink('skip', null, $this->catalog, $this).'\'; return false;">'.$this->GetLocalizedString('filter_ignore_selection').'</button>&nbsp;' //this button throws an error, commented out, noRiddle
                //.'<button class="btn button-catalogue" type="submit"'.($this->isEmpty ? ' disabled="disabled"' : '').'>'.$this->GetLocalizedString('filter_submit').'</button>'
                .'<span class="cssButton cssButtonColor1"><i class="fa fa-search"></i><span class="cssButtonText">'.$this->GetLocalizedString('filter_submit').'</span><button type="submit"'.($this->isEmpty ? ' disabled="disabled"' : '').' class="cssButtonText" title="Suchen">'.$this->GetLocalizedString('filter_submit').'</button></span>'
                .'</td></tr>';
        //EOC css buttons and localize, noRiddle
        $html .= '</table>';
        return $html;
	}
}
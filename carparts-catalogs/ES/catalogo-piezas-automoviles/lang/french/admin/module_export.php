<?php
/* --------------------------------------------------------------
   $Id: module_export.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(modules.php,v 1.8 2002/04/09); www.oscommerce.com 
   (c) 2003	 nextcommerce (modules.php,v 1.5 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE_MODULES_EXPORT', 'Module export');
define('HEADING_TITLE_MODULES_SYSTEM', 'Module système');

define('TABLE_HEADING_MODULES', 'Module');
define('TABLE_HEADING_SORT_ORDER', 'Ordre');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_MODULE_DIRECTORY', 'Module répértoire');

define('TABLE_HEADING_FILENAME', 'Nom du module (pour l\'utilisation interne)');
define('ERROR_EXPORT_FOLDER_NOT_WRITEABLE', 'Le répértoire "export/" est en lecture seule!');
define('TEXT_MODULE_INFO', 'Veuillez vérifier la version la plus actuelle des modules chez le prestataire correspondant!');

define('TABLE_HEADING_MODULES_INSTALLED', 'Les modules suivants ont été installés');
define('TABLE_HEADING_MODULES_NOT_INSTALLED', 'Les modules suivants sont encore disponibles');
?>

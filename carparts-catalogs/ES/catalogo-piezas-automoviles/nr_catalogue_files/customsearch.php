<?php
require_once('nr_catalogue_files/extender.php');

echo '<h1>'.CommonExtender::LocalizeString('SearchByFrame').'</h1>';

include('nr_catalogue_files/guayaquillib'.DIRECTORY_SEPARATOR.'render'.DIRECTORY_SEPARATOR.'catalog'.DIRECTORY_SEPARATOR.'framesearchform.php');

class FrameSearchExtender extends CommonExtender
{
    function FormatLink($type, $dataItem, $catalog, $renderer)
    {
        //BOC set link to content 1001 vehicles.php, noRiddle
        //return 'vehicles.php?ft=findByFrame&c='.$catalog.'&frame=$frame$&frameNo=$frameno$';
        return xtc_href_link(FILENAME_CONTENT, 'coID=1001').'?ft=findByFrame&c='.$catalog.'&frame=$frame$&frameNo=$frameno$';
        //EOC set link to content 1001 vehicles.php, noRiddle
    }   
}
$renderer = new GuayaquilFrameSearchForm(new FrameSearchExtender());
echo $renderer->Draw(array_key_exists('c', $_GET) ? $_GET['c'] : '', $cataloginfo, @$formframe, @$formframeno);

echo '<br><br>';

?>
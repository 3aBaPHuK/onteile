<?php
/* --------------------------------------------------------------
   $Id: campaigns.php 10597 2017-01-23 18:10:51Z Tomcraft $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(manufacturers.php,v 1.14 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (manufacturers.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Seguimiento de campañas');

define('TABLE_HEADING_CAMPAIGNS', 'Campañas');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_HEADING_NEW_CAMPAIGN', 'Nueva campaña');
define('TEXT_HEADING_EDIT_CAMPAIGN', 'Editar campaña');
define('TEXT_HEADING_DELETE_CAMPAIGN', 'Borrar campaña');

define('TEXT_CAMPAIGNS', 'Campañas:');
define('TEXT_DATE_ADDED', 'agregado el:');
define('TEXT_LAST_MODIFIED', 'última modificación el:');
define('TEXT_LEADS', 'Leads:');
define('TEXT_SALES', 'Ventas:');
define('TEXT_LATE_CONVERSIONS', 'Conversiones tardías:');
define('TEXT_NEW_INTRO', 'Por favor, introduzca una nueva campaña.');
define('TEXT_EDIT_INTRO', 'Por favor, haga todos los cambios necesarios.');

define('TEXT_CAMPAIGNS_NAME', 'Nombre de la campaña:');
define('TEXT_CAMPAIGNS_REFID', 'RefID de la campaña:');
define('TEXT_DISPLAY_NUMBER_OF_CAMPAIGNS', 'Campañas perseguidas:');

define('TEXT_DELETE_INTRO', '¿Está seguro de que quiere borrar esta campaña?');

define('TEXT_CAMPAIGNS_ERROR_REFID', 'Especifique un refID válido');
define('TEXT_CAMPAIGNS_ERROR_REFID_EXISTS', 'El refID introducido ya existe');
?>

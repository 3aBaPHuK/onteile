<?php
/* -----------------------------------------------------------------------------------------
   $Id: easybill.php 4242 2013-01-11 13:56:09Z gtb-modified $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
  
define('MODULE_EASYBILL_TEXT_DESCRIPTION', 'Escriba su factura en línea');
define('MODULE_EASYBILL_TEXT_TITLE', 'easyBill - Escribir facturas en línea');
define('MODULE_EASYBILL_STATUS_DESC', 'Estado del módulo');
define('MODULE_EASYBILL_STATUS_TITLE', 'Estado');
define('MODULE_EASYBILL_API_TITLE', 'API Key');
define('MODULE_EASYBILL_API_DESC', 'la clave debe ser creada en el menú de proveedores de easyBill.');
define('MODULE_EASYBILL_BILLINGID_TITLE', 'Gestionar número de factura:');
define('MODULE_EASYBILL_BILLINGID_DESC', 'Las configuraciones de easyBill se deben realizar en el menú del proveedor de easyBill.<br/><br/>Si los números de factura deben ser gestionados por la tienda, se debe asegurar el número de factura consecutivo de forma única.');
define('MODULE_EASYBILL_BILLCREATE_TITLE', 'Crear facturas');
define('MODULE_EASYBILL_BILLCREATE_DESC', '¿Deben crearse las facturas automáticamente durante el proceso de pago o manualmente?');
define('MODULE_EASYBILL_BILLSAVE_TITLE', 'Guardar facturas');
define('MODULE_EASYBILL_BILLSAVE_DESC', '¿Deben archivarse las facturas en easyBill o localmente?');
define('MODULE_EASYBILL_PAYMENT_TITLE', 'Modalidades de pago');
define('MODULE_EASYBILL_PAYMENT_DESC', '¿Qué modalidades de pago no deben generar una factura automática?<br/><br/>Lista separada por punto y coma (;)');
define('MODULE_EASYBILL_DO_STATUS_CHANGE_TITLE', 'Cambiar el estado del pedido');
define('MODULE_EASYBILL_DO_STATUS_CHANGE_DESC', '¿Debe cambiarse el estado del pedido después de que se haya creado la factura?');
define('MODULE_EASYBILL_DO_AUTO_PAYMENT_TITLE', 'Marcar la factura como pagada');
define('MODULE_EASYBILL_DO_AUTO_PAYMENT_DESC', '¿Debe marcarse la factura como pagada al crearla?');
define('MODULE_EASYBILL_NO_AUTO_PAYMENT_TITLE', 'Marcar la factura como pagada - excepciones');
define('MODULE_EASYBILL_NO_AUTO_PAYMENT_DESC', '¿Qué modalidades de pago no deben establecerse automáticamente como pagadas?<br/><br/>Lista separada por punto y coma (;)');
define('MODULE_EASYBILL_STATUS_CHANGE_TITLE', 'estado del pedido');
define('MODULE_EASYBILL_STATUS_CHANGE_DESC', 'Establecer a este estado después de que se haya creado la factura.');
define('MODULE_EASYBILL_STANDARD_TAX_CLASS_TITLE', '<hr /><b>Tasa de impuesto estándar:</b>');
define('MODULE_EASYBILL_STANDARD_TAX_CLASS_DESC', 'Por favor, especifique una tasa por defecto en caso de que no se pueda determinar ninguna clase de impuesto.');
  
?>

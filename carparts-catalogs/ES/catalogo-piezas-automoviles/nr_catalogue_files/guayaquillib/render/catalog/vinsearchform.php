<?php

require_once dirname(__FILE__) . '/../template.php';

class GuayaquilVinSearchForm extends GuayaquilTemplate
{
    var $catalog = NULL;
    var $cataloginfo = NULL;
    var $vag_arr;

    function __construct(IGuayaquilExtender $extender)
    {
        parent::__construct($extender);
        $this->vag_arr = array('audi', 'seat', 'skoda', 'volkswagen'); //added array for VAG brands (used below), noRiddle
    }

    function Draw($catalog, $cataloginfo, $prevvin = '')
    {
        $this->cataloginfo = $cataloginfo;
        $this->catalog = $catalog;

        $html = $this->DrawVinCheckScript();
        $html .= $this->DrawVinExample($cataloginfo);
        $html .= $this->DrawVinForm($catalog, $prevvin);

        return $html;
    }

    function DrawVinCheckScript()
    {
        //BOC, make vin search possible independent from c parameter (= brand) | e.g. fiat professional vin: ZFA26300009141064 | revoked
        $html = '<script type="text/javascript">
		function checkVinValue(value, submit_btn) {
		    value = value.replace(/[^\da-zA-Z]/g,\'\');
            value = value.replace(/o/gi,0); //added replacement of letter o with number 0, noRiddle 06-2018
            var expr = new RegExp(\'' . $this->GetVinRegularExpression() . '\', \'i\');
            if (expr.test(value))
            {
                jQuery(submit_btn).attr(\'disabled\', \'1\');
                jQuery(\'#VINInput\').attr(\'class\',\'g_input\');
                window.location = \'' . $this->FormatLink('vehicles', NULL, $this->catalog) . '\'.replace(\'\\$vin\\$\', value);
            } else
            jQuery(\'#VINInput\').attr(\'class\',\'g_input_error\');
        }
		</script> ';
        
        /*$html = '<script type="text/javascript">
		function checkVinValue(value, submit_btn) {
		    value = value.replace(/[^\da-zA-Z]/g,\'\');
            var expr = new RegExp(\'' . $this->GetVinRegularExpression() . '\', \'i\');
            if (expr.test(value))
            {
                jQuery(submit_btn).attr(\'disabled\', \'1\');
                jQuery(\'#VINInput\').attr(\'class\',\'g_input\');
                window.location = \'' . $this->FormatLink('vehicles', NULL, '') . '\'.replace(\'\\$vin\\$\', value);
            } else
            jQuery(\'#VINInput\').attr(\'class\',\'g_input_error\');
        }
		</script> ';*/
        //EOC, make vin search possible independent from c parameter (= brand) | e.g. fiat professional vin: ZFA26300009141064

        return $html;
    }

    function GetVinRegularExpression()
    {
        return '\\^[A-z0-9]{12}[0-9]{5}\$';
    }

    function GetVinExample($cataloginfo)
    {
        if ($cataloginfo) {
            foreach ($cataloginfo->features->feature as $feature) {
                if ((string)$feature['name'] == 'vinsearch') {
                    return $feature['example'];
                }
            }
        }

        return 'WAUBH54B11N111054';
    }

    function DrawVinExample($cataloginfo)
    {
        return $this->GetLocalizedString('InputVIN', array($this->GetVinExample($cataloginfo))) . '<br>';
    }

    function DrawVinForm($catalog, $prevvin)
    {
        /*$html = '
            <form name="findByVIN" onSubmit="checkVinValue(this.vin.value);return false;" id="findByVIN" >
                <div id="VINInput" class="g_input"><input name="vin" type="text" id="vin" size="17" style="width:200px;" value="' . $prevvin . '"/></div>
                <input type="submit" name="vinSubmit" value="' . $this->GetLocalizedString('Search') . '" id="vinSubmit" />
                <input type="hidden" name="option" value="com_guayaquil" />
                <input type="hidden" name="view" value="vehicles" />
                <input type="hidden" name="ft" value="findByVIN" />
		    </form>';*/
            
        $html = '
            <form name="findByVIN" onSubmit="checkVinValue(this.vin.value);return false;" id="findByVIN" >
                <div id="VINInput" class="g_input">
                <input name="vin" type="text" id="vin" size="17" value="'.($prevvin != '' ? $prevvin : $this->GetLocalizedString('search_placeholder')).'" placeholder="'. $this->GetLocalizedString('search_placeholder').'" onfocus="if(this.value==this.defaultValue) this.value=\'\';" onblur="if(this.value==\'\') this.value=this.defaultValue;" />';
      //$html .= '<button class="btn button-catalogue" type="submit">'.$this->GetLocalizedString('Search').'</button>';
      $html .= '<p><span class="cssButton cssButtonColor1">
                    <i class="fa fa-search"></i><span class="cssButtonText">'.$this->GetLocalizedString('Search').'</span><button type="submit" class="cssButtonText" title="'.$this->GetLocalizedString('Search').'">'.$this->GetLocalizedString('Search').'</button>
                </span></p>';
      $html .= '<input type="hidden" name="option" value="com_guayaquil" />
                <input type="hidden" name="view" value="vehicles" />
                <input type="hidden" name="ft" value="findByVIN" />
                </div>
		    </form>';
            //<input type="submit" name="vinSubmit" value="' . $this->GetLocalizedString('Search') . '" id="vinSubmit" />
            //filter by vag_arr to display different text, noRiddle
            $is_vag = in_array($_SESSION['nr_ctlg_brands'][$this->catalog]['our_brand'], $this->vag_arr) ? true : false;
            $html .= $_GET['coID'] != '1001' ? '<p>'.($is_vag ? TXT_IMPORT_INFO_VIN_VAG : TXT_IMPORT_INFO_VIN).'</p>' : '';

        return $html;
    }
}
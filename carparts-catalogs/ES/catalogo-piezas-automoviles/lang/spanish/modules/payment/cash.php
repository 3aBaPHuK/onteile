<?php

/* -----------------------------------------------------------------------------------------
   $Id: cash.php 1102 2005-07-24 15:05:38Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com
   (c) 2003	 nextcommerce (invoice.php,v 1.4 2003/08/13); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_CASH_TEXT_DESCRIPTION', 'Pago en efectivo');
define('MODULE_PAYMENT_CASH_TEXT_TITLE', 'Pago en efectivo');
define('MODULE_PAYMENT_CASH_TEXT_INFO', '');
define('MODULE_PAYMENT_CASH_STATUS_TITLE', 'Activar módulo para pago en efectivo');
define('MODULE_PAYMENT_CASH_STATUS_DESC', '¿Quiere aceptar pagos en efectivo?');
define('MODULE_PAYMENT_CASH_ORDER_STATUS_ID_TITLE', 'Determinar estado del pedido');
define('MODULE_PAYMENT_CASH_ORDER_STATUS_ID_DESC', 'Cambiar los pedidos realizados con este módulo a este estado');
define('MODULE_PAYMENT_CASH_SORT_ORDER_TITLE', 'Orden de visualización');
define('MODULE_PAYMENT_CASH_SORT_ORDER_DESC', 'Orden de la visualización. Primero se muestra el dígito más pequeño.');
define('MODULE_PAYMENT_CASH_ZONE_TITLE', 'Zona de pago');
define('MODULE_PAYMENT_CASH_ZONE_DESC', 'Si se selecciona una zona, el método de pago se aplica sólo a esa zona.');
define('MODULE_PAYMENT_CASH_ALLOWED_TITLE', 'Zonas permititdas');
define('MODULE_PAYMENT_CASH_ALLOWED_DESC', 'Introduzca <b>individualmente</b>  las zonas permitidas para este módulo. (por ejemplo AT,DE (si está vacío, se permiten todas las zonas))');
?>

<?php
/**************************************
* file: new_lang_consts_fr.php
* use: new language constants
* (c) noRiddle 01-2017
**************************************/

define('AH_IMAGE_BUTTON_SEARCH', 'SAISIR LE NUMÉRO DE PIÈCE ICI');
//new error for invalid part no coming from search_results.php
define('AH_JS_AT_LEAST_ONE_INPUT', 'Malheureusement nous n\'avons pas trouvé de pièce avec ce numéro.\nNous vous proposons l\'aide suivant:');
//errors for new field VIN in checkout_payment, noRiddle
define('ERROR_NO_VIN_INDICATED', 'Veuillez indiquer le numéro de châssis à 17 positions (VIN). br /> Cet information assure que vos pièces commandées vont bien avec votre véhicule.<br /> Si le numéro VIN n\'est pas pertinant, indiquez "no" dans le champ VIN.');
define('ERROR_VIN_NOT_PLAUSIBLE', 'Veuillez indiquer une numéro de châssis (VIN, à 17 positions) valable. <br /> Par cette information, nous assurons que vos pièces commandées vont bien avec votre véhicule.');
//new email fields, noRiddle
define('EMAIL_PRODUCTS_PRICE_QUESTION', 'Demande de produit pour: ');
define('EMAIL_VIN', 'Numéro de chassis (VIN): ');
define('AH_ERROR_VIN_EMPTY', '<li><b> Numéro de châssis (VIN):</b><br /> Veuillez indiquer votre numéro de châssis (VIN à 17 positions).<br />Par cette information, nous assurons que vos pièces commandées vont bien avec votre véhicule.</li>');
define('AH_ERROR_VIN_NOPLAUSIBLE', '<li><b> Numéro de châssis (VIN):</b><br /> Veuillez indiquer votre numéro de châssis (VIN à 17 positions).<br />Par cette information, nous assurons que vos pièces commandées vont bien avec votre véhicule.</li>');

//BOC SEO meta title texts, noRiddle
define('SEO_META_TITLE_1', 'acheter pas cher en ligne');
define('SEO_META_TITLE_2', 'Pièce de rechange en ligne');
define('SEO_META_TITLE_3', 'Commander pièce de rechange en ligne');
define('SEO_META_TITLE_4', 'Pour la recherche en ligne au sein du catalogue de véhicule recherche VIN');
//EOC SEO meta title texts, noRiddle

define('NR_GOOG_DEFAULT_TXT', 'Langues additionelles');

//BOC new for ADAC field, noRiddle
define('ENTRY_ADAC_NO_FALSE', 'Votre numéro ADAC n\'est pas correct. <br />Veuillez le vérifier.<br />Vous pouvez créer un compte sans numéro ADAC à tout moment.<br />Dans ce cas, laissez vide le champ corresondant.');
define('ENTRY_ACCOUNT_ADAC_NO_FALSE', 'Votre numéro ADAC n\'est pas correct.<br /> Veuillez le vérifier.' );
define('TXT_SUCCESS_ADAC_MEMBER', 'Votre statut client est membre ADAC. Vous pouvez acheter à prix réduits.');
define('TXT_ERROR_ADAC_MEMBER', 'Votre statut client ne pouvait pas être changé vers membre ADAC. Nous vous prions de nous en excuser. <br />S\'ils vous plaît<a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=7').'" title="contact">contactez</a> nous.');
//EOC new for ADAC field, noRiddle

//BOC inclusion for affiliate program, noRiddle
require(DIR_WS_LANGUAGES . 'french/'.'affiliate_french.php');
//EOC inclusion for affiliate program, noRiddle

//BOC additional text for login.php, noRiddle
define('ADD_TEXT_GUEST_LOGIN', 'En plus vous pouvez profiter des avantages clients tels que <a class="iframe" href="'.xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=964').'" title="rabais" target="_blank"> des rabais ADAC, de club ou de garage</a> seulement en tant que client.');
define('ADD_TEXT_NEW_LOGIN', 'Nous mettons l\'accent sur la <a class="iframe" href="'.xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=2').'" title="protection des données" target="_blank">protection de vos données</a>.');
define('ADD_INFO_TEXT_NEW_LOGIN', '<ul><li>If you are a commercial customer, e.g. a <strong>garage</strong>, you may buy with <strong>special conditions</strong>.<br />For this purpose we need your company registration, which you may send us via our <a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=7').'" title="Contact">contact form</a>.</li>'.($cat_env == 'de' ? '<li>Also we offer <strong>discounts</strong> for <strong>club-</strong> and <strong>ADAC-members</strong>.<br />To get the Für die ADAC-conditions just indicate your ADAC member number in "Your account" to which you will find the link in the top menu.</li>' : '<li>Also we offer  <strong>discounts</strong> for <strong>club-</strong>members. Please send us your club id card via our <a href="xx_7.html" title="send file via contact form">contact form</a>!')
.'</ul>');
//EOC additional text for login.php, noRiddle

//BOC needed for order mail if deposit, noRiddle
define('AH_INCLUSIVE', 'incl.');
//EOC needed for order mail if deposit, noRiddle

//BOC new texts for catalogue, noRiddle
define('S_TXT_PARTNO', 'Numéro de pièce');
define('S_TXT_NAME', 'Nom');
define('S_TXT_QTY', 'Quantité');
define('S_TXT_CHECK', 'cocher');
define('S_TXT_INTOCART', 'Mettre la pièce coché au panier');
define('S_SPECIALS_HEADING_TXT', 'Une sélection de nos offres attractives d\'accesoires pour %s');
define('S_PART_WAS_REPLACED_WITH', 'Cette pièce a été remplacée par ');
define('IMAGE_BUTTON_CATALOGUE_CHECKOUT', 'Terminer la commande dans la boutique');
define('IMAGE_BUTTON_UPDATE_CAT_CART', 'actualiser le panier de%s');
define('NR_UPDATE_CART', 'Votre %s panier contient:');
define('NR_NOW_CREDIT_FOR_VINSEARCH', 'Pourquoi est-ce que je ne vois plus le champ de recherche VIN ?<br /> Par la création de votre compte, vous recevez 10 points gratuits afin de pouvoir utiliser la recherche VIN. <br /> Vous pouvez recevoir d\'autres requêtes gratuites par l\'achat des pièces de rechange dans une de nos boutiques ou bien vous devriez acheter des requêtes <a href="'.xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=1').'" title="acheter de l\'avoir pour la recherche VIN">» ici </a>.<br /> L\'identification de votre véhicule à l\'aide des paramètres (si disponible) est gratuite dans tous les cas.');
define('TXT_SEND_ORDER_VI_CREDIT', 'Vous avez gagnez %d points sur votre compte crédit de recherche VIN.');

define('TXT_MAY_WE_HELP', 'Besoin d\'aide ?');
define('TXT_DIRECTLY_TO_PARTNER_BUTTON1', 'Directement à notre');
define('TXT_DIRECTLY_TO_PARTNER_BUTTON2', 'partenaire');
define('TXT_CONTACT_US_IF_NEED_HELP', 'Quelques questions sur cette marque ou une pièce de rechange ?');
define('TXT_IMPORT_INFO_VIN', 'Pas tous les NIVs sont listés dans notre base de données. Si le NIV saisi n\'est pas trouvé servez vous de la "Identification du véhicule par paramètre" à gauche s\'il vous plaît, si elle est disponible.<br />');
define('TXT_IMPORT_INFO_VIN_VAG', 'For technical reasons only approx. 50% of all VINs for this brand are registered. If your entered VIN cannot be found please use "Identification of car by parameters" on the left.<br />');
//EOC new texts for catalogue, noRiddle

//BOC new texts for cookieconsent plugin, noRiddle
define('COOKIES_TXT_NEW', '<i class="fa fa-cog"></i> Par l\\\'utilisation ultérieure de ce site web vous acceptez l\\\'utilisation des cookies qui permettent un accès sans accroc sur notre site web.');
define('COOKIES_LINKTXT_TITLE', 'Informations suivants');
define('COOKIES_LINKTXT', '<i class="fa fa-info-circle"></i>');
define('COOKIES_CLOSE_TITLE', 'fermer');
define('COOKIES_CLOSE', '<i class="fa fa-close"></i>');
//EOC new texts for cookieconsent plugin, noRiddle

//BOC käufersiegel, noRiddle
define('KS_THIS_SHOP_WAS', 'La boutique a été évalué avec');
define('KS_OF', ' de ');
define('KS_STARES_RATED', ' étolies sur kauefersiegel.de - basé sur ');
define('KS_RATINGS', ' évaluations');
//EOC käufersiegel, noRiddle

//BOC new button for login box, noRiddle
define('IMAGE_BUTTON_NEW_CUST', 'Nouveau client ?');
//EOC new button for login box, noRiddle

//BOC button "all brands at a glance" in shops box, 10-2020, noRiddle
define('AH_ALL_BRANDS_MAIN_TXT', 'Portail principal');
define('AH_ALL_BRANDS_VIEW_TXT', '&raquo; Toutes les marques en un coup d\'oeil');
//EOC button "all brands at a glance" in shops box, 10-2020, noRiddle

//01-2021, noRiddle
define('NO_GENDER', 'Pas de titre');
?>
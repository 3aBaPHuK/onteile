<?php
/* -----------------------------------------------------------------------------------------
   $Id: fedexeu.php 5123 2013-07-18 11:49:11Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce( fedexeu.php,v 1.01 2003/02/18 03:25:00); www.oscommerce.com
   (c) 2003	 nextcommerce (fedexeu.php,v 1.5 2003/08/1); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   fedex_europe_1.02        	Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/



define('MODULE_SHIPPING_FEDEXEU_TEXT_TITLE', 'FedEx Express Europa');
define('MODULE_SHIPPING_FEDEXEU_TEXT_DESCRIPTION', 'FedEx Express Europa');
define('MODULE_SHIPPING_FEDEXEU_TEXT_WAY', ' Envíos a ');
define('MODULE_SHIPPING_FEDEXEU_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_FEDEXEU_INVALID_ZONE', 'No es posible realizar un envío a este país.');
define('MODULE_SHIPPING_FEDEXEU_UNDEFINED_RATE', 'Los costos de envío no se pueden calcular por el momento.');

define('MODULE_SHIPPING_FEDEXEU_STATUS_TITLE' , 'FedEx Express Europe');
define('MODULE_SHIPPING_FEDEXEU_STATUS_DESC' , '¿Desea ofrecer envíos a través de FedEx Express Europa?');
define('MODULE_SHIPPING_FEDEXEU_HANDLING_TITLE' , 'Tarifa de manejo');
define('MODULE_SHIPPING_FEDEXEU_HANDLING_DESC' , 'Costo de procesamiento para este modo de envío en Euro');
define('MODULE_SHIPPING_FEDEXEU_TAX_CLASS_TITLE' , 'Tasa de impuesto');
define('MODULE_SHIPPING_FEDEXEU_TAX_CLASS_DESC' , 'Seleccione el tipo de IVA para este modo de envío.');
define('MODULE_SHIPPING_FEDEXEU_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_FEDEXEU_ZONE_DESC' , 'Si selecciona una zona, este modo de envío sólo se ofrecerá en esa zona.');
define('MODULE_SHIPPING_FEDEXEU_SORT_ORDER_TITLE' , 'Orden de la visualización');
define('MODULE_SHIPPING_FEDEXEU_SORT_ORDER_DESC' , 'Menor se muestra en primer lugar.');
define('MODULE_SHIPPING_FEDEXEU_ALLOWED_TITLE' , 'Zonas de envío individuales');
define('MODULE_SHIPPING_FEDEXEU_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_1_TITLE' , 'Países de la zona europea 1');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_1_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona 1 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_1_TITLE' , 'Tabla de tarifas para zona 1 hasta 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_1_DESC' , 'Tabla de tarifas para la zona 1, basada en  <b>\'PAK\'</b>  hasta 2.50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_1_TITLE' , 'Tabla de tarifas para zona 1 hasta 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_1_DESC' , 'Tabla de tarifas para la zona 1, basada en  <b>\'ENV\'</b>  hasta 60 páginas en formato DIN A4 y 0,50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_1_TITLE' , 'Tabla de tarifas para zona 1 hasta 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_1_DESC' , 'Tabla de tarifas para la zona 1, basada en  <b>\'BOX\'</b>  hasta 10 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_1_TITLE' , 'Recargo de aumento hasta 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_1_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_1_TITLE' , 'Recargo de aumento hasta 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_1_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_1_TITLE' , 'Recargo de aumento hasta 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_1_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_2_TITLE' , 'Países de la zona europea 2');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_2_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona 2 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_2_TITLE' , 'Tabla de tarifas para zona 2 hasta 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_2_DESC' , 'Tabla de tarifas para la zona 2, basada en  <b>\'PAK\'</b>  hasta 2.50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_2_TITLE' , 'Tabla de tarifas para zona 2 hasta 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_2_DESC' , 'Tabla de tarifas para la zona 2, basada en  <b>\'ENV\'</b>  hasta 60 páginas en formato DIN A4 y 0,50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_2_TITLE' , 'Tabla de tarifas para zona 2 hasta 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_2_DESC' , 'Tabla de tarifas para la zona 2, basada en  <b>\'BOX\'</b>  hasta 10 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_2_TITLE' , 'Recargo de aumento hasta 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_2_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_2_TITLE' , 'Recargo de aumento hasta 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_2_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_2_TITLE' , 'Recargo de aumento hasta 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_2_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_3_TITLE' , 'Países de la zona europea 3');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_3_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona 3 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_3_TITLE' , 'Tabla de tarifas para zona 3 hasta 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_3_DESC' , 'Tabla de tarifas para la zona 3, basada en  <b>\'PAK\'</b>  hasta 2.50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_3_TITLE' , 'Tabla de tarifas para zona 3 hasta 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_3_DESC' , 'Tabla de tarifas para la zona 3, basada en  <b>\'ENV\'</b>  hasta 60 páginas en formato DIN A4 y 0,50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_3_TITLE' , 'Tabla de tarifas para zona 3 hasta 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_3_DESC' , 'Tabla de tarifas para la zona 3, basada en  <b>\'BOX\'</b>  hasta 10 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_3_TITLE' , 'Recargo de aumento hasta 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_3_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_3_TITLE' , 'Recargo de aumento hasta 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_3_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_3_TITLE' , 'Recargo de aumento hasta 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_3_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_4_TITLE' , 'Países de la zona mundial A');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_4_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona A (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_4_TITLE' , 'Tabla de tarifas para zona A hasta 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_4_DESC' , 'Tabla de tarifas para la zona A, basada en  <b>\'PAK\'</b>  hasta 2.50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_4_TITLE' , 'Tabla de tarifas para zona A hasta 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_4_DESC' , 'Tabla de tarifas para la zona A, basada en  <b>\'ENV\'</b>  hasta 60 páginas en formato DIN A4 y 0,50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_4_TITLE' , 'Tabla de tarifas para zona A hasta 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_4_DESC' , 'Tabla de tarifas para la zona A, basada en  <b>\'BOX\'</b>  hasta 10 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_4_TITLE' , 'Recargo de aumento hasta 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_4_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_4_TITLE' , 'Recargo de aumento hasta 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_4_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_4_TITLE' , 'Recargo de aumento hasta 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_4_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_5_TITLE' , 'Países de la zona mundial B');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_5_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona B (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_5_TITLE' , 'Tabla de tarifas para zona B hasta 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_5_DESC' , 'Tabla de tarifas para la zona B, basada en  <b>\'PAK\'</b>  hasta 2.50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_5_TITLE' , 'Tabla de tarifas para zona B hasta 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_5_DESC' , 'Tabla de tarifas para la zona B, basada en  <b>\'ENV\'</b>  hasta 60 páginas en formato DIN A4 y 0,50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_5_TITLE' , 'Tabla de tarifas para zona B hasta 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_5_DESC' , 'Tabla de tarifas para la zona B, basada en  <b>\'BOX\'</b>  hasta 10 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_5_TITLE' , 'Recargo de aumento hasta 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_5_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_5_TITLE' , 'Recargo de aumento hasta 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_5_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_5_TITLE' , 'Recargo de aumento hasta 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_5_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_6_TITLE' , 'Países de la zona mundial C');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_6_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona C (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_6_TITLE' , 'Tabla de tarifas para zona C hasta 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_6_DESC' , 'Tabla de tarifas para la zona C, basada en  <b>\'PAK\'</b>  hasta 2.50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_6_TITLE' , 'Tabla de tarifas para zona C hasta 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_6_DESC' , 'Tabla de tarifas para la zona C, basada en  <b>\'ENV\'</b>  hasta 60 páginas en formato DIN A4 y 0,50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_6_TITLE' , 'Tabla de tarifas para zona C hasta 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_6_DESC' , 'Tabla de tarifas para la zona C, basada en  <b>\'BOX\'</b>  hasta 10 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_6_TITLE' , 'Recargo de aumento hasta 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_6_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_6_TITLE' , 'Recargo de aumento hasta 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_6_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_6_TITLE' , 'Recargo de aumento hasta 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_6_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_7_TITLE' , 'Países de la zona mundial D');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_7_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona D (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_7_TITLE' , 'Tabla de tarifas para zona D hasta 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_7_DESC' , 'Tabla de tarifas para la zona D, basada en  <b>\'PAK\'</b>  hasta 2.50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_7_TITLE' , 'Tabla de tarifas para zona D hasta 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_7_DESC' , 'Tabla de tarifas para la zona D, basada en  <b>\'ENV\'</b>  hasta 60 páginas en formato DIN A4 y 0,50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_7_TITLE' , 'Tabla de tarifas para zona D hasta 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_7_DESC' , 'Tabla de tarifas para la zona D, basada en  <b>\'BOX\'</b>  hasta 10 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_7_TITLE' , 'Recargo de aumento hasta 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_7_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_7_TITLE' , 'Recargo de aumento hasta 40 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_7_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_7_TITLE' , 'Recargo de aumento hasta 70 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_7_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');

define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_8_TITLE' , 'Países de la zona mundial E');
define('MODULE_SHIPPING_FEDEXEU_COUNTRIES_8_DESC' , 'Lista de países separados por comas en forma de códigos de país de dos caracteres en código ISO que forman parte de la zona E (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_8_TITLE' , 'Tabla de tarifas para zona E hasta 2.50 kg PAK');
define('MODULE_SHIPPING_FEDEXEU_COST_PAK_8_DESC' , 'Tabla de tarifas para la zona E, basada en  <b>\'PAK\'</b>  hasta 2.50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_8_TITLE' , 'Tabla de tarifas para zona E hasta 0.50 kg ENV');
define('MODULE_SHIPPING_FEDEXEU_COST_ENV_8_DESC' , 'Tabla de tarifas para la zona E, basada en  <b>\'ENV\'</b>  hasta 60 páginas en formato DIN A4 y 0,50 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_8_TITLE' , 'Tabla de tarifas para zona E hasta 10 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_COST_BOX_8_DESC' , 'Tabla de tarifas para la zona E, basada en  <b>\'BOX\'</b>  hasta 10 kg de peso de envío.');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_8_TITLE' , 'Recargo de aumento hasta 20 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_20_8_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_8_TITLE' , 'Recargo de aumento hasta 30 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_40_8_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_8_TITLE' , 'Recargo de aumento hasta 50 kg BOX');
define('MODULE_SHIPPING_FEDEXEU_STEP_BOX_70_8_DESC' , 'Recargo de aumento por cada 0,50 kg de exceso en EUR');
?>

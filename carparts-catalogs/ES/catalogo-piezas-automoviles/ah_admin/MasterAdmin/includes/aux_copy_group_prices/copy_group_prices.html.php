<?php
/*******************************************************************************************
* file: /copy_group_prices.html.php
* (c) noRiddle 12-2015
*******************************************************************************************/
?>
<div class="script-code">
    <h2><?php echo ADMIN_TOOLS_COPY_CUSTOMERS_STATUS_PRICES_TITLE.'<br /><span class="sml">'.ADMIN_TOOLS_COPY_CUSTOMERS_STATUS_PRICES_DESC.'</span>'; //echo ADMIN_TOOLS_SCRIPT_HEADING; ?></h2>
    <div class="div-marg">
        <div class="bord">
            <?php
            echo '<p>'.ADMIN_TOOLS_COPY_PERSOFF_ALL_GUIDANCE.'</p>'
            .'<div>'
            //.'<form name="copy_pers_off_all" action="'.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=copy_group_prices&action=copy_persoff_all', 'NONSSL' , false).'" method="post">';
            .xtc_draw_form('copy_pers_off_all', FILENAME_MASTER_ADMIN, 'script=copy_group_prices&action=copy_persoff_all')
                .xtc_draw_hidden_field('copy_all_persoff', '1').' <button type="submit" onclick="this.blur();">'.BUTTON_COPY.'</button>'
            .'</form>'
            .'</div>'
            .'<div id="copy-persoff-all-out" class="hve-out">'.((isset($_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out']) && $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] != '') ? $_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out'] : '').'</div>';
            unset($_SESSION[$_SESSION['ma_shopsess'].'persoff_all_out']);
            ?>
        </div>
    </div>
<!-- BOC recalculate price with factor of all personal_offers, 02-2021, noRiddle -->
    <div class="div-marg">
        <div class="bord">
            <h4 id="tog-copy-all-withfact" class="gold"><?php echo ADMIN_TOOLS_CHANGE_PERSOFF_FACTOR; ?></h4>
            <div id="change-all">
                <p><?php echo ADMIN_TOOLS_CHANGE_PERSOFF_FACTOR_GUIDANCE; ?></p>
            <?php
            echo xtc_draw_form('change_all_pers_off_factor', FILENAME_MASTER_ADMIN, 'script=copy_group_prices&action=all_pers_off_factor', 'post', 'id="change-all-pers-off-factor"');
            echo '<table>';
            echo '<tr><td colspan="2">'.ADMIN_TOOLS_SYNC_FACTOR_TXT.xtc_draw_checkbox_field('chck_factor_copy', '1', '', '1', 'id="chck-factor-copy"').'</td></tr>';

            foreach($ie_personal_offers_array as $persoff) {
                $poi_arr = explode('_', $persoff);
                $i = end($poi_arr);
                
                echo '<tr>'
                        .'<td>'
                            .'<span class="turq">personal_offers_by_customers_status_</span>'.xtc_draw_input_field('change_pers_off_'.$i, $i, ' class="turq" size="1" readonly="readonly"')
                        .'</td>'
                        .'<td>'
                            .' <span class="matt-red">'.ADMIN_TOOLS_COPY_PERSOFF_FACT_TXT.'</span> '
                            .xtc_draw_input_field('change_persoff_factor[]', '', 'data-grp="'.$i.'" class="matt-red" size="12"')
                            .' '.ADMIN_TOOLS_CHANGE_PERSOFF_FACT_LAST.' '.$last_persoff_factor_arr[$i]['factor'].' ('.$last_persoff_factor_arr[$i]['last_modified'].')'
                            .' => '.ADMIN_TOOLS_CHANGE_PERSOFF_FACT_LAST_REV.' '.$last_persoff_factor_arr[$i]['revoce_fact']
                        .'</td>'
                    .'</tr>';
            }

            echo '</table>';
            echo '<p><button type="submit" onclick="this.blur();">'.BUTTON_EXECUTE.'</button></p>';
            echo '</form>';
            ?>
                <div id="change-persoff-factor-out" class="hve-out"><?php echo $_SESSION[$_SESSION['ma_shopsess'].'persoff_factor_out'] != '' ? $_SESSION[$_SESSION['ma_shopsess'].'persoff_factor_out'] : ''; ?></div>
                <?php 
                unset($_SESSION[$_SESSION['ma_shopsess'].'persoff_factor_out']);
                ?>
            </div>
        </div>
    </div>
<!-- EOC recalculate price with factor of all personal_offers, 02-2021, noRiddle -->
    <div class="div-marg">
        <div class="bord">
            <h4 id="top-copy-with-factor" class="gold"><?php echo ADMIN_TOOLS_COPY_PERSOFF_WITH_FACTOR; ?></h4>
            <div id="copy-with-factor">
                <ul><?php echo ADMIN_TOOLS_COPY_PERSOFF_WITH_FACTOR_GUIDANCE; ?></ul>
                <?php
                //echo '<form name="copy_pers_off_factor" action="'.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=copy_group_prices&action=copy_persoff_price_factor', 'NONSSL' , false).'" method="post">';
                echo xtc_draw_form('copy_pers_off_factor', FILENAME_MASTER_ADMIN, 'script=copy_group_prices&action=copy_persoff_price_factor')
                    .'<table>'
                        .'<tr>'
                            .'<td>'
                                .ADMIN_TOOLS_PRICES_FROM.' <span class="turq">personal_offers_by_customers_status_</span>'.xtc_draw_input_field('copy_persoff_from_with_factor', '', ' class="turq" size="1"')
                            .'</td>'
                            .'<td>'
                                .ADMIN_TOOLS_COPY_PERSOFF_TXT.' <span class="matt-green">personal_offers_by_customers_status_</span>'
                                .xtc_draw_input_field('copy_persoff_to_with_factor', '', 'class="matt-green" size="1"')
                            .'</td>'
                            .'<td>'
                                .' <span class="matt-red">'.ADMIN_TOOLS_COPY_PERSOFF_FACT_TXT.'</span> '
                                .xtc_draw_input_field('copy_persoff_factor', '', 'class="matt-red" size="12"')
                            .'</td>'
                            .'<td>'
                                .'<button type="submit" onclick="this.blur();">'.BUTTON_COPY.'</button>'
                            .'</td>'
                        .'</tr>'
                    .'</table>'
                .'</form>';
                ?>
                <div id="copy-persoff-factor-out" class="hve-out"><?php echo $_SESSION[$_SESSION['ma_shopsess'].'copy_with_factor_out'] != '' ? $_SESSION[$_SESSION['ma_shopsess'].'copy_with_factor_out'] : ''; ?></div>
                <?php unset($_SESSION[$_SESSION['ma_shopsess'].'copy_with_factor_out']); ?>
            </div>
        </div>
    </div>
    
    <div class="div-marg">
        <div class="bord">
            <h4 id="tog-copy-single" class="gold"><?php echo ADMIN_TOOLS_COPY_PERSOFF_SINGLE; ?></h4>
            <div id="copy-single">
                <p><?php echo ADMIN_TOOLS_COPY_PERSOFF_GUIDANCE; ?></p>
            <?php
            foreach($ie_personal_offers_array as $poff) {
                $poj_arr = explode('_', $poff);
                $j = end($poj_arr);
                //BOC pre fill values, 05-2020, noRiddle
                // onfocus="if(this.value==this.defaultValue) this.value='';" onblur="if(this.value=='') this.value=this.defaultValue;"
                $prefill_val = '';
                switch($j) {
                    case '1':
                        $prefill_val = '2,6,11,12';
                      break;
                    case '3':
                        $prefill_val = '5';
                      break;
                    case '7':
                        $prefill_val = '15';
                      break;
                    case '8':
                        $prefill_val = '9,10,13,14';
                      break;
                }
                //EOC pre fill values, 05-2020, noRiddle
                //echo '<form name="copy_pers_off_'.$j.'" action="'.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=copy_group_prices&action=copy_persoff_single', 'NONSSL' , false).'" method="post">';
                echo xtc_draw_form('copy_pers_off_'.$j, FILENAME_MASTER_ADMIN, 'script=copy_group_prices&action=copy_persoff_single')
                    .'<table>'
                        .'<tr>'
                            .'<td>'
                                .'<span class="turq">personal_offers_by_customers_status_</span>'.xtc_draw_input_field('get_pers_off_'.$j, $j, ' class="turq" size="1" readonly="readonly"')
                            .'</td>'
                            .'<td>'
                                .ADMIN_TOOLS_COPY_PERSOFF_TXT.' <span class="matt-green">personal_offers_by_customers_status_</span>'
                                //.xtc_draw_input_field('set_pers_off_'.$j, '', 'class="matt-green" size="10" placeholder="'.ADMIN_TOOLS_COPY_PERSOFF_INPUT_PLACEHOLDER.'"')
                                .xtc_draw_input_field('set_pers_off_'.$j, '', 'class="matt-green" size="10" '.($prefill_val != '' ? 'value="'.$prefill_val.'" onfocus="if(this.value==this.defaultValue) this.value=\'\';" onblur="if(this.value==\'\') this.value=this.defaultValue;"' : 'placeholder="'.ADMIN_TOOLS_COPY_PERSOFF_INPUT_PLACEHOLDER.'"'))
                            .'</td>'
                            .'<td>'
                                .'<button type="submit" onclick="this.blur();">'.BUTTON_COPY.'</button>'
                            .'</td>'
                        .'</tr>'
                    .'</table>'
                .'</form>';
            }
            ?>
                <div id="copy-persoff-single-out" class="hve-out"><?php echo $_SESSION[$_SESSION['ma_shopsess'].'persoff_single_out'] != '' ? $_SESSION[$_SESSION['ma_shopsess'].'persoff_single_out'] : ''; ?></div>
                <?php unset($_SESSION[$_SESSION['ma_shopsess'].'persoff_single_out']); ?>
            </div>
        </div>
    </div>
</div>
<?php
/*******************************************************************************************
* routine to reload browser while importing data from csv_import_aux into live tables
* file: /import_into_live_tables.php
* (c) noRiddle 12-2015
*******************************************************************************************/

class admin_tools_import {

    //function admin_tools_import() {
    function __construct($how_img) {
        global $shp_sfx;
        if(!isset($_GET['step'])) {
            $count_entries_sql = xtc_db_query("SELECT count(import_id) as total FROM csv_import_aux"); //why count(products_model) ?, we'll use PRIMARY KEY import_id
            $count_entries = xtc_db_fetch_array($count_entries_sql);
            $count_entries = $count_entries['total'];
            if($count_entries > 0) {
                if($count_entries >= IE_IMPORT_FACTOR) {
                    $counts = ceil($count_entries/IE_IMPORT_FACTOR); //IE_IMPORT_FACTOR found in table configuration, can be set in script
                    //$counts = 2; // for tests
                } else {
                    $counts = (int)1;
                }
            } else {
                $_SESSION[$_SESSION['ma_shopsess'].'out'] .= '<br /><span style="color:#c00;">'.ADMIN_TOOLS_NO_DATA_IN_AUX_TABLE.'</span>';
            }
            $_GET['counts'] = $counts; //put $counts into get var in order to no have to execute the select count() on each reload
            $_GET['imp_filename'] = $_SESSION[$_SESSION['ma_shopsess'].'nR_postfile'];
            unset($_SESSION[$_SESSION['ma_shopsess'].'nR_postfile']);
            
            $_SESSION[$_SESSION['ma_shopsess'].'price_updt_notinshop_file'] = 'price_update_not_in_shop'.date('d-m-Y_H:i:s').'_'.$shp_sfx.'.csv'; //define a file where to save products not found in shop while making a price update
        }
    
        $this->wait_between = 2;
        $this->counts = $_GET['counts'];
        $this->ie_personal_offers_array = $this->ie_personal_offers_array();
        //$this->get_group_perm_c = $this->get_group_perm(); //not needed, set per default in table categories
        $this->get_group_perm_p = $this->get_group_perm(false);
        $this->langs = $this->get_langs();
        $this->image_extension = '.jpg'; //new var, changed 09-2019
        $this->how_img = $how_img === true ? true : false;
        $this->debug = false;
    }

    //BOC functions
    function write_reload() {
        $langs = $this->langs;
        $imp_err = false;

        if($this->counts > 0) {
            $this->steps(0); //set first step
            
            for($i = 0; $i < $this->counts; $i++) {
                if(isset($_GET['step']) && $_GET['step'] == $i) {
                    $imp_start_time = microtime(true); //start time

                    if($i == 0) {
                        xtc_db_query("UPDATE csv_import_aux SET products_model = TRIM(products_model)"); //remove all white spaces first for faster comparison
                        //xtc_db_query("UPDATE csv_import_aux SET products_model = REGEX_REPLACE(products_model, '[^ -~]', '')"); //take off all invisible characters | doesn't work below mySQL 8.0
                        xtc_db_query("UPDATE ".TABLE_PRODUCTS." SET products_model = TRIM(products_model)"); //remove all white spaces first for faster comparison
                        //BOC empty all fields in csv_import_aux with default values in live DB
                        xtc_db_query("UPDATE csv_import_aux SET products_sort = '' WHERE products_sort = '0'");
                        xtc_db_query("UPDATE csv_import_aux SET products_startpage = '' WHERE products_startpage = '0'");
                        xtc_db_query("UPDATE csv_import_aux SET products_startpage_sort = '' WHERE products_startpage_sort = '0'");
                        //EOC empty all fields in csv_import_aux with default values in live DB
                        //BOC replace commas with points in float values, 10-2020, noRiddle
                        xtc_db_query("UPDATE csv_import_aux SET products_price = REPLACE(products_price, ',', '.')");
                        xtc_db_query("UPDATE csv_import_aux SET vpe_value = REPLACE(vpe_value, ',', '.')");
                        xtc_db_query("UPDATE csv_import_aux SET products_weight = REPLACE(products_weight, ',', '.')");
                        xtc_db_query("UPDATE csv_import_aux SET products_discount_allowed = REPLACE(products_discount_allowed, ',', '.')");
                        $persoff_tbls_qu_str = "UPDATE csv_import_aux SET ";
                        $persoff_tbls_arr = array();
                        foreach($this->ie_personal_offers_array as $po_tble) {
                            $persoff_tbls_arr[] = $po_tble." = REPLACE(".$po_tble.", ',', '.')";
                        }
                        $persoff_tbls_qu_str .= implode(', ', $persoff_tbls_arr);
                        xtc_db_query($persoff_tbls_qu_str);
                        //EOC replace commas with points in float values, 10-2020, noRiddle

                        /*if($this->counts > 1) {
                            xtc_db_query("ALTER TABLE products DROP INDEX idx_products_status, DROP INDEX idx_products_startpage, DROP INDEX idx_manufacturers_id, DROP INDEX idx_products_date_added");  //drop unnecessary indexes
                        }*/

                        $_SESSION[$_SESSION['ma_shopsess'].'tot_impprod'] = 0; //set session for total amount of imported products
                        $_SESSION[$_SESSION['ma_shopsess'].'tot_updprod'] = 0; //set session for total amount of updated products
                        $_SESSION[$_SESSION['ma_shopsess'].'price_updt_notfound'] = 0; //set session for total amount of not found products when price update
                        //$_SESSION[$_SESSION['ma_shopsess'].'miss_cat'] = 0; //Set session to count missing categories
                        if(isset($_SESSION[$_SESSION['ma_shopsess'].'iecat'])) unset($_SESSION[$_SESSION['ma_shopsess'].'iecat']);
                    }

                    //BOC let's try to speed this up, 05-2020, noRiddle
                    //$aux_table_sql = xtc_db_query("SELECT * FROM csv_import_aux FORCE INDEX FOR ORDER BY (PRIMARY) ORDER BY import_id ASC LIMIT ".($i * IE_IMPORT_FACTOR).", ".IE_IMPORT_FACTOR);
                    $imp_fact = $i * IE_IMPORT_FACTOR;
                    $aux_table_sql = xtc_db_query("SELECT * FROM csv_import_aux WHERE import_id > ".$imp_fact." ORDER BY import_id ASC LIMIT ".IE_IMPORT_FACTOR);
                    //EOC let's try to speed this up, 05-2020, noRiddle
                    
                    if($i == ($this->counts - 1)) $db_num_rows = xtc_db_num_rows($aux_table_sql); //store no. of results for last loop in var
                    
                    while($aux_table_data = xtc_db_fetch_array($aux_table_sql)) {
                        if(empty($aux_table_data['products_model'])) {
                            if(strpos($_SESSION[$_SESSION['ma_shopsess'].'language_code'], '_') !== false) {
                                $sess_lang_code_arr = explode('_', $_SESSION[$_SESSION['ma_shopsess'].'language_code']);
                                $sess_lang_code = $sess_lang_code_arr[0];
                            } else {
                                $sess_lang_code = $_SESSION[$_SESSION['ma_shopsess'].'language_code'];
                            }

                            $_SESSION[$_SESSION['ma_shopsess'].'out'] .= '<span style="color:#c00;">'.sprintf(ADMIN_TOOLS_NO_PROD_MODEL, $aux_table_data['products_name_'.$sess_lang_code]).'</span><br />';
                            $imp_err = true;
                        } else {
                            if(preg_match('#[^ -~]#', $aux_table_data['products_model'])) {
                                $aux_table_data['products_model'] = preg_replace('#[^ -~]#', '', $aux_table_data['products_model']);
                            } //take off all invisible characters (can't do it with REGEX_REPLACE now since we are on mySQL 5.6, see above)
                            $this->insert_product($aux_table_data, $aux_table_data['products_model']);
                        }
                    }
                    
                    //BOC time calculation
                    $imp_end_time = microtime(true);
                    $time_for_one_loop = ($imp_end_time - $imp_start_time);
                    
                    $_SESSION[$_SESSION['ma_shopsess'].'out'] .= sprintf(MOD_CSV_GEN_INFOS, $this->counts, $_GET['step']);
                    $_SESSION[$_SESSION['ma_shopsess'].'out'] .= '<span style="color:#0A7E00;">'.sprintf(MOD_CSV_TIME_ONE_LOOP_TXT, ($i == ($this->counts - 1) ? $db_num_rows : IE_IMPORT_FACTOR), number_format($time_for_one_loop, 4)).' sec.</span>';
                    if($i == ($this->counts - 1)) {
                        if($_SESSION[$_SESSION['ma_shopsess'].'tot_impprod'] > 0) {
                            $_SESSION[$_SESSION['ma_shopsess'].'out'] .= '<br /><span style="color:#0A7E00; font-weight:bold;">READY !! | '.sprintf(MOD_CSV_TOT_PRODUCTS_IMP_OR_UPD, $_SESSION[$_SESSION['ma_shopsess'].'tot_impprod'], $_SESSION[$_SESSION['ma_shopsess'].'tot_updprod']).'</span>';
                        }
                        if($_SESSION[$_SESSION['ma_shopsess'].'tot_updprod'] > 0) {
                            $_SESSION[$_SESSION['ma_shopsess'].'out'] .= '<br /><span style="color:#0A7E00; font-weight:bold;">READY !! | '.sprintf(MOD_CSV_PRICE_UPDT_NOT_FOUND, $_SESSION[$_SESSION['ma_shopsess'].'price_updt_notfound'], $_SESSION[$_SESSION['ma_shopsess'].'tot_updprod']).'</span>';
                            if($_SESSION[$_SESSION['ma_shopsess'].'price_updt_notfound'] > 0) {
                                $_SESSION[$_SESSION['ma_shopsess'].'out'] .= '<br /><span style="color:#c00;">Fuer die nicht im Shop gefundenen Artikel bitte File <i>/export/'.$_SESSION[$_SESSION['ma_shopsess'].'price_updt_notinshop_file'].'</i> runterladen</span>';
                                if($updt_nf_fp && is_resource($updt_nf_fp)) {
                                    fclose($updt_nf_fp); //close file if it is open
                                }
                            }
                        }

                        /*if($this->counts > 1) {
                            xtc_db_query("ALTER TABLE products ADD INDEX idx_products_status(products_status), 
                                                               ADD INDEX idx_products_startpage(products_startpage), 
                                                               ADD INDEX idx_manufacturers_id(manufacturers_id), 
                                                               ADD INDEX idx_products_date_added(products_date_added)");  //set indexes again
                        }*/

                        xtc_db_query("UPDATE categories SET products_sorting = 'p.products_id', products_sorting2 = 'ASC'"); //sorting should be by products_id, otherwise catagory calls are very slow, noRiddle
                    }
                    
                    $_SESSION[$_SESSION['ma_shopsess'].'tot_time'] += number_format(($time_for_one_loop + 3), 4); //added waiting time for reload
                    //EOC time calculation
                    
                    $_SESSION[$_SESSION['ma_shopsess'].'out'] .= '<br />--|-- TOTAL elapsed time: '.number_format($_SESSION[$_SESSION['ma_shopsess'].'tot_time'], 4).' sec. --|--';

                    if($imp_err !== true) {
                        if($_GET['step'] < ($this->counts - 1)) {$this->steps($i + 1);} //set next step
                    }
                    
                    //BOC unset set sessions
                    if($_GET['step'] == ($this->counts - 1)) {
                        unset($_SESSION[$_SESSION['ma_shopsess'].'tot_time']);
                        unset($_SESSION[$_SESSION['ma_shopsess'].'tot_impprod']);
                        unset($_SESSION[$_SESSION['ma_shopsess'].'tot_updprod']);
                        unset($_SESSION[$_SESSION['ma_shopsess'].'price_updt_notfound']);
                        //unset($_SESSION[$_SESSION['ma_shopsess'].'miss_cat']);
                        unset($_SESSION[$_SESSION['ma_shopsess'].'price_updt_notinshop_file']);
                        unset($_SESSION[$_SESSION['ma_shopsess'].'cnt_fields']);
                        if(isset($_SESSION[$_SESSION['ma_shopsess'].'iecat'])) unset($_SESSION[$_SESSION['ma_shopsess'].'iecat']);
                        
                        //delete DB cache if activated
                        if(defined('DB_CACHE') && DB_CACHE == 'true') {
                            clear_dir(SQL_CACHEDIR);
                        }
                    }
                    //EOC unset set sessions
                }
            }
        }
    }

    function insert_product($aux_arr, $p_model) {
        //global $shop;
        $langs = $this->langs;
        $sizeof_langs = sizeof($langs);
        
        //BOC check whether is only price update or general import/update
        $prc_upd = false;
        if($aux_arr['products_tax_class_id'] == ''  && $aux_arr['products_quantity'] == '') {
            $prc_upd = true;
        }
        //EOC check whether is only price update or general import/update
        
        //BOC check model early and store result in vars
        if($prdid = $this->check_model($p_model)) {
            $chm = true;
            //$prdid = $this->check_model($p_model);
            $_SESSION[$_SESSION['ma_shopsess'].'tot_updprod']++; //count updated products
        } else {
            $chm = false;
            if($prc_upd === false) { //don't count when price update and product not found in shop
                $_SESSION[$_SESSION['ma_shopsess'].'tot_impprod']++; //count imported products
            } else {
                $_SESSION[$_SESSION['ma_shopsess'].'price_updt_notfound']++;
            }
        }
        //EOC check model early and store result in vars

        $group_perm_arr_p = $this->get_group_perm_p;
        
        //BOC table products //replaced array_merge with direct definition of new array elements, 10-2020, noRiddle
        $prod_arr = array('products_model' => trim($aux_arr['products_model'])); // default Ersatzteil
        if($aux_arr['products_replace'] != '') {$prod_arr['products_replace'] = $aux_arr['products_replace'];}
		if($aux_arr['bf_prod_model'] != '') {$prod_arr['bf_prod_model'] = $aux_arr['bf_prod_model'];} //new for blackforest parts, changed 09-2019
        //if($aux_arr['set_by_admin'] != '') {$prod_arr['set_by_admin'] = $aux_arr['set_by_admin'];} //added new field set_by_admin //added 30.06.2016
        $prod_arr['set_by_admin'] = $aux_arr['set_by_admin']; //make overwrite with empty value possible for set_by_admin, 20-06-2019, noRiddle | !!!! IMPORTANT also for new gm_price_status synchronisation
        if(!empty($aux_arr['products_manufacturers_model'])) {
            $prod_arr['products_manufacturers_model'] = $aux_arr['products_manufacturers_model'];
        }
        if($aux_arr['products_supplier_id'] != '') {$prod_arr['products_supplier_id'] = $aux_arr['products_supplier_id'];}
        if(!empty($aux_arr['products_ean'])) {$prod_arr['products_ean'] = $aux_arr['products_ean'];}
        if($aux_arr['products_quantity'] != '') {$prod_arr['products_quantity'] = (int)$aux_arr['products_quantity'];}
        //BOC new fields for vpe
        if($aux_arr['vpe_status'] != '') {$prod_arr['products_vpe_status'] = (int)$aux_arr['vpe_status'];}
        if($aux_arr['vpe'] != '') {$prod_arr['products_vpe'] = (int)$aux_arr['vpe'];}
        if($aux_arr['vpe_value'] != '') {$prod_arr['products_vpe_value'] = (int)$aux_arr['vpe_value'];}
        //EOC new fields for vpe
        if($aux_arr['products_sort'] != '') {$prod_arr['products_sort'] = (int)$aux_arr['products_sort'];}
        if($aux_arr['products_startpage'] != '') {$prod_arr['products_startpage'] = (int)$aux_arr['products_startpage'];}
        if($aux_arr['products_startpage_sort'] != '') {$prod_arr['products_startpage_sort'] = (int)$aux_arr['products_startpage_sort'];}
        if($aux_arr['products_shippingtime'] != '') {$prod_arr['products_shippingtime'] = (int)$aux_arr['products_shippingtime'];} //default 1 setzen AH //default set in products
        //if(!empty($aux_arr['manufacturers_id'])) {$prod_arr['manufacturers_id'] = $this->get_manufacurers_id(trim($aux_arr['manufacturers_id']));} //we don't need this, 05-2020, noRiddle
        if($aux_arr['products_tax_class_id'] != '') {$prod_arr['products_tax_class_id'] = (int)$aux_arr['products_tax_class_id'];} // default 1 setzen AH //default set in products
        if($aux_arr['products_status'] != '') {$prod_arr['products_status'] = (int)$aux_arr['products_status'];}
        if($aux_arr['gm_price_status'] != '') {$prod_arr['gm_price_status'] = (int)$aux_arr['gm_price_status'];} // default 0 setzen AH //default set in products
        if($aux_arr['gm_min_order'] != '') {$prod_arr['gm_min_order'] = (int)$aux_arr['gm_min_order'];} // default 1 setzen AH //default set in products
        if($aux_arr['gm_graduated_qty'] != '') {$prod_arr['gm_graduated_qty'] = (int)$aux_arr['gm_graduated_qty'];} // default 1 setzen AH //default set in products
        if($aux_arr['products_weight'] != '' && $aux_arr['products_weight'] != '0.000') {$prod_arr['products_weight'] = (float)$aux_arr['products_weight'];}
        if($aux_arr['products_discount_allowed'] != '') {$prod_arr['products_discount_allowed'] = (float)$aux_arr['products_discount_allowed'];}
        if($aux_arr['products_price'] != '') {$prod_arr['products_price'] = (float)$aux_arr['products_price'];}// default 99999 setzen AH //default set in products
        if($aux_arr['products_dealer_price'] != '') {$prod_arr['products_dealer_price'] = (float)$aux_arr['products_dealer_price'];}
        if($aux_arr['products_bulk'] != '') {$prod_arr['products_bulk'] = (float)$aux_arr['products_bulk'];} //added products_bulk for bulk goods shipping
        if($aux_arr['products_sbk'] != '') {$prod_arr['products_sbk'] = (float)$aux_arr['products_sbk'];} //added products_sbk for "sonderbeschaffungskosten, 10-2019, noRiddle
        if($aux_arr['products_deposit'] != '') {$prod_arr['products_deposit'] = (float)$aux_arr['products_deposit'];} //added products_deposit
        //BOC we handle images differently from now on, we get just the number of images and use a pattern for the image names, changed 09-2019  | revoked 04-2021, noRiddle
        if($aux_arr['products_image'] != '') { //take image in CSV if available
            $prod_arr['products_image'] = $aux_arr['products_image'];
        } else { //build image name with model no. if checkbox mod_pim is set
            if(isset($_GET['imgmodl']) && $_GET['imgmodl'] == '1') {
                $prod_arr['products_image'] = $prod_arr['products_model'].'_0.jpg';
            }
        }
        /*if($aux_arr['products_image'] != '' && is_numeric($aux_arr['products_image']) && $aux_arr['products_image'] > 0) {
            $main_img_nme = $aux_arr['bf_prod_model'] != '' ? $aux_arr['bf_prod_model'].'_' : $aux_arr['products_model'].'_';
            $frst_img_nme = $main_img_nme.'0'.$this->image_extension;
            $prod_arr['products_image'] = $frst_img_nme;
        }*/ //we don't handle images for the time being, 10-2020, noRiddle
        //EOC we handle images differently from now on, we get just the number of images and use a pattern for the image names, changed 09-2019 | revoked 04-2021, noRiddle
        if($chm) {
            $prod_arr['products_last_modified'] = 'now()';
        } else {
            $prod_arr['products_date_added'] = 'now()';
            $prod_arr['products_last_modified'] = 'now()';
        }

        foreach($group_perm_arr_p as $gpk => $gpv) {
            if($aux_arr['group_permission_'.$gpk] != '') {
                $prod_arr[$gpv] = $aux_arr['group_permission_'.$gpk]; //should be changeable
            }
        }
        //EOC table products
        
        if($chm) {
            $products_id = $prdid;
            xtc_db_perform(TABLE_PRODUCTS, $prod_arr, 'update', 'products_id = '.(int)$products_id);
        } else {
            if($prc_upd === false) { //don't import when price update and product not found in shop
                xtc_db_perform(TABLE_PRODUCTS, $prod_arr);

                $products_id = xtc_db_insert_id();
            } else {
                //BOC write "not in shop models" in file in case of price update
                if($_SESSION[$_SESSION['ma_shopsess'].'price_updt_notfound'] > 0) {
                    if($_SESSION[$_SESSION['ma_shopsess'].'price_updt_notfound'] == 1) {
                        //BOC write header
                        $updt_nf_header_str = '';
                        $updt_nf_columns_query = xtc_db_query("SHOW COLUMNS FROM csv_import_aux");
                        $_SESSION[$_SESSION['ma_shopsess'].'cnt_fields'] = 0;
                        while($updt_nf_columns = xtc_db_fetch_array($updt_nf_columns_query)) {
                            if($updt_nf_columns['Field'] != 'import_id' && $updt_nf_columns['Field'] != 'products_id') { //added products_id
                                $_SESSION[$_SESSION['ma_shopsess'].'cnt_fields']++;
                                $updt_nf_header_str .= $updt_nf_columns['Field'].CSV_SEPERATOR;
                            }
                        }
                        $updt_nf_header_str = rtrim($updt_nf_header_str, CSV_SEPERATOR);

                        $updt_nf_data_str = trim($aux_arr['products_model']).CSV_SEPERATOR.(str_repeat(CSV_SEPERATOR, ($_SESSION[$_SESSION['ma_shopsess'].'cnt_fields'] - 1)));

                        $updt_nf_fp = fopen(DIR_FS_CATALOG.'export/'.$_SESSION[$_SESSION['ma_shopsess'].'price_updt_notinshop_file'], "w+");
                        fwrite($updt_nf_fp, $updt_nf_header_str."\r\n");
                        fwrite($updt_nf_fp, $updt_nf_data_str."\r\n");
                        fclose($updt_nf_fp);
                        //EOC write header
                    } else {
                        $updt_nf_data_str = trim($aux_arr['products_model']).CSV_SEPERATOR.(str_repeat(CSV_SEPERATOR, ($_SESSION[$_SESSION['ma_shopsess'].'cnt_fields'] - 1)));

                        $updt_nf_fp = fopen(DIR_FS_CATALOG.'export/'.$_SESSION[$_SESSION['ma_shopsess'].'price_updt_notinshop_file'], "a+");
                        fwrite($updt_nf_fp, $updt_nf_data_str."\r\n");
                        fclose($updt_nf_fp);
                    }
                }
                //EOC write "not in shop models" in file in case of price update
            }
        }
        
        //BOC products_description
        if($prc_upd === false) { //don't do anything when price update
            for($l = 0; $l < $sizeof_langs; $l++) {
                if(strpos($langs[$l]['code'], '_') !== false) {
                    $langs_code_arr = explode('_', $langs[$l]['code']);
                    $langs_l_code = $langs_code_arr[0];
                } else {
                    $langs_l_code = $langs[$l]['code'];
                }

                if($chm === false) { //second condition: don't import when price update and product not found in shop
                    $prod_desc_array = array('products_id' => $products_id, 'language_id' => $langs[$l]['id']);
                } else {
                    $prod_desc_array = array();
                }

                if($aux_arr['products_name_'.$langs_l_code] != '') {
                    $prod_desc_array['products_name'] = $aux_arr['products_name_'.$langs_l_code]; //products_name default Ersatzteil //default set in table products_descriptiom
                }
                if($aux_arr['products_description_'.$langs_l_code] != '') {
                    $prod_desc_array['products_description'] = $aux_arr['products_description_'.$langs_l_code];
                }
                if($aux_arr['products_short_description_'.$langs_l_code] != '') {
                    $prod_desc_array['products_short_description'] = $aux_arr['products_short_description_'.$langs_l_code];
                }

                if(!empty($prod_desc_array)) {
                    if($chm) {
                        xtc_db_perform(TABLE_PRODUCTS_DESCRIPTION, $prod_desc_array, 'update', 'products_id = '.$products_id.' AND language_id = '.$langs[$l]['id']);
                    } else {
                        xtc_db_perform(TABLE_PRODUCTS_DESCRIPTION, $prod_desc_array);
                    }
                }
            }
        }
        //EOC products_description
        
        //BOC products_attributes (for deposit), has to be already in all shops (tables: products_options, products_options_values, products_options_values_to_products_options)
        if($aux_arr['products_deposit'] != '' && $aux_arr['products_deposit'] > 0) {
            $attr_test_qu = xtc_db_query("SELECT products_attributes_id FROM ".TABLE_PRODUCTS_ATTRIBUTES." WHERE products_id = ".$products_id." AND options_id = 1");
                                    
            if(xtc_db_num_rows($attr_test_qu)) {
                $prod_attr_upd_array = array('options_id' => '1', //should be equal in all shops 
                                             'options_values_id' => '1', //should be equal in all shops
                                             'options_values_price' => $aux_arr['products_deposit'], 
                                             'price_prefix' => '+',
                                             'attributes_model' => 'DEPOSIT',
                                             'attributes_stock' => $aux_arr['products_quantity'],
                                             'sortorder' => '1'
                                            );

                xtc_db_perform(TABLE_PRODUCTS_ATTRIBUTES, $prod_attr_upd_array, 'update', 'products_id = '.$products_id);
            } else {  //no insert import if weight && qty empty
                if($prc_upd === false) { //don't import when price update and product not found in shop
                    $prod_attr_ins_array = array('products_id' => $products_id, 
                                                 'options_id' => '1', //should be equal in all shops 
                                                 'options_values_id' => '1', //should be equal in all shops
                                                 'options_values_price' => $aux_arr['products_deposit'], 
                                                 'price_prefix' => '+',
                                                 'attributes_model' => 'DEPOSIT',
                                                 'attributes_stock' => $aux_arr['products_quantity'],
                                                 'sortorder' => '1'
                                                );

                    xtc_db_perform(TABLE_PRODUCTS_ATTRIBUTES, $prod_attr_ins_array);
                }
            }
        }
        //EOC products_attributes (for deposit)
        
        //BOC personal offers
        foreach($this->ie_personal_offers_array as $po) { // Preis default 99999
            if($aux_arr[$po] != '') {
                if($chm || (!$chm && $prc_upd === false)) { //don't import when price update and product not found in shop
                    xtc_db_query("DELETE FROM ".$po." WHERE products_id = ".$products_id);

                    $prices = explode('::', $aux_arr[$po]);
                    for ($ii = 0, $cp = count($prices); $ii < $cp; $ii ++) {
                        $values = explode(':', $prices[$ii]);
                        $cv = count($values);
                        $group_array = array ('products_id' => $products_id,
                                              'quantity' => ($cv == 2 ? (int)$values[0] : '1'), //if quantity is not set, e.g.: 1:43.79
                                              'personal_offer' => ($cv == 2 ? (float)$values[1] : (float)$values[0]) //if quantity is not set, e.g.: 1:43.79
                                             );

                        xtc_db_perform($po, $group_array);
                    }
                }
            }
        }
        //EOC personal offers
        
        //BOC additional images
        //BOC we handle images differently from now on, we get just the number of images and use a pattern for the image names, changed 09-2019
        /*for($k = 1; $k <= IE_NUM_OF_ADDI_PICS; $k++) {
            if ($aux_arr['image_name_'.$k] != "") {
                if ($this->checkImage($k, $products_id)) {
                    $insert_array = array ('image_name' => $aux_arr['image_name_'.$k]);
                    xtc_db_perform(TABLE_PRODUCTS_IMAGES, $insert_array, 'update', 'products_id = \''.$products_id.'\' AND image_nr = \''.$k.'\'');
                } else {
                    if($prc_upd === false) { //don't import when price update and product not found in shop
                        $insert_array = array ('image_name' => $aux_arr['image_name_'.$k], 'image_nr' => $k, 'products_id' => $products_id);
                        xtc_db_perform(TABLE_PRODUCTS_IMAGES, $insert_array);
                    }
                }
            }
        }*/
        
        /*if($aux_arr['products_image'] != '' && is_numeric($aux_arr['products_image']) && $aux_arr['products_image'] > 1) {
            for($k = 1; $k < (int)$aux_arr['products_image']; $k++) {
                ${'img_nme'.$k} = $main_img_nme.$k.$this->image_extension;

                if ($this->checkImage($k, $products_id)) {
                    $insert_array = array ('image_name' => ${'img_nme'.$k});
                    xtc_db_perform(TABLE_PRODUCTS_IMAGES, $insert_array, 'update', 'products_id = \''.$products_id.'\' AND image_nr = \''.$k.'\'');
                } else {
                    if($prc_upd === false) { //don't import when price update and product not found in shop
                        $insert_array = array ('image_name' => ${'img_nme'.$k}, 'image_nr' => $k, 'products_id' => $products_id);
                        xtc_db_perform(TABLE_PRODUCTS_IMAGES, $insert_array);
                    }
                }
                
            }
        }*/ //we don't handle images for the time being, 10-2020, noRiddle
        //EOC we handle images differently from now on, we get just the number of images and use a pattern for the image names, changed 09-2019
        //EOC additional images
        
        //BOC cats
        if($prc_upd === false) {  //don't do anything when price update

//BOC debug, 19-05-2021, noRiddle
if($this->debug === true) {
    $debug_import_log = fopen(DIR_FS_CATALOG.'export/debug_import_'.date('d-m-Y').'.log', "a+");
    $imp_debug_str = $aux_arr['products_model'].':';
}
//EOC debug, 19-05-2021, noRiddle

            for($c = 1; $c <= IE_CAT_DEPTH; $c++) {
                if(strpos($langs[0]['code'], '_') !== false) {
                    $langs_Ocode_arr = explode('_', $langs[0]['code']);
                    $langs_Olc_code = $langs_Ocode_arr[0];
                } else {
                    $langs_Olc_code = $langs[0]['code'];
                }

                if(trim($aux_arr['cat_'.$c.'_'.$langs_Olc_code]) != '') {
                    if($c == 1) {
                        $parentid = 0;
                    } else {
                        $parentid = $haveparentid;
                    }

                    $check_cat = $this->check_cat($aux_arr['cat_'.$c.'_'.$langs_Olc_code], $langs[0]['id'], $parentid);
                    if($check_cat) {
                        $havecatid = $check_cat['catid'];
                        $haveparentid = $havecatid;
                    } else {
                        /*$cat_miss = false;

                        if($c != 1) {
                            if(trim($aux_arr['cat_'.($c - 1).'_'.$langs_Olc_code]) == '') {
                                $cat_miss = true;
                                $_SESSION[$_SESSION['ma_shopsess'].'miss_cat']++;
                            }
                        }*/

                        //if($cat_miss === false;) {
                            //BOC cat
                            $cat_arr = array('parent_id' => $parentid, 'categories_status' => 1, 'date_added' => 'now()', 'last_modified' => 'now()'); //date weg
                            //$categ_arr = array_merge($cat_arr, $group_perm_arr_c); //not needed, set per default in table categories
                            
                            //xtc_db_perform(TABLE_CATEGORIES, $categ_arr);
                            xtc_db_perform(TABLE_CATEGORIES, $cat_arr);

                            $havecatid = xtc_db_insert_id();
                            $haveparentid = $havecatid;
                            //EOC cat
                        
                            //BOC cat desc
                            for($lc = 0; $lc < $sizeof_langs; $lc++) {
                                if(strpos($langs[$lc]['code'], '_') !== false) {
                                    $langs_code_arr = explode('_', $langs[$lc]['code']);
                                    $langs_lc_code = $langs_code_arr[0];
                                } else {
                                    $langs_lc_code = $langs[$lc]['code'];
                                }

                                $cat_desc_arr = array('categories_id' => $havecatid, 'language_id' => $langs[$lc]['id'], 'categories_name' => trim($aux_arr['cat_'.$c.'_'.$langs_lc_code])); // 1. cat default Originalteil
                                
                                xtc_db_perform(TABLE_CATEGORIES_DESCRIPTION, $cat_desc_arr);
                                //echo '<pre>'.print_r($cat_desc_arr, true).'</pre>';
                            }
                            //EOC cat desc
                        //}
                    }
                } else { //fallback in case first categorie is empty in CSV, 08-2018
                    if($c == 1) {
                        $cat_arr = array('parent_id' => 0, 'categories_status' => 1, 'date_added' => 'now()', 'last_modified' => 'now()');
                        xtc_db_perform(TABLE_CATEGORIES, $cat_arr);
                        $havecatid = xtc_db_insert_id();

                        for($lc = 0; $lc < $sizeof_langs; $lc++) {
                            if(strpos($langs[$lc]['code'], '_') !== false) {
                                $langs_code_arr = explode('_', $langs[$lc]['code']);
                                $langs_lc_code = $langs_code_arr[0];
                            } else {
                                $langs_lc_code = $langs[$lc]['code'];
                            }

                            $cat_desc_arr = array('categories_id' => $havecatid, 'language_id' => $langs[$lc]['id'], 'categories_name' => ($langs_lc_code == 'de' ? 'Diverses' : 'Misc.'));
                            xtc_db_perform(TABLE_CATEGORIES_DESCRIPTION, $cat_desc_arr);
                        }
                    }
                }

                //BOC debug, 19-05-2021, noRiddle
                if($this->debug === true) {
                    if($aux_arr['cat_'.$c.'_'.$langs_Olc_code] != '') {
                        $imp_debug_str .= ' => "'.$aux_arr['cat_'.$c.'_'.$langs_Olc_code].'"'.(' $check_cat='.($check_cat ? $check_cat['catid'] : 'false')).' $parentid='.$parentid.' | $havecatid='.$havecatid.' | $haveparentid='.$haveparentid;
                    }
                }
                //EOC debug, 19-05-2021, noRiddle

                //BOC poducts to categories
                if($c == IE_CAT_DEPTH) {
                    if(isset($havecatid) && $havecatid != '' && $havecatid != '0') {
                        $this->intoProdToCat($products_id, $havecatid);
                    }
                }
                //EOC poducts to categories
            }

//BOC debug, 19-05-2021, noRiddle
if($this->debug === true) {
    fwrite($debug_import_log, $imp_debug_str."\r\n");
    fclose($debug_import_log);
}
//EOC debug, 19-05-2021, noRiddle

        }
        //EOC cats
    }

    function get_langs() {
        $languages_array = array();
        $languages_query = xtc_db_query("SELECT languages_id, code FROM ".TABLE_LANGUAGES." WHERE status = 1 ORDER BY ie_sort_order");
        while ($languages = xtc_db_fetch_array($languages_query)) {
            $languages_array[] = array('id' => $languages['languages_id'], 'code' => $languages['code']);
        }
        return $languages_array;
    }
    
    function ie_personal_offers_array() {
        $pers_off_col_query = xtc_db_query("SHOW COLUMNS FROM csv_import_aux LIKE 'personal_offers_by_customers_status_%'");
        while($pers_off_cols = xtc_db_fetch_array($pers_off_col_query)) {
            $ie_personal_offers_array[] = $pers_off_cols['Field'];
        }
        return $ie_personal_offers_array;
    }
    
    function get_group_perm($cat = true) {
        $group_perm_arr = array();
        $group_perm_query = xtc_db_query("SHOW COLUMNS FROM csv_import_aux LIKE 'group_permission_%'");
        while($group_perms = xtc_db_fetch_array($group_perm_query)) {
            if($cat) {
                //$group_perm_arr = array_merge($group_perm_arr, array($group_perms['Field'] => '1')); //not needed, set per default in table categories
            } else {
                $group_perm_arr[] = $group_perms['Field'];
            }
        }
        return $group_perm_arr;
    }
    
    function check_model($prod_model) {
        $model_query = xtc_db_query("SELECT products_id FROM ".TABLE_PRODUCTS." WHERE products_model = '".$prod_model."'");
        //$model_query = xtc_db_query("SELECT products_id FROM ".TABLE_PRODUCTS." WHERE REPLACE(products_model,' ','') = '".trim($prod_model)."'"); //take off spaces (not needed any more, was too slow anyway)
        if(!xtc_db_num_rows($model_query)) {
            $productid = false;
        } else {
            $prod_id = xtc_db_fetch_array($model_query);
            $productid = $prod_id['products_id'];
        }
        return $productid;
    }

    function checkImage($imgnr, $pID) {
        $img_query = xtc_db_query("SELECT image_id FROM ".TABLE_PRODUCTS_IMAGES." WHERE products_id = ".(int)$pID." AND image_nr = ".(int)$imgnr);
        if (!xtc_db_num_rows($img_query)) {
            return false;
        } else {
            return true;
        }
    }

    function get_manufacurers_id($manf_name) {
        $langs = $this->langs;
        $sizeof_langs = sizeof($langs);
        //reworked to use session since often consecutive manufacturers are the same in csv_import_aux table (csv), 05-2020, noRiddle
        if(!isset($_SESSION[$_SESSION['ma_shopsess'].'ieman'])) $_SESSION[$_SESSION['ma_shopsess'].'ieman'] = array();

        if($manf_name == '') {
            return;
        } else {
            if(!isset($_SESSION[$_SESSION['ma_shopsess'].'ieman'][$manf_name])) {
                $mfn_query = xtc_db_query("SELECT manufacturers_id FROM ".TABLE_MANUFACTURERS." WHERE manufacturers_name = '".xtc_db_input($manf_name)."'");
                if(!xtc_db_num_rows($mfn_query)) {
                    $manu_arr = array('manufacturers_name' => $manf_name);
                    xtc_db_perform(TABLE_MANUFACTURERS, $manu_arr);
                    $man_id = xtc_db_insert_id();
                    for($lm = 0; $lm < $sizeof_langs; $lm++) {
                        $manu_inf_arr = array('manufacturers_id' => $man_id, 'languages_id' => $langs[$lm]['id']);
                        xtc_db_perform(TABLE_MANUFACTURERS_INFO, $manu_inf_arr);
                    }
                    $_SESSION[$_SESSION['ma_shopsess'].'ieman'][$manf_name] = $man_id;
                } else {
                    $mfn = xtc_db_fetch_array($mfn_query);
                    //$man_id = $mfn['manufacturers_id'];
                    $_SESSION[$_SESSION['ma_shopsess'].'ieman'][$manf_name] = $mfn['manufacturers_id'];
                }
            }
            //return $man_id;
            return $_SESSION[$_SESSION['ma_shopsess'].'ieman'][$manf_name];
        }
    }

    function check_cat($catname, $langid, $parid) {
        //reworked to use session since often consecutive categories are the same in csv_import_aux table (csv), 05-2020, noRiddle
        if(!isset($_SESSION[$_SESSION['ma_shopsess'].'iecat'])) $_SESSION[$_SESSION['ma_shopsess'].'iecat'] = array();

        if(!isset($_SESSION[$_SESSION['ma_shopsess'].'iecat'][$catname][$langid][$parid]) || (isset($_SESSION[$_SESSION['ma_shopsess'].'iecat'][$catname][$langid][$parid]) && $_SESSION[$_SESSION['ma_shopsess'].'iecat'][$catname][$langid][$parid] === false)) {
            $cat_query = xtc_db_query("SELECT c.categories_id
                                         FROM ".TABLE_CATEGORIES." c
                                         JOIN ".TABLE_CATEGORIES_DESCRIPTION." cd
                                           ON cd.categories_id = c.categories_id
                                        WHERE cd.categories_name = '".xtc_db_input($catname)."'
                                          AND cd.language_id = ".(int)$langid."
                                          AND c.parent_id = ".(int)$parid);
            if(!xtc_db_num_rows($cat_query)) {
                //$catid = false;
                $_SESSION[$_SESSION['ma_shopsess'].'iecat'][$catname][$langid][$parid] = false;
            } else {
                $catid_result = xtc_db_fetch_array($cat_query);
                //$catid = array('catid' => $catid_result['categories_id']);
                $_SESSION[$_SESSION['ma_shopsess'].'iecat'][$catname][$langid][$parid] = array('catid' => $catid_result['categories_id']);
            }
        }

        //return $catid;
        return $_SESSION[$_SESSION['ma_shopsess'].'iecat'][$catname][$langid][$parid];
    }

    function intoProdToCat($pid, $cid) {
        $prodToCat_query = xtc_db_query("SELECT * FROM ".TABLE_PRODUCTS_TO_CATEGORIES." WHERE products_id = ".(int)$pid." AND categories_id = ".(int)$cid);
        if(!xtc_db_num_rows($prodToCat_query)) {
            $ptoc_data_arr = array ('products_id' => $pid, 'categories_id' => $cid);
            xtc_db_perform(TABLE_PRODUCTS_TO_CATEGORIES, $ptoc_data_arr);
        }
    }

    function steps($step) {
        if(!isset($_GET['step']) || (isset($_GET['step']) && $_GET['step'] != $step && $_GET['step'] < $step)) { // 
            $how_img = ((isset($this->how_img) && $this->how_img === true) || isset($_GET['imgmodl']) && $_GET['imgmodl'] == '1');
            //echo '<meta http-equiv="refresh" content="3; URL='.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=import&imp_filename='.$_GET['imp_filename'].'&counts='.$this->counts.'&step='.$step).'">';
            $this->ie_meta_out = '<meta http-equiv="refresh" content="'.$this->wait_between.'; URL='.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=import&imp_filename='.$_GET['imp_filename'].($how_img ? '&imgmodl=1' : '').'&counts='.$this->counts.'&step='.$step).'">';
        } else {
            $this->ie_meta_out = false;
        }
        
        return $this->ie_meta_out;
    }
    //EOC functions

} //end class
?>
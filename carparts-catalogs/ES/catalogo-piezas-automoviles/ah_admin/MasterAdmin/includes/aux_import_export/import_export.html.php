<?php
/*******************************************************************************************
* file: /import_export.html.php
* (c) noRiddle 12-2015
*******************************************************************************************/
?>
<div class="script-code">
    <h2><?php echo ADMIN_TOOLS_NEW_IMPEXP_SCRIPT_TITLE.'<br /><span class="sml">'.ADMIN_TOOLS_NEW_IMPEXP_SCRIPT_DESC.'</span>'; //echo ADMIN_TOOLS_SCRIPT_HEADING; ?></h2>
    <div class="div-marg">
        <div class="bord">
            <h4 id="tog-set-funcs" class="gold"><?php echo ADMIN_TOOLS_IMPORT_SET_FUNCS; ?></h4>
            <?php
                $config_query = xtc_db_query("SELECT configuration_key,
                                                     configuration_id,
                                                     configuration_value,
                                                     use_function,
                                                     set_function
                                                FROM " . TABLE_CONFIGURATION . "
                                               WHERE configuration_group_id = '20'
                                            ORDER BY sort_order");
                

                echo '<div id="set-funcs">';
                //echo '<form name="configuration" action="'.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=save', 'NONSSL' , false).'" method="post">';
                echo xtc_draw_form('configuration', FILENAME_MASTER_ADMIN, 'script=import_export&action=save');
                echo '<table>';
                while($config_vals = xtc_db_fetch_array($config_query)) {
                    //echo '<pre>'.print_r($config_vals).'</pre>';
                    if($config_vals['configuration_key'] != 'COMPRESS_EXPORT' && $config_vals['configuration_key'] != 'CSV_CATEGORY_DEFAULT' && $config_vals['configuration_key'] != 'CSV_CAT_DEPTH') {
                        $field = '<tr>';
                        $field .= '<td>'.constant(strtoupper($config_vals['configuration_key'].'_TITLE')).'</td>';
                        $field .= '<td>'.xtc_draw_input_field($config_vals['configuration_key'], $config_vals['configuration_value'], 'size="10"').'</td>';
                        $field .= '<td>'.constant(strtoupper( $config_vals['configuration_key'].'_DESC')).'</td>';
                        $field .= '</tr>';
                        echo $field;
                    }
                }
                //echo '<tr colspan="3"><td><input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_SAVE . '"/></td></tr>';
                echo '<tr colspan="3"><td><button type="submit" onclick="this.blur();">'.BUTTON_SAVE.'</button></td></tr>';
                echo '</table>';
                echo '</form></div>';
            ?>
        </div>
    </div>
    <table class="table-impexp">
        <tr>
            <td>
                <h3>IMPORT <span class="fs10"><?php echo ADMIN_TOOLS_LOAD_PER_FTP; ?></span></h3>
                <div class="div-marg">
                    <div class="bord">
            <?php
                        //echo '<form name="upload" id="upload" action="'.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=upload', 'NONSSL' , false).'" method="post" enctype="multipart/form-data">';
                        echo xtc_draw_form('upload', FILENAME_MASTER_ADMIN, 'script=import_export&action=upload', 'POST', 'enctype="multipart/form-data" id="upload"');
                        echo '<label for="file_upload">'.ADMIN_TOOLS_FILE_UPLOAD_TXT.'</label>'.xtc_draw_file_field('file_upload', '', 'id="file_upload"');
                        //echo '<input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_UPLOAD . '"/>';
                        echo '<button type="submit" onclick="this.blur();">'.BUTTON_UPLOAD.'</button>';
            ?>
                        </form>
                        <div id="upload-msg"><?php echo $upl_return_message != '' ? $upl_return_message : ''; ?></div>
                    </div>
                </div>
                <div class="div-marg">
                    <div class="bord">
            <?php
                        //echo '<form name="import" action="'.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=import', 'NONSSL' , false).'" method="post" enctype="multipart/form-data">';
                        echo xtc_draw_form('import', FILENAME_MASTER_ADMIN, 'script=import_export&action=import', 'POST', 'enctype="multipart/form-data"');
                        //BOC new options
                          echo '<h4 id="tog-aux-funcs" class="gold">'.ADMIN_TOOLS_IMPORT_AUX_FUNCS.'</h4>';
                          echo '<div id="aux-funcs">';
                          echo '<p><input type="checkbox" id="mod-process-imgs-model" class="mod-aux-act" name="mod_pim" value="1" /> <label for="mod-process-imgs-model">' . MOD_PIM . '</label></p>'; //checkbox to process images as model.jpg, 04-2021, noRiddle
                          echo '<p><input type="checkbox" id="mod-checkall-auxact" name="mod_ceckall_auxact" /> <label for="mod-checkall-auxact">' . MOD_CHK_ALL . '</label></p>'
                               .'<p><input type="checkbox" id="mod_delete_products" class="mod-aux-act" name="mod_delete_products" value="1" /> <label for="mod_delete_products">' . MOD_CSV_DELETE_PRODUCTS . '</label></p>'
                               .'<p><input type="checkbox" id="mod_delete_categories" class="mod-aux-act" name="mod_delete_categories" value="1" /> <label for="mod_delete_categories">' . MOD_CSV_DELETE_CATEGORIES . '</label></p>'
                               .'<p><input type="checkbox" id="mod_delete_specials" class="mod-aux-act" name="mod_delete_specials" value="1" /> <label for="mod_delete_specials">' . MOD_CSV_DELETE_SPECIALS . '</label></p>'
                               //.'<p><input type="checkbox" id ="mod_delete_images" class="mod-aux-act" name="mod_delete_images" value="1" /> <label for="mod_delete_images">' . MOD_CSV_DELETE_IMAGES . '</label></p>'
                               .'<p><input type="checkbox" id="mod_delete_manufacturers" class="mod-aux-act" name="mod_delete_manufacturers" value="1" /> <label for="mod_delete_manufacturers">' . MOD_CSV_DELETE_MANUFACTURERS . '</label></p>'
                               .'<p><input type="checkbox" id="mod_delete_reviews" class="mod-aux-act" name="mod_delete_reviews" value="1" /> <label for="mod_delete_reviews">' . MOD_CSV_DELETE_REVIEWS . '</label></p>'
                               .'<p><input type="checkbox" id="" class="mod-aux-act" name="mod_delete_attributes" value="1" /> <label for="mod_delete_attributes">' . MOD_CSV_DELETE_ATTRIBUTES . '</label></p>'
                               .'<p><input type="checkbox" id="" class="mod-aux-act" name="mod_delete_xsell" value="1" /> <label for="mod_delete_xsell">' . MOD_CSV_DELETE_XSELL . '</label></p>';
                        echo '</div>';
                        //EOC new options
                        echo '<div class="marg-t30">';
                        echo '<label for="select_file">'.ADMIN_TOOLS_SELECT_FILE_TXT.'</label>'.xtc_draw_pull_down_menu('select_file', $files, '', 'id="select_file"');
                        //echo '<input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_IMPORT . '"/>';
                        echo '<button type="submit" onclick="this.blur();">'.BUTTON_IMPORT.'</button>';
                        echo '</div></form>';
            ?>
                        <?php $imp_file = isset($_GET['imp_filename']) ? '<span style="color:#000;">File: '.$_GET['imp_filename'].'</span><br />' : ''; ?>
                        <div id="imp-out"><?php echo $imp_file.($_SESSION[$_SESSION['ma_shopsess'].'out'] != '' ? $_SESSION[$_SESSION['ma_shopsess'].'out'] : ''); ?></div>
                        <?php
                        unset($_SESSION[$_SESSION['ma_shopsess'].'out']);
                        ?>
                    </div>
                </div>
                <hr />
                <h3>IMPORT SONDERANGEBOTE</h3>
                <div class="div-marg">
                    <div class="bord">
            <?php
                        //echo '<form name="import_specials" action="'.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=import_specials', 'NONSSL' , false).'" method="post" enctype="multipart/form-data">';
                        echo xtc_draw_form('import_specials', FILENAME_MASTER_ADMIN, 'script=import_export&action=import_specials', 'POST', 'enctype="multipart/form-data"');
                        echo '<div class="">';
                        echo '<label for="select_file_specials">'.ADMIN_TOOLS_SELECT_FILE_TXT.'</label>'.xtc_draw_pull_down_menu('select_file_specials', $files_specials, '', 'id="select_file_specials"');
                        //echo '<input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_IMPORT . '"/>';
                        echo '<button type="submit" onclick="this.blur();">'.BUTTON_IMPORT.'</button>';
                        echo '</div></form>';
            ?>
                        <?php $imp_spec_file = isset($post_select_spec_file) ? '<span style="color:#000;">File: '.$post_select_spec_file.'</span><br />' : ''; ?>
                        <div id="imp-spec-out"><?php echo $imp_spec_file.($spec_tot != '' ? $spec_tot : ''); ?></div>
                    </div>
                </div>
            </td>
    <!--<hr />-->
            <td>
                <h3>EXPORT <span class="fs10"><?php //echo ADMIN_TOOLS_EXPORT_INDEXES_TXT; ?></span></h3>
                <div class="div-marg">
                    <div class="bord">
            <?php
                        //echo '<form name="export" action="'.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=export', 'NONSSL' , false).'" method="post" enctype="multipart/form-data">';
                        echo xtc_draw_form('export', FILENAME_MASTER_ADMIN, 'script=import_export&action=export', 'POST', 'enctype="multipart/form-data"');
                        $exp_content = array();
                        $exp_content[] = array('id' => 'products', 'text' => ADMIN_TOOLS_TEXT_PRODUCTS);
                        $exp_content[] = array('id' => 'startpage', 'text' => ADMIN_TOOLS_TEXT_STARTPAGE);
                        $exp_content[] = array('id' => 'gm_price_status_normal', 'text' => ADMIN_TOOLS_TEXT_NORMAL);
                        $exp_content[] = array('id' => 'gm_price_status_onrequ', 'text' => ADMIN_TOOLS_TEXT_ON_REQUEST);
                        $exp_content[] = array('id' => 'gm_price_status_np', 'text' => ADMIN_TOOLS_TEXT_NON_PURCHASABLE);
                        $exp_content[] = array('id' => 'set_by_admin', 'text' => ADMIN_TOOLS_TEXT_SET_BY_ADMIN); //added export filter for new field set_by_admin, noRiddle //added 30.06.2016
                        $exp_content[] = array('id' => 'products_sbk', 'text' => ADMIN_TOOLS_TEXT_SBK); //added 05-2020, noRiddle
                        if($country_envir == 'bf') {
                            $exp_content[] = array('id' => 'blackforest', 'text' => ADMIN_TOOLS_TEXT_BLACKFOREST);
                        }  //added 09-2019
                        echo '<label for="select_content">'.ADMIN_TOOLS_SELECT_EXPORT_TXT.'</label>'.xtc_draw_pull_down_menu('select_content', $exp_content, 'products', 'id="select_content"');
                        //echo '<input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_EXPORT . '"/>';
                        echo '<button type="submit" onclick="this.blur();">'.BUTTON_EXPORT.'</button>';
                        echo '</form>';
            ?>
                        <?php $exp_file = isset($_GET['filename']) ? '<span style="color:#000;">File: '.$_GET['filename'].'</span><br />' : ''; ?>
                        <div id="exp-out"><?php echo $exp_file.($_SESSION[$_SESSION['ma_shopsess'].'expout'] != '' ? $_SESSION[$_SESSION['ma_shopsess'].'expout'] : ''); ?></div>
                        <?php unset($_SESSION[$_SESSION['ma_shopsess'].'expout']); ?>
                    </div>
                </div>
                <hr />
                <h3>EXPORT SONDERANGEBOTE</h3>
                <div class="div-marg">
                    <div class="bord">
            <?php
                        //echo '<form name="export_specials" action="'.xtc_href_link(FILENAME_MASTER_ADMIN, 'script=import_export&action=export_specials', 'NONSSL' , false).'" method="post" enctype="multipart/form-data">';
                        echo xtc_draw_form('export_specials', FILENAME_MASTER_ADMIN, 'script=import_export&action=export_specials', 'POST', 'enctype="multipart/form-data"');
                        echo '<div class="">';
                        echo '<button type="submit" onclick="this.blur();">'.BUTTON_EXPORT.'</button>';
                        echo '</div></form>';
            ?>
                        <div id="exp-spec-out"><?php echo ($spec_tot_exp != '' ? $spec_tot_exp : ''); ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<?php
/*******************************************
* file: add_select_products.php
* use: add products_icons to default select
* (c) noRiddle 12-2016
*******************************************/

//gm_xx_status
$add_select_product[] = 'p.gm_price_status';
$add_select_product[] = 'p.gm_min_order';
$add_select_product[] = 'p.gm_graduated_qty';

//products_bulk (bulk goods price
$add_select_product[] = 'p.products_bulk';

//$add_where_search = $add_select_product;

$add_select_default = $add_select_search = $add_select_cart = $add_select_product;
//$add_select_default = $add_select_search = $add_select_product;
?>
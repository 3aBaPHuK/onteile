<?php
/* -----------------------------------------------------------------------------------------
   $Id: ot_coupon.php 2096 2011-08-15 15:42:57Z Tomcraft1980 $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(t_coupon.php,v 1.1.2.2 2003/05/15); www.oscommerce.com
   (c) 2006 XT-Commerce (ot_coupon.php 899 2005-04-29)

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:

   Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
   http://www.oscommerce.com/community/contributions,282
   Copyright (c) Strider | Strider@oscworks.com
   Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
   Copyright (c) Andre ambidex@gmx.net
   Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_ORDER_TOTAL_COUPON_TITLE', 'Cupones de descuento');
define('MODULE_ORDER_TOTAL_COUPON_HEADER', 'Cupones de regalo / descuento ');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION', 'Cupón de descuento');
define('SHIPPING_NOT_INCLUDED', ' [Envío no incluido]');
define('TAX_NOT_INCLUDED', ' [sin IVA incluido]');
define('MODULE_ORDER_TOTAL_COUPON_USER_PROMPT', '');
define('ERROR_NO_INVALID_REDEEM_COUPON', 'Código del cupón no válido');
  //BOF - DokuMan - 2010-08-31 - constants already defined in german.php
  //define('ERROR_INVALID_STARTDATE_COUPON', 'Dieser Gutschein ist noch nicht verf&uuml;gbar');
  //define('ERROR_INVALID_FINISDATE_COUPON', 'Dieser Gutschein ist nicht mehr g&uuml;ltig');
  //define('ERROR_INVALID_USES_COUPON', 'Dieser Gutschein kann nur ');
  //define('TIMES', ' mal benutzt werden.');
  //define('ERROR_INVALID_USES_USER_COUPON', 'Die maximale Nutzung dieses Gutscheines wurde erreicht.');
  //define('REDEEMED_COUPON', 'ein Gutschein &uuml;ber ');
  //EOF - DokuMan - 2010-08-31 - constants already defined in german.php
define('REDEEMED_MIN_ORDER', 'para mercancías por un valor de ');
define('REDEEMED_RESTRICTIONS', ' [Artículos / Limitación de categorías]');
define('TEXT_ENTER_COUPON_CODE', 'Por favor introduzca aquí el código del cupón ');
  
define('MODULE_ORDER_TOTAL_COUPON_STATUS_TITLE', 'Mostrar valor');
define('MODULE_ORDER_TOTAL_COUPON_STATUS_DESC', '¿Quiere mostrar el valor del cupón de descuento?');
define('MODULE_ORDER_TOTAL_COUPON_SORT_ORDER_TITLE', 'Orden de clasificación');
define('MODULE_ORDER_TOTAL_COUPON_SORT_ORDER_DESC', 'Orden de visualización');
define('MODULE_ORDER_TOTAL_COUPON_INC_SHIPPING_TITLE', 'Incluidos costos de envío');
define('MODULE_ORDER_TOTAL_COUPON_INC_SHIPPING_DESC', 'Adicionar los costos de envío al valor de la mercancía');
define('MODULE_ORDER_TOTAL_COUPON_INC_TAX_TITLE', 'IVA incluido');
define('MODULE_ORDER_TOTAL_COUPON_INC_TAX_DESC', 'Adicionar IVA al valor de la mercancía');
define('MODULE_ORDER_TOTAL_COUPON_CALC_TAX_TITLE', 'Calcular el IVA de nuevo');
define('MODULE_ORDER_TOTAL_COUPON_CALC_TAX_DESC', 'Calcular el IVA de nuevo');
define('MODULE_ORDER_TOTAL_COUPON_TAX_CLASS_TITLE', 'Tasa de IVA');
define('MODULE_ORDER_TOTAL_COUPON_TAX_CLASS_DESC', 'Usar la siguiente tasa de IVA en caso de utilizar el cupón de descuento como abono.');
  //BOF - web28 - 2010-06-20 - no discount for special offers
define('MODULE_ORDER_TOTAL_COUPON_SPECIAL_PRICES_TITLE', 'Descuento en ofertas especiales');
define('MODULE_ORDER_TOTAL_COUPON_SPECIAL_PRICES_DESC', 'Permitir descuentos en ofertas especiales');
  //EOF - web28 - 2010-06-20 - no discount for special offers
?>

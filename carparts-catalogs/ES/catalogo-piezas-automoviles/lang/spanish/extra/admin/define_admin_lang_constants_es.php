<?php
/*************************************************
* file: define_admin_lang_constants_de.php
* use: define additional lang constants
* (c) noRiddle 12-2016 - 01-2017
*************************************************/

//BOC favorites top menu
define('NR_MASTER_ADMIN_FAV_ALT_TXT', 'MasterAdminTools');
//BOC favorites top menu

//BOC order_status_comments
define('TEXT_ORDER_STATUS_COMMENTS_DROPDOWN', 'Plantilla de comentarios');
define('TXT_ORDER_STATUS_COMMENTS_CHOOSE', 'por favor seleccione');
//EOC order_status_comments

//BOC edit gm_price_status
define('NR_EDITPROD_PLEASE_CHOOSE', 'por favor seleccione');
define('NR_EDITPROD_PROD_NOTAVAIL', 'Pieza se suprime');
define('NR_EDITPROD_PROD_WILL_REPLACED', 'Pieza será reemplazada');
define('NR_EDITPROD_HEADING', '¿El artículo %s se reemplazará o se suprime?');
define('NR_EDITPROD_HEADING_COMMENT', '(Por favor, con cuidado, no se puede deshacer desde aquí.)');
define('NR_EDITPROD_REPL_MODEL_TXT', 'Dado que el artículo será reemplazado, introduzca aquí el número de artículo del artículo que lo reemplaza <strong>sin espacios y caracteres especiales</strong> ');
define('NR_EDITPROD_BUTTON', 'Actualizar artículo');
define('NR_EDITPROD_ALERT_NO_SPECIALCHARS', 'Por favor, introduzca el nuevo número de artículo (sin espacios ni caracteres especiales!!)<br />o seleccione "Pieza se suprime" .');
define('NR_EDITPROD_ALERT_REPLPART_NO_EXISTS', 'La pieza de repuesto especificada no existe en la tienda.<br />Por favor, compruebe utilizando la función de búsqueda en la parte delantera de la tienda y cree un artículo si es necesario.');
define('NR_EDITPROD_EDIT_SUCCESS', 'OKAY: Artículo con número de artículo %s ha sido cambiado con éxito.');
define('NR_EDITPRODPRICE_HEADING', 'Artículo %s: ¿Cambiar precio de venta recomendado? )clic para abrir)');
define('NR_EDITPRODPRICE_HEADING_COMMENT', '(Por favor, introduzca el nuevo precio de venta recomendado en <u>neto</u> y con punto como coma)');
define('NR_EDITPRODPRICE_TXT', 'Nuevo precio de venta recomendado');
define('NR_EDITPRODPRICE_EDIT_SUCCESS', 'OKAY: Precio del artículo con el número de artículo %s ha sido cambiado con éxito.');
//EOC edit gm_price_status

//BOC new buttons for admin access
define('ADMACC_BUTTON_SET_ALL', 'Establecer todos derechos');
define('ADMACC_BUTTON_UNSET_ALL', 'Revocar todos derechos');
define('ADMACC_BUTTON_SET_SW', 'Derechos almacenistas');
define('ADMACC_BUTTON_SET_TRANSL', 'Derechos traductores');
define('ADMACC_BUTTON_SET_MSW', 'Derechos almacenista principal');
define('ADMACC_BUTTON_SET_SEO', 'Derechos agente SEO');
//BOC new buttons for admin access

//BOC gm_price_status, noRiddle
define('TEXT_PRODUCTS_PS_NORMAL', 'normal');
define('TEXT_PRODUCTS_PS_ONREQUEST', 'Precio a consultar');
define('TEXT_PRODUCTS_PS_NOPURCHASE', 'no disponible');
define('TEXT_PRODUCTS_GM_PRICE_STATUS', 'Estado del precio:');
//EOC gm_price_status, noRiddle

//BOC bulk costs, noRiddle
define('TEXT_PRODUCTS_BULK', 'Costos de mercancías voluminosas:');
//EOC bulk costs, noRiddle

//BOC inclusion for affiliate program, noRiddle
require(DIR_FS_LANGUAGES.'german/admin/'.'affiliate_configuration.php');
require(DIR_FS_LANGUAGES.'german/admin/'.'affiliate_german.php');
//EOC inclusion for affiliate program, noRiddle

//BOC heading for staff who edited order, noRiddle
define('TABLE_HEADING_CUSTOMER_STAFF', 'Empleado');
//EOC heading for staff who edited order, noRiddle

//BOC new contant for total shipping weight in order
define('AH_ORDER_TOT_WEIGHT', 'Peso de envío:');
//EOC new contant for total shipping weight in order

//BOC multi_order_status based on module by XTC-DELUXE.DE, noRiddle
define('TABLE_HEADING_EDIT', 'Editar todo');
define('HEADING_MULTI_ORDER_STATUS', 'Para todos los pedidos marcados:');
define('BUTTON_CLOSE_PRINT_PAGES', 'Cerrar todas las ventanas de impresión.');
define('WARNING_ORDER_NOT_UPDATED_ALL', 'Aviso: Algunos pedidos no han sido actualizados.');
define('TEXT_DO_STATUS_CHANGE', 'Actualizar estado:');
define('TEXT_DO_PRINT_INVOICE', 'Imprimir factura:');
define('TEXT_DO_PRINT_PACKINGSLIP', 'Imprimir comprobante de entrega:');
define('MULTISTATUS_DELETE_EXPLAIN', '¿Borrar?<br />(<span style="font-size:8px;">Las tres casillas de verificación deben estar marcadas,<br />para evitar un borrado accidental.</span>)');
//EOC multi_order_status based on module by XTC-DELUXE.DE, noRiddle

//BOC EK Preis
define('DEALERS_PRICE', 'Precio de compra');
//EOC EK Preis

//BOC vin search credit, noRiddle
define('TABLE_HEADING_CATALOG_CREDIT', 'Crédito de búsqueda de NIV');
//EOC vin search credit, noRiddle

//BOC PDFBill NEXT
define('BUTTON_INVOICE_PDF', 'Factura PDF');
define('BUTTON_PACKINGSLIP_PDF', 'Comprobante de entrega PDF');
define('BUTTON_BILL_NR', 'Número de factura');
define('BUTTON_SET_BILL_NR', 'Asignar número de factura');

define('MODULE_PDF_BILL_STATUS_TITLE', '¿Módulo activado?');
define('MODULE_PDF_BILL_STATUS_DESC', '');
define('PDF_BILL_LASTNR_TITLE', 'Último número de factura');
define('PDF_BILL_LASTNR_DESC', 'El último número de factura para la asignación automática.');
define('PDF_USE_ORDERID_PREFIX_TITLE', 'Prefijo del número de factura');
define('PDF_USE_ORDERID_PREFIX_DESC', 'Prefijo del número de factura si se utiliza el número de pedido como número de factura.');
define('PDF_USE_ORDERID_TITLE', 'Número de pedido como número de factura');
define('PDF_USE_ORDERID_DESC', 'Con esta opción se utiliza el número de pedido como número de factura.');
define('PDF_STATUS_COMMENT_TITLE', 'Comentario sobre el estado del pedido en PDF');
define('PDF_STATUS_COMMENT_DESC', 'Comentario que se añade al sistema cuando se envía una factura.');
define('PDF_STATUS_COMMENT_SLIP_TITLE', 'Comentario sobre el estado del comprobante de entrega en PDF');
define('PDF_STATUS_COMMENT_SLIP_DESC', 'Comentario que se añade al sistema cuando se envía un comprobante de entrega.');
define('PDF_FILENAME_SLIP_TITLE', 'Nombre de archivo del comprobante de entrega');
define('PDF_FILENAME_SLIP_DESC', 'Nombre de archivo del comprobante de entrega. Espacios se sustituyen por un guión bajo. Variables: <strong>{oID}</strong>, <strong>{bill}</strong>, <strong>{cID}</strong>. <strong>Por favor sin .pdf</strong>.');
define('PDF_MAIL_SUBJECT_TITLE', 'Asunto del correo de factura');
define('PDF_MAIL_SUBJECT_DESC', 'Introduzca aquí el asunto del correo de la factura. <strong>{oID}</strong> sirve como marcador de posición para el número de pedido.');
define('PDF_MAIL_SUBJECT_SLIP_TITLE', 'Asunto del correo del comprobante de entrega');
define('PDF_MAIL_SUBJECT_SLIP_DESC', 'Introduzca aquí el asunto del correo del comprobante de entrega. <strong>{oID}</strong> sirve como marcador de posición para el número de pedido.');
define('PDF_MAIL_SLIP_COPY_TITLE', 'Comprobante de entrega - dirección de reenvío');
define('PDF_MAIL_SLIP_COPY_DESC', 'Introduzca aquí una dirección de correo electrónico si desea recibir una copia.');
define('PDF_MAIL_BILL_COPY_TITLE', 'Factura - dirección de reenvío');
define('PDF_MAIL_BILL_COPY_DESC', 'Introduzca aquí una dirección de correo electrónico si desea recibir una copia.');
define('PDF_FILENAME_TITLE', 'Nombre de archivo de la factura');
define('PDF_FILENAME_DESC', 'Nombre de archivo de la factura. Espacios se sustituyen por un guión bajo. Variables: <strong>{oID}</strong>, <strong>{bill}</strong>, <strong>{cID}</strong>. <strong>Por favor sin .pdf</strong>.');
define('PDF_SEND_ORDER_TITLE', 'Enviar factura PDF automáticamente');
define('PDF_SEND_ORDER_DESC', 'Si esta opción está activada, el PDF de la factura se enviará automáticamente inmediatamente después de realizar el pedido.');

define('PDF_USE_CUSTOMER_ID_TITLE', 'Utilice el ID de cliente como número de cliente');
define('PDF_USE_CUSTOMER_ID_DESC', 'El ID de cliente se utiliza como número de cliente. Si se le asigna un número de cliente, por favor póngalo a false.');

define('PDF_STATUS_ID_BILL_TITLE', 'ID de estado del pedido - Factura PDF');
define('PDF_STATUS_ID_BILL_DESC', 'Puede encontrar el ID de estado del pedido en la línea del navegador después de <strong>oID=</strong> al editar el estado del pedido.');
define('PDF_STATUS_ID_SLIP_TITLE', 'ID de estado del pedido - Comprobante de entrega');
define('PDF_STATUS_ID_SLIP_DESC', 'Puede encontrar el ID de estado del pedido en la línea del navegador después de <strong>oID=</strong> al editar el estado del pedido.');

define('PDF_PRODUCT_MODEL_LENGTH_TITLE', 'Longitud máxima del número de artículo');
define('PDF_PRODUCT_MODEL_LENGTH_DESC', 'Número de caracteres después de que se corta el número de un artículo. Tenga en cuenta que un número de artículo demasiado largo puede destruir el diseño del archivo PDF.');
define('PDF_UPDATE_STATUS_TITLE', 'Actualizar el estado del pedido');
define('PDF_UPDATE_STATUS_DESC', 'El estado del pedido se actualiza automáticamente después de enviar el PDF por correo electrónico.');
define('PDF_USE_ORDERID_SUFFIX_TITLE', 'Sufijo del número de factura');
define('PDF_USE_ORDERID_SUFFIX_DESC', 'Sufijo para el número de factura si el número de pedido se utiliza como número de factura.');
define('PDF_STATUS_SEND_TITLE', 'Enviar factura al cambiar el estado de pedido');
define('PDF_STATUS_SEND_DESC', 'Enviar factura automáticamente cuando se cambia al estado del pedido de abajo');
define('PDF_STATUS_SEND_ID_TITLE', 'Enviar factura PDF con el ID de estado del pedido');
define('PDF_STATUS_SEND_ID_DESC', 'Al cambiar a este ID de estado de pedido, la factura se enviará.<br />(si "Enviar factura al cambiar el estado de pedido" está ajustado a "SI")');

define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_TITLE', 'Enviar el comprobante de entrega sólo al encargado de la logística o recogida de pedidos');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_DESC', 'Si activa esta función, el comprobante de entrega no se enviará al cliente, sino sólo al correo especificado abajo.');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME_TITLE', 'Nombre del Logista');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_NAME_DESC', 'Introduzca aquí el nombre del encargado de la logística que recibirá el comprobante de entrega.<br />(Sólo si "Enviar el comprobante de entrega sólo al encargado de la logística/recogida de pedido" está ajustado a "Sí")');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL_TITLE', 'Correo electrónico del Logista');
define('PDF_MAIL_SLIP_ONLY_LOGISTICIAN_EMAIL_DESC', 'Introduzca aquí la dirección de correo electrónico del logista que recibirá el comprobante de entrega.<br />(Sólo si "Enviar el comprobante de entrega sólo al encargado de la logística/recogida de pedido" está ajustado a "Sí")');

define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_TITLE', 'Número de factura siguiente');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_DESC', '<span style="color:#c00;">Modificar sólo con precaución, el sistema asigna el número de factura consecutivamente !<br />Si "Número de pedido como número de factura" está ajustado a "Sí", el cambio no tiene efecto.</span>');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT_TITLE', 'Formato del número de factura');
define('MODULE_INVOICEPDF_NUMBER_IBN_BILLNR_FORMAT_DESC', 'Esquema de estructura del número de factura: {n}=número consecutivo, {d}=Día, {m}=Mes, {y}=Año,<br />p.ej. "100{n}-{d}-{m}-{y}" resulta en "10099-28-02-2007"');
define('PDF_COMMENT_ON_PACKING_SLIP_TITLE', '¿Comentario del cliente en el comprobante de entrega?');
define('PDF_COMMENT_ON_PACKING_SLIP_DESC', '¿Imprimir el comentario que el cliente ha introducido en el pedido en el comprobante de entrega?');

//BOC - Innergemeinschaftliche Lieferungen
define('PDF_BILL_EU_CUSTOMERS_GROUP_ID_TITLE', 'Grupo de clientes Distribuidores de la UE:');
define('PDF_BILL_EU_CUSTOMERS_GROUP_ID_DESC', '<b>ID de grupo de clientes (cID)</b> de los distribuidores de la UE - para estos clientes se añade a la factura el aviso de exención de impuestos según  § 4 Nr. 1 b Umsatzsteuergesetz (UStG) !<br /><b>¡Separarar entradas múltiples con comas!</b><br />DEncontrará el ID de grupo de clientes en la línea del navegador después de <strong>cID=</strong> al editar el estado del pedido.');
//EOC - Innergemeinschaftliche Lieferungen

define('TABLE_HEADING_BILL_NR', 'Número de factura');
define('PDF_BILL_NR_INFO2', '¡Por favor, abra el encargo para editarlo, <br />e introduzca el nuevo número de factura en la parte inferior!');
//EOC PDFBill NEXT

//BOC yandex heat map, noRiddle
define('ACTIVATE_YANDEX_HEATMAP_TITLE', 'Activate Yandex heat map ?');
define('ACTIVATE_YANDEX_HEATMAP_DESC', 'To use this the heat map must be integrated in <i>/templates/DEIN_TEMPLATE/javascript/general_bottom.js.php</i>.');
//EOC yandex heat map, noRiddle
?>

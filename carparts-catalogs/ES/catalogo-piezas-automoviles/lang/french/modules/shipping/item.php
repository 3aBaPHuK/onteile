<?php
/* -----------------------------------------------------------------------------------------
   $Id: item.php 5118 2013-07-18 10:58:36Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(item.php,v 1.6 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (item.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_ITEM_TEXT_TITLE', 'Frais de port par pièce');
define('MODULE_SHIPPING_ITEM_TEXT_DESCRIPTION', 'Frais de port par pièce');
define('MODULE_SHIPPING_ITEM_TEXT_WAY', 'Meilleur chemin');
define('MODULE_SHIPPING_ITEM_INVALID_ZONE', 'Veuillez indiquer les zones <b>individuellement</b> pour lesquelles l\'envoi est possible. Par exemple AT,DE');

define('MODULE_SHIPPING_ITEM_STATUS_TITLE' , 'Activer les frais de port par pièce');
define('MODULE_SHIPPING_ITEM_STATUS_DESC' , 'Voulez-vous proposer les frais de port par pièce?');
define('MODULE_SHIPPING_ITEM_ALLOWED_TITLE' , 'Zones d\'envoi permises');
define('MODULE_SHIPPING_ITEM_ALLOWED_DESC' , 'Veuillez indiquer les zones <b>individuellement</b> pour lesquelles l\'envoi est possible. Par exemple AT,DE (Laissez ce champ vide si vous voulez permettre toutes les zones)');
define('MODULE_SHIPPING_ITEM_TAX_CLASS_TITLE' , 'Catégorie fiscale');
define('MODULE_SHIPPING_ITEM_TAX_CLASS_DESC' , 'Appliquer la catégorie fiscale aux frais de port');
define('MODULE_SHIPPING_ITEM_ZONE_TITLE' , 'Zone d\'envoi');
define('MODULE_SHIPPING_ITEM_ZONE_DESC' , 'Si vous choisissez une zone ce type d\'envoi est seulement porposé dans cette zone.');
define('MODULE_SHIPPING_ITEM_SORT_ORDER_TITLE' , 'Ordre de tri');
define('MODULE_SHIPPING_ITEM_SORT_ORDER_DESC' , 'Ordre d\'affichage');
define('MODULE_SHIPPING_ITEM_NUMBER_ZONES_TITLE' , 'Nombre de zones');
define('MODULE_SHIPPING_ITEM_NUMBER_ZONES_DESC' , 'Nombre des zones à disposition');
define('MODULE_SHIPPING_ITEM_DISPLAY_TITLE' , 'Activer l\'affichage');
define('MODULE_SHIPPING_ITEM_DISPLAY_DESC' , 'Voulez-vous afficher si l\'envoi n\'est pas possible dans le pays ou si les frais de port n\'ont pas pu être calculés?');

for ($module_shipping_item_i = 1; $module_shipping_item_i <= MODULE_SHIPPING_ITEM_NUMBER_ZONES; $module_shipping_item_i ++) {
define('MODULE_SHIPPING_ITEM_COUNTRIES_'.$module_shipping_item_i.'_TITLE' , '<hr/>Zone '.$module_shipping_item_i.' Pays');
define('MODULE_SHIPPING_ITEM_COUNTRIES_'.$module_shipping_item_i.'_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone '.$module_shipping_item_i.' (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_ITEM_COST_'.$module_shipping_item_i.'_TITLE' , 'Zone '.$module_shipping_item_i.' Frais de port');
define('MODULE_SHIPPING_ITEM_COST_'.$module_shipping_item_i.'_DESC' , 'Frais de port vers la zone '.$module_shipping_item_i.' seront multipliés avec la quantité d\'articles de la commande,si ce type d\'envoi est indiqué.');
define('MODULE_SHIPPING_ITEM_HANDLING_'.$module_shipping_item_i.'_TITLE' , 'Zone '.$module_shipping_item_i.' Frais de traitement');
define('MODULE_SHIPPING_ITEM_HANDLING_'.$module_shipping_item_i.'_DESC' , 'Frais de traitement pour cette zone d\'envoi');
}
?>

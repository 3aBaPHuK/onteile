<?php
/* --------------------------------------------------------------
   $Id: currencies.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(currencies.php,v 1.15 2003/05/02); www.oscommerce.com 
   (c) 2003	 nextcommerce (currencies.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/
   
define('HEADING_TITLE', 'Monnaies');

define('TABLE_HEADING_CURRENCY_NAME', 'Monnaie');
define('TABLE_HEADING_CURRENCY_CODES', 'Sigle');
define('TABLE_HEADING_CURRENCY_VALUE', 'Valeur');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_CURRENCY_STATUS', 'Statut');

define('TEXT_INFO_EDIT_INTRO', 'Veuillez effectuer tous les modifications nécessaires');
define('TEXT_INFO_CURRENCY_TITLE', 'Nom:');
define('TEXT_INFO_CURRENCY_CODE', 'Code de monnaie selon ISO 4217:');
define('TEXT_INFO_CURRENCY_SYMBOL_LEFT', 'Symbole gauche:');
define('TEXT_INFO_CURRENCY_SYMBOL_RIGHT', 'Symbole droit:');
define('TEXT_INFO_CURRENCY_DECIMAL_POINT', 'Point décimale:');
define('TEXT_INFO_CURRENCY_THOUSANDS_POINT', 'Séparateur de mille:');
define('TEXT_INFO_CURRENCY_DECIMAL_PLACES', 'Décimale:');
define('TEXT_INFO_CURRENCY_LAST_UPDATED', 'Dernières modifications:');
define('TEXT_INFO_CURRENCY_VALUE', 'Valeur:');
define('TEXT_INFO_CURRENCY_EXAMPLE', 'Exemples:');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez entrer la nouvelle monnaie contenant les données importantes');
define('TEXT_INFO_DELETE_INTRO', 'Voulez vous vraiment supprimer cette monnaie?');
define('TEXT_INFO_HEADING_NEW_CURRENCY', 'Nouvelle monnaie');
define('TEXT_INFO_HEADING_EDIT_CURRENCY', 'Modifier la monnaie');
define('TEXT_INFO_HEADING_DELETE_CURRENCY', 'Supprimer la monnaie');
define('TEXT_INFO_SET_AS_DEFAULT', TEXT_SET_DEFAULT . ' (Actualisation manuelle du taux de change nécessaire.)');
define('TEXT_INFO_CURRENCY_UPDATED', 'Le taux de change %s (%s) a été actualisé avec succès');

define('ERROR_REMOVE_DEFAULT_CURRENCY', 'Erreur: La monnaie par défaut ne peut pas être supprimée. Veuillez définir une nouvelle monnaie par défaut et répéter le processus.');
define('ERROR_CURRENCY_INVALID', 'Erreur: Le taux de change pour %s (%s) a été actualisé. Est-ce que c\'est un sigle de monnaie valable?');
define('WARNING_PRIMARY_SERVER_FAILED', 'Le service primaire "%s" ne pouvait pas calculer le taux de change %s (%s) ou il n\'est pas disponible. Il sera essayé de nouveau par le service secondaire.');
?>

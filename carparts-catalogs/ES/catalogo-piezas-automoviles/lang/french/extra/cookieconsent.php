<?php
  /* --------------------------------------------------------------
   $Id: cookie_consent.js.php $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2019 [www.modified-shop.org]
   --------------------------------------------------------------
   Released under the GNU General Public License
   --------------------------------------------------------------*/

  define('TEXT_COOKIE_CONSENT_LABEL_INTRO_HEADING', 'Ce site web utilise des cookies et d\'autres technologies');
  define('TEXT_COOKIE_CONSENT_LABEL_INTRO', 'Nous utilisons des cookies et des technologies similaires, aussi de tierce personne, afin d\'assurer le fonctionnenment juste du site web présent, afin d\'analyser notre offre et afin de être capable de vous offrir le meilleur possible expérience de shopping. Vous pouvez trouver plus d\'information dans notre \"Politique de protection des données\".');
  define('TEXT_COOKIE_CONSENT_LABEL_INTRO_TEXT_PRIVACY', 'Protection des données');
  define('TEXT_COOKIE_CONSENT_LABEL_INTRO_TEXT_IMPRINT', 'Mentions légales');

  define('TEXT_COOKIE_CONSENT_LABEL_BUTTON_YES', 'Sauvegarder');
  define('TEXT_COOKIE_CONSENT_LABEL_BUTTON_BACK', 'Retour');
  define('TEXT_COOKIE_CONSENT_LABEL_BUTTON_YES_ALL', 'Accepter tous');
  define('TEXT_COOKIE_CONSENT_LABEL_BUTTON_ADVANCED_SETTINGS', 'Plus d\'information');
  define('TEXT_COOKIE_CONSENT_LABEL_CPC_HEADING', 'Ajustements de cookies');
  define('TEXT_COOKIE_CONSENT_LABEL_CPC_ACTIVATE_ALL', 'Activer tous');
  define('TEXT_COOKIE_CONSENT_LABEL_CPC_DEACTIVATE_ALL', 'Désactiver tous');
  define('TEXT_COOKIE_CONSENT_LABEL_NOCOOKIE_HEAD', 'Pas de cookies permis.');
  define('TEXT_COOKIE_CONSENT_LABEL_NOCOOKIE_TEXT', 'S\'il vous plaît activez cookies dans les ajustements de votre navigateur.');

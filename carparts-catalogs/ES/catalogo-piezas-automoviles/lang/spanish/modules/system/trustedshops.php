<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_TRUSTEDSHOPS_TEXT_TITLE', 'Trusted Shops');
define('MODULE_TRUSTEDSHOPS_TEXT_DESCRIPTION', 'Con el módulo Trusted Shops usted se beneficia de todas las ventajas de la marca de confianza líder en Europa para su tienda online.<br/><br/>Y esto en <a target="_blank" href="http://www.trustedshops.de/shopbetreiber/index.html?shopsw=MODIFIED#offer?utm_source=Xtmodified&utm_medium=modul&utm_content=link1&utm_campaign=modultracking&a_aid=55cb437783a78">condiciones particularmente favorables</a> debido a la precertificación del modified eCommerce Shopsoftware.');
define('MODULE_TRUSTEDSHOPS_STATUS_TITLE', '¿Activar módulo?');
define('MODULE_TRUSTEDSHOPS_STATUS_DESC', 'Activar Trusted Shops');

?>

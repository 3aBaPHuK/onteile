<?php
/* --------------------------------------------------------------
   $Id: csv_backend.php 5217 2013-07-22 14:47:23Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/


define('TITLE', 'Backend CSV');

define('IMPORT', 'Import');
define('EXPORT', 'Export');
define('UPLOAD', 'Télécharger le fichier sur le serveur');
define('SELECT', 'Choisir les fichiers à importer et effectuer l\'import (/répertoire import)');
define('SAVE', 'Sauvegarder sur serveur (/répertoire export)');
define('LOAD', 'Envoyer le fichier au navigateur');
define('CSV_TEXTSIGN_TITLE', 'Signe de reconnaissance de texte');
define('CSV_TEXTSIGN_DESC', 'P.ex. "   |  <span style="color:#c00;"> Si signe de séparation et un point-virgule, réglez le signe de reconnaissance de texte sur "!</span>');
define('CSV_SEPERATOR_TITLE', 'Signe de séparation');
define('CSV_SEPERATOR_DESC', 'P.ex. ;   |  <span style="color:#c00;"> Si le champ de saisie reste vide le \t (=Tab) est utilisé ! </span>');
define('COMPRESS_EXPORT_TITLE', 'Compression');
define('COMPRESS_EXPORT_DESC', 'Compression des données exportées');
define('CSV_SETUP', 'Paramètres');
define('TEXT_IMPORT', '');
define('TEXT_PRODUCTS', 'Produits');
define('TEXT_EXPORT', 'Fichier exporté est sauvegardé dans le répertoire /export');
define('CSV_CATEGORY_DEFAULT_TITLE', 'Catégorie pour l\'import');
define('CSV_CATEGORY_DEFAULT_DESC', 'Tous articles qui n‘ont <b>pas</b> de catégorie et qui n‘existent pas encore dans la boutique sont importé dans cette catégorie.<br/><b>Important:</b> Si vous ne voulez pas importer des articles sans catégorie dans le fichier d’import CSV, sélectionnez la catégorie Top. Pas d’articles sont importés dans cette catégorie. ');
//BOC added constants for category depth, noRiddle
define('CSV_CAT_DEPTH_TITLE', 'Profondeur de la catégorie');
define('CSV_CAT_DEPTH_DESC', 'Quelle profondeur doit avoir l‘arbre des catégories? (p.ex. paramètre par défaut 4: catégorie principale et trois sous-catégories)<br /> Ce paramètre est important afin d‘importer les catégories en format CSV correctement. La même chose vaut pour l‘export.<br /><span style="color:#c00;"> Plus que 4 peut éventuellement mener à une perte de performance et n‘est éventuellement pas favorable aux clients !');
//EOC added constants for category depth, noRiddle
?>

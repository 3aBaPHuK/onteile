<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_SUPERMAILER_TEXT_TITLE', 'SuperMailer - Newsletter Software');
define('MODULE_SUPERMAILER_TEXT_DESCRIPTION', 'La interfaz permite una conexión directa a su tienda online. Sin esfuerzo manual, los datos del destinatario se importan automáticamente a su software SuperMailer.<hr noshade>');
define('MODULE_SUPERMAILER_STATUS_TITLE', '¿Activar módulo?');
define('MODULE_SUPERMAILER_STATUS_DESC', 'Activar el sistema de newsletter SuperMailer<br/><b>Importante:</b> Para ello, se requiere un <a href="http://www.superscripte.de/Reseller/reseller.php?ResellerID=3178&ProgramName=SuperMailer" target="_blank"><strong><u>registro en SuperMailer</u></strong></a> .');
define('MODULE_SUPERMAILER_EMAIL_ADDRESS_TITLE', 'Dirección de correo electrónico de SuperMailer');
define('MODULE_SUPERMAILER_EMAIL_ADDRESS_DESC', 'Introduzca aquí la dirección de correo electrónico de la que SuperMailer obtiene los datos.');
define('MODULE_SUPERMAILER_GROUP_TITLE', 'Grupo SuperMailer');
define('MODULE_SUPERMAILER_GROUP_DESC', 'Introduzca aquí el grupo para el newsletter.');

?>

<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_invoice.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_invoice.php, v 1.4 2003/02/17);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'Facturación de comisión');

define('TEXT_AFFILIATE', 'Afiliado');
define('TEXT_AFFILIATE_PAYMENT', 'Pago de comisión');
define('TEXT_AFFILIATE_BILLED', 'Fecha de facturación');
define('TABLE_HEADING_ORDER_ID', 'Número de pedido');
define('TABLE_HEADING_ORDER_DATE', 'Fecha');
define('TABLE_HEADING_ORDER_VALUE', 'Valor del pedido');
define('TABLE_HEADING_COMMISSION_RATE', '% comisión');
define('TABLE_HEADING_COMMISSION_VALUE', 'Comisión');

define('TEXT_SUB_TOTAL', 'Subtotal');
define('TEXT_TAX', 'IVA');
define('TEXT_TOTAL', 'Total');
define('ENTRY_PERCENT', '%');
?>

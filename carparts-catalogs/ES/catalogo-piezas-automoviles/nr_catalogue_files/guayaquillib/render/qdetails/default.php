<?php
/**********************************************
* file reworked and adadapted by noRiddle
*
* (c) noRiddle 03-2018
**********************************************/

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'template.php';
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'details'.DIRECTORY_SEPARATOR.'detailslist.php';

//includesd in /templates/catalogue/javascript/general.js.php, noRiddle
/*<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('.guayaquil_zoom').colorbox({
        href: function () {
                var url = jQuery(this).attr('full');
                return url;
            },
        photo:true,
        rel: "img_group",
        opacity: 0.3,
        title : function () {
            var title = jQuery(this).attr('title');
            var url = jQuery(this).attr('link');
            return '<a href="' + url + '">' + title + '</a>';
        },
        current: 'Рис. {current} из {total}',
        maxWidth : '98%',
        maxHeight : '98%'
        }
    )
});

</script>*/


class GuayaquilQuickDetailsList extends GuayaquilTemplate
{
	var $groups = NULL;
	var $vehicleid = NULL;
	var $ssd = NULL;
    var $catalog;

    var $currentcategory;
    var $currentunit;
    var $currentdetail;

    var $foundunit = false;

    var $closedimage = '../details/images/closed.gif';
    var $cartimage = '../details/images/cart.gif';
    var $detailinfoimage = '../details/images/info.gif';
    var $zoom_image = NULL;
    var $size = 175;

    var $drawtoolbar = true;

    function __construct(IGuayaquilExtender $extender)
    {
        parent::__construct($extender);

        $this->detaillistrenderer = $this->CrateDetailListRenderer();
        $this->detaillistrenderer->group_by_filter = 1;

        $this->closedimage = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->closedimage);
        $this->cartimage = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->cartimage);
        $this->detailinfoimage = $this->Convert2uri(dirname(__FILE__).DIRECTORY_SEPARATOR.$this->detailinfoimage);
        //BOC use font icons, noRiddle
        //$this->zoom_image = $this->Convert2uri(dirname(__FILE__).'/../images/zoom.png');
        $this->zoom_image = '<i class="fa fa-search-plus"></i>';
        //EOC use font icons, noRiddle

        //$this->AppendJavaScript(dirname(__FILE__).'/../jquery.colorbox.js'); /templates/catalogue/javascript/general.js.php, noRiddle
        //$this->AppendCSS(dirname(__FILE__).'/../colorbox.css'); /templates/catalogue/css/general.css.php, noRiddle
    }

    protected function CrateDetailListRenderer()
    {
        return new GuayaquilDetailsList($this->extender);
    }

	function Draw($details, $catalog, $vehicle_id, $ssd)
	{
        $this->catalog = $catalog;
        $this->vehicleid = $vehicle_id;
        $this->ssd = $ssd;

        //BOC add button to go back to main groups (qgroups)
        GuayaquilToolbar::AddButton($this->GetLocalizedString('back_to_group_link'), $this->FormatLink('groups', null, $this->catalog));
        //EOC add button to go back to main groups (qgroups)
        GuayaquilToolbar::AddButton($this->GetLocalizedString('VehicleLink'), $this->FormatLink('vehicle', null, $this->catalog));

        $html = $this->drawtoolbar ? GuayaquilToolbar::Draw() : '';

        foreach ($details->Category as $category)
            $html .= $this->DrawCategory($category);

        if (!$this->foundunit)
            $html .= $this->DrawEmptySet();

		//BOC don't need link to laximo, noRiddle
        //$html .= '<div style="visibility: visible; display: block; height: 20px; text-align: right;"><a href="http://dev.laximo.ru" rel="follow" style="visibility: visible; display: inline; font-size: 10px; font-weight: normal; text-decoration: none;">guayaquil</a></div>';
        $html .= '<div style="display:block; height:20px; text-align:right;"></div>';
        //EOC don't need link to laximo, noRiddle

		return $html;
	}

    protected function DrawCategory($category)
    {
        $this->currentcategory = $category;

        $html = '<div class="gdCategory">'.
            $this->DrawCategoryContent($category);

        foreach ($category->Unit as $unit)
            $html .= $this->DrawUnit($unit);

        $html .= '</div>';

        return $html;
    }

    protected function DrawCategoryContent($category)
    {
        $link = $this->FormatLink('category', $category, $this->catalog);
        //return '<h3><a href="'.$link.'">'.$category['name'].'</a></h3>';
        return '<h3><a href="'.$link.'"><span class="cssButton cssButtonColor1"><i class="fa fa-eye"></i><span class="cssButtonText">Category: '.$category['name'].'</span></span></a></h3>';
    }

    protected function DrawUnit($unit)
    {
        $this->currentunit = $unit;
        $this->foundunit = true;

        //BOC take off fix width, noRiddle
        /*return '<table class="gdUnit">
            <tr>
                <td class="gdImageCol" width="'.($this->size + 4).'" align=center valign=top>
                    '.$this->DrawUnitImage($unit).'
                </td><td class="gdDetailCol" valign=top>
                    '.$this->DrawUnitDetails($unit).'
                </td>
            </tr>
        </table>';*/
        
        return '<div class="gdSubCategory"><table class="gdUnit">
            <tr>
                <td class="gdImageCol" align="center" valign="top">
                    '.$this->DrawUnitImage($unit).'
                </td><td class="gdDetailCol" valign="top">
                    '.$this->DrawUnitDetails($unit).'
                </td>
            </tr>
        </table></div>';
        //EOC take off fix width, noRiddle
    }

    protected function DrawUnitImage($unit)
    {
/*        $note = (string)$unit['note'];
        if (strlen($note))
            $html .= '<br>Примечание: '.$note;
*/
        $link = $this->FormatLink('unit', $unit, $this->catalog);

        //BOC make URL of image independant from hypertext protocol, noRiddle
        //$img = str_replace('%size%', $this->size, $unit['imageurl']);
        $img = str_replace(array('%size%', 'http:'), array($this->size, ''), $unit['imageurl']);
        //EOC make URL of image independant from hypertext protocol, noRiddle
        
        if (strlen($img))
            $img = '<img class="img_group" src="'.$img.'">';

        //BOC use above defined font icon | make URL of image independant from hypertext protocol, noRiddle
        /*return '
            <div class="guayaquil_unit_icons">
                <div class="guayaquil_zoom" link="'.$link.'" full="'.str_replace('%size%', 'source', $unit['imageurl']).'" title="'.$unit['code'].': '.$unit['name'].'"><img src="'.$this->zoom_image.'"></div>
            </div>
            <div class="gdImage'.(!strlen($img) ? ' gdNoImage' : '').'" style="width:'.(int)$this->size.'px; height:'.(int)$this->size.'px;">
               '.$img.'
            </div>
            <a href="'.$link.'"><b>'.$unit['code'].':</b> '.$unit['name'].'</a>
        ';*/
        
        $rtn_val = '<div class="guayaquil_unit_icons">
                        <div class="guayaquil_zoom" link="'.$link.'" full="'.str_replace(array('%size%', 'http:'), array('source', ''), $unit['imageurl']).'" title="'.$unit['code'].': '.$unit['name'].'">'.$this->zoom_image.'</div>
                    </div>';
        //$rtn_val .= '<div class="gdImage'.(!strlen($img) ? ' gdNoImage' : '').'" style="width:'.(int)$this->size.'px; height:'.(int)$this->size.'px;">';
        $rtn_val .= '<div class="gdImage'.(!strlen($img) ? ' gdNoImage' : '').'">';
        $rtn_val .= '<a href="'.$link.'">'.$img.'</a>
                    </div>';
        //$rtn_val .= '<br /><a href="'.$link.'"><span class="cssButton cssButtonColor1"><i class="fa fa-eye"></i><span class="cssButtonText">'.wordwrap($unit['name'], 100, "<br />\n").'</span></span></a>';
        $rtn_val .= '<br /><a href="'.$link.'"><span class="cssButton cssButtonColor1" data-unitname="'.wordwrap(htmlspecialchars($unit['name']), 35, "<br />\n").'"><i class="fa fa-eye"></i><span class="cssButtonText">'.mb_substr($unit['name'], 0, 25, $_SESSION['language_charset']).' ...</span></span></a>';
        //<br /><a href="'.$link.'"><span class="btn button-catalogue"><i class="icon-eye-open"></i> '.wordwrap($unit['code'].': '.$unit['name'], 25, "<br />\n").'</span></a>
        
        return $rtn_val;
        //EOC use above defined font icon | make URL of image independant from hypertext protocol, noRiddle
    }

    protected function DrawUnitDetails($unit)
    {
        $this->detaillistrenderer->currentunit = $unit;
        return $this->detaillistrenderer->Draw($this->catalog, $unit->Detail);
    }

    protected function DrawEmptySet()
    {
        $link = $this->FormatLink('vehicle', null, $this->catalog);

        //BOC localize this hard coded russian output, noRidlle
        //return 'Ничего не найдено, воспользуйтесь <a href="'.$link.'">иллюстрированным каталогом</a> для поиска требуемой детали';
        return $this->GetLocalizedString('nothing_found_see_catalogue', $link);
        //EOC localize this hard coded russian output, noRidlle
    }
}

<?php
/*-----------------------------------------------------------
   $Id: general_bottom.js.php 89 2016-06-06 12:44:27Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
  -----------------------------------------------------------
   based on: (c) 2003 - 2006 XT-Commerce (general.js.php)
  -----------------------------------------------------------
   Released under the GNU General Public License
   -----------------------------------------------------------
*/
// this javascriptfile get includes at the BOTTOM of every template page in shop
// you can add your template specific js scripts here
?>
<script>
/*<![CDATA[*/
with(document.documentElement){className += 'js'}
if (('ontouchstart' in document.documentElement)) {document.documentElement.className += ' touch';} else {document.documentElement.className += ' no-touch';}
/*]]>*/
</script>
<?php
$script_array = array(
  DIR_TMPL_JS.'jquery.colorbox.min.js',
  //DIR_TMPL_JS.'cookieconsent.min.js', //add cookieconsent, noRiddle
  DIR_TMPL_JS.'jquery.unveil.min.js',
  DIR_TMPL_JS.'jquery.bxslider.min.js',
  DIR_TMPL_JS.'jquery.easyTabs.min.js',
  DIR_TMPL_JS.'jquery.alerts.min.js',
  DIR_TMPL_JS.'jquery.sumoselect.min.js',
  DIR_TMPL_JS.'flowtype.js',
  DIR_TMPL_JS.'jquery.sidebar.min.js',
  //'nr_catalogue_files/guayaquillib/render/jquery.tooltip.js', //always needed ?, noRiddle | gives an error, can't compress it
);
$script_min = DIR_TMPL_JS.'tpl_plugins.min.js';
  
$this_f_time = filemtime(DIR_FS_CATALOG.DIR_TMPL_JS.'general_bottom.js.php');
  
if (COMPRESS_JAVASCRIPT == 'true') {
  require_once(DIR_FS_BOXES_INC.'combine_files.inc.php');
  $script_array = combine_files($script_array,$script_min,false,$this_f_time);
}

foreach ($script_array as $script) {
  $script .= strpos($script,$script_min) === false ? '?v=' . filemtime(DIR_FS_CATALOG.$script) : '';
  echo '<script src="'.DIR_WS_BASE.$script.'" type="text/javascript"></script>'.PHP_EOL;
}
//BOC google adsense, noRiddle
?>
<script>
$(function() {
    $(".adsbygoogle").each(function () {(adsbygoogle = window.adsbygoogle || []).push({});});
});
</script>
<?php //EOC google adsense, noRiddle ?>

<?php //BOC include javascript for catalogue here, noRiddle  ?>
<script src="nr_catalogue_files/guayaquillib/render/jquery.tooltip.js" type="text/javascript"></script>
<?php if(isset($_GET['coID']) && $_GET['coID'] == '1008') { ?>
<script src="nr_catalogue_files/guayaquillib/render/applicability/applicability.js" type="text/javascript"></script>
<?php } ?>
<?php if(isset($_GET['coID']) && ($_GET['coID'] == '1006' || $_GET['coID'] == '1007' || $_GET['coID'] == '1008')) { ?>
<script src="nr_catalogue_files/guayaquillib/render/details/detailslist.js" type="text/javascript"></script>
<script>
$(function() {
    var $smdl = $('#search_model');
        $qms = $('#quick-model-search');
    $('.prod-repl-dat').click(function(e) {
        e.preventDefault();
        var which_prod_repl = $(this).data('prod_repl');
        $qms.val(which_prod_repl);
        $smdl.submit();
    });
});
</script>
<?php } ?>
<?php if(isset($_GET['coID']) && $_GET['coID'] == '1006') { ?>
<script src="nr_catalogue_files/guayaquillib/render/dragscrollable.js" type="text/javascript"></script>
<script src="nr_catalogue_files/guayaquillib/render/jquery.mousewheel.js" type="text/javascript"></script>
<script src="nr_catalogue_files/guayaquillib/render/unit/unit.js" type="text/javascript"></script>
<?php } ?>
<?php if(isset($_GET['coID']) && $_GET['coID'] == '1004') { ?>
<script src="nr_catalogue_files/guayaquillib/render/vehicle/unitlist-floated.js" type="text/javascript"></script>
<?php } ?>
<?php /*if(isset($_GET['coID']) && $_GET['coID'] == '1001') { //we don't need this, tooltip is displayed in table, noRiddle
echo '<script src="nr_catalogue_files/guayaquillib/render/vehicles/vehicletable.js" type="text/javascript"></script>';
}*/ ?>
<?php if(isset($_GET['coID']) && $_GET['coID'] == '1003') { ?>
<script src="nr_catalogue_files/guayaquillib/render/qgroups/groups.js" type="text/javascript"></script>
<?php } ?>
<?php //EOC include javascript for catalogue here, noRiddle ?>
<?php
ob_start();
foreach(auto_include(DIR_FS_CATALOG.DIR_TMPL_JS.'/extra/','php') as $file) require ($file);
$javascript = ob_get_clean();
if (COMPRESS_JAVASCRIPT == 'true') {
  require_once(DIR_FS_EXTERNAL.'compactor/compactor.php');
  $compactor = new Compactor(array('strip_php_comments' => false, 'compress_css' => false, 'compress_scripts' => true));
  $javascript = $compactor->squeeze($javascript);
}
echo $javascript.PHP_EOL;
?>

<?php if (strstr($PHP_SELF, FILENAME_CONTENT) && $_GET['coID'] == 8) { ?>
  <!--[if lt IE 10]>
  <script src="<?php echo DIR_WS_BASE.DIR_TMPL_JS; ?>jquery.css3-multi-column.js"></script>
  <![endif]-->
<?php } ?>

<?php
//BOC cokebumoslider, noRiddle
//if (basename($PHP_SELF) == FILENAME_DEFAULT && !isset($_GET['cPath']) && !isset($_GET['manufacturers_id'])) {
?>
<script src="<?php echo DIR_WS_BASE.'templates/'.CURRENT_TEMPLATE; ?>/kb-plugin/js/slider-nR.min.js" defer></script>
<!--[if lte IE 7]>
<script src="<?php echo DIR_WS_BASE.'templates/'.CURRENT_TEMPLATE; ?>/javascript/activatesliderKB-ie7.js"></script>
<![endif]-->
<script src="<?php echo DIR_WS_BASE.'templates/'.CURRENT_TEMPLATE; ?>/javascript/activatesliderKB.js" defer></script>
<?php
//}
//EOC cokebumoslider, noRiddle
//BOC Google Translator, noRiddle
?>
<script>
function googleTranslateElementInit() {
    new google.translate.TranslateElement({
        pageLanguage: '<?php echo $_SESSION['language_code']; ?>',
        includedLanguages: 'bg,cs,cy,cz,da,dk,ee,el,es,fi,fr,gr,hr,hu,is,it,lt,lv,nl,no,pl,pt,ro,se,sk,sl,sr,sv,tr',
        //layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT,
        multilanguagePage: true
    },
    'google_translate_element');
}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script>
$(function() {
    setTimeout(function() {
        $('select.goog-te-combo option[value=""]').text('<?php echo NR_GOOG_DEFAULT_TXT; ?>');
    }, 1200);
});
</script>
<?php //BOC Google Translator, noRiddle ?>
<?php
/************************************
* add_admin_footer.php
*
*
* (c) 09-2018 noRiddle
************************************/

if(isset($_GET['coID']) && $_GET['coID'] > 1100 && $_GET['coID'] < 2400 && substr($_GET['coID'], -1, 1) == 8) {
    $post_url = xtc_href_link(FILENAME_CONTENT_MANAGER);
?>
<style>
.working-gif {display:none; position:absolute; top:50%; left:50%; margin:-17px 0 0 -17px; z-index:3;}
.dataTableConfig a.button {margin:0 0 0 6px; text-decoration:none;}
.dataTableConfig span.button {
display: inline-block;
margin:0 0 0 6px;
padding:6px 15px;
background-color:#6f6f6f;
color:#ddd;
font-size:10px;
line-height:14px;
font-weight:bold;
border-style:solid;
border-width:1px;
border-color:#5d5d5d;
vertical-align:middle;
border-radius:4px;
}
</style>
<img class="working-gif" src="<?php echo DIR_WS_ICONS; ?>loading.gif" alt="loading" />

<script>
$(function() {
    var $wrk_gif = $('.working-gif'),
        $cnt_meta_desc_ipt = $('input[name^="content_meta_description"]'),
        auto_meta_desc_butt = '<a class="button auto-mdesc" href="#">autom. meta desc</a>', // <span style="color:red; font-weight:bold;">PLEASE DO NOT WORK, I\'m trying to fix problems here</span>
        meta_desc_okay = '<span class="button">OK</span>';

    $(auto_meta_desc_butt).appendTo($cnt_meta_desc_ipt.parent('td'));

    //$cnt_meta_desc_ipt.on('focus', function() {
    $('.auto-mdesc').on('click', function(e) {
        e.preventDefault();

        var $this = $(this);
            $met_ipt = $this.prev('input');
            $tptd = $met_ipt.parent('td');

        $tptd.css('position', 'relative');
        $wrk_gif.appendTo($tptd);

        var spl_regex = /\d/g,
            matches = $met_ipt.attr('name').match(spl_regex),
            fileflag = matches[0],
            langid = matches[1];
        //console.log(matches);

        $wrk_gif.show();

        $.post("<?php echo $post_url; ?>",
            {
            make_meta_desc: true,
            coid: "<?php echo $_GET['coID']; ?>",
            lng_id: ""+langid+""
            },
            function(data){
                if(data == 'NOTHING FOUND' || data == 'ERROR' || data == 'META-DESCRIPTION HAS GOT CATEGORIES ALREADY') {
                    if(data == 'NOTHING FOUND') {
                        var add_data = data+'<br />Forgotten to eliminate links in list ?';
                        alert(add_data);
                    } else {
                        alert(data);
                    }
                    if(data == 'META-DESCRIPTION HAS GOT CATEGORIES ALREADY') {
                        $this.replaceWith($(meta_desc_okay));
                    }
                } else {
                    //$met_ipt.val(data);
                    
                    var textbox = $(document.createElement('textarea')),
                        ipt_name = $met_ipt.attr('name');
                    $met_ipt.replaceWith(textbox);
                    $(textbox).attr('name', ipt_name).val(data).css({'width': '90%', 'height': '170px'});;
                    
                    $this.replaceWith($(meta_desc_okay));
                }
                //console.log(data);
                $wrk_gif.hide();
            }
        );
    });
});
</script>
<?php
}
?>
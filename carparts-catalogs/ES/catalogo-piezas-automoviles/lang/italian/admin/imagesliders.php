<?php
/************************************************************************
* language file for imageslider                                         *
* plugin derived from imageslider by Hetfield                           *
* based on                                                              *
* - responsive kenburner slider by codecanyon, licence necessary        *
*                                                                       *
* copyright (c) for all changes: noRiddle of webdesign-endres.de 2013   *
************************************************************************/

$slider_measures = array();
$slider_flyin_measures = array();
if((isset($data20) && !empty($data20)) && !isset($dataC20) && file_exists('../includes/local/configure.php')) {
    switch($shop) {
        case 'audi'.$shop_suffix:
            $slider_measures[$shop] = array('1420px', '250px');
            $slider_flyin_measures[$shop] = array('478', '146');
        break;
        default:
            $slider_measures[$shop] = array('1420px', '250px');
            $slider_flyin_measures[$shop] = array('478', '146');
        break;
    }
} else if((isset($dataC20) && !empty($dataC20)) || !file_exists('../includes/local/configure.php')) {
    $slider_measures[$shop] = array('1580px', '250px');
    $slider_flyin_measures[$shop] = array('478', '146');
}

define('HEADING_TITLE_II', 'CoKeBuMo-Imageslider ed. 1.5.2');
define('TABLE_HEADING_IMAGESLIDERS', 'Imageslider');
define('TABLE_HEADING_SPEED', 'Speed');
define('TABLE_HEADING_SORTING', 'Sorting');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');
define('TEXT_HEADING_NEW_IMAGESLIDER', 'New image');
define('TEXT_HEADING_EDIT_IMAGESLIDER', 'Edit image');
define('TEXT_HEADING_DELETE_IMAGESLIDER', 'Delete image');
define('TEXT_IMAGESLIDERS', 'Image:');
define('TEXT_IMAGE_DIMENSIONS', 'Original image dimensions: ');
define('TEXT_DATE_ADDED', 'addes the:');
define('TEXT_LAST_MODIFIED', 'last modified the:');
define('TEXT_IMAGE_NONEXISTENT', 'IMAGE NOT EXISTING');
define('TEXT_NEW_INTRO', 'Please create the new image with all relevant data.');
define('TEXT_EDIT_INTRO', 'Please conduct the necessary changes');
define('TEXT_IMAGESLIDERS_TITLE', 'Title for the image:');
define('TEXT_IMAGESLIDERS_NAME', 'Name for image entry:');
define('TEXT_IMAGESLIDERS_IMAGE', 'Image:');
define('TEXT_IMAGESLIDERS_URL', 'Link image <span style="color:#B30000; font-size:9px;"> (only possible if no video has been implemented (see below), in case of a video implementation choose "no link")</span>');
define('TEXT_TARGET', 'Target window:');
define('TEXT_TYP', 'Link type:');
define('TEXT_URL', 'Link address:');
define('TEXT_IMAGESLIDERS_ALT', 'alt-tag for image (Google ?):');
define('TEXT_IMAGESLIDERS_VIDEO_URL', 'Embed-URL f a desired videos for thsi picture. "YouTube" and "Vimeo" are supported.');
define('TEXT_IMAGESLIDERS_VIDEO_DESC', 'Video description:');
define('TEXT_IMAGESLIDERS_CREDITS_DESC', 'Explanations and Credits:');
define('TEXT_IMAGESLIDERS_CREDITS_EXPL', '<p>Based on kenburner by codecanyon, code widely modified for better text-effects and other changes by <a href="http://www.revilonetz.de" target="_blank">noRiddle</a> ed. 1.5.2 | 03-2013 - '.date('Y').'</p><p>In case of any questions regarding the interface or in case of wishes for extras and and extensions feel free to contact me <a class="imgslider-link" href="http://www.revilonetz.de/kontakt" target="_blank">noRiddle</a></p>');
define('TXT_BUTTON_COPY_SLIDER', 'Copy slider from shop %s');
define('TXT_IDS_RANGE', 'Enter Ids from-to here in the form X-Y. (Example:10-15)<br />If filed will be left empty all images will be copied.');
define('TXT_COPY_SLIDER_EXPL', 'Only active images will be copied from the slider of the shop %1$s.<br />ATTENTION: %1$s probably has got different dimensions, please verify.');
define('TXT_BUTTON_NEWIMAGE', 'Insert new image');
define('TXT_BUTTON_COPY_IMAGE', 'Copy image with all configurations');
define('TXT_DEFAULT_COPY_IMAGE', 'Chosse image');

define('NONE_TARGET', 'No target');
define('TARGET_BLANK', '_blank');
define('TARGET_TOP', '_top');
define('TARGET_SELF', '_self');
define('TARGET_PARENT', '_parent');
define('TYP_NONE', 'No link');
define('TYP_PRODUCT', 'Link to product (please enter only the productsID into the field "Link address")');
define('TYP_CATEGORIE', 'Link to category (please enter only the catID  into the field "Link address". <span style="color:#c00">!! With sub-categories enter always only the last ID without underscore before it !!</span>)');
define('TYP_CONTENT', 'Link to content page (please enter only the coID into the field "Link address")');
define('TYP_INTERN', 'Internal shop link (e.g. account.php or newsletter.php)');
define('TYP_EXTERN', 'External Link (e.g. http://www.external_link.com)');
define('TEXT_IMAGESLIDERS_DESCRIPTION', 'Image description:');
define('INFO_IMAGESLIDERS_DESCRIPTION', 'When images are inserted the with shall have a maximum of '.$slider_flyin_measures[$shop][0].', and the height a maximum of '.$slider_flyin_measures[$shop][1].'.<br />Please unconditionally delete the image dimensions in the filebrowser.<br />In the dropdown "Style" you can chosse different font sizes.<br />For font sizes don\'t use the dropdown for "Size".');
define('TEXT_DELETE_INTRO', 'Are you sure yoiu want to delete this image?');
define('TEXT_DELETE_IMAGE', 'Also delete image?');
define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Error: The directory %s is write-protected. Please correct the admission rights to this directory!');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Fehler: Das Verzeichnis %s existiert nicht!');
define('TEXT_DISPLAY_NUMBER_OF_IMAGESLIDERS', 'Displayed are <b>%d</b> to <b>%d</b> (from total <b>%d</b> images in the slideshow)');  //'Anzahl Bilder in Slideshow');
define('IMAGE_ICON_STATUS_GREEN', 'Active');
define('IMAGE_ICON_STATUS_GREEN_LIGHT', 'Activate');
define('IMAGE_ICON_STATUS_RED', 'Not active');
define('IMAGE_ICON_STATUS_RED_LIGHT', 'Deactivate');
define('ACTIVE', 'active');
define('NOTACTIVE', 'not active');

define('INFO_IMAGE_NAME', 'Please enter a name for the image to be able to recognize it later in case of editing.');
define('INFO_IMAGE_SORT', 'Enter a whole number here to determine the sorting of the display of the images.');
define('INFO_IMAGE_ACTIVE', 'Here you can switch the image active or inactive.<br>The other images will not be influenced by this.');
define('INFO_IMAGE_UPLOAD', 'The name of the uploaded image shall not be too long (not more than 38 characters without file extension).<br>For each language in which the slider shall be displayed an image has to be uploaded.<br><span style="color:#c00;">!! Dimensions for this shop !!</span><br>Image width: '.$slider_measures[$shop][0].'<br>Image height: '.$slider_measures[$shop][1]);
define('INFO_IMAGE_TITLE', 'Der Title des Bildes wird zur Referenzierung für den Text der im Bild dargestellt wird benötigt.<br>Bitte kurzen eindeutigen Title angeben.<br>Title darf nur einmal vorkommen, also bei anderen Bildern andere Title benutzen.<br>Am besten Sie benutzen immer den gleichen Title und nummerieren diesen durch.<br>z.B.:<br>cap1, nächstes Bild cap2 usw.<br /><span style="color:#c00">Wenn kein Text (Bildbeschreibung) angegeben wird, hier auch keinen Title vergeben !</span>');
define('INFO_IMAGE_ALT', 'Enter the alt-tag for the image here.<br />This could be important for search engines (SERP).');

//***BOC KenBurns Codecanyon***
define('TEXT_IMAGESLIDERS_DATA_ATTRIBUTES', 'Effects for the slider <span style="color:#B30000; font-size:9px;">(on choosing a value in one language all other languages will also be set to that value)</span>');
define('INFO_IMAGE_DATA_ATTRIBUTES', 'Von den hier dargestellten Optionen muß jeweils eine ausgew&auml;hlt werden wenn der KenBurns-Effekt aktiviert ist.<br>Die Textfelder m&uuml;ssen ebenfalls ausgef&uuml;llt werden (siehe dort).');
define('INFO_IMAGE_CAPTION_ATTRIBUTES', 'Bitte darauf achten, daß die Effekte zu dem Darstellungsort passen (s.u.)');
define('INFO_IMAGE_VIDEO_URL', 'Im Falle das vorliegende Bild ein Video enthalten soll, hier die Embed-URL eingeben.<br />Embed-URL bedeutet die URL die in dem von "YouTube" oder "Vimeo" zur Verfügung gestellten Einbettungscode enthalten ist. <span style="color:#c00;">! Nur die URL, nicht den ganzen Embed-Code !</span><br />Sie k&ouml;nnen das Anzeigen von "related videos" nach Abspielen des Videos unterdrücken indem Sie folgendes (bei YouTube-Videos) an die URL anhängen: <i>&amp;amp;rel=0</i><br />Es gibt noch weitere Parameter mit Hilfe derer man das Verhalten des Videoplayers beeinflussen kann.<br />Schauen Sie dazu bitte in die YouTube- und/ oder Vimeo-Spezifikationen.');

define('TXT_GENERAL_KENBURNS', 'Schalten sie den KenBurns-Effekt an oder aus');
define('TXT_IF_KENBURNS_ON', 'Wenn Kenburns ausgeschaltet ist, sind die darunter stehenden Einstellung nicht alle nötig (siehe dort)');
define('TXT_GENERAL_TRANSITION', 'Der &Uuml;bergang der Bilder, "fade" oder "slide" (<span style="color:#c00;">! muß immer angegeben werden</span>)');
define('TXT_GENERAL_STARTALIGN', 'Beginn des KenBurns-Effektes, horizontal | vertikal');
define('TXT_GENERAL_ENDALIGN', 'Ende des KenBurns-Effektes, horizontal | vertikal');
define('TXT_GENERAL_ZOOM_AND_PAN', 'Zoom-Einstellungen und Dauer der Bewegung');
define('TXT_GENERAL_CAPTION', 'Stellen Sie ein wo der Text erscheint und mit welchem Effekt');
define('TEXT_IMAGESLIDERS_CAPTION_ATTRIBUTES', 'Text attributes <span style="color:#B30000; font-size:9px;">(on choosing a value in one language all other languages will also be set to that value)</span>');
define('TXT_CAPTION_DIRECTION_AND_EFFECT', 'The effects and appearing spots which fit together are displayed side by side');
define('TXT_CAPTION_LEFT', 'Text left');
define('TXT_CAPTION_FADE_RIGHT', 'slides from the left to the right into the canvas');
define('TXT_CAPTION_RIGHT', 'Text right');
define('TXT_CAPTION_FADE_LEFT', 'slides from right to left into the canvas');
define('TXT_CAPTION_TOP', 'Text top');
define('TXT_CAPTION_FADE_DOWN', 'slides from top to bottom into the canvas');
define('TXT_CAPTION_BOTTOM', 'Text bottom');
define('TXT_CAPTION_FADE_UP', 'slides frombottom to top into the canvas');
define('TXT_CAPTION_FADE', 'will be faded in (always fits)');
define('TXT_KENBURN_ON', '<b>On</b>');
define('TXT_KENBURN_OFF', '<b>Off</b>');                                  
define('TXT_TRANSITION_FADE', '<b>Fade</b>');
define('TXT_TRANSITION_SLIDE', '<b>Slide</b>');
define('TXT_STARTALIGN_LEFTTOP', '<b>left | top</b>');
define('TXT_STARTALIGN_LEFTCENTER', '<b>left | center</b>');
define('TXT_STARTALIGN_LEFTBOTTOM', '<b>left | bottom</b>');
define('TXT_STARTALIGN_CENTERTOP', '<b>center | top</b>');
define('TXT_STARTALIGN_CENTERCENTER', '<b>center | center</b>');
define('TXT_STARTALIGN_CENTERBOTTOM', '<b>center | bottom</b>');
define('TXT_STARTALIGN_RIGHTTOP', '<b>right | top</b>');
define('TXT_STARTALIGN_RIGHTCENTER', '<b>right | center</b>');
define('TXT_STARTALIGN_RIGHTBOTTOM', '<b>right | bottom</b>');
define('TXT_STARTALING_RANDOM', '<b>at random</b>');
define('TXT_ENDALIGN_LEFTTOP', '<b>left | top</b>');
define('TXT_ENDALIGN_LEFTCENTER', '<b>left | center</b>');
define('TXT_ENDALIGN_LEFTBOTTOM', '<b>left | bottom</b>');
define('TXT_ENDALIGN_CENTERTOP', '<b>center | top</b>');
define('TXT_ENDALIGN_CENTERCENTER', '<b>center | center</b>');
define('TXT_ENDALIGN_CENTERBOTTOM', '<b>center | bottom</b>');
define('TXT_ENDALIGN_RIGHTTOP', '<b>right | top</b>');
define('TXT_ENDALIGN_RIGHTCENTER', '<b>right | center</b>');
define('TXT_ENDALIGN_RIGHTBOTTOM', '<b>right | bottom</b>');
define('TXT_ENDALIGN_RANDOM', '<b>at random</b>');
define('TXT_ZOOM', '<b>Zoom in zoom out</b>, enter one of the values "out | in | random"');
define('TXT_ZOOMFACT', 'Zoom factor (please enter a one digit float (comma number) (! comma must be a point/dot), at best between 1 and 2). If the value above is "out" the zoom factor must be suitably high or the image must be bigger than the HTML container it is in (meaning bigger than the in the info icon with the image itself indicated dimensions.');
define('TXT_PANDURATION', 'Duration of the movement in seconds (<span style="color:#c00;">! must be indicated always</span>) (if it is not indicated the value in the deafult settings for "timer" <i>general.js.php</i>  will be taken (which is 5))');
?>
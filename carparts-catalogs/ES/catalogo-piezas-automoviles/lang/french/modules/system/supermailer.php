<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_SUPERMAILER_TEXT_TITLE', 'SuperMailer - Newsletter Software');
define('MODULE_SUPERMAILER_TEXT_DESCRIPTION', 'L\'interface permet une liaison direct avec votre boutique en ligne. Les données des destinataire seront importées automatiquement dans le logiciel SuperMailer.<hr noshade>');
define('MODULE_SUPERMAILER_STATUS_TITLE', 'Activer module ?');
define('MODULE_SUPERMAILER_STATUS_DESC', 'Activer système de newsletter SuperMailer<br/><bAttention :</b>Une <a href="http://www.superscripte.de/Reseller/reseller.php?ResellerID=3178&ProgramName=SuperMailer" target="_blank"><strong><u>inscription auprès de SuperMailer</u></strong></a>est nécessaire.');
define('MODULE_SUPERMAILER_EMAIL_ADDRESS_TITLE', 'Adresse mail SuperMailer');
define('MODULE_SUPERMAILER_EMAIL_ADDRESS_DESC', 'Saisissez l\'adresse mail pour importer données');
define('MODULE_SUPERMAILER_GROUP_TITLE', 'Groupe SuperMailer');
define('MODULE_SUPERMAILER_GROUP_DESC', 'Saisissez le groupe pour la newsletter.');

?>

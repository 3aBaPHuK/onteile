<?php
/***************************************************************************
* file: cat_cart_ajax_url.php
* path: /includes/extra/ajax/
* use: ajax response for on-the-fly token for cart link to remote shops
*
* (c) 07-2020, noRiddle
***************************************************************************/

//BOC create remote url for catalogue_order_details_cart.php via ajax
function cat_cart_ajax_url() {
    if(isset($_POST['cc_create_sec_remote']) && $_POST['cc_create_sec_remote'] == 'csr_true') {
        global $cat_env, ${'data_'.$cat_env};
        $token_db_arr = ${'data_'.$cat_env};

        $tok_str = '';

        require_once (DIR_FS_INC . 'xtc_create_password.inc.php');

        $remote_cart_token = xtc_RandomString(32);
        
        //BOC save into DB
        $rem_shplink = new mysqli($token_db_arr['cart_token']['sql_server'], $token_db_arr['cart_token']['sql_user'], $token_db_arr['cart_token']['sql_pass'], $token_db_arr['cart_token']['sql_db']);
        if ($rem_shplink->connect_errno) {
            die("Connection failed: " . $rem_shplink->connect_error);
        }
        //set charset for connection
        $rem_shplink->set_charset('utf8');
        
        $cart_token_qu_str = "INSERT INTO cart_tokens (customers_id, token, shop, date_added) VALUES(".$_SESSION['customer_id'].", '".$remote_cart_token."', '".$_POST['shop']."', now()) 
                              ON DUPLICATE KEY UPDATE customers_id=".$_SESSION['customer_id'].", token='".$remote_cart_token."', shop='".$_POST['shop']."', date_added=now()";
        $cart_token_qu = $rem_shplink->query($cart_token_qu_str);

        $rem_shplink->close();
        //EOC save into DB

        $tok_str .= '&cid='.$_SESSION['customer_id'].'&cred='.$remote_cart_token;

        return $tok_str;
    }
    
    return '';
}
//EOC create remote url for catalogue_order_details_cart.php via ajax
?>
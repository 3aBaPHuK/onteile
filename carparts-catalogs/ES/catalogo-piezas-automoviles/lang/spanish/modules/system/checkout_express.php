<?php
/* -----------------------------------------------------------------------------------------
   $Id: checkout_express.php 10597 2017-01-23 18:10:51Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_CHECKOUT_EXPRESS_STATUS_TITLE', 'Estado');
define('MODULE_CHECKOUT_EXPRESS_STATUS_DESC', 'Estado del módulo');
define('MODULE_CHECKOUT_EXPRESS_STATUS_INFO', 'Activo');
define('MODULE_CHECKOUT_EXPRESS_TEXT_TITLE', 'Pago exprés');
define('MODULE_CHECKOUT_EXPRESS_TEXT_DESCRIPTION', 'Pago exprés permite guardar la información de envío y pago. <br/><br/><b>Atención:</b> ¡Esto sólo funciona con una plantilla compatible con la versión 2.0.0.0 o superior de la tienda!');
define('MODULE_CHECKOUT_EXPRESS_CONTENT_TITLE', 'Contenido');
define('MODULE_CHECKOUT_EXPRESS_CONTENT_DESC', 'También puede crear una página de contenido y explicar el proceso de pago exprés.');

define('MODULE_CHECKOUT_EXPRESS_DESCRIPTION_INSTALL', '<br/><br/>Además, puede instalar una página de información para la opción Pago exprés.');
define('MODULE_CHECKOUT_EXPRESS_BUTTON_INSTALL', 'Insertar contenido');
?>

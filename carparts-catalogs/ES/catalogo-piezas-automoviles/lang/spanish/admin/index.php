<?php
/* --------------------------------------------------------------
   $Id: index.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(index.php,v 1.5 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (index.php,v 1.5 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Seleccionar opción...');

define('BOX_TITLE_ORDERS', 'pedidos');
define('BOX_TITLE_STATISTICS', 'Estadística');

define('BOX_ENTRY_SUPPORT_SITE', 'Página de soporte');
define('BOX_ENTRY_SUPPORT_FORUMS', 'Foro de soporte');
define('BOX_ENTRY_MAILING_LISTS', 'Listas de correo');
define('BOX_ENTRY_BUG_REPORTS', 'Informes de error');
define('BOX_ENTRY_FAQ', 'Preguntas y respuestas');
define('BOX_ENTRY_LIVE_DISCUSSIONS', 'Discusiones en vivo');
define('BOX_ENTRY_CVS_REPOSITORY', 'CVS Repository');
define('BOX_ENTRY_INFORMATION_PORTAL', 'Portal de información');

define('BOX_ENTRY_CUSTOMERS', 'Clientes:');
define('BOX_ENTRY_PRODUCTS', 'Artículos:');
define('BOX_ENTRY_REVIEWS', 'Reseñas:');

define('BOX_CONNECTION_PROTECTED', 'Está protegido por una %s conexión SSL segura.');
define('BOX_CONNECTION_UNPROTECTED', ' <span class="col-red">No</span> está protegido por una conexión SSL segura.');
define('BOX_CONNECTION_UNKNOWN', 'desconocido');

define('BOX_HEADING_CONFIGURATION', 'Configuración');
define('BOX_CONFIGURATION_MYSTORE', 'Mi tienda');
define('BOX_CONFIGURATION_LOGGING', 'Información de Logging');
define('BOX_CONFIGURATION_CACHE', 'Opciones de caché');

define('BOX_MODULES_PAYMENT', 'Modalidad de pago');
define('BOX_MODULES_SHIPPING', 'Modo de envío');

define('BOX_CATALOG_MANUFACTURERS', 'Fabricante');

define('BOX_CUSTOMERS_CUSTOMERS', 'Clientes');
define('BOX_CUSTOMERS_ORDERS', 'Pedidos');
define('BOX_CUSTOMERS_ACCOUNTING', 'Liquidación');

define('BOX_TAXES_COUNTRIES', 'Países');
define('BOX_TAXES_GEO_ZONES', 'Zonas fiscales');

define('BOX_LOCALIZATION_CURRENCIES', 'Monedas');
define('BOX_LOCALIZATION_LANGUAGES', 'Idiomas');

define('CATALOG_CONTENTS', 'Contenido');

define('REPORTS_PRODUCTS', 'Artículo');
define('REPORTS_ORDERS', 'Pedidos');

define('TOOLS_BACKUP', 'Respaldo de datos');
define('TOOLS_BANNERS', 'Gestión de Banners');
define('TOOLS_FILES', 'Gestión de datos');
?>

<?php

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'template.php';

class GuayaquilWizard extends GuayaquilTemplate
{
	var $wizard = NULL;
	var $catalog = NULL;

	function __construct(IGuayaquilExtender $extender)
	{
		parent::__construct($extender);
	}

	function Draw($catalog, $wizard)
	{
		$this->wizard = $wizard;
		$this->catalog = $catalog;

		$html = '<div class="DrawHeader">';
		$html .= '<script type="text/javascript">';
		$html .= 'function openWizard(valueid) {';
		$html .= 'window.location = \''.$this->FormatLink('wizard', null, $catalog).'\'.replace(\'\\$valueid\\$\', valueid);';
		$html .= '}';
		$html .= '</script>';
		$html .= '<form name="findByParameterIdentifocation" id="findByParameterIdentifocation">';
		$html .= '<table border="0" width="100%">';
		
		$html .= $this->DrawHeader();

        //prevent error  "Invalid argument supplied for foreach() in /usr/www/users/onteile/oem-kataloge/nr_catalogue_files/guayaquillib/render/wizard2/wizard.php on line 32"
        if(is_object($wizard->row)) {
            foreach($wizard->row as $condition)
                $html .= $this->DrawConditionRow($catalog, $condition);
        }

		$html .= '</table>';
		$html .= '</form>';

		if ($wizard->row['allowlistvehicles'] == 'true')
			$html .= $this->DrawVehiclesListLink($catalog, $wizard);

		$html .= '</div>';
        //show me, noRiddle
        //$html .= '<pre>'.print_r($wizard->row, true).'</pre>';

		return $html;
	}

	function DrawHeader()
	{
		return '';
	}

	function DrawConditionRow($catalog, $condition)
	{
		//$html = '<tr width="60%"'.($condition['automatic'] == 'false' ? ' class="guayaquil_SelectedRow"' : '').'>';
        $html = '<tr'.($condition['automatic'] == 'false' ? ' class="guayaquil guayaquil_SelectedRow"' : ' class="guayaquil"').'>'; //why width on tr ?, noRiddle
		$html .= '<td width="22%">'.$this->DrawConditionName($catalog, $condition).'</td>'; //set width here, noRiddle

		$html .= '<td width="78%">'; //set width here, noRiddle
		if ($condition['determined'] == 'false')
			$html .= $this->DrawSelector($catalog, $condition);
		else
			$html .= $this->DrawDisabledSelector($catalog, $condition);
		
		$html .= '</td><td>';
		$html .= $this->DrawConditionDescription($catalog, $condition);
		$html .= '</td></tr>';

		return $html;
	}

	function DrawConditionName($catalog, $condition)
	{
		return $condition['name'];
	}

	function DrawConditionDescription($catalog, $condition)
	{
		return $condition['description'];
	}

	function DrawSelector($catalog, $condition)
	{
		//$html = '<select style="width:250px" name="Select'.$condition['type'].'" onChange="openWizard(this.options[this.selectedIndex].value); return false;">';
        $html = '<select name="Select'.$condition['type'].'" onChange="openWizard(this.options[this.selectedIndex].value); return false;">'; //don't set width here, noRiddle

		$html .= '<option value="null">&nbsp;</option>';

		foreach($condition->options->row as $row) {
			$html .= $this->DrawSelectorOption($row);
		}

		$html .= '</select>';

		return $html;
	}

	function DrawDisabledSelector($catalog, $condition)
	{
		//$html = '<select disabled style="width:250px" name="Select'.$condition['type'].'">';
        $html = '<select disabled="disabled" name="Select'.$condition['type'].'">'; //don't set width here, noRiddle
		$html .= $this->DrawDisabledSelectorOption($condition);
		$html .= '</select>';

		return $html;
	}

	function DrawSelectorOption($row)
	{
		return '<option value="'.$row['key'].'">'.$row['value'].'</option>';
	}

	function DrawDisabledSelectorOption($condition)
	{
		return '<option disabled selected value="null">'.$condition['value'].'</option>';
	}

	function DrawVehiclesListLink($catalog, $wizard)
	{
        //BOC build nice button, noRiddle
        //$html = '<br><a class="gWizardVehicleLink" href="'.$this->FormatLink('vehicles', $wizard, $catalog).'">';
        $html = '<p style="margin-top:15px">';
        $html .= '<a class="gWizardVehicleLink" href="'.$this->FormatLink('vehicles', $wizard, $catalog).'" title="'.$this->GetLocalizedString('List vehicles').'"><span class="cssButton cssButtonColor1"><i class="fa fa-eye"></i><span class="cssButtonText">';
        $html .= $this->GetLocalizedString('List vehicles');
        $html .= '</span></a>';
        $html .= '</p>';
        //EOC build nice button, noRiddle

		return $html;
	}
}

?>
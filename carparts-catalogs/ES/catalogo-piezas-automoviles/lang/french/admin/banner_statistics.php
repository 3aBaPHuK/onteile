<?php
/* --------------------------------------------------------------
   $Id: banner_statistics.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(banner_statistics.php,v 1.3 2003/02/16); www.oscommerce.com 
   (c) 2003	 nextcommerce (banner_statistics.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Statistique de la bannière');

define('TABLE_HEADING_SOURCE', 'Base');
define('TABLE_HEADING_VIEWS', 'Afficher');
define('TABLE_HEADING_CLICKS', 'Clics');

define('TEXT_BANNERS_DATA', 'D<br />A<br />T<br />E<br />N');
define('TEXT_BANNERS_DAILY_STATISTICS', '%s statistique du jour pour %s &s');
define('TEXT_BANNERS_MONTHLY_STATISTICS', '%s statistique du moins pour %s');
define('TEXT_BANNERS_YEARLY_STATISTICS', '%s statistique de l\'année');

define('STATISTICS_TYPE_DAILY', 'quitidiennement');
define('STATISTICS_TYPE_MONTHLY', 'mensuellement');
define('STATISTICS_TYPE_YEARLY', 'annuellement');

define('TITLE_TYPE', 'Type:');
define('TITLE_YEAR', 'Année:');
define('TITLE_MONTH', 'Mois:');

define('ERROR_GRAPHS_DIRECTORY_DOES_NOT_EXIST', 'Erreur: le répertoire \'graphs\' n\'existe pas! Veuillez créer un répertoire \'graphs\' dans le répertoire \'images\'.');
define('ERROR_GRAPHS_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le répertoire \'graphs\' est verrouillé!');
?>

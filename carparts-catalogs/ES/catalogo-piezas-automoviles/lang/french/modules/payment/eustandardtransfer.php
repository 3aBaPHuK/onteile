<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ptebanktransfer.php,v 1.4.1 2003/09/25 19:57:14); www.oscommerce.com
   (c) 2003 xtCommerce www.xt-commerce.com

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_TEXT_TITLE', 'Virement bancaire stndard en UE');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_TEXT_DESCRIPTION', 
'<br />En UE la méthode de paiement la moins cher et la plus facile est le virement par IBAN ou BIC.
<br />Veuillez utiliser les données suivantes pour le virement du montant total:<br />' .
          '<br />Nom de la banque: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKNAM') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKNAM : '') .
 '<br />Bénéficaire: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_BRANCH') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_BRANCH : '') .
'<br />Code bancaire: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNAM') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNAM : '') .
'<br />Numéro de compte: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNUM') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNUM : '') .
  '<br />IBAN: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCIBAN') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCIBAN : '') .
  '<br />BIC/SWIFT: ' . (defined('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKBIC') ? MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKBIC : '') .
 '<br /><br />La marchandise sera livrée après réception du paiement en question.<br />');

  if (MODULE_PAYMENT_EUSTANDARDTRANSFER_SUCCESS == 'True') {
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_TEXT_INFO', 'Veuillez transférer le montant de facture sur notre compte. Vous recevrez les informations de compte au cours de la dernière étape de la commande.');
  } else {
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_TEXT_INFO', 'Veuillez transférer le montant dû sur notre compte. Vous recevrez les coordonnées bancaires après l\'acceptation de commande par mail.');
  }
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_STATUS_TITLE', 'Activer le module de virement bancaire standard en UE');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_STATUS_DESC', 'Voulez-vous accepter le virement?');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BRANCH_TITLE', 'Bénéficaire');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BRANCH_DESC', 'Bénéficaire du virement');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKNAM_TITLE', 'Nom de la banque');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKNAM_DESC', 'Nom complet de la banque');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNAM_TITLE', 'Code bancaire');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNAM_DESC', 'Le code bancaire du compte affiché.');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNUM_TITLE', 'Numéro de compte');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCNUM_DESC', 'Votre numéro de compte');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCIBAN_TITLE', 'Compte bancaire IBAN');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ACCIBAN_DESC', 'ID Compte international.<br/>(Demander à votre banque si vous n\'en êtes pas sûr.)');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKBIC_TITLE', 'Banque BIC');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_BANKBIC_DESC', 'ID bancaire international.<br/>(Demandez à votre banque si vous n\'en êtes pas sûr.)');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_SORT_ORDER_TITLE', 'Ordre d\'affichage');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_SORT_ORDER_DESC', 'Ordre d\'affichage. Afficher le chiffre le plus petit en premier.');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ALLOWED_TITLE' , 'Zone autorisée');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ALLOWED_DESC' , 'Indiquez <b>séparément</br> les zones autorisées pour ce module. (p.ex. AT,DE (si vide, toutes les zones autorisées))');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ZONE_TITLE' , 'Zone de paiement');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ZONE_DESC' , 'Si une zone est choisie, le mode de paiement n\'est que valable pour cette zone.');
  
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ORDER_STATUS_ID_TITLE' , 'Établir statut de commande');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_ORDER_STATUS_ID_DESC' , 'Moduler les commandes effectuées avec ce module vers ce statut');

define('MODULE_PAYMENT_EUSTANDARDTRANSFER_SUCCESS_TITLE' , 'Afficher les données bancaires');
define('MODULE_PAYMENT_EUSTANDARDTRANSFER_SUCCESS_DESC' , 'Afficher les données bancaires sur la page de succès?');
?>

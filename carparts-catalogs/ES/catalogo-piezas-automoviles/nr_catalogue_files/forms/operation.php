<?php

echo '<h1>' . CommonExtender::LocalizeString('SearchByCustom').' ' . $operation['description'] . '</h1>';

include_once('nr_catalogue_files/guayaquillib/render/catalog/operationform.php');

if (!class_exists('OperationSearchExtender')) {
    class OperationSearchExtender extends CommonExtender
    {
        function FormatLink($type, $dataItem, $catalog, $renderer)
        {
            //BOC set link to content 1001 which includes vehicles.php, noRiddle
            //return 'vehicles.php?ft=execCustomOperation&c=' . $catalog;
            return xtc_href_link(FILENAME_CONTENT, 'coID=1001').'?ft=execCustomOperation&c=' . $catalog;
            //EOC set link to content 1001 which includes vehicles.php, noRiddle
        }
    }
}

$renderer = new GuayaquilOperationSearchForm(new OperationSearchExtender());
echo $renderer->Draw(array_key_exists('c', $_GET) ? $_GET['c'] : '', $operation, @$_GET['data']);

//echo '<br><br>'; //commented out, noRiddle

?>



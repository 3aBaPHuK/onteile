<?php
  /* --------------------------------------------------------------
   $Id: backup.php 1780 2011-02-08 02:09:45Z cybercosmonaut $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'PDFBILL fichiers suivants');

define('TABLE_HEADING_TITLE', 'Nom du fichier');
define('TABLE_HEADING_FILE_DATE', 'Date');
define('TABLE_HEADING_FILE_SIZE', 'Taille');

define('TEXT_LOG_DIRECTORY', 'Répértoire de données:');
define('TEXT_DELETE_INTRO', 'Êtes-vous sûr de vouloir supprimer ce fichier?');

define('SUCCESS_LOG_DELETED', 'Le fichier a été supprimé avec succès.');

define('ERROR_LOG_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le répértoire n\'existe pas.');
define('ERROR_LOG_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le répértoire est en lecture seule.');
define('ERROR_DOWNLOAD_LINK_NOT_ACCEPTABLE', 'Erreur: Le lien de téléchargement n\'est pas acceptable.');
?>

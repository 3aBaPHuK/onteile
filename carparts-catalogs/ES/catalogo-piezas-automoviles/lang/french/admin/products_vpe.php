<?php
/* --------------------------------------------------------------
   $Id: products_vpe.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(orders_status.php,v 1.7 2002/01/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (orders_status.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Unité de conditionnement');

define('TABLE_HEADING_PRODUCTS_VPE', 'Unité de conditionnement');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Veuillez faire tous les changements nécessaires');
define('TEXT_INFO_PRODUCTS_VPE_NAME', 'Unité de conditionnement:');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez entrer la nouvelle UDC avec toutes les données pertinentes');
define('TEXT_INFO_DELETE_INTRO', 'Êtes-vous sûr de vouloir supprimer cette UDC?');
define('TEXT_INFO_HEADING_NEW_PRODUCTS_VPE', 'Nouveau statut de commande.');
define('TEXT_INFO_HEADING_EDIT_PRODUCTS_VPE', 'Editer l\'unité de conditionnement');
define('TEXT_INFO_HEADING_DELETE_PRODUCTS_VPE', 'Supprimer l\'unité de conditionnement');

define('ERROR_REMOVE_DEFAULT_PRODUCTS_VPE', 'Erreur: L\'unité de conditionnement par défaut ne peut pas être supprimée. Veuillez définir une nouvelle UDC par défaut et rééssayez.');
?>

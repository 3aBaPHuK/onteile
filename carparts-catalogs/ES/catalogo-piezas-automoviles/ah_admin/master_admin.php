<?php
/********************************************************************************************************
* file: /master_admin.php
* (c) noRiddle 12-2015
********************************************************************************************************/

defined('NEW_SELECT_CHECKBOX') OR define('NEW_SELECT_CHECKBOX', 'false');
require_once(dirname(__FILE__).'/includes/application_top.php');

//BOC let only admin call this file, noRiddle
$admin_access_query = xtc_db_query("select * from " . TABLE_ADMIN_ACCESS . " where customers_id = '" . (int)$_SESSION['customer_id'] . "'");
$admin_access = xtc_db_fetch_array($admin_access_query);

//if (!isset($_SESSION['customer_id']) || (isset($_SESSION['customer_id']) && ($_SESSION['customers_status']['customers_status_id'] != '0' && $_SESSION['customer_id'] != '1' && $_SESSION['customer_id'] != '13899' && $_SESSION['customer_id'] != '53621'))) { //Andreas, noRiddle, Sergej Barg
if (!isset($_SESSION['customer_id']) || (isset($_SESSION['customer_id']) && ($_SESSION['customers_status']['customers_status_id'] != '0' && $admin_access['master_admin'] != '1'))) {
    xtc_redirect(xtc_href_link(FILENAME_LOGIN, '', 'NONSSL')); 
} 
//EOC let only admin call this file, noRiddle

//BOC define filenames
//define('FILENAME_MASTER_ADMIN', 'master_admin.php');
//EOC define filenames

//BOC define script array
$script_array = array('import_export', 'copy_group_prices', 'verify_keys', /*'set_prod_status',*/ 'copy_content_into_other_shops');
//EOC define script array

$script = $_GET['script'];

include(dirname(__FILE__).'/MasterAdmin/includes/lang_constants.php');

if(isset($script) && in_array($script, $script_array)) {
    include(dirname(__FILE__).'/MasterAdmin/includes/'.$script.'.php');
}

require(DIR_WS_INCLUDES.'head.php');

//meta refresh, see export_into_csv.php
if($at_e->ie_meta_out != false) {
    echo $at_e->ie_meta_out;
}
if($at_i->ie_meta_out != false) {
    echo $at_i->ie_meta_out;
}
?>

<link rel="stylesheet" type="text/css" href="MasterAdmin/css/master_admin.css" />
</head>
<body>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<div id="master-admin">
    <div id="inside-wrap">
        <h1><?php echo ADMIN_TOOLS_HEADING; ?></h1>
        <?php if(!isset($script) || (isset($script) && !in_array($script, $script_array))) { ?>
        <ul class="master-tools-list">
            <li>
                <a href="<?php echo $PHP_SELF.'?script=import_export'; ?>">
                    <p class="mod-name"><?php echo ADMIN_TOOLS_NEW_IMPEXP_SCRIPT_TITLE; ?></p>
                    <p class="mod-desc"><?php echo ADMIN_TOOLS_NEW_IMPEXP_SCRIPT_DESC; ?></p>
                </a>
            </li>
            <li>
                <a href="<?php echo $PHP_SELF.'?script=copy_group_prices'; ?>">
                    <p class="mod-name"><?php echo ADMIN_TOOLS_COPY_CUSTOMERS_STATUS_PRICES_TITLE; ?></p>
                    <p class="mod-desc"><?php echo ADMIN_TOOLS_COPY_CUSTOMERS_STATUS_PRICES_DESC; ?></p>
                </a>
            </li>
            <li>
                <a href="<?php echo $PHP_SELF.'?script=verify_keys'; ?>">
                    <p class="mod-name"><?php echo ADMIN_TOOLS_VERIFY_KEYS_TITLE; ?></p>
                    <p class="mod-desc"><?php echo ADMIN_TOOLS_VERIFY_KEYS_DESC; ?></p>
                </a>
            </li>
            <li>
                <a href="<?php echo $PHP_SELF.'?script=copy_content_into_other_shops'; ?>">
                    <p class="mod-name"><?php echo ADMIN_TOOLS_COPY_CONTENTS_TITLE; ?></p>
                    <p class="mod-desc"><?php echo ADMIN_TOOLS_COPY_CONTENTS_DESC; ?></p>
                </a>
            </li>
            <?php if(!isset($shop)) { //only for originalersatzteile-online.com ?>
            <!--
            <li>
                <a href="<?php echo $PHP_SELF.'?script=set_prod_status'; ?>">
                    <p class="mod-name"><?php echo ADMIN_TOOLS_SET_PROD_STATUS_TITLE; ?></p>
                    <p class="mod-desc"><?php echo ADMIN_TOOLS_SET_PROD_STATUS_DESC; ?></p>
                </a>
            </li>
            -->
            <?php } ?>
        </ul>
        <?php
        } else if(isset($script) && in_array($script, $script_array)) {
        ?>
        <ul class="master-tools-list-hori">
            <li>
                <a href="<?php echo $PHP_SELF; ?>">
                    <p class="mod-name"><?php echo ADMIN_TOOLS_NEW_HOME_TITLE; ?></p>
                </a>
            </li>
        </ul>
        <?php
            include(dirname(__FILE__).'/MasterAdmin/includes/aux_'.$script.'/'.$script.'.html.php');
        }
        ?>
    </div>
</div>
<div id="credits">
    <p>&copy; by noRiddle | kontakt@revilonetz.de | 2015 - <?php echo date('M. Y'); ?></p>
</div>
<?php //require(DIR_WS_INCLUDES . 'footer.php'); // don't include footer because of conflicts with mailhive, hard code it ?>
<?php defined( '_VALID_XTC' ) or die( 'Direct Access to this location is not allowed.' ); ?>
<br />
<table border="0" width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td align="center" class="smallText">
      <?php
      /*
      The following copyright announcement is in compliance
      to section 2c of the GNU General Public License, and
      thus can not be removed, or can only be modified
      appropriately.

      Please leave this comment intact together with the
      following copyright announcement.
  
      Copyright announcement changed due to the permissions
      from LG Hamburg from 28th February 2003 / AZ 308 O 70/03
    */
      ?>
      <a style="text-decoration:none;" href="http://www.modified-shop.org" target="_blank"><span style="color:#B0347E;">mod</span><span style="color:#6D6D6D;">ified eCommerce Shopsoftware</span></a><span style="color:#555555;">&nbsp;&copy;2009-<?php echo date("Y"); ?>&nbsp;provides no warranty and is redistributable under the <a style="color:#555555;text-decoration:none;" href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">GNU General Public License (Version 2)</a><br />eCommerce Engine 2006 based on <a style="text-decoration:none; color:#555555;" href="http://www.xt-commerce.com/" rel="nofollow" target="_blank">xt:Commerce</a></span>
    </td>
  </tr>
  <tr>
    <td><?php echo xtc_image(DIR_WS_IMAGES . 'pixel_trans.gif', '', '1', '5'); ?></td>
  </tr>
</table>
<script>/*<![CDATA[*/!window.jQuery && document.write(unescape('%3Cscript src="//code.jquery.com/jquery-1.11.0.min.js"%3E%3C/script%3E'))/*]]>*/</script>
<script>
/* <![CDATA[ */
$(function(){
    var hide_conts = ['aux-funcs', 'set-funcs', 'copy-single', 'copy-with-factor', 'change-all'];
    $.each(hide_conts, function(idx, val) {
        if(val != 'aux-funcs' && val != 'set-funcs') {
            if($('#'+val + ' .hve-out').is(':empty')) {
                $('#'+val).hide();
            }
        } else {
            $('#'+val).hide();
        }
    });

    $('#tog-aux-funcs, #tog-set-funcs, #tog-copy-single, #top-copy-with-factor, #tog-copy-all-withfact').click(function() { //added new #tog-copy-all-withfact, 02-2021, noRiddle
        $(this).next('div').slideToggle();
    });
    
    //BOC new copy all with factor, 02-2021, noRiddle
    var $ch_po_fct = $('input[name="change_persoff_factor[]"]'),
        ch_po_fct_l = $ch_po_fct.length -1;
    $ch_po_fct.on('keyup keydown keypress', function(e) {
        let keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
        }
        
        if($('#chck-factor-copy').prop('checked')) {
            $ch_po_fct.not(this).val($(this).val());
        }
    });

    $('form#change-all-pers-off-factor').on('submit', function(e) {
        e.preventDefault();

        let sett = 0,
            is_done = true,
            $chpersoff_fact_out = $('#change-persoff-factor-out'),
            lodng_gif = '<img src="images/loading.gif" />';

        $ch_po_fct.each(function(idx) {
            if(is_done === true) {
                setTimeout(function() {
                    is_done = false;
                    $chpersoff_fact_out.append($(lodng_gif));

                    let fact_val = $ch_po_fct.eq(idx).val(),
                        grp = $ch_po_fct.eq(idx).data('grp'),
                        csfrn = $('form#change-all-pers-off-factor input[type="hidden"]').attr('name'),
                        csfrt = $('form#change-all-pers-off-factor input[type="hidden"]').val();
                    //console.log(grp + ' : ' + fact_val);
                    let params = {grp: grp, fact_val: fact_val};
                    params[csfrn] = ""+csfrt+"";
                    if(idx == ch_po_fct_l) {
                        params['is_last'] = 'true';
                    }

                    $.post("<?php echo $PHP_SELF.'?script='.$_GET['script']; ?>", params
                        ).done(function(data) {
                            $chpersoff_fact_out.find('img').remove();
                            $chpersoff_fact_out.append(data);
                            is_done = true;
                        }).fail(function(data) {
                            $chpersoff_fact_out.find('img').remove();
                            $chpersoff_fact_out.append('<pre style="color:#c00;">Meine Freundin Miss Ling hat wieder zugeschlagen (bei ~_'+grp+')</pre>');
                        });
                }, sett);

                sett += 1500;
            }
        });
    });
    //EOC new copy all with factor, 02-2021, noRiddle
    
    //BOC checking checkboxes on index verification
    //vars
    var $anltbls = $('input.anl-tbl').not('[value=csv_import_aux]'),
        $optmtbls = $('input.opt-tbl').not('[value=csv_import_aux]');
        $enlkeys = $('input.enbl-idxs').not('[value=csv_import_aux]');
    
    $('#enblkeys').click(function() {
        $enlkeys.prop('checked', $(this).is(':checked') );
    });
    $('#anlztabls').click(function() {
        $anltbls.prop('checked', $(this).is(':checked') );
    });
    $('#optmztabls').click(function() {
        $optmtbls.prop('checked', $(this).is(':checked') );
    });
    //EOC checking checkboxes on index verification
    //BOC check all aux functions for import
    var $mod_aux_act = $('input.mod-aux-act');
    $('#mod-checkall-auxact').click(function() {
        $mod_aux_act.prop('checked', $(this).is(':checked') );
    });    
    //EOC check all aux functions for import
});
/* ]]> */
</script>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
<?php
/************************************************
* file: add_db_fileds.php
* use: add new fields to products editing
* (c) noRiddle 07-2017
************************************************/

// new for gm_price_status
$add_products_fields[] = 'gm_price_status';
// new for bulk costs
$add_products_fields[] = 'products_bulk';
//dealer price (EK)
$add_products_fields[] = 'products_dealer_price';
?>
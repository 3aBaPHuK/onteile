<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_contact.php 7 2010-04-05 14:40:12Z Standard $
   
   Gambio-Affiliate
   
   modified from
   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 - 2008 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate (affiliate_contact.php, v 1.3 2003/02/16);
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('HEADING_TITLE', 'envoyer un email aux participants du programme d\'affiliation');

define('TEXT_AFFILIATE', 'Partenaire:');
define('TEXT_SUBJECT', 'Objet:');
define('TEXT_FROM', 'Expéditeur:');
define('TEXT_MESSAGE', 'Message:');
define('TEXT_SELECT_AFFILIATE', 'Choisir partenaire');
define('TEXT_ALL_AFFILIATES', 'Tous les partenaires');
define('TEXT_NEWSLETTER_AFFILIATES', 'À tous les abonnées au Newsletter du partenaire');

define('NOTICE_EMAIL_SENT_TO', 'Remarque: email n\'a pas été envoyé à: %s');
define('ERROR_NO_AFFILIATE_SELECTED', 'Erreur: Pas de partenaire a été choisi.');
?>

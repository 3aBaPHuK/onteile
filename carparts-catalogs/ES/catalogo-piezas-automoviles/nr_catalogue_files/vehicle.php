﻿<?php
if(!isset($_SESSION['customer_id'])) xtc_redirect(xtc_href_link(FILENAME_DEFAULT)); //redirect to start page if not logged in, noRiddle

// Include soap request class
include('nr_catalogue_files/guayaquillib/data/requestOem.php');
// Include view class
include('nr_catalogue_files/guayaquillib/render/vehicle/category-floated.php');
include('nr_catalogue_files/guayaquillib/render/vehicle/unitlist-floated.php');
include('nr_catalogue_files/extender.php');

class CategoryExtender extends CommonExtender
{
    function FormatLink($type, $dataItem, $catalog, $renderer)
    {
        $ssd = (string)$dataItem['ssd']; // Получаем SSD категории
        if ($type == 'quickgroup')
            //BOC set link to content 1003 which includes qgroups.php, noRiddle
            //$link = 'qgroups.php?c=' . $catalog . '&vid=' . $renderer->vehicleid . '&ssd=' . $renderer->ssd;
            $link = xtc_href_link(FILENAME_CONTENT, 'coID=1003').'?c=' . $catalog . '&vid=' . $renderer->vehicleid . '&ssd=' . $renderer->ssd;
            //EOC set link to content 1003 which includes qgroups.php, noRiddle
        else
            //BOC set link to content 1004 which includes vehicle.php, noRiddle
            //$link = 'vehicle.php?&c=' . $catalog . '&vid=' . $renderer->vehicleid . '&cid=' . $dataItem['categoryid'] . '&ssd=' . ($ssd ? $ssd : $renderer->ssd);
            $link = xtc_href_link(FILENAME_CONTENT, 'coID=1004').'?c=' . $catalog . '&vid=' . $renderer->vehicleid . '&cid=' . $dataItem['categoryid'] . '&ssd=' . ($ssd ? $ssd : $renderer->ssd);
            //EOC set link to content 1004 which includes vehicle.php, noRiddle

        return $link;
    }
}

class UnitExtender extends CommonExtender
{
    function FormatLink($type, $dataItem, $catalog, $renderer)
    {
        if ($type == 'filter')
            //BOC set link to content 1005 which includes unitfilter.php, noRiddle
            //$link = 'unitfilter.php?c=' . $catalog . '&vid=' . $renderer->vehicle_id . '&uid=' . $dataItem['unitid'] .  '&cid=' . $renderer->categoryid . '&ssd=' . $dataItem['ssd'] . '&f=' . urlencode($dataItem['filter']);
            $link = xtc_href_link(FILENAME_CONTENT, 'coID=1005').'?c=' . $catalog . '&vid=' . $renderer->vehicle_id . '&uid=' . $dataItem['unitid'] .  '&cid=' . $renderer->categoryid . '&ssd=' . $dataItem['ssd'] . '&f=' . urlencode($dataItem['filter']);
            //EOC set link to content 1005 which includes unitfilter.php, noRiddle
        else
            //BOC set link to content 1006 which includes unit.php, noRiddle
            //$link = 'unit.php?c=' . $catalog . '&vid=' . $renderer->vehicle_id . '&uid=' . $dataItem['unitid'] . '&cid=' . $renderer->categoryid . '&ssd=' . $dataItem['ssd'];
            $link = xtc_href_link(FILENAME_CONTENT, 'coID=1006').'?c=' . $catalog . '&vid=' . $renderer->vehicle_id . '&uid=' . $dataItem['unitid'] . '&cid=' . $renderer->categoryid . '&ssd=' . $dataItem['ssd'];
            //EOC set link to content 1006 which includes unit.php, noRiddle

        return $link;
        //return 'unit.php?c='.$catalog.'&uid='.$dataItem['unitid'].'&ssd='.$dataItem['ssd'];
    }
}

// Create request object
$request = new GuayaquilRequestOEM($_GET['c'], $_GET['ssd'], Config::$catalog_data);
if (Config::$useLoginAuthorizationMethod) {
    $request->setUserAuthorizationMethod(Config::$userLogin, Config::$userKey);
}

// Append commands to request
$request->appendGetCatalogInfo();
$request->appendGetVehicleInfo($_GET['vid']);
$request->appendListCategories($_GET['vid'], isset($_GET['cid']) ? $_GET['cid'] : -1);
$request->appendListUnits($_GET['vid'], isset($_GET['cid']) ? $_GET['cid'] : -1);

// Execute request
$data = $request->query();

// Check errors
if ($request->error != '') {
    echo $request->error;
} else {
    $catalogInfo = $data[0]->row;
    $vehicle = $data[1]->row;
    $categories = $data[2];
    $units = $data[3];

    if (array_key_exists('checkQG', $_GET) && CommonExtender::isFeatureSupported($catalogInfo, 'quickgroups')) {
        //BOC set link to content 1003 which includes qgroups.php, noRiddle
        //$link = 'qgroups.php?c=' . $_GET['c'] . '&vid=' . $_GET['vid'] . '&ssd=' . $_GET['ssd'] . '&path_data=' . $_GET['path_data'];
        $link = xtc_href_link(FILENAME_CONTENT, 'coID=1003').'?c=' . $_GET['c'] . '&vid=' . $_GET['vid'] . '&ssd=' . $_GET['ssd'] . '&path_data=' . $_GET['path_data'];
        //BOC set link to content 1003 which includes qgroups.php, noRiddle
        header("Location: ". $link);
        exit();
    }

    //echo '<h1>' . CommonExtender::FormatLocalizedString('CarName', $vehicle['name']) . '</h1>';
    //echo '<div class="col-md-4 bord-e3">';
    echo '<h1>'
         //.'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.Config::$nr_icon_arr[$_GET['c']].'" alt ="" />&nbsp;'
         .'<img src="nr_catalogue_files/guayaquillib/render/catalogs/imagesclear/60x60/'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['brand_icon'].'" alt ="'.$_SESSION['nr_ctlg_brands'][$_GET['c']]['our_brand'].'" />&nbsp;'
         .CommonExtender::FormatLocalizedString('CarName', $vehicle['name'])
         .'</h1>';
    
    echo '<h2>'.CommonExtender::FormatLocalizedString('ImageDetails', $vehicle['name']).'</h2>'; //display also 'parts by images', noRiddle

    echo '<div id="pagecontent">';

    $renderer = new GuayaquilCategoriesList(new CategoryExtender());
    $renderer->vehicle_id = $_GET['vid'];
    $renderer->categoryid = array_key_exists('cid', $_GET) ? $_GET['cid'] : -1;
    echo $renderer->Draw($_GET['c'], $categories, $renderer->vehicle_id, $renderer->categoryid, $_GET['ssd'], $catalogInfo);

    $renderer = new GuayaquilUnitsList(new UnitExtender());
    $renderer->vehicle_id = $_GET['vid'];
    $renderer->categoryid = array_key_exists('cid', $_GET) ? $_GET['cid'] : -1;
    $renderer->imagesize = 250;
    echo $renderer->Draw($_GET['c'], $units);

    echo '</div>';
}
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id: ap.php 5118 2013-07-18 10:58:36Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(ap.php,v 1.02 2003/02/18); www.oscommerce.com 
   (c) 2003	 nextcommerce (ap.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   austrian_post_1.05       	Autor:	Copyright (C) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/
   

define('MODULE_SHIPPING_AP_TEXT_TITLE', 'österreichische Post AG (poste autrichienne');
define('MODULE_SHIPPING_AP_TEXT_DESCRIPTION', 'österreichische Post AG - envoi mondial');
define('MODULE_SHIPPING_AP_TEXT_WAY', 'Envoi vers');
define('MODULE_SHIPPING_AP_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_AP_INVALID_ZONE', 'Malheureusement il n\'est pas possible d\'envoyer dans ce pays');
define('MODULE_SHIPPING_AP_UNDEFINED_RATE', 'Les frais de port ne peuvent pas être calculés pour le moment');

define('MODULE_SHIPPING_AP_STATUS_TITLE' , 'Österreichische Post AG (poste autrichienne)');
define('MODULE_SHIPPING_AP_STATUS_DESC' , 'Voulez-vous offrir l\'envoi avec la Österreichische Post AG?');
define('MODULE_SHIPPING_AP_TAX_CLASS_TITLE' , 'Taux d\'imposition');
define('MODULE_SHIPPING_AP_TAX_CLASS_DESC' , 'Séléctionnez le taux de TVA pour ce type d\'envoi');
define('MODULE_SHIPPING_AP_ZONE_TITLE' , 'Zone de livraison');
define('MODULE_SHIPPING_AP_ZONE_DESC' , 'Si vous séléctionnez une zone, ce type d\'envoi sera seulement proposé dans cette zone.');
define('MODULE_SHIPPING_AP_SORT_ORDER_TITLE' , 'Ordre de l\'affichage');
define('MODULE_SHIPPING_AP_SORT_ORDER_DESC' , 'Les plus bas sont affichés d\'abord.');
define('MODULE_SHIPPING_AP_ALLOWED_TITLE' , 'Zones d\'envoi individuelles');
define('MODULE_SHIPPING_AP_ALLOWED_DESC' , 'Indiquez les zones <b>individuellement</b> dans lesquelles l\'envoi est possible. Par exemple AT,DE');
define('MODULE_SHIPPING_AP_NUMBER_ZONES_TITLE' , 'Nombre de zones');
define('MODULE_SHIPPING_AP_NUMBER_ZONES_DESC' , 'Nombre des zones à disposition');
define('MODULE_SHIPPING_AP_DISPLAY_TITLE' , 'Activer l\'affichage');
define('MODULE_SHIPPING_AP_DISPLAY_DESC' , 'Voulez-vous afficher quand l\'envoi dans un certain pays n\'est pas possible ou si les frais de port ne peuvent pas être calculés?');

for ($module_shipping_ap_i = 1; $module_shipping_ap_i <= MODULE_SHIPPING_AP_NUMBER_ZONES; $module_shipping_ap_i ++) {
define('MODULE_SHIPPING_AP_COUNTRIES_'.$module_shipping_ap_i.'_TITLE' , '<hr/>Zone '.$module_shipping_ap_i.' Pays');
define('MODULE_SHIPPING_AP_COUNTRIES_'.$module_shipping_ap_i.'_DESC' , 'Liste de codes ISO de pays (2 signes) séparée par des virgules qui font partis de la zone '.$module_shipping_ap_i.' (indiquez WORLD pour le reste du monde.).');
define('MODULE_SHIPPING_AP_COST_'.$module_shipping_ap_i.'_TITLE' , 'Zone '.$module_shipping_ap_i.' Tableau de tarifs jusqu\'à 20kg');
define('MODULE_SHIPPING_AP_COST_'.$module_shipping_ap_i.'_DESC' , 'Frais de port vers la zone '.$module_shipping_ap_i.' Destinations basées sur un groupe de poids de commandes maximal. Par exemple: 3:8.50,7:10.50,... Poids de moins un égal à 3 couterait 8.50 pour la zone '.$module_shipping_ap_i.' destinations.');
define('MODULE_SHIPPING_AP_HANDLING_'.$module_shipping_ap_i.'_TITLE' , 'Zone '.$module_shipping_ap_i.' Frais de traitement');
define('MODULE_SHIPPING_AP_HANDLING_'.$module_shipping_ap_i.'_DESC' , 'Frais de traitement pour cette zone d\'envoi');
}
?>

<?php
/*******************************************************************************
* file: cat_cart_ajax_call.php
* path: /includes/extra/application_bottom/
* use: post ajax call to receive on-the-fly token for cart link to remote shops
*
* (c) 07-2020, noRiddle
*******************************************************************************/

if(basename($PHP_SELF, '.php') == 'catalogue_shopping_cart') {
?>
<script>
$(function() {
    var $rem_gen = $('.rem-gen'),
        loading_spinner = '<i class="fa fa-spinner working-gif"></i>';

    $rem_gen.each(function() {
        let $this = $(this);

        $this.on("click", function(e){
            e.preventDefault();
            let oldhref = $this.data('lnk'),
                shp = $this.data('shp'),
                hid_lnk = $('#click-'+shp);

                $(loading_spinner).insertAfter(hid_lnk);

            $.post('ajax.php',
                {ext: 'cat_cart_ajax_url',
                 type: 'plain',
                 shop: shp,
                 cc_create_sec_remote: 'csr_true'
                },
                function(data){
                    let new_url = oldhref + data;

                    $this.attr('href', new_url);
                    hid_lnk.attr('href', new_url);

                    setTimeout(function() {$('#click-'+shp)[0].click();}, 500);
                    $(loading_spinner).remove();
                }
            );
        });
    });
});
</script>
<?php
}
?>
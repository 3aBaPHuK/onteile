<?php
/* --------------------------------------------------------------
   $Id: stats_sales_report.php 1311 2005-10-18 12:30:40Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(stats_sales_report.php,v 1.6 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (stats_sales_report.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/
 

define('REPORT_DATE_FORMAT', 'j.m.A.');

define('HEADING_TITLE', 'rapport de vente');

define('REPORT_SALES_STATISTICS', 'Filtre de statistiques de vente');
define('REPORT_TYPE_YEARLY', 'Annuel');
define('REPORT_TYPE_MONTHLY', 'Mensuel');
define('REPORT_TYPE_WEEKLY', 'Hebdomadaire');
define('REPORT_TYPE_DAILY', 'Quotidien');
define('REPORT_START_DATE', 'de date');
define('REPORT_END_DATE', 'à date (inclus)');
define('REPORT_DETAIL', 'Détail');
define('REPORT_MAX', 'Montrer les meilleurs');
define('REPORT_ALL', 'Tous');
define('REPORT_SORT', 'Tri');
define('REPORT_EXP', 'Export');
define('REPORT_SEND', 'Envoyer');
define('EXP_NORMAL', 'Normal');
define('EXP_HTML', 'Seulement HTML');
define('EXP_CSV', 'CSV');

define('TABLE_HEADING_DATE', 'Date');
define('TABLE_HEADING_ORDERS', '# commandes');
define('TABLE_HEADING_ITEMS', '# articles');
define('TABLE_HEADING_REVENUE', 'Chiffre d\'affaires');
define('TABLE_HEADING_SHIPPING', 'Expédition');

define('DET_HEAD_ONLY', 'Pas de détails');
define('DET_DETAIL', 'Montrer les détails');
define('DET_DETAIL_ONLY', 'Détails avec montant');

define('SORT_VAL0', 'Standard');
define('SORT_VAL1', 'Description');
define('SORT_VAL2', 'Description à partir de');
define('SORT_VAL3', '# articles');
define('SORT_VAL4', '# articles à partir de');
define('SORT_VAL5', 'Chiffre d\'affaires');
define('SORT_VAL6', 'Chiffre d\'affaires à partir de');

define('REPORT_STATUS_FILTER', 'Statut');
define('REPORT_PAYMENT_FILTER', 'Mode de paiement');

define('SR_SEPARATOR1', ';');
define('SR_SEPARATOR2', ';');
define('SR_NEWLINE', '<br />');
?>

<?php
/************************************************
* file: product_bulk_deposit.php
* use: calculate bulk costs and deposit costs
* (c) noRiddle 05-2017
************************************************/

//BOC products_bulk (Sperrgtkosten)
$info_smarty->assign('PRODUCTS_BULK', ''); //overwrite from product class
if(defined('MODULE_ORDER_TOTAL_BULKGOODS_STATUS') && MODULE_ORDER_TOTAL_BULKGOODS_STATUS == 'true') {
    global $xtPrice;

    $nr_factor = (constant('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$_SESSION['customers_status']['customers_status']) > 0) ? ((float)constant('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$_SESSION['customers_status']['customers_status'])) : 1;

    //echo '<br /><br /><pre>'.constant('MODULE_ORDER_TOTAL_BULKGOODS_CUSTSTATUS_FACTOR_'.$_SESSION['customers_status']['customers_status']).'</pre>';
    //echo '<br /><br /><pre>'.$_SESSION['customers_status']['customers_status'].'</pre>';

    if($product->data['products_bulk'] > 0) {
        $nr_tot_bulk_costs = $xtPrice->xtcFormat(($product->data['products_bulk'] * $nr_factor), true, $product->data['products_tax_class_id'], true);
        $info_smarty->assign('PRODUCTS_BULK', $nr_tot_bulk_costs);
    } else {
        $info_smarty->assign('PRODUCTS_BULK', '');
    }
}
//EOC products_bulk (Sperrgtkosten)
?>
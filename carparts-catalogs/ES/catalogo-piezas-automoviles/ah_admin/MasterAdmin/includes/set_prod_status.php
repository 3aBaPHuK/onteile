<?php
/*******************************************************************************************
* set products status of certain products_supplier_id to 0
* file: /set_prod_status.php
* (c) noRiddle 02-2016
*******************************************************************************************/

$set_prod_out_0 = '';
$set_prod_out_1 = '';

if(isset($_GET['action']) && $_GET['action'] == 'set_prod_status_0') {
    if(isset($_POST['supplier_id_0'])) {
        if($_POST['supplier_id_0'] != '' && is_numeric($_POST['supplier_id_0'])) {
            if(xtc_db_query("UPDATE ".TABLE_PRODUCTS." SET products_status = 0 WHERE products_supplier_id = ".(int)$_POST['supplier_id_0'])) {
                $set_prod_out_0 .= sprintf(ADMIN_TOOLS_SET_PROD_STATUS_SUCCESS_TXT_0, $_POST['supplier_id_0']);
            }
        } else {
            $set_prod_out_0 .= ADMIN_TOOLS_SET_PROD_STATUS_ERROR_TXT;
        }
    }
}

if(isset($_GET['action']) && $_GET['action'] == 'set_prod_status_b_0') {
    if(isset($_POST['supplier_id_b_01']) && isset($_POST['supplier_id_b_02'])) {
        if($_POST['supplier_id_b_01'] != '' && $_POST['supplier_id_b_02'] != '' && is_numeric($_POST['supplier_id_b_01']) && is_numeric($_POST['supplier_id_b_02'])) {
            if(xtc_db_query("UPDATE ".TABLE_PRODUCTS." SET products_status = 0 WHERE products_supplier_id BETWEEN ".(int)$_POST['supplier_id_b_01']." AND ".(int)$_POST['supplier_id_b_02'])) {
                $set_prod_out_b_0 .= sprintf(ADMIN_TOOLS_SET_PROD_STATUS_SUCCESS_TXT_B_0, $_POST['supplier_id_b_01'], $_POST['supplier_id_b_02']);
            }
        } else {
            $set_prod_out_b_0 .= ADMIN_TOOLS_SET_PROD_STATUS_ERROR_TXT;
        }
    }
}

if(isset($_GET['action']) && $_GET['action'] == 'set_prod_status_1') {
    if(isset($_POST['supplier_id_1'])) {
        if($_POST['supplier_id_1'] != '' && is_numeric($_POST['supplier_id_1'])) {
            if(xtc_db_query("UPDATE ".TABLE_PRODUCTS." SET products_status = 1 WHERE products_supplier_id = ".(int)$_POST['supplier_id_1'])) {
                $set_prod_out_1 .= sprintf(ADMIN_TOOLS_SET_PROD_STATUS_SUCCESS_TXT_1, $_POST['supplier_id_1']);
            }
        } else {
            $set_prod_out_1 .= ADMIN_TOOLS_SET_PROD_STATUS_ERROR_TXT;
        }
    }
}
?>
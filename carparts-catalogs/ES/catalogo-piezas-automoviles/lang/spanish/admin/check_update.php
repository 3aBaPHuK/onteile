<?php
/* --------------------------------------------------------------
   $Id: check_update.php 10383 2016-11-07 08:48:16Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(customers.php,v 1.13 2002/06/15); www.oscommerce.com
   (c) 2003 nextcommerce (customers.php,v 1.8 2003/08/15); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Actualización de software');
define('HEADING_SUBTITLE', 'Verificación de actualización');

define('TEXT_DB_VERSION', 'Versión de la base de datos:');
define('TEXT_INFO_UPDATE_RECOMENDED', '<div class="error_message">Está disponible una nueva versión. Puede descargarlo aquí: <a href="http://www.modified-shop.org/download" target="_blank">http://www.modified-shop.org/download</a></div>');
define('TEXT_INFO_UPDATE_NOT_POSSIBLE', '<div class="error_message">Lamentablemente no se ha podido realizar ninguna comprobación. Por favor visite nuestra <a target="_blank" href="http://www.modified-shop.org"><b>Página web</b></a>.</div>');
define('TEXT_INFO_UPDATE', '<div class="success_message">Su versión es actual.</div>');

define('TEXT_HEADING_DEVELOPERS', 'Programador del modified eCommerce Shopsoftware:');
define('TEXT_HEADING_SUPPORT', 'Apoye el desarrollo futuro:');
define('TEXT_HEADING_DONATIONS', 'Donaciones:');
define('TEXT_HEADING_BASED_ON', 'El Shopsoftware se basa en:');

define('TEXT_INFO_THANKS', 'Agradecemos a todos los programadores y desarrolladores que trabajan en este proyecto. Si hemos olvidado a alguien en la lista de abajo, le pedimos que nos informe a través del <a style="font-size: 12px; text-decoration: underline;" href="http://www.modified-shop.org/forum/" target="_blank">Foro</a> o a uno de los desarrolladores mencionados.');
define('TEXT_INFO_DISCLAIMER', 'Este programa fue publicado con la esperanza de ser útil. Sin embargo, no damos ninguna garantía de que la implementación esté libre de errores.');
define('TEXT_INFO_DONATIONS', 'El modified eCommerce Shopsoftware es un proyecto de OpenSource - invertimos mucho trabajo y tiempo libre en este proyecto y por lo tanto agradeceríamos una donación como un pequeño reconocimiento.');
define('TEXT_INFO_DONATIONS_IMG_ALT', 'Apoya este proyecto con su donación');
define('BUTTON_DONATE', '<a href="http://www.modified-shop.org/spenden"><img src="https://www.paypal.com/de_DE/DE/i/btn/btn_donateCC_LG.gif" alt="' . TEXT_INFO_DONATIONS_IMG_ALT . '" border="0"></a>');
?>

<?php
/* -----------------------------------------------------------------------------------------
   $Id: cao_faktura.php 10299 2016-09-24 07:25:17Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_CAO_FAKTURA_TEXT_TITLE', 'CAO Faktura');
define('MODULE_CAO_FAKTURA_TEXT_DESCRIPTION', 'CAO Faktura Connector');
define('MODULE_CAO_FAKTURA_STATUS_TITLE', '¿Activar módulo?');
define('MODULE_CAO_FAKTURA_STATUS_DESC', 'Activar CAO Faktura');

define('MODULE_CAO_FAKTURA_EMAIL_TITLE', 'Dirección de correo electrónico');
define('MODULE_CAO_FAKTURA_EMAIL_DESC', 'Por favor, introduzca su dirección de correo electrónico. Esta debe coincidir con la dirección de correo electrónico introducida en CAO Faktura.');

define('MODULE_CAO_FAKTURA_PASSWORD_TITLE', 'Contraseña');
define('MODULE_CAO_FAKTURA_PASSWORD_DESC', 'Por favor, introduzca una contraseña. Esto debe coincidir con la contraseña introducida en CAO Faktura.');

?>

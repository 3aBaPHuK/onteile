<?php
/*******************************************************
* file: add_wysiwyg.php
* use: define additional wysiwyg editors to use
* (c) noRiddle 04-2017
*******************************************************/

//BOC load frontend css in editor, noRiddle
$css_path = '../templates/'.CURRENT_TEMPLATE.'/stylesheet.css';
$css_path2 = '../templates/'.CURRENT_TEMPLATE.'/editor.css'; //Wichtig f�r Hintergrund: html,body definieren
$customConfig['contentsCss'] = "contentsCss: ['".$css_path."','".$css_path2."'],";
//$customConfig['contentsCss'] = "contentsCss: ['".$css_path."'],";
//EOC load frontend css in editor, noRiddle

$customConfig['extraAllowedContent'] = "extraAllowedContent: 'ins(*){*}[*]',";

//BOC load custom templates file (Vorlage in Editor), noRiddle
$customConfig['templates_files'] = "templates_files: ['".DIR_WS_CATALOG.DIR_ADMIN."includes/modules/ckeditor/plugins/templates/templates/ah_default.js'],";
//EOC load custom templates file (Vorlage in Editor), noRiddle

//BOC load own styles to use custom tabs, noRiddle
//$customConfig['stylesSet'] = "stylesSet: ['default:../".DIR_ADMIN."includes/modules/ckeditor/ah_styles.js'],";
$customConfig['stylesSet'] =  "stylesSet : 'ah_styles:".DIR_WS_CATALOG.DIR_ADMIN."includes/modules/ckeditor/ah_styles.js',";
//EOC load own styles to use custom tabs, noRiddle

//BOC imageslider editor
if($type == 'imagesliders_description') {
    $editorName = 'imagesliders_description['.$langID.']';
    $default_editor_height = 400;
}
if($type == 'imagesliders_video_expl') {
    $editorName = 'imagesliders_video_expl['.$langID.']';
    $default_editor_height = 400;
}
//EOC imageslider editor

//BOC placeholder
if($type == 'ph_value') {
    $editorName = 'ph_value['.$langID.']';
    $default_editor_width = 840;
    $default_editor_height = 100;
}
//EOC placeholder

//echo '<br /><br /><br /><br /><pre>'.print_r($customConfig, true).'</pre>';

//BOC don't remove empty tags, we want to use font awesome icons, 03-2021, noRiddle
$html .= '$(function() {
            $.each(CKEDITOR.dtd.$removeEmpty, function (i, value) {
                CKEDITOR.dtd.$removeEmpty[i] = false;
            })
           })';
//EOC don't remove empty tags, we want to use font awesome icons, 03-2021, noRiddle
?>
<?php
/**************************************************
* sitemap creator per ajax
* (c) Mario Maeles and noRiddle 07-2015
**************************************************/

function get_entries($off = '', $lim = '') { //new vars for LIMIT, noRiddle
    //BOC set LIMIT, noRiddle
    if(empty($off) && !empty($lim)) {
        $limit = 'LIMIT '.$lim;
    } elseif(!empty($off) && !empty($lim)) {
        $limit = 'LIMIT '.$off.', '.$lim;
    }
    //EOC set LIMIT, noRiddle
    $result_array = array();
    
    if($off == '' && $lim == '') { //make separate count query since otherwise it will take too long, noRiddle
        $query = "SELECT COUNT(*) as cnt
                   FROM content_manager cm
                   JOIN languages l
                     ON cm.languages_id = l.languages_id
                    AND l.status = 1
                  WHERE cm.content_group BETWEEN 1105 AND 1930
                    AND cm.content_group NOT LIKE '%0'
                    AND cm.content_active = 1";
                    
        $result = xtc_db_query($query);
        $contents = xtc_db_fetch_array($result);
        $result_array[] = $contents['cnt'];
    } else {        
        $query = "SELECT cm.content_title, cm.content_group, cm.last_modified, l.code
                    FROM content_manager cm
                    JOIN languages l
                      ON cm.languages_id = l.languages_id
                     AND l.status = 1
                   WHERE cm.content_group BETWEEN 1105 AND 1930
                     AND cm.content_group NOT LIKE '%0'
                     AND cm.content_active = 1
                ORDER BY cm.content_group, l.code ".$limit;

        $result = xtc_db_query($query);
        
        while ($contents = xtc_db_fetch_array($result)) {
            $result_array[] = array('type' => 'content',
                                    'id' => $contents['content_group'],
                                    'content_name' => trim($contents['content_title']),
                                    'last_modified' => $contents['last_modified'],
                                    'lang' => $contents['code']
                                   );
        }
        
        //xtc_db_free_result($result); //use this ??
        //EOC get all in one query not to have queries in loop in create_sitemap_loop.php, noRiddle
    }
        
    return $result_array;
}
?>
<?php
/* -----------------------------------------------------------------------------------------
   $Id: gls.php 5121 2013-07-18 11:38:19Z Tomcraft $   

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(dp.php,v 1.4 2003/02/18 04:28:00); www.oscommerce.com 
   (c) 2003	nextcommerce (dp.php,v 1.5 2003/08/13); www.nextcommerce.org
   (c) 2009	shd-media (gls.php 899 27.05.2009);

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   GLS (German Logistic Service) based on DP (Deutsche Post)        
   (c) 2002 - 2003 TheMedia, Dipl.-Ing Thomas Pl&auml;nkers | http://www.themedia.at & http://www.oscommerce.at
    
   GLS contribution made by shd-media (c) 2009 shd-media - www.shd-media.de
   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_GLS_TEXT_TITLE', 'GLS');
define('MODULE_SHIPPING_GLS_TEXT_DESCRIPTION', 'GLS -  Módulo de envío mundial');
define('MODULE_SHIPPING_GLS_TEXT_WAY', ' Envíos a ');
define('MODULE_SHIPPING_GLS_POSTCODE_INFO_TEXT', 'Incluido el recargo por isla');
define('MODULE_SHIPPING_GLS_TEXT_UNITS', 'kg');
define('MODULE_SHIPPING_GLS_INVALID_ZONE', 'No es posible realizar un envío a este país.');
define('MODULE_SHIPPING_GLS_UNDEFINED_RATE', 'Los costos de envío no se pueden calcular por el momento.');

define('MODULE_SHIPPING_GLS_STATUS_TITLE' , 'GLS');
define('MODULE_SHIPPING_GLS_STATUS_DESC' , '¿Desea ofrecer envíos a través de GLS?');
define('MODULE_SHIPPING_GLS_HANDLING_TITLE' , 'Costo de procesamiento');
define('MODULE_SHIPPING_GLS_HANDLING_DESC' , 'Costo de procesamiento para este modo de envío en Euro');
define('MODULE_SHIPPING_GLS_TAX_CLASS_TITLE' , 'Tasa de impuesto');
define('MODULE_SHIPPING_GLS_TAX_CLASS_DESC' , 'Seleccione el tipo de IVA para este modo de envío.');
define('MODULE_SHIPPING_GLS_ZONE_TITLE' , 'Zona de envío');
define('MODULE_SHIPPING_GLS_ZONE_DESC' , 'Si selecciona una zona, este modo de envío sólo se ofrecerá en esa zona.');
define('MODULE_SHIPPING_GLS_SORT_ORDER_TITLE' , 'Orden de la visualización');
define('MODULE_SHIPPING_GLS_SORT_ORDER_DESC' , 'Menor se muestra en primer lugar.');
define('MODULE_SHIPPING_GLS_ALLOWED_TITLE' , 'Zonas de envío individuales');
define('MODULE_SHIPPING_GLS_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE');

define('MODULE_SHIPPING_GLS_COUNTRIES_1_TITLE' , 'Países de la GLS Zona 1');
define('MODULE_SHIPPING_GLS_COUNTRIES_1_DESC' , 'Lista separada por comas de los códigos de país ISO de dos caracteres que forman parte de la zona 1 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_GLS_COST_1_TITLE' , 'Tabla de costos de envío de GLS Zona 1');
define('MODULE_SHIPPING_GLS_COST_1_DESC' , 'Los costos de envío para los países de la Zona 1 se basan en una indicación de peso (de a) del pedido. Ejemplo: 0-3:8.50,3-7:10.50,etc. Pesos superiores a 0 e inferiores o iguales a 3 serían 8,50 para los países de la zona 1.');

define('MODULE_SHIPPING_GLS_COUNTRIES_2_TITLE' , 'Países de la GLS Zona 2');
define('MODULE_SHIPPING_GLS_COUNTRIES_2_DESC' , 'Lista separada por comas de los códigos de país ISO de dos caracteres que forman parte de la zona 2 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_GLS_COST_2_TITLE' , 'Tabla de costos de envío de GLS Zona 2');
define('MODULE_SHIPPING_GLS_COST_2_DESC' , 'Los costos de envío para los países de la Zona 2 se basan en una indicación de peso (de a) del pedido. Ejemplo: 0-3:8.50,3-7:10.50,etc. Pesos superiores a 0 e inferiores o iguales a 3 serían 8,50 para los países de la zona 2.');

define('MODULE_SHIPPING_GLS_COUNTRIES_3_TITLE' , 'Países de la GLS Zona 3');
define('MODULE_SHIPPING_GLS_COUNTRIES_3_DESC' , 'Lista separada por comas de los códigos de país ISO de dos caracteres que forman parte de la zona 3 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_GLS_COST_3_TITLE' , 'Tabla de costos de envío de GLS Zona 3');
define('MODULE_SHIPPING_GLS_COST_3_DESC' , 'Los costos de envío para los países de la Zona 3 se basan en una indicación de peso (de a) del pedido. Ejemplo: 0-3:8.50,3-7:10.50,etc. Pesos superiores a 0 e inferiores o iguales a 3 serían 8,50 para los países de la zona 3.');

define('MODULE_SHIPPING_GLS_COUNTRIES_4_TITLE' , 'Países de la GLS Zona 4');
define('MODULE_SHIPPING_GLS_COUNTRIES_4_DESC' , 'Lista separada por comas de los códigos de país ISO de dos caracteres que forman parte de la zona 4 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_GLS_COST_4_TITLE' , 'Tabla de costos de envío de GLS Zona 4');
define('MODULE_SHIPPING_GLS_COST_4_DESC' , 'Los costos de envío para los países de la Zona 4 se basan en una indicación de peso (de a) del pedido. Ejemplo: 0-3:8.50,3-7:10.50,etc. Pesos superiores a 0 e inferiores o iguales a 3 serían 8,50 para los países de la zona 4.');

define('MODULE_SHIPPING_GLS_COUNTRIES_5_TITLE' , 'Países de la GLS Zona 5');
define('MODULE_SHIPPING_GLS_COUNTRIES_5_DESC' , 'Lista separada por comas de los códigos de país ISO de dos caracteres que forman parte de la zona 5 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_GLS_COST_5_TITLE' , 'Tabla de costos de envío de GLS Zona 5');
define('MODULE_SHIPPING_GLS_COST_5_DESC' , 'Los costos de envío para los países de la Zona 5 se basan en una indicación de peso (de a) del pedido. Ejemplo: 0-3:8.50,3-7:10.50,etc. Pesos superiores a 0 e inferiores o iguales a 3 serían 8,50 para los países de la zona 5.');

define('MODULE_SHIPPING_GLS_COUNTRIES_6_TITLE' , 'Países de la GLS Zona 6');
define('MODULE_SHIPPING_GLS_COUNTRIES_6_DESC' , 'Lista separada por comas de los códigos de país ISO de dos caracteres que forman parte de la zona 6 (introduzca WORLD para el resto del mundo.).');
define('MODULE_SHIPPING_GLS_COST_6_TITLE' , 'Tabla de costos de envío de GLS Zona 6');
define('MODULE_SHIPPING_GLS_COST_6_DESC' , 'Los costos de envío para los países de la Zona 6 se basan en una indicación de peso (de a) del pedido. Ejemplo: 0-3:8.50,3-7:10.50,etc. Pesos superiores a 0 e inferiores o iguales a 3 serían 8,50 para los países de la zona 6.');

define('MODULE_SHIPPING_GLS_POSTCODE_TITLE' , 'GLS Recargo por isla - Códigos postales');
define('MODULE_SHIPPING_GLS_POSTCODE_DESC' , 'Zonas de códigos postales');
define('MODULE_SHIPPING_GLS_POSTCODE_EXTRA_COST_TITLE' , 'GLS Recargo por isla - costos');
define('MODULE_SHIPPING_GLS_POSTCODE_EXTRA_COST_DESC' , 'Recargo por isla: Indique aquí cuánto debe añadirse a los costos de envío si la dirección de entrega se encuentra en una isla alemana.');
?>

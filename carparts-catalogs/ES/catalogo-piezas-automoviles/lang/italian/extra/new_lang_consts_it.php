<?php
/**************************************
* file: new_lang_consts_en.php
* use: new language constants
* (c) noRiddle 01-2017
**************************************/

define('AH_IMAGE_BUTTON_SEARCH', 'INSERT PART NUMBER HERE');
//new error for invalid part no coming from search_results.php
define('AH_JS_AT_LEAST_ONE_INPUT', 'Unfortunately, we could not find a spare part with the given number.\nMaybe we can help you:');
//errors for new field VIN in checkout_payment, noRiddle
define('ERROR_NO_VIN_INDICATED', 'Please indicate your Vehicle Identification Number (VIN 17-digit). <br />With this information we assure that your ordered parts fit into your car.<br />In case the VIN is not relevant for your order, please put the word "no" in the VIN field.');
define('ERROR_VIN_NOT_PLAUSIBLE', 'Please indicate a valid Vehicle Identification Number (VIN 17-digit). <br />With this information we assure that your ordered parts fit into your car.');
//new email fields, noRiddle
define('EMAIL_PRODUCTS_PRICE_QUESTION', 'Product enquiry for: ');
define('EMAIL_VIN', 'Vehicle identification number (VIN): ');
define('AH_ERROR_VIN_EMPTY', '<li><b>Vehicle identification number (VIN):</b><br />Please indicate your Vehicle Identification Number (VIN 17-digits).<br />With this information we assure that the part you request information about fits into your car.</li>');
define('AH_ERROR_VIN_NOPLAUSIBLE', '<li><b>Vehicle identification number (VIN):</b><br />Please indicate your Vehicle Identification Number (VIN 17-digits).<br />With this information we assure that the part you request information about fits into your car.</li>');

//BOC SEO meta title texts, noRiddle
define('SEO_META_TITLE_1', 'buy online at a good price');
define('SEO_META_TITLE_2', 'spare part online');
define('SEO_META_TITLE_3', 'order spare part online');
define('SEO_META_TITLE_4', 'for online car catalogue VIN search');
//EOC SEO meta title texts, noRiddle

define('NR_GOOG_DEFAULT_TXT', 'Additional languages'); //google translator

//BOC new for ADAC field, noRiddle
define('ENTRY_ADAC_NO_FALSE', 'Your entered ADAC-member no. is not correct.<br />Please verify it.<br />You may create an account without ADAC-member no. any time.<br />In this case just leave the respective field empty.');
define('ENTRY_ACCOUNT_ADAC_NO_FALSE', 'Your entered ADAC-member no. is not correct.<br />Please verify it.');
define('TXT_SUCCESS_ADAC_MEMBER', 'Your customers status has been set to ADAC member. You can now buy for reduced prices.');
define('TXT_ERROR_ADAC_MEMBER', 'Unfortunately your customers status could not be updated to ADAC member, we apologise.<br />Please <a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=7').'" title="Contact">contact</a> us for this matter.');
//EOC new for ADAC field, noRiddle

//BOC inclusion for affiliate program, noRiddle
require(DIR_WS_LANGUAGES .'english/'.'affiliate_english.php');
//EOC inclusion for affiliate program, noRiddle

//BOC additional text for login.php, noRiddle
define('ADD_TEXT_GUEST_LOGIN', 'Furthermore you can only profit from advantages like <a class="iframe" href="'.xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=964').'" title="Rabatte" target="_blank">discounts for ADAC-, club- and garages</a>, as a registered client.');
define('ADD_TEXT_NEW_LOGIN', 'We attach maximum importance to <a class="iframe" href="'.xtc_href_link(FILENAME_POPUP_CONTENT, 'coID=2').'" title="Datenschutz" target="_blank">protect your data</a>.');
define('ADD_INFO_TEXT_NEW_LOGIN', '<ul><li>If you are a commercial customer, e.g. a <strong>garage</strong>, you may buy with <strong>special conditions</strong>.<br />For this purpose we need your company registration, which you may send us via our <a href="'.xtc_href_link(FILENAME_CONTENT, 'coID=7').'" title="Contact">contact form</a>.</li>'.($cat_env == 'de' ? '<li>Also we offer <strong>discounts</strong> for <strong>club-</strong> and <strong>ADAC-members</strong>.<br />To get the F�r die ADAC-conditions just indicate your ADAC member number in "Your account" to which you will find the link in the top menu.</li>' : '<li>Also we offer  <strong>discounts</strong> for <strong>club-</strong>members. Please send us your club id card via our <a href="xx_7.html" title="send file via contact form">contact form</a>!')
.'</ul>');
//EOC additional text for login.php, noRiddle

//BOC needed for order mail if deposit, noRiddle
define('AH_INCLUSIVE', 'incl.');
//EOC needed for order mail if deposit, noRiddle

//BOC new texts for catalogue, noRiddle
define('S_TXT_PARTNO', 'Part no.');
define('S_TXT_NAME', 'Name');
define('S_TXT_QTY', 'qty.');
define('S_TXT_CHECK', 'Check');
define('S_TXT_INTOCART', 'Add checked part to the shopping cart');
define('S_SPECIALS_HEADING_TXT', 'A selection of our attractive accessories offers for %s');
define('S_PART_WAS_REPLACED_WITH', 'This part was replaced with  ');
define('IMAGE_BUTTON_CATALOGUE_CHECKOUT', 'Complete order in brand shop');
define('IMAGE_BUTTON_UPDATE_CAT_CART', 'Update %s shopping cart'); //display also brand, noRiddle
define('NR_UPDATE_CART', 'Your %s shopping cart contains:'); //new heading in cart, noRiddle
define('NR_NOW_CREDIT_FOR_VINSEARCH', 'Why do I not see the VIN search field ?<br />With creating your account you got 10 free points in order to be able to use the search by VIN.<br />You may achieve more free VIN requests either through buying spare parts in one of our brand shops or otherwise would have to buy points for more requests <a href="'.xtc_href_link(FILENAME_PRODUCT_INFO, 'products_id=1').'" title="buy points for searches by VIN">&raquo; here</a>.<br />The identification of vehicles by parameter (if available) will be free for you in any case.');
define('TXT_SEND_ORDER_VI_CREDIT', 'We added %d points to your VIN search credits account.');

define('TXT_MAY_WE_HELP', 'Need help ?');
define('TXT_DIRECTLY_TO_PARTNER_BUTTON1', 'Straight to our');
define('TXT_DIRECTLY_TO_PARTNER_BUTTON2', '-partner');
define('TXT_CONTACT_US_IF_NEED_HELP', 'Any questions about this brand or spare parts ?');
define('TXT_IMPORT_INFO_VIN', 'Not all VINs are listed in our database. If your entered VIN cannot be found please use, if it is available, "Identification of car by parameters" on the left.<br />');
define('TXT_IMPORT_INFO_VIN_VAG', 'For technical reasons only approx. 50% of all VINs for this brand are registered. If your entered VIN cannot be found please use "Identification of car by parameters" on the left.<br />');
//EOC new texts for catalogue, noRiddle

//BOC new texts for cookieconsent plugin, noRiddle
define('COOKIES_TXT_NEW', '<i class="fa fa-cog"></i> By using our website you accept the use of cookies which guarantee the unobstructed access to this shop.');
define('COOKIES_LINKTXT_TITLE', 'More information');
define('COOKIES_LINKTXT', '<i class="fa fa-info-circle"></i>');
define('COOKIES_CLOSE_TITLE', 'close');
define('COOKIES_CLOSE', '<i class="fa fa-close"></i>');
//EOC new texts for cookieconsent plugin, noRiddle

//BOC k�ufersiegel, noRiddle
define('KS_THIS_SHOP_WAS', 'This shop was rated on kaeufersiegel.de with ');
define('KS_OF', ' of ');
define('KS_STARES_RATED', 'stars - based on ');
define('KS_RATINGS', ' ratings');
//EOC k�ufersiegel, noRiddle

//BOC new button for login box, noRiddle
define('IMAGE_BUTTON_NEW_CUST', 'New customer ?');
//EOC new button for login box, noRiddle

//BOC button "all brands at a glance" in shops box, 10-2020, noRiddle
define('AH_ALL_BRANDS_MAIN_TXT', 'Main portal');
define('AH_ALL_BRANDS_VIEW_TXT', '&raquo; All brands at a glance');
//EOC button "all brands at a glance" in shops box, 10-2020, noRiddle

//01-2021, noRiddle
define('NO_GENDER', 'No salutation');
?>
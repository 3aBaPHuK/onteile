<?php
/************************************************
* file: checkout_process_end.php
* use: include needed Things in checkout
* (c) noRiddle 07-2017
************************************************/

if (MODULE_PDF_BILL_STATUS== 'True') {
    if(PDF_SEND_ORDER == 'true') {
        require_once(DIR_WS_CLASSES.'FPDF/PdfRechnung.php');
        require_once(DIR_FS_INC.'xtc_php_mail.inc.php');
        require_once(DIR_FS_INC.'xtc_pdf_bill.inc.php');
        require_once(DIR_FS_INC.'xtc_get_attributes_model.inc.php');
        require_once(DIR_FS_INC.'xtc_format_price_order.inc.php');
        require_once(DIR_FS_INC.'xtc_utf8_decode.inc.php');

        //generate billnr and send invoice
        include(DIR_ADMIN.'includes/modules/invoice_number/invoicepdf_number_functions.php');        
        action_checkout_next_ibillnr($insert_id);        
    }
}

//inclusion for affiliate program
if(defined('MODULE_AFFILIATE_MODULE_STATUS') && MODULE_AFFILIATE_MODULE_STATUS == 'true') {
    require(DIR_WS_INCLUDES . 'affiliate_checkout_process.php');
}
?>
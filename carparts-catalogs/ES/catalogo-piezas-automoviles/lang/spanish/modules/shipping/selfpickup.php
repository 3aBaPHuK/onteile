<?PHP
/* -----------------------------------------------------------------------------------------
   $Id: selfpickup.php 5137 2013-07-18 14:48:00Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce( freeamount.php,v 1.01 2002/01/24 03:25:00); www.oscommerce.com
   (c) 2003 nextcommerce (freeamount.php,v 1.4 2003/08/13); www.nextcommerce.org
   (c) 2006 xt:Commerce; www.xt-commerce.com

   Released under the GNU General Public License
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   selfpickup         Autor: sebthom

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

define('MODULE_SHIPPING_SELFPICKUP_TEXT_TITLE', 'Recogida por el cliente');
define('MODULE_SHIPPING_SELFPICKUP_TEXT_DESCRIPTION', 'Recogida por el cliente de la mercancía en nuestra oficina');
define('MODULE_SHIPPING_SELFPICKUP_TEXT_WAY', 'Recogida por el cliente de la mercancía en nuestra oficina');
define('MODULE_SHIPPING_SELFPICKUP_ALLOWED_TITLE' , 'Zonas permitidas');
define('MODULE_SHIPPING_SELFPICKUP_ALLOWED_DESC' , 'Introduzca <b>individualmente</b> las zonas a las que debe ser posible el envío. p.ej. AT,DE (deje este campo vacío si desea permitir todas las zonas))');
define('MODULE_SHIPPING_SELFPICKUP_STATUS_TITLE', 'Activar Recogida por el cliente');
define('MODULE_SHIPPING_SELFPICKUP_STATUS_DESC', '¿Desea ofrecer la Recogida por el cliente?');
define('MODULE_SHIPPING_SELFPICKUP_SORT_ORDER_TITLE', 'Orden de clasificación');
define('MODULE_SHIPPING_SELFPICKUP_SORT_ORDER_DESC', 'Orden de la visualización');
?>

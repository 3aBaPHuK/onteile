<?php
/*******************************************************
* file: affiliate_module.php
* use: language file for system module
* (c) noRiddle 07-2017
* Released under the GNU General Public License
*******************************************************/

define('MODULE_AFFILIATE_MODULE_TEXT_TITLE', 'Módulo de afiliado');
define('MODULE_AFFILIATE_MODULE_TEXT_DESCRIPTION', 'Proporciona la extensión de afiliado.');
define('MODULE_AFFILIATE_MODULE_STATUS_TITLE', '¿Activar módulo?');
define('MODULE_AFFILIATE_MODULE_STATUS_DESC', 'Activar el Módulo de Afiliados');
?>

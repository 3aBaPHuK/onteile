<?php
  /* --------------------------------------------------------------
   $Id: blacklist_logs.php 10584 2017-01-20 10:45:19Z Tomcraft $

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   --------------------------------------------------------------
   Released under the GNU General Public License
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Liste noire logs');

define('TABLE_HEADING_IP', 'Adresse IP');
define('TABLE_HEADING_BANNED', 'Temps bloqué');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_EDIT_ENTRY', 'Modifier l\'entrée');
define('TEXT_NEW_ENTRY', 'Nouvelle entrée');
define('TEXT_ENTRY_IP', 'Adresse IP');
define('TEXT_ENTRY_IP_INFO', 'Indiquez l\'adresse IP qu\'il faut bloquer.');
define('TEXT_ENTRY_TIME', 'Temps');
define('TEXT_ENTRY_TIME_INFO', 'Indiquez la date et l\'heure jusqu\'à quand vous voulez bloquer ladresse IP.');

define('TEXT_DELETE_INTRO', 'Vous en êtes sûr de vouloir supprimer l\'adresse IP?');

define('ERROR_LOG_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le répertoire de sauvegarde n\'existe pas.');
define('ERROR_LOG_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le répertoire de sauvegarde est verrouillé.');
?>

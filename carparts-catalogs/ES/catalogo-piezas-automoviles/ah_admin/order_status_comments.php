<?php
/* -----------------------------------------------------------
*  file: order_status_comments.php
*  use: define pattern texts for order status comments
* (c) noRiddle 12-2016
*  ---------------------------------------------------------*/

require('includes/application_top.php');

//BOC vars
$languages = xtc_get_languages();
$action = (isset($_GET['action']) ? $_GET['action'] : '');
//display per page
$cfg_max_display_results_key = 'MAX_DISPLAY_LIST_PRODUCTS';
$page_max_display_results = xtc_cfg_save_max_display_results($cfg_max_display_results_key);
//EOC vars

//BOC switch action
if (xtc_not_null($action)) {
    switch ($action) {
        case 'set_st_flag':
            if (($_GET['flag'] == '0') || ($_GET['flag'] == '1')) {
                xtc_db_query("UPDATE ".TABLE_ORDER_STATUS_COMMENTS." SET order_status_comments_status = ".(int)$_GET['flag']." WHERE order_status_comments_id = '".(int)$_GET['oscID']."'");
            }
        break;
        case 'insert':
        case 'save':
            $osc_id = (int)$_GET['oscID'];
            
            if ($_GET['action'] == 'insert') {
                $sql_data_array = array('date_added' => 'now()',
                                        'order_status_comments_status' => 1,
                                        'order_status_comments_sort_order' => (int)$_POST['order_status_comments_sort_order']
                                       );
                xtc_db_perform(TABLE_ORDER_STATUS_COMMENTS, $sql_data_array);
                $osc_id = xtc_db_insert_id();
            } elseif ($_GET['action'] == 'save') {
                $sql_data_array = array('last_modified' => 'now()',
                                        'order_status_comments_status' => ($_POST['order_status_comments_status'] == 'on' ? 1 : 0),
                                        'order_status_comments_sort_order' => (int)$_POST['order_status_comments_sort_order']
                                       );
                xtc_db_perform(TABLE_ORDER_STATUS_COMMENTS, $sql_data_array, 'update', "order_status_comments_id = '" . (int)$osc_id . "'");
            }
            
            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                //echo '<pre>'.print_r($_POST, true).'</pre>';
                $order_status_comments_title_array = $_POST['order_status_comments_title'];
                $order_status_comments_text_array = $_POST['order_status_comments_text'];
                $language_id = $languages[$i]['id'];
                
                $sql_data_array = array(
                    'order_status_comments_title' => xtc_db_prepare_input($order_status_comments_title_array[$language_id]),
                    'order_status_comments_text' => xtc_db_prepare_input($order_status_comments_text_array[$language_id])
                );

                if ($action == 'insert') {
                    $insert_sql_data = array(
                        'order_status_comments_id' => (int)$osc_id,
                        'language_id' => $language_id
                    );

                    $sql_data_array = xtc_array_merge($sql_data_array, $insert_sql_data);
                    xtc_db_perform(TABLE_ORDER_STATUS_COMMENTS_TXT, $sql_data_array);
                } elseif ($action == 'save') {
                    $ord_stat_comms_query = xtc_db_query("SELECT * 
                                                        FROM ".TABLE_ORDER_STATUS_COMMENTS_TXT." 
                                                       WHERE language_id = '".$language_id."' 
                                                         AND order_status_comments_id = '".(int)$osc_id."'");
                    if (xtc_db_num_rows($ord_stat_comms_query) == 0) {
                        xtc_db_perform(TABLE_ORDER_STATUS_COMMENTS_TXT, array('order_status_comments_id' => (int)$osc_id , 'language_id' => $language_id));
                    }
                    xtc_db_perform(TABLE_ORDER_STATUS_COMMENTS_TXT, $sql_data_array, 'update', "order_status_comments_id = '".(int)$osc_id."' and language_id = '".$language_id."'");
                }
            }
            
            xtc_redirect(xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, 'page='.$_GET['page'].'&oscID='.(int)$osc_id));
        break;

        case 'deleteconfirm':
            $osc_id = xtc_db_prepare_input((int)$_GET['oscID']);

            if ($_POST['delete_comment_text'] == 'on') {
                xtc_db_query("DELETE FROM ".TABLE_ORDER_STATUS_COMMENTS." WHERE order_status_comments_id = '" . (int)$osc_id . "'");
                xtc_db_query("DELETE FROM ".TABLE_ORDER_STATUS_COMMENTS_TXT." WHERE order_status_comments_id = '" . (int)$osc_id . "'");
            }

            xtc_redirect(xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, 'page=' . (int)$_GET['page']));
          break;
    }
}
//EOC switch action

//BOC HTML
require (DIR_WS_INCLUDES.'head.php');
?>
</head>
<body>
  <!-- header //-->
  <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
  <!-- header_eof //-->
  <!-- body //-->
  <table class="tableBody">
    <tr>
      <?php //left_navigation
      if (USE_ADMIN_TOP_MENU == 'false') {
        echo '<td class="columnLeft2">'.PHP_EOL;
        echo '<!-- left_navigation //-->'.PHP_EOL;       
        require_once(DIR_WS_INCLUDES . 'column_left.php');
        echo '<!-- left_navigation eof //-->'.PHP_EOL; 
        echo '</td>'.PHP_EOL;      
      }
      ?>
      <!-- body_text //-->
      <td class="boxCenter">   
        <div class="pageHeading pdg2 mrg5"><?php echo HEADING_TITLE; ?></div>
        <?php
        if (isset($_GET['action']) && ($_GET['action']=='edit' || $_GET['action']=='new')) {
          if ($_GET['action'] == 'new') {
            unset($_GET['oscID']);
          } else {
            $ord_stat_comm_query = xtc_db_query("SELECT *
                                                   FROM ".TABLE_ORDER_STATUS_COMMENTS."
                                                  WHERE order_status_comments_id = '".(int)$_GET['oscID']."'
                                           ");
            $ord_stat_comms = xtc_db_fetch_array($ord_stat_comm_query);          
          }
          echo xtc_draw_form('order_status_comm', FILENAME_ORDER_STATUS_COMMENTS, 'page='.(int)$_GET['page'].((isset($_GET['oscID'])) ? '&oscID='.(int)$_GET['oscID'] : '').'&action='.(($_GET['action']=='new') ? 'insert' : 'save'), 'post');
          ?>
          <div class="div_box mrg5">       
            <div class="main div_header"><?php echo sprintf(TEXT_ORDER_STATUS_COMMENT, (int)$ord_stat_comms['order_status_comments_sort_order']); ?></div>
              <?php
                echo '<div class="div_box">'; 
                $rowspan = ' rowspan="'. 3 .'"';
                ?>
                <table class="tableConfig borderall">
                  <tr>
                    <?php
                    if($_GET['action']=='edit') {
                        $chkd = false;
                        if($ord_stat_comms['order_status_comments_status'] == '1') {
                            $chkd = true;
                        }
                    ?>
                    <td class="main">
                        <b><?php  echo $lng_image.TEXT_ORDER_STATUS_COMMENTS_STATUS; ?></b> 
                        <?php
                            echo xtc_draw_checkbox_field('order_status_comments_status', '', $chkd);
                        ?>
                    </td>
                    <?php 
                    } else {
                        echo '<td class="main"></td>';
                    }
                    ?>
                    <td class="main">
                        <b><?php  echo $lng_image.TEXT_ORDER_STATUS_COMMENTS_SORT_ORDER; ?></b> <?php echo xtc_draw_input_field('order_status_comments_sort_order', ((isset($ord_stat_comms['order_status_comments_sort_order'])) ? $ord_stat_comms['order_status_comments_sort_order'] : '')); ?>
                    </td>
                  </tr>
                </table>
                <?php
                echo '</div>';
              ?>
            <div style="clear:both;"></div>
            <div class="pdg2">
              <?php
              include('includes/lang_tabs.php');
              for ($i=0; $i<sizeof($languages); $i++) {
                echo ('<div id="tab_lang_' . $i . '">');
                $lng_image = '<div style="float:left;margin-right:5px;">'.xtc_image(DIR_WS_LANGUAGES.$languages[$i]['directory'].'/admin/images/'.$languages[$i]['image']).'</div>';
                $order_status_comments_query = xtc_db_query("SELECT *
                                                        FROM ".TABLE_ORDER_STATUS_COMMENTS_TXT."
                                                       WHERE order_status_comments_id = '".(int)$_GET['oscID']."'
                                                         AND language_id = '".$languages[$i]['id']."'");
                $order_status_comments = xtc_db_fetch_array($order_status_comments_query);
                //echo '<pre>'.print_r($order_status_comments, true).'</pre>';
                ?>
                <table class="tableInput border0">
                  <tr>
                    <td class="main"><b><?php  echo $lng_image.TEXT_ORDER_STATUS_COMMENTS_TITLE; ?></b></td>
                    <td class="main"><?php echo xtc_draw_input_field('order_status_comments_title['.$languages[$i]['id'].']', ((isset($order_status_comments['order_status_comments_title'])) ? $order_status_comments['order_status_comments_title'] : ''), 'style="width:100%" maxlength="255"'); ?></td>
                  </tr>
                  <tr>
                    <td class="main"><b><?php  echo $lng_image.TEXT_ORDER_STATUS_COMMENTS_TEXT; ?></b></td>
                    <td class="main"><?php echo xtc_draw_textarea_field('order_status_comments_text['.$languages[$i]['id'].']', 'soft', '100%', '18', ((isset($order_status_comments['order_status_comments_text'])) ? $order_status_comments['order_status_comments_text'] : '')); ?></td>
                  </tr>
                </table>
                <?php
                echo ('</div>');
              } ?>
            </div>
            <div style="clear:both;"></div>
            <!-- BOF Save block //-->
            <div class="txta-r">
              <?php echo xtc_button_link(BUTTON_CANCEL, xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, 'page='.(int)$_GET['page'].'&oscID='.(int)$_GET['order_status_comments_id'])).'&nbsp;'.xtc_button(BUTTON_SAVE); ?>
            </div>
            <!-- EOF Save block //-->
          </div>
          </form>
        <?php } else { ?>
          <table class="tableCenter">
            <tr>
              <td class="boxCenterLeft">
                <table class="tableBoxCenter collapse">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ORDER_STATUS_COMMENTS_ID.xtc_sorting(FILENAME_ORDER_STATUS_COMMENTS,'osc_id'); ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ORDER_STATUS_COMMENTS_TITLE.xtc_sorting(FILENAME_ORDER_STATUS_COMMENTS,'osc_title'); ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ORDER_STATUS_COMMENTS_SORT_ORDER.xtc_sorting(FILENAME_ORDER_STATUS_COMMENTS,'osc_sort_order'); ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ORDER_STATUS_COMMENTS_STATUS.xtc_sorting(FILENAME_ORDER_STATUS_COMMENTS,'osc_status'); ?></td>
                    <td class="dataTableHeadingContent txta-r"><?php echo TABLE_HEADING_ORDER_STATUS_COMMENTS_ACTION; ?>&nbsp;</td>
                  </tr>
                  <?php
                  //BOC sorting
                $sorting = (isset($_GET['sorting']) ? $_GET['sorting'] : '');
                    if (xtc_not_null($sorting)) {
                        switch ($sorting) {
                            case 'osc_id':
                                $sort = 'osc.order_status_comments_id ASC';
                              break;
                            case 'osc_id-desc':
                                $sort = 'osc.order_status_comments_id DESC';
                              break;
                            case 'osc_title':
                                $sort = 'osct.order_status_comments_title ASC';
                              break;
                            case 'osc_title-desc':
                                $sort = 'osct.order_status_comments_title DESC';
                              break;
                            case 'osc_status':
                                $sort = 'osc.order_status_comments_status ASC';
                              break;
                            case 'osc_status-desc':
                                $sort = 'osc.order_status_comments_status DESC';
                              break;
                            case 'osc_sort_order':
                                $sort = 'osc.order_status_comments_sort_order ASC';
                              break;
                            case 'osc_sort_order-desc':
                                $sort = 'osc.order_status_comments_sort_order DESC';
                              break;
                            default:
                                $sort = 'osc.order_status_comments_id DESC';
                             break;
                    }
                } else {
                    $sort    = 'osc.order_status_comments_id ASC';
                }
                //EOC sorting
                  
                  $order_status_comments_query_raw = "SELECT osc.order_status_comments_id,
                                                             osc.order_status_comments_sort_order,
                                                             osc.order_status_comments_status,
                                                             osc.date_added,
                                                             osc.last_modified,
                                                             osct.order_status_comments_title,
                                                             osct.order_status_comments_text
                                                 FROM ".TABLE_ORDER_STATUS_COMMENTS." osc
                                            LEFT JOIN ".TABLE_ORDER_STATUS_COMMENTS_TXT." osct
                                                   ON osct.order_status_comments_id = osc.order_status_comments_id
                                                  AND osct.language_id = '".(int)$_SESSION['languages_id']."'
                                             ORDER BY ".$sort;
                  $order_status_comments_split = new splitPageResults($_GET['page'], $page_max_display_results, $order_status_comments_query_raw, $order_status_comments_query_numrows);
                  $order_status_comments_query = xtc_db_query($order_status_comments_query_raw);
                  while($order_status_comments = xtc_db_fetch_array($order_status_comments_query)) {
                    if (((!$_GET['oscID']) || (@$_GET['oscID'] == $order_status_comments['order_status_comments_id'])) && (!$oscInfo) && (substr($_GET['action'], 0, 3) != 'new')) {
                      $oscInfo = new objectInfo($order_status_comments);
                    }

                    if ((is_object($oscInfo)) && ($order_status_comments['order_status_comments_id'] == $oscInfo->order_status_comments_id)) {
                      echo '<tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'pointer\'" onclick="document.location.href=\''.xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, 'page='.(int)$_GET['page']. '&oscID='.$order_status_comments['order_status_comments_id'].'&action=edit').'\'">'."\n";
                    } else {
                      echo '<tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'pointer\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, 'page='.(int)$_GET['page'].'&oscID='.$order_status_comments['order_status_comments_id']).'\'">'."\n";
                    }
                  ?>
                  <td class="dataTableContent"><?php echo $order_status_comments['order_status_comments_id']; ?></td>
                  <td class="dataTableContent"><?php echo $order_status_comments['order_status_comments_title']; ?></td>
                  <td class="dataTableContent"><?php echo $order_status_comments['order_status_comments_sort_order']; ?></td>
                  <td class="dataTableContent">
                  <?php
                  if ($order_status_comments['order_status_comments_status'] == '1') {
                    echo xtc_image(DIR_WS_IMAGES.'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10).'<a href="' . xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, xtc_get_all_get_params(array('action', 'oscID')) . 'action=set_st_flag&flag=0&oscID=' . $order_status_comments['order_status_comments_id']).'">&nbsp;&nbsp;'.xtc_image(DIR_WS_IMAGES.'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10).'</a>';
                  } else {
                    echo '<a href="'.xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, xtc_get_all_get_params(array('action', 'oscID')).'action=set_st_flag&flag=1&oscID='.$order_status_comments['order_status_comments_id']).'">'.xtc_image(DIR_WS_IMAGES.'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10).'</a>&nbsp;&nbsp;'.xtc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
    }
                  ?>
                  </td>
                  <td class="dataTableContent txta-r">
                    <?php 
                    if ((is_object($oscInfo)) && ($order_status_comments['order_status_comments_id'] == $oscInfo->order_status_comments_id)) {
                        echo xtc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ICON_ARROW_RIGHT);
                    } else { 
                        echo '<a href="' . xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, 'page='.(int)$_GET['page'].'&oscID=' . $order_status_comments['order_status_comments_id']).'">'.xtc_image(DIR_WS_IMAGES . 'icon_arrow_grey.gif', IMAGE_ICON_INFO).'</a>'; } 
                  ?>
                  &nbsp;
                  </td>
                </tr>
                <?php
                  }
                ?>              
                </table>
                <div class="smallText pdg2 flt-l"><?php echo $order_status_comments_split->display_count($order_status_comments_query_numrows, $page_max_display_results, (int)$_GET['page'], TEXT_DISPLAY_NUMBER_OF_ORDER_STYTUS_COMMENTS); ?></div>
                <div class="smallText pdg2 flt-r"><?php echo $order_status_comments_split->display_links($order_status_comments_query_numrows, $page_max_display_results, MAX_DISPLAY_PAGE_LINKS, (int)$_GET['page'], xtc_get_all_get_params(array('page'))); ?></div>
                <?php echo draw_input_per_page($PHP_SELF,$cfg_max_display_results_key, $page_max_display_results); ?>
                <?php
                if ($_GET['action'] != 'new') {
                ?>
                  <div class="smallText pdg2 flt-r"><?php echo xtc_button_link(BUTTON_INSERT, xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, 'page=' . (int)$_GET['page'] . '&action=new')); ?></div>
                <?php
                }
                ?>
              </td>
              <?php
                $heading = array();
                $contents = array();
                switch ($_GET['action']) {
                              
                  case 'delete':
                    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_ORDER_STATUS_COMMENT . '</b>');
                    $contents = array('form' => xtc_draw_form('order_status_comment', FILENAME_ORDER_STATUS_COMMENTS, 'page=' . (int)$_GET['page'] . '&oscID=' . $oscInfo->order_status_comments_id . '&action=deleteconfirm'));
                    $contents[] = array('text' => TEXT_INFO_DELETE_COMMENT_INTRO);
                    $contents[] = array('text' => '<br /><b>' . $oscInfo->order_status_comments_title . '</b>');
                    $contents[] = array('text' => '<br />' . xtc_draw_checkbox_field('delete_comment_text', 'on', true) . ' ' . TEXT_DELETE_COMMENT);
                    $contents[] = array('align' => 'center', 'text' => '<br />' . xtc_button(BUTTON_DELETE) . '&nbsp;' . xtc_button_link(BUTTON_CANCEL, xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, 'page=' . (int)$_GET['page'] . '&oscID=' . $oscInfo->order_status_comments_id)));
                    break;

                  default:
                    if (is_object($oscInfo)) {
                      $heading[] = array('text' => '<b>' . $oscInfo->order_status_comments_title . '</b>');
                      $contents[] = array('align' => 'center', 'text' => xtc_button_link(BUTTON_EDIT, xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, 'page=' . (int)$_GET['page'] . '&oscID=' . $oscInfo->order_status_comments_id . '&action=edit')) . '&nbsp;' . xtc_button_link(BUTTON_DELETE, xtc_href_link(FILENAME_ORDER_STATUS_COMMENTS, 'page=' . (int)$_GET['page'] . '&oscID=' . $oscInfo->order_status_comments_id . '&action=delete')));
                      $contents[] = array('text' => '<br />' . TEXT_DATE_ADDED_ORDER_STATUS_COMMENT . ' ' . xtc_date_short($oscInfo->date_added));
                      if (xtc_not_null($oscInfo->last_modified)) {
                        $contents[] = array('text' => TEXT_LAST_MODIFIED_ORDER_STATUS_COMMENT . ' ' . xtc_date_short($oscInfo->last_modified));
                      }
                    }
                    break;
                }

                if ( (xtc_not_null($heading)) && (xtc_not_null($contents)) ) {
                  echo '            <td class="boxRight">' . "\n";
                  $box = new box;
                  echo $box->infoBox($heading, $contents);
                  echo '            </td>' . "\n";
                }
              ?>
            </tr>
          </table>
        <?php } ?>
      </td>
      <!-- body_text_eof //-->
    </tr>
  </table>
  <!-- body_eof //-->
  <!-- footer //-->
  <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
  <!-- footer_eof //-->
  <br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>